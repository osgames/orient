﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using DInput = Microsoft.DirectX.DirectInput;
using DX = Microsoft.DirectX;

namespace IONServerSocket
{
    public class DancePad
    {
        public static DInput.JoystickState state = new DInput.JoystickState();
        private DInput.Device applicationDevice = null;
        private List<String> pressedButton = new List<String>();
        private bool pressed = false;
        private bool[] buttonHeld = new bool[16];
        private Server server = null;

        public DancePad(Server server)
        {
            this.server = server;
            foreach (DInput.DeviceInstance instance in DInput.Manager.GetDevices(DInput.DeviceClass.GameControl, DInput.EnumDevicesFlags.AttachedOnly))
            {
                 // Create the device.  Just pick the first one
                applicationDevice = new DInput.Device(instance.InstanceGuid);
                String instanceString = applicationDevice.DeviceInformation.InstanceName;
                if (instanceString.Contains("Joypad") || instanceString.Contains("TigerGame") || instanceString.Contains("XBOX DDR") || instanceString.Contains("Joystick"))
                {
                    break;
                }
                else
                {
                    applicationDevice = null;
                }

            }

            if (this.applicationDevice != null)
            {
                Console.WriteLine("applicationDevice gefunden");
                Console.WriteLine(applicationDevice.DeviceInformation.ToString());

                // Set the data format to the c_dfDIJoystick pre-defined format.
                applicationDevice.SetDataFormat(DInput.DeviceDataFormat.Joystick);
                // Set the cooperative level for the device.
                //applicationDevice.s.SetCooperativeLevel(this, DInput.CooperativeLevelFlags.Exclusive | DInput.CooperativeLevelFlags.Foreground);
                // Enumerate all the objects on the device.
                foreach (DInput.DeviceObjectInstance d in applicationDevice.Objects)
                {
                    // For axes that are returned, set the DIPROP_RANGE property for the
                    // enumerated axis in order to scale min/max values.

                    if ((0 != (d.ObjectId & (int)DInput.DeviceObjectTypeFlags.Axis)))
                    {
                        // Set the range for the axis.
                        applicationDevice.Properties.SetRange(DInput.ParameterHow.ById, d.ObjectId, new DInput.InputRange(-1000, +1000));
                    }
                }

                // init bool buffer that remembers held buttons
                for (int i = 0; i < 16; i++) buttonHeld[i] = false;

                try
                {
                    // Acquire the device.
                    applicationDevice.Acquire();
                }
                catch (DInput.InputException exc)
                {
                    Console.WriteLine(exc.ErrorString);
                    // Failed to acquire the device.
                    // This could be because the app doesn't have focus.
                    return;
                }
                Thread th = new Thread(new ThreadStart(Run));
                th.Start();

            }

        }

        public void Run()
        {
            Console.WriteLine("Thread gestartet");
            while (true)
            {
                this.GetData();
                Thread.Sleep(100);
            }
        }

        public void GetData()
        {
            // Make sure there is a valid device.
            if (null == applicationDevice)
                return;

            try
            {
                // Poll the device for info.
                applicationDevice.Poll();
            }
            catch (DInput.InputException inputex)
            {
                Console.WriteLine(inputex.ErrorString);
                if ((inputex is DInput.NotAcquiredException) || (inputex is DInput.InputLostException))
                {
                    // Check to see if either the app needs to acquire the device, or
                    // if the app lost the device to another process.
                    try
                    {
                        // Acquire the device.
                        applicationDevice.Acquire();
                    }
                    catch (DInput.InputException)
                    {
                        Console.WriteLine("InputException");
                        // Failed to acquire the device.
                        // This could be because the app doesn't have focus.
                        return;
                    }
                }

            } //catch(InputException inputex)

            // Get the state of the device.
            try { state = applicationDevice.CurrentJoystickState; }
            // Catch any exceptions. None will be handled here, any device re-aquisition will be handled above.  
            catch (DInput.InputException)
            {
                Console.WriteLine("Fehler - State");
                return;
            }
            byte[] buttons = state.GetButtons();

            String instName = applicationDevice.DeviceInformation.InstanceName;

            // button mappings for augsburg's ps2 dance mat (registers as joypad)
            if (instName.Contains("Joypad"))
            {
                if (buttons[8] > 127)
                {
                    if (!buttonHeld[8])
                    {                    
                        pressedButton.Add("Select");
                        pressed = true;
                        buttonHeld[8] = true;
                    }
                } 
                else buttonHeld[8] = false;
                if (buttons[9] > 127)
                {
                    if (!buttonHeld[9])
                    {
                        pressedButton.Add("Start");
                        pressed = true;
                        buttonHeld[9] = true;
                    }
                }
                else buttonHeld[9] = false;
                if (buttons[2] > 127)
                {
                    if (!buttonHeld[2])
                    {
                        pressedButton.Add("Cross");
                        pressed = true;
                        buttonHeld[2] = true;
                    }
                }
                else buttonHeld[2] = false;
                if (buttons[1] > 127)
                {
                    if (!buttonHeld[1])
                    {
                        pressedButton.Add("Circle");
                        pressed = true;
                        buttonHeld[1] = true;
                    }
                }
                else buttonHeld[1] = false;
                if (buttons[0] > 127)
                {
                    if (!buttonHeld[0])
                    {
                        pressedButton.Add("Triangle");
                        pressed = true;
                        buttonHeld[0] = true;
                    }
                }
                else buttonHeld[0] = false;
                if (buttons[3] > 127)
                {
                    if (!buttonHeld[3])
                    {
                        pressedButton.Add("Square");
                        pressed = true;
                        buttonHeld[3] = true;
                    }
                }
                else buttonHeld[3] = false;
                if (buttons[12] > 127)
                {
                    if (!buttonHeld[12])
                    {
                        pressedButton.Add("Up");
                        pressed = true;
                        buttonHeld[12] = true;
                    }
                }
                else buttonHeld[12] = false;
                if (buttons[14] > 127)
                {
                    if (!buttonHeld[14])
                    {
                        pressedButton.Add("Down");
                        pressed = true;
                        buttonHeld[14] = true;
                    }
                }
                else buttonHeld[14] = false;
                if (buttons[15] > 127)
                {
                    if (!buttonHeld[15])
                    {
                        pressedButton.Add("Left");
                        pressed = true;
                        buttonHeld[15] = true;
                    }
                }
                else buttonHeld[15] = false;
                if (buttons[13] > 127)
                {
                    if (!buttonHeld[13])
                    {
                        pressedButton.Add("Right");
                        pressed = true;
                        buttonHeld[13] = true;
                    }
                }
                else buttonHeld[13] = false;
            }
            else if (instName.Contains("TigerGame")) // button mappings for HW's ps2 dance mat (registers as TigerGame), almost same as above but select and start are swapped
            {
                if (buttons[8] > 127)
                {
                    if (!buttonHeld[8])
                    {
                        pressedButton.Add("Start");
                        pressed = true;
                        buttonHeld[8] = true;
                    }
                }
                else buttonHeld[8] = false;
                if (buttons[9] > 127)
                {
                    if (!buttonHeld[9])
                    {
                        pressedButton.Add("Select");
                        pressed = true;
                        buttonHeld[9] = true;
                    }
                }
                else buttonHeld[9] = false;
                if (buttons[2] > 127)
                {
                    if (!buttonHeld[2])
                    {
                        pressedButton.Add("Cross");
                        pressed = true;
                        buttonHeld[2] = true;
                    }
                }
                else buttonHeld[2] = false;
                if (buttons[1] > 127)
                {
                    if (!buttonHeld[1])
                    {
                        pressedButton.Add("Circle");
                        pressed = true;
                        buttonHeld[1] = true;
                    }
                }
                else buttonHeld[1] = false;
                if (buttons[0] > 127)
                {
                    if (!buttonHeld[0])
                    {
                        pressedButton.Add("Triangle");
                        pressed = true;
                        buttonHeld[0] = true;
                    }
                }
                else buttonHeld[0] = false;
                if (buttons[3] > 127)
                {
                    if (!buttonHeld[3])
                    {
                        pressedButton.Add("Square");
                        pressed = true;
                        buttonHeld[3] = true;
                    }
                }
                else buttonHeld[3] = false;
                if (buttons[12] > 127)
                {
                    if (!buttonHeld[12])
                    {
                        pressedButton.Add("Up");
                        pressed = true;
                        buttonHeld[12] = true;
                    }
                }
                else buttonHeld[12] = false;
                if (buttons[14] > 127)
                {
                    if (!buttonHeld[14])
                    {
                        pressedButton.Add("Down");
                        pressed = true;
                        buttonHeld[14] = true;
                    }
                }
                else buttonHeld[14] = false;
                if (buttons[15] > 127)
                {
                    if (!buttonHeld[15])
                    {
                        pressedButton.Add("Left");
                        pressed = true;
                        buttonHeld[15] = true;
                    }
                }
                else buttonHeld[15] = false;
                if (buttons[13] > 127)
                {
                    if (!buttonHeld[13])
                    {
                        pressedButton.Add("Right");
                        pressed = true;
                        buttonHeld[13] = true;
                    }
                }
                else buttonHeld[13] = false;
            }
            else if (instName.Contains("XBOX DDR")) //button mappings for the xbox dancemat
            {
                if (buttons[8] > 127)
                {
                    if (!buttonHeld[8])
                    {
                        pressedButton.Add("Start");
                        pressed = true;
                        buttonHeld[8] = true;
                    }
                }
                else buttonHeld[8] = false;
                if (buttons[9] > 127)
                {
                    if (!buttonHeld[9])
                    {
                        pressedButton.Add("Select");
                        pressed = true;
                        buttonHeld[9] = true;
                    }
                }
                else buttonHeld[9] = false;
                if (buttons[2] > 127)
                {
                    if (!buttonHeld[2])
                    {
                        pressedButton.Add("Up");
                        pressed = true;
                        buttonHeld[2] = true;
                    }
                }
                else buttonHeld[2] = false;
                if (buttons[1] > 127)
                {
                    if (!buttonHeld[1])
                    {
                        pressedButton.Add("Down");
                        pressed = true;
                        buttonHeld[1] = true;
                    }
                }
                else buttonHeld[1] = false;
                if (buttons[0] > 127)
                {
                    if (!buttonHeld[0])
                    {
                        pressedButton.Add("Left");
                        pressed = true;
                        buttonHeld[0] = true;
                    }
                }
                else buttonHeld[0] = false;
                if (buttons[3] > 127)
                {
                    if (!buttonHeld[3])
                    {
                        pressedButton.Add("Right");
                        pressed = true;
                        buttonHeld[3] = true;
                    }
                }
                else buttonHeld[3] = false;
                if (buttons[4] > 127)
                {
                    if (!buttonHeld[4])
                    {
                        pressedButton.Add("LeftDown");
                        pressed = true;
                        buttonHeld[4] = true;
                    }
                }
                else buttonHeld[4] = false;
                if (buttons[6] > 127)
                {
                    if (!buttonHeld[6])
                    {
                        pressedButton.Add("LeftUp");
                        pressed = true;
                        buttonHeld[6] = true;
                    }
                }
                else buttonHeld[6] = false;
                if (buttons[7] > 127)
                {
                    if (!buttonHeld[7])
                    {
                        pressedButton.Add("RightUp");
                        pressed = true;
                        buttonHeld[7] = true;
                    }
                }
                else buttonHeld[7] = false;
                if (buttons[5] > 127)
                {
                    if (!buttonHeld[5])
                    {
                        pressedButton.Add("RightDown");
                        pressed = true;
                        buttonHeld[5] = true;
                    }
                }
                else buttonHeld[5] = false;
            }
            else if (instName.Contains("Joystick")) // button mappings for the pc dance mat (registers as USB Joystick)
            {
                if (buttons[9] > 127)
                {
                    if (!buttonHeld[9])
                    {
                        pressedButton.Add("Start");
                        pressed = true;
                        buttonHeld[9] = true;
                    }
                }
                else buttonHeld[9] = false;
                if (buttons[8] > 127)
                {
                    if (!buttonHeld[8])
                    {
                        pressedButton.Add("Select");
                        pressed = true;
                        buttonHeld[8] = true;
                    }
                }
                else buttonHeld[8] = false;
                if (buttons[2] > 127)
                {
                    if (!buttonHeld[2])
                    {
                        pressedButton.Add("Up");
                        pressed = true;
                        buttonHeld[2] = true;
                    }
                }
                else buttonHeld[2] = false;
                if (buttons[1] > 127)
                {
                    if (!buttonHeld[1])
                    {
                        pressedButton.Add("Down");
                        pressed = true;
                        buttonHeld[1] = true;
                    }
                }
                else buttonHeld[1] = false;
                if (buttons[3] > 127)
                {
                    if (!buttonHeld[3])
                    {
                        pressedButton.Add("Right");
                        pressed = true;
                        buttonHeld[3] = true;
                    }
                }
                else buttonHeld[3] = false;
                if (buttons[0] > 127)
                {
                    if (!buttonHeld[0])
                    {
                        pressedButton.Add("Left");
                        pressed = true;
                        buttonHeld[0] = true;
                    }
                }
                else buttonHeld[0] = false;
                if (buttons[4] > 127)
                {
                    if (!buttonHeld[4])
                    {
                        pressedButton.Add("RightDown");
                        pressed = true;
                        buttonHeld[4] = true;
                    }
                }
                else buttonHeld[4] = false;
                if (buttons[7] > 127)
                {
                    if (!buttonHeld[7])
                    {
                        pressedButton.Add("LeftUp");
                        pressed = true;
                        buttonHeld[7] = true;
                    }
                }
                else buttonHeld[7] = false;
                if (buttons[6] > 127)
                {
                    if (!buttonHeld[6])
                    {
                        pressedButton.Add("RightUp");
                        pressed = true;
                        buttonHeld[6] = true;
                    }
                }
                else buttonHeld[6] = false;
                if (buttons[5] > 127)
                {
                    if (!buttonHeld[5])
                    {
                        pressedButton.Add("LeftDown");
                        pressed = true;
                        buttonHeld[5] = true;
                    }
                }
                else buttonHeld[5] = false;
            }
            if (pressed)
            {
                foreach (String button in pressedButton)
                {
                    String xmlString = server.createXML("DancePad", "", "Movement", button);
                    server.incomingXML(xmlString);

                }
            }
           
            pressed = false;
            pressedButton.Clear();

            //TODO: Method to process xmlString with ION
        }
    }
    
}
