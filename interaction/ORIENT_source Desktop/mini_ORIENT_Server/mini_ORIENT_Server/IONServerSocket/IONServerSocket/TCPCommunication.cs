﻿using System;
using System.Text;
using System.Collections;
using System.Threading;
using System.IO;
using System.Net.Sockets;


namespace IONServerSocket 
{
    public class TCPCommunication
    {
        // Der Listener
        private static TcpListener listener = null;
        // Die Liste der laufenden Server-Threads
        private static ArrayList threads = new ArrayList();
        private Thread th;
        private Server server;

        public TCPCommunication(Server server)
        {
            //Kommunikation über TCP
            int port = 1183;
            // Listener initialisieren und starten
            listener = new TcpListener(port);
            listener.Start();
            this.server = server;
           
            // Haupt-Server-Thread initialisieren und starten
            th = new Thread(new ThreadStart(Run));
            th.Start();
            Console.WriteLine("Server is running on port:" + port);
        }
        // Hauptthread des Servers
        // Nimmt die Verbindungswünsche von Clients entgegen
        // und startet die Server-Threads für die Clients
        public void Run()
        {
            String cmd = "";
            Console.WriteLine("Start");
            while (true)
            {
                //cmd = Console.ReadLine();
                //if (!cmd.ToLower().Equals("stop"))
                //    Console.WriteLine("Unbekannter Befehl: " + cmd);

                // Wartet auf eingehenden Verbindungswunsch
                TcpClient c = listener.AcceptTcpClient();
                // Initialisiert und startet einen Server-Thread
                // und fügt ihn zur Liste der Server-Threads hinzu
                Console.WriteLine("New Client has been connected");
                threads.Add(new ServerThread(c, server));
            }

            // Haupt-Server-Thread stoppen
            th.Abort();
            // Alle Server-Threads stoppen
            for (IEnumerator e = threads.GetEnumerator(); e.MoveNext(); )
            {
                // Nächsten Server-Thread holen
                ServerThread st = (ServerThread)e.Current;
                // und stoppen
                st.stop = true;
                while (st.running)
                    Thread.Sleep(1000);
            }
            // Listener stoppen
            listener.Stop();  
        }
    }

    //Thread für verbundenen Client
    class ServerThread
    {
        // Stop-Flag
        public bool stop = false;
        // Flag für "Thread läuft"
        public bool running = false;
        // Die Verbindung zum Client
        private TcpClient connection = null;
        private Server server = null;
        // Speichert die Verbindung zum Client und startet den Thread
        public ServerThread(TcpClient connection, Server ser)
        {
            this.server = ser;
            // Speichert die Verbindung zu Client,
            // um sie später schließen zu können
            this.connection = connection;
            // Initialisiert und startet den Thread
            new Thread(new ThreadStart(Run)).Start();
        }

        // Der eigentliche Thread
        public void Run()
        {
            // Setze Flag für "Thread läuft"
            this.running = true;
            // Hole den Stream für's lesen
            Stream stream = this.connection.GetStream();
            StreamReader input = new StreamReader(stream);
            StreamWriter output = new StreamWriter(stream);
            String send = null;
            bool loop = true;
            while (loop)
            {
                try
                {
                    //Nachricht erhalten
                    send = input.ReadLine();
                    Console.WriteLine(send);
                    //Nachricht versenden: 1. Orient; 2. Bluetooth-Server
                    server.incomingXML(send);
                    //Console.WriteLine("-----------------SENDEN-----------------");
                    sendToBluetoothServer(output, send);

                    // Wiederhole die Schleife so lange bis von außen der Stopwunsch kommt
                    loop = !this.stop;
                }
                catch (Exception)
                {
                    // oder bis ein Fehler aufgetreten ist
                    loop = false;
                }
            }
            // Schließe die Verbindung zum Client
            this.connection.Close();
            // Setze das Flag "Thread läuft" zurück
            this.running = false;
        }

        private void sendToBluetoothServer(StreamWriter output, String toSend)
        {
            char c = '\n';
            toSend = toSend + c;
            output.Write(toSend);
            output.Flush();
            Console.WriteLine("Sent to Bluetooth-Server: " + toSend);
        }
    }
}
