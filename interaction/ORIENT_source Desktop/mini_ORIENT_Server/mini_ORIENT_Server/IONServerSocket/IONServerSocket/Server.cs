using System;
using System.Text;
using System.Collections;
using System.Threading;
using System.IO;
using System.Net.Sockets;

namespace IONServerSocket
{
    public class Server
    {
        // Kommunikation mit WII �ber UDP
        private static UDPCommunication udpCom = null;
        // Kommunikation mit Handy �ber TCP
        private static TCPCommunication tcpCom = null;
        // DancePad
        private static DancePad dance = null;

        private static TcpClient tcpclnt = null;

        private string hostName;

        private int port;

        // create a new instance of the server by specifying the orient host adress and port
        public Server(string hostName_in, int port_in)
        {
            //DancePad
            //Console.WriteLine("Tanzmatte starten");
            dance = new DancePad(this);

            //Kommunikation �ber UDP
            udpCom = new UDPCommunication(this);

            //Kommunikation �ber TCP
            tcpCom = new TCPCommunication(this);

            //set default port and adress
            hostName = hostName_in;
            port = port_in;

        }

        // constructor with default values for host name and port
        public Server() : this("localhost",45700) {}

        // main entry point for the program
        public static void Main(string[] args)
        {
            Server s = null;
            if (args.Length == 2)
            {
                try
                {
                    string hostName = args[0];
                    int port = int.Parse(args[1]);
                    s = new Server(hostName, port);
                }
                catch
                {
                    Console.WriteLine("Could not convert argument 2 (port number), not a number ");
                    return;
                }
            }

            // no or not the right number of arguments provided  
            if (s == null) 
                s = new Server();     
        }

        public String createXML(String device, String address, String type, String value)
        {
            String xmlString = "<input>" +
                                     "<device>" + device + "</device>" + //mobile phone, wii or dance mat
                                     "<address>" + address + "</address>" + //Address of the device e.g. bluetooth address
                                     "<interaction>" +
                                          "<type>" + type + "</type>" + //keyboard, speech, emotion, rfid or gesture and movement
                                          "<value>" + value + "</value>" + // there are 1 or values possible (e.g. for wii-gesture and speech recognition. Thus we can send an n-best list with the probability
                                     "</interaction>" +
                               "</input>";

            return xmlString;
        }

        public void incomingXML(String xml)
        {
            Console.WriteLine("xml:" + xml);

            tcpclnt = new TcpClient();
            Console.WriteLine("Connecting.....");

            tcpclnt.Connect(hostName, port);
            StreamWriter SW = new StreamWriter(tcpclnt.GetStream());


            SW.Write(xml + "<EOF>");
            SW.Flush();
            Console.WriteLine("Message sent to ORIENT: " + xml);


        }

        
    }
}