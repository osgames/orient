﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Xml;
using System.Threading;

namespace IONServerSocket
{
    public class UDPCommunication
    {
            private static int listenPort = 2222;
            private static int remotePort = 3333;
            private Socket sendSocket = new Socket(AddressFamily.InterNetwork,
                                    SocketType.Dgram,
                                    ProtocolType.Udp);
            IPAddress address = IPAddress.Parse("127.0.0.1");
            public delegate void IncomingMessage(string message);
            public static event IncomingMessage MessageArrives;

            private Server server = null;
            

            public UDPCommunication(Server server)
            {
                this.server = server;
                Thread listiningThread = new Thread(new ThreadStart(startListining));
                listiningThread.Start();
            }

            void Form1_MessageSend(string message)
            {
                send(message);
            }


            private void startListining()
            {

                UdpClient udpListener = new UdpClient(listenPort);
                IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, listenPort);

                try
                {
                    while (true)
                    {
                        Console.WriteLine("Waiting for incoming message");
                        byte[] bytes = udpListener.Receive(ref groupEP);
                        //string incomingMessage = Encoding.ASCII.GetString(bytes, 0, bytes.Length);
                        string incomingMessage = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                        //Console.WriteLine("Received message from {0} :\n {1}\n",
                        //    groupEP.ToString(),
                        //
                        string message = String.Format("Received message from {0} :\n {1}\n",
                            groupEP.ToString(), incomingMessage);
                        Console.WriteLine(message);
                        if (MessageArrives != null)
                            MessageArrives(message);

                        //Value aus erhaltener XML auslesen
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(incomingMessage);
                        XmlElement root = xmlDoc.DocumentElement;
                        string value ="";
                        string type = "";
                        if(root.Name!=null&&root.Name.Equals("ClassificationResult")){
                            value = root.SelectSingleNode("./WinnerClasses/Class/Name").InnerText;
                            type = "gesture";
                            string xmlString = server.createXML("Wii", address.ToString() + listenPort.ToString(), type, value);
                            server.incomingXML(xmlString);
                        }else{
                            if(root.Name!=null&&root.Name.Equals("WiimoteState")){
                                XmlNode node = root.SelectSingleNode("./Buttons");
                                XmlNodeList list = node.ChildNodes;
                                type = "choice";
                               
                                foreach (XmlNode button in list)
                                {
                                    string text = button.InnerText;
                                    if (text.Equals("True"))
                                    {
                                        value = button.Name;
                                        string xmlString = server.createXML("Wii", address.ToString() + listenPort.ToString(), type, value);
                                        server.incomingXML(xmlString);
                                    }
                                }
                                                              
                            }
                        }

                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally
                {
                    udpListener.Close();
                }
            }

            public void send(string messageToSend)
            {
                byte[] sendbuf = Encoding.UTF8.GetBytes(messageToSend);
                IPEndPoint ep = new IPEndPoint(address, remotePort);
                sendSocket.SendTo(sendbuf, ep);
                Console.WriteLine("Message sent to the remote address");
            }
    }
}
