package de.uni.augsburg.informatik.mm.map.desktop.connection.discovery;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Set;
import java.util.Vector;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;


//import de.avetana.bluetooth.connection.BadURLFormat;
//import de.avetana.bluetooth.connection.JSR82URL;
//import de.avetana.bluetooth.util.ServiceDescriptor;
//import de.avetana.bluetooth.util.ServiceFinderPane;
import de.uni.augsburg.informatik.mm.map.desktop.connection.discovery.Discovery;
import de.uni.augsburg.informatik.mm.map.desktop.connection.MapConnectionController;

public class MapDiscovery {
	//private ServiceFinderPane m_servicePanel;
	private Discovery myDiscoverer;
	private String SERVICE_NAME = "MapServiceAPMobile";
	private Vector<String>scannedUrls = new Vector<String>();
	private Hashtable<String, String> storedConnections;
	private MapConnectionController mapConnectionController;
	
	
	/**
	 * @param mapConnectionController
	 * @param storedConnections
	 */
	public MapDiscovery(MapConnectionController mapConnectionController, Hashtable<String, String> storedConnections){
		try {
			//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.storedConnections = storedConnections;
			this.mapConnectionController = mapConnectionController;
			//m_servicePanel = new ServiceFinderPaneExtended(this,"lastBTSearchRFComm", new UUID[] { new UUID(0x100) });
			//m_servicePanel.m_refresh.setVisible(false);
			//m_servicePanel.m_select.setVisible(false);

			//this.setVisible(true);
			//myDiscoverer = new Discovery();
			myDiscoverer = new Discovery(null, this); // starts thread and returns
			
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public synchronized void analyzeServices(Vector<ServiceRecord> currServices) {
			for (int i = 0; i < currServices.size(); i++) {
				ServiceRecord s = (ServiceRecord) currServices.elementAt(i);
				if (s != null && s.getAttributeValue(0x100) != null) {
					String urlString = s.getConnectionURL(ServiceRecord.AUTHENTICATE_NOENCRYPT, false);
					String serviceName = s.getAttributeValue(0x100).toString().substring(s.getAttributeValue(0x100).toString().indexOf("=")+1, s.getAttributeValue(0x100).toString().length());
					if (serviceName.equalsIgnoreCase(SERVICE_NAME)) {
						if(!scannedUrls.contains(urlString)){
							scannedUrls.addElement(urlString);
						}
					}
				}
			}
	}


	public synchronized void inquiryCompleted() {
		Hashtable<String, String> storedConnectionsHelp = new Hashtable<String, String>();
		
		for(int i = 0; i<scannedUrls.size();i++){
			String urlsCurrent = scannedUrls.get(i);
			String sub = urlsCurrent.split(";")[0];
			String sub2 = sub.split(":")[1];
			boolean flag = false;
			//System.out.println(urlsCurrent+" ### "+sub);
			Enumeration set = storedConnections.keys();
			while(set.hasMoreElements()&&!flag)
			{
				String url = (String) set.nextElement();
				if(url.contains(sub2)&&!url.contains(sub)){
					flag = true;
				}
				System.out.println(flag);
			}
			if(!flag)storedConnectionsHelp.put(urlsCurrent, sub);
			if(!storedConnections.containsKey(urlsCurrent)&&!flag){//neues Handy! oder wieder in den Accesspoint eingetreten!
				if(mapConnectionController.newConnection(sub)){
					mapConnectionController.newMobilePhoneFoundFirstConnection(urlsCurrent);
					myDiscoverer.stopSearch();
				}
			}
			try {
				Thread.sleep(2000);

			} catch (Exception e) {
			}
		}
		System.out.println("Anzahl Ger�te mit verf�gbarem Dienst: "+storedConnectionsHelp.size());
		
		storedConnections = storedConnectionsHelp;
		mapConnectionController.controlConnections(storedConnections);
		
		scannedUrls.clear();
		
	}
	
	public synchronized void closeServer(){
		myDiscoverer.closeServer();
	}


}
