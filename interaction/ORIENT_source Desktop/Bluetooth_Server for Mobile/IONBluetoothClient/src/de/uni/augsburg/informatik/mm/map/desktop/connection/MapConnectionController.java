package de.uni.augsburg.informatik.mm.map.desktop.connection;

import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import de.uni.augsburg.informatik.mm.map.desktop.connection.client.MapReplyHandler;
import de.uni.augsburg.informatik.mm.map.desktop.connection.discovery.MapDiscovery;
import de.uni.augsburg.informatik.mm.map.desktop.connection.server.MapServerAP;
import de.uni.augsburg.informatik.mm.map.desktop.model.MobilePhone;
import de.uni.augsburg.informatik.mm.map.desktop.model.MediaFile;


public class MapConnectionController {
	private int maxConnections = 1000;
	private MapServerAP mapServerAP;
	private MapDiscovery mapDiscovery;
	private Vector<ConnectionListener> listener;
	private Date lastDate;
	
	public Hashtable<String,MobilePhone>clients = new Hashtable<String, MobilePhone>();
	
	public MapConnectionController() {
		try {
			initStack();//Zugriff auf den Bluetooth Stack!
		} catch (Exception e) {
			e.printStackTrace();
		}
		listener = new Vector<ConnectionListener>();
		this.mapServerAP = new MapServerAP(this);
		this.mapDiscovery = new MapDiscovery(this, new Hashtable<String, String>());

			}
	
	public int getMaxConnections() {
		return maxConnections;
	}
	
	
	public void requestFromMobilePhone(MediaFile file) {
		
		for(int i = 0;i<listener.size();i++){
			if(file.getType().equals(MediaFile.TEXT)&&new String(file.getData()).equals("disconnect")) 
				listener.elementAt(i).mobilePhoneDisconnected(file.getReplyAddress());
			else listener.elementAt(i).messageFromMobilePhone(file);
		}
	}

	private MapReplyHandler reply;
	public void sendToMobilePhone(MediaFile file) {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		reply = new MapReplyHandler(file, mapServerAP.getMyServerURL());
		reply.start();
	}

	public void newMobilePhoneFoundFirstConnection(String urlsCurrent) {
		for(int i = 0;i<listener.size();i++){
			listener.elementAt(i).newMobilePhoneFoundFirstConnection(urlsCurrent);
		}	}
	
	public void initStack() throws Exception {
		de.avetana.bluetooth.stack.BluetoothStack
				.init(new de.avetana.bluetooth.stack.AvetanaBTStack());
	}

	public void closeServer() {
		mapServerAP.closeServer();
	}

	
	
	public void addConnectionListener(ConnectionListener listener){
		this.listener.addElement(listener);
	}

//	public void disconnected(String url) {
//		for(int i = 0;i<listener.size();i++){
//			listener.elementAt(i).mobilePhoneDisconnected(url);
//		}
//		
//	}

	public void controlConnections(Hashtable<String, String> storedConnections) {
		Enumeration set = storedConnections.keys();
		while(set.hasMoreElements()){
			String urlPhone = (String)set.nextElement();
			String urlSub = storedConnections.get(urlPhone);
			if(clients.containsKey(urlSub)){
				MobilePhone client = clients.get(urlSub);
				Date date = client.getLastConnection();
				if(lastDate!=null&&date!=null){
					if(lastDate.getTime()>date.getTime()){
						date = lastDate;
					}
				}
				lastDate = date;
				Date dateCurrent = new Date();
				if(date!=null)System.out.println(((dateCurrent.getTime()-date.getTime())/1000)+"  differEnce!!!!!!!!!!");
				if(date!=null&&((dateCurrent.getTime()-date.getTime())/1000)>20){
					System.out.println("Letztes inquiryCompletede " + date.getTime());
					System.out.println("current date " + dateCurrent.getTime());
					clients.remove(urlSub);
					for(int j = 0;j<listener.size();j++){
						listener.elementAt(j).mobilePhoneDisconnected(urlSub);
					}
				}
			}else{
				MobilePhone client = new MobilePhone(urlPhone);
				clients.put(urlSub, client);
			}
		}
		Enumeration set2 = clients.keys();
		while(set2.hasMoreElements()){
			String urlPhone = (String)set2.nextElement();
			if(!storedConnections.contains(urlPhone)){
				MobilePhone client = clients.get(urlPhone);
				Date date = client.getLastConnection();
				if(lastDate!=null&&date!=null){
					if(lastDate.getTime()>date.getTime()){
						date = lastDate;
					}
				}
				lastDate = date;
				Date dateCurrent = new Date();
				if(date!=null&&date!=null&&((dateCurrent.getTime()-date.getTime())/1000)>5){
					clients.remove(urlPhone);
					for(int j = 0;j<listener.size();j++){
						listener.elementAt(j).mobilePhoneDisconnected(urlPhone);
					}
				}
			}
		}
	}
	
	public boolean newConnection(String url){
		String urlsub = url.split(";")[0];
		MobilePhone client = clients.get(urlsub);
		if(client==null)return true;
		else{
			Date date = client.getLastConnection();
			Date dateCurrent = new Date();
			return !(date!=null&&date!=null&&((dateCurrent.getTime()-date.getTime())/1000)<5);
		}
	}
	
	public void setLastConnection(String url){
		
		String urlsub = url.split(";")[0];
		Date date = new Date();
		System.out.println(url+"  setLastConnection"+date.getTime());
		MobilePhone client = clients.get(urlsub);
		if(client==null){
			client = new MobilePhone(url);
			clients.put(urlsub, client);
		}
		client.setLastConnection(date);
	}

	public void run() {
		
		
	}

	
	
}
