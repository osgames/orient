package de.uni.augsburg.informatik.mm.map.desktop.connection.server;

import java.io.IOException;
import java.util.Hashtable;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.ServiceRecord;
import javax.microedition.io.Connection;
import javax.microedition.io.Connector;
import javax.obex.ServerRequestHandler;
import javax.obex.SessionNotifier;

import de.avetana.bluetooth.sdp.LocalServiceRecord;
import de.uni.augsburg.informatik.mm.map.desktop.connection.MapConnectionController;
import de.uni.augsburg.informatik.mm.map.desktop.model.MediaFile;

// Verbindungen aufnehmen
public class MapServerAP extends Thread {

	private final String SERVICE_NAME = "MapServiceAPMobile";

	private final String UUID_STRING = "11112233445566778899AABBCCDDEEFF";

	private final String CONNECTION_STRING;

	private MapConnectionController controller;

	private SessionNotifier sessionNotifier;

	private boolean stillNeeded = true;

	private String myServerURL;

	private Hashtable<ServerRequestHandler, Connection> clientConnections;

	public MapServerAP(MapConnectionController controller) {
		this.controller = controller;
		this.CONNECTION_STRING = "btgoep://localhost:" + UUID_STRING
				+ ";master=true;authenticate=false;"
				+ "encrypt=false;authorize=false;name=" + SERVICE_NAME;
		this.clientConnections = new Hashtable<ServerRequestHandler, Connection>();
		this.start();
	}

	public void run() {
		try {
			LocalDevice local = LocalDevice.getLocalDevice();
			sessionNotifier = (SessionNotifier) Connector
					.open(CONNECTION_STRING);
			LocalServiceRecord record = (LocalServiceRecord) local
					.getRecord(sessionNotifier);
			myServerURL = record.getConnectionURL(
					ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
			System.out.println("Die Service-URL von dem registrierten Dienst lautet: " + myServerURL);
		} catch (BluetoothStateException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		} catch (NullPointerException e) {
			System.out.println("EX11111");
		}
		while (stillNeeded) {
			if (clientConnections.size() < controller.getMaxConnections()) {
				MapRequestHandler handler = new MapRequestHandler(this);
				new Thread(handler).start();
				Connection con = null;
				try {
					con = sessionNotifier.acceptAndOpen(handler);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Ein neuer Client hat sich verbunden!");
				clientConnections.put(handler, con);
			}
			try {
				sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			sessionNotifier.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void requestFromMobilePhone(MediaFile file, MapRequestHandler handler) {
		controller.requestFromMobilePhone(file);
	}

	public String getMyServerURL() {
		return myServerURL;
	}

	public void closeServer() {
		stillNeeded = false;
	}

//	public void disconnect(MapRequestHandler handler) {
//		clientConnections.remove(handler);
//		controller.disconnected(handler.getURL());
//	}

	public void setLastConnection(String url) {
		controller.setLastConnection(url);
	}

}
