package de.uni.augsburg.informatik.mm.map.desktop.connection.server;

import java.io.IOException;
import java.io.InputStream;

import javax.obex.HeaderSet;
import javax.obex.Operation;
import javax.obex.ResponseCodes;
import javax.obex.ServerRequestHandler;

import de.uni.augsburg.informatik.mm.map.desktop.model.MediaFile;



public class MapRequestHandler extends ServerRequestHandler implements Runnable{

	private int MAXLENGTH = 1024 * 256;
	private MapServerAP server;
	private String replyURL;
	
	public MapRequestHandler(MapServerAP server){
		this.server = server;
	}
	
	/**
	 * wird vom Mobiltelefon aufgerufen z.B. um Audio oder Text zu übertragen
	 */
	public int onPut(Operation op) {
		try {
			HeaderSet hs = op.getReceivedHeaders();
			int length = (int) ((Long)hs.getHeader(HeaderSet.LENGTH)).longValue();
			String contentType = (String)hs.getHeader(HeaderSet.TYPE);
			String name = (String)hs.getHeader(HeaderSet.NAME);
			replyURL = (String)hs.getHeader(HeaderSet.DESCRIPTION);
			server.setLastConnection(replyURL);
			byte[] data = new byte[length];
			int position = 0;
			InputStream inputStream = op.openInputStream();
			while (position < length) {
				if (inputStream == null) {
					throw new IOException();
				}
				if (position < (length - MAXLENGTH)) {
					length = inputStream.read(data, position, MAXLENGTH);
				} else {
					length = inputStream.read(data, position, length
							- position);
				}
				if (length < 0) {
					throw new IOException();
				}
				position += length;
			}
			//inputStream.close();
			op.close();
			
			server.requestFromMobilePhone(new MediaFile(data, name,contentType, replyURL), this);
		} catch (IOException e) {
			
		}
		
		return ResponseCodes.OBEX_HTTP_OK;

	}
	
	public int onGet(Operation op) {
		HeaderSet hs;
		try {
			hs = op.getReceivedHeaders();
			String name = (String)hs.getHeader(HeaderSet.NAME);
			replyURL = (String)hs.getHeader(HeaderSet.DESCRIPTION);
			server.setLastConnection(replyURL);
			System.out.println(name);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseCodes.OBEX_HTTP_OK;
	}
	
	public void onDisconnect(HeaderSet request, HeaderSet reply) {
		super.onDisconnect(request, reply);
	}
	
	public int onConnect(HeaderSet request, HeaderSet reply) {
		return super.onConnect(request, reply);
	}

	public void run() {
		// TODO Auto-generated method stub
		
	}

	public String getURL() {
		return replyURL;
	}
}
