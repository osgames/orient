package de.uni.augsburg.informatik.mm.map.desktop.connection.client;

import java.io.IOException;
import java.io.OutputStream;

import javax.microedition.io.Connection;
import javax.microedition.io.StreamConnection;
import javax.obex.HeaderSet;
import javax.obex.Operation;

import de.avetana.bluetooth.connection.Connector;
import de.avetana.bluetooth.obex.OBEXConnection;
import de.uni.augsburg.informatik.mm.map.desktop.model.MediaFile;

public class MapReplyHandler extends Thread {// implements Runnable{

	private String myAPURL;

	private int MAXLENGTH = 1024 * 256;

	private OBEXConnection con = null;

	private MediaFile media;

	private int counter = 0;

	public MapReplyHandler(MediaFile file, String myServerURL) {
		this.myAPURL = myServerURL;
		this.media = file;
	}

	public void run() {

		connect();

	}

	public void connect() {

		System.out.println("try to connect for "+media.getReplyAddress());
		Connection connection = null;
		try {
			connection = Connector.open(media.getReplyAddress());
			System.out.println("connection"+connection);
			if (connection != null && connection instanceof StreamConnection) {
				StreamConnection constream = (StreamConnection) connection;
				con = new OBEXConnection(constream);
				System.out.println("send connect");
				con.connect(null);
				System.out.println("send data");
				if (media != null)
					send(media);
			}
		} catch (IOException io) {
			System.out.println("IOException can not connect!"+io.getMessage());
			if (con != null)
				try {
					con.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
				
			try {
				sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("try it again!");
			if (counter < 10)
				connect();
			counter++;
		}
	}

	public void send(MediaFile file) throws IOException {
		System.out.println("message to send");
		HeaderSet headerPut = con.createHeaderSet();
		headerPut.setHeader(HeaderSet.NAME, file.getDescription());
		headerPut.setHeader(HeaderSet.LENGTH, new Long(file.getData().length));
		headerPut.setHeader(HeaderSet.DESCRIPTION, myAPURL);
		headerPut.setHeader(HeaderSet.TYPE, file.getType());
		Operation operation = con.put(headerPut);
		OutputStream output = operation.openOutputStream();
		int length = file.getData().length;
		int position = 0;
		int sendLength = 0;
		while (position != length) {
			// OutputStream outputStream = this.outputStream;
			sendLength = length - position > MAXLENGTH ? MAXLENGTH
					: (int) length - position;

			if (output == null) {
				throw new IOException();
			}
			output.write(file.getData(), position, sendLength);
			position += sendLength;
		}
		output.close();
		operation.close();
		System.out.println("message has been sent");
		try{
			con.disconnect(null);
			
		}catch(IOException ee){
			try{
				con.disconnect(null); System.out.println("exception...");
			}catch(IOException e2){
				System.out.println("exception...");
			}
		}
		con.close();
		System.out.println("connection has been closed");

	}

	

}
