package de.uni.augsburg.informatik.mm.map.desktop.model;

import java.util.Date;

public class MobilePhone {
	
	private String url;
	private Date lastConnection;
	private String function = "";

	public MobilePhone(String url) {
		this.url = url;
	}

	public void setLastConnection(Date date) {
		this.lastConnection = date;
	}

	public Date getLastConnection() {
		if(lastConnection!=null)System.out.println("getLast"+lastConnection.getTime());
		return lastConnection;
	}
	
	public void setFunction(String f){
		this.function = f;
	}
	
	public String getFunction(){
		return function;
	}

}
