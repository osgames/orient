/*
 * Created on 28.10.2004
 *
 * if the program is called without any parameters, it does an inquiry. 
 * If it is calles with 1 parameter it does a service serach on the device specified (e.g. 000d9305170e). 
 * If it is called with 2 parameters, the second parameter is considered a UUID on which the service search is supposed to be restricted on.
 */
package de.uni.augsburg.informatik.mm.map.desktop.connection.discovery;

import java.io.IOException;
import java.util.Vector;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;



/**
 * @author gmelin
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class Discovery implements DiscoveryListener {

	private static final boolean doException = false;

	private boolean searchCompleted = false;

	private Vector<RemoteDevice> devices;

	private MapDiscovery mapDiscovery;

	private Vector<ServiceRecord> tempServices;

	private boolean running = true;

	private DiscoveryAgent da;

	private int transID;

	private Thread discoverThread;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.bluetooth.DiscoveryListener#deviceDiscovered(javax.bluetooth.RemoteDevice,
	 *      javax.bluetooth.DeviceClass)
	 */
	public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
		devices.add(btDevice);
		try {
			System.out.println(
					"BT device discovered " + btDevice + " name "
							+ btDevice.getFriendlyName(true) + " majc 0x"
							+ Integer.toHexString(cod.getMajorDeviceClass())
							+ " minc 0x"
							+ Integer.toHexString(cod.getMinorDeviceClass())
							+ " sc 0x"
							+ Integer.toHexString(cod.getServiceClasses()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (doException) {
			byte[] b = new byte[0];
			b[1] = 0;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.bluetooth.DiscoveryListener#servicesDiscovered(int,
	 *      javax.bluetooth.ServiceRecord[])
	 */
	public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
		// TODO Auto-generated method stub
		// System.out.println (servRecord.length + " services discovered " +
		// transID);

		if (doException) {
			byte[] b = new byte[0];
			b[1] = 0;
		}

		for (int i = 0; i < servRecord.length; i++) {
			try {

				tempServices.add(servRecord[i]);
				// System.out.println ("Record " + (i + 1));
				// System.out.println ("" +
				// servRecord[i].getAttributeValue(0x100));
			} catch (Exception e) {
				e.printStackTrace();
			}
			/*
			 * if (servRecord[0] instanceof RemoteServiceRecord) {
			 * RemoteServiceRecord rsr = (RemoteServiceRecord)servRecord[0]; if
			 * (rsr.raw.length == 0) return; for (int j = 0; j <
			 * rsr.raw.length;j++) { System.out.print (" " +
			 * Integer.toHexString(rsr.raw[j] & 0xff)); }
			 * System.out.println("\n----------------");
			 * 
			 * ServiceRecord rsr2; try { rsr2 =
			 * RemoteServiceRecord.createServiceRecord("000000000000", new
			 * byte[0][0], new int[] { 0,1,2,3,4,5,6,7,8,9,10, 256 }, rsr.raw);
			 * byte[] raw2 = MSServiceRecord.getByteArray (rsr2); for (int j =
			 * 0; j < raw2.length;j++) { System.out.print (" " +
			 * Integer.toHexString(raw2[j] & 0xff)); } System.out.println();
			 *  } catch (IOException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); }
			 *  }
			 */
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.bluetooth.DiscoveryListener#serviceSearchCompleted(int, int)
	 */
	public void serviceSearchCompleted(int transID, int respCode) {
		// TODO Auto-generated method stub
		mapDiscovery.analyzeServices(tempServices);
		tempServices.clear();

		// System.out.println ("Service search completed " + transID + " / " +
		// respCode);
		searchCompleted = true;
		synchronized (discoverThread) {
			discoverThread.notify();
		}
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.bluetooth.DiscoveryListener#inquiryCompleted(int)
	 */
	public void inquiryCompleted(int discType) {
		// System.out.println ("Inquiry completed " + discType);

		searchCompleted = true;
		synchronized (discoverThread) {
			discoverThread.notify();
		}
		/*
		 * try {
		 * LocalDevice.getLocalDevice().getDiscoveryAgent().startInquiry(DiscoveryAgent.GIAC,
		 * InqTest.this); } catch (Exception e) { e.printStackTrace(); }
		 */

	}

	public Discovery(final String addr, final MapDiscovery mapDiscovery) {
		this.mapDiscovery = mapDiscovery;
		tempServices = new Vector<ServiceRecord>();

		// System.out.println("Discoverer started");

		discoverThread = new Thread() {
			public void run() {
				while (running) {
					try {
						devices = new Vector<RemoteDevice>();
						da = LocalDevice.getLocalDevice().getDiscoveryAgent();
						searchCompleted = false;
						devices.clear();
						if (addr == null || addr.equals("inq")) {
							//System.out.println("DiscoveryAgent starting inquiry");
							da
									.startInquiry(DiscoveryAgent.GIAC,
											Discovery.this);
							synchronized (this) {
								wait(30000);
							}
							
							if (!searchCompleted) {
								System.out.println("Cancelling inquiry");
								da.cancelInquiry(Discovery.this);
							}
							if (("" + addr).equals("inq"))
								continue;
						} else
							devices.add(new RemoteDevice(addr));
						for (int i = 0; i < devices.size(); i++) {
							RemoteDevice rc = (RemoteDevice) devices.get(i);
							searchCompleted = false;
							//System.out.println("DiscoveryAgent starting service search");
							transID = da.searchServices(new int[] { 0x05, 0x06,
									0x07, 0x08, 0x09, 0x100, 0x303 },
									new UUID[0], rc, Discovery.this);
							
							synchronized (this) {
								wait(30000);
							}
							if (!searchCompleted) {
								System.out.println("Cancelling service search");
								da.cancelServiceSearch(transID);
							}
						}
					

						mapDiscovery.inquiryCompleted();
					} catch (BluetoothStateException ex) {
						ex.printStackTrace();
						System.err.println("Discovery thread terminated!");
					} catch (InterruptedException ex) {
						System.out.println("Discovery thread interrupted");
					}
				}
			}
		};
		
		discoverThread.start();

	}

	public void closeServer() {
		running = false;
		discoverThread.interrupt();
		try {
			discoverThread.join();
		} catch (InterruptedException e) {
		}
	}

	public void stopSearch() {
		running = false;
	}

}
