package de.uni.augsburg.informatik.mm.map.desktop.connection;

import de.uni.augsburg.informatik.mm.map.desktop.model.MediaFile;

public interface ConnectionListener {
	public void messageFromMobilePhone(MediaFile file);

	public void newMobilePhoneFoundFirstConnection(String urlsCurrent);

	public void mobilePhoneDisconnected(String replyAddress);
}
