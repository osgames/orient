package bluetooth;

public class ConversionException extends Exception {
	
	public ConversionException(String msg) {
		System.err.println("Conversion exception: "+msg);
	}

}
