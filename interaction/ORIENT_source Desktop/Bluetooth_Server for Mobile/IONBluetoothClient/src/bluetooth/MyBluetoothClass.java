package bluetooth;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.Random;

import main.Client;

import de.uni.augsburg.informatik.mm.map.desktop.connection.ConnectionListener;
import de.uni.augsburg.informatik.mm.map.desktop.connection.MapConnectionController;
import de.uni.augsburg.informatik.mm.map.desktop.model.MediaFile;
import de.uni.augsburg.informatik.mm.map.desktop.model.MobilePhone;


public class MyBluetoothClass implements ConnectionListener{

	private MapConnectionController mapConnectionController;
	private String replyURL;
	private String name;
	private Client client;


	public MyBluetoothClass(String host, int port) {
		mapConnectionController = new MapConnectionController();
		mapConnectionController.addConnectionListener(this);
		client = new Client(host, port, this);
	}
	
	public void senddebugMessage(String xmlString) {
		MediaFile file = new MediaFile(xmlString.getBytes(),
				"Bla_Desc", MediaFile.TEXT, replyURL);
		mapConnectionController.sendToMobilePhone(file);
		
		//Ausgabe zum �berwachen der Sende-Aktivit�t
		Date currentDate = new Date();
		System.out
				.println("---------------------------------------------------------");
		System.out
				.println("Daten sollen an das Handy mit folgender Service-URL geschickt werden: "
						+ file.getReplyAddress());
		System.out.println("Folgende Nachricht wurden ans Handy geschickt: "
				+ new String(file.getData()));
		System.out.println("Aktuelles Datum: " + currentDate.getTime());
		System.out
				.println("---------------------------------------------------------");

	}

	// newMobilePhoneFoundFirstConnection(String mobilePhoneUrl)
	// wird aufgerufen, wenn ein neues Handy durch das Scannen gefunden wird
	public void newMobilePhoneFoundFirstConnection(String mobilePhoneUrl) {
		MediaFile file = new MediaFile(MediaFile.NEWACCESSPOINT.getBytes(),
				"Bla_Desc", MediaFile.TEXT, mobilePhoneUrl);
		mapConnectionController.sendToMobilePhone(file);
		
		//Ausgabe zum �berwachen der Sende-Aktivit�t
		System.out
				.println("---------------------------------------------------------");
		System.out
				.println("Neues Mobiltelefon wurde gefunden: "
						+ file.getReplyAddress());
		System.out.println("Folgende Nachricht wurden ans Handy geschickt: "
				+ new String(file.getData()));
		System.out
				.println("---------------------------------------------------------");

	}

	// 3. KLasse muss die Methode public void messageFromMobilePhone(MediaFile
	// file); implementieren
	// wird automatisch aufgerufen, wenn eine Nachricht f�r den DesktopPC
	// eingegangen ist
	// Eingegangene Nachricht enth�lt ein Medienfile
	public void messageFromMobilePhone(MediaFile file) {
		replyURL = file.getReplyAddress();
		
		//Text
		String message = new String(file.getData());
		
		//Audio
		String amrname="";
		byte[] audioData = file.getData();
		

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(file.getType().equals(MediaFile.TEXT)){
//			Ausgabe zum �berwachen der Sende-Aktivit�t
			System.out
					.println("---------------------------------------------------------");
			System.out
					.println("Daten vom Handy mit folgender Service-URL wurden erhalten: "
							+ replyURL);
			System.err.println("Folgende Nachricht ist eingegangen: "
					+ message);
			
			System.out
					.println("---------------------------------------------------------");
			String[] param = message.split("�");
			
			if(!message.contains("connect")){
				if(param[2].equals("SetNameForAudio")){
					name = param[1];
				}
				else{
				//XML mit Wort erzeugen
				String toSend = "<input>" +
									"<device>" + param[0] + "</device>" + //mobile phone, wii or dance mat
									"<address>" + replyURL + "</address>" + //Address of the device e.g. bluetooth address
									"<interaction>" +
										"<type>" + param[1] + "</type>" + //keyboard, speech, emotion, rfid or gesture and movement
										"<value>" + param[2] + "</value>" + // there are 1 or values possible (e.g. for wii-gesture and speech recognition. Thus we can send an n-best list with the probability
									"</interaction>" +
								"</input>";
	
				//XML an ION schicken
				System.out.println(toSend);
				client.queueMessageForSending(toSend);
				}
			}
		}
		else if(file.getType().equals(MediaFile.AUDIO)){
			
		{//		Ausgabe zum �berwachen der Sende-Aktivit�t
			System.out
			.println("---------------------------------------------------------");
			System.err
					.println("Audio vom Handy mit folgender Service-URL wurden erhalten: "
							+ replyURL);
			//Verarbeiten des byte-Array
			try{
				Date d = new Date();
				amrname = "test"+d.getTime()+".amr";
				FileOutputStream amrFile = new FileOutputStream("ext_bin\\"+amrname);
				for(int i = 0; i < audioData.length; i++)
				{
					amrFile.write(audioData[i]);
				}
				amrFile.close();
			}
			catch(IOException ie)
			{
				ie.printStackTrace();
			}
			}
			//Erfolgsantwort
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			//senddebugMessage("Audio-Datei mit der Groesse "+ audioData.length+" erfolgreich von " + replyURL + " erhalten.");
			String pcmname=null;
			try {
				pcmname=amr2pcm(amrname);
			} catch (ConversionException c) {
				return;
			}

			String recText = recogniseWords(pcmname);
			String emotion = recogniseEmotion(pcmname);
			System.err.println("Recognized Text: "+recText+"\n Recognized Emotion: "+emotion);
			//senddebugMessage("Erkannter Text: "+recText+", erkannte Emotion: "+emotion);
			name = "Audio";
			generateAndSendXML(recText);
			senddebugMessage(recText);
			File f = new File (System.getProperty("user.dir")+"\\ext_bin\\"+pcmname);
			f.delete();
		}
		

	}

	// Wird aufgerufen nachdem das Handy eine Nachricht geschickt hat und sich
	// dann erfolgreich vom System wieder abgemeldet hat.
	public void mobilePhoneDisconnected(String mobilePhoneUrl) {
		///senddebugMessage(mobilePhoneUrl);
		System.out.println("disconnected"+mobilePhoneUrl);
	}

	//Spracherkennung
	private String[] processFile(String[] cmd, File workingDir, String pcmname) throws IOException, InterruptedException {
    	Process child = Runtime.getRuntime().exec(cmd, null, workingDir);
    	
    	if (pcmname != null) {
    		OutputStreamWriter osr = new OutputStreamWriter(child.getOutputStream());
    		osr.write(pcmname);
    		osr.close();
    	}
    	
        InputStream isErr = child.getErrorStream();
        InputStreamReader isrErr = new InputStreamReader(isErr);
        BufferedReader brErr = new BufferedReader(isrErr);
        
        String line;
        String result[] = new String[2];
        result[0] = "";
        while ((line = brErr.readLine()) != null) {
        	result[0]+=line;
        }
        
        InputStream isOut = child.getInputStream();
        InputStreamReader isrOut = new InputStreamReader(isOut);
        BufferedReader brOut = new BufferedReader(isrOut);
        
        result[1]="";
        while ((line = brOut.readLine()) != null) {
        	result[1]+=line;
        }
        
        child.waitFor();
        return result;
    }
    
	String amr2pcm(String amrname) throws ConversionException {
		//amr2wav
		String wavname = amrname.replace(".amr", ".wav");
		String wd=System.getProperty("user.dir")+"\\ext_bin\\";
		String cmd[] = {wd+"ffmpeg","-y","-i",amrname,"-ac","1","-ar","16000",wavname};
		File workingDir = new File(wd);
		if (!workingDir.exists())
			System.err.println("ext_bin does not exist");
		String result[]=null;
		try {
			result=processFile(cmd,workingDir,null);
		} catch (IOException e) {
			if (result !=null)
				throw new ConversionException(result[0]+"\n"+result[1]+"Probably files were not there");
			throw new ConversionException("Probably files were not there");
		} catch (InterruptedException e) {
			if (result !=null)
				throw new ConversionException(result[0]+"\n"+result[1]+"Problem");
			throw new ConversionException("Problem!");
		} 
		File f;
		f=new File(wd+amrname);
		f.delete();
		f = new File(wd+wavname);
		if (!f.exists())
			throw new ConversionException("amr-to-wav conversion was not succesful!");
		
		//wav2pcm
		String pcmname = wavname.replace(".wav", ".pcm");
		String cyg_wd = wd;
		cyg_wd.replace("\\","/");
		cyg_wd.replace("([A-Za-z]):\\","/cygdrive/$1/");
		String cmd1[] = {cyg_wd+"emo_awav2pcm",wavname,pcmname};
		try {
			result = processFile(cmd1,workingDir,null);
		} catch (IOException e) {
			if (result !=null)
				throw new ConversionException(result[0]+"\n"+result[1]+"Probably files were not there");
			throw new ConversionException("Probably files were not there");
		} catch (InterruptedException e) {
			if (result !=null)
				throw new ConversionException(result[0]+"\n"+result[1]+"Problem");
			throw new ConversionException("Problem!");
		} 
		// delete wave file
		f.delete(); 
		f = new File(wd+pcmname);
		if (!f.exists())
			throw new ConversionException("wav-to-pcm conversion was not succesful!");
		return pcmname;
	}
	
	String recogniseWords(String pcmname) {
		String wd=System.getProperty("user.dir")+"\\ext_bin\\";
		File workingDir = new File(wd);
		String result[] = null;
		try {
			String cmd[] = {wd+"isr", "-b50","-w15","masr.swu","masr.wl","masr.tree","-","masr.cl","-","mfcc","1.4"};
			result=processFile(cmd,workingDir,pcmname);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (result != null) {
			System.err.println("err: "+result[0]);
			return result[1].replace(" ;", "");
		}
		return "problem";
	}
			
	String recogniseEmotion(String pcmname) {
		String wd=System.getProperty("user.dir")+"\\ext_bin\\";
		File workingDir = new File(wd);
		String result[] = null;
		File f = new File (wd+pcmname);
		long n_samples = f.length()/2-1;
		try {
			String cmd[] = {wd+"emo_classify_file",pcmname,"[0.."+n_samples+"]","emo.cl"};
			result=processFile(cmd,workingDir,null);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (result != null) {
			System.err.println("err: "+result[0]);
			return result[1];
		}
		return "problem";
	}
	
	private void generateAndSendXML(String value){
		//XML mit Wort erzeugen
		String toSend = "<input>" +
							"<device>MobilePhone</device>" + //mobile phone, wii or dance mat
							"<address>" + replyURL + "</address>" + //Address of the device e.g. bluetooth address
							"<interaction>" +
								"<type>" + name + "</type>" + //keyboard, speech, emotion, rfid or gesture and movement
								"<value>" + value + "</value>" + // there are 1 or values possible (e.g. for wii-gesture and speech recognition. Thus we can send an n-best list with the probability
							"</interaction>" +
						"</input>";

		//XML an ION schicken
		System.out.println(toSend);
		client.queueMessageForSending(toSend);
	}
}
