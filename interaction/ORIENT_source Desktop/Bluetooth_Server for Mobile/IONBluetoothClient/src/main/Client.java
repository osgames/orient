package main;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Vector;

import bluetooth.MyBluetoothClass;




public class Client implements Runnable{

    private PrintStream out = null;
    public InputStreamReader in = null;
	//private InputStream in = null;
    private MyBluetoothClass btClass;
	private boolean aborting = false;

	private final Vector writeQueue = new Vector();

	private String url;
	private int port;
	
	public Client(String url, int port, MyBluetoothClass btClass){
		this.port = port;
		this.url = url;
		this.btClass = btClass;
		new Thread(this).start();
	}

	public void run() {
		Socket server;
		try {
			server = new Socket ( url, port );
			in = new InputStreamReader(server.getInputStream());
		    out =  new PrintStream(server.getOutputStream());
			startWriteQueue();
			while (!aborting) {
				try {
					String message = "";
					int c;

					while ((c = in.read()) != -1) {
						if ((char) c == '\n')
						{
							break;
						}
						else
						{
							message = message.concat(String.valueOf((char) c));
						}
					}
					if(!message.equals(""))
					{
						System.out.println("-------------------------------------------------------------------");
						System.out.println("ANTWORT: " + message);
						//Weitersenden an Handy
						//btClass.senddebugMessage(message);
					}
					
					
				} catch (IOException ex) {
					close();
						System.out.println(ex.getMessage());
				}
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	}
	
	
	
	/**
	 * For closing the TCP connection.
	 * 
	 */
	public void close() {
		if (!aborting) {
			try {
				sendMessage("close");
			} catch (IOException e1) {
				System.out.println(e1.getMessage());
			}
			synchronized (this) {
				aborting = true;
			}

			synchronized (writeQueue) {
				writeQueue.notify();
			}

			if (out != null) {
				try {
					out.close();
					synchronized (this) {
						out = null;
					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}

			if (in != null) {
				try {
					in.close();
					synchronized (this) {
						in = null;
					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}

		}
	}

	/**
	 * For writing a message to the server. The message is attached to a queue
	 * 
	 * @param data
	 */
	public void queueMessageForSending(String data) {
		synchronized (writeQueue) {
			writeQueue.addElement(data);
			writeQueue.notify();
		}
	}

	/**
	 * Is solled to write a message to the server
	 * 
	 * @param data
	 * @throws IOException
	 */
	private void sendMessage(String data) throws IOException {
		out.println(data);
		out.flush();
		System.out.println("Sent message:"+data);
	}

	private void startWriteQueue() {
		Runnable writer = new Runnable() {
			public void run() {
				while (!aborting) {
					if (!writeQueue.isEmpty()) {
						String sendData = (String) writeQueue.elementAt(0);
						try {
							sendMessage(sendData);
							writeQueue.removeElement(sendData);

						} catch (IOException e) {
							close();

							System.out.println(e.getMessage());
						}
					}

					synchronized (writeQueue) {
						if (writeQueue.size() == 0) {
							try {
								writeQueue.wait();
							} catch (InterruptedException e) {
								System.out.println(e.getMessage());
							}
						}
					}
				}
			}
		};

		Thread writeThread = new Thread(writer);
		writeThread.start();
	}

	

}
