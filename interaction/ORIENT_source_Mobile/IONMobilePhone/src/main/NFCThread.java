package main;

public class NFCThread extends Thread {
	
    private MainScreen nfc;
    public volatile boolean stop;

    public NFCThread(MainScreen nfc) {
        this.nfc=nfc;
        stop=false;
        this.start();
    }

    public void run() {
       
    	while(!stop) {
    		if(nfc.x<nfc.getWidth()/2){
				nfc.x+=10;
				nfc.y-=6;
			}else{
				nfc.resetXY();
			}
			nfc.repaint();
			try{
				Thread.sleep(250);
			} catch(InterruptedException e) {}
		}
    }
    
   

}