package main;
import java.io.IOException;

import javax.microedition.contactless.DiscoveryManager;
import javax.microedition.contactless.TargetType;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.midlet.MIDletStateChangeException;



public class MainScreen extends Canvas implements CommandListener {

	public int x;
	public int y; 
	public static final String RFID = "RFID";
	//public static final String AUDIOREC = "Audio";
	public static final String START = "Press Start";
	public static final String SEND = "Speak Name + Press Send";
	public NFCThread thread;
	
	private String speechString = "Press Start";

	private IONMidlet _midlet;

	protected Command exitCommand = new Command("Exit", Command.EXIT, 1);

	protected Command sendCommand = new Command("Send", Command.SCREEN, 1);

	protected Command startCommand = new Command("Start", Command.SCREEN, 1);

	private Image handy;
	private Image logo;
	private Image icon_mouth;

	private RFID rfid;

	private AudioRecorder audio;

	public MainScreen(IONMidlet midlet) {
		resetXY();
		this._midlet = midlet;
		try {
			handy = Image.createImage("/Handy.gif");
			logo = Image.createImage("/Logo.png");
			icon_mouth = Image.createImage("/icon_mouth.gif");
		} catch (IOException e) {

		}
		this.addCommand(exitCommand);
		this.addCommand(startCommand);
		this.setCommandListener(this);
		thread = new NFCThread(this);
		rfid = new RFID(this);
		audio = new AudioRecorder(this);

	}

	
	protected void paint(Graphics g) {
		
			
			g.setFont(Font.getFont(Font.FACE_MONOSPACE, 
					Font.STYLE_BOLD, Font.SIZE_MEDIUM ) );
			this.setTitle("ORIENT");
			g.setColor(0xEEEEEE);
			g.fillRect(0,0,getWidth(),getHeight());
			g.setColor(0xcccc00);
			g.drawImage(logo, this.getWidth()/2, 30,Graphics.HCENTER|Graphics.VCENTER);
			g.drawImage(handy, x, y,Graphics.HCENTER|Graphics.VCENTER);
			g.fillRect(-5,getHeight()/2-5, getWidth()+10,10);
			g.drawImage(icon_mouth, this.getWidth()/2, getHeight()/2+getHeight()/4,Graphics.HCENTER|Graphics.VCENTER);
			g.drawImage(handy, this.getWidth()/2+20, getHeight()/2+getHeight()/4-10,Graphics.HCENTER|Graphics.VCENTER);
			g.setColor(0x000000);
			g.drawString("+", this.getWidth()/2, getHeight()/2+getHeight()/4+35,Graphics.HCENTER|Graphics.BOTTOM );
			g.drawString(speechString, this.getWidth()/2, getHeight()/2+getHeight()/4+50,Graphics.HCENTER|Graphics.BOTTOM );
			
	}
	
	
	public void resetXY(){
		x = this.getWidth()/3;
		y = this.getHeight()/3;
	}

	public void sendMessage(String temp, String type) {
		_midlet.sendMessage(temp, type);
	}

	public void makeSuccessScreenActive(String temp) {
		_midlet.makeSuccessScreenActive(temp);
	}

	public void commandAction(Command comm, Displayable arg1) {
		if (comm == exitCommand) {
			_midlet.sendMessage("exit", "choice");
			if (audio != null) {
				audio.close();
			}
			if (rfid != null) {
				rfid.exit();
			}
			try {
				_midlet.destroyApp(false);
			} catch (MIDletStateChangeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (comm == startCommand) {
			_midlet.sendMessage("start", "choice");
			
			audio.recordAudio();
			speechString = SEND;
			repaint();
		} else if (comm == sendCommand) {
			
			audio.stopRecord();
			speechString = START;
			repaint();
		}
	}

	public IONMidlet getMidlet() {
		return _midlet;
	}

	public void removeCommand(String c) {
		if (c.equalsIgnoreCase(AudioRecorder.START))
			this.removeCommand(startCommand);
		else {
			if (c.equalsIgnoreCase(AudioRecorder.SEND))
				this.removeCommand(sendCommand);
		}
	}

	public void addCommand(String c) {
		if (c.equalsIgnoreCase(AudioRecorder.START))
			this.addCommand(startCommand);
		else {
			if (c.equalsIgnoreCase(AudioRecorder.SEND))
				this.addCommand(sendCommand);
		}
	}

	public void sendAudioFile(byte[] soundData) {
	    _midlet.makeWaitingScreenActive();
		_midlet.sendAudioFile(soundData);
	}

}
