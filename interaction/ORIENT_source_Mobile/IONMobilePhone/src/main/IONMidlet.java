package main;
import java.io.ByteArrayOutputStream;
import java.util.Date;

import javax.microedition.media.Player;
import javax.microedition.media.control.RecordControl;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Choice;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.List;

import de.uni.augsburg.informatik.mm.map.mobile.connection.client.ConnectionListener;
import de.uni.augsburg.informatik.mm.map.mobile.connection.client.MapConnectionController;
import de.uni.augsburg.informatik.mm.map.mobile.connection.client.MapConnectionControllerImpl;
import de.uni.augsburg.informatik.mm.map.mobile.model.MediaFile;

public class IONMidlet extends MIDlet implements ConnectionListener{
	
	public String name = "mobile";
	public boolean connected = false;

	private String pcURL;
	private MapConnectionController mapConnectionController;
	private MainScreen mainScreen;

	//Close application
	public void destroyApp(boolean arg0) throws MIDletStateChangeException {
		setkeepConnected(false);
		mapConnectionController.unregisterServer();
		notifyDestroyed();
	}

	public void pauseApp() {
	}

	//Start application
	public void startApp() throws MIDletStateChangeException {
		String service = this.getAppProperty("Service");
		mapConnectionController = new MapConnectionControllerImpl(service);
		mapConnectionController.addConnectionListener(this);
		mainScreen = new MainScreen(this);
		Form ws = new WaitingScreen(this, "Please wait... We are connecting to ORIENT!", true);
		Display.getDisplay(this).setCurrent(ws);
	}

//SCREENS
	//Shows error message
	public void makeErrorScreenActive(String message) {
		Alert alert = new Alert("ERROR",
				message, null, AlertType.ERROR);
		alert.setTimeout(2000);
		Display.getDisplay(this).setCurrent(alert, Display.getDisplay(this).getCurrent());
	}
	

	
	//Shows Loginscreen
	public void makeMainScreenActive(){
		Display.getDisplay(this).setCurrent(mainScreen);
	}
		
	//Shows Waitingscreen
	public void makeWaitingScreenActive(){
		Form ws = new WaitingScreen(this, "Please wait... We are transmitting your data to ORIENT!", false);
		Display.getDisplay(this).setCurrent(ws);
	}

//BLUETOOTH
	/**
	 * Sends message to server.
	 * message: MobilePhone|Name|Word
	 */
//	public void sendMessage(String message){
//		String toSend = "MobilePhone§" + type + "§" + message;
//		setkeepConnected(true);
//		MediaFile media = new MediaFile(toSend.getBytes(), "", MediaFile.TEXT);
//		mapConnectionController.messageToMAP(media, pcURL);
//		
//	}

	/**
	 * Sends audiofile to server.
	 */
	public void sendAudioFile(byte[] audioFile){
		setkeepConnected(true);
		//ap.setDebug("a");
		MediaFile media = new MediaFile(audioFile, "", MediaFile.AUDIO);
		//ap.setDebug("b"+pcURL);
		mapConnectionController.messageToMAP(media, pcURL);
		//ap.setDebug("c");
	}
	

	public void messageFromMAP(MediaFile file) {
		if(!connected)connected = true;
		if(new String(file.getData()).equalsIgnoreCase(MediaFile.NEWACCESSPOINT)){
			makeSuccessScreenActive("You are successfully connected to ORIENT!");
			pcURL = file.getReplyAddress();
			this.sendMessage("connect", "connect");
		}else{
			//makeSuccessScreenActive("Spryte "+new String(file.getData())+" has been selected!");
			 Display.getDisplay(this).setCurrent(mainScreen);
		}
		
		
	}

	public void setServerNotReachable(String serverURL) {
		// TODO Auto-generated method stub
		
	}
	
	public void setkeepConnected(boolean value){
		mapConnectionController.setKeepConnected(value);
	}

	public void sendMessage(String message, String type) {
		String toSend = "MobilePhone§" + type + "§" + message;
		setkeepConnected(true);
		MediaFile media = new MediaFile(toSend.getBytes(), "", MediaFile.TEXT);
		mapConnectionController.messageToMAP(media, pcURL);
	}

	public void makeSuccessScreenActive(String temp) {
		  Alert a = new Alert("Info", temp,
	        null,
	        AlertType.INFO);
		  a.setTimeout(1500);
		  Display.getDisplay(this).vibrate(1000);
			AlertType.INFO.playSound(Display.getDisplay(this));

		 Display.getDisplay(this).setCurrent(a, mainScreen);
		 
		
	}

	public void debug(String string) {
	//	makeSuccessScreenActive(string);
		
	}


}