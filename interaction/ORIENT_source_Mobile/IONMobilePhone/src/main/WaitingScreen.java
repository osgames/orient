package main;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.MIDletStateChangeException;

import main.IONMidlet;


public class WaitingScreen extends Form implements CommandListener{

	private int maximum = 20;
	boolean load = true;
	private Gauge skala;
	private StringItem text;
	protected Command exitCommand = new Command("Exit", Command.EXIT, 1);

	protected Command cancelCommand = new Command("Cancel", Command.CANCEL, 1);

	
	private IONMidlet midlet;
	
	public WaitingScreen(IONMidlet midlet, String message, boolean start){
		super("Connecting...");
		this.midlet = midlet;
		
		text = new StringItem("",message);
		text.setLayout(StringItem.LAYOUT_CENTER|StringItem.LAYOUT_NEWLINE_AFTER);
		
		skala = new Gauge("", false,Gauge.INDEFINITE,
				Gauge.CONTINUOUS_RUNNING);
		skala.setLayout(Gauge.LAYOUT_CENTER);
		
		append(text);
		append(skala);
	    if(start)this.addCommand(exitCommand);
	    else this.addCommand(cancelCommand);
		this.setCommandListener(this);
	}
	
	public void commandAction(Command cmd, Displayable disp){
		if (cmd == exitCommand) {
			try {
				midlet.destroyApp(true);
			} catch (MIDletStateChangeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			midlet.makeMainScreenActive();
		}
	}
	
	
}