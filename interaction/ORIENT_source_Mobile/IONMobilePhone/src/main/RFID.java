package main;
import java.io.IOException;

import javax.microedition.contactless.ContactlessException;
import javax.microedition.contactless.DiscoveryManager;
import javax.microedition.contactless.TargetListener;
import javax.microedition.contactless.TargetProperties;
import javax.microedition.contactless.TargetType;
import javax.microedition.contactless.ndef.NDEFMessage;
import javax.microedition.contactless.ndef.NDEFRecord;
import javax.microedition.contactless.ndef.NDEFTagConnection;
import javax.microedition.io.Connector;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Display;

public class RFID implements TargetListener {
	private MainScreen screen;

	public RFID(MainScreen screen) {
		this.screen = screen;
		try {
			DiscoveryManager.getInstance().addTargetListener(this,
					TargetType.NDEF_TAG);
		} catch (ContactlessException ce) {
		}
	}

	public void targetDetected(TargetProperties[] properties) {
		NDEFTagConnection conn = null;

		try {
			Display.getDisplay(screen.getMidlet()).vibrate(1000);
			AlertType.INFO.playSound(Display.getDisplay(screen.getMidlet()));

			conn = (NDEFTagConnection) Connector.open(properties[0].getUrl());
			System.out.println("Connection type: " + conn.getClass());

			NDEFMessage message = conn.readNDEF();

			NDEFRecord record = message.getRecord(0);
			byte[] data = record.getPayload();
			String temp = new String(data);

			screen.sendMessage(temp, MainScreen.RFID);
			screen.makeSuccessScreenActive("The object "+temp+" has been virtualized!");

		} catch (Exception ex) {
		} finally {
			try {
				conn.close();
				// here
				// DiscoveryManager.getInstance().removeTargetListener(this,
				// TargetType.NDEF_TAG);
				// to here
			} catch (IOException ex) {
			}
		}

	}

	public void exit() {
		DiscoveryManager.getInstance().removeTargetListener(this,
				TargetType.NDEF_TAG);

	}

}
