package main;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.control.RecordControl;

public class RecordThread extends Thread {
	private AudioRecorder audioRec;
	
    public RecordThread(AudioRecorder audioRec) {
        this.audioRec = audioRec;
    }
    
    public void run() {
        try {
            // create the player using standard encoding
        	audioRec.player = Manager.createPlayer("capture://audio");
        	audioRec.player.prefetch();
        	audioRec.player.addPlayerListener(audioRec);
        	audioRec.player.realize();
            
            // get the RecordControl over this Player
        	audioRec.rc = (RecordControl)audioRec.player.getControl("RecordControl");
            // create an OutputStream which the RecordControl will use
            // to write write the recorded data.
        	audioRec.output = new ByteArrayOutputStream();
        	audioRec.rc.setRecordStream(audioRec.output);
            // start the recording
        	audioRec.rc.startRecord();
        	audioRec.player.start();
//            int seconds = 0;
//            while(audioRec.running) {
//                try {
//                    sleep(1000);
//                    seconds++;
//                    audioRec.audioItem.setText(Integer.toString(seconds)+"s");
//                } catch(InterruptedException e) {
////                    System.out.println(e.toString());
//                	//midlet.makeErrorScreenActive(e.toString());
//                }
//
//            }
        } catch(MediaException e) {
//            System.out.println(e.toString());
        	//midlet.makeErrorScreenActive(e.toString());
        } catch(IOException e) {
//            System.out.println(e.toString());
        	//midlet.makeErrorScreenActive(e.toString());
        }
    }
}

