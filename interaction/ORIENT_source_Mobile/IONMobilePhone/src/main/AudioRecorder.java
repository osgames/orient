package main;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.microedition.media.Player;
import javax.microedition.media.PlayerListener;
import javax.microedition.media.control.RecordControl;

public class AudioRecorder implements PlayerListener {
	private MainScreen screen;

	private RecordThread recordThread;

	private byte[] soundData = null;

	public static final String SEND = "Send";

	public static final String START = "Start";

	// AudioChat
	public boolean running = false;

	public ByteArrayOutputStream output;

	public Player player;

	public RecordControl rc;

	/**
	 * With the AudioRecorder you can record audiofiles and send them to every
	 * participant who chose the function "Audiochat(Receiver)".
	 */
	public AudioRecorder(MainScreen screen) {
		this.screen = screen;
	}

	/**
	 * Record audio.
	 */
	void recordAudio() {
		screen.addCommand(SEND);
		synchronized (this) {
			screen.removeCommand(START);
			if (!running) {
				running = true;
				recordThread = new RecordThread(this);
				recordThread.start();
			}
		}
	}

	/**
	 * Stops the record and displays the result.
	 */
	void stopRecord() {
		if (!running) // ignore while not running
			return;
		this.running = false;
		try {
			rc.commit();
			// save the recordedData in a byte array
			soundData = output.toByteArray();
			player.close();

		} catch (IOException ex) {
			// ex.printStackTrace();
		}
	}

	public void playerUpdate(Player player, String event, Object eventData) {
		if (event.equals("closed")) {

			if (soundData != null) {
				screen.removeCommand(SEND);
				screen.sendAudioFile(soundData);
				screen.addCommand(START);

			}
		}
	}

	public void close() {
		if (player != null)
			player.close();
	}

}
