package de.avetana.bluetooth.obex;

public class Service {
	private javax.bluetooth.ServiceRecord srec;

	public void setServiceRecord(javax.bluetooth.ServiceRecord srec) {
		this.srec = srec;
	}
	
	public javax.bluetooth.ServiceRecord getServiceRecord(){
		return this.srec;
	}

}
