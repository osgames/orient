package de.uni.augsburg.informatik.mm.map.mobile.connection.server;



import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;
import javax.microedition.io.Connection;



import de.avetana.bluetooth.obex.Service;
import de.avetana.javax.obex.SessionNotifier;
import de.avetana.obexsolo.OBEXConnector;
import de.uni.augsburg.informatik.mm.map.mobile.connection.client.MapConnectionControllerImpl;
import de.uni.augsburg.informatik.mm.map.mobile.model.MediaFile;

public class MapServer implements Runnable {

	
	UUID uuid = new UUID("8841", true);

	private String connection;

	private String serverURL;

	private MapConnectionControllerImpl controller;

	private SessionNotifier sessionNotifier;
	private Connection con;


	private LocalDevice local = null;

	public MapServer(String service_name, MapConnectionControllerImpl controller) {
		super();
		this.controller = controller;
		this.connection = "btgoep://localhost:" + uuid + ";authenticate=false;"
				+ "encrypt=false;master=false;name=" + service_name;
		new Thread(this).start();
	}

	public void run() {
		try {
			local = LocalDevice.getLocalDevice();
		} catch (BluetoothStateException e) {
			e.printStackTrace();
		}
		try {
			local.setDiscoverable(DiscoveryAgent.LIAC);
		} catch (BluetoothStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		waitingForClient();

	}
	Service service;
	public void waitingForClient() {
		 service = new Service();
		try {
			sessionNotifier = (SessionNotifier) OBEXConnector.open(connection,
					service);
		} catch (IOException e) {
			e.printStackTrace();
		}
		ServiceRecord record = service.getServiceRecord();
		serverURL = record.getConnectionURL(
				ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
		//controller.debug("Die Service-URL von dem registrierten Dienst lautet:" + serverURL);

		MapHandler handler = new MapHandler(this);
		try {
			con = sessionNotifier.acceptAndOpen(handler);
		} catch (IOException e) {
			e.printStackTrace();
			//controller.debug("nnnn");
		}
		
	}

	public String getURL() {
		return serverURL;
	}

	public void sentMediaFileFromMAP(MediaFile file, MapHandler handler) {
		controller.messageFromMAP(file);
		try {
			Thread.sleep(500);

		} catch (Exception e) {
		}
		if(con!=null)
			try {
				con.close();
			} catch (IOException e1) {
				//controller.debug("io excpetion con close" );
			}catch (Exception e){
				//controller.debug("excpetion con close" );
			}
		controller.restartServer();
		System.gc();
	}
	

	public void debug(String string) {
		controller.debug(string);
		
	}

	public void unregisterServer() {
		//controller.debug("unregister"+service.getServiceRecord());
		if(service!=null)service.setServiceRecord(null);
		try {
			sessionNotifier.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
