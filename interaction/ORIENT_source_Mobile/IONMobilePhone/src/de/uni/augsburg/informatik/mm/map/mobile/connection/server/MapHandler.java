package de.uni.augsburg.informatik.mm.map.mobile.connection.server;



import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;




import de.avetana.javax.obex.HeaderSet;
import de.avetana.javax.obex.Operation;
import de.avetana.javax.obex.ResponseCodes;
import de.avetana.javax.obex.ServerRequestHandler;
import de.uni.augsburg.informatik.mm.map.mobile.model.MediaFile;


public class MapHandler extends ServerRequestHandler {

	private MapServer server;
	
	public MapHandler(MapServer server) {
		this.server = server;
	}

	public void onDisconnect(HeaderSet request, HeaderSet reply) {
		super.onDisconnect(request, reply);
	}

	public int onPut(Operation op) {
		try {
			HeaderSet set = op.getReceivedHeaders();
			String name = (String) set.getHeader(HeaderSet.NAME);
			String type = (String) set.getHeader(HeaderSet.TYPE);
			String replyURL = (String) set.getHeader(HeaderSet.DESCRIPTION);
			int length = (int) ((Long) set.getHeader(HeaderSet.LENGTH))
					.longValue();
			InputStream in = op.openInputStream();
			byte[] data;
			int buffer = in.read();
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();

			for (int i = 0; i < length; i++) {
				outStream.write((byte) buffer);
				buffer = in.read();
			}

			data = outStream.toByteArray();
			in.close();
			outStream.close();
			if(server.getURL().startsWith("btspp://"))new Thread(new MapReceivedData(server, new MediaFile(data, name, type,
					replyURL), this));
		} catch (IOException e) {
			server.debug("exONPUT");
		}catch(Exception e){
			server.debug("fehler");
		}
		return ResponseCodes.OBEX_HTTP_OK;
	}
	
	public int onConnect(HeaderSet request, HeaderSet reply) {
		return super.onConnect(request, reply);
	}

}
