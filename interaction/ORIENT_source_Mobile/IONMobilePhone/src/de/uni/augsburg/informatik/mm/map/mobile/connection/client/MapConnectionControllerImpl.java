package de.uni.augsburg.informatik.mm.map.mobile.connection.client;
//import MapMIDlet;

import java.util.Hashtable;
import java.util.Vector;

import main.IONMidlet;


import de.uni.augsburg.informatik.mm.map.mobile.connection.server.MapServer;
import de.uni.augsburg.informatik.mm.map.mobile.model.MediaFile;



public class MapConnectionControllerImpl implements MapConnectionController{
	private MapServer myServer;
	//private String pcServerURL;
	private Vector listener;
	private String serviceName;
	private boolean keepConnected;
	private Hashtable map = new Hashtable();
	
	public MapConnectionControllerImpl(String serviceName){
		this.serviceName = serviceName;
		this.listener = new Vector();
		myServer = new MapServer(serviceName, this);
		this.keepConnected = false;
		
	}
	
	public void addConnectionListener(ConnectionListener listener){
		this.listener.addElement(listener);
	}
	
	public void messageToMAP(MediaFile mediaFile, String pcServerURL){
		mediaFile.setReplyAddress(myServer.getURL());
		if(!map.containsKey(pcServerURL))map.put(pcServerURL, new MapClientPut(this, pcServerURL));
		((MapClientPut)(map.get(pcServerURL))).addMediaFile(mediaFile);
	
	}
	
	public void messageFromMAP(MediaFile file) {
		for(int i = 0;i<listener.size();i++){
			((ConnectionListener)listener.elementAt(i)).messageFromMAP(file);
		}
	}
	
	public void debug(String string){
		((IONMidlet)listener.elementAt(0)).debug(string);
	}

	public void unregisterServer() {
		myServer.unregisterServer();		
	}

	public void restartServer() {
		unregisterServer();
		try {
			Thread.sleep(1000);

		} catch (Exception e) {
		}
		myServer.waitingForClient();
		
	}

	public boolean isKeepConnected() {
		return keepConnected;
	}

	public void setKeepConnected(boolean keepConnected) {
		this.keepConnected = keepConnected;
	}

	public String getServerURL() {
		return myServer.getURL();
	}

	public void setServerNotReachable(String serverURL) {
		MapClientPut client = (MapClientPut) map.remove(serverURL);
		if(client!=null)client.closeConnection();
		//debug("Server deleted: "+serverURL+" "+client);
		for(int i = 0;i<listener.size();i++){
			((ConnectionListener)listener.elementAt(i)).setServerNotReachable(serverURL);
		}
	}

}
