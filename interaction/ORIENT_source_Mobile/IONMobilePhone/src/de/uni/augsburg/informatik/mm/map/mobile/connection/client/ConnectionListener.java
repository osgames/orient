package de.uni.augsburg.informatik.mm.map.mobile.connection.client;
import de.uni.augsburg.informatik.mm.map.mobile.model.MediaFile;


public interface ConnectionListener {
	public void messageFromMAP(MediaFile file);

	public void setServerNotReachable(String serverURL);
}
