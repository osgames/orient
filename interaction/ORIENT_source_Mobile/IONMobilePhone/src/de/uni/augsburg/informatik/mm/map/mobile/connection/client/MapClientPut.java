package de.uni.augsburg.informatik.mm.map.mobile.connection.client;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;

import de.avetana.javax.obex.ClientSession;
import de.avetana.javax.obex.HeaderSet;
import de.avetana.javax.obex.Operation;
import de.avetana.obexsolo.OBEXConnector;
import de.uni.augsburg.informatik.mm.map.mobile.model.MediaFile;

public class MapClientPut extends Thread {

	private MapConnectionControllerImpl controller;

	private ClientSession con;

	private String pcURL;
	private Vector writeQueue = new Vector();

	private int count = 0;


	public MapClientPut(MapConnectionControllerImpl controller,String pcURL) {
		this.controller = controller;
		
	
		this.pcURL = pcURL;
		
		this.start();
	}

	private int MAXLENGTH = 1024 * 256;
	//private int MAXLENGTH = 1023;//900;//768;//512;

	/**
	 * versendet daten an den PC z.B. Text. Es kann aber auch ein Bild, Audio
	 * oder Video sein
	 */
	public void run() {
		connect();
	}

	public void connect() {
		try {
			con = (ClientSession) OBEXConnector.open(pcURL, null);
			count = 0;
			con.connect(con.createHeaderSet());
			while(controller.isKeepConnected()){
				if (!writeQueue.isEmpty())
                {
                    MediaFile sendData =  (MediaFile)writeQueue.elementAt(0);
                    try
                    {
                    	send(sendData);
                        writeQueue.removeElement(sendData);
                    }
                    catch (IOException e)
                    {
                    	try {
            				sleep(300);
            			} catch (InterruptedException e2) {
            				// TODO Auto-generated catch block
            				e.printStackTrace();
            			}
            			//  controller.debug("4fehler");
            			addMediaFile(sendData);

                    }
                }else{

	                synchronized(writeQueue)
	                {
	                    if (writeQueue.size() == 0)
	                    {
	                        try
	                        {
	                            writeQueue.wait(500);
//	                            if(controller.isKeepConnected())addMediaFile(new MediaFile(MediaFile.KEEPCONNECTED.getBytes(),MediaFile.KEEPCONNECTED, MediaFile.TEXT,controller.getServerURL()));
//	                            else addMediaFile(new MediaFile("diconnect".getBytes(),MediaFile.KEEPCONNECTED, MediaFile.TEXT,controller.getServerURL()));
	                        }
	                        catch (InterruptedException e)
	                        {
	                            // can't happen in MIDP
	                        }
	                    }
	                }
                }
			}
			con.disconnect(null);
			con.close();
	
			
		} catch (IOException e) {
			try {
				Thread.sleep(300);

			} catch (Exception e2) {
			}
			count++;
			if(count==5){
				controller.setKeepConnected(false);
				controller.setServerNotReachable(pcURL);
			}
			else connect();
			e.printStackTrace();

		}
	}
		
		
		
	public void send(MediaFile file) throws IOException{
		byte[]message = file.getData();
		long length = message.length;
		HeaderSet header = con.createHeaderSet();
		header.setHeader(HeaderSet.LENGTH, new Long(length));
		header.setHeader(HeaderSet.TYPE, file.getType());
		header.setHeader(HeaderSet.NAME, file.getDescription());
		header.setHeader(HeaderSet.DESCRIPTION, file.getReplyAddress());
		if(file.getType().equals(MediaFile.TEXT)&&new String(message).equals(MediaFile.KEEPCONNECTED)){
			Operation op = con.get(header);
			op.close();
		}else{
			Operation operation = con.put(header);// Erst den header
			OutputStream output = operation.openOutputStream();// ausgabe
			int position = 0;
			int sendLength = 0;
			while (position != length) {
				// OutputStream outputStream = this.outputStream;
				sendLength = length - position > MAXLENGTH ? MAXLENGTH
						: (int) length - position;
				if (output == null) {
					throw new IOException();
				}
				output.write(message, position, sendLength);
				position += sendLength;
			}
			output.close();
			operation.close();
		}
	}
	
	public void closeConnection(){
		if(con!=null)
			try {
				con.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
		

	public void addMediaFile(MediaFile mediaFile) {
		synchronized (writeQueue) 
		{
			writeQueue.addElement(mediaFile);
			writeQueue.notify();
		}
		
	}

}
