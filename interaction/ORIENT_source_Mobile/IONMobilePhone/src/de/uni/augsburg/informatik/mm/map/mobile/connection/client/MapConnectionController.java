package de.uni.augsburg.informatik.mm.map.mobile.connection.client;
import de.uni.augsburg.informatik.mm.map.mobile.model.MediaFile;


public interface MapConnectionController {
	public void addConnectionListener(ConnectionListener listener);
	public void messageToMAP(MediaFile mediaFile, String pcURL);
	public void unregisterServer();
	public void setKeepConnected(boolean keepConnected);
}
