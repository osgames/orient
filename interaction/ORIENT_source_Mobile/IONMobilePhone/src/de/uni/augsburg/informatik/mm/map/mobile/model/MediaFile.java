package de.uni.augsburg.informatik.mm.map.mobile.model;

public class MediaFile {

		public static final String VIDEO = "video/3gpp";

		public static final String TEXT = "text/txt";

		public static final String AUDIO = "audio/amr";

		public static final String PIC = "pic/jpg";

		public static final String KEEPCONNECTED = "keepConnected";
		
		public static final String NEWACCESSPOINT ="newAccesspoint";
		
		private byte[]data;
		private String description;
		private String type;
		private String replyAddress;
		
		public MediaFile(byte[] data, String description, String type, String replyAddress){
			this.data = data;
			this.description = description;
			this.type = type;
			this.replyAddress = replyAddress;
		}
		
		public MediaFile(byte[] data, String description, String type){
			this.data = data;
			this.description = description;
			this.type = type;
		}

		public byte[] getData() {
			return data;
		}

		public String getDescription() {
			return description;
		}

		public String getType() {
			return type;
		}

		public String getReplyAddress() {
			return replyAddress;
		}

		public void setReplyAddress(String replyAddress) {
			this.replyAddress = replyAddress;
			
		}


	}
