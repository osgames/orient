package de.uni.augsburg.informatik.mm.map.mobile.connection.server;
import de.uni.augsburg.informatik.mm.map.mobile.model.MediaFile;


public class MapReceivedData implements Runnable{
	private MediaFile file;
	private MapServer server;
	private MapHandler handler;
	
	public MapReceivedData(MapServer server, MediaFile file, MapHandler handler) {
		this.server = server;
		this.file = file;
		this.handler = handler;
		new Thread(this).start();
	}

	public void run() {
		server.sentMediaFileFromMAP(file, handler);		
	}

}
