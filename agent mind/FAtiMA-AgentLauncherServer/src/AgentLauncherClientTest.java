package src;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

//THIS IS JUST A CLASS TO TEST THE SERVER - CLIENT COMMUNICATION
public class AgentLauncherClientTest{
	  public static void main(String[] args) throws IOException {

		  	final String IP = "192.168.1.2"; 
	        Socket socket = null;
	        PrintWriter out = null;
	        BufferedReader in = null;

	        try {
	            
	        	socket = new Socket(IP, 4444);
	            out = new PrintWriter(socket.getOutputStream(), true);
	            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	            
	        } catch (UnknownHostException e) {

	        	System.err.println("Don't know about host: localhost.");
	            System.exit(1);
	            
	        } catch (IOException e) {
	            System.err.println("Couldn't get I/O for the connection to: " + IP);
	            System.exit(1);
	        }

	        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
	       
	        String fromUser = "java -cp \"FAtiMA.jar\" FAtiMA.Agent DinnerParty Sam";
	        out.println(fromUser);
	        out.close();
	        in.close();
	        stdIn.close();
	        socket.close();
	    }
}
