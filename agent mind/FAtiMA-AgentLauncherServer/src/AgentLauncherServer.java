package src;

import java.awt.BorderLayout;
import java.awt.Font;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class AgentLauncherServer extends JFrame {
	private static final String UI_TITLE = "Agent Launcher Server";
	private static JTextArea _textUI = new JTextArea(20, 100);

	private static final long serialVersionUID = 1L;
	static int TCP_PORT = 4444;
	
	public static void printlnUI(String text){
		_textUI.append(text + "\n");
		_textUI.setEditable(false);
	}

	private void setupUI() {

		_textUI.setFont(new Font("MonoSpaced", Font.PLAIN, 14));
		
		JScrollPane scrollingArea = new JScrollPane(_textUI);

		//... Get the content pane, set layout, add to center
		JPanel content = new JPanel();
		content.setLayout(new BorderLayout());
		content.add(scrollingArea, BorderLayout.CENTER);

		//... Set window characteristics.
		this.setContentPane(content);
		this.setTitle(UI_TITLE);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}

	private void run() throws IOException {
		ServerSocket serverSocket = null;
		Socket socket;
		boolean listening = true;

		try {  	
			serverSocket = new ServerSocket(TCP_PORT); //TODO: Read the port from a config file     
			printlnUI("Agent Launcher Server has started on port " + TCP_PORT);
		} catch (IOException e) {
			printlnUI("Could not listen on port: " + TCP_PORT);
			System.in.read(); //just for the user can see the error message
			System.exit(-1);
		}

		try{
			while (listening){
				socket = serverSocket.accept();
				printlnUI("New AgentLauncher Request");
				new AgentLauncherThread(socket).start();   	
			}	        
			serverSocket.close(); 	
		} catch(IOException e){
			printlnUI("Socket exception: " + e);
			System.in.read(); //just for the user can see the error message
			System.exit(-1);
		}
	}

	static public void main(String args[]) throws IOException {
		
		AgentLauncherServer server = new AgentLauncherServer();
		server.setupUI();
		server.run();
		
	}

}
