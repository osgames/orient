package src;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class AgentLauncherThread extends Thread {
	public static final int AGENT_DISPLAY_NAME_POSITION = 9;

	private Socket socket = null;

	public AgentLauncherThread(Socket socket) {
		super("AgentLauncherThread");
		this.socket = socket;
	}

	public void run() {
		PrintWriter out = null;
		BufferedReader in = null;
		try {
			out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));			
			String agentArguments = in.readLine();
			AgentLauncherServer.printlnUI("Agent Arguments: " + agentArguments);

			String[] agentArgumentsSplitted = agentArguments.split(" ");	
			String agentDisplayName = agentArgumentsSplitted[AGENT_DISPLAY_NAME_POSITION];

			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec("java -cp \"FAtiMA.jar\" FAtiMA.Agent " + agentArguments);


			// any error message?
			StreamGobbler errorGobbler = new 
			StreamGobbler(proc.getErrorStream(), "ERROR", agentDisplayName);            

			// any output?
			StreamGobbler outputGobbler = new 
			StreamGobbler(proc.getInputStream(), "OUTPUT", agentDisplayName);

			// kick them off
			errorGobbler.start();
			outputGobbler.start();

			// any error???
			int exitVal = proc.waitFor();
			AgentLauncherServer.printlnUI("ExitValue: " + exitVal);        			

			Thread.sleep(1000);
			out.close();
			in.close();
			socket.close();

		} catch(Throwable t) {
			t.printStackTrace();
			out.close();
		}
	}
}


class StreamGobbler extends Thread
{
	InputStream is;
	String type;
	String streamId;

	StreamGobbler(InputStream is, String type, String streamId)
	{
		this.is = is;
		this.streamId = streamId;
		this.type = type;
	}

	public void run()
	{
		try
		{
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line=null;
			while ( (line = br.readLine()) != null && (!line.equalsIgnoreCase(""))){
				AgentLauncherServer.printlnUI(type + "-" + streamId + "->" + line);    
			}
		}catch (IOException ioe)
		{
			ioe.printStackTrace();  
		}
	}
}
