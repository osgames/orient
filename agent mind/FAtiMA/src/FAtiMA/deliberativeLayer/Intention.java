/** 
 * Intention.java - Represents an explicit intention to achieve a goal
 *  
 * Copyright (C) 2006 GAIPS/INESC-ID 
 *  
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Company: GAIPS/INESC-ID
 * Project: FAtiMA
 * Created: 14/01/2004 
 * @author: Jo�o Dias
 * Email to: joao.assis@tagus.ist.utl.pt
 * 
 * History: 
 * Jo�o Dias: 14/01/2004 - File created
 * Jo�o Dias: 24/05/2006 - Added comments to each public method's header
 * Jo�o Dias: 24/05/2006 - Removed the Intention's type from the class (was not being used)
 * Jo�o Dias: 10/07/2006 - the class is now serializable 
 * Jo�o Dias: 17/07/2007 - Instead of storing two instances of emotions (Hope and Fear),
 * 						   the class now stores only the hashkeys of such emotions in order
 * 						   to make the class easily serializable
 */
package FAtiMA.deliberativeLayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.ListIterator;

import FAtiMA.autobiographicalMemory.AutobiographicalMemory;
import FAtiMA.culture.Ritual;
import FAtiMA.deliberativeLayer.goals.ActivePursuitGoal;
import FAtiMA.deliberativeLayer.goals.Goal;
import FAtiMA.deliberativeLayer.plan.Plan;
import FAtiMA.emotionalState.ActiveEmotion;
import FAtiMA.emotionalState.EmotionalState;
import FAtiMA.motivationalSystem.MotivationalState;
import FAtiMA.sensorEffector.Event;
import FAtiMA.util.AgentLogger;


/**
 * Represents an explicit intention to achieve an ActivePursuitGoal
 * @author Jo�o Dias
 */
public class Intention implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final int MAXPLANS = 100;
	
	private String _fearEmotionID;

	private String _hopeEmotionID;
	private ActivePursuitGoal _goal;
	private ArrayList _planConstruction;
	private Intention _subIntention = null;
	private Intention _parentIntention = null;
	private boolean _strongCommitment;


	/**
	 * Creates a new Intention to a achieve a goal
	 * @param p - the initial empty plan to achieve the goal
	 * @param g - the goal that the intention tries to achieve
	 * @see Plan
	 * @see Goal
	 */
	public Intention(ActivePursuitGoal g) {
		_goal = g;
		_planConstruction = new ArrayList();
		_fearEmotionID = null;
		_hopeEmotionID = null;
		_strongCommitment = false;
	}
	
	
	
	/**
	 * Adds a plan to the set of alternative plans that the agent has
	 * to achieve the intention
	 * @param p - the Plan to add
	 * @see Plan
	 */
	public void AddPlan(Plan p) {
		if(_planConstruction.size() <= MAXPLANS)
		{
			_planConstruction.add(p);
		}
	}
	
	public void AddPlans(ArrayList plans)
	{
		ListIterator li;
		Plan p;
		li = plans.listIterator();
		while(li.hasNext())
		{
			p = (Plan) li.next();
			_planConstruction.add(p);
		}
	}
	
	public void AddSubIntention(Intention i)
	{
		_subIntention = i;
		i.setMainIntention(this);
	}
	
	public boolean containsIntention(String goalName)
	{
		
		if(this._goal instanceof Ritual){
			if (this._goal.getNameWithCharactersOrdered().equalsIgnoreCase(goalName)) return true;
			if(this._subIntention != null)
			{
				return this._subIntention.containsIntention(goalName);
			}
			return false;
		}else{
			if (this._goal.getName().toString().equalsIgnoreCase(goalName)) return true;
			if(this._subIntention != null)
			{
				return this._subIntention.containsIntention(goalName);
			}
			return false;			
		}
		
		
	}
	
	public void RemoveSubIntention()
	{
		_subIntention = null;
	}
	
	public Intention GetSubIntention()
	{
		if(_subIntention != null)
		{
			return _subIntention.GetSubIntention();
		}
		else return this;
	}
	
	public void setMainIntention(Intention i)
	{
		this._parentIntention = i;
	}
	
	public boolean isRootIntention()
	{
		return this._parentIntention == null;
	}
	
	public Intention getParentIntention()
	{
		return this._parentIntention;
	}

	
	/**
	 * Gets the Fear emotion associated with the intention.
	 * This fear is caused by the prospect of failling to achieve the goal  
	 * @return - the Fear emotion
	 */
	public ActiveEmotion GetFear() {
		if(_fearEmotionID == null) return null;
		return EmotionalState.GetInstance().GetEmotion(_fearEmotionID);
	}


	/**
	 * Gets the goal that intention corresponds to
	 * @return the intention's goal
	 */
	public ActivePursuitGoal getGoal() {
		return _goal;
	}
	
	/**
	 * Gets the Home emotion associated with the intention.
	 * This hope is caused by the prospect of succeeding in achieving the goal  
	 * @return - the Hope emotion
	 */
	public ActiveEmotion GetHope() {
		if(_hopeEmotionID == null) return null;
		return EmotionalState.GetInstance().GetEmotion(_hopeEmotionID);
	}

	/**
	 * Gets the best plan developed so far to achieve the intention
	 * @return the best plan
	 */
	public Plan GetBestPlan() {
		ListIterator li;
		Plan p;
		Plan bestPlan = null;
		float minH = 9999999;

		li = _planConstruction.listIterator();
		while (li.hasNext()) {
			p = (Plan) li.next();
			if (p.h() < minH) {
				bestPlan = p;
				minH = p.h();
			}
		}
		return bestPlan;
	}
	
	/**
	 * Gets the likelihood of the agent achieving the intention
	 * @return a float value representing the probability [0;1]
	 */
	public float GetProbability() {
		ListIterator li;
		float p;
		float bestProb = 0;
		li = _planConstruction.listIterator();
		while (li.hasNext()) {
			p = ((Plan) li.next()).getProbability();
			if (p > bestProb) bestProb = p; 
		}
		return bestProb;
	}
	
	public float GetExpectedUtility()
	{
		_goal.SetProbability(new Float(GetProbability()));
		return _goal.GetExpectedUtility();
	}
	

	/**
	 * Gets the number of alternative plans that the agent has
	 * to achieve the intention
	 * @return
	 */
	public int NumberOfAlternativePlans() {
		return _planConstruction.size();
	}

	/**
	 * Removes the last plan from the list of alternative plans
	 * @return the removed Plan
	 */
	public Plan RemovePlan() {
		return (Plan) _planConstruction.remove(_planConstruction.size() - 1);
	}
	
	/**
	 * Removes the received plan from the list of alternative plans
	 * @param p - the plan to remove
	 */
	public void RemovePlan(Plan p) {
		_planConstruction.remove(p);
	}
	
	/**
	 * Sets the Fear emotion associated with the intention.
	 * This fear is caused by the prospect of failling to achieve the goal
	 * @param fear - the Fear emotion to associate with the intention  
	 */
	public void SetFear(ActiveEmotion fear) {
		if(fear != null) _fearEmotionID = fear.GetHashKey();
	}
	
	/**
	 * Sets the Hope emotion associated with the intention.
	 * This hope is caused by the prospect of succeeding in achieving the goal
	 * @param hope - the hope emotion to associate with the intention  
	 */
	public void SetHope(ActiveEmotion hope) {
		if(hope!= null) _hopeEmotionID = hope.GetHashKey();
	}
	
	public boolean IsStrongCommitment()
	{
		return _strongCommitment;
	}
	
	public void SetStrongCommitment()
	{
		if(!_strongCommitment)
		{
			_strongCommitment = true;
			ProcessIntentionActivation();
		}
	}
	
	/**
	 * Converts the intention to a String
	 * @return the converted String
	 */
	public String toString() {
		return "Intention: " + _goal;
	}
	
	/**
	 * Updates all the plans for the intention according to the new 
	 * state of the world. Supports continuous planning.
	 */
	public void CheckLinks() {
	    ListIterator li;
	    li = _planConstruction.listIterator();
	    
	    while(li.hasNext()) {
	        ((Plan) li.next()).UpdatePlan();
	    }
	    
	    if(this._subIntention != null)
	    {
	    	this._subIntention.CheckLinks();
	    }
	}
	
	/**
	 * Updates the probability of achieving the intention
	 * This function should be called whenever the plans change
	 */
	public void UpdateProbabilities() {
	    ListIterator li;
	    li = _planConstruction.listIterator();
	    
	    while(li.hasNext()) {
	        ((Plan) li.next()).UpdateProbabilities();
	    }
	    
	    if(this._subIntention != null)
	    {
	    	this._subIntention.UpdateProbabilities();
	    }
	}
	
	/**
	 * Registers and appraises the activation of a given intention
	 * @param intention - the intention that was activated
	 */
	private void ProcessIntentionActivation() 
	{
	    Event e = _goal.GetActivationEvent();
	    
	    AgentLogger.GetInstance().logAndPrint("Adding a new Strong Intention: " + _goal.getName().toString());
	  
	    AutobiographicalMemory.GetInstance().StoreAction(e);
	    
	    float probability = GetProbability();
		ActiveEmotion hope = EmotionalState.GetInstance().AppraiseGoalSucessProbability(_goal, probability);
		ActiveEmotion fear = EmotionalState.GetInstance().AppraiseGoalFailureProbability(_goal, 1 - probability);
		
		SetHope(hope);
		SetFear(fear);	
	}
	
	/**
	 * Registers and appraises the failure of this intention
	 */
	public void ProcessIntentionFailure() 
	{	
		//mental disengagement consists in lowering the goal's importance
		_goal.DecreaseImportanceOfFailure(0.5f);
		
		//_numberOfGoalsTried++;
		MotivationalState.GetInstance().UpdateCompetence(false);
		
		
	    Event e = _goal.GetFailureEvent();
	    
	    float observedError = _goal.getProbability();
	    float previousExpectedError = _goal.getUncertainty();
	    
	    float newExpectedError = ActivePursuitGoal.alfa * observedError + (1 - ActivePursuitGoal.alfa) * previousExpectedError;
	    float deltaError = newExpectedError - previousExpectedError;
	    MotivationalState.GetInstance().UpdateCertainty(-deltaError);
	    _goal.setUncertainty(newExpectedError);
	    
	    
	    AutobiographicalMemory.GetInstance().StoreAction(e);
	    EmotionalState.GetInstance().AppraiseGoalFailure(GetHope(),GetFear(), _goal);
	    
	    if(!isRootIntention())
	    {
	    	AgentLogger.GetInstance().logAndPrint("Removing Parent Intention!: " + this.getParentIntention());
	    	getParentIntention().ProcessIntentionFailure();
	    	//getParentIntention().CheckLinks();
	    }
	    
	     AgentLogger.GetInstance().logAndPrint("Goal FAILED - " + _goal.getName().toString());	    
	}
	
	/**
	 * Registers and appraises the success of the intention
	 */
	public void ProcessIntentionSuccess() 
	{
	
		//_numberOfGoalsAchieved++;
		//_numberOfGoalsTried++;
		MotivationalState.GetInstance().UpdateCompetence(true);
	    Event e = _goal.GetSuccessEvent();
	    
	    float observedError = 1 - _goal.getProbability();
	    float previousExpectedError = _goal.getUncertainty();
	    
	    float newExpectedError = ActivePursuitGoal.alfa * observedError + (1 - ActivePursuitGoal.alfa) * previousExpectedError;
	    float deltaError = newExpectedError - previousExpectedError;
	    MotivationalState.GetInstance().UpdateCertainty(-deltaError);
	    _goal.setUncertainty(newExpectedError);
	    
	    
	    
	    AutobiographicalMemory.GetInstance().StoreAction(e);
	    EmotionalState.GetInstance().AppraiseGoalSuccess(GetHope(), GetFear(), _goal);
	    
	    if(!isRootIntention())
	    {
	    	getParentIntention().CheckLinks();
	    }
	        		
	    		
	    AgentLogger.GetInstance().logAndPrint("Goal SUCCESS - " + getGoal().getName());
	}

}