using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;

namespace ION.Core.Extensions
{
    public abstract class AwakeAction : Action
    {
        #region Constants
        public const string ACTION_NAME = "Awake";
        #endregion

    }
}
