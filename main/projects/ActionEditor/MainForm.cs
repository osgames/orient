/* ****************************************************
 * Name: MainForm.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/16 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Mogre;
using PAGE.Generic.Resources.Actions;
using PAGE.OGRE.Domain.Assets;
using PAGE.Orient;
using PAGE.Orient.Domain.Cameras;
using PAGE.Orient.GUI.Windows.Forms.Forms;

namespace ActionEditor
{
    public partial class MainForm : Form
    {
        private OgreCharacter character;
        private OgreSet set;
        private ResourceAction action;
        private bool needsSave;
        private float oldTime;

        private ActionList Actions
        {
            get { return this.character == null ? null : this.character.ResourceAsset.Actions; }
        }

        public MainForm()
        {
            InitializeComponent();

            this.FormClosing += Form1_FormClosing;

            OrientGraphicsRealizer.Instance.Setup("./data/resources.xml", this.panel1);
            OrientGraphicsRealizer.Instance.Camera.CameraMode = CameraMode.Free;

            this.set = (OgreSet) OrientGraphicsRealizer.Instance.LoadSet("CharTestSet");
            this.set.Visible = true;
            this.set.HideWaypoints();

            this.actionsComboBox1.ResourcesManager = OrientGraphicsRealizer.Instance.ResourcesManager;
            this.actionsComboBox1.SelectedResourceChanged += actionsComboBox1_SelectedResourceChanged;
            this.actionControl1.TokenChanged += actionControl1_TokenChanged;
            this.charactersComboBox1.ResourcesManager = OrientGraphicsRealizer.Instance.ResourcesManager;
            this.charactersComboBox1.SelectedResourceChanged += charactersComboBox1_SelectedResourceChanged;

            this.panel1.AllowNavigation = true;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            OrientGraphicsRealizer.Instance.Destroy();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void charactersComboBox1_SelectedResourceChanged(object sender, EventArgs e)
        {
            if (this.character != null)
                OrientGraphicsRealizer.Instance.RemoveCharacter(this.character);

            this.character = (OgreCharacter) OrientGraphicsRealizer.Instance.LoadCharacter(
                this.charactersComboBox1.SelectedResource.IdToken);

            this.actionsComboBox1.Character = this.character.ResourceAsset;

            this.character.Set = this.set;
            this.character.Place(null, this.set, "waypoint0");
            this.character.Visible = true;
            this.character.HideAllDebuggingSpots();

            if (this.Actions.Count == 0)
            {
                this.newToolStripMenuItem_Click(this, EventArgs.Empty);
            }

            OrientGraphicsRealizer.Instance.Camera.PlaceOnCharacterBodyCamera(this.character);
            OrientGraphicsRealizer.Instance.Camera.FocusCharacterBody(this.character);

            this.actionToolStripMenuItem.Enabled = true;
            this.newToolStripMenuItem.Enabled = true;
            this.createActionsFromAnimationsToolStripMenuItem.Enabled = true;
            this.deleteAllToolStripMenuItem.Enabled = true;

            this.actionsGroupBox.Enabled = true;

            this.RefreshAnimations();
        }

        private bool AnimationExists(string animName)
        {
            foreach (ResourceAction resourceAction in this.Actions)
            {
                string[] fullTypeNames = this.action.GetType().ToString().Split(new char[] {'.'});
                string actionType = fullTypeNames[fullTypeNames.Length - 1];
                if ((actionType == "AnimationAction") ||
                    (actionType == "WalkToAction"))
                {
                    AnimationAction animAction = (AnimationAction) resourceAction;
                    if (animAction.AnimationName == animName)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void RefreshAnimations()
        {
            ArrayList animNames = new ArrayList();
            AnimationStateIterator iter = this.character.Entity.AllAnimationStates.GetAnimationStateIterator();
            while (iter.MoveNext())
            {
                AnimationState animation = iter.Current;
                animNames.Add(animation.AnimationName);
            }
            this.actionControl1.AnimationNames = animNames;
        }

        private void RefreshCharacterAction()
        {
            if (this.character == null) return;

            this.deleteToolStripMenuItem.Enabled = this.action != null;

            float maxTime = this.character.Entity.GetAnimationState(
                ((AnimationAction) this.action).AnimationName).Length;
            this.trackBar1.Maximum = (int) maxTime*1000;
            this.trackBar1.Value = 0;
            this.actionControl1.TriggerMaxTime = maxTime;
            this.oldTime = 0;

            if (action is WalkToAction)
            {
                this.character.Place(null, this.set, "waypoint0");
                this.character.Move(new MoveActionCallbackClass(
                                        this.character, this.set, this.action.IdToken), this.action.IdToken,
                                    this.set, "waypoint2");
            }
            else if (action is AnimationAction)
            {
                this.character.Animate(null, this.action.IdToken);
            }
        }

        private void DeleteCurrentAction()
        {
            this.Actions.Remove(this.action);
            this.action = null;
            this.needsSave = true;
            this.actionsComboBox1.RefreshResourceList();
        }

        private void createActionsFromAnimationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnimationStateIterator iter = this.character.Entity.AllAnimationStates.GetAnimationStateIterator();
            while (iter.MoveNext())
            {
                AnimationState animation = iter.Current;
                if (this.Actions.Contains(animation.AnimationName) ||
                    this.AnimationExists(animation.AnimationName))
                    continue;

                AnimationAction animationAction =
                    new AnimationAction(animation.AnimationName, animation.AnimationName);
                this.Actions.Add(animationAction);
            }
            this.needsSave = true;
            this.actionsComboBox1.RefreshResourceList();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int actionCount = 0;
            string actionName = "Action " + actionCount++;
            while (this.Actions.Contains(actionName))
            {
                actionName = "Action " + actionCount++;
            }

            string animName = "";
            AnimationStateIterator iter = this.character.Entity.AllAnimationStates.GetAnimationStateIterator();
            while (iter.MoveNext())
            {
                AnimationState animation = iter.Current;
                animName = animation.AnimationName;
                break;
            }

            this.action = new AnimationAction(actionName.ToLower(), animName);
            this.Actions.Add(this.action);

            this.needsSave = true;

            this.actionsComboBox1.RefreshResourceList();
            this.actionsComboBox1.Text = this.action.IdToken;
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.DeleteCurrentAction();
        }

        private void deleteAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
                "Are you sure you want to delete all Actions?", "Delete all Actions",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                this.Actions.Clear();
                this.needsSave = true;
            }
            this.charactersComboBox1_SelectedResourceChanged(this, EventArgs.Empty);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //saves character file
            this.saveXMLFileDialog.FileName = this.character.ResourceAsset.IdToken + ".xml";
            if (this.saveXMLFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.character.ResourceAsset.SaveToXml(this.saveXMLFileDialog.FileName);
                this.needsSave = false;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (this.needsSave)
            {
                DialogResult result = MessageBox.Show(
                    "File has not been saved!\nDo you want to save actions file now?",
                    "File Save Prompt", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Cancel)
                {
                    e.Cancel = true;
                    return;
                }
                if (result == DialogResult.Yes)
                {
                    this.saveToolStripMenuItem_Click(this, EventArgs.Empty);
                }
            }
            OrientGraphicsRealizer.Instance.Destroy();
            base.OnClosing(e);
        }

        private void actionControl1_TokenChanged(object sender, EventArgs e)
        {
            this.action = this.actionControl1.TokenObject;
            this.RefreshCharacterAction();
        }

        private void actionsComboBox1_SelectedResourceChanged(object sender, EventArgs e)
        {
            this.action = this.actionsComboBox1.SelectedResource;
            if (this.action != null)
                this.playPauseBtn.Enabled = this.trackBar1.Enabled = true;
            else this.playPauseBtn.Enabled = this.trackBar1.Enabled = false;
            this.RefreshCharacterAction();
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            float newTime = this.trackBar1.Value;
            this.actionControl1.TriggerTime = this.trackBar1.Value/1000.0f;
            float updateTime = newTime - this.oldTime;
            this.oldTime = newTime;
            OrientGraphicsRealizer.Instance.ManualUpdate(updateTime);
        }

        private void playPauseBtn_Click(object sender, EventArgs e)
        {
            this.trackBar1.Value = 0;
            this.character.Entity.GetAnimationState(
                ((AnimationAction) action).AnimationName).TimePosition = 0;
            if (this.playPauseBtn.Text == "Play")
                this.playPauseBtn.Text = "Pause";
            else
                this.playPauseBtn.Text = "Play";
            OrientGraphicsRealizer.Instance.Pause();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if ((this.character == null) ||
                (this.action == null) ||
                (this.playPauseBtn.Text == "Play")) return;

            float curTime = this.character.Entity.GetAnimationState(
                ((AnimationAction) action).AnimationName).TimePosition;
            this.trackBar1.Value = (int) curTime*1000;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox ab = new AboutBox();
            ab.FormClosed += ab_FormClosed;
            ab.Show();
            ab.Refresh();
            this.Enabled = false;
        }

        private void ab_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Enabled = true;
            this.Focus();
        }
    }
}