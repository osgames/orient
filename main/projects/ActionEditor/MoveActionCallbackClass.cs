using PAGE.Generic;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Resources.Actions;

namespace ActionEditor
{
    class MoveActionCallbackClass : ActionCallback
    {
        protected HumanLikeCharacter character = null;
        protected GenericSet set = null;
        protected string actionID;

        public MoveActionCallbackClass(HumanLikeCharacter character, GenericSet set,
            string actionID)
        {
            this.character = character;
            this.actionID = actionID;
            this.set = set;
            this.ignoreMultipleResponses = false;
        }

        protected override void Succeeded()
        {
            this.character.Place(null, this.set, "waypoint0");
            this.character.Move(this, this.actionID, this.set, "waypoint2");   
        }

        protected override void Failed()
        {
        }

        protected override void Triggered(ActionTrigger trigger)
        {
        }
    }
}
