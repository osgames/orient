using System;
using System.Windows.Forms;
using PAGE.Orient;

namespace CharacterEditor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            using (MainForm frm = new MainForm())
            {
                frm.Show();

                while (frm.Created)
                {
                    OrientGraphicsRealizer.Instance.Update();
                    Application.DoEvents();
                }
            }
        }
    }
}