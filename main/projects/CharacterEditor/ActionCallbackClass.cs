using PAGE.OGRE.Domain.Assets;
using PAGE.Generic;
using PAGE.Generic.Resources.Actions;

namespace CharacterEditor
{
    class ActionCallbackClass : ActionCallback
    {
        protected OgreCharacter character = null;
        protected string actionID;

        public ActionCallbackClass(OgreCharacter character, string actionID)
        {
            this.character = character;
            this.actionID = actionID;
        }

        protected override void Succeeded()
        {
            this.character.Animate(null, this.actionID);
            //TODO facial// this.character.ChangeFace("natural");
        }

        protected override void Failed()
        {
        }

        protected override void Triggered(ActionTrigger trigger)
        {
        }
    }
}
