/* ****************************************************
 * Name: MainForm.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/28 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Collections;
using System.Windows.Forms;
using Mogre;
using PAGE.Generic.Resources;
using PAGE.Generic.Resources.Actions;
using PAGE.Generic.Resources.Assets;
using PAGE.Generic.Resources.Spots;
using PAGE.OGRE.Domain.Assets;
using PAGE.OGRE.Domain.Spots;
using PAGE.OGRE.Math;
using PAGE.Orient;
using PAGE.Orient.Domain.Assets;
using PAGE.Orient.Domain.Cameras;
using PAGE.Orient.GUI.Windows.Forms.Forms;
using PAGE.Util;
using Math=System.Math;

namespace CharacterEditor
{
    public partial class MainForm : Form
    {
        private OrientCharacter character;
        private OrientCharacter testCharacter;
        private OgreSet set;
        private OgreItem testBall;
        private OgreItem testCube;
        private ResourceSpot curSpot;
        private bool needsSave;
        private float oldTime;
        private AnimationAction curAction;
        private SpotForm spotForm;

        private const string TEST_CHAR_ID = "spryte_adult_lightgrey";
        private const string TEST_ITEM_ID = "cube";

        private TokenObjectList<ResourceWaypoint> ResourceWaypoints
        {
            get { return this.character.ResourceAsset.ResourceWaypoints; }
        }

        private TokenObjectList<ResourceCameraSpot> ResourceCameraSpots
        {
            get { return this.character.ResourceAsset.ResourceCameraSpots; }
        }

        private TokenObjectList<ResourceItemSpot> ResourceItemSpots
        {
            get { return this.character.ResourceAsset.ResourceItemSpots; }
        }

        public MainForm()
        {
            InitializeComponent();

            this.FormClosing += Form1_FormClosing;

            OrientGraphicsRealizer.Instance.Setup("./data/resources.xml", this.panel1);

            this.spotForm = new SpotForm(this.spotListComboBox1);
            this.spotForm.Show();
            this.spotForm.spotControl1.SpotChanged += spotControl1_SpotChanged;
            this.spotForm.EditableSpotType = true;

            if (!OrientGraphicsRealizer.Instance.ResourcesManager.Characters.Contains(TEST_CHAR_ID))
            {
                MessageBox.Show("Can't continue. Character test is missing...",
                                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            if (!OrientGraphicsRealizer.Instance.ResourcesManager.Items.Contains(TEST_ITEM_ID))
            {
                MessageBox.Show("Can't continue. Item test is missing...",
                                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            string itemID = "testball" + DateTime.Now.Ticks;
            ResourceItem item = new ResourceItem(itemID,
                                                 OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
            item.FileName = "greenball.mesh";
            item.Scale = new OgreScale(0.04f, 0.04f, 0.04f);
            OrientGraphicsRealizer.Instance.ResourcesManager.Items.Add(item);
            this.testBall = (OgreItem) OrientGraphicsRealizer.Instance.LoadItem(itemID);
            this.testBall.Visible = false;

            this.testCube = (OgreItem) OrientGraphicsRealizer.Instance.LoadItem(TEST_ITEM_ID);
            this.testCube.Visible = false;
            this.testCube.Unplace();
            this.testCube.HideAllDebuggingSpots();
            this.testCube.Node.Scale(3, 3, 3);

            this.testCharacter = (OrientCharacter) OrientGraphicsRealizer.Instance.LoadCharacter(TEST_CHAR_ID);
            this.testCharacter.Visible = false;
            this.testCharacter.Unplace();
            //this.testCharacter.ChangeTexture("test-helper.jpg");
            this.testCharacter.ChangeFace("natural", 1);
            this.testCharacter.HideAllDebuggingSpots();

            foreach (ResourceAction action in this.testCharacter.ResourceAsset.Actions)
            {
                this.actionCBox.Items.Add(action.IdToken);
            }

            ResourceCameraSpot spot = new ResourceCameraSpot("camera-spot",
                                                             OrientGraphicsRealizer.Instance.ResourcesManager.
                                                                 ResourcesFactory);
            spot.RelativePosition = new OgrePosition(30, 20, 0);
            spot.Tags.Add("camera-spot");
            OgreCameraSpot camSpot = new OgreCameraSpot(spot, this.testCharacter);
            this.testCharacter.CameraSpots.Add(camSpot);
            OrientGraphicsRealizer.Instance.Camera.Place(null, this.testCharacter, "camera-spot", false);
            OrientGraphicsRealizer.Instance.Camera.FocusAsset(this.set);
            OrientGraphicsRealizer.Instance.Camera.CameraMode = CameraMode.Free;

            this.characterControl1.ResourcesManager = OrientGraphicsRealizer.Instance.ResourcesManager;

            this.set = (OgreSet) OrientGraphicsRealizer.Instance.LoadSet("CharTestSet");
            this.set.Visible = true;
            this.set.HideWaypoints();

            this.panel1.AllowNavigation = true;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.CheckNeedSave();
            this.spotForm.Dispose();
            OrientGraphicsRealizer.Instance.Destroy();
        }

        #region Menu Buttons

        #region File Buttons

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CheckNeedSave();

            if (this.openXMLFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = this.openXMLFileDialog.FileName;
                this.saveXMLFileDialog.FileName = fileName;

                ResourceHumanLikeCharacter resourceChar =
                    new ResourceHumanLikeCharacter("character0",
                                                   OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
                resourceChar.LoadFromXml(fileName);
                if (resourceChar.IdToken == null)
                {
                    MessageBox.Show("Invalid character file: " + fileName);
                    return;
                }
                OrientGraphicsRealizer.Instance.ResourcesManager.Characters.Remove(resourceChar.IdToken);
                OrientGraphicsRealizer.Instance.ResourcesManager.Characters.Add(resourceChar);
                this.CreateNewCharacter(resourceChar.IdToken);
            }
        }

        private void newCharToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CheckNeedSave();

            int numChar = 0;
            while (OrientGraphicsRealizer.Instance.ResourcesManager.Characters.Contains("character " + numChar))
            {
                numChar++;
            }
            ResourceHumanLikeCharacter resourceChar = new ResourceHumanLikeCharacter(
                "character " + numChar,
                OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
            ((ResourcesManager) OrientGraphicsRealizer.Instance.ResourcesManager).Characters.Add(resourceChar);

            this.CreateNewCharacter(resourceChar.IdToken);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //saves character file
            this.saveXMLFileDialog.FileName = this.character.ResourceAsset.IdToken + ".xml";
            if (this.saveXMLFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.character.ResourceAsset.SaveToXml(this.saveXMLFileDialog.FileName);
                this.needsSave = false;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Spots Buttons

        private void newSpotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CreateNewSpot();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.curSpot != null)
            {
                this.ResourceWaypoints.Remove(this.curSpot.IdToken);
                this.ResourceCameraSpots.Remove(this.curSpot.IdToken);
                this.ResourceItemSpots.Remove(this.curSpot.IdToken);
                this.curSpot = null;
                this.spotListComboBox1.RefreshSpotList();
                this.RefreshCurSpot();
            }
        }

        private void deleteAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to delete \n" +
                                                  "all the character's spot?", "Character Editor",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

            if (result == DialogResult.No) return;

            this.ResourceWaypoints.Clear();
            this.ResourceCameraSpots.Clear();
            this.ResourceItemSpots.Clear();

            this.curSpot = null;
            this.spotListComboBox1.RefreshSpotList();
            this.RefreshCurSpot();
        }

        private void directionFromCharToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.curSpot == null) return;
            Vector3 direction = this.character.Node.Position -
                                ((IOgreSpot) this.character.AllSpots[this.curSpot.IdToken][0]).Node.WorldPosition;
            direction.Normalise();
            this.curSpot.RelativeDirection = new OgreDirection(direction);
            this.spotForm.spotControl1.Spot = this.curSpot;
            this.RefreshCurSpot();
        }

        private void duplicateSpotsForAllCharactersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ArrayList characters = new ArrayList(OrientGraphicsRealizer.Instance.ResourcesManager.Characters);
            characters.Sort();

            //adds same spots to other characters
            foreach (ResourceHumanLikeCharacter c in characters)
            {
                if (c == this.character.ResourceAsset) continue;

                foreach (ResourceCameraSpot spot in this.ResourceCameraSpots)
                {
                    if (c.ResourceCameraSpots.Contains(spot.IdToken))
                    {
                        c.ResourceCameraSpots.Remove(spot.IdToken);
                    }
                    c.ResourceCameraSpots.Add(spot);
                }
                foreach (ResourceWaypoint spot in this.ResourceWaypoints)
                {
                    if (c.ResourceWaypoints.Contains(spot.IdToken))
                    {
                        c.ResourceWaypoints.Remove(spot.IdToken);
                    }
                    c.ResourceWaypoints.Add(spot);
                }
                this.saveXMLFileDialog.FileName = c.IdToken + ".xml";
                DialogResult result = this.saveXMLFileDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    c.SaveToXml(this.saveXMLFileDialog.FileName);
                }
            }
        }

        #endregion

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox ab = new AboutBox();
            ab.FormClosed += ab_FormClosed;
            ab.Show();
            ab.Refresh();
            this.Enabled = false;
        }

        private void ab_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Enabled = true;
            this.Focus();
        }

        #endregion

        #region Form Buttons

        private void playPauseBtn_Click(object sender, EventArgs e)
        {
            if (this.curAction == null) return;

            this.trackBar1.Value = 0;
            this.testCharacter.Entity.GetAnimationState(
                this.curAction.AnimationName).TimePosition = 0;
            this.testCharacter.Entity.GetAnimationState(
                this.curAction.AnimationName).AddTime(0);
            if (this.playPauseBtn.Text == "Play")
                this.playPauseBtn.Text = "Pause";
            else
                this.playPauseBtn.Text = "Play";
            OrientGraphicsRealizer.Instance.Pause();
        }

        private void screenshotBtn_Click(object sender, EventArgs e)
        {
            OrientGraphicsRealizer.Instance.SaveScreenshot("characterEditorScreenshot.png");
        }

        #endregion

        #region Private Methods

        private void CheckNeedSave()
        {
            if (this.needsSave)
            {
                DialogResult result = MessageBox.Show(
                    "Character file not saved!\nDo you want to save changes?", "Character Editor",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                else if (result == DialogResult.Yes)
                {
                    this.saveToolStripMenuItem_Click(this, EventArgs.Empty);
                }
            }
        }

        private void CreateNewCharacter(string idToken)
        {
            if (this.character != null)
            {
                OrientGraphicsRealizer.Instance.RemoveCharacter(this.character);
                this.character = (OrientCharacter) OrientGraphicsRealizer.Instance.LoadCharacter(idToken);
            }
            else
            {
                this.character = (OrientCharacter) OrientGraphicsRealizer.Instance.LoadCharacter(idToken);
            }
            this.character.Visible = true;
            this.character.HideAllDebuggingSpots();
            this.character.Animate(null, "idle-standing");
            this.testCharacter.Animate(null, "idle-standing");
            this.needsSave = true;
            this.characterControl1.TokenObject = this.character.ResourceAsset;
            this.characterControl1.GenericCharacter = this.character;
            this.RefreshCharacter();
        }

        private void CreateNewSpot()
        {
            int count = 0;
            while (this.ResourceWaypoints.Contains("spot " + count))
            {
                count++;
            }
            ResourceWaypoint spot = new ResourceWaypoint("spot " + count,
                                                         OrientGraphicsRealizer.Instance.ResourcesManager.
                                                             ResourcesFactory);
            spot.Tags.Add(spot.IdToken);
            this.ResourceWaypoints.Add(spot);
            this.character.Waypoints.Add(new OgreWaypoint(spot, this.character));
            this.spotListComboBox1.Asset = this.character.ResourceAsset;
            this.spotListComboBox1.Text = spot.IdToken;
            this.RefreshSpots();
        }

        #region Refreshes

        private void RefreshTestAssets()
        {
            if (this.curSpot == null)
            {
                this.previewGroupBox.Enabled = false;
                this.testCharacter.Visible = false;
                this.testBall.Visible = false;
                this.playPauseBtn.Enabled = false;
                this.trackBar1.Enabled = false;
                return;
            }

            if (this.ResourceCameraSpots.Contains(this.curSpot.IdToken))
            {
                this.previewGroupBox.Enabled = false;
                this.testCharacter.Visible = false;
                this.playPauseBtn.Enabled = false;
                this.trackBar1.Enabled = false;

                OrientGraphicsRealizer.Instance.Camera.Place(null, this.character, this.curSpot.IdToken, false);
                OrientGraphicsRealizer.Instance.Camera.FocusCharacterHeight(this.character);

                this.testBall.Unplace();
                this.testBall.Visible = false;
                this.testCube.Unplace();
                this.testCube.Visible = false;
            }
            else if (this.ResourceItemSpots.Contains(this.curSpot.IdToken))
            {
                this.previewGroupBox.Enabled = false;
                this.testCharacter.Visible = false;
                this.playPauseBtn.Enabled = false;
                this.trackBar1.Enabled = false;

                OrientGraphicsRealizer.Instance.Camera.Place(null, this.testCharacter, "camera-spot", false);
                OrientGraphicsRealizer.Instance.Camera.FocusItemHeight(this.testCube);

                this.testBall.Unplace();
                this.testBall.Place(null, this.character, this.curSpot.IdToken, false);
                this.testBall.Visible = true;

                this.testCube.Unplace();
                this.testCube.Place(null, this.character, this.curSpot.IdToken, false);
                this.testCube.Visible = true;
            }
            else
            {
                this.testCharacter.Unplace();
                this.testCharacter.Place(null, this.character, this.curSpot.IdToken, false);
                this.testCharacter.Visible = true;

                OrientGraphicsRealizer.Instance.Camera.Place(null, this.testCharacter, "camera-spot", false);
                OrientGraphicsRealizer.Instance.Camera.FocusCharacterHeight(this.testCharacter);

                this.previewGroupBox.Enabled = true;
                this.playPauseBtn.Enabled = true;
                this.trackBar1.Enabled = true;

                this.testBall.Unplace();
                this.testBall.Place(null, this.character, this.curSpot.IdToken, false);
                this.testBall.Visible = true;

                this.testCube.Unplace();
                this.testCube.Visible = false;
            }
        }

        private void RefreshCharacter()
        {
            if (this.character == null)
            {
                this.spotToolStripMenuItem.Enabled = false;
                this.characterControl1.Enabled = false;
                OrientGraphicsRealizer.Instance.Camera.FocusAsset(this.set);
            }
            else
            {
                this.spotToolStripMenuItem.Enabled = true;
                this.spotListComboBox1.Asset = this.character.ResourceAsset;
                this.characterControl1.Enabled = true;
                OrientGraphicsRealizer.Instance.Camera.FocusCharacterHeight(this.character);
            }
            this.RefreshSpots();
        }

        private void RefreshSpots()
        {
            if (this.character == null) return;

            if ((this.ResourceWaypoints.Count > 0) || (this.ResourceCameraSpots.Count > 0))
            {
                this.deleteToolStripMenuItem.Enabled = true;
                this.deleteAllToolStripMenuItem.Enabled = true;
                this.duplicateSpotsForAllCharactersToolStripMenuItem.Enabled = true;
            }
            else
            {
                this.deleteToolStripMenuItem.Enabled = false;
                this.deleteAllToolStripMenuItem.Enabled = false;
                this.duplicateSpotsForAllCharactersToolStripMenuItem.Enabled = false;
            }
        }

        private void RefreshCurSpot()
        {
            if (this.curSpot == null)
            {
                this.directionFromCharToolStripMenuItem.Enabled = false;
            }
            else
            {
                this.directionFromCharToolStripMenuItem.Enabled = true;

                this.character.Destroy();
                this.character.Reset();
                this.character.Visible = true;
            }

            this.RefreshTestAssets();
        }

        #endregion

        #endregion

        #region Form Control Events

        private void characterControl1_TokenChanged(object sender, EventArgs e)
        {
            this.CreateNewCharacter(this.character.ResourceAsset.IdToken);
            this.needsSave = true;
        }

        private void actionCBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string actionID = (string) this.actionCBox.SelectedItem;
            this.testCharacter.Animate(null, actionID);
            this.curAction = (AnimationAction) this.testCharacter.ResourceAsset.Actions[actionID];
            float maxTime = this.character.Entity.GetAnimationState(this.curAction.AnimationName).Length;
            this.trackBar1.Maximum = (int) Math.Ceiling(maxTime)*1000;
            this.character.Entity.GetAnimationState(this.curAction.AnimationName).TimePosition = 0;
            this.trackBar1.Value = 0;
            this.oldTime = 0;
        }

        private void spotListComboBox1_SelectedSpotChanged(object sender, EventArgs e)
        {
            this.curSpot = this.spotListComboBox1.SelectedSpot;
            this.RefreshCurSpot();
        }

        private void spotControl1_SpotChanged(object sender, EventArgs e)
        {
            this.curSpot = this.spotForm.spotControl1.Spot;
            this.RefreshCurSpot();
            this.needsSave = true;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            float newTime = this.trackBar1.Value;
            float updateTime = newTime - this.oldTime;
            this.oldTime = newTime;
            OrientGraphicsRealizer.Instance.ManualUpdate(updateTime);
        }

        #endregion
    }
}