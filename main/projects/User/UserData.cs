using System;

namespace User
{

	public class UserDataException : Exception
	{
		public UserDataException()
		{
		}
		public UserDataException(string errorMessage) : base(errorMessage)
		{
		}
		public UserDataException(string errorMessage, Exception innerException): base(errorMessage, innerException)
		{
		}
	}

	/// <summary>
	/// Summary description for UserData.
	/// </summary>
	public class UserData
	{
        //singleton instance
        public static UserData Instance = new UserData();
                
        string _userName;
		string _userCode;
		int _age;
		char _sex; //'X'- not set, 'M'-Male, 'F'-Female

		public UserData()
		{
			_userName = null;
			_userCode = null;
			_age = -1;
			_sex = 'X';
		}

		public void setUserName(string userName)
		{
            string aux = userName.Replace(":","-");

            string[] aux2 = aux.Split(" ".ToCharArray(),StringSplitOptions.RemoveEmptyEntries);
            
			_userName = aux2[0];
            
		}

		public void setUserCode(string userCode)
		{			
			if ((userCode.Length == 5) && (char.IsLetter(userCode,0)) && (char.IsLetter(userCode,1))  &&
				(char.IsNumber(userCode,2)) && (char.IsNumber(userCode,3)) && (char.IsNumber(userCode,4)))
			{
				_userCode = userCode;
			}else
				throw new UserDataException("Invalid user code, user code must be two letters followed by three numbers");
		}

		public void setAge(int age)
		{
			if ((age >= 8) && (age <= 13))
			{
				_age = age;
			}else
				throw new UserDataException("The age has to be between 8 and 13 years");
		}

		public void setSex(char sex)
		{
			if ((sex.Equals('M')) || (sex.Equals('F')))
				_sex = sex;
			else throw new UserDataException("Gender can only be M or F");
		}


		public string getUserName()
		{
			if (_userName == null)
				throw new UserDataException("Undefined user name");
			else
				return _userName;
		}

		public string getUserCode()
		{
			if (_userCode == null)
				throw new UserDataException("Undefined user code");
			else
				return _userCode;
		}

		public int getAge()
		{
			if (_age != -1)
				return _age;
			else
				throw new UserDataException("User age not defined");
		}

		public char getSex()
		{
			if (_sex != 'X')
				return _sex;
			else
				throw new UserDataException("User gender not defined");
		}
	}
}
