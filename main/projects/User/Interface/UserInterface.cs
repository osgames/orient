using System;
using System.Collections.Generic;
using System.Text;
using Mogre;
using ION.Core;
using AMS.Profile;
using ION.Core.Extensions;


namespace User.Interface
{

    // this class represents the user interface (i.e. on screen information displayed in response to the user's input)

    public class UserInterface
    {
        private Overlay overlay1 = null;
        private Overlay overlay2 = null;
        private Overlay overlay3 = null;
        private Overlay overlay4 = null;

        public static UserInterface Instance = new UserInterface();

        private TextAreaOverlayElement messageTxt = null;

        //panel for text message only
        private OverlayContainer panelMessageText = null;

        private TextAreaOverlayElement itemTxt = null;

        //panel to display item context
        private OverlayContainer panelItemContext = null;

        private TextAreaOverlayElement charTxt = null;

        //panel to display character context
        private OverlayContainer panelCharContext = null;

        private TextAreaOverlayElement gestureTxt = null;

        //panel to display gesture context
        private OverlayContainer panelGestureContext = null;

        //panel for animated pictures only
        private OverlayContainer panelPic = null;

        //panel for animate colour overlay for meteor attack
        private OverlayContainer panelMeteor = null;

        //panel to list the inventory items
        private OverlayContainer panelInventoryList = null;

        //busyButton
        private OverlayContainer panelBusy = null;
        private TextAreaOverlayElement busyTxt = null;

        //private TextAreaOverlayElement inventoryListTxt = null;

        //panel for location-context dependent messages (e.g. information about leaving the scene)
        private OverlayContainer panelContextInfoText = null;

        private TextAreaOverlayElement contextInfoTxt = null;

        private DateTime messageTextSent;
        private double messageDuration;

        // those counters are used to animate a highlighting of a fresh button press
        private ushort gestureTxtCounter = 0;
        private ushort charTxtCounter = 0;
        private ushort itemTxtCounter = 0;
        private uint busyCounter;

        private DateTime timeStep;
        private DateTime meteorStartTime = DateTime.Now;
        private bool animateMeteorFlag = false;

        private string languageCode = "EN";

        private List<string> inventoryList = new List<string>();

        // display a message for the amount of seconds specified 
        public void displayMessage(string msg_en, string msg_de, double duration)
        {
            lock (this)
            {
                messageTextSent = DateTime.Now;
                messageDuration = duration;
                if (languageCode.Equals("DE", StringComparison.CurrentCultureIgnoreCase))
                    messageTxt.Caption = msg_de;
                else
                    messageTxt.Caption = msg_en;
            
                //experiment: try how it looks if message is displayed in SF's narrator box instead:
                //if (Universe.Instance.HasEntity("NarratorEntity"))
                //{
                //    ION.Core.Entity narrator = Universe.Instance.GetEntity("NarratorEntity");
                //    Dictionary<string, object> actionArguments = new Dictionary<string, object>();
                //    actionArguments.Add("utterance", msg);
                //    narrator.GetAction("say").Start(new Arguments(actionArguments));                
                //}

            }
        }

        public void displayContextInfo(string info_en, string info_de)
        {
            if (languageCode.Equals("DE",StringComparison.CurrentCultureIgnoreCase))
                contextInfoTxt.Caption = info_de;
            else
                contextInfoTxt.Caption = info_en;
        }

        // clear all context
        public void clearContext()
        {
            itemTxt.Caption = "";
            charTxt.Caption = "";
            gestureTxt.Caption = "";
        }

        // update the item context
        public void updateItemContext(string newItemContext_en, string newItemContext_de)
        {
            if (languageCode.Equals("DE", StringComparison.CurrentCultureIgnoreCase))
                itemTxt.Caption = newItemContext_de;
            else
                itemTxt.Caption = newItemContext_en;
 
            itemTxtCounter = 11;
        }

        // update the character context
        public void updateCharacterContext(string newCharacterContext)
        {
            charTxt.Caption = newCharacterContext;
            charTxtCounter = 11;
        }

        public string getCharacterContext()
        {
            return charTxt.Caption;
        }

        // update the gesture context
        public void updateGestureContext(string newGestureContext_en, string newGestureContext_de)
        {
            if (languageCode.Equals("DE", StringComparison.CurrentCultureIgnoreCase))
                gestureTxt.Caption = newGestureContext_de;
            else
                gestureTxt.Caption = newGestureContext_en;

            gestureTxtCounter = 11;
        }

        public void animateMeteor()
        { 
            meteorStartTime = DateTime.Now;
            animateMeteorFlag = true;
        }


        // update the inventory list
        public void updateInventoryList(List<string> inventoryList_in)
        {
            inventoryList = inventoryList_in;
            updateInventoryList();
        }

        // update the inventory list
        private void updateInventoryList()
        {
         
            //empty the inventory
            overlay2.Clear();

            // fill up the 5 spaces in the inventory with either empty items or inventory items

            int y = 70;

            float w = 70f;
            float h = 70f;

            // iterate throught the 5 inventory slots
            for (int i = 0; i < 5; i++)
            {
                OverlayElement element1 = null;
                string item = null;

                // check if this slot is empty
                if (inventoryList.Count > i)
                {   
                    // not empty
                    item = inventoryList[i];
                    try
                    {
                       element1 = OverlayManager.Singleton.GetOverlayElement(item);
                    }
                    catch (Exception)
                    {
                        element1 = OverlayManager.Singleton.CreateOverlayElement("Panel", item);                    
                    }
                }
                else
                {   // empty

                    try
                    {
                        element1 = OverlayManager.Singleton.GetOverlayElement("inventorySlot" + i);
                    }
                    catch (Exception)
                    {
                        element1 = OverlayManager.Singleton.CreateOverlayElement("Panel", "inventorySlot" + i);
                    }
                }
                
                
                OverlayContainer panel1 = (OverlayContainer)element1;

                panel1.MetricsMode = GuiMetricsMode.GMM_PIXELS;
                panel1.SetPosition(10, y + 10 + i * 70);

                if (item == null)
                { 
                    panel1.MaterialName = "BegBackground";
                    panel1.GetMaterial().GetTechnique(0).GetPass(0).SetSceneBlending(SceneBlendType.SBT_TRANSPARENT_ALPHA);
                    panel1.GetMaterial().GetTechnique(0).GetPass(0).GetTextureUnitState(0).SetAlphaOperation(LayerBlendOperationEx.LBX_SOURCE1, LayerBlendSource.LBS_MANUAL, LayerBlendSource.LBS_CURRENT, 0.5f);
                } 
                else if (item.Contains("liquid"))
                {
                    panel1.MaterialName = "Liquid";
                    panel1.GetMaterial().GetTechnique(0).GetPass(0).SetSceneBlending(SceneBlendType.SBT_TRANSPARENT_ALPHA);
                    panel1.GetMaterial().GetTechnique(0).GetPass(0).GetTextureUnitState(0).SetAlphaOperation(LayerBlendOperationEx.LBX_SOURCE1, LayerBlendSource.LBS_MANUAL, LayerBlendSource.LBS_CURRENT, 0.5f);
                }
                else if (item.Contains("seedpod"))
                {
                    panel1.MaterialName = "Seedpod";
                    panel1.GetMaterial().GetTechnique(0).GetPass(0).SetSceneBlending(SceneBlendType.SBT_TRANSPARENT_ALPHA);
                    panel1.GetMaterial().GetTechnique(0).GetPass(0).GetTextureUnitState(0).SetAlphaOperation(LayerBlendOperationEx.LBX_SOURCE1, LayerBlendSource.LBS_MANUAL, LayerBlendSource.LBS_CURRENT, 0.5f);
                }
                else if (item.Contains("soil"))
                {
                    panel1.MaterialName = "SoilItem";
                    panel1.GetMaterial().GetTechnique(0).GetPass(0).SetSceneBlending(SceneBlendType.SBT_TRANSPARENT_ALPHA);
                    panel1.GetMaterial().GetTechnique(0).GetPass(0).GetTextureUnitState(0).SetAlphaOperation(LayerBlendOperationEx.LBX_SOURCE1, LayerBlendSource.LBS_MANUAL, LayerBlendSource.LBS_CURRENT, 0.5f);
                }
                else if (item.Contains("green"))
                {
                    panel1.MaterialName = "GreenDrinkItem";
                    panel1.GetMaterial().GetTechnique(0).GetPass(0).SetSceneBlending(SceneBlendType.SBT_TRANSPARENT_ALPHA);
                    panel1.GetMaterial().GetTechnique(0).GetPass(0).GetTextureUnitState(0).SetAlphaOperation(LayerBlendOperationEx.LBX_SOURCE1, LayerBlendSource.LBS_MANUAL, LayerBlendSource.LBS_CURRENT, 0.5f);
                }

                panel1.SetDimensions(w, h);
                overlay2.Add2D(panel1);
            }
        }

        public void update()
        {
            lock (this)
            {
                TimeSpan tmpTime = DateTime.Now.TimeOfDay - timeStep.TimeOfDay;
                double d2 = tmpTime.TotalMilliseconds;

                if (d2 > 200)
                {
                    // animate dots
                    switch (busyCounter % 3)
                    {
                        case 0: 
                            busyTxt.Caption = "busy .";
                            break;
                        case 1: 
                            busyTxt.Caption = "busy ..";
                            break;
                        case 2:
                            busyTxt.Caption = "busy ...";
                            break;
                    }

                    timeStep = DateTime.Now;

                    // animate colour changes
                    // animate forward
                    float modifier = 0f;
                    if (busyCounter % 10 < 5)
                        modifier = ((busyCounter % 5) / 4.0f) * 0.5f;                   
                    else // and back
                        modifier = ((4-(busyCounter % 5)) / 4.0f) * 0.5f;

                    ColourValue cb = new ColourValue(0.2f - modifier, 0.2f - modifier, 0.9f - modifier); //new ColourValue(0.9f - modifier, 0.9f - modifier, 0.9f - modifier);
                    busyTxt.ColourBottom = cb;
                    ColourValue ct = new ColourValue(0.9f - modifier, 0, 0);//new ColourValue(0.3f + modifier, 0.5f + modifier, 0.3f + modifier);
                    busyTxt.ColourTop = ct;

                    busyCounter++;

                }

                TimeSpan diff = DateTime.Now.TimeOfDay - messageTextSent.TimeOfDay;
                double d = diff.TotalMilliseconds;

                if (d >= messageDuration)
                {
                    messageTxt.Caption = "";
                }

                if (itemTxtCounter > 0)
                {
                    itemTxtCounter--;
                    setTextAppearance(itemTxt, itemTxtCounter);
                }

                if (gestureTxtCounter > 0)
                {
                    gestureTxtCounter--;
                    setTextAppearance(gestureTxt, gestureTxtCounter);
                }

                if (charTxtCounter > 0)
                {
                    charTxtCounter--;
                    setTextAppearance(charTxt, charTxtCounter);
                }

                // animate the meteor attack
                if (animateMeteorFlag)
                {
                    TimeSpan timeSinceStart = DateTime.Now.TimeOfDay - meteorStartTime.TimeOfDay;
                    float dur = (float) timeSinceStart.TotalMilliseconds;
                    if ((dur > 6000) && (dur <= 8000))
                    {
                        panelMeteor.Show();
                        overlay4.Show();
                        panelMeteor.GetMaterial().GetTechnique(0).GetPass(0).GetTextureUnitState(0).SetAlphaOperation(LayerBlendOperationEx.LBX_SOURCE1, LayerBlendSource.LBS_MANUAL, LayerBlendSource.LBS_CURRENT, (dur-6000)/2000 );                      
                    }
                    else if ((dur > 8000) && (dur <= 9000))
                    {
                        panelMeteor.GetMaterial().GetTechnique(0).GetPass(0).GetTextureUnitState(0).SetAlphaOperation(LayerBlendOperationEx.LBX_SOURCE1, LayerBlendSource.LBS_MANUAL, LayerBlendSource.LBS_CURRENT, 1);
                    }
                    else if ((dur > 9000) && (dur <= 12000))
                    {
                        panelMeteor.GetMaterial().GetTechnique(0).GetPass(0).GetTextureUnitState(0).SetAlphaOperation(LayerBlendOperationEx.LBX_SOURCE1, LayerBlendSource.LBS_MANUAL, LayerBlendSource.LBS_CURRENT, (12000-dur) / 3000);
                    }
                    else if (dur > 12000)
                    {
                        panelMeteor.GetMaterial().GetTechnique(0).GetPass(0).GetTextureUnitState(0).SetAlphaOperation(LayerBlendOperationEx.LBX_SOURCE1, LayerBlendSource.LBS_MANUAL, LayerBlendSource.LBS_CURRENT, 0);
                        animateMeteorFlag = false;
                        panelMeteor.Hide();
                        overlay4.Hide();
                        updateInventoryList();                     
                    }         
                }
                
            }

        }

        public void updateSystemButtontoBusy()
        {
            overlay3.Show();
        }

        public void updateSystemButtontoIdle()
        {
            overlay3.Hide();
        }

        public void loadLanguage()
        {
            //Read Orient's profile
            //AMS.Profile.dll -> http://www.codeproject.com/csharp/ReadWriteXmlIni.asp
            Xml profile = new Xml("OrientProfile.xml");
            try
            {
                using (profile.Buffer())
                {
                    if (profile.HasEntry("Application","Language"))
                        languageCode = (string)profile.GetValue("Application", "Language");
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.Instance().WriteLine("Error reading orient profile (UserInterface): " + ex.Message);
            }

        }

        public void initialise(uint screenWidth, uint screenHeight)
        {
            // load language
            loadLanguage();

            // init timer
            timeStep = DateTime.Now;

            // create overlays

            uint height = screenHeight;
            uint width = screenWidth;

            overlay1 = OverlayManager.Singleton.Create("over1");
            overlay2 = OverlayManager.Singleton.Create("over2");
            overlay3 = OverlayManager.Singleton.Create("over3");
            overlay4 = OverlayManager.Singleton.Create("over4");

            overlay4.ZOrder = 101;

            //beg
            OverlayElement element2 = OverlayManager.Singleton.CreateOverlayElement("Panel", "beg");
            OverlayContainer panel2 = (OverlayContainer)element2;
            panel2.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            panel2.SetPosition(10, 10);

            panel2.MaterialName = "Beg";
            panel2.GetMaterial().GetTechnique(0).GetPass(0).SetSceneBlending(SceneBlendType.SBT_TRANSPARENT_ALPHA);
            panel2.GetMaterial().GetTechnique(0).GetPass(0).GetTextureUnitState(0).SetAlphaOperation(LayerBlendOperationEx.LBX_SOURCE1, LayerBlendSource.LBS_MANUAL, LayerBlendSource.LBS_CURRENT, 0.5f);
            panel2.SetDimensions(70, 70);
            overlay1.Add2D(panel2);
            //beg end

            //meteor overlay
            panelMeteor = (OverlayContainer)OverlayManager.Singleton.CreateOverlayElement("Panel", "meteor"); 
            panelMeteor.MaterialName = "BegBackground";
            panelMeteor.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            panelMeteor.SetPosition(0, 0);
            panelMeteor.SetDimensions(width, height);
            panelMeteor.GetMaterial().GetTechnique(0).GetPass(0).SetSceneBlending(SceneBlendType.SBT_TRANSPARENT_ALPHA);
            panelMeteor.GetMaterial().GetTechnique(0).GetPass(0).GetTextureUnitState(0).SetAlphaOperation(LayerBlendOperationEx.LBX_SOURCE1, LayerBlendSource.LBS_MANUAL, LayerBlendSource.LBS_CURRENT, 0.0f);
            panelMeteor.Hide();
            overlay4.Add2D(panelMeteor);

            //busy button
            OverlayElement element3 = OverlayManager.Singleton.CreateOverlayElement("Panel", "busyButton");
            busyTxt = (TextAreaOverlayElement)OverlayManager.Singleton.CreateOverlayElement("TextArea", "textBusy");
            panelBusy = (OverlayContainer)element3;
            panelBusy.AddChild(busyTxt);
            busyTxt.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            busyTxt.SetPosition(width/2f, 150);
            busyTxt.SetDimensions(width / 4.0f, 30f);
            busyTxt.SetAlignment(TextAreaOverlayElement.Alignment.Left);
            setTextAppearance(busyTxt, 0);
            busyTxt.CharHeight = 60f;
            overlay3.Add2D(panelBusy);
            //busy b. end


            OverlayElement ele = OverlayManager.Singleton.CreateOverlayElement("Panel", "container:1");
            OverlayElement ele2 = OverlayManager.Singleton.CreateOverlayElement("Panel", "container:2");
            OverlayElement ele3 = OverlayManager.Singleton.CreateOverlayElement("Panel", "container:3");
            OverlayElement ele4 = OverlayManager.Singleton.CreateOverlayElement("Panel", "container:4");
            OverlayElement ele5 = OverlayManager.Singleton.CreateOverlayElement("Panel", "container:5");
            OverlayElement ele6 = OverlayManager.Singleton.CreateOverlayElement("Panel", "container:6");

            //new panelPic for text only
            OverlayElement elePic = OverlayManager.Singleton.CreateOverlayElement("Panel", "container:7");

            this.panelMessageText = (OverlayContainer)ele;
            this.panelCharContext = (OverlayContainer)ele2;
            this.panelItemContext = (OverlayContainer)ele3;
            this.panelGestureContext = (OverlayContainer)ele4;
            this.panelInventoryList = (OverlayContainer)ele5;
            this.panelContextInfoText = (OverlayContainer)ele5;
            this.panelPic = (OverlayContainer)elePic;

            panelPic.MetricsMode = GuiMetricsMode.GMM_PIXELS;

            //For the resolution of the screen in ogre.cfg set to be 1280x960
            panelPic.SetPosition(1066, 660);

            //For the resolution of the screen in ogre.cfg set to be 1280x800
            //panelPic.SetPosition(1066, 500);

            //panelPic.MaterialName = "blank-black";
            panelPic.SetDimensions(200, 200);

            // create Text areas
            this.messageTxt = (TextAreaOverlayElement)OverlayManager.Singleton.CreateOverlayElement("TextArea", "textMsg");
            this.itemTxt = (TextAreaOverlayElement)OverlayManager.Singleton.CreateOverlayElement("TextArea", "textItem");
            this.charTxt = (TextAreaOverlayElement)OverlayManager.Singleton.CreateOverlayElement("TextArea", "textChar");
            this.gestureTxt = (TextAreaOverlayElement)OverlayManager.Singleton.CreateOverlayElement("TextArea", "textGesture");
            this.contextInfoTxt = (TextAreaOverlayElement)OverlayManager.Singleton.CreateOverlayElement("TextArea", "textContextInfo");

            this.messageTxt.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            this.itemTxt.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            this.charTxt.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            this.gestureTxt.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            this.contextInfoTxt.MetricsMode = GuiMetricsMode.GMM_PIXELS;

            // position and size text elements

            this.messageTxt.SetPosition(width / 2f, height / 2f - 50f);
            this.messageTxt.SetDimensions(width - 10f, 60f);
            this.messageTxt.SetAlignment(TextAreaOverlayElement.Alignment.Center);

            this.charTxt.SetPosition(10f, height - 80.0f);
            this.charTxt.SetDimensions(width / 4.0f, 100f);
            this.charTxt.SetAlignment(TextAreaOverlayElement.Alignment.Left);

            this.gestureTxt.SetPosition(width / 2.0f, height - 80.0f);
            this.gestureTxt.SetDimensions(width / 4.0f, 100f);
            this.gestureTxt.SetAlignment(TextAreaOverlayElement.Alignment.Center);
            
            this.itemTxt.SetPosition(width - 10f, height - 80.0f);
            this.itemTxt.SetDimensions(width / 4.0f, 100f);
            this.itemTxt.SetAlignment(TextAreaOverlayElement.Alignment.Right);

            this.contextInfoTxt.SetPosition(width / 2f, height / 2f + 60f);
            this.contextInfoTxt.SetDimensions(width - 20f, 52f);
            this.contextInfoTxt.SetAlignment(TextAreaOverlayElement.Alignment.Center);

            overlay1.Add2D(this.panelMessageText);
            overlay1.Add2D(this.panelCharContext);
            overlay1.Add2D(this.panelItemContext);
            overlay1.Add2D(this.panelGestureContext);
            overlay1.Add2D(this.panelInventoryList);
            overlay1.Add2D(this.panelContextInfoText);
            overlay1.Add2D(this.panelPic);

            setTextAppearance(messageTxt, 0);
            setTextAppearance(itemTxt, 0);
            setTextAppearance(charTxt, 0);
            setTextAppearance(gestureTxt, 0);
            setTextAppearance(contextInfoTxt, -3);
            
            //the contextInfo message at the middle is too long for the screen resolution 1024x768, so I shorten it
            this.contextInfoTxt.CharHeight = 45;

            this.panelMessageText.AddChild(this.messageTxt);
            this.panelItemContext.AddChild(this.itemTxt);
            this.panelCharContext.AddChild(this.charTxt);
            this.panelGestureContext.AddChild(this.gestureTxt);
            this.panelContextInfoText.AddChild(this.contextInfoTxt);

            overlay1.Show();
            overlay2.Show();
            overlay3.Hide();
            overlay4.Hide();

            //panelPic.MaterialName = "Wii-move1";
            //panelPic.Show();
            panelPic.Hide();

            //initialise inventory for an empty inventory list
            updateInventoryList(new List<string>());
        }

        private void setTextAppearance(TextAreaOverlayElement txt, float modifier)
        {
            txt.CharHeight = 60 + 3f * modifier;
            txt.FontName = "Ogre";

            float colorMod = ((float)modifier) / 20f;

            ColourValue cb = new ColourValue(0.8f, 0.8f - colorMod, 0.8f - colorMod);
            txt.ColourBottom = cb;
            ColourValue ct = new ColourValue(1.0f, 1.0f - colorMod, 1.0f - colorMod); //new ColourValue(0.5f + colorMod, 0.7f - colorMod, 0.5f - colorMod);
            txt.ColourTop = ct;
        }



    }
}
