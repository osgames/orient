using System;
using System.Collections.Generic;
using System.Text;
using ION.Core.Events;
using ION.Core;

namespace User.Interface
{

    // monitors user interface and story events and sets camera focus accordingly 

    public class CameraController : IEventCallback<ActionStarted>
    {
        public static CameraController Instance = new CameraController();

        public void initialise()
        {
            Universe.Instance.CreateEventListener<ActionStarted>(this);
        }

        #region IEventCallback<ActionStarted> Members

        public void Invoke(ActionStarted evt)
        {
            //if the action is a user navigation action, move the first person camera
            if (evt.Action.Name.Equals("UserNavigationAction"))
            {
                string dir = (string)evt.Context["Direction"];
                moveFirstPersonCam(dir);
            }
            // if the user selects a character, focus it
            else if (evt.Action.Name.Equals("UserSpeechRecogAction"))
            {
                string charName = (string)evt.Context["CharacterName"];
                if (Universe.Instance.HasEntity(charName)) focusCameraOnCharacter(charName);
            }
            //if the action is inserting a greenDrink or soil 
            else if (evt.Action.Name.Equals("place-item"))
            {
                string objectName = evt.Source.Parent.Name;
                if (objectName.Equals("greenDrink") || objectName.Equals("soil") || objectName.Equals("soilOnGround"))
                   if (Universe.Instance.HasEntity(objectName)) 
                       focusCameraOnItem(objectName);
            }
            // if a character performs an important action (e.g. speak-action), focus on the character
            else if (evt.Action.Name.Equals("speak-action")
                || evt.Action.Name.Equals("AngryGesture")
                || evt.Action.Name.Equals("pick-floor-action")
                || evt.Action.Name.Equals("pick-table-action")
                || evt.Action.Name.Equals("pick-from-tree-action")
                || evt.Action.Name.Equals("Surprise")
                || evt.Action.Name.Equals("PointsAt")
                || evt.Action.Name.Equals("FallDown")
                || evt.Action.Name.Equals("GetUpFallen")
                || evt.Action.Name.Equals("press-button-action")
                || evt.Action.Name.Equals("handrise-action")
                || evt.Action.Name.Equals("nightfever - action")
                || evt.Action.Name.Equals("elbowstrike-action"))
            {
                focusCameraOnCharacter(evt.Action.Parent.Name);   
            }
            // if a character performs any action with the user as a target, focus on the character
            else if (evt.Context.Contains("target"))
            {
                string targetName = (string)evt.Context["target"];
                if (targetName.Equals("user")) focusCameraOnCharacter(evt.Action.Parent.Name);
            }    

        }

        private void focusCameraOnCharacter(string charName)
        {
            //focus camera on character body
            ION.Core.Entity camera;
            if (Universe.Instance.TryGetEntity("orient-camera", out camera))
            {
                Context camContext = new Context();
                camContext["character"] = charName;
                camContext["target"] = "face";
                Action focus = camera.GetAction("focus-character");
                if (!focus.IsRunning) focus.Start(camContext);
            }
        }

        private void focusCameraOnItem(string itemName)
        {
            //focus camera on item
            ION.Core.Entity camera;
            if (Universe.Instance.TryGetEntity("orient-camera", out camera))
            {
                Context camContext = new Context();
                camContext["target"] = itemName;
                Action focus = camera.GetAction("focus-asset");
                if (!focus.IsRunning) focus.Start(camContext);
            }
        }

        private void moveFirstPersonCam(string direction)
        {
            //move first person camera after user command
            ION.Core.Entity camera;
            if (Universe.Instance.TryGetEntity("orient-camera", out camera))
            {
                Context camContext = new Context();
                camContext["direction"] = direction;
                Action move1stPerson = camera.GetAction("move-1stperson");
                if (!move1stPerson.IsRunning) move1stPerson.Start(camContext);
            }
        }

        #endregion
    }
}
