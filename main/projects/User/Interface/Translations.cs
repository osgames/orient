using System;
using System.Collections.Generic;
using System.Text;

namespace User.Interface
{
    public class Translations
    {
        // SINGLETON
        private static Translations instance = null;

        public static Translations Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Translations();
                }
                return instance;
            }
        }

        public string translateGesture_de(string gestureName)
        {
            if (gestureName.Equals("GreetBack", StringComparison.CurrentCultureIgnoreCase))
                return "Gr��en";
            if (gestureName.Equals("EatDrink", StringComparison.CurrentCultureIgnoreCase))
                return "Essen/Trinken";
            if (gestureName.Equals("UserPick", StringComparison.CurrentCultureIgnoreCase))
                return "Nehmen";
            if (gestureName.Equals("Accept", StringComparison.CurrentCultureIgnoreCase))
                return "Zustimmen/Akzeptieren";
            if (gestureName.Equals("Reject", StringComparison.CurrentCultureIgnoreCase))
                return "Ablehnen";
            if (gestureName.Equals("GiveAttention", StringComparison.CurrentCultureIgnoreCase))
                return "Beachten";
            if (gestureName.Equals("Ask", StringComparison.CurrentCultureIgnoreCase))
                return "Fragen";
            if (gestureName.Equals("Apologise", StringComparison.CurrentCultureIgnoreCase))
                return "Entschuldigen";
            if (gestureName.Equals("Give", StringComparison.CurrentCultureIgnoreCase))
                return "Geben";
            if (gestureName.Equals("heal", StringComparison.CurrentCultureIgnoreCase))
                return "Heilen";

            return "";
        }

        public string translateItem_de(string itemName)
        {
            if (itemName.Equals("Seedpod", StringComparison.CurrentCultureIgnoreCase))
                return "Saatkorn";
            if (itemName.Equals("Soil", StringComparison.CurrentCultureIgnoreCase))
                return "Erde";
            if (itemName.Equals("greenDrink", StringComparison.CurrentCultureIgnoreCase))
                return "Getr�nk";
            if (itemName.Equals("Meal", StringComparison.CurrentCultureIgnoreCase))
                return "Essen";
            if (itemName.Equals("Gardening", StringComparison.CurrentCultureIgnoreCase))
                return "Gartenarbeit";
            if (itemName.Equals("Recycling", StringComparison.CurrentCultureIgnoreCase))
                return "Recycling";
            if (itemName.Equals("Drink", StringComparison.CurrentCultureIgnoreCase))
                return "Trinken";

            return "";
        }

        public string translateItem_deWithDefArticle(string itemName, int caseNo)
        {
            if (itemName.Equals("Seedpod", StringComparison.CurrentCultureIgnoreCase))
            {
               if (caseNo == 1) return "das Saatkorn";
               else if (caseNo == 2) return "des Saatkorns";
               else if (caseNo == 3) return "dem Saatkorn";
               else if (caseNo == 4) return "das Saatkorn";
            }
            if (itemName.Equals("Soil", StringComparison.CurrentCultureIgnoreCase))
            {
                if (caseNo == 1) return "die Erde";
                else if (caseNo == 2) return "der Erde";
                else if (caseNo == 3) return "der Erde";
                else if (caseNo == 4) return "die Erde";
            }
            if (itemName.Equals("greenDrink", StringComparison.CurrentCultureIgnoreCase))
            {
                if (caseNo == 1) return "das Getr�nk";
                else if (caseNo == 2) return "des Getr�nks";
                else if (caseNo == 3) return "dem Getr�nk";
                else if (caseNo == 4) return "das Getr�nk";
            }
            if (itemName.Equals("Meal", StringComparison.CurrentCultureIgnoreCase))
            {
                if (caseNo == 1) return "das Essen";
                else if (caseNo == 2) return "des Essens";
                else if (caseNo == 3) return "dem Essen";
                else if (caseNo == 4) return "das Essen";
            }
            if (itemName.Equals("Gardening", StringComparison.CurrentCultureIgnoreCase))
            {
                if (caseNo == 1) return "die Gartenarbeit";
                else if (caseNo == 2) return "der Gartenarbeit";
                else if (caseNo == 3) return "der Gartenarbeit";
                else if (caseNo == 4) return "die Gartenarbeit";
            }
            if (itemName.Equals("Recycling", StringComparison.CurrentCultureIgnoreCase))
            {
                if (caseNo == 1) return "das Recycling";
                else if (caseNo == 2) return "des Recyclings";
                else if (caseNo == 3) return "dem Recycling";
                else if (caseNo == 4) return "das Recycling";
            }
            if (itemName.Equals("Drink", StringComparison.CurrentCultureIgnoreCase))
            {
                if (caseNo == 1) return "das Trinken";
                else if (caseNo == 2) return "des Trinkens";
                else if (caseNo == 3) return "dem Trinken";
                else if (caseNo == 4) return "das Trinken";
            }

            return "";
        }

        public string translateItem_deWithIndefArticle(string itemName, int caseNo)
        {
            if (itemName.Equals("Seedpod", StringComparison.CurrentCultureIgnoreCase))
            {
                if (caseNo == 1) return "ein Saatkorn";
                else if (caseNo == 2) return "eines Saatkorns";
                else if (caseNo == 3) return "einem Saatkorn";
                else if (caseNo == 4) return "ein Saatkorn";
            }
            if (itemName.Equals("Soil", StringComparison.CurrentCultureIgnoreCase))
            {
                if (caseNo == 1) return "eine Erde";
                else if (caseNo == 2) return "einer Erde";
                else if (caseNo == 3) return "einer Erde";
                else if (caseNo == 4) return "eine Erde";
            }
            if (itemName.Equals("greenDrink", StringComparison.CurrentCultureIgnoreCase))
            {
                if (caseNo == 1) return "ein Getr�nk";
                else if (caseNo == 2) return "eines Getr�nks";
                else if (caseNo == 3) return "einem Getr�nk";
                else if (caseNo == 4) return "ein Getr�nk";
            }
            if (itemName.Equals("Meal", StringComparison.CurrentCultureIgnoreCase))
            {
                if (caseNo == 1) return "ein Essen";
                else if (caseNo == 2) return "eines Essens";
                else if (caseNo == 3) return "einem Essem";
                else if (caseNo == 4) return "ein Essen";
            } // the following ones dont even make sense but are added for completion, should normally never be needed
            if (itemName.Equals("Gardening", StringComparison.CurrentCultureIgnoreCase))
            {
                if (caseNo == 1) return "eine Gartenarbeit";
                else if (caseNo == 2) return "einer Gartenarbeit";
                else if (caseNo == 3) return "einer Gartenarbeit";
                else if (caseNo == 4) return "eine Gartenarbeit";
            }
            if (itemName.Equals("Recycling", StringComparison.CurrentCultureIgnoreCase))
            {
                if (caseNo == 1) return "ein Recycling";
                else if (caseNo == 2) return "eines Recyclings";
                else if (caseNo == 3) return "einem Recycling";
                else if (caseNo == 4) return "ein Recycling";
            }
            if (itemName.Equals("Drink", StringComparison.CurrentCultureIgnoreCase))
            {
                if (caseNo == 1) return "ein Trinken";
                else if (caseNo == 2) return "eines Trinkens";
                else if (caseNo == 3) return "einem Trinken";
                else if (caseNo == 4) return "eines Trinken";
            }

            return "";
        }

        public string translateItem_deWithNegArticle(string itemName, int caseNo)
        {
            // negative article in german is like the indefinitive one but plus a k at the front (e.g. ein - kein)
            string inDefString = translateItem_deWithIndefArticle(itemName, caseNo);
            if (!inDefString.Equals(""))
                inDefString = "k" + inDefString;
            return inDefString;
        }







    }
}
