/* ****************************************************
 * Name: AgentActionProperty.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/12
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace FAtiMA.Actions
{
    class AgentActionProperty
    {
        private string name;
        private string value;

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public string Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }
    }
}
