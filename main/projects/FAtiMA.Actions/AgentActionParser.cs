/* ****************************************************
 * Name: AgentActionParser.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/12
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml.Schema;
using System.Xml;

namespace FAtiMA.Actions
{
    /// <summary>
	/// Summary description for ActionParser
	/// </summary>
	public class AgentActionParser : XmlParser
	{
        ArrayList agentActionList = new ArrayList();
        ArrayList agentNonSpeechActList = new ArrayList();
        ArrayList agentSpeechActList = new ArrayList();

		class Singleton
		{
			internal static readonly AgentActionParser Instance = new AgentActionParser();

			// explicit static constructor to assure a single execution
			static Singleton()
			{
			}
		}

		AgentActionParser()
		{
		}

        public static AgentActionParser Instance
		{
			get
			{
				return Singleton.Instance;
			}
		}

		protected override void XmlErrorsHandler(object sender, ValidationEventArgs args) 
		{
			// TO DO: deal with xml errors
			Console.WriteLine("Validation error: " + args.Message);
		}

		protected override object ParseElements(XmlDocument xml)
	    {
            char[] delimiters = { '(', ',', ')' };
            AgentAction a;
            AgentActionProperty p;

            foreach (XmlNode node in xml.DocumentElement.ChildNodes)
            {
                if (node.Name.Equals("Action"))
                {
                    a = new AgentAction();
                    string [] temp = (node.Attributes["name"].InnerXml).Split(delimiters,StringSplitOptions.RemoveEmptyEntries);
                    a.Name = temp[0];

                    for (int i = 1; i < temp.Length; i++)
                    {
                        a.AddParameter(temp[i]);
                    }
                    foreach (XmlNode auxNode in node.ChildNodes)
                    {
                        if (auxNode.Name.Equals("Effects"))
                        {
                            foreach (XmlNode secAuxNode in auxNode.ChildNodes)
                            {
                                if(secAuxNode.Name.Equals("Effect")  && (Convert.ToDouble(secAuxNode.Attributes["probability"].InnerText)) == 1.0)
                                 
                                foreach (XmlNode thiAuxNode in secAuxNode.ChildNodes)
                                {
                                    if (thiAuxNode.Name.Equals("Property"))
                                    {
                                        p = new AgentActionProperty();
                                        p.Name = Convert.ToString(thiAuxNode.Attributes["name"].InnerText);
                                        p.Value = Convert.ToString(thiAuxNode.Attributes["value"].InnerText);
                                        a.AddProperty(p);
                                    }
                                }
                            }
                        }
                    }
                    if(a.Name.Equals("SpeechAct"))
                        agentSpeechActList.Add(a);
                    else
                        agentNonSpeechActList.Add(a);
                    agentActionList.Add(a);
                }
            }
            return agentActionList;
	    }

		protected override void ParseElements(XmlDocument xml,object result) 
		{
		}

        public ArrayList AgentActionList
        {
            get
            {
                return this.agentActionList;
            }
        }

        public ArrayList AgentNonSpeechActList
        {
            get
            {
                return this.agentNonSpeechActList;
            }
        }

        public ArrayList AgentSpeechActList
        {
            get
            {
                return this.agentSpeechActList;
            }
        }

    }
}
