/* ****************************************************
 * Name: AgentAction.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/12
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace FAtiMA.Actions
{
    class AgentAction
    {
        private string name;
        private List<string> parameters;
        private ArrayList properties;
        

        public AgentAction()
        {
            parameters = new List<string>();
            properties = new ArrayList();
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public ArrayList Properties
        {
            get
            {
                return this.properties;
            }
        }

        public void AddProperty(AgentActionProperty property)
        {
            this.properties.Add(property);
        }

        public List<String> Parameters
        {
            get
            {
                return this.parameters;
            }
        }

        public void AddParameter(String parameter)
        {
            this.parameters.Add(parameter);
        }

    }
}
