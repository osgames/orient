using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using ION.Core;
using ION.Core.Events;


namespace FAtiMA.Actions
{
    public class AgentActionEffectUpdater : IEventCallback<PropertyCreated<Property<string>>>
    {
        public static AgentActionEffectUpdater Instance = new AgentActionEffectUpdater();

        private ArrayList propertiesValuesToSet;

        public void initialise()
        {
            propertiesValuesToSet = new ArrayList();
        }

        public void UpdateActionEffectsUserStoryAction(Action source, Context context)
        {
            RemoteAction a = userStoryActionToRemoteAction(source, context);
            UpdateActionEffects(a);
        }

        public void UpdateActionEffectsBehaviour(Action source, Context context)
        {
            RemoteAction a = (RemoteAction)context["REMOTE_ACTION_PARAMETERS"];
            UpdateActionEffects(a);
        }

        private void UpdateActionEffects(RemoteAction a)
        {
            string propertyChange = "";
            bool actionExist = false;
            AgentAction agentAction = null;
            ArrayList agentSpeechActList = AgentActionParser.Instance.AgentSpeechActList;
            ArrayList agentNonSpeechActList = AgentActionParser.Instance.AgentNonSpeechActList;

            if (a.ActionType.Equals("SpeechAct"))
            {
                for (int i = 0; i < agentSpeechActList.Count && actionExist == false; i++)
                {
                    AgentAction action = (AgentAction)agentSpeechActList[i];
                    SpeechAct speechAction = a as SpeechAct;

                    if ((speechAction.Meaning.Equals(action.Parameters[1])))
                    {
                        actionExist = true;
                        agentAction = action;
                    }
                }
            }
            else
            {
                for (int i = 0; i < agentNonSpeechActList.Count && actionExist == false; i++)
                {
                    AgentAction action = (AgentAction)agentNonSpeechActList[i];
                    if (action.Name.Equals(a.ActionType))
                    {
                        actionExist = true;
                        agentAction = action;
                    }
                }
            }

            if (actionExist && agentAction != null)
            {
                if (agentAction.Properties.Count > 0)
                {
                    for (int j = 0; j < agentAction.Properties.Count; j++)
                    {
                        AgentActionProperty agentActionProperty = (AgentActionProperty)agentAction.Properties[j];
                        propertyChange = agentActionProperty.Name + agentActionProperty.Value;
                        propertyChange = propertyChange.Replace("[AGENT]", a.Subject.ToString());
                        propertyChange = propertyChange.Replace(agentAction.Parameters[0], a.Target);

                        if ((a.Parameters.Count + 1) == agentAction.Parameters.Count)
                        {
                            for (int k = 0; k < a.Parameters.Count; k++)
                            {
                                propertyChange = propertyChange.Replace(agentAction.Parameters[k + 1], a.Parameters[k]);
                            }
                        }
                        propertyChange = propertyChange.Replace('(', ' ');
                        propertyChange = propertyChange.Replace(')', ' ');
                        //Send(PROPERTY_CHANGED + " " +  propertyChange);
                        updateIONProperty(propertyChange);
                    }
                }
            }
        }

        private void updateIONProperty(string propertyDescription)
        {
            // split the property description string into 3 elements separated by spaces: entity property value
            string[] elements = propertyDescription.Split(' ');
            if (elements.Length == 3 && Universe.Instance.HasEntity(elements[0]))
            {
                Entity entity = Universe.Instance.GetEntity(elements[0]);
                if (entity.HasProperty<Property<string>>(elements[1])) // property already exists
                {
                    entity.GetProperty<Property<string>>(elements[1]).Value = elements[2];
                }
                else // need to create property
                {
                    // register an event listener so that when the property is there, we will be notified
                    entity.CreateEventListener<PropertyCreated<Property<string>>>(this);
                    // create the property
                    entity.CreateProperty<Property<string>>(elements[1]);

                    propertiesValuesToSet.Add(elements);
                }
            }

        }

        private RemoteAction userStoryActionToRemoteAction(Action action, Context context)
        {
            RemoteAction rmAction = new RemoteAction();
            rmAction.Subject = "user";
            rmAction.ActionType = action.Name;
            if (context.Contains("target"))
                rmAction.Target = (string)context["target"];

            if (context.Contains("parameters"))
            {
                string parameters = (string)context["parameters"];
                foreach (string parameter in parameters.Split(' '))
                {
                    rmAction.Parameters.Add(parameter);
                }
            }
            return rmAction;
        }

        #region IEventCallback<PropertyCreated<Property<string>>> Members

        public void Invoke(PropertyCreated<Property<string>> evt)
        {
            //iterate through properties to change and see if a matching one is found
            for (int i=0; i < propertiesValuesToSet.Count; i++)
            {
                string[] elements = (string[]) propertiesValuesToSet[i];
                if (evt.Element.Parent.Name.Equals(elements[0]) && evt.Element.Name.Equals(elements[1]))
                {
                    // set property value
                    (evt.Element as Property<string>).Value = elements[2];

                    // remove current entry from properties Values to set list
                    propertiesValuesToSet.RemoveAt(i);

                    // get out of this for loop
                    break;
                }     
            }
        }

        #endregion
    }
}
