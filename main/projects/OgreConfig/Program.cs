using Mogre;

namespace OgreConfig
{
    class Program
    {
        static void Main(string[] args)
        {
            new Root().ShowConfigDialog();
        }
    }
}
