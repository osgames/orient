using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;

namespace FAtiMA.RemoteAgent
{
    public class LoadLanguageEngine : Action
    {
        public const int TIME_OUT = 20;
        public const string NAME = "LoadLanguageEngine";

        private LanguageEngineMaster proxy;
        private ulong initialTick;
        

        protected override void OnStart(Context context)
        {
            this.initialTick = Universe.Instance.Tick;
            this.proxy = (LanguageEngineMaster)this.Parent;

            int connectionPort = this.proxy.StartMaster();
            //once the Server is on, we can launch the java proccess that corresponds to the slave

            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = "jre\\bin\\java.exe";
            proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            proc.StartInfo.UseShellExecute = true;

            //port, agentSex, userSex, agentLanguage, userLanguage
            proc.StartInfo.Arguments = "-cp \"LanguageServer.jar;Language.jar\" " +
                "LanguageServerSlave " +
                connectionPort + " " +
                this.proxy.AgentSex + " " +
                this.proxy.UserSex + " " +
                this.proxy.AgentLanguageFile + " " +
                this.proxy.UserLanguageFile;

            proc.Start();
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
            throw new Exception("It was not possible to load the LanguageEngine: please check if the language server and language files "
                + this.proxy.AgentLanguageFile + " and " + this.proxy.UserLanguageFile + " are available");
        }

        protected override void OnStep(Context context)
        {
            this.proxy.ConnectToSlave();
            if (this.proxy.IsReady)
            {
                End();
            }
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
