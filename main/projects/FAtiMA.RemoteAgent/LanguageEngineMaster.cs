using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using ION.Core;
using ION.Core.Extensions;
using FAtiMA.Actions;

namespace FAtiMA.RemoteAgent
{
    public class LanguageEngineMaster : Entity
    {
        public const string AGENT_LANGUAGE = "AgentLanguageEngine";
        public const string USER_LANGUAGE = "UserLanguageEngine";
        public const string LANGUAGE_ENGINE = "LanguageEngine";
        public const string AGENT_SEX = "AgentSex";
        public const string USER_SEX = "UserSex";

        public const string SAY_REQUEST = "Say";
        public const string INPUT_REQUEST = "Input";
        public const string NARRATE_REQUEST = "Narrate";
        public const string KILL_REQUEST = "Kill";

        public const int PORT = 45500;

        private Socket master;
        private Socket slave;

        private string agentSex;
        private string userSex;
        private string agentLanguageFile;
        private string userLanguageFile;

        public LanguageEngineMaster()
        {
            master = null;
            slave = null;
        }

        ~LanguageEngineMaster()
        {           
            this.Close();
        }

        public string AgentSex
        {
            get
            {
                return this.agentSex;
            }
        }

        public string UserSex
        {
            get
            {
                return this.userSex;
            }
        }

        public string AgentLanguageFile
        {
            get
            {
                return this.agentLanguageFile;
            }
        }

        public string UserLanguageFile
        {
            get
            {
                return this.userLanguageFile;
            }
        }

        public bool IsReady
        {
            get
            {
                return this.slave != null;
            }
        }

        protected override void OnCreate(Arguments arguments)
        {
            base.OnCreate(arguments);

            if (arguments == null ||
                !arguments.ContainsKey(AGENT_LANGUAGE) ||
                !arguments.ContainsKey(AGENT_SEX) ||
                !arguments.ContainsKey(USER_LANGUAGE) ||
                !arguments.ContainsKey(USER_SEX))
            {
                return;
            }

            this.agentSex = (string)arguments[AGENT_SEX];
            this.userSex = (string)arguments[USER_SEX];
            this.agentLanguageFile = (string)arguments[AGENT_LANGUAGE];
            this.userLanguageFile = (string)arguments[USER_LANGUAGE];

            LoadLanguageEngine load = this.CreateAction<LoadLanguageEngine>(LoadLanguageEngine.NAME);
            load.Start();
        }

        public int StartMaster()
        {
            // Establish local endpoint...
            IPAddress ipAddress = IPAddress.Any;            
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress,PORT);
            
            // Create a TCP/IP socket...
            this.master = new Socket(AddressFamily.Unspecified, SocketType.Stream, ProtocolType.Tcp);
            // Bind the socket...
            this.master.Bind(localEndPoint);

            this.master.Listen(5);
            
            return localEndPoint.Port;
        }

        public void ConnectToSlave()
        {
            this.master.Blocking = false;
            try
            {
                this.slave = this.master.Accept();
                this.slave.Blocking = true;
            }
            catch (SocketException)
            {
                //ignore
            }

            this.master.Blocking = true;
        }

        public string Say(SpeechAct speech)
        {
            string utterance = null;
            string[] aux;
            string result = processLanguageRequest(SAY_REQUEST, speech.toLanguageEngine());

            if (result != null)
            {
                string[] delimiters = new string[2];
                delimiters[0] = "<Utterance>";
                delimiters[1] = "</Utterance";
                aux = result.Split(delimiters,StringSplitOptions.None);
                if (aux.GetUpperBound(0) > 0)
                {
                    utterance = aux[1];
                }
                else
                {
                    utterance = "...";
                }
                //utterance = result.Split("<Utterance>".ToCharArray())[1].Split("</Utterance".ToCharArray())[0];
            }
            else
            {
                utterance = "...";
            }

            return utterance;
        }

        public SpeechAct Input(string input)
        {
            SpeechAct newSpeech;
            string result = processLanguageRequest(INPUT_REQUEST, input);

            if (result != null)
            {
                newSpeech = (SpeechAct)SpeechActParser.Instance.Parse(result);
                return newSpeech;
            }
            else
            {
                return null;
            }
        }

        public string Narrate(String amSummary)
        {
            string result = processLanguageRequest(NARRATE_REQUEST, amSummary);

            if (result != null)
            {
                string[] delimiters = new string[2];
                delimiters[0] = "<Summary>";
                delimiters[1] = "</Summary";
                string[] aux2 = result.Split(delimiters,StringSplitOptions.None);
                if (aux2.Length > 1)
                {
                    return aux2[1];
                }
                else
                {
                    return "";
                }
            }
            return "";
        }

        private string processLanguageRequest(string method, string speechAct)
        {
            string answer = null;
            try
            {
                Send(method + " " + speechAct);

                NetworkStream socketStream = new NetworkStream(this.slave);
                //receiving answer to request

                StreamReader reader = new StreamReader(socketStream, Encoding.UTF8);
                answer = reader.ReadLine();
            }
            catch (Exception e)
            {
                throw new Exception("The application cannot continue. A critical error with the LanguageServer occurred: " + e.Message);
            }

            return answer;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            this.Close();
        }

        private void Send(String message)
        {
            //sending request
            NetworkStream socketStream = new NetworkStream(this.slave);
            byte[] aux = Encoding.UTF8.GetBytes(message + "\n");
            socketStream.Write(aux, 0, aux.Length);
            socketStream.Flush();
        }

        public void Close()
        {
            if (this.slave != null)
            {
                Send(KILL_REQUEST);
                this.slave.Close();
                this.slave = null;
            }
            if (this.master != null)
            {
                this.master.Close();
                this.master = null;
            }
            
        }
    }
}
