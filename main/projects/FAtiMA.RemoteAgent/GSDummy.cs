using System;
using ION.Core;
using ION.Core.Extensions;
using ION.Realizer.Orient.Actions.Body.Behaviours;
using FAtiMA.Actions;

namespace FAtiMA.RemoteAgent
{

    #region DummyAction
    public class DummyAction : Behaviour
    {
        protected ulong initialTick = 0;

        protected override void OnStart(Context context)
        {
            Console.Write("OnStart " + this.Name);
            
            /*if (context.Arguments != null)
            {
                foreach (string argument in context.Arguments.Keys)
                {
                    Console.Write(" " + argument);
                }
            }*/
            
            Console.WriteLine();

            this.initialTick = Universe.Instance.Tick;
        }

        protected bool HasEnded()
        {
            return (Universe.Instance.Tick - this.initialTick > 5);
        }

        protected override void OnStep(Context context)
        {
            if (HasEnded())
            {
                this.End();
            }
        }

        protected override void OnEnd(Context context)
        {            
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
    #endregion

    #region DummySayAction
    public class DummySayAction : DummyAction
    {
        protected override void OnStart(Context context)
        {
            if (!context.Arguments.ContainsKey(RemoteCharacter.REMOTE_ACTION_PARAMETERS))
            {
                return;
            }
            
            SpeechAct speech = (SpeechAct)context.Arguments[RemoteCharacter.REMOTE_ACTION_PARAMETERS];
            Console.WriteLine(speech.Subject + " says to " + speech.Target + ": " + speech.Utterance);

            this.initialTick = Universe.Instance.Tick;
        }
    }

    public class DummySaySimpleAction : DummyAction
    {

        protected override void OnStart(Context context)
        {
            if (!context.Arguments.ContainsKey(RemoteCharacter.REMOTE_ACTION_PARAMETERS))
            {
                ApplicationLogger.Instance().WriteLine("ERROR: Action DummySaySimpleAction: No parameters (Ignored)");
                return;
            }


            RemoteAction remoteAction = (RemoteAction)context.Arguments[RemoteCharacter.REMOTE_ACTION_PARAMETERS];
            string speechLine = string.Empty;

            foreach (string str in remoteAction.Parameters)
            {
                speechLine += str + " ";
            }
            speechLine = speechLine.TrimEnd();
            Console.WriteLine("saySimple:" + remoteAction.Subject + ":" + speechLine);            
            
            this.initialTick = Universe.Instance.Tick;
        }


    }
    
    #endregion

    #region DummyWalkToAction
    public class DummyWalkToAction : DummyAction
    {
        string subject = null;
        string target=null;
        string iSpot=null;

        protected override void OnStart(Context context)
        {
            string displayMsg = "";

            if (!context.Arguments.ContainsKey(RemoteCharacter.REMOTE_ACTION_PARAMETERS))
            {
                return;
            }

            RemoteAction action = (RemoteAction)context.Arguments[RemoteCharacter.REMOTE_ACTION_PARAMETERS];
            subject = action.Subject;
            target = action.Target;

            displayMsg = subject + " walks to " + target; 

            if (action.Parameters.Count > 0)
            {
                iSpot = action.Parameters[0];
                displayMsg += "'s " + iSpot;
            }
            Console.WriteLine(displayMsg);

            this.initialTick = Universe.Instance.Tick;
        }
    }
    #endregion

    #region DummyAttackAction
    public class DummyAttackAction : DummyAction
    {
        string subject = null;
        Entity target = null;
     
        //List<string> attacks = new ({ "punches", "kicks", "pushes" });

        protected override void OnStart(Context context)
        {
            string displayMsg = "";

            if (!context.Arguments.ContainsKey(RemoteCharacter.REMOTE_ACTION_PARAMETERS))
            {
                return;
            }

            RemoteAction action = (RemoteAction)context.Arguments[RemoteCharacter.REMOTE_ACTION_PARAMETERS];
            subject = action.Subject;
            string auxTarget = action.Target;
            
            target = Universe.Instance.GetEntity<Entity>(auxTarget);

            displayMsg = subject + " attacks " + auxTarget;

            Console.WriteLine(displayMsg);
               
            this.initialTick = Universe.Instance.Tick;
            
        }
    
        protected override void OnEnd(Context context)
        {
            Property<string> hurt;
            Property<string> targetStrength;
            int str;
            
            if(target == null)
            {
                return;
            }


            targetStrength = target.GetProperty<Property<string>>("strength");
            if (targetStrength == null)
            {
                str = 5;
            }
            else
            {
                str = Int16.Parse(targetStrength.Value);
            }

            Random D10 = new Random();
            int rool = D10.Next(1, 10);
            if (rool >= str)
            {
                hurt = target.GetProperty<Property<string>>("hurt");
                if (hurt == null)
                {
                    hurt = target.CreateProperty<Property<string>>("hurt");
                }

                hurt.Value = "True";
            }
        }
    }

    #endregion

    #region DummyStealAction
    public class DummyStealAction : DummyAction
    {
        Entity subject = null;
        String item = null;

        protected override void OnStart(Context context)
        {
            string displayMsg = "";

            if (!context.Arguments.ContainsKey(RemoteCharacter.REMOTE_ACTION_PARAMETERS))
            {
                return;
            }

            RemoteAction action = (RemoteAction)context.Arguments[RemoteCharacter.REMOTE_ACTION_PARAMETERS];
            subject = Universe.Instance.GetEntity<Entity>(action.Subject);
            item = action.Parameters[0];

            displayMsg = action.Subject + " steals " + action.Target + "'s " + item;

            Console.WriteLine(displayMsg);

            this.initialTick = Universe.Instance.Tick;

        }
    }

    #endregion

        public class Character : ION.Realizer.Orient.Entities.Character
        {
            public Character()
            {
                //this.CreateAction<DummySayAction>("say-to");
                //this.CreateAction<DummySaySimpleAction>("say");
                //this.CreateAction<DummyAction>("cry");
                //this.CreateAction<DummyAction>("runAway");
                //this.CreateAction<DummyAction>("walkAway");

                this.CreateAction<DummyStealAction>("steal");
                this.CreateAction<DummyAction>("poke");
                this.CreateAction<DummyAction>("throw");
                //this.CreateAction<DummyAttackAction>("attack");
                this.CreateAction<DummyAction>("swipe");
                this.CreateAction<DummyAction>("pick-from-floor");                
            }
        }
    
}
