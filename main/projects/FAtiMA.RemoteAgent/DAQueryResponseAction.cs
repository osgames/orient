using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;

namespace FAtiMA.RemoteAgent
{
    public class DAQueryResponseAction : Action
    {
        public const string ACTION_REPRESENTATION = "ACTION_REP";
        public const string TRIGGER_ID = "TRIGGER_ID";
        public const string APPRAISAL_VALUE = "APPRAISAL_VAL";

        protected override void OnStart(Context context)
        {
            
        }

        protected override void OnEnd(Context context)
        {
            
        }

        protected override void OnFail(Context context)
        {
            
        }

        protected override void OnStep(Context context)
        {
            End();
        }

        protected override void OnCreate(Arguments arguments)
        {
            
        }

        protected override void OnDestroy()
        {
            
        }

        public static string ACTION_NAME
        {
            get { return "DAQueryActionResponse"; }
        }
    }
}
