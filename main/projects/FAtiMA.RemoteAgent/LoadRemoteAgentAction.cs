using System;
using System.IO;
using System.Text;
using System.Net.Sockets;
using AMS.Profile;
using ION.Core;
using System.Diagnostics;
using System.Net;
using ION.Core.Extensions;

namespace FAtiMA.RemoteAgent
{
    public class LoadRemoteAgentAction : Action
    {
        public enum LoadMode {CREATENEW,RESTORE};

        public const string DEBUG_MODE = "DebugMode";
        public const string LANGUAGE_ACTS = "LanguageActs";
        public const string USER_LANGUAGE_ACTS = "UserLanguageActs";
        public const string SEX = "sex";
        public const string ROLE = "role";
        public const string CULTURE = "culture";
        public const string DISPLAY_NAME = "DisplayName";
        public const string USER_DIRECTORY = "UserDirectory";
        public const string LOAD_MODE = "LoadMode";

        //This parameter is not used in the action
        //It is here just so the SF can get it from the RunningContext of the action
        public const string NAME = "Name";
        public const string ACTION_NAME = "LoadRemoteAgent";

        protected const string EOM_TAG = "\n";
        protected const string EOF_TAG = "<EOF>";
        //only one connection at a time
        protected const int PENDING_CONNECTION_QUEUE_LENGTH = 10;
        protected const int INITIAL_SERVER_PORT = 46874;
        protected const int SERVER_PORT_RANGE = 100;
        protected const int bufferSize = 1024;

        protected Socket serverSocket;
        protected byte[] buffer = new byte[bufferSize];
        protected static int currentServerPort = INITIAL_SERVER_PORT;

        protected RemoteCharacter rc;

        protected override void OnStart(Context context)
        {
            this.rc = (RemoteCharacter) this.Parent;
            if (context.Arguments == null ||
                !context.Arguments.ContainsKey(LOAD_MODE) ||
                !context.Arguments.ContainsKey(DEBUG_MODE) ||
                !context.Arguments.ContainsKey(DISPLAY_NAME) ||
                !context.Arguments.ContainsKey(LANGUAGE_ACTS) ||
                !context.Arguments.ContainsKey(USER_LANGUAGE_ACTS) ||
                !context.Arguments.ContainsKey(USER_DIRECTORY) ||
                !this.rc.HasProperty<Property<string>>(SEX) ||
                !this.rc.HasProperty<Property<string>>(ROLE) ||
                !this.rc.HasProperty<Property<string>>(CULTURE))
            {
                throw new Exception("LoadRemoteAgent.OnStart -> Properties that are needed are not defined");
            }

            bool debugMode = (bool)context.Arguments[DEBUG_MODE];
            LoadMode mode = (LoadMode)context.Arguments[LOAD_MODE];
            string languageActs = (string)context.Arguments[LANGUAGE_ACTS];
            string userLanguageActs = (string)context.Arguments[USER_LANGUAGE_ACTS];
            string displayName = (string)context.Arguments[DISPLAY_NAME];
            string userDirectory = (string)context.Arguments[USER_DIRECTORY];
            string sex = rc.GetProperty<Property<string>>(SEX).Value;
            string role = rc.GetProperty<Property<string>>(ROLE).Value;
            string culture = rc.GetProperty<Property<string>>(CULTURE).Value;

            //everything is ready and ok, 1.� step - we start the ServerSocket in order to wait for the connection
            currentServerPort++;
            if (currentServerPort == INITIAL_SERVER_PORT + SERVER_PORT_RANGE)
            {
                currentServerPort = INITIAL_SERVER_PORT;
            }
            StartServer(currentServerPort);
            //once the Server is on, we can launch the java proccess

            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = "jre\\bin\\java.exe";
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.StartInfo.UseShellExecute = true;

            string fatimaAgentActionsFile ="";
            string fatimaAgentGoalsFile="";
            string useRemoteAgentLauncher = "false";
            string orientIPAddress = null;
            string remoteAgentLauncherIPAddress = null;
            string remoteAgentLauncherPort = null;
            //Read Orient's profile
            //AMS.Profile.dll -> http://www.codeproject.com/csharp/ReadWriteXmlIni.asp
            Xml profile = new Xml("OrientProfile.xml");
            try
            {
                using (profile.Buffer())
                {
                    useRemoteAgentLauncher = (string)profile.GetValue("Application", "UseRemoteAgentLauncher");
                    orientIPAddress = (string)profile.GetValue("Application", "OrientIP");
                    remoteAgentLauncherIPAddress = (string)profile.GetValue("Application", "RemoteAgentLaucherServerIP");
                    remoteAgentLauncherPort = (string)profile.GetValue("Application", "RemoteAgentLaucherServerPort");
                    fatimaAgentActionsFile = (string)profile.GetValue("Application", "FatimaAgentActionsFile");
                    fatimaAgentGoalsFile = (string)profile.GetValue("Application", "FatimaAgentGoalsFile");
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.Instance().WriteLine("Error reading orient profile (InsertCharacterAction): " + ex.Message);
            }

            if (mode == LoadMode.RESTORE && System.IO.File.Exists(userDirectory + "/" + this.rc.Name))
            {
                proc.StartInfo.Arguments = //"-cp \"FAtiMA.jar;Language.jar\" " +
                //"FAtiMA.Agent " +
                "ion " +
                orientIPAddress + " " +
                currentServerPort + " " +
                userDirectory + " " +
                this.rc.Name;
            }
            else
            {
                proc.StartInfo.Arguments = //"-cp \"FAtiMA.jar;Language.jar\" " +
                    //"FAtiMA.Agent " +
                    "ion " +
                    orientIPAddress + " " +
                    currentServerPort + " " +
                    userDirectory + " " +
                    this.rc.Name + " " +
                    languageActs + " " +
                    userLanguageActs + " " +
                    sex + " " +
                    role + " " +
                    displayName + " " +
                    debugMode + " " +
                    fatimaAgentActionsFile + " " +
                    fatimaAgentGoalsFile + " " +
                    culture;
            }
            
            if (useRemoteAgentLauncher == "false")
            {
                proc.Start();
            }
            else
            {                
                int agentLauncherServerPort = Convert.ToInt32(remoteAgentLauncherPort);

                TcpClient socketServer;
                try
                {
                    if (remoteAgentLauncherIPAddress.IndexOf(".") != -1)
                    {
                        //IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Parse(remoteAgentLauncherIPAddress), agentLauncherServerPort);
                        //socketServer = new TcpClient(ipEndPoint);
                        socketServer = new TcpClient();
                        socketServer.Connect(remoteAgentLauncherIPAddress, agentLauncherServerPort);
                    }
                    else
                    {
                        socketServer = new TcpClient(remoteAgentLauncherIPAddress, agentLauncherServerPort);
                    }
                }
                catch(Exception ex)
                {
                    ApplicationLogger.Instance().WriteLine(String.Format("Failed to connect to RemoteAgentLaucherServer at {0}:{1} -> ", remoteAgentLauncherIPAddress, agentLauncherServerPort, ex.Message));
                    return;
                }

                NetworkStream networkStream = socketServer.GetStream();
                System.IO.StreamReader streamReader = new System.IO.StreamReader(networkStream);
                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);

                try
                {
                    //Send the agentArguments
                    streamWriter.WriteLine(proc.StartInfo.Arguments);
                    streamWriter.Flush();
                }
                catch
                {
                    ApplicationLogger.Instance().WriteLine(
                        "Exception Occurred When Trying To Send The Agent Arguments To The Agent Launcher Server");
                }
                // tidy up
                networkStream.Close();
            }
        }

        private void Restore(Arguments arguments)
        {
        }

        private void CreateNew(Arguments arguments)
        {
        }
        
        protected bool HasEnded()
        {
            return this.rc.ReceiverAlive;            
        }

        protected override void OnStep(Context context)
        {
            if (HasEnded())
            {
                this.End();
            }
        }

        #region RemoteConnection

        protected void StartServer(int port)
        {
            // Establish local endpoint...
            ApplicationLogger.Instance().WriteLine("FAtiMA: Creating the socket server..");
            IPAddress ipAddress = IPAddress.Any;
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, port);
            // Create a TCP/IP socket...
            this.serverSocket = new Socket(AddressFamily.Unspecified, SocketType.Stream, ProtocolType.Tcp);
            // Bind the socket...
            this.serverSocket.Bind(localEndPoint);
            this.serverSocket.Listen(PENDING_CONNECTION_QUEUE_LENGTH);

            // accept new connection...
            this.serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), this.serverSocket);
            // wait until a connection is made before continuing...
            ApplicationLogger.Instance().WriteLine("FAtiMA: Comm ready!");
        }

        protected void AcceptCallback(IAsyncResult ar)
        {
            // get the socket handler...
            try
            {
                this.rc.Socket = ((Socket)ar.AsyncState).EndAccept(ar);
                ApplicationLogger.Instance().WriteLine("FAtiMA: Incoming connection ...");

                // create the state object...

                StringBuilder data = new StringBuilder();
                // begin receiving the connection request
                this.rc.Socket.BeginReceive(this.buffer, 0, bufferSize, 0,
                    new AsyncCallback(ReceiveCallback), data);

                //now that we received the connection, we can stop the serversocket
                ApplicationLogger.Instance().WriteLine("FAtiMA: Shuting Down...");
                this.serverSocket.Close();

            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
            }
        }

        protected void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                String receivedMsg = String.Empty;
                // read data from remote device...
                int bytesRead = +this.rc.Socket.EndReceive(ar);
                if (bytesRead > 0)
                {
                    // there may be more...
                    StringBuilder data = (StringBuilder)ar.AsyncState;
                    data.Append(Encoding.UTF8.GetString(this.buffer, 0, bytesRead));
                    // check for EOM.
                    receivedMsg = data.ToString();
                    int EomIndex = receivedMsg.IndexOf(EOM_TAG);
                    if (EomIndex > -1)
                    {
                        // finished receiving...
                        receivedMsg = receivedMsg.Substring(0, EomIndex);
                        // create the corresponding character
                        if (receivedMsg.StartsWith(this.rc.Name))
                        {
                            //everything is ok, the agent that connected is the right agent
                            this.rc.Start();
                            //Aqui tenho de lan�ar o evento de que a mente acabou de se ligar com sucesso
                        }
                        else
                        {
                            //Serious error - someone else tried to connect to this RemoteCharacter
                            this.rc.Socket.Close();
                            this.rc.Socket = null;
                            //lan�ar um evento a dizer que houve merda
                        }
                    }
                    else
                    {
                        // not all data read. Read more...
                        this.rc.Socket.BeginReceive(this.buffer, 0, bufferSize, 0,
                            new AsyncCallback(ReceiveCallback), data);
                    }
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
            }
        }
        #endregion RemoteConnection

        protected override void OnEnd(Context context)
        {         
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
