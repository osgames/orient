using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using ION.Core.Extensions;

namespace FAtiMA.RemoteAgent
{
    public class AwakeRemoteCharacterAction : AwakeAction
    {        
        private RemoteCharacter rc;

        protected override void OnStart(Context context)
        {
            this.rc = (RemoteCharacter)this.Parent;

            String msg = RemoteCharacter.AGENTS;

            IFilterable<Entity> entities = Universe.Instance.Entities;

            foreach (Entity e in entities.FilteredBy(new RemoteCharacter.InactiveFilter()))
            {
                msg = msg + " " + e.Name;
            }

            rc.Send(msg);

            //advancing the narrative time in 5 hours 
            rc.Send(RemoteCharacter.ADVANCE_TIME + " 1500");

            rc.IsActive = true;
            
        }


        protected override void OnStep(Context context)
        {
            if (this.rc.IsActive)
            {
                this.End();
            }
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
