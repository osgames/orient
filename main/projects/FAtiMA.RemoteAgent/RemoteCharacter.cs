// RemoteCharacter.cs - 
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Jo�o Dias
// Email to: joao.dias@inesc-id.pt
// 
// History:
// Jo�o Dias: 24/09/2006 - File Created
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using ION.Core;
using ION.Core.Events;
using ION.Core.Extensions;
using ION.Realizer.Orient.Actions.Body.Behaviours;
using ION.Realizer.Orient.Entities;
using ION.Realizer.Orient.Actions.User;
using System.Collections;
using FAtiMA.Actions;


namespace FAtiMA.RemoteAgent
{
    public class RemoteCharacter : Character, IEventCallback<ActionStarted>, 
                                       IEventCallback<ActionEnded>,
                                       IEventCallback<ActionFailed>,
                                       IEventCallback<PropertyChanged<Property<string>>>,
                                       IEventCallback<PropertyDestroyed<Property<string>>>,
                                       IEventCallback<EntityCreated<Character>>,
                                       IEventCallback<EntityCreated<UserEntity>>,  
                                       IEventCallback<EntityCreated<Item>>,
                                       IEventCallback<EntityCreated<ModellessItem>>,
                                       IEventCallback<EntityDestroyed>
                            
    {
        private const string EOM_TAG = "\n";
        private const string EOF_TAG = "<EOF>";
        private const int bufferSize = 1024;

        public const string EMOTIONAL_STATE_PROPERTY = "emotional-state";
        public const string RELATIONS_PROPERTY = "relations";
        public const string REMOTE_ACTION_PARAMETERS = "REMOTE_ACTION_PARAMETERS";

        public const string START_MESSAGE = "CMD Start";
        public const string STOP_MESSAGE = "CMD Stop";
        public const string RESET_MESSAGE = "CMD Reset";
        public const string SAVE_MESSAGE = "CMD Save";
        public const string REMOVE_ALL_GOALS_MESSAGE = "REMOVEALLGOALS";
        public const string ADD_GOALS_MESSAGE_START = "ADDGOALS ";
        public const string ACTION_FINISHED = "ACTION-FINISHED";
        public const string ACTION_STARTED = "ACTION-STARTED";
        public const string ACTION_FAILED = "ACTION-FAILED";
        public const string ENTITY_ADDED = "ENTITY-ADDED";
        public const string ENTITY_REMOVED = "ENTITY-REMOVED";
        public const string PROPERTY_CHANGED = "PROPERTY-CHANGED";
        public const string PROPERTY_REMOVED = "PROPERTY-REMOVED";
        public const string USER_SPEECH = "USER-SPEECH";
        public const string AGENTS = "AGENTS";
        public const string LOOK_AT = "LOOK-AT";
        public const string ADVANCE_TIME = "ADVANCE-TIME";
        public const string STOP_TIME = "STOP-TIME";
        public const string RESUME_TIME = "RESUME-TIME";
        public const string SHUTDOWN_MESSAGE = "SHUTDOWN";
        

        private Socket socket;
        private NetworkStream socketStream;
        private bool receiverAlive;
        private bool isActive;

        private Thread receiverThread;                

        private byte[] buffer = new byte[bufferSize];

        #region Filter Classes (Filter and InactiveFilter)

        public class Filter :      IFilter<Entity>,
                            IFilter<Property>
        {
            public Filter()
            {
            }

            public virtual bool Blocks(Entity e) 
            {
                return !((e is RemoteCharacter) || (e is Item) || (e is ModellessItem) || (e is UserEntity));
            }

            public bool Blocks(Property p)
            {
                if (p is Property<string>) return false;
                if (p is Property<int>) return false;
                if (p is Property<float>) return false;
                return true;
            }
        }

        public class InactiveFilter : Filter
        {
            public InactiveFilter()
            {
            }

            public override bool Blocks(Entity e)
            {
                if (e is RemoteCharacter)
                {
                    return !((RemoteCharacter)e).isActive;
                }
                else if (e is UserEntity)
                {
                    return !((UserEntity)e).IsActive;
                }
                if ((e is Item)||(e is ModellessItem)) return false;
                return true;
            }
        }

        #endregion
       
        public RemoteCharacter() :base()
		{
            this.isActive = false;
            this.receiverAlive = false;
		}

        ~RemoteCharacter()
        {
            Console.WriteLine("AGENT ENTITY: " + this.Name + " was destroyed...");
            
            if (this.socketStream != null)
            {
                if (receiverAlive)
                {
                    this.Send(SHUTDOWN_MESSAGE); //Makes the java mind close the socket
                    this.receiverAlive = false;
                }
                socketStream.Close();
            }

            if (this.socket != null)
            {
                try
                {
                    socket.Close();
                }
                catch(Exception)
                {
                }
            }
        }


        public Socket Socket
        {
            get
            {
                return this.socket;
            }
            internal set
            {
                this.socket = value;
                this.socketStream = new NetworkStream(this.socket);
            }
        }

        public bool IsActive
        {
            get
            {
                return this.isActive;
            }
            internal set
            {
                this.isActive = value;
            }
        }

        public bool ReceiverAlive
        {
            get
            {
                return this.receiverAlive;
            }
            internal set
            {
                this.receiverAlive = value;
            }
        }

        internal void Start()
        {
            IFilterable<Entity> entities = Universe.Instance.Entities;

            foreach (Entity e in entities.FilteredBy(new Filter()))
            {
                RegisterEventListeners(e);
            }

            Send("OK");
            this.receiverAlive = true;

            this.receiverThread = new Thread(new ThreadStart(ReceiveThread));
            this.receiverThread.Start();
        }

        // MARCO: ADD BASE OnCreate
        protected override void OnCreate(Arguments arguments)
        {
            base.OnCreate(arguments);
            
            this.CreateAction<LoadRemoteAgentAction>(LoadRemoteAgentAction.ACTION_NAME);
            this.CreateAction<AwakeRemoteCharacterAction>(AwakeAction.ACTION_NAME);
            this.CreateAction<PutToSleepRemoteCharacterAction>(PutToSleepAction.ACTION_NAME);

            this.CreateAction<DAQueryResponseAction>(DAQueryResponseAction.ACTION_NAME);

            Universe.Instance.CreateEventListener<EntityCreated<Character>>(this);
            Universe.Instance.CreateEventListener<EntityCreated<Item>>(this);
            Universe.Instance.CreateEventListener<EntityCreated<ModellessItem>>(this);
            Universe.Instance.CreateEventListener<EntityCreated<UserEntity>>(this);
            Universe.Instance.CreateEventListener<EntityDestroyed>(this);

            //creating emotionalstate and relations properties
            this.CreateProperty<Property<EmotionalState>>(EMOTIONAL_STATE_PROPERTY);
            this.CreateProperty<Property<RelationSet>>(RELATIONS_PROPERTY);                

        }

        // MARCO: ADD BASE OnDestroy
        protected override void  OnDestroy()
        {
            Console.WriteLine("AGENT ENTITY: " + this.Name + " was destroyed...");
            if (receiverAlive)
            {
                this.Send(SHUTDOWN_MESSAGE); //Makes the java mind close the socket
                this.receiverAlive = false;
            }

            base.OnDestroy();
        }

        public void RunRemoteAction(RemoteAction remoteAction)
        {
            Action a = this.GetAction(remoteAction.ActionType);

            Dictionary<string, object> arguments = new Dictionary<string, object>();
            arguments.Add(REMOTE_ACTION_PARAMETERS, remoteAction);
            if (remoteAction.ActionType.Equals("saySimple"))
            {
                string utterance = String.Empty;
                foreach (string s in remoteAction.Parameters)
                {
                    utterance += s + " ";
                }
                if (!utterance.Equals(String.Empty))
                {
                    utterance = utterance.Trim();                   
                }
                arguments.Add("utterance", utterance);
            }

            a.Start(new Arguments(arguments));
        }
        

        private void Parse(String msg)
        {
            SpeechAct speech;
            RemoteAction action;
            //ApplicationLogger.Instance().WriteLine("REMOTE_AGENT -> RECEIVED MESSAGE: " + msg);

             if (msg.StartsWith(PROPERTY_CHANGED))
            {
                string[] aux = msg.Split(' ');
                string propertyName = aux[2];
                string value = aux[3];
                if(this.HasProperty<Property<String>>(propertyName))
                {
                    Property<String> p = this.GetProperty<Property<String>>(propertyName);
                    if (!p.Value.Equals(value))
                    {
                        p.Value = value;
                    }
                }
            }
            else if (msg.StartsWith("<EmotionalState"))
            {
                string facialExpression = string.Empty;
                EmotionalState es = (EmotionalState)EmotionalStateParser.Instance.Parse(msg);
                this.SetProperty<EmotionalState>(EMOTIONAL_STATE_PROPERTY, es);

                Emotion e = es.GetStrongestEmotion();
                if (e != null)
                {
                    if (e.Type.Equals(Emotion.GLOATING_EMOTION))
                    {
                        facialExpression = "happy";
                    }
                    else if (e.Type.Equals(Emotion.REPROACH_EMOTION)
                            || e.Type.Equals(Emotion.RESENTMENT_EMOTION)
                            || e.Type.Equals(Emotion.ANGER_EMOTION)
                            || e.Type.Equals(Emotion.HATE_EMOTION))
                    {
                        facialExpression = "angry";
                    }
                    else if(e.Type.Equals(Emotion.DISTRESS_EMOTION) || e.Type.Equals(Emotion.PITTY_EMOTION))
                    {
                        facialExpression = "scared";
                    }
                    else if (e.Type.Equals(Emotion.JOY_EMOTION) || e.Type.Equals(Emotion.HAPPY_FOR_EMOTION))
                    {
                        facialExpression = "happy";
                    }
                    else if (es.Mood > 0.5f)
                    {
                        facialExpression = "happy";
                    }
                    else if (es.Mood < 0.5f)
                    {
                        facialExpression = "afraid";
                    }
                    else
                    {
                        facialExpression = "natural";
                    }
                }
                else
                {
                    facialExpression = "natural";
                }

                Property<string> currentExpression = this.GetProperty<Property<string>>("expression");
                if (currentExpression != null)
                {
                    if (!currentExpression.Value.Equals(facialExpression))
                    {
                        currentExpression.Value = facialExpression;

                        Dictionary<string, object> arguments = new Dictionary<string, object>();
                        arguments.Add("expression", facialExpression);
                        Action a = this.GetAction("change-face");
                        a.Start(new Arguments(arguments));
                    }
                }
            }
            else if (msg.StartsWith("<Relations"))
            {
                List<Relation> auxRelations = (List<Relation>)RelationsParser.Instance.Parse(msg);

                RelationSet relations = new RelationSet();

                foreach (Relation rel in auxRelations)
                {
                    if (rel.Subject.Equals(this.Name))
                    {
                        relations.AddRelation(rel);
                    }
                }

                this.SetProperty<RelationSet>(RELATIONS_PROPERTY, relations);
            }
            else if (msg.StartsWith("look-at"))
            {
                string[] aux = msg.Split(' ');
                LookAt(aux[1]);
                RemoteAction rmAction = new RemoteAction();
                rmAction.Subject = this.Name;
                rmAction.ActionType = "look-at";
                rmAction.Target = aux[1];
                foreach(Entity entity in Universe.Instance.Entities)
                {
                    if (entity is RemoteCharacter)
                    {                        
                        RemoteCharacter rmChar = (RemoteCharacter) entity;
                        if (rmChar.isActive)
                            rmChar.Send(ACTION_FINISHED + " " + rmAction.ToXML());
                    }                        
                }                
            }
            else if (msg.StartsWith("<SpeechAct"))
            {
                LanguageEngineMaster le = Universe.Instance.GetEntity<LanguageEngineMaster>(LanguageEngineMaster.LANGUAGE_ENGINE);

                speech = (SpeechAct) SpeechActParser.Instance.Parse(msg);

                if (speech.Meaning.Equals("episodesummary"))
                {
                    speech.Utterance = le.Narrate(speech.AMSummary);
                }
                else
                {
                    speech.Utterance = le.Say(speech);
                }

                
                Action a = this.GetAction("say-to");

                Dictionary<string, object> arguments = new Dictionary<string, object>();
                arguments.Add(REMOTE_ACTION_PARAMETERS, speech);
                arguments.Add("utterance", speech.Utterance);
                arguments.Add("target", speech.Target);
                a.Start(new Arguments(arguments));
            }
            else if (msg.StartsWith("<Action"))
            {
                action = (RemoteAction)RemoteActionParser.Instance.Parse(msg);
                Action a = this.GetAction(action.ActionType);
                Dictionary<string, object> arguments = new Dictionary<string, object>();

                if (action.Target != null)
                {
                    arguments.Add("target", action.Target);
                }
                arguments.Add(REMOTE_ACTION_PARAMETERS, action);
                if (action.Parameters.Count > 0)
                {
                    string parameterStr = "";
                    foreach (string parameter in action.Parameters)
                    {
                        if (!parameterStr.Equals(""))
                            parameterStr = parameterStr + " ";
                        parameterStr = parameterStr + parameter;
                    }
                    arguments.Add("parameters", parameterStr);
                }
                a.Start(new Arguments(arguments));
            }else if (msg.StartsWith("DAQueryResponse"))
            {
                ApplicationLogger.Instance().WriteLine("Parsing DAQueryResponse: " + msg);
                //RESPONSE FORMAT: DAQueryResponse|ACTION_REPRESENTATION:TRIGGER_ID:APPRAISAL_VALUE
                string[] response = msg.Split('|')[1].Split(':');
                if (response.Length != 3)
                {
                    ApplicationLogger.Instance().WriteLine("Error parsing DAQueryResponse for agent: " + this.Name + "\n:Response: " + msg);
                    throw new Exception("Error parsing DAQueryResponse: AgentID:" + this.Name + " Response: " + msg);
                }
                else
                {
                    string actionRepresentation = response[0];
                    string triggerID = response[1];
                    string appraisalValue = response[2];

                    Action a = this.GetAction(DAQueryResponseAction.ACTION_NAME);

                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                    arguments.Add(DAQueryResponseAction.ACTION_REPRESENTATION, actionRepresentation);
                    arguments.Add(DAQueryResponseAction.TRIGGER_ID, triggerID);
                    arguments.Add(DAQueryResponseAction.APPRAISAL_VALUE, appraisalValue);
                    a.Start(new Arguments(arguments));
                }
            }
        }

        private void SetProperty<T>(string propertyName,T value)
        {
            Property<T> property;

            if (this.HasProperty<Property<T>>(propertyName))
            {
                property = this.GetProperty<Property<T>>(propertyName);

                property.Value = value;
            }
        }

        private void LookAt(string entityName)
        {
            Console.WriteLine(this.Name + " looks at " + entityName);
            string msg =  LOOK_AT + " " + entityName;
            if (Universe.Instance.HasEntity<Entity>(entityName))
            {
                Entity e = Universe.Instance.GetEntity<Entity>(entityName);

                //TODO isto vai dar merda
                foreach (Property<string> p in e.Properties.FilteredBy(new Filter()))
                {
                    msg = msg + " " + p.Name + ":" + p.Value;
                }

                Send(msg);
            }
        }

        private void RegisterEventListeners(Entity e)
        {
            e.CreateEventListener<ActionStarted>(this);
            e.CreateEventListener<ActionEnded>(this);
            e.CreateEventListener<ActionFailed>(this);

            e.CreateEventListener<PropertyChanged<Property<String>>>(this);
            e.CreateEventListener<PropertyDestroyed<Property<String>>>(this);
        }

        private RemoteAction userStoryActionToRemoteAction(UserStoryAction action, Context context)
        {
            RemoteAction rmAction = new RemoteAction();
            rmAction.Subject = UserEntity.ENTITY_NAME;
            rmAction.ActionType = action.Name;
            if (context.Arguments.ContainsKey(UserStoryAction.TARGET_NAME))
             rmAction.Target = (string) context[UserStoryAction.TARGET_NAME];

            if (context.Arguments.ContainsKey(UserStoryAction.PARAMETERS_NAME))
            {
             string parameters = (string)context[UserStoryAction.PARAMETERS_NAME];
             foreach (string parameter in parameters.Split(' '))
             {
                 rmAction.Parameters.Add(parameter);
             }
            }
            return rmAction;
        }

        #region EventListeners

        public void Invoke(ActionStarted e)
        {
            if (e.Action is Behaviour) // a character has performed an action
            {
                if (this.receiverAlive && this.isActive && e.Context.Arguments.ContainsKey(REMOTE_ACTION_PARAMETERS))
                {
                    RemoteAction a = (RemoteAction)e.Context.Arguments[REMOTE_ACTION_PARAMETERS];
                    Send(ACTION_STARTED + " " + a.ToXML());
                }
            }
            else if (e.Action is UserStoryAction) // the user performs an action
            {
                if (this.receiverAlive && this.isActive)
                {
                    // create a remoteaction
                    RemoteAction a = userStoryActionToRemoteAction((UserStoryAction)e.Action, e.Context);
                    Send(ACTION_STARTED + " " + a.ToXML());
                }
            }
        }

        public void Invoke(ActionEnded e)
        {
            // DEBUG
            Console.WriteLine("-- RemoteCharacter.ActionEnded " + e.Action.Name);

            if (this.receiverAlive)
            {
                if (e.Action is PutToSleepAction)
                {
                    Send(ENTITY_REMOVED + " " + ((Entity)e.Action.Parent).Name);

                }
                else if (this.isActive)
                {
                    if (e.Action is AwakeAction)
                    {
                        Send(ENTITY_ADDED + " " + ((Entity)e.Action.Parent).Name);
                    }
                    else if (e.Action is Behaviour && e.Context.Arguments.ContainsKey(REMOTE_ACTION_PARAMETERS))
                    {
                        RemoteAction a = (RemoteAction)e.Context.Arguments[REMOTE_ACTION_PARAMETERS];
                        Send(ACTION_FINISHED + " " + a.ToXML());
                    }
                    else if (e.Action is UserStoryAction) // a character has performed an action
                    {
                        // create a remoteaction
                        RemoteAction a = userStoryActionToRemoteAction((UserStoryAction)e.Action, e.Context);
                        Send(ACTION_FINISHED + " " + a.ToXML());
                    }
                }
            }
        }

        public void Invoke(ActionFailed e)
        {
            if (e.Action is Behaviour) // a character has failed performing an action
            {
                if (this.receiverAlive & this.isActive && e.Context.Arguments.ContainsKey(REMOTE_ACTION_PARAMETERS))
                {
                    RemoteAction a = (RemoteAction)e.Context.Arguments[REMOTE_ACTION_PARAMETERS];
                    Send(ACTION_FAILED + " " + a.ToXML());
                }
            }
            else if (e.Action is UserStoryAction) // the user has failed performing an action
            {
                if (this.receiverAlive && this.isActive)
                {
                    // create a remoteaction
                    RemoteAction a = userStoryActionToRemoteAction((UserStoryAction)e.Action, e.Context);
                    Send(ACTION_FAILED + " " + a.ToXML());
                }
            }

        }

        public void Invoke(PropertyChanged<Property<String>> e)
        {
            if (this.receiverAlive && this.isActive)
            {
                if (e.Property.Parent is Entity)
                {
                    Entity entity = (Entity)e.Property.Parent;
                    string msg = PROPERTY_CHANGED + " " + entity.Name + " " + e.Property.Name + " " + e.Property.Value;
                    Send(msg);
                }
            }
        }

        public void Invoke(PropertyDestroyed<Property<String>> e)
        {
            if (this.receiverAlive)
            {
                if (e.OldParent is Entity)
                {
                    Entity entity = (Entity) e.OldParent;
                    string msg = PROPERTY_REMOVED + " " + entity.Name + " " + e.OldName;
                    Send(msg);
                }
            }
        }

        public void Invoke(EntityCreated<Character> e)
        {
            RegisterEventListeners(e.Entity);
        }


        public void Invoke(EntityCreated<Item> e)
        {
            RegisterEventListeners(e.Entity);
        }

        public void Invoke(EntityCreated<ModellessItem> e)
        {
            RegisterEventListeners(e.Entity);
        }

        public void Invoke(EntityCreated<UserEntity> e)
        {
            RegisterEventListeners(e.Entity);
        }

        public void Invoke(EntityDestroyed e)
        {
            if(!(new Filter()).Blocks(e.Entity))
            {
                if (this.receiverAlive && this.isActive)
                {
                    //Send(ENTITY_REMOVED + " " + e.Entity.Name);
                }
            }
        }

        #endregion

    #region RemoteCommunication

        private void ReceiveThread()
        {
            StreamReader socketReader = null;
            string msg = String.Empty;
            try
            {
                while (receiverAlive)
                {
                    if (socketReader == null)
                    {
                        socketReader = new StreamReader(this.socketStream, Encoding.UTF8);
                    }

                    msg = socketReader.ReadLine();
            
                    Parse(msg);
                }
            }
            catch(KeyNotFoundException)
            {
                throw; //rethrow
            }
            catch (Exception e)
            {
                ApplicationLogger.Instance().WriteLine("Agent " + this.Name + " lost the connection with the mind...: " + e.Message);
            }
        }

        public void Send(String msg)
        {
            byte[] aux = Encoding.UTF8.GetBytes(msg + "\n");
            this.socketStream.Write(aux, 0, aux.Length);
            this.socketStream.Flush();
        }

        #endregion
    }
}
