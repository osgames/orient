using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using ION.Core.Events;
using ION.Core.Extensions;

namespace FAtiMA.RemoteAgent
{
    public class PutToSleepRemoteCharacterAction : PutToSleepAction
    {
        private RemoteCharacter rc;

        protected override void OnStart(Context context)
        {
            this.rc = (RemoteCharacter) this.Parent;
            rc.IsActive = false;
            //TODO: Meter invisivel
        }


        protected override void OnEnd(Context context)
        {            
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnStep(Context context)
        {
            if (!this.rc.IsActive)
            {
                this.End();
            }
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
