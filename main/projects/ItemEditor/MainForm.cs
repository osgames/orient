/* ****************************************************
 * Name: MainForm.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/28 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Collections;
using System.Windows.Forms;
using Mogre;
using PAGE.Generic.Resources;
using PAGE.Generic.Resources.Actions;
using PAGE.Generic.Resources.Assets;
using PAGE.Generic.Resources.Spots;
using PAGE.OGRE.Domain.Assets;
using PAGE.OGRE.Domain.Spots;
using PAGE.OGRE.Math;
using PAGE.Orient;
using PAGE.Orient.Domain.Assets;
using PAGE.Orient.Domain.Cameras;
using PAGE.Orient.GUI.Windows.Forms.Forms;
using PAGE.Orient.GUI.Windows.Forms.ComboBox;
using PAGE.Util;
using Math=System.Math;

namespace ItemEditor
{
    public partial class MainForm : Form
    {
        private OgreItem item = null;
        private OrientCharacter testCharacter = null;
        private OgreSet set = null;
        private OgreItem testBall = null;
        private OgreItem testCube = null;
        private ResourceSpot curSpot = null;
        private bool needsSave = false;
        private float oldTime = 0;
        private AnimationAction curAction = null;
        private SpotForm spotForm = null;

        private const string TEST_CHAR_ID = "spryte_adult_lightgrey";
        private const string TEST_ITEM_ID = "cube";

        private TokenObjectList<ResourceWaypoint> ResourceWaypoints
        {
            get { return this.item.ResourceAsset.ResourceWaypoints; }
        }

        private TokenObjectList<ResourceCameraSpot> ResourceCameraSpots
        {
            get { return this.item.ResourceAsset.ResourceCameraSpots; }
        }

        private TokenObjectList<ResourceItemSpot> ResourceItemSpots
        {
            get { return this.item.ResourceAsset.ResourceItemSpots; }
        }

        public MainForm()
        {
            InitializeComponent();

            this.FormClosing += Form1_FormClosing;

            OrientGraphicsRealizer.Instance.Setup("./data/resources.xml", this.panel1);

            this.spotForm = new SpotForm(this.spotListComboBox1);
            this.spotForm.Show();
            this.spotForm.spotControl1.SpotChanged += spotControl1_SpotChanged;
            this.spotForm.EditableSpotType = true;

            if (!OrientGraphicsRealizer.Instance.ResourcesManager.Characters.Contains(TEST_CHAR_ID))
            {
                MessageBox.Show("Can't continue. Character test is missing...",
                                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            if (!OrientGraphicsRealizer.Instance.ResourcesManager.Items.Contains(TEST_ITEM_ID))
            {
                MessageBox.Show("Can't continue. Item test is missing...",
                                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            string itemID = "testball" + DateTime.Now.Ticks;
            ResourceItem item = new ResourceItem(itemID, 
                OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
            item.FileName = "greenball.mesh";
            item.Scale = new OgreScale(0.04f, 0.04f, 0.04f);
            OrientGraphicsRealizer.Instance.ResourcesManager.Items.Add(item);
            this.testBall = (OgreItem)OrientGraphicsRealizer.Instance.LoadItem(itemID);
            this.testBall.Visible = false;

            this.testCube = (OgreItem) OrientGraphicsRealizer.Instance.LoadItem(TEST_ITEM_ID);
            this.testCube.Visible = false;
            this.testCube.Unplace();
            this.testCube.HideAllDebuggingSpots();
            this.testCube.Node.Scale(7, 7, 7);

            this.testCharacter = (OrientCharacter)OrientGraphicsRealizer.Instance.LoadCharacter(TEST_CHAR_ID);
            this.testCharacter.Visible = false;
            this.testCharacter.Unplace();
            //this.testCharacter.ChangeTexture("test-helper.jpg");
            this.testCharacter.ChangeFace("natural", 1);
            this.testCharacter.HideAllDebuggingSpots();

            foreach (ResourceAction action in this.testCharacter.ResourceAsset.Actions)
            {
                this.actionCBox.Items.Add(action.IdToken);
            }

            ResourceCameraSpot spot = new ResourceCameraSpot("camera-spot",
                OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
            spot.RelativePosition = new OgrePosition(30, 20, 0);
            spot.Tags.Add("camera-spot");
            OgreCameraSpot camSpot = new OgreCameraSpot(spot, this.testCharacter);
            this.testCharacter.CameraSpots.Add(camSpot);
            OrientGraphicsRealizer.Instance.Camera.Place(null, this.testCharacter, "camera-spot", false);
            OrientGraphicsRealizer.Instance.Camera.FocusAsset(this.set);
            OrientGraphicsRealizer.Instance.Camera.CameraMode = CameraMode.Free;

            this.itemControl1.ResourcesManager = OrientGraphicsRealizer.Instance.ResourcesManager;

            this.set = (OgreSet)OrientGraphicsRealizer.Instance.LoadSet("CharTestSet");
            this.set.Visible = true;
            this.set.HideWaypoints();

            this.panel1.AllowNavigation = true;
        }

        void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.CheckNeedSave();
            this.spotForm.Dispose();
            OrientGraphicsRealizer.Instance.Destroy();
        }

        #region Menu Buttons

        #region File Buttons

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CheckNeedSave();

            if (this.openXMLFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = this.openXMLFileDialog.FileName;
                this.saveXMLFileDialog.FileName = fileName;

                ResourceItem resourceItem =
                    new ResourceItem("item0",
                    OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
                resourceItem.LoadFromXml(fileName);
                if (resourceItem.IdToken == null)
                {
                    MessageBox.Show("Invalid item file: " + fileName);
                    return;
                }
                OrientGraphicsRealizer.Instance.ResourcesManager.Items.Remove(resourceItem.IdToken);
                OrientGraphicsRealizer.Instance.ResourcesManager.Items.Add(resourceItem);
                this.CreateNewItem(resourceItem.IdToken);
            }
        }

        private void newItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CheckNeedSave();

            int numItem = 0;
            while (OrientGraphicsRealizer.Instance.ResourcesManager.Items.Contains("item " + numItem))
            {
                numItem++;
            }
            ResourceItem resourceItem = new ResourceItem(
                "item " + numItem, 
                OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
            ((ResourcesManager)OrientGraphicsRealizer.Instance.ResourcesManager).Items.Add(resourceItem);

            this.CreateNewItem(resourceItem.IdToken);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //saves item file
            this.saveXMLFileDialog.FileName = this.item.ResourceAsset.IdToken + ".xml";
            if (this.saveXMLFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.item.ResourceAsset.SaveToXml(this.saveXMLFileDialog.FileName);
                this.needsSave = false;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Spots Buttons

        private void newSpotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CreateNewSpot();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(this.curSpot != null)
            {
                this.ResourceWaypoints.Remove(this.curSpot.IdToken);
                this.ResourceCameraSpots.Remove(this.curSpot.IdToken);
                this.ResourceItemSpots.Remove(this.curSpot.IdToken);
                this.curSpot = null;
                this.spotListComboBox1.RefreshSpotList();
                this.RefreshCurSpot();
            }
        }

        private void deleteAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to delete \n" +
                                                  "all the item's spot?", "Item Editor",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);

            if(result == DialogResult.No) return;

            this.ResourceWaypoints.Clear();
            this.ResourceCameraSpots.Clear();
            this.ResourceItemSpots.Clear();

            this.curSpot = null;
            this.spotListComboBox1.RefreshSpotList();
            this.RefreshCurSpot();
        }

        private void directionFromCharToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.curSpot == null) return;
            Vector3 direction = this.item.Node.Position -
                ((IOgreSpot) this.item.AllSpots[this.curSpot.IdToken][0]).Node.WorldPosition;
            direction.Normalise();
            this.curSpot.RelativeDirection = new OgreDirection(direction);
            this.spotForm.spotControl1.Spot = this.curSpot;
            this.RefreshCurSpot();
        }
                
        #endregion

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox ab = new AboutBox();
            ab.FormClosed += ab_FormClosed;
            ab.Show();
            ab.Refresh();
            this.Enabled = false;
        }

        void ab_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Enabled = true;
            this.Focus();
        }

        #endregion

        #region Form Buttons

        private void playPauseBtn_Click(object sender, EventArgs e)
        {
            if(this.curAction == null) return;

            this.trackBar1.Value = 0;
            this.testCharacter.Entity.GetAnimationState(
                    this.curAction.AnimationName).TimePosition = 0;
            this.testCharacter.Entity.GetAnimationState(
                    this.curAction.AnimationName).AddTime(0);
            if (this.playPauseBtn.Text == "Play")
                this.playPauseBtn.Text = "Pause";
            else
                this.playPauseBtn.Text = "Play";
            OrientGraphicsRealizer.Instance.Pause();
        }

        private void screenshotBtn_Click(object sender, EventArgs e)
        {
            OrientGraphicsRealizer.Instance.SaveScreenshot("itemEditorScreenshot.png");
        }
        #endregion

        #region Private Methods

        private void CheckNeedSave()
        {
            if (this.needsSave)
            {
                DialogResult result = MessageBox.Show(
                    "Item file not saved!\nDo you want to save changes?", "Item Editor",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                else if (result == DialogResult.Yes)
                {
                    this.saveToolStripMenuItem_Click(this, EventArgs.Empty);
                }
            }
        }

        private bool creatingItem = false;
        private void CreateNewItem(string idToken)
        {
            if (this.creatingItem) return;
            this.creatingItem = true;

            if (this.item != null) 
                OrientGraphicsRealizer.Instance.RemoveItem(this.item);
            this.item = (OgreItem)OrientGraphicsRealizer.Instance.LoadItem(idToken);

            this.item.Visible = true;
            this.item.HideAllDebuggingSpots();
            this.needsSave = true;
            this.itemControl1.TokenObject = this.item.ResourceAsset;
            this.itemControl1.GenericItem = this.item;
            this.RefreshItem();

            this.creatingItem = false;
        }

        private void CreateNewSpot()
        {
            int count = 0;
            while (this.ResourceWaypoints.Contains("spot " + count))
            {
                count++;
            }
            ResourceWaypoint spot = new ResourceWaypoint("spot " + count,
                OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
            spot.Tags.Add(spot.IdToken);
            this.ResourceWaypoints.Add(spot);
            this.item.Waypoints.Add(new OgreWaypoint(spot, this.item));
            this.spotListComboBox1.Asset = this.item.ResourceAsset;
            this.spotListComboBox1.Text = spot.IdToken;
            this.RefreshSpots();
        }

        #region Refreshes

        private void RefreshTestAssets()
        {
            if (this.curSpot == null)
            {
                this.previewGroupBox.Enabled = false;
                this.testCharacter.Visible = false;
                this.testBall.Visible = false;
                this.playPauseBtn.Enabled = false;
                this.trackBar1.Enabled = false;
                return;
            }

            if(this.ResourceCameraSpots.Contains(this.curSpot.IdToken))
            {
                this.previewGroupBox.Enabled = false;
                this.testCharacter.Visible = false;
                this.playPauseBtn.Enabled = false;
                this.trackBar1.Enabled = false;

                OrientGraphicsRealizer.Instance.Camera.Place(null, this.item, this.curSpot.IdToken, false);

                this.testBall.Unplace();
                this.testBall.Visible = false;
                this.testCube.Unplace();
                this.testCube.Visible = false;
            }
            else if (this.ResourceItemSpots.Contains(this.curSpot.IdToken))
            {
                this.previewGroupBox.Enabled = false;
                this.testCharacter.Visible = false;
                this.playPauseBtn.Enabled = false;
                this.trackBar1.Enabled = false;

                OrientGraphicsRealizer.Instance.Camera.Place(null, this.testCharacter, "camera-spot", false);
                OrientGraphicsRealizer.Instance.Camera.FocusItemHeight(this.testCube);

                this.testBall.Unplace();
                this.testBall.Place(null, this.item, this.curSpot.IdToken, false);
                this.testBall.Visible = true;

                this.testCube.Unplace();
                this.testCube.Place(null, this.item, this.curSpot.IdToken, false);
                this.testCube.Visible = true;
            }
            else
            {
                this.testCharacter.Unplace();
                this.testCharacter.Place(null, this.item, this.curSpot.IdToken, false);
                this.testCharacter.Visible = true;

                OrientGraphicsRealizer.Instance.Camera.Place(null, this.testCharacter, "camera-spot", false);
                OrientGraphicsRealizer.Instance.Camera.FocusCharacterHeight(this.testCharacter);

                this.previewGroupBox.Enabled = true;
                this.playPauseBtn.Enabled = true;
                this.trackBar1.Enabled = true;

                this.testBall.Unplace();
                this.testBall.Place(null, this.item, this.curSpot.IdToken, false);
                this.testBall.Visible = true;

                this.testCube.Unplace();
                this.testCube.Visible = false;
            }
        }

        private void RefreshItem()
        {
            if (this.item == null)
            {
                this.spotToolStripMenuItem.Enabled = false;
                this.itemControl1.Enabled = false;
                OrientGraphicsRealizer.Instance.Camera.FocusAsset(this.set);
            }
            else
            {
                this.spotToolStripMenuItem.Enabled = true;
                this.spotListComboBox1.Asset = this.item.ResourceAsset;
                this.itemControl1.Enabled = true;
                OrientGraphicsRealizer.Instance.Camera.FocusItemHeight(this.item);
            }
            this.RefreshSpots();
        }

        private void RefreshSpots()
        {
            if (this.item == null) return;

            if ((this.ResourceWaypoints.Count > 0) || (this.ResourceCameraSpots.Count > 0))
            {
                this.deleteToolStripMenuItem.Enabled = true;
                this.deleteAllToolStripMenuItem.Enabled = true;
            }
            else
            {
                this.deleteToolStripMenuItem.Enabled = false;
                this.deleteAllToolStripMenuItem.Enabled = false;
            }
        }

        private void RefreshCurSpot()
        {
            if (this.curSpot == null)
            {
                this.directionFromItemToolStripMenuItem.Enabled = false;
            }
            else
            {
                this.directionFromItemToolStripMenuItem.Enabled = true;

                this.item.Destroy();
                this.item.Reset();
                this.item.Visible = true;
            }

            this.RefreshTestAssets();
        }

        #endregion

        #endregion

        #region Form Control Events

        private void itemControl1_TokenChanged(object sender, EventArgs e)
        {
            this.CreateNewItem(this.item.ResourceAsset.IdToken);
            this.needsSave = true;
        }

        private void actionCBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string actionID = (string)this.actionCBox.SelectedItem;
            this.testCharacter.Animate(null, actionID);
            this.curAction = (AnimationAction)this.testCharacter.ResourceAsset.Actions[actionID];
            float maxTime = this.testCharacter.Entity.GetAnimationState(this.curAction.AnimationName).Length;
            this.trackBar1.Maximum = (int)Math.Ceiling(maxTime) * 1000;
            this.testCharacter.Entity.GetAnimationState(this.curAction.AnimationName).TimePosition = 0;
            this.trackBar1.Value = 0;
            this.oldTime = 0;
        }

        private void spotListComboBox1_SelectedSpotChanged(object sender, EventArgs e)
        {
            this.curSpot = this.spotListComboBox1.SelectedSpot;
            this.RefreshCurSpot();
        }

        private void spotControl1_SpotChanged(object sender, EventArgs e)
        {
            this.curSpot = this.spotForm.spotControl1.Spot;
            this.RefreshCurSpot();
            this.needsSave = true;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            float newTime = this.trackBar1.Value;
            float updateTime = newTime - this.oldTime;
            this.oldTime = newTime;
            OrientGraphicsRealizer.Instance.ManualUpdate(updateTime);
        }

        #endregion
    }
}