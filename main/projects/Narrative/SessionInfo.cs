
using System;
using System.Collections.Generic;
using System.IO;

namespace Narrative
{
    [Serializable()]
    public class SessionInfo
    {
        #region Fields

        private string userName;
        private string userCode;
        private string logFileDir;
        
        #endregion

        #region Properties
        
        public string UserName
        {
            get { return userName; }
        }

        public string UserCode
        {
            get { return userCode; }
        }


        public string LogFileDir
        {
            get { return logFileDir; }
            set { logFileDir = value; }
        }

        #endregion

        public SessionInfo(string userName, string userCode, string logFileDir)
        {
            this.userName = userName;
            this.userCode = userCode;
            this.logFileDir = logFileDir;
        }
        
        
    }
}
