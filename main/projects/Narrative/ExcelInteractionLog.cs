using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Narrative
{
    public class ExcelInteractionLog
    {
        private static ExcelInteractionLog excelInteractionLog = null;

        public static string LOG_DIR = "logs\\";
        private const string LOG_FILE = "excel_interaction_log.csv";       
        private static StreamWriter sWriter;

        private List<string> advicesByUser;
        private string copingStrategyAcceptedByAgent;
        private string copingStrategySuggestedByAgent;        
        private List<string> tomOfUser;
        private string currentCopingStrategy;
        private string currentEpisodeName;

        private enum AgentStates
        {
            AGENT_ASKS,
            AGENT_SUGGESTS,
            TOM
        } ;

        private AgentStates state = AgentStates.AGENT_ASKS;

        private ExcelInteractionLog()
        {
            Init();
        }

        public static ExcelInteractionLog Instance()
        {
            if (excelInteractionLog == null)
                excelInteractionLog = new ExcelInteractionLog();

            return excelInteractionLog;
        }

        private void Init()
        {
            this.advicesByUser = new List<string>();
            this.copingStrategyAcceptedByAgent = "";
            this.copingStrategySuggestedByAgent = "";
            this.tomOfUser = new List<string>();
            this.currentCopingStrategy = "";
            this.currentEpisodeName = "";
        }

        private void changeState(AgentStates newState)
        {
            this.state = newState;
        }

        public void agentSays(string speechacttype)
        {
            if ((speechacttype.IndexOf("askforadvice") != -1) || (speechacttype.IndexOf("askfor2ndadvice") != -1) || (speechacttype.IndexOf("askagain") != -1))
            {
                changeState(AgentStates.AGENT_ASKS);
            }
            if ((speechacttype.IndexOf("suggestcopingstrategy") != -1) || (speechacttype.IndexOf("askifkeepcopingstrategy") != -1))
            {
                changeState(AgentStates.AGENT_SUGGESTS);
            }
            if ((speechacttype.IndexOf("askforreason") != -1)|| (speechacttype.IndexOf("askwhykeep") != -1))
            {
                if (this.state == AgentStates.AGENT_ASKS)
                {
                    this.copingStrategyAcceptedByAgent = this.currentCopingStrategy;
                }else if (this.state == AgentStates.AGENT_SUGGESTS)
                {
                    this.copingStrategySuggestedByAgent = this.currentCopingStrategy;
                }

                changeState(AgentStates.TOM);
            }           
        }

        public void userSays(string userText)
        {
            if (this.state == AgentStates.AGENT_ASKS)
            {
                this.advicesByUser.Add(userText);
            }else if (this.state == AgentStates.TOM)
            {
                this.tomOfUser.Add(userText);
            }
        }

        public void changeCopingStrategy(string newCopingStrategy)
        {
            this.currentCopingStrategy = newCopingStrategy;
        }

        public void changeEpisode(string newEpisode)
        {
            this.currentEpisodeName = newEpisode;
        }

        public void saveLog(string userCode)
        {
            if (currentEpisodeName == "")
                return;

            try
            {                
                if (!Directory.Exists(LOG_DIR + StoryFacilitator.USERS_DIR))
                {
                    Directory.CreateDirectory(LOG_DIR + StoryFacilitator.USERS_DIR);
                }

                if (File.Exists(LOG_DIR + StoryFacilitator.USERS_DIR + "\\" + LOG_FILE))
                {
                    sWriter = File.AppendText(LOG_DIR + StoryFacilitator.USERS_DIR + "\\" + LOG_FILE);
                    sWriter.WriteLine();
                    sWriter.Flush();
                }
                else
                {
                    sWriter = File.CreateText(LOG_DIR + StoryFacilitator.USERS_DIR + "\\" + LOG_FILE);
                    sWriter.WriteLine();
                    sWriter.Flush();
                }

                sWriter.WriteLine("SUBJECT ID   ;DATE   ;EPISODE NAME      ;ADVICE USER     ;CODE CS    ;SUGGESTION VICTIM CS    ;ToM USER     ;");
                if ((advicesByUser.Count == 0) && (tomOfUser.Count == 0))
                {
                    sWriter.Write(userCode + ";" + DateTime.Now + ";" + currentEpisodeName + ";" + "" + ";" +
                                  copingStrategyAcceptedByAgent + ";" + copingStrategySuggestedByAgent + "" + ";");
                }
                else
                {
                    int max = Math.Max(advicesByUser.Count, tomOfUser.Count);
                    for (int i = 0; i < max; i++)
                    {
                        string userAdvice = ((advicesByUser.Count <= i) ? "" : advicesByUser[i]);
                        string tomUser = ((tomOfUser.Count <= i) ? "" : tomOfUser[i]);

                        if (i == 0)
                        {
                            sWriter.WriteLine(userCode + ";" + DateTime.Now + ";" + currentEpisodeName + ";" +
                                              userAdvice + ";" +
                                              copingStrategyAcceptedByAgent + ";" + copingStrategySuggestedByAgent + ";" +
                                              tomUser + ";");
                        }
                        else
                        {
                            sWriter.WriteLine(";" + ";" + ";" + userAdvice + ";" + ";" + ";" + tomUser + ";");
                        }
                    }
                }

                sWriter.WriteLine();
                sWriter.Flush();

                sWriter.Close();              
                
            }finally
            {
                Init();
            }
        }

    }
}
