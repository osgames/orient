// Participant - Holds the information about a property that will belong to a character or an object
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;

namespace Narrative.Participants
{
	/// <summary>
	/// Holds the information about a property that will belong to a character or an object
	/// </summary>
	[Serializable()]
	public class ParticipantProperty
    {

        #region Constants
	    /// <summary>
	    /// Represents the property that the victim has which indicates the sugested coping strategy by the user
	    /// </summary>
        public const string COPING_STRATEGY = "copingStrategy";
        #endregion

        #region Fields

        private string name;
		private string val;
		private string holder;
	    private bool storyLevel;

		#endregion

		#region Properties
		
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				this.name = value;
			}
		}
		
		public string Value
		{
			get
			{
				return val;
			}
			set
			{
				val = value;
			}
		}

		public string Holder
		{
			get { return holder; }
			set { holder = value; }
		}

	    public bool StoryLevel
	    {
	        get { return storyLevel; }
	        set { storyLevel = value; }
	    }

	    #endregion

		public ParticipantProperty(string holder, string name, string val, bool storyLevel)
		{
			Holder = holder;
			Name = name;
			Value = val;
		    this.storyLevel = storyLevel;
		}
	    
	    public ParticipantProperty(ParticipantProperty participantProperty)
	    {
            this.name = participantProperty.name;
		    this.val = participantProperty.val;
		    this.holder = participantProperty.holder;
            this.storyLevel = participantProperty.storyLevel;	        
	    }

		public override bool Equals(object obj)
		{
			if ((obj == null)||(obj.GetType() != this.GetType()))
				return false;
			ParticipantProperty p = obj as ParticipantProperty;
			return ((p.Name == this.Name) && (p.Holder == this.Holder));
		}

		public override int GetHashCode()
		{			
			return (name+holder).GetHashCode();
		}



		public override string ToString()
		{
			return "PROPERTY:" + Holder + ":" + Name + ":" + Value + ":" + storyLevel + "\n";
		}
	    
	    public ParticipantProperty Clone()
	    {
	        return new ParticipantProperty(this);
	    }

	}
}
