// Goal - Holds the information about a characters goal:
//		  Name, Importance of Success, ImportanceOfFailure
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;

namespace Narrative.Participants
{
	/// <summary>
	/// Holds the information about a characters goal:
	/// Name, Importance of Success, ImportanceOfFailure
	/// </summary>
	[Serializable()]
	public class Goal
	{
		#region Fields

		private string name;
		private int importanceOfSucess;
		private int importanceOfFailure;

		#endregion

		#region Properties
		
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}
		
		public int ImportanceOfSucess
		{
			get
			{
				return importanceOfSucess;
			}
			set
			{
				importanceOfSucess = value;
			}
		}
				
		public int ImportanceOfFailure
		{
			get
			{
				return importanceOfFailure;
			}
			set
			{
				importanceOfFailure = value;
			}
		}


		#endregion

		public Goal(string name, int importanceOfSucess, int importanceOfFailure)
		{
			Name = name;
			ImportanceOfFailure = importanceOfFailure;
			ImportanceOfSucess = importanceOfSucess;
		}
	    
	    public Goal(Goal goal)
	    {
            this.name = goal.name;
            this.importanceOfSucess = goal.importanceOfSucess;
            this.importanceOfFailure = goal.importanceOfFailure;
	    }

		public override string ToString()
		{
			return "GOAL:" + Name + ":" + ImportanceOfSucess + ":" + ImportanceOfFailure + "\n";
		}
	    
	    public Goal Clone()
	    {
	        return new Goal(this);
	    }

	}
}
