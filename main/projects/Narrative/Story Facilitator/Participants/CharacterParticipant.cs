// CharacterParticipant - Class that represents a character in an episode
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//
using System;
using System.Collections;

namespace Narrative.Participants
{
	/// <summary>
	/// Summary description for CharacterParticipant.
	/// </summary>
	[Serializable()]
	public sealed class CharacterParticipant : Participant
	{
		
		#region Exceptions

		public class GoalNotFoundException : Exception
		{
			public GoalNotFoundException()
			{
			}
			public GoalNotFoundException(string errorMessage) : base(errorMessage)
			{
			}
			public GoalNotFoundException(string errorMessage, Exception innerException) :base(errorMessage, innerException)
			{
			}
		}

		#endregion

		#region Fields
	    
		private ArrayList goals;

		#endregion
		
		#region Properties

		public ArrayList Goals
		{
			get { return goals; }
		}

		#endregion
		
		public CharacterParticipant(string name)
		{
			this.Name = name;
			Properties = new ArrayList();
			goals = new ArrayList();
		}

		public CharacterParticipant()
		{
			Properties = new ArrayList();
			goals = new ArrayList();
		}
	    
	    public CharacterParticipant(CharacterParticipant characterParticipant)
	    {
            this.Name = characterParticipant.Name;
            this.Properties = new ArrayList();
            this.goals = new ArrayList();

	        foreach (ParticipantProperty participantProperty in characterParticipant.Properties)
	        {
                this.Properties.Add(participantProperty.Clone());
	        }
	        foreach (Goal goal in characterParticipant.Goals)
	        {
                this.goals.Add(goal.Clone());
	        }
	    }


		public override void AddProperty(ParticipantProperty p)
		{
			Properties.Add(p);
		}


		public override ArrayList GetProperties()
		{
			return Properties;
		}

		public override string GetPropertyValue(string propertyName)
		{
			foreach(ParticipantProperty p in Properties)
			{
				if (p.Name == propertyName)
					return p.Value;
			}
			throw new PropertyNotFoundException("ParticipantProperty: " + propertyName + " not found for participant: " + this.Name);
		}
		
		public void AddGoal(Goal g)
		{
			this.Goals.Add(g);
		}

		public Goal GetGoal(string goalName)
		{
			foreach(Goal g in Goals)
			{
				if (g.Name == goalName)
				{
					return g;
				}
			}
			throw new GoalNotFoundException("Goal: " + goalName + " not found for character " + this.Name);
		}

		public override string ToString()
		{
			string res = "CHARACTER_PARTICIPANT:" + Name;

			res+="PROPERTIES\n";
			foreach(ParticipantProperty p in Properties)
				res+=p.ToString();
			res+="GOALS\n";
			foreach(Goal g in Goals)
				res+=g.ToString();

			return res + "\n";
		}
	    
	    public override Participant Clone()
	    {
            return new CharacterParticipant(this);
	    }

	}
}
