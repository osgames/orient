using System;
using System.Collections.Generic;
using System.Text;

namespace Narrative.Participants
{
    public class CharacterManager
    {

        #region Exceptions
        public class CharacterManagerException : Exception
        {
			public CharacterManagerException()
			{
			}
			public CharacterManagerException(string errorMessage) : base(errorMessage)
			{
			}
			public CharacterManagerException(string errorMessage, Exception innerException) :base(errorMessage, innerException)
			{
			}          
        }
        #endregion


        #region Internal Classes
        class CharacterStatus
        {
            private string characterName;
            private bool mindRunning;
            
            public CharacterStatus(string characterName, bool mindRuning)
            {
                this.CharacterName = characterName;
                this.MindRunning = mindRuning;
            }

            public string CharacterName
            {
                get { return characterName; }
                set { characterName = value; }
            }

            public bool MindRunning
            {
                get { return mindRunning; }
                set { mindRunning = value; }
            }

            public override bool Equals(object obj)
            {
                if (obj is CharacterStatus)
                {
                    if ((obj as CharacterStatus).CharacterName == this.CharacterName)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            
            public override int GetHashCode()
            {
			    return (characterName + mindRunning.ToString()).GetHashCode();
            }
        }
        #endregion

        #region Fields

        private List<CharacterStatus>  characterNames;
        
        #endregion


        #region
        
        private List<CharacterStatus> CharacterNames
        {
            get { return characterNames; }
            set { characterNames = value; }
        }        
        
        #endregion

        
        public CharacterManager()
        {
            CharacterNames = new List<CharacterStatus>();    
        }
        
        
        public void AddCharacter(string characterName, bool running)
        {
            CharacterNames.Add(new CharacterStatus(characterName, running));
        }


        public void RemoveCharacter(string characterName)
        {
            CharacterNames.Remove(new CharacterStatus(characterName, false)); //Equals in character status only tests the character name
        }
        
        public void SetCharacterMindStatus(string name, bool mindActive)
        {
            foreach (CharacterStatus characterStatus in characterNames)
            {
                if (characterStatus.CharacterName == name)
                {
                    characterStatus.MindRunning = mindActive;
                    return;
                }
            }
            throw new CharacterManagerException("Character " + name + " not found...");
        }
        
        public bool GetCharacterMindStatus(string name)
        {
            foreach (CharacterStatus characterStatus in characterNames)
            {
                if (characterStatus.CharacterName == name)
                {
                    return characterStatus.MindRunning;                    
                }
            }
            throw new CharacterManagerException("Character " + name + " not found...");            
        }                
    }
}
