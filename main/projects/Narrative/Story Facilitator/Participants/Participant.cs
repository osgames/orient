// Participant - Abstract class that represents a participan in an episode
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Collections;

namespace Narrative.Participants
{
	/// <summary>
	/// Represents an entity that can be in the story:
	/// Object or Character
	/// </summary>
	[Serializable()]
	public abstract class Participant
	{
		#region Exceptions

		public class PropertyNotFoundException : Exception
		{
			public PropertyNotFoundException()
			{
			}
			public PropertyNotFoundException(string errorMessage) : base(errorMessage)
			{
			}
			public PropertyNotFoundException(string errorMessage, Exception innerException) :base(errorMessage, innerException)
			{
			}
		}

		#endregion
		
		#region Fields
		
		private string name;
		private ArrayList properties;

		#endregion

		#region Properties
				
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		public ArrayList Properties
		{
			get { return properties; }
			set { properties = value; }
		}

		#endregion


		public Participant()
		{			
		}

		public abstract ArrayList GetProperties();
		public abstract string GetPropertyValue(string propertyName);
		public abstract void AddProperty(ParticipantProperty p);
        public abstract Participant Clone();
	}
}
