using System;
using System.Collections;

namespace Narrative
{
    public class NarratorSentence
    {
        private String sentenceID;
        private String language;
        private String sentence;
        private String soundFile;

        public NarratorSentence(String sentenceID, String language, String sentence, String soundFile)
        {
            SentenceID = sentenceID;
            Language = language;
            Sentence = sentence;
            SoundFile = soundFile;
        }

        public string Language
        {
            get { return language; }
            set { language = value; }
        }

        public string Sentence
        {
            get { return sentence; }
            set { sentence = value; }
        }

        public string SoundFile
        {
            get { return soundFile; }
            set { soundFile = value; }
        }

        public string SentenceID
        {
            get { return sentenceID; }
            set { sentenceID = value; }
        }

        public static string GetSentence(ArrayList narratorSentences, string id, string language)
        {
            foreach (NarratorSentence sentence in narratorSentences)
            {
                if ((id == sentence.SentenceID) && (language == sentence.Language))
                {
                    return sentence.Sentence;
                }
            }
            return null;
        }

        public static string GetSoundFileName(ArrayList narratorSentences, string id, string language)
        {
            foreach (NarratorSentence sentence in narratorSentences)
            {
                if ((id == sentence.SentenceID) && (language == sentence.Language))
                {
                    return sentence.SoundFile;
                }
            }
            return null;
        }
    }
}
