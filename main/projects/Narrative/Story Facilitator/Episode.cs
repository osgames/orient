// Episode - Represents an episode and holds all the information about an episode.
// 
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Collections;
using Narrative.Actions;
using Narrative.Conditions;
using Narrative.Participants;

namespace Narrative
{
	/// <summary>
	/// Class that holds all the information about an episode:
	/// Preconditions
	/// FinishConditions
	/// Triggers
	/// Intro
	/// and EmotionalInfo
	/// Note: The Name of the episode must be unique or else Equals will not function properly
	/// </summary>
	[Serializable()]
	public class Episode
	{
	    /// <summary>
	    /// Used to set the episodeID.
	    /// Each time an episode is selected by the SF the current ID (episodeIDNumber) is associated with that episode,
	    /// and the global variable episodeIDNumber is incremented. This allows us to differentiate episodes with the same name
	    /// (The INTERACTION episodes might be selected several times)
	    /// </summary>
        //public static int episodesIDNumber = 0;
        public const int EPISODE_ID_NOT_SET = -1;
	    
		public const string NEXT_EPISODE_NOT_DEFINED = "";
        public const int NO_IDLE_TIME_LIMIT = -1;
        public const int NO_TIME_LIMIT = -1;
		public enum EpisodeTypeEnum {INTRO, NORMAL, OUTRO, INTERACTION};

		#region Exceptions

		public class EpisodeException : Exception
		{
			public EpisodeException()
			{
			}
			public EpisodeException(string errorMessage) : base(errorMessage)
			{
			}
			public EpisodeException(string errorMessage, Exception innerException) :base(errorMessage, innerException)
			{
			}
		}

		#endregion

		#region Fields

		private ArrayList participants;
		private ArrayList preConditions; //Will contain elements of ConditionList
		private ArrayList finishConditions; //Will contain elements of ConditionList
		//private EmotionalInfo 
		private ArrayList triggers;
		private ArrayList intro;
		private EpisodeTypeEnum episodeType;	    

		private string name;
        private int episodeID;
		private string scenery;

		private string nextEpisode; //Empty string means that the next episode should be selected by the Story Facilitator's Episode selection process

	    private int idleTimeLimit; //Seconds
	    private int timeLimit; //Seconds

		#endregion

		#region Properties
		
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}

		public string Scenery
		{
			get
			{
				return scenery;
			}
			set
			{
				scenery = value;
			}			
		}

		public ArrayList Intro
		{
			get { return intro; }
			set { intro = value; }
		}

		public ArrayList Participants
		{
			get { return participants; }
		}

		public ArrayList Triggers
		{
			get { return triggers; }
			set { triggers = value; }
		}

		public string NextEpisode
		{
			get { return nextEpisode; }
			set { nextEpisode = value; }
		}

		public EpisodeTypeEnum EpisodeType
		{
			get { return episodeType; }
			set { episodeType = value; }
		}

	    public int TimeLimit
	    {
	        get { return timeLimit; }
	        set { timeLimit = value; }
	    }

	    public int IdleTimeLimit	    
	    {
	        get { return idleTimeLimit; }
	        set { idleTimeLimit = value; }
	    }

	    public ArrayList PreConditions
	    {
	        get { return preConditions; }
	        set { preConditions = value; }
	    }
        /// <summary>
        /// Returns a unique number that identifies the episode.
        /// This is necessary because we might want to select the same episode
        /// more than once (INTERACTION episodes) and we need to have different
        /// identifiers for those episodes (the name property is the same)
        /// -1 means not set yet
        /// </summary>
	    public int EpisodeID
	    {
	        get { return episodeID; }
            set { episodeID = value;}
	    }

	    #endregion


		public Episode(string name, string scenery)
		{		    
			InitEpisode(name, scenery);
			episodeType = EpisodeTypeEnum.NORMAL;
            //episodesIDNumber = episodesIDNumber++;		    
		}

		public Episode(string name, string scenery, EpisodeTypeEnum episodeType)
		{
			InitEpisode(name, scenery);
			EpisodeType = episodeType;
            //episodesIDNumber = episodesIDNumber++;
		}
	    
	    /// <summary>
	    /// Copy constructor
	    /// </summary>
	    /// <param name="ep"></param>
	    public Episode(Episode ep)
	    {
	        this.participants = new ArrayList();
	        foreach (Participant participant in ep.participants)
	        {
                this.participants.Add(participant.Clone());
	        }

	        this.preConditions = new ArrayList();
	        foreach (ConditionList cl in ep.preConditions)
	        {
                this.preConditions.Add(cl.Clone());
	        }

	        this.finishConditions = new ArrayList();
	        foreach (ConditionList cl in ep.finishConditions)
	        {
                this.finishConditions.Add(cl.Clone());
	        }

	        this.triggers = new ArrayList();
	        foreach (Trigger trigger in ep.triggers)
	        {
                this.triggers.Add(trigger.Clone());
	        }

	        this.intro = new ArrayList();
	        foreach (Action action in ep.intro)
	        {
                this.intro.Add(action.Clone());
	        }

            this.episodeType = ep.episodeType;
            this.name = ep.name;
            this.scenery = ep.scenery;
            this.nextEpisode = ep.nextEpisode;
            this.idleTimeLimit = ep.idleTimeLimit;
            this.timeLimit = ep.timeLimit;
            this.episodeID = ep.EpisodeID;
	    }

		private void InitEpisode(string name, string scenery)
		{
			this.Name = name;
			Scenery = scenery;
			participants = new ArrayList();
			PreConditions = new ArrayList();
			finishConditions = new ArrayList();
			Triggers = new ArrayList();
			Intro = new ArrayList();
			nextEpisode = NEXT_EPISODE_NOT_DEFINED;
            IdleTimeLimit = NO_IDLE_TIME_LIMIT;
            TimeLimit = NO_TIME_LIMIT;
		}


		#region Methods
		
		
		public void AddParticipant(Participant p)
		{
			Participants.Add(p);
		}		

		
		public void AddTrigger(Trigger t)
		{
			Triggers.Add(t);
		}


		public void AddCharacterGoal(string characterName, Goal goal)
		{
			foreach(Participant p in Participants)
			{
				if (p.Name.Equals(characterName))
				{
					(p as CharacterParticipant).AddGoal(goal);
					return;
				}
			}
			throw new EpisodeException("Character: " + characterName + " not found in episode: " + Name);
		}

		public void AddFinishCondition(ConditionList fc)
		{
			finishConditions.Add(fc);
		}

		public void AddPreCondition(ConditionList pc)
		{
			PreConditions.Add(pc);
		}


		/// <summary>
		/// Returns true if this episode is suitable for selection by the story manager. 
		/// </summary>
		/// <param name="storyMemory"></param>
		/// <returns>True if at least one of the ConditionList of the Preconditions of this Episode is true </returns>
		public bool IsSelectable(StoryMemory storyMemory)
		{
			if (PreConditions.Count == 0)
				return true;
			else
				foreach(ConditionList cList in PreConditions)
				{
					if (cList.IsSatisfied(storyMemory))
						return true;
				}
			return false;
		}

		/// <summary>
		/// Returns true if this episode has finished.
		/// Episode has finished if at least one of the ConditionList of the FinishConditions of this Episode is true
		/// </summary>
		/// <param name="storyMemory"></param>
		/// <returns>True if at least one of the ConditionList of the FinishConditions of this Episode is true </returns>
		public bool HasFinished(StoryMemory storyMemory)
		{
			if (finishConditions.Count == 0)
				return true;
			else
				foreach(ConditionList cList in finishConditions)
				{
					if (cList.IsSatisfied(storyMemory))
						return true;
				}

			return false;
		}

		public CharacterParticipant GetCharacter(string name)
		{
			foreach (Participant participant in participants)
			{
				if (participant is CharacterParticipant){
					CharacterParticipant characterParticipant = participant as CharacterParticipant;
					if (characterParticipant.Name == name)
						return characterParticipant;
				}
			}
			throw new EpisodeException("GetCharacter: character " + name + " not found in episode " + this.Name);
		}

        public ObjectParticipant GetObject(string name)
        {
            foreach (Participant participant in participants)
            {
                if (participant is ObjectParticipant)
                {
                    ObjectParticipant objectParticipant = participant as ObjectParticipant;
                    if (objectParticipant.Name == name)
                        return objectParticipant;
                }
            }
            throw new EpisodeException("GetObject: object " + name + " not found in episode " + this.Name);
        }	    
		
		public override string ToString()
		{			
			string res = "EPISODE:" + Name + ":" + Scenery + ":" + EpisodeID  +"\n";

			res+="PARTICIPANTS\n";
			foreach(Participant p in Participants)
				res+=p.ToString()+"\n";
			res+="PRECONDITIONS\n";
			foreach(ConditionList cl in PreConditions)
				res+=cl.ToString() + "\n";
			res+="FINISHCONDITIONS\n";
			foreach(ConditionList cl in finishConditions)
				res+=cl.ToString() + "\n";
			res+="INTRO\n";
			foreach(Action action in Intro)
				res+=action.ToString();
			res+="TRIGGERS\n";
			foreach(Trigger t in Triggers)
				res+=t.ToString();


			return res+"\n";
		}

		public override bool Equals(object obj)
		{
			if ((obj == null)||(obj.GetType() != this.GetType()))
				return false;
			Episode e = obj as Episode;
			return (e.Name == this.Name);
		}

		public override int GetHashCode()
		{
			return this.Name.GetHashCode();
		}
	  
	    public Episode Clone()
	    {
	        return new Episode(this);
	    }
	    
	    /// <summary>
	    /// This method returns a copy of the episode.
	    /// The returned episode will have a unique ID, and all the conditions that
	    /// have the CurrentEpisode flag set to true will be updated (their episodeID will be
	    /// updated to the ID of this new episode).
	    /// </summary>
	    /// <returns></returns>
	    public Episode GetCopy()
	    {
	        Episode episode = this.Clone();
	        
            //Set new episodeID
            //episode.episodeID = episodesIDNumber++;
            episode.episodeID = (int)DateTime.Now.Ticks;
	        
	        //Update preconditions + finishConditions + trigger's conditions
	        //If the conditions have the CurrentEpisode flag to true, update
	        //the episodeID to the new Episode
	        ArrayList conditions = new ArrayList();
	        foreach (ConditionList conditionList in episode.preConditions)
	        {
                conditions.AddRange(conditionList.GetConditions());    
	        }
            foreach (ConditionList conditionList in episode.finishConditions)
            {
                conditions.AddRange(conditionList.GetConditions());
            }
            foreach (Trigger trigger in episode.triggers)
            {
                conditions.AddRange(trigger.ConditionList.GetConditions());
            }

            foreach (Condition condition in conditions)
            {
                if (condition is EventCondition)
                {
                    EventCondition ec = (EventCondition)condition;
                    if (ec.CurrentEpisode)
                    {
                        ec.EpisodeID = episode.EpisodeID;
                    }
                }
            }
	        
	        return episode;
	    }
	    
	    /// <summary>
	    /// Tests the consistency of all conditions. If an EventCondition
	    /// has the CurrentEpisode flag set to true, the episodeID field must contain a valid episode id
	    /// (Should be equal to the ID of this episode)
	    /// (See StoryFacilitator.ActiveEpisode property)	    
	    /// </summary>
	    /// <returns></returns>
	    public bool Validate()
	    {
            ArrayList conditions = new ArrayList();
            foreach (ConditionList conditionList in preConditions)
            {
                conditions.AddRange(conditionList.GetConditions());
            }
            foreach (ConditionList conditionList in finishConditions)
            {
                conditions.AddRange(conditionList.GetConditions());
            }
            foreach (Trigger trigger in triggers)
            {
                conditions.AddRange(trigger.ConditionList.GetConditions());
            }

            foreach (Condition condition in conditions)
            {
                if (condition is EventCondition)
                {
                    EventCondition ec = (EventCondition)condition;
                    if (ec.CurrentEpisode)
                    {
                        if (ec.EpisodeID != this.EpisodeID)
                            return false;
                    }
                }
            }

	        return true;
	    }

		#endregion
	}
}
