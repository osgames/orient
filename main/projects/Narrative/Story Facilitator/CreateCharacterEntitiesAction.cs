using System;
using System.Collections;
using System.Collections.Generic;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Extensions;
using EntityCreated=ION.Core.Events.EntityCreated;

namespace Narrative
{
    /// <summary>
    /// Creates a set of RemoteCharacters and awaits for the confirmation of their creation
    /// </summary>
    public class CreateCharacterEntitiesAction : Action, IEventCallback<ION.Core.Events.EntityCreated>
    {
        public const string ACTION_NAME = "CreateCharacterEntitiesAction";
        public const string CHARACTER_LIST = "CHARACTER_LIST";
        public ArrayList characters;
        
        public CreateCharacterEntitiesAction()
        {
            characters = new ArrayList();
        }
        
        protected override void OnStart(Context context)
        {
            Arguments arguments = context.Arguments;
            characters = (arguments[CHARACTER_LIST] as ArrayList).Clone() as ArrayList;
            foreach (ScenarioCharacter scenarioCharacter in characters)
            {
                Dictionary<string, object> args = new Dictionary<string, object>();
                args["type"] = scenarioCharacter.Body;
                string language = string.Empty;
                if (scenarioCharacter.AgentLanguage.Equals("en"))
                {
                    language = "english";
                }else 
                    language = "german";
                args["language"] = language;               
                try
                {
                    Universe.Instance.CreateEntity<RemoteCharacter>(scenarioCharacter.Name, new Arguments(args));
                }catch
                {
                    ApplicationLogger.Instance().WriteLine("Error loading entity: " + scenarioCharacter.Name + " on CreateCharacterEntitiesAction.");
                }
            }
        }

        protected override void OnEnd(Context context)
        {
            characters = new ArrayList();
        }

        protected override void OnStep(Context context)
        {
            if (HasEnded())
            {
                this.End();
            }
        }

        protected bool HasEnded()
        {
            if (characters.Count == 0)
                return true;
            else 
                return false;
        }

        protected override void OnCreate(Arguments arguments)
        {
            Universe.Instance.CreateEventListener<ION.Core.Events.EntityCreated>(this);
        }



        public void Invoke(EntityCreated e)
        {
            if (e.Entity is RemoteCharacter)
            {
                foreach (ScenarioCharacter scenarioCharacter in (ArrayList)characters.Clone())
                {
                    if (e.Entity.Name == scenarioCharacter.Name)
                    {
                        characters.Remove(scenarioCharacter);
                    }
                }
            }
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
