using System;
using System.Collections.Generic;
using System.Text;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Events;
using UserEntity = ION.Realizer.Orient.Entities.UserEntity;
using Narrator=ION.Realizer.Orient.Entities.Narrator;

namespace Narrative
{
    /// <summary>
    /// Removes all remote charactes and the user entity
    /// </summary>
    public class RemoveCharacterEntitiesAction : Action, IEventCallback<ION.Core.Events.EntityDestroyed>
    {
       #region Constants
       public const string ACTION_NAME = "RemoveCharacterEntitiesAction";
       #endregion

        public List<ulong> characterUIDs;
        
        public RemoveCharacterEntitiesAction()
        {
            characterUIDs = new List<ulong>();
        }

        protected override void OnStart(Context context)
        {
            foreach (Entity entity in Universe.Instance.Entities)
            {
                if ((entity is RemoteCharacter) || (entity is UserEntity) || (entity is Narrator) || (entity is LanguageEngineMaster))
                {
                    characterUIDs.Add(entity.UID);
                    Universe.Instance.DestroyEntity(entity.Name);
                }
            }
        }


        protected override void OnEnd(Context context)
        {
            characterUIDs = new List<ulong>();
        }

       protected bool HasEnded()
        {
            return (characterUIDs.Count == 0);
        }

        protected override void OnCreate(Arguments arguments)
        {
            Universe.Instance.CreateEventListener<ION.Core.Events.EntityDestroyed>(this);
        }

        protected override void OnDestroy()
        {            
        }

        public void Invoke(EntityDestroyed e)
        {
            if (characterUIDs.Contains(e.Entity.UID))
            {
                characterUIDs.Remove(e.Entity.UID);
            }
        }

        protected override void OnStep(Context context)
        {
            if (HasEnded())
            {
                this.End();
            }
        }

        protected override void OnFail(Context context)
        {
            throw new NotImplementedException();
        }
    }
}
