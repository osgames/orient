
using System;
using System.IO;

namespace Narrative
{
    public class ComicsLogger
    {
        private static ComicsLogger userLogger = new ComicsLogger();

        public static string LOG_DIR = "users\\";
        private const string LOG_FILE = "_comics2D_log.txt";
        private string user = "default";
        private static StreamWriter sWriter;

        private ComicsLogger()
        {
        }

        ~ComicsLogger()
        {
            Close();
        }
        private void Close()
        {
            try
            {
                user = "default";
                sWriter.Close();
            }
            catch (Exception)
            {
                //ignore
            }
        }

        public static ComicsLogger Instance()
        {
            return userLogger;
        }

        public void Write(string message)
        {
            if ((sWriter == null) || (sWriter.BaseStream == null) || (!sWriter.BaseStream.CanWrite))
            {
                if ((user != "") && (!Directory.Exists(LOG_DIR + user)))
                {
                    Directory.CreateDirectory(LOG_DIR + user);
                }
                string dateTimeStr = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + "_" +
                     DateTime.Now.Minute + "_" + DateTime.Now.Second;
                sWriter = File.CreateText(LOG_DIR + user + "\\" + dateTimeStr + LOG_FILE);
            }
            sWriter.Write(message);
            sWriter.Flush();
        }

        public void WriteLine(string message)
        {
            if ((sWriter == null) || (sWriter.BaseStream == null) || (!sWriter.BaseStream.CanWrite))
            {
                if ((user != "") && (!Directory.Exists(LOG_DIR + user)))
                {
                    Directory.CreateDirectory(LOG_DIR + user);
                }
                string dateTimeStr = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + "_" +
                                     DateTime.Now.Minute + "_" + DateTime.Now.Second;
                sWriter = File.CreateText(LOG_DIR + user + "\\" + dateTimeStr + LOG_FILE);
            }
            sWriter.WriteLine(message);
            sWriter.Flush();
        }

        public void SetUserCode(String subDir)
        {
            Close(); //New file will be created in the new dir
            this.user = subDir;
        }

        public String getUserCode()
        {
            return this.user;
        }
    }
}
