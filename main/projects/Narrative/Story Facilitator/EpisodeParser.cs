// EpisodeParser - This class is used to read an episode from an XML file
// 
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Xml;
using System.Xml.Schema;
using System.Collections;
using Narrative.Actions;
using Narrative.Conditions;
using Narrative.Participants;

namespace Narrative
{
	/// <summary>
	/// Summary description for EpisodeParser.
	/// </summary>
	public sealed class EpisodeParser: XmlParser
	{

		//EPISODE
		private const string EPISODE_NAME = "name";
		private const string EPISODE_SCENERY = "scenery";
		private const string EPISODE_NEXT_EPISODE = "nextEpisode";	    
        private const string EPISODE_INTERACTION = "interaction";
        private const string EPISODE_INTERACTION_TRUE_VALUE = "true";
        private const string EPISODE_IDLE_TIME_LIMIT = "idleTimeLimit";
        private const string EPISODE_TIME_LIMIT = "timeLimit";

		private const string PARTICIPANTS = "participants";
		private const string PRECONDITIONS = "preconditions";
		private const string EMOTIONALINFO = "emotionalInfo";
		private const string GOALS = "goals";
		private const string TRIGGERS = "triggers";
		private const string FINISHCONDITIONS = "finishConditions";
		private const string INTRO = "intro";
	    private const string NARRATOR_SENTENCES = "narratorSentences";

		//PARTICIPANTS
		private const string OBJECT = "object";
		private const string CHARACTER = "character";
		private const string PROPERTY = "property";
		//ATTRIBUTES 
		private const string OBJECT_NAME = "name";		
		private const string CHARACTER_NAME = "name";
		private const string PROPERTY_NAME = "name";
		private const string PROPERTY_VALUE = "value";

		//PRECONDITIONS / FINISHCONDITIONS
		private const string EVENT_CONDITION = "event";
		private const string PROPERTY_CONDITION = "property";
		//ATTRIBUTES
		private const string EVENT_CONDITION_SUBJECT = "subject";
		private const string EVENT_CONDITION_ACTION = "action";
		private const string EVENT_CONDITION_PARAMETERS = "parameters";
		private const string EVENT_CONDITION_EPISODENAME = "episodeName";
		private const string EVENT_CONDITION_EVENTYPE = "eventType";
		private const string EVENT_NEGATION = "negation";
        private const string EVENT_CONDITION_CURRENT_EPISODE = "currentEpisode";
		private const string PROPERTY_CONDITION_OPERATOR = "operator";
		private const string PROPERTY_CONDITION_NAME = "name";
		private const string PROPERTY_CONDITION_PARTICIPANT_NAME = "participantName";
		private const string PROPERTY_CONDITION_VALUE = "value";
		private const string PROPERTY_CONDITION_OPERATOR_EQUAL = "Equal";
		private const string PROPERTY_CONDITION_OPERATOR_LESS_THAN = "LessThan";
		private const string PROPERTY_CONDITION_OPERATOR_GREATER_THAN = "GreaterThan";
		private const string PROPERTY_CONDITION_OPERATOR_NOT_EQUAL = "NotEqual";
		//ATTRIBUTE VALUES
		private const string EVENT_CONDITION_EVENTYPE_ACTION_START = "ACTION-START";
		private const string EVENT_CONDITION_EVENTYPE_ACTION_FINISH = "ACTION-FINISH";

		//GOALS
		private const string GOAL = "Goal";
		//ATTRIBUTES
		private const string GOAL_CHARACTER = "character";
		private const string GOAL_NAME = "name";
		private const string GOAL_IMPORTANCE_OF_SUCESS = "importanceOfSuccess";
		private const string GOAL_IMPORTANCE_OF_FAILURE = "importanceOfFailure";

		//ACTIONS
		private const string INSERT_OBJECT = "insertObject";
        private const string REMOVE_OBJECT = "removeObject";
		private const string INSERT_CHARACTER = "insertCharacter";
	    private const string INSERT_USER = "insertUser";
		private const string REMOVE_CHARACTER = "removeCharacter";
		private const string NARRATE = "narrate";
		private const string ACT_FOR_CHARACTER = "actForCharacter";
		private const string NEXT_EPISODE = "nextEpisode";
		private const string CHANGE_GOAL_IMPORTANCE_SUCCESS = "changeGoalImportanceOfSuccess";
		private const string CHANGE_GOAL_IMPORTANCE_FAILURE = "changeGoalImportanceOfFailure";
		private const string CHANGE_STORY_MODE = "changeStoryMode";
		private const string ADD_GOALS = "addGoals";
		private const string REMOVE_GOAL = "removeGoal";
		private const string REMOVE_ALL_GOALS = "removeAllGoals";
        private const string CHANGE_PROPERTY = "changeProperty";
        private const string TELL_AVAILABLE_COPING_STRATEGIES = "tellAvailableCS";
        private const string WAIT_FOR_EVENT = "waitForEvent";
	    private const string ACT_FOR_ENVIRONMENT = "actForEnvironment";

		//ATTRIBUTES
		private const string INSERT_OBJECT_NAME = "name";
        private const string INSERT_OBJECT_POSITION = "position";
        private const string INSERT_OBJECT_CONTAINER = "container";
		private const string INSERT_CHARACTER_NAME = "name";
        private const string INSERT_USER_NAME = "name";
        private const string INSERT_CHARACTER_POSITION = "position";
		private const string REMOVE_CHARACTER_NAME = "name";
        private const string REMOVE_OBJECT_NAME = "name";
		private const string NARRATE_TEXT = "text";
        private const string NARRATE_TEXT_WAIT = "wait";
		private const string ACT_FOR_CHARACTER_NAME = "characterName";
		private const string ACT_FOR_CHARACTER_ACTION_NAME = "actionName";
		private const string ACT_FOR_CHARACTER_ACTION_PARAMETERS = "actionParameters";
		private const string NEXT_EPISODE_NAME = "episodeName";
		private const string CHANGE_GOAL_IMPORTANCE_SUCCESS_CHARACTER_NAME = "characterName";
		private const string CHANGE_GOAL_IMPORTANCE_SUCCESS_GOAL_NAME = "goalName";
		private const string CHANGE_GOAL_IMPORTANCE_SUCCESS_NEW_VALUE = "newValue";		
		private const string CHANGE_GOAL_IMPORTANCE_FAILURE_CHARACTER_NAME = "characterName";
		private const string CHANGE_GOAL_IMPORTANCE_FAILURE_GOAL_NAME = "goalName";
		private const string CHANGE_GOAL_IMPORTANCE_FAILURE_NEW_VALUE = "newValue";		
		private const string CHANGE_STORY_MODE_NEW_MODE = "newMode";
		private const string CHANGE_STORY_MODE_INTERACTION_MODE = "interactionMode";
		private const string CHANGE_STORY_MODE_PLAY_STORY_MODE = "storyMode";
        private const string CHANGE_STORY_MODE_LOADING_STORY_MODE = "loadingMode";
        private const string CHANGE_STORY_MODE_USER_SELECTION_MODE = "userSelectionMode";
        private const string CHANGE_STORY_MODE_FREE_NAVIGATION_MODE = "freeNavigationMode";
        private const string CHANGE_STORY_MODE_FIRST_PERSON_NOFREELOOK_NAVIGATION_MODE = "firstPersonNoFreeLookNavigationMode";
        private const string CHANGE_STORY_MODE_FIRST_PERSON_FREELOOK_NAVIGATION_MODE = "firstPersonFreeLookNavigationMode";
        private const string ADD_GOALS_CHARACTER_NAME = "characterName";
		private const string REMOVE_GOAL_CHARACTER_NAME = "characterName";
		private const string REMOVE_GOAL_GOAL_NAME = "goalName";
		private const string REMOVE_ALL_GOALS_CHARACTER_NAME = "characterName";
        private const string CHANGE_PROPERTY_PARTICIPANT_NAME = "participantName";
        private const string CHANGE_PROPERTY_PROPERTY_NAME = "propertyName";
        private const string CHANGE_PROPERTY_NEW_VALUE = "newValue";
        private const string TELL_AVAILABLE_COPING_STRATEGIES_CHARACTER_NAME = "characterName";
        private const string WAIT_FOR_EVENT_CHARACTER_NAME = "characterName";
        private const string WAIT_FOR_EVENT_ACTION_NAME = "actionName";
        private const string WAIT_FOR_EVENT_ACTION_PARAMETERS = "actionParameters";
        private const string ACT_FOR_ENVIRONMENT_ACTION_NAME = "actionName";
        private const string ACT_FOR_ENVIRONMENT_ACTION_PARAMETERS = "parameters";

		//TRIGGERS
		private const string TRIGGER = "trigger";
		private const string TRIGGER_CONDITION = "condition";
		private const string TRIGGER_ACTIONS = "actions";
		//ATTRIBUTES
		private const string TRIGGER_PRIORITY = "priority";
	    private const string TRIGGER_EXPECTED_OUTCOME_EVENT = "expectedOutcome";
	    
	    //NARRATOR_SENTENCES        
        private const string SENTENCE = "sentence";
        private const string SENTENCE_LANGUAGE = "language";
	        
	    //ATTRIBUTES
        private const string SENTENCE_ID = "id";
        private const string SENTENCE_LANGUAGE_NAME = "name";
        private const string SENTENCE_LANGUAGE_SENTENCE = "sentence";
        private const string SENTENCE_LANGUAGE_SENTENCE_SOUND_FILE = "soundFile";

		#region Exceptions
		public class EpisodeParserException : Exception
		{
			public EpisodeParserException()
			{
			}
			public EpisodeParserException(string errorMessage) : base(errorMessage)
			{
			}
			public EpisodeParserException(string errorMessage, Exception innerException) :base(errorMessage, innerException)
			{
			}
		}
		#endregion

		class Singleton
		{
			internal static readonly EpisodeParser Instance = new EpisodeParser();

			// explicit static constructor to assure a single execution
			static Singleton()
			{
			}
		}	

		EpisodeParser()
		{
		}

		public static EpisodeParser Instance
		{
			get
			{
				return Singleton.Instance;
			}
		}

		protected override void XmlErrorsHandler(object sender, ValidationEventArgs args) 
		{
			// TO DO: deal with xml errors
			Console.WriteLine("Validation error: " + args.Message);
		}

	    public Episode ParseEpisode(string filename, string language)
	    {
                // load xml file
                XmlDocument xml = new XmlDocument();
                xml.Load(filename);

                // parse elements
                return this.ParseEpisodeXML(xml, language);
	    }

	    /// <summary>
	    /// Parses the episode and in the end replaces the text in the narrator sentences accordingly with
	    /// the language
	    /// </summary>
	    /// <param name="xml"></param>
	    /// <param name="language"></param>
	    /// <returns></returns>
        private Episode ParseEpisodeXML(XmlDocument xml, string language)
        {
            RemoveComments(xml as XmlNode);

            IEnumerator it;

            //Check if the attributes exist
            ArrayList attribList = new ArrayList();
            attribList.Add(EPISODE_NAME);
            attribList.Add(EPISODE_SCENERY);
            CheckAttributes(xml.DocumentElement, "Unknown", attribList);

            string name = xml.DocumentElement.Attributes[EPISODE_NAME].Value;
            string scenery = xml.DocumentElement.Attributes[EPISODE_SCENERY].Value;

            Episode episode = new Episode(name, scenery);

            if (xml.DocumentElement.Attributes[EPISODE_NEXT_EPISODE] != null)
            {
                episode.NextEpisode = xml.DocumentElement.Attributes[EPISODE_NEXT_EPISODE].Value;
            }

            if (xml.DocumentElement.Attributes[EPISODE_INTERACTION] != null)
            {
                if (xml.DocumentElement.Attributes[EPISODE_INTERACTION].Value == EPISODE_INTERACTION_TRUE_VALUE)
                {
                    episode.EpisodeType = Episode.EpisodeTypeEnum.INTERACTION;
                }
            }

            //Default value for Episode.TimeLimit is Episode.NO_TIME_LIMIT
            if (xml.DocumentElement.Attributes[EPISODE_TIME_LIMIT] != null)
            {
                try
                {
                    episode.TimeLimit = Int32.Parse(xml.DocumentElement.Attributes[EPISODE_TIME_LIMIT].Value);
                }
                catch (FormatException e)
                {
                    throw new EpisodeParserException(
                        "Error converting the episode timeLimit attribute in episode:" + name, e);
                }
            }

            //Default value for Episode.IdleTimeLimit is Episode.NO_IDLE_TIME_LIMIT
            if (xml.DocumentElement.Attributes[EPISODE_IDLE_TIME_LIMIT] != null)
            {
                try
                {
                    episode.IdleTimeLimit = Int32.Parse(xml.DocumentElement.Attributes[EPISODE_IDLE_TIME_LIMIT].Value);
                }
                catch (FormatException e)
                {
                    throw new EpisodeParserException(
                        "Error converting the episode idleTimeLimit attribute in episode:" + name, e);
                }
            }


            it = xml.DocumentElement.ChildNodes.GetEnumerator();

            ArrayList narratorSentences = new ArrayList();
            while (it.MoveNext())
            {
                XmlElement child = (XmlElement)it.Current;

                if (child.Name.ToUpper().Equals(PARTICIPANTS.ToUpper()))
                    parseParticipants(child, episode);
                else if (child.Name.ToUpper().Equals(GOALS.ToUpper()))
                    parseGoals(child, episode);
                else if (child.Name.ToUpper().Equals(PRECONDITIONS.ToUpper()))
                    parsePreConditions(child, episode);
                else if (child.Name.ToUpper().Equals(EMOTIONALINFO.ToUpper()))
                {
                    //Console.WriteLine("EmotionalInfo");
                    //parseEmotionalInfo(child);
                }
                else if (child.Name.ToUpper().Equals(TRIGGERS.ToUpper()))
                    parseTrigger(child, episode);
                else if (child.Name.ToUpper().Equals(FINISHCONDITIONS.ToUpper()))
                    parseFinishConditions(child, episode);
                else if (child.Name.ToUpper().Equals(INTRO.ToUpper()))
                    parseIntro(child, episode);
                else if (child.Name.ToUpper().Equals(NARRATOR_SENTENCES.ToUpper()))
                {
                    narratorSentences = parseNarratorSentences(child, episode);
                }
                else
                    throw new EpisodeParserException("Unkown tag: " + child.Name + " in episode:" + episode.Name);
            }

            ArrayList narrativeActions = (ArrayList)episode.Intro.Clone();
            foreach (Trigger trigger in episode.Triggers)
            {
                narrativeActions.AddRange(trigger.Actions);
            }
            
            foreach (Action action in narrativeActions)
            {
                if (action is NarrateAction)
                {
                    NarrateAction narrateAction = action as NarrateAction;
                    string sentence =
                        NarratorSentence.GetSentence(narratorSentences, narrateAction.narrateActionInfo.Text, language);
                    string soundFile = 
                        NarratorSentence.GetSoundFileName(narratorSentences, narrateAction.narrateActionInfo.Text, language);
                    
                    if ((sentence == null) || soundFile == null)
                    {
                        throw new EpisodeParserException("Narrator sentence not found or incorrect for sentenceID: " +
                                                         narrateAction.narrateActionInfo.Text + " in episode: " +
                                                         episode.Name);
                    }

                    narrateAction.narrateActionInfo.Text = sentence;
                    narrateAction.narrateActionInfo.SoundFile = soundFile;
                }else if (action is ActForCharacterAction)
                {
                    ActForCharacterAction actForCharacter = action as ActForCharacterAction;
                    if (actForCharacter.actForCharacterActionInfo.ActionName == ActForCharacterAction.ACT_FOR_CHARACTER_SAY_SIMPLE_ACTION_NAME)
                    {
                        string sentence =
                            NarratorSentence.GetSentence(narratorSentences, actForCharacter.actForCharacterActionInfo.Parameters, language);
                        string soundFile =
                            NarratorSentence.GetSoundFileName(narratorSentences, actForCharacter.actForCharacterActionInfo.Parameters, language);

                        if ((sentence == null) || soundFile == null)
                        {
                            throw new EpisodeParserException("Narrator sentence not found or incorrect for sentenceID: " +
                                                            actForCharacter.actForCharacterActionInfo.Parameters + " in episode: " +
                                                            episode.Name);
                        }

                        actForCharacter.actForCharacterActionInfo.Parameters = sentence;
                    }
                }else if (action is WaitForEventAction)
                {
                    WaitForEventAction waitForEvent = action as WaitForEventAction;
                    if (waitForEvent.waitForEventActionInfo.CharacterName == StoryFacilitator.NARRATOR_NAME)
                    {
                        string sentence =
                            NarratorSentence.GetSentence(narratorSentences, waitForEvent.waitForEventActionInfo.Parameters, language);
                        string soundFile =
                            NarratorSentence.GetSoundFileName(narratorSentences, waitForEvent.waitForEventActionInfo.Parameters, language);

                        if ((sentence == null) || soundFile == null)
                        {
                            throw new EpisodeParserException("Narrator sentence not found or incorrect for sentenceID: " +
                                                            waitForEvent.waitForEventActionInfo.Parameters + " in episode: " +
                                                            episode.Name);
                        }

                        waitForEvent.waitForEventActionInfo.Parameters = sentence;                        
                    }
                }
            }
            
            return episode;
        }
	    
	    /// <summary>
	    /// Parses the episode without replacing the narrator sentences
	    /// </summary>
	    /// <param name="xml"></param>
	    /// <returns></returns>
		protected override object ParseElements(XmlDocument xml)
		{	
			RemoveComments(xml as XmlNode);

			IEnumerator it;

			//Check if the attributes exist
			ArrayList attribList = new ArrayList();
			attribList.Add(EPISODE_NAME);
			attribList.Add(EPISODE_SCENERY);
			CheckAttributes(xml.DocumentElement, "Unknown", attribList);			

			string name = xml.DocumentElement.Attributes[EPISODE_NAME].Value;
			string scenery = xml.DocumentElement.Attributes[EPISODE_SCENERY].Value;

			Episode episode = new Episode(name, scenery);

			if (xml.DocumentElement.Attributes[EPISODE_NEXT_EPISODE] != null)
			{
				episode.NextEpisode = xml.DocumentElement.Attributes[EPISODE_NEXT_EPISODE].Value;
			}
		    
		    if (xml.DocumentElement.Attributes[EPISODE_INTERACTION] != null)
		    {
		        if (xml.DocumentElement.Attributes[EPISODE_INTERACTION].Value == EPISODE_INTERACTION_TRUE_VALUE)
		        {
                    episode.EpisodeType = Episode.EpisodeTypeEnum.INTERACTION;
		        }
		    }
		    
		    //Default value for Episode.TimeLimit is Episode.NO_TIME_LIMIT
		    if (xml.DocumentElement.Attributes[EPISODE_TIME_LIMIT] != null)
		    {
                try
                {
                    episode.TimeLimit = Int32.Parse(xml.DocumentElement.Attributes[EPISODE_TIME_LIMIT].Value);
                }catch(FormatException e)
                {
                    throw new EpisodeParserException(
                        "Error converting the episode timeLimit attribute in episode:" + name, e);
                }
		    }

            //Default value for Episode.IdleTimeLimit is Episode.NO_IDLE_TIME_LIMIT
            if (xml.DocumentElement.Attributes[EPISODE_IDLE_TIME_LIMIT] != null)
            {
                try
                {
                    episode.IdleTimeLimit = Int32.Parse(xml.DocumentElement.Attributes[EPISODE_IDLE_TIME_LIMIT].Value);
                }
                catch (FormatException e)
                {
                    throw new EpisodeParserException(
                        "Error converting the episode idleTimeLimit attribute in episode:" + name, e);
                }
            }
		    

			it = xml.DocumentElement.ChildNodes.GetEnumerator();
			
			while(it.MoveNext())
			{
				XmlElement child = (XmlElement) it.Current;				

				if (child.Name.ToUpper().Equals(PARTICIPANTS.ToUpper()))					
					parseParticipants(child, episode);
				else if (child.Name.ToUpper().Equals(GOALS.ToUpper()))
					parseGoals(child, episode);
				else if (child.Name.ToUpper().Equals(PRECONDITIONS.ToUpper()))					
					parsePreConditions(child, episode);
				else if (child.Name.ToUpper().Equals(EMOTIONALINFO.ToUpper()))
				{
                    //Console.WriteLine("EmotionalInfo");
                    //parseEmotionalInfo(child);
				}
				else if (child.Name.ToUpper().Equals(TRIGGERS.ToUpper()))					
					parseTrigger(child, episode);
				else if (child.Name.ToUpper().Equals(FINISHCONDITIONS.ToUpper()))
					parseFinishConditions(child, episode);
				else if (child.Name.ToUpper().Equals(INTRO.ToUpper()))
					parseIntro(child, episode);
				else 
					throw new EpisodeParserException("Unkown tag: " + child.Name + " in episode:" +episode.Name);
			}
            
			return episode;
		}



		private void CheckAttributes(XmlElement child, string episodeName, ArrayList attributeList)
		{
			foreach(string str in attributeList)
			{
				if (!child.HasAttribute(str))
					throw new EpisodeParserException("Attribute '" + str + "' expected but not found in '"+child.Name+"' of episode " + episodeName);
			}
		}

		void parseParticipants(XmlElement child, Episode episode)
		{						
			foreach(XmlElement participant in child.ChildNodes)
			{				
				if (participant.Name.ToString().ToUpper().Equals(OBJECT.ToUpper()))
					parseParticipantsObject(participant, episode);
				else if (participant.Name.ToString().ToUpper().Equals(CHARACTER.ToUpper()))
					parseParticipantsCharacter(participant, episode);
				else
					throw new EpisodeParserException("Unkown tag in participants: " + participant.Name + " in episode:" +episode.Name);				
			}
		}

		void parseParticipantsObject(XmlElement child, Episode episode)
		{			
			//Check if the attributes exist
			ArrayList attribList = new ArrayList();
			attribList.Add(OBJECT_NAME);
			CheckAttributes(child, episode.Name, attribList);

			string name = child.Attributes[OBJECT_NAME].Value;

			ObjectParticipant objectParticipant = new ObjectParticipant(name);

			foreach(XmlElement property in child.ChildNodes)
			{
				if(!property.Name.ToUpper().Equals(PROPERTY.ToUpper()))
					throw new EpisodeParserException("Unknow tag in participants/object: " + property.Name + " in episode:" +episode.Name);

				//Check the attributes before using
				attribList.Clear();
				attribList.Add(PROPERTY_NAME);
				attribList.Add(PROPERTY_VALUE);
				CheckAttributes(property, episode.Name, attribList);

				string prop_name = property.Attributes[PROPERTY_NAME].Value;
				string prop_value = property.Attributes[PROPERTY_VALUE].Value;

				objectParticipant.AddProperty(new ParticipantProperty(name, prop_name, prop_value, false));								
			}
			episode.AddParticipant(objectParticipant);
		}

		void parseParticipantsCharacter(XmlElement child, Episode episode)
		{
			CharacterParticipant characterParticipant = new CharacterParticipant();

			//Check if the attributes exist
			ArrayList attribList = new ArrayList();
			attribList.Add(CHARACTER_NAME);
			CheckAttributes(child, episode.Name, attribList);

			characterParticipant.Name = child.Attributes[CHARACTER_NAME].Value;

			foreach(XmlElement property in child.ChildNodes)
			{
				if(!property.Name.ToUpper().Equals(PROPERTY.ToUpper()))
					throw new EpisodeParserException("Unknow tag in participants/character: " + property.Name + " in episode:" +episode.Name);

				//Check the attributes before using
				attribList.Clear();
				attribList.Add(PROPERTY_NAME);
				attribList.Add(PROPERTY_VALUE);
				CheckAttributes(property, episode.Name, attribList);

				string prop_name = property.Attributes[PROPERTY_NAME].Value;
				string prop_value = property.Attributes[PROPERTY_VALUE].Value;
				
				characterParticipant.AddProperty(new ParticipantProperty(characterParticipant.Name, prop_name, prop_value, false));
				
			}
			episode.AddParticipant(characterParticipant);
		}


		void parsePreConditions(XmlElement child, Episode episode)
		{			
			foreach(XmlElement condList in child.ChildNodes)
			{
				ConditionList cl = parseConditionList(condList, episode.Name);
				episode.AddPreCondition(cl);
			}				
		}

		void parseFinishConditions(XmlElement child, Episode episode)
		{			
			foreach(XmlElement condList in child.ChildNodes)
			{
				ConditionList cl = parseConditionList(condList, episode.Name);
				episode.AddFinishCondition(cl);
			}				
		}


		ConditionList parseConditionList(XmlElement condList, string episodeName)
		{
			ConditionList cl = new ConditionList();
			foreach(XmlElement cond in condList.ChildNodes)
			{				
				if (cond.Name.ToUpper().Equals(EVENT_CONDITION.ToUpper()))
				{
					//Check the attributes before using
					ArrayList attribList = new ArrayList();										
					attribList.Add(EVENT_CONDITION_SUBJECT);
					attribList.Add(EVENT_CONDITION_ACTION);
					attribList.Add(EVENT_CONDITION_PARAMETERS);
					CheckAttributes(cond, episodeName, attribList);

					EventCondition eventCond = new EventCondition();
					eventCond.Subject = cond.Attributes[EVENT_CONDITION_SUBJECT].Value;
					eventCond.Action = cond.Attributes[EVENT_CONDITION_ACTION].Value;
					eventCond.Parameters = cond.Attributes[EVENT_CONDITION_PARAMETERS].Value;
					if (cond.Attributes[EVENT_CONDITION_EPISODENAME] != null)
					{
						eventCond.EpisodeName = cond.Attributes[EVENT_CONDITION_EPISODENAME].Value;
					} //Default value for EpisodeName:"" means any episode
					if (cond.Attributes[EVENT_NEGATION] != null)
					{					    
						eventCond.Negated = true; // Default is false
					}
				    if (cond.Attributes[EVENT_CONDITION_CURRENT_EPISODE] != null)
				    {
				       eventCond.CurrentEpisode = true;  //Default is false
				    }				    
					if (cond.Attributes[EVENT_CONDITION_EVENTYPE] != null)
					{
						switch(cond.Attributes[EVENT_CONDITION_EVENTYPE].Value)
						{
							case EVENT_CONDITION_EVENTYPE_ACTION_START:
								eventCond.Type = StoryEvent.EventType.ACTION_STARTED;
								break;
							case EVENT_CONDITION_EVENTYPE_ACTION_FINISH:
								eventCond.Type = StoryEvent.EventType.ACTION_FINISHED;
								break;
						}
					}
					
					cl.AddCondition(eventCond);
				}
				else if (cond.Name.ToUpper().Equals(PROPERTY_CONDITION.ToUpper()))
				{
					//Check the attributes before using
					ArrayList attribList = new ArrayList();										
					attribList.Add(PROPERTY_CONDITION_PARTICIPANT_NAME);
					attribList.Add(PROPERTY_CONDITION_NAME);
					attribList.Add(PROPERTY_CONDITION_VALUE);
					attribList.Add(PROPERTY_CONDITION_OPERATOR);
					CheckAttributes(cond, episodeName, attribList);

					PropertyCondition pc = new PropertyCondition();
					pc.ParticipantName = cond.Attributes[PROPERTY_CONDITION_PARTICIPANT_NAME].Value;
					pc.PropertyName = cond.Attributes[PROPERTY_CONDITION_NAME].Value;
					pc.Value = cond.Attributes[PROPERTY_CONDITION_VALUE].Value;
					if (cond.Attributes[PROPERTY_CONDITION_OPERATOR].Value == PROPERTY_CONDITION_OPERATOR_EQUAL)
						pc.Operator = PropertyCondition.Operators.Equal;
					else if(cond.Attributes[PROPERTY_CONDITION_OPERATOR].Value == PROPERTY_CONDITION_OPERATOR_NOT_EQUAL)
						pc.Operator = PropertyCondition.Operators.NotEqual;
					else if(cond.Attributes[PROPERTY_CONDITION_OPERATOR].Value == PROPERTY_CONDITION_OPERATOR_LESS_THAN)
						pc.Operator = PropertyCondition.Operators.LesserThan;
					else if(cond.Attributes[PROPERTY_CONDITION_OPERATOR].Value == PROPERTY_CONDITION_OPERATOR_GREATER_THAN)
						pc.Operator = PropertyCondition.Operators.GreaterThan;
					else
						throw new PropertyCondition.OperatorNotImplementedException("Operator '" + cond.Attributes[PROPERTY_CONDITION_OPERATOR].Value + "' not recognized or not implemented in PreConditions in episode: " + episodeName);
					cl.AddCondition(pc);
				}				
			}
			return cl;
		}

		TriggerConditionList parseTriggerConditionList(XmlElement condList, string episodeName, int triggerID)
		{
			TriggerConditionList cl = new TriggerConditionList();
			foreach(XmlElement cond in condList.ChildNodes)
			{				
				if (cond.Name.ToUpper().Equals(EVENT_CONDITION.ToUpper()))
				{
					//Check the attributes before using
					ArrayList attribList = new ArrayList();										
					attribList.Add(EVENT_CONDITION_SUBJECT);
					attribList.Add(EVENT_CONDITION_ACTION);
					attribList.Add(EVENT_CONDITION_PARAMETERS);
					CheckAttributes(cond, episodeName, attribList);

					EventCondition eventCond = new EventCondition();
					eventCond.Subject = cond.Attributes[EVENT_CONDITION_SUBJECT].Value;
					eventCond.Action = cond.Attributes[EVENT_CONDITION_ACTION].Value;
					eventCond.Parameters = cond.Attributes[EVENT_CONDITION_PARAMETERS].Value;

					if (cond.Attributes[EVENT_CONDITION_EPISODENAME] != null)
						eventCond.EpisodeName = cond.Attributes[EVENT_CONDITION_EPISODENAME].Value;
					else
						eventCond.EpisodeName = episodeName;

                    if (cond.Attributes[EVENT_NEGATION] != null)
                    {
                        eventCond.Negated = true; // Default is false
                    }
                    if (cond.Attributes[EVENT_CONDITION_CURRENT_EPISODE] != null)
                    {
                        eventCond.CurrentEpisode = true;  //Default is false
                    }	

					if (cond.Attributes[EVENT_CONDITION_EVENTYPE] != null)
					{
						switch(cond.Attributes[EVENT_CONDITION_EVENTYPE].Value)
						{
							case EVENT_CONDITION_EVENTYPE_ACTION_START:
								eventCond.Type = StoryEvent.EventType.ACTION_STARTED;
								break;
							case EVENT_CONDITION_EVENTYPE_ACTION_FINISH:
								eventCond.Type = StoryEvent.EventType.ACTION_FINISHED;
								break;
						}
					}

					eventCond.TriggerID = triggerID;
					cl.AddCondition(eventCond);
				}
				else if (cond.Name.ToUpper().Equals(PROPERTY_CONDITION.ToUpper()))
				{
					//Check the attributes before using
					ArrayList attribList = new ArrayList();										
					attribList.Add(PROPERTY_CONDITION_PARTICIPANT_NAME);
					attribList.Add(PROPERTY_CONDITION_NAME);
					attribList.Add(PROPERTY_CONDITION_VALUE);
					attribList.Add(PROPERTY_CONDITION_OPERATOR);
					CheckAttributes(cond, episodeName, attribList);

					PropertyCondition pc = new PropertyCondition();
					pc.ParticipantName = cond.Attributes[PROPERTY_CONDITION_PARTICIPANT_NAME].Value;
					pc.PropertyName = cond.Attributes[PROPERTY_CONDITION_NAME].Value;
					pc.Value = cond.Attributes[PROPERTY_CONDITION_VALUE].Value;
					pc.TriggerID = triggerID;
					if (cond.Attributes[PROPERTY_CONDITION_OPERATOR].Value == PROPERTY_CONDITION_OPERATOR_EQUAL)
						pc.Operator = PropertyCondition.Operators.Equal;
					else if(cond.Attributes[PROPERTY_CONDITION_OPERATOR].Value == PROPERTY_CONDITION_OPERATOR_NOT_EQUAL)
						pc.Operator = PropertyCondition.Operators.NotEqual;
					else if(cond.Attributes[PROPERTY_CONDITION_OPERATOR].Value == PROPERTY_CONDITION_OPERATOR_LESS_THAN)
						pc.Operator = PropertyCondition.Operators.LesserThan;
					else if(cond.Attributes[PROPERTY_CONDITION_OPERATOR].Value == PROPERTY_CONDITION_OPERATOR_GREATER_THAN)
						pc.Operator = PropertyCondition.Operators.GreaterThan;
					else
						throw new PropertyCondition.OperatorNotImplementedException("Operator '" + cond.Attributes[PROPERTY_CONDITION_OPERATOR].Value + "' not recognized or not implemented in PreConditions in episode: " + episodeName);
					cl.AddCondition(pc);
				}				
			}
			return cl;
		}


		public ArrayList parseActionList(XmlElement child, string episodeName)
		{
			ArrayList actionList =  new ArrayList();

			foreach(XmlElement action in child.ChildNodes)
			{
				if (action.Name.ToUpper().Equals(INSERT_OBJECT.ToUpper()))
				{
					//Check if the attributes exist
					ArrayList attribList = new ArrayList();
					attribList.Add(INSERT_OBJECT_NAME);
				    attribList.Add(INSERT_OBJECT_POSITION);
                    attribList.Add(INSERT_OBJECT_CONTAINER);
					CheckAttributes(action, episodeName, attribList);

					string name = action.Attributes[INSERT_OBJECT_NAME].Value;
					InsertObjectAction.InsertObjectActionInfo insertObjectActionInfo = new InsertObjectAction.InsertObjectActionInfo();
					insertObjectActionInfo.ObjectName = name;
                    insertObjectActionInfo.PositionName = action.Attributes[INSERT_OBJECT_POSITION].Value;
                    insertObjectActionInfo.Container = action.Attributes[INSERT_OBJECT_CONTAINER].Value;
					actionList.Add(new InsertObjectAction(insertObjectActionInfo));					    
				}
				else if (action.Name.ToUpper().Equals(INSERT_CHARACTER.ToUpper()))
				{
					//Check if the attributes exist
					ArrayList attribList = new ArrayList();
					attribList.Add(INSERT_CHARACTER_NAME);
					CheckAttributes(action, episodeName, attribList);

					string name =  action.Attributes[INSERT_CHARACTER_NAME].Value;
					InsertCharacterAction.InsertCharacterActionInfo insertCharacterActionInfo = new InsertCharacterAction.InsertCharacterActionInfo();
					insertCharacterActionInfo.CharacterName = name;
                    if (action.Attributes[INSERT_CHARACTER_POSITION] != null)
                    {
                        insertCharacterActionInfo.PositionName = action.Attributes[INSERT_CHARACTER_POSITION].Value;
                    }
					actionList.Add(new InsertCharacterAction(insertCharacterActionInfo));
				}
				else if (action.Name.ToUpper().Equals(REMOVE_CHARACTER.ToUpper()))
				{
					//Check if the attributes exist
					
					ArrayList attribList = new ArrayList();
					attribList.Add(REMOVE_CHARACTER_NAME);
					CheckAttributes(action, episodeName, attribList);

					string name =  action.Attributes[REMOVE_CHARACTER_NAME].Value;
					RemoveCharacterAction.RemoveCharacterActionInfo removeCharacterActionInfo = new RemoveCharacterAction.RemoveCharacterActionInfo();
					removeCharacterActionInfo.CharacterName = name;
					actionList.Add(new RemoveCharacterAction(removeCharacterActionInfo));					
				}
                else if (action.Name.ToUpper().Equals(REMOVE_OBJECT.ToUpper()))
                {
                    //Check if the attributes exist

                    ArrayList attribList = new ArrayList();
                    attribList.Add(REMOVE_OBJECT_NAME);
                    CheckAttributes(action, episodeName, attribList);

                    string name = action.Attributes[REMOVE_OBJECT_NAME].Value;
                    RemoveObjectAction.RemoveObjectActionInfo removeObjectActionInfo = new RemoveObjectAction.RemoveObjectActionInfo();
                    removeObjectActionInfo.ObjectName = name;
                    actionList.Add(new RemoveObjectAction(removeObjectActionInfo));					                    
                }
                else if (action.Name.ToUpper().Equals(NARRATE.ToUpper()))
                {
                    //Check if the attributes exist
                    ArrayList attribList = new ArrayList();
                    attribList.Add(NARRATE_TEXT);
                    CheckAttributes(action, episodeName, attribList);

                    string text = action.Attributes[NARRATE_TEXT].Value;                    
                    NarrateAction.NarrateActionInfo narrateActionInfo = new NarrateAction.NarrateActionInfo();
                    narrateActionInfo.Text = text;
                    if (action.Attributes[NARRATE_TEXT_WAIT] != null)
                    {
                        if (action.Attributes[NARRATE_TEXT_WAIT].Value.ToUpper().Equals("FALSE"))
                            narrateActionInfo.WaitForActionEnd = false;
                    }                        
                    actionList.Add(new NarrateAction(narrateActionInfo));
                }
                else if (action.Name.ToUpper().Equals(ACT_FOR_CHARACTER.ToUpper()))
                {
                    //Check if the attributes exist
                    ArrayList attribList = new ArrayList();
                    attribList.Add(ACT_FOR_CHARACTER_NAME);
                    attribList.Add(ACT_FOR_CHARACTER_ACTION_NAME);
                    attribList.Add(ACT_FOR_CHARACTER_ACTION_PARAMETERS);
                    CheckAttributes(action, episodeName, attribList);

                    string characterName = action.Attributes[ACT_FOR_CHARACTER_NAME].Value;
                    string actionName = action.Attributes[ACT_FOR_CHARACTER_ACTION_NAME].Value;
                    string actionParameters = action.Attributes[ACT_FOR_CHARACTER_ACTION_PARAMETERS].Value;
                    ActForCharacterAction.ActForCharacterActionInfo actForCharacterActionInfo = new ActForCharacterAction.ActForCharacterActionInfo();
                    actForCharacterActionInfo.CharacterName = characterName;
                    actForCharacterActionInfo.ActionName = actionName;
                    actForCharacterActionInfo.Parameters = actionParameters;
                    actionList.Add(new ActForCharacterAction(actForCharacterActionInfo));
                }
                else if (action.Name.ToUpper().Equals(ACT_FOR_ENVIRONMENT.ToUpper()))
                {
                    //Check if the attributes exist
                    ArrayList attribList = new ArrayList();                    
                    attribList.Add(ACT_FOR_ENVIRONMENT_ACTION_NAME);
                    attribList.Add(ACT_FOR_ENVIRONMENT_ACTION_PARAMETERS);
                    CheckAttributes(action, episodeName, attribList);

                    string actionName = action.Attributes[ACT_FOR_ENVIRONMENT_ACTION_NAME].Value;
                    string actionParameters = action.Attributes[ACT_FOR_ENVIRONMENT_ACTION_PARAMETERS].Value;
                    ActForEnvironmentAction.ActForEnvironmentActionInfo actForEnvironmentActionInfo = new ActForEnvironmentAction.ActForEnvironmentActionInfo();
                    actForEnvironmentActionInfo.ActionName = actionName;
                    actForEnvironmentActionInfo.Parameters = actionParameters;
                    actionList.Add(new ActForEnvironmentAction(actForEnvironmentActionInfo));

                }
                else if (action.Name.ToUpper().Equals(NEXT_EPISODE.ToUpper()))
                {
                    string nextEpisodeName = "";
                    if (action.Attributes[NEXT_EPISODE_NAME] != null)
                        nextEpisodeName = action.Attributes[NEXT_EPISODE_NAME].Value;
                    NextEpisodeAction.NextEpisodeActionInfo nextEpisodeActionInfo = new NextEpisodeAction.NextEpisodeActionInfo();
                    nextEpisodeActionInfo.EpisodeName = nextEpisodeName;
                    actionList.Add(new NextEpisodeAction(nextEpisodeActionInfo));

                }
                else if (action.Name.ToUpper().Equals(CHANGE_GOAL_IMPORTANCE_SUCCESS.ToUpper()))
                {
                    //Check if the attributes exist
                    ArrayList attribList = new ArrayList();
                    attribList.Add(CHANGE_GOAL_IMPORTANCE_SUCCESS_CHARACTER_NAME);
                    attribList.Add(CHANGE_GOAL_IMPORTANCE_SUCCESS_GOAL_NAME);
                    attribList.Add(CHANGE_GOAL_IMPORTANCE_SUCCESS_NEW_VALUE);
                    CheckAttributes(action, episodeName, attribList);

                    string characterName = action.Attributes[CHANGE_GOAL_IMPORTANCE_SUCCESS_CHARACTER_NAME].Value;
                    string goalName = action.Attributes[CHANGE_GOAL_IMPORTANCE_SUCCESS_GOAL_NAME].Value;
                    int newValue = int.Parse(action.Attributes[CHANGE_GOAL_IMPORTANCE_SUCCESS_NEW_VALUE].Value);

                    ChangeGoalImportanceOfSuccessAction.ChangeGoalImportanceOfSuccessActionInfo changeGoalImportanceOfSuccess = new ChangeGoalImportanceOfSuccessAction.ChangeGoalImportanceOfSuccessActionInfo(characterName, goalName, newValue);
                    actionList.Add(new ChangeGoalImportanceOfSuccessAction(changeGoalImportanceOfSuccess));
                }
                else if (action.Name.ToUpper().Equals(CHANGE_GOAL_IMPORTANCE_FAILURE.ToUpper()))
                {
                    //Check if the attributes exist
                    ArrayList attribList = new ArrayList();
                    attribList.Add(CHANGE_GOAL_IMPORTANCE_FAILURE_CHARACTER_NAME);
                    attribList.Add(CHANGE_GOAL_IMPORTANCE_FAILURE_GOAL_NAME);
                    attribList.Add(CHANGE_GOAL_IMPORTANCE_FAILURE_NEW_VALUE);
                    CheckAttributes(action, episodeName, attribList);

                    string characterName = action.Attributes[CHANGE_GOAL_IMPORTANCE_FAILURE_CHARACTER_NAME].Value;
                    string goalName = action.Attributes[CHANGE_GOAL_IMPORTANCE_FAILURE_GOAL_NAME].Value;
                    int newValue = int.Parse(action.Attributes[CHANGE_GOAL_IMPORTANCE_FAILURE_NEW_VALUE].Value);

                    ChangeGoalImportanceOfFailureAction.ChangeGoalImportanceOfFailureActionInfo changeGoalImportanceOfFailure = new ChangeGoalImportanceOfFailureAction.ChangeGoalImportanceOfFailureActionInfo(characterName, goalName, newValue);
                    actionList.Add(new ChangeGoalImportanceOfFailureAction(changeGoalImportanceOfFailure));
                }
                else if (action.Name.ToUpper().Equals(CHANGE_STORY_MODE.ToUpper()))
                {
                    //Check if the attributes exist
                    ArrayList attribList = new ArrayList();
                    attribList.Add(CHANGE_STORY_MODE_NEW_MODE);
                    CheckAttributes(action, episodeName, attribList);

                    ChangeStoryModeAction.ChangeStoryModeActionInfo changeStoryModeActionInfo = new ChangeStoryModeAction.ChangeStoryModeActionInfo();

                    if (action.Attributes[CHANGE_STORY_MODE_NEW_MODE].Value == CHANGE_STORY_MODE_INTERACTION_MODE)
                    {
                        changeStoryModeActionInfo.StoryMode = ChangeStoryModeAction.ChangeStoryModeActionInfo.StoryModes.InteractionMode;
                    }
                    else if (action.Attributes[CHANGE_STORY_MODE_NEW_MODE].Value == CHANGE_STORY_MODE_PLAY_STORY_MODE)
                    {
                        changeStoryModeActionInfo.StoryMode = ChangeStoryModeAction.ChangeStoryModeActionInfo.StoryModes.StoryMode;
                    }
                    else if (action.Attributes[CHANGE_STORY_MODE_NEW_MODE].Value == CHANGE_STORY_MODE_LOADING_STORY_MODE)
                    {
                        changeStoryModeActionInfo.StoryMode = ChangeStoryModeAction.ChangeStoryModeActionInfo.StoryModes.StoryLoadingMode;
                    }
                    else if (action.Attributes[CHANGE_STORY_MODE_NEW_MODE].Value == CHANGE_STORY_MODE_USER_SELECTION_MODE)
                    {
                        changeStoryModeActionInfo.StoryMode = ChangeStoryModeAction.ChangeStoryModeActionInfo.StoryModes.UserSelectionMode;
                    }
                    else if (action.Attributes[CHANGE_STORY_MODE_NEW_MODE].Value == CHANGE_STORY_MODE_FREE_NAVIGATION_MODE)
                    {
                        changeStoryModeActionInfo.StoryMode = ChangeStoryModeAction.ChangeStoryModeActionInfo.StoryModes.FreeNavigationMode;
                    }
                    else if (action.Attributes[CHANGE_STORY_MODE_NEW_MODE].Value == CHANGE_STORY_MODE_FIRST_PERSON_NOFREELOOK_NAVIGATION_MODE)
                    {
                        changeStoryModeActionInfo.StoryMode = ChangeStoryModeAction.ChangeStoryModeActionInfo.StoryModes.FirstPersonNoFreeLookMode;
                    }
                    else if (action.Attributes[CHANGE_STORY_MODE_NEW_MODE].Value == CHANGE_STORY_MODE_FIRST_PERSON_FREELOOK_NAVIGATION_MODE)
                    {
                        changeStoryModeActionInfo.StoryMode = ChangeStoryModeAction.ChangeStoryModeActionInfo.StoryModes.FirstPersonFreeLookMode;
                    }
                    else
                        throw new ChangeStoryModeAction.ChangeStoryModeActionInfo.StoryModeNotImplementedException("Error parsing action: " + action.Name + "in episode '" + episodeName + "': Story mode: " + action.Attributes[CHANGE_STORY_MODE_NEW_MODE].Value + " not implemented");

                    actionList.Add(new ChangeStoryModeAction(changeStoryModeActionInfo));
                }
                else if (action.Name.ToUpper().Equals(ADD_GOALS.ToUpper()))
                {
                    //Check if the attributes exist
                    ArrayList attribList = new ArrayList();
                    attribList.Add(ADD_GOALS_CHARACTER_NAME);
                    CheckAttributes(action, episodeName, attribList);

                    string characterName = action.Attributes[ADD_GOALS_CHARACTER_NAME].Value;
                    ArrayList goals = parseGoalsFromAddGoals(action, episodeName);
                    AddGoalsAction.AddGoalsActionInfo addGoalsActionInfo = new AddGoalsAction.AddGoalsActionInfo(characterName, goals);

                    actionList.Add(new AddGoalsAction(addGoalsActionInfo));
                }
                else if (action.Name.ToUpper().Equals(REMOVE_GOAL.ToUpper()))
                {
                    //Check if the attributes exist
                    ArrayList attribList = new ArrayList();
                    attribList.Add(REMOVE_GOAL_CHARACTER_NAME);
                    attribList.Add(REMOVE_GOAL_GOAL_NAME);
                    CheckAttributes(action, episodeName, attribList);

                    string characterName = action.Attributes[REMOVE_GOAL_CHARACTER_NAME].Value;
                    string goalName = action.Attributes[REMOVE_GOAL_GOAL_NAME].Value;
                    RemoveGoalAction.RemoveGoalActionInfo removeGoalActionInfo = new RemoveGoalAction.RemoveGoalActionInfo(characterName, goalName);

                    actionList.Add(new RemoveGoalAction(removeGoalActionInfo));
                }
                else if (action.Name.ToUpper().Equals(REMOVE_ALL_GOALS.ToUpper()))
                {
                    //Check if the attributes exist
                    ArrayList attribList = new ArrayList();
                    attribList.Add(REMOVE_ALL_GOALS_CHARACTER_NAME);
                    CheckAttributes(action, episodeName, attribList);

                    string characterName = action.Attributes[REMOVE_ALL_GOALS_CHARACTER_NAME].Value;
                    RemoveAllGoalsAction.RemoveAllGoalsActionInfo removeAllGoalsAInfo = new RemoveAllGoalsAction.RemoveAllGoalsActionInfo(characterName);

                    actionList.Add(new RemoveAllGoalsAction(removeAllGoalsAInfo));
                }
                else if (action.Name.ToUpper().Equals(CHANGE_PROPERTY.ToUpper()))
                {
                    ArrayList attribList = new ArrayList();
                    attribList.Add(CHANGE_PROPERTY_PARTICIPANT_NAME);
                    attribList.Add(CHANGE_PROPERTY_PROPERTY_NAME);
                    attribList.Add(CHANGE_PROPERTY_NEW_VALUE);
                    CheckAttributes(action, episodeName, attribList);

                    string participantName = action.Attributes[CHANGE_PROPERTY_PARTICIPANT_NAME].Value;
                    string propertyName = action.Attributes[CHANGE_PROPERTY_PROPERTY_NAME].Value;
                    string newValue = action.Attributes[CHANGE_PROPERTY_NEW_VALUE].Value;

                    ChangePropertyAction.ChangePropertyActionInfo changePropertyAInfo = new ChangePropertyAction.ChangePropertyActionInfo(participantName, propertyName, newValue);

                    actionList.Add(new ChangePropertyAction(changePropertyAInfo));
                }
                else if (action.Name.ToUpper().Equals(TELL_AVAILABLE_COPING_STRATEGIES.ToUpper()))
                {
                    ArrayList attribList = new ArrayList();
                    attribList.Add(TELL_AVAILABLE_COPING_STRATEGIES_CHARACTER_NAME);
                    CheckAttributes(action, episodeName, attribList);

                    string characterName = action.Attributes[TELL_AVAILABLE_COPING_STRATEGIES_CHARACTER_NAME].Value;

                    TellAvailableCS.TellAvailableCSInfo actionInfo = new TellAvailableCS.TellAvailableCSInfo(characterName);

                    actionList.Add(new TellAvailableCS(actionInfo));
                }
                else if (action.Name.ToUpper().Equals(WAIT_FOR_EVENT.ToUpper()))
                {
                    ArrayList attribList = new ArrayList();
                    attribList.Add(WAIT_FOR_EVENT_ACTION_NAME);
                    attribList.Add(WAIT_FOR_EVENT_CHARACTER_NAME);
                    attribList.Add(WAIT_FOR_EVENT_ACTION_PARAMETERS);
                    CheckAttributes(action, episodeName, attribList);

                    string characterName = action.Attributes[WAIT_FOR_EVENT_CHARACTER_NAME].Value;
                    string actionName = action.Attributes[WAIT_FOR_EVENT_ACTION_NAME].Value;
                    string parameters = action.Attributes[WAIT_FOR_EVENT_ACTION_PARAMETERS].Value;

                    WaitForEventAction.WaitForEventActionInfo actionInfo = new WaitForEventAction.WaitForEventActionInfo();
                    actionInfo.CharacterName = characterName;
                    actionInfo.ActionName = actionName;
                    actionInfo.Parameters = parameters;

                    actionList.Add(new WaitForEventAction(actionInfo));
                }
                else if (action.Name.ToUpper().Equals(INSERT_USER.ToUpper()))
                {
                    ArrayList attribList = new ArrayList();
                    attribList.Add(INSERT_USER_NAME);
                    CheckAttributes(action, episodeName, attribList);

                    InsertUserAction.InsertUserActionInfo actionInfo = new InsertUserAction.InsertUserActionInfo();
                    actionInfo.UserName = action.Attributes[INSERT_USER_NAME].Value;

                    actionList.Add(new InsertUserAction(actionInfo));
                }
                else
                    throw new EpisodeParserException("Unknow action in intro: " + action.Name + " in episode " + episodeName);
			}

			return actionList;
		}

		ArrayList parseGoalsFromAddGoals(XmlElement child, string episodeName)
		{
			ArrayList goals = new ArrayList();
			foreach(XmlElement xmlGoal in child.ChildNodes)
			{
				if (!xmlGoal.Name.ToUpper().Equals(GOAL.ToUpper()))
					throw new EpisodeParserException("Unknow tag in goals: " + xmlGoal.Name + " in episode: " + episodeName);

				//Check if the attributes exist
				ArrayList attribList = new ArrayList();				
				attribList.Add(GOAL_NAME);
				attribList.Add(GOAL_IMPORTANCE_OF_SUCESS);
				attribList.Add(GOAL_IMPORTANCE_OF_FAILURE);
				CheckAttributes(xmlGoal, episodeName, attribList);
				string goalName = xmlGoal.Attributes[GOAL_NAME].Value;
				int importanceOfSuccess =  int.Parse(xmlGoal.Attributes[GOAL_IMPORTANCE_OF_SUCESS].Value);
				int importanceOfFailure =  int.Parse(xmlGoal.Attributes[GOAL_IMPORTANCE_OF_FAILURE].Value);

				goals.Add(new Goal(goalName, importanceOfSuccess, importanceOfFailure));
			}

			return goals;
		}

		void parseGoals(XmlElement child, Episode episode)
		{
				foreach(XmlElement xmlGoal in child.ChildNodes)
				{
					if (!xmlGoal.Name.ToUpper().Equals(GOAL.ToUpper()))
						throw new EpisodeParserException("Unknow tag in goals: " + xmlGoal.Name + " in episode: " + episode.Name);

					//Check if the attributes exist
					ArrayList attribList = new ArrayList();
					attribList.Add(GOAL_CHARACTER);
					attribList.Add(GOAL_NAME);
					attribList.Add(GOAL_IMPORTANCE_OF_SUCESS);
					attribList.Add(GOAL_IMPORTANCE_OF_FAILURE);
					CheckAttributes(xmlGoal, episode.Name, attribList);

					string characterName = xmlGoal.Attributes[GOAL_CHARACTER].Value;
					string goalName = xmlGoal.Attributes[GOAL_NAME].Value;
					int importanceOfSuccess =  int.Parse(xmlGoal.Attributes[GOAL_IMPORTANCE_OF_SUCESS].Value);
					int importanceOfFailure =  int.Parse(xmlGoal.Attributes[GOAL_IMPORTANCE_OF_FAILURE].Value);

					try
					{
						episode.AddCharacterGoal(characterName, new Goal(goalName, importanceOfSuccess, importanceOfFailure));
					}catch(Episode.EpisodeException ex)
					{
						throw new EpisodeParserException("Goal for inexistent character: " + characterName + " in episode " + episode.Name, ex);
					}
					
				}
		}

		private void parseIntro(XmlElement child, Episode episode)
		{
			episode.Intro = parseActionList(child, episode.Name);
		}

		private void parseTrigger(XmlElement child, Episode episode)
		{
			foreach(XmlElement triggerXML in child.ChildNodes)
			{
				if (!triggerXML.Name.ToUpper().Equals(TRIGGER.ToUpper()))
					throw new EpisodeParserException("Unknow tag in triggers: " + triggerXML.Name + " in episode " + episode.Name);
				
				int priority = 1;                
				if (triggerXML.Attributes[TRIGGER_PRIORITY] != null)
					priority = int.Parse(triggerXML.Attributes[TRIGGER_PRIORITY].Value);
				Trigger trigger = new Trigger(priority);
                if (triggerXML.Attributes[TRIGGER_EXPECTED_OUTCOME_EVENT] != null)
                {
                    trigger.ExpectedOutcome = triggerXML.Attributes[TRIGGER_EXPECTED_OUTCOME_EVENT].Value;
                }
                
				foreach(XmlElement cond_action in triggerXML.ChildNodes)
				{
					if (cond_action.Name.ToUpper().Equals(TRIGGER_CONDITION.ToUpper()))
					{
						//The TriggerID is generated automatically
						TriggerConditionList cl = parseTriggerConditionList(cond_action, episode.Name, trigger.TriggerID);
						trigger.ConditionList = cl;
					}else if (cond_action.Name.ToUpper().Equals(TRIGGER_ACTIONS.ToUpper()))
					{
						ArrayList actionList = parseActionList(cond_action, episode.Name);						
						trigger.Actions = actionList;						
					}else
						throw new EpisodeParserException("Unknow tag in trigger action/conditions: " + cond_action.Name + " in episode " + episode.Name);
				}
				episode.AddTrigger(trigger);
			}
		}
	    
	    private ArrayList parseNarratorSentences(XmlElement child, Episode episode)
	    {
	        ArrayList sentences = new ArrayList();
	        foreach(XmlElement sentenceXML in child.ChildNodes)
	        {
                if (!sentenceXML.Name.ToUpper().Equals(SENTENCE.ToUpper()))
                    throw new EpisodeParserException("Unknow tag in NarratorSentences: " + sentenceXML.Name + " in episode " + episode.Name);

                string sentenceID = sentenceXML.Attributes[SENTENCE_ID].Value;
	            foreach (XmlElement languageXML in sentenceXML.ChildNodes)
	            {
                    if (!languageXML.Name.ToUpper().Equals(SENTENCE_LANGUAGE.ToUpper()))
                        throw new EpisodeParserException("Unknow tag in NarratorSentences: " + sentenceXML.Name + " in episode " + episode.Name);
	                
                    string languageName = languageXML.Attributes[SENTENCE_LANGUAGE_NAME].Value;
                    string sentence = languageXML.Attributes[SENTENCE_LANGUAGE_SENTENCE].Value;
                    string soundFile = languageXML.Attributes[SENTENCE_LANGUAGE_SENTENCE_SOUND_FILE].Value;
                    sentences.Add(new NarratorSentence(sentenceID, languageName, sentence, soundFile));
	            }
	        }
            return sentences;
	    }

		protected override void ParseElements(XmlDocument xml, object result) 
		{
		}


	}
}
