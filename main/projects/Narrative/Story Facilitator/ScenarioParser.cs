using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using ION.Core.Extensions;
using Narrative.Actions;
using Narrative.Participants;

// ScenarioParser - This class is used to read a scenario from an XML file
//					A scenario describes the characters and objects that take part in a story
// 
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
// NOTE: This version uses a constant EPISODE_FOLDER that specifies a different folder for the episodes
// The correct folder is not being used yet


namespace Narrative
{
	/// <summary>
	/// Summary description for ScenarioParser.
	/// </summary>
	public class ScenarioParser : XmlParser
	{

		//Scenario
		private const string SCENARIO_NAME = "name";
        private const string SCENARIO_LANGUAGE = "language";
        private const string SCENARIO_USER_LANGUAGE = "userLanguage";
        private const string Scenario_USER_LANGUAGE_SET = "userLanguageSet";

		private const string CAST = "cast";
		private const string PROPS = "props";
		private const string EPISODES = "episodes";

		//CAST
		private const string CHARACTER = "character";
		private const string PROPERTY = "property";
		//ATTRIBUTES
		private const string CHARACTER_NAME = "name";
		private const string CHARACTER_DISPLAY_NAME = "displayName";
		private const string CHARACTER_BODY = "body";
        private const string CHARACTER_AGENT_LANGUAGE = "agentLanguage";
        private const string CHARACTER_AGENT_LANGUAGE_SET = "agentLanguageSet";
        private const string CHARACTER_VOICE = "voice";


		//PROPS
		private const string OBJECT = "object";
		//ATTRIBUTES
		private const string OBJECT_NAME = "name";
        private const string OBJECT_MODEL = "model";

		//EPISODES
        private const string SCENARIOS_FOLDER = "data\\scenarios\\";
		private const string EPISODE = "episode";
		
		//ATTRIBUTES		
		private const string EPISODE_FILE = "file";
		private const string EPISODE_TYPE = "episodeType";
		//VALUES
		private const string EPISODE_TYPE_INTRO = "INTRO";
		private const string EPISODE_TYPE_NORMAL = "NORMAL";
        private const string EPISODE_TYPE_INTERACTION = "INTERACTION";
		private const string EPISODE_TYPE_OUTRO = "OUTRO";

		//PROPERTY
		//ATTRIBUTES
		private const string PROPERTY_NAME = "name";
		private const string PROPERTY_VALUE = "value";        

		

		#region Exceptions
		public class ScenarioParserException : Exception
		{
			public ScenarioParserException()
			{
			}
			public ScenarioParserException(string errorMessage) : base(errorMessage)
			{
			}
			public ScenarioParserException(string errorMessage, Exception innerException) :base(errorMessage, innerException)
			{
			}
		}
		#endregion

		class Singleton
		{
			internal static readonly ScenarioParser Instance = new ScenarioParser();

			// explicit static constructor to assure a single execution
			static Singleton()
			{
			}
		}	

		ScenarioParser()
		{
		}

		public static ScenarioParser Instance
		{
			get
			{
				return Singleton.Instance;
			}
		}


		protected override void XmlErrorsHandler(object sender, ValidationEventArgs args)
		{
			// TO DO: deal with xml errors
			Console.WriteLine("Validation error: " + args.Message);			
		}


		private void CheckAttributes(XmlElement child, string scenarioName, ArrayList attributeList)
		{
			foreach(string str in attributeList)
			{
				if (!child.HasAttribute(str))
					throw new ScenarioParserException("Attribute '" + str + "' expected but not found in '"+child.Name+"' of scenario " + scenarioName);
			}
		}


		protected override object ParseElements(XmlDocument xml)
		{			
			RemoveComments(xml as XmlNode);
			IEnumerator it;

			//Check if the attributes exist
			ArrayList attribList = new ArrayList();
			attribList.Add(SCENARIO_NAME);
			attribList.Add(SCENARIO_USER_LANGUAGE);
            attribList.Add(Scenario_USER_LANGUAGE_SET);
            attribList.Add(SCENARIO_LANGUAGE);
			CheckAttributes(xml.DocumentElement, "Unknown", attribList);

			string name = xml.DocumentElement.Attributes[SCENARIO_NAME].Value;			
            string userLanguage = xml.DocumentElement.Attributes[SCENARIO_USER_LANGUAGE].Value;
		    string userLanguageSet = xml.DocumentElement.Attributes[Scenario_USER_LANGUAGE_SET].Value;
            string language = xml.DocumentElement.Attributes[SCENARIO_LANGUAGE].Value;
		    
		    
			Scenario scenario = new Scenario(name, language, userLanguage, userLanguageSet);			
			
			it = xml.DocumentElement.ChildNodes.GetEnumerator();

			while(it.MoveNext())
			{
				XmlElement child = (XmlElement) it.Current;				
				if (child.Name.ToUpper().Equals(CAST.ToUpper()))
				{
					parseCast(child, scenario);
				}else if (child.Name.ToUpper().Equals(PROPS.ToUpper()))
				{
					parseProps(child, scenario);
				}else if (child.Name.ToUpper().Equals(EPISODES.ToUpper()))
				{
					parseEpisodes(child, scenario);
				} else
					throw new ScenarioParserException("Unkown tag: " + child.Name + " in scenario:" + scenario);
			}

			return scenario;
			
		}

		private void parseCast(XmlElement child, Scenario scenario)
		{
			foreach (XmlElement xmlCharacter in child.ChildNodes)
			{
				if(!xmlCharacter.Name.ToUpper().Equals(CHARACTER.ToUpper()))
					throw new ScenarioParserException("Invalid tag in cast: " + xmlCharacter.Name);

				//Check if the attributes exist
				ArrayList attribList = new ArrayList();
				attribList.Add(CHARACTER_NAME);
				attribList.Add(CHARACTER_DISPLAY_NAME);
				attribList.Add(CHARACTER_BODY);
				attribList.Add(CHARACTER_AGENT_LANGUAGE);
                attribList.Add(CHARACTER_AGENT_LANGUAGE_SET);
				CheckAttributes(xmlCharacter, scenario.Name, attribList);

				string name = xmlCharacter.Attributes[CHARACTER_NAME].Value;
				string displayName = xmlCharacter.Attributes[CHARACTER_DISPLAY_NAME].Value;
				string body = xmlCharacter.Attributes[CHARACTER_BODY].Value;
				string agentLanguage = xmlCharacter.Attributes[CHARACTER_AGENT_LANGUAGE].Value;
                string agentLanguageSet = xmlCharacter.Attributes[CHARACTER_AGENT_LANGUAGE_SET].Value;
				ScenarioCharacter scenarioCharacter = new ScenarioCharacter(name, displayName, body, agentLanguage, agentLanguageSet);

			    if (xmlCharacter.Attributes[CHARACTER_VOICE] != null)
			    {
                    scenarioCharacter.Voice = xmlCharacter.Attributes[CHARACTER_VOICE].Value;
			    }else
			    {
                    scenarioCharacter.Voice = ScenarioCharacter.NO_VOICE;
			    }
			    
			    
				foreach(XmlElement xmlProperty in xmlCharacter.ChildNodes)
				{
					if (!xmlProperty.Name.ToUpper().Equals(PROPERTY.ToUpper()))
						throw new ScenarioParserException("Invalid tag " + xmlProperty.Name + " in xml for character: " + scenarioCharacter.Name);

					attribList.Clear();
					attribList.Add(PROPERTY_NAME);
					attribList.Add(PROPERTY_VALUE);
					CheckAttributes(xmlProperty, scenario.Name, attribList);

					string propertyName = xmlProperty.Attributes[PROPERTY_NAME].Value;
					string propertyValue = xmlProperty.Attributes[PROPERTY_VALUE].Value;

					ParticipantProperty participantProperty = new ParticipantProperty(scenarioCharacter.Name, propertyName, propertyValue, true);

					scenarioCharacter.Properties.Add(participantProperty);
				}

				scenario.Cast.Add(scenarioCharacter);
			}
		}

		private void parseProps(XmlElement child, Scenario scenario)
		{
			foreach (XmlElement xmlObject in child.ChildNodes)
			{
				if(!xmlObject.Name.ToUpper().Equals(OBJECT.ToUpper()))
					throw new ScenarioParserException("Invalid tag in props: " + xmlObject.Name);

				//Check if the attributes exist
				ArrayList attribList = new ArrayList();
				attribList.Add(OBJECT_NAME);
			    attribList.Add(OBJECT_MODEL);
				CheckAttributes(xmlObject, scenario.Name, attribList);

				string name = xmlObject.Attributes[OBJECT_NAME].Value;
                string model = xmlObject.Attributes[OBJECT_MODEL].Value;
				ScenarioObject scenarioObject = new ScenarioObject(name, model);

				foreach(XmlElement xmlProperty in xmlObject.ChildNodes)
				{
					if (!xmlProperty.Name.ToUpper().Equals(PROPERTY.ToUpper()))
						throw new ScenarioParserException("Invalid tag " + xmlProperty.Name + " in xml for object: " + scenarioObject.Name);

					attribList.Clear();
					attribList.Add(PROPERTY_NAME);
					attribList.Add(PROPERTY_VALUE);
					CheckAttributes(xmlProperty, scenario.Name, attribList);

					string propertyName = xmlProperty.Attributes[PROPERTY_NAME].Value;
					string propertyValue = xmlProperty.Attributes[PROPERTY_VALUE].Value;

					ParticipantProperty participantProperty = new ParticipantProperty(scenarioObject.Name, propertyName, propertyValue, true);

					scenarioObject.Properties.Add(participantProperty);
				}
                scenario.Props.Add(scenarioObject);
			}
		}

		private void parseEpisodes(XmlElement child, Scenario scenario)
		{
			foreach(XmlElement xmlEpisode in child.ChildNodes)
			{
				if (!xmlEpisode.Name.ToUpper().Equals(EPISODE.ToUpper()))
					throw new ScenarioParserException("Invalid tag in episodes: " + xmlEpisode.Name);

				ArrayList attribList = new ArrayList();
				attribList.Clear();
				attribList.Add(EPISODE_FILE);
				CheckAttributes(xmlEpisode, scenario.Name, attribList);

				string episodeFile = xmlEpisode.Attributes[EPISODE_FILE].Value;
				Episode.EpisodeTypeEnum episodeType;

				if (xmlEpisode.Attributes[EPISODE_TYPE] != null)
				{
					switch(xmlEpisode.Attributes[EPISODE_TYPE].Value)
					{
						case EPISODE_TYPE_INTRO:
							episodeType = Episode.EpisodeTypeEnum.INTRO;
							break;
						case EPISODE_TYPE_NORMAL:
							episodeType = Episode.EpisodeTypeEnum.NORMAL;
							break;
						case EPISODE_TYPE_OUTRO:
							episodeType = Episode.EpisodeTypeEnum.OUTRO;
							break;
					    case EPISODE_TYPE_INTERACTION:
                            episodeType = Episode.EpisodeTypeEnum.INTERACTION;
                            break;					        
						default:
							throw new ScenarioParserException("Error parsing scenario, invalid episode type: " + xmlEpisode.Attributes[EPISODE_TYPE].Value);							
					}

				}
				else //Default value
				{
					episodeType = Episode.EpisodeTypeEnum.NORMAL;
				}
				
				Episode episode;

				try
				{
					episode = (Episode)EpisodeParser.Instance.ParseEpisode(SCENARIOS_FOLDER + "\\" + scenario.Name + "\\episodes\\" + episodeFile + ".xml", scenario.Language);
					episode.EpisodeType = episodeType;
				    episode.Triggers = ExpandAllTriggers(episode.Triggers, scenario.Cast);
					scenario.Episodes.Add(episode);
				}
				catch(Exception e)
				{
                    ApplicationLogger.Instance().WriteLine("Error reading episode " + SCENARIOS_FOLDER + "\\" + scenario.Name + "\\episodes\\" + "\\" + episodeFile + ".xml" + "\n");
                    ApplicationLogger.Instance().WriteLine(e.Message);    
                    throw new Scenario.ScenarioException(
                        "Error reading episode " + SCENARIOS_FOLDER + "\\" + scenario.Name + "\\episodes\\" + "\\" +
                        episodeFile + ".xml" + "\n", e);
				}
				
			}
		}


		protected override void ParseElements(XmlDocument xml, object elements)
		{
			throw new NotImplementedException();
		}

        private static ArrayList ExpandAllTriggers(ArrayList triggers, ArrayList characters)
        {
            ArrayList groundedTriggerList = new ArrayList();
            List<string> characterIDs = new List<string>();
            foreach (ScenarioCharacter character in characters)
            {
                characterIDs.Add(character.Name);
            }
            foreach (Trigger trigger in triggers)
            {
                groundedTriggerList.AddRange(ExpandTrigger(trigger, characterIDs));
            }
            return groundedTriggerList;
        }

        private static List<Trigger> ExpandTrigger(Trigger trigger, ICollection<string> variableValues)
        {
            if (variableValues.Count == 0)
            {
                throw new StoryFacilitator.StoryFacilitatorException("ScenarioParser: ExandingTriggers -> Variable values cannot be empty");
            }
            if (trigger.GetVariableList().Count == 0)
            {
                List<Trigger> triggers = new List<Trigger>();
                triggers.Add(trigger);
                return triggers;
            }
            else
            {
                List<Trigger> triggers = new List<Trigger>();
                string firstVariable = trigger.GetFirstVariable();
                foreach (string variableValue in variableValues)
                {
                    Trigger newTrigger = new Trigger(trigger, true);
                    Conditions.Substitution substitution = new Conditions.Substitution(firstVariable, variableValue);
                    newTrigger.ApplySubstitution(substitution);
                    triggers.AddRange(ExpandTrigger(newTrigger, variableValues));
                }
                return triggers;
            }
        }
	}
}
