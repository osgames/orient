using System;
using System.Collections;

namespace Narrative
{
    /// <summary>
    /// Summary description for Scenario.
    /// </summary>
    [Serializable()]
    public class Scenario
    {

        #region Fields

        private string name;
        private string language;
        private string userLanguageSet;
        private string userLanguage;
        private ArrayList cast;
        private ArrayList props;
        private ArrayList episodes;

        #endregion

        #region Properties

        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        public string UserLanguage
        {
            get { return userLanguage; }
            set { userLanguage = value; }
        }

        public ArrayList Cast
        {
            get { return cast; }
            set { cast = value; }
        }

        public ArrayList Props
        {
            get { return props; }
            set { props = value; }
        }

        public ArrayList Episodes
        {
            get { return episodes; }
            set { episodes = value; }
        }

        public string UserLanguageSet
        {
            get { return userLanguageSet; }
            set { userLanguageSet = value; }
        }

        public string Language
        {
            get { return language; }
            set { language = value; }
        }

        #endregion

        #region Exceptions

        public class ScenarioException : Exception
        {
            public ScenarioException()
            {
            }
            public ScenarioException(string errorMessage)
                : base(errorMessage)
            {
            }
            public ScenarioException(string errorMessage, Exception innerException)
                : base(errorMessage, innerException)
            {
            }
        }

        #endregion

        public Scenario(string name, string language, string userLanguage, string userLanguageSet)
        {
            Name = name;
            UserLanguage = userLanguage;
            UserLanguageSet = userLanguageSet;
            Cast = new ArrayList();
            Props = new ArrayList();
            Episodes = new ArrayList();
            Language = language;
        }

        #region Methods

        public ScenarioCharacter GetCharacter(string name)
        {
            foreach (ScenarioCharacter scenarioCharacter in cast)
            {
                if (scenarioCharacter.Name == name)
                    return scenarioCharacter;
            }
            throw new ScenarioException("Character " + name + " not found in scenario " + this.Name);
        }

        public ScenarioObject GetObject(string name)
        {
            foreach (ScenarioObject scenarioObject in props)
            {
                if (scenarioObject.Name == name)
                    return scenarioObject;
            }
            throw new ScenarioException("Object " + name + " not found in scenario " + this.Name);
        }

        public override string ToString()
        {
            string res = "SCENARIO:" + Name + ":" + UserLanguage + ":" + userLanguageSet + "\n";
            res += "CAST\n";
            foreach (ScenarioCharacter scenarioCharacter in cast)
                res += scenarioCharacter.ToString();
            res += "PROPS\n";
            foreach (ScenarioObject scenarioObject in props)
                res += scenarioObject.ToString();
            res += "EPISODES\n";
            foreach (Episode episode in episodes)
                res += episode.ToString();
            return res;
        }

        #endregion

    }
}
