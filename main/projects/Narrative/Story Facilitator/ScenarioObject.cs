// ObjectParticipant - Class that represents an object in an episode
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Collections;
using Narrative.Participants;

namespace Narrative
{
	/// <summary>
	/// An object which the characters can interact with
	/// </summary>
	[Serializable()]
	public class ScenarioObject
	{
		#region Fields
		private string name;
	    private string model;
		private ArrayList properties;
		#endregion

		#region Properties
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		public ArrayList Properties
		{
			get { return properties; }
			set { properties = value; }
		}

	    public string Model
	    {
	        get { return model; }
	        set { model = value; }
	    }

	    #endregion

		public ScenarioObject(string name, string model)
		{
			Name = name;
		    Model = model;
			properties = new ArrayList();
		}

		public override string ToString()
		{
			string res = "OBJECT_PARTICIPANT:" + Name + ":" + model + "\n";

			res+="PROPERTIES\n";
			foreach(ParticipantProperty p in Properties)
				res+=p.ToString();

			return res + "\n";
		}
	}
}
