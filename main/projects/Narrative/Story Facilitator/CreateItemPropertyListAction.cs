using System;
using System.Collections;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Events;
using ION.Realizer.Orient.Entities;
using Narrative.Participants;
using Property=ION.Core.Property;

namespace Narrative
{
    public class CreateItemPropertyListAction : Action, IEventCallback<ION.Core.Events.PropertyCreated>, IEventCallback<ION.Core.Events.PropertyChanged>
    {
        #region Constants

        public const string ACTION_NAME = "CreateItemPropertyListAction";

        //Parameters
        public const string ITEM_PROPERTIES_LIST = "ITEM_PROPERTIES_LIST";

        #endregion

        public ArrayList properties;
        
        public CreateItemPropertyListAction()
        {
            properties = new ArrayList();
        }

        protected override void OnStart(Context context)
        {
            Arguments arguments = context.Arguments;
            properties = (arguments[ITEM_PROPERTIES_LIST] as ArrayList).Clone() as ArrayList;
            
            //Assign the properties to the entity
            foreach (ParticipantProperty participantProperty in properties)
            {
                Entity item = Universe.Instance.GetEntity<Entity>(participantProperty.Holder);
                item.CreateProperty<Property<string>>(participantProperty.Name);
            }
        }

        protected override void OnEnd(Context context)
        {            
            properties = new ArrayList();
        }

        protected override void OnStep(Context context)
        {
            if (HasEnded())
                this.End();
        }

        protected bool HasEnded()
        {
            return (properties.Count == 0);
        }

        protected override void OnCreate(Arguments arguments)
        {
            //Register the event listner for property created
            Universe.Instance.CreateEventListener<PropertyCreated>(this);
            Universe.Instance.CreateEventListener<PropertyChanged>(this);
        }


        public void Invoke(PropertyCreated e)
        {
            if (e.Parent is Entity)
            {
                foreach (ParticipantProperty participantProperty in (ArrayList)properties.Clone())
                {
                    if ((e.Property.Name == participantProperty.Name) && (e.Parent.Name == participantProperty.Holder))
                    {
                        (e.Property as Property<string>).Value = participantProperty.Value;
                    }
                }
            }            
        }

        public void Invoke(PropertyChanged e)
        {
            if (e.Property.Parent is Entity)
            {
                foreach (ParticipantProperty participantProperty in (ArrayList)properties.Clone())
                {
                    if ((e.Property.Name == participantProperty.Name) && (e.Property.Parent.Name == participantProperty.Holder))
                    {
                        properties.Remove(participantProperty);//The property was created and its value was set, remove it from the list of properties awaiting confirmation
                    }
                }
            }            
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
