using System;
using System.Collections.Generic;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Events;
using ION.Realizer.Orient.Entities;

namespace Narrative
{
    public class RemoveAllItemsAndUserAction : Action, IEventCallback<ION.Core.Events.EntityDestroyed>
    {

        #region Constants
        public const string ACTION_NAME = "RemoveAllItemsAndUserAction";
        #endregion

        public List<ulong> itemUIDs;
        
        public RemoveAllItemsAndUserAction()
        {
            itemUIDs = new List<ulong>();
        }

        protected override void OnStart(Context context)
        {
            foreach (Entity entity in Universe.Instance.Entities)
            {
                if (entity is Item || entity is ModellessItem || entity is UserEntity)
                {
                    itemUIDs.Add(entity.UID);
                    Universe.Instance.DestroyEntity(entity.Name);
                }
            }
        }


        protected override void OnEnd(Context context)
        {
            itemUIDs = new List<ulong>();
        }

       protected bool HasEnded()
        {
            return (itemUIDs.Count == 0);
        }

        protected override void OnCreate(Arguments arguments)
        {
            Universe.Instance.CreateEventListener<ION.Core.Events.EntityDestroyed>(this);
        }

        protected override void OnDestroy()
        {            
        }

        public void Invoke(EntityDestroyed e)
        {
            if (itemUIDs.Contains(e.Entity.UID))
            {
                itemUIDs.Remove(e.Entity.UID);
            }
        }

        protected override void OnStep(Context context)
        {
            if (HasEnded())
            {
                this.End();
            }
        }

        protected override void OnFail(Context context)
        {
            throw new NotImplementedException();
        }
    }
}
