using System;
using System.Collections;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Events;
using Narrative.Participants;

namespace Narrative
{
    /// <summary>
    /// Action that receives an ArrayList as an argument,PROPERTIES_LIST, which contains ParticipantProperties
    /// For each of those ParticipantProperties this action whill create the property and set its value in the correnponding
    /// RemoteCharacter
    /// </summary>
    public class CreatePropertyListAction : Action, IEventCallback<PropertyCreated>, IEventCallback<ION.Core.Events.PropertyChanged>
    {
        #region Constants        
        
        public const string ACTION_NAME = "CreatePropertyListAction";
        
        //Parameters
        public const string PROPERTIES_LIST = "PROPERTIES_LIST";
                
        
        #endregion

        public ArrayList storyProperties;        
        
        
        public CreatePropertyListAction()
        {
            storyProperties = new ArrayList();            
        }
        protected override void OnStart(Context context)
        {
            Arguments arguments = context.Arguments;
            storyProperties = (arguments[PROPERTIES_LIST] as ArrayList).Clone() as ArrayList;            
           
            //Assign the properties to that entity
            foreach (ParticipantProperty participantProperty in storyProperties)
            {

                Entity remoteCharacter =
                    Universe.Instance.GetEntity(participantProperty.Holder);

                if (!remoteCharacter.HasProperty<Property<string>>(participantProperty.Name))
                    remoteCharacter.CreateProperty<Property<string>>(participantProperty.Name);
                else
                    ((Property<string>) remoteCharacter.GetProperty(participantProperty.Name)).Value =
                        participantProperty.Value;
            }
            
        }

        protected override void OnEnd(Context context)
        {
            storyProperties = new ArrayList();            
        }

        protected override void OnStep(Context context)
        {
            if (HasEnded())
            {
                this.End();
            }
        }

        protected bool HasEnded()
        {
            if (storyProperties.Count == 0) //The action will finish upon confirmation of the creation of all properties
                return true;
            else
                return false;
        }

        protected override void OnCreate(Arguments arguments)
        {
            //Register the event listner for property created
            Universe.Instance.CreateEventListener<PropertyCreated>(this);
            Universe.Instance.CreateEventListener<PropertyChanged>(this);
        }



        #region Callbacks
        public void Invoke(PropertyCreated e)
        {
            if (e.Parent is Entity)
            {
                foreach (ParticipantProperty participantProperty in (ArrayList)storyProperties.Clone())
                {
                    if ((e.Property.Name == participantProperty.Name)&&(e.Parent.Name == participantProperty.Holder))
                    {
                        (e.Property as Property<string>).Value = participantProperty.Value;
                    }
                }
            }
        }

        public void Invoke(PropertyChanged e)
        {
            if (e.Property.Parent is Entity)
            {
                foreach (ParticipantProperty participantProperty in (ArrayList)storyProperties.Clone())
                {
                    if ((e.Property.Name == participantProperty.Name) && (e.Property.Parent.Name == participantProperty.Holder))
                    {
                        storyProperties.Remove(participantProperty);//The property was created and its value was set, remove it from the list of properties awaiting confirmation                        
                    }
                }
            }
        }
        
        #endregion

        protected override void OnFail(Context context)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
