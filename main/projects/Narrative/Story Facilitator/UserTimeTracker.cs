using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Narrative
{
    public class UserTimeTracker
    {
        #region Constants
        public const string LOG_FILE_PREFIX = "time_log_";
        public const string LOG_FILE_SUFIX = ".txt";
        #endregion

        #region Fields

        private string userCode;
        private StreamWriter sWriter;
        private DateTime previousTime;
        private DateTime startTime;
        private string logsDir = "logs\\";
        
        #endregion

        #region Properties
        
        public string UserCode
        {
            get { return userCode; }            
        }

        public string LogsDir
        {
            get { return logsDir; }
            set { logsDir = value; }
        }

        #endregion


        public UserTimeTracker(string logsDir, string userCode)
        {
            this.userCode = userCode;
            previousTime = DateTime.Now;
            startTime = DateTime.Now;
            this.LogsDir = logsDir;
            
            if (!Directory.Exists(LogsDir + StoryFacilitator.USERS_DIR + "\\" + userCode))
            {
                Directory.CreateDirectory(LogsDir + StoryFacilitator.USERS_DIR + "\\" + userCode);
            }

            if (File.Exists(LogsDir + StoryFacilitator.USERS_DIR + "\\" + userCode + "\\" + LOG_FILE_PREFIX + userCode + LOG_FILE_SUFIX))
            {
                sWriter = File.AppendText(LogsDir + StoryFacilitator.USERS_DIR + "\\" + userCode + "\\" + LOG_FILE_PREFIX + userCode + LOG_FILE_SUFIX);
                sWriter.WriteLine();
                sWriter.Flush();
            }else
            {
                sWriter = File.CreateText(LogsDir + StoryFacilitator.USERS_DIR + "\\" + userCode + "\\" + LOG_FILE_PREFIX + userCode + LOG_FILE_SUFIX);
            }
        }
        
        ~UserTimeTracker()
        {
            End();            
        }
        
        public void End()
        {
            try
            {
                TimeSpan elapsedTime = DateTime.Now - startTime;
                sWriter.WriteLine("The user interacted with the aplication for: " + elapsedTime.Minutes + " minutes and " + elapsedTime.Seconds + " seconds");
                sWriter.Flush();
                sWriter.Close();   
            }catch(Exception)
            {
                //Ignore
            }            
        }
        
        public void EpisodeStart(string episodeName, string episodeType)
        {
            sWriter.WriteLine("Episode " + episodeName + " started at: " + DateTime.Now + ". This episode is of type: " + episodeType + " episode.");
            previousTime = DateTime.Now;
            sWriter.Flush();
        }                
        
        public void EpisodeEnd(string episodeName)
        {
            TimeSpan elapsedTime = DateTime.Now - previousTime;
            sWriter.WriteLine("Episode " + episodeName + " ended at: " + DateTime.Now + ". The episode took " + elapsedTime.TotalSeconds + " seconds to complete.");
            previousTime = DateTime.Now;
            sWriter.Flush();
        }
        
        public double GetTotalTimeInSeconds()
        {
            TimeSpan elapsedTime = DateTime.Now - startTime;
            return elapsedTime.TotalSeconds;
        }

    }
}
