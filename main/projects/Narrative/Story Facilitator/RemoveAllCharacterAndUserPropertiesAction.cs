using System;
using System.Collections;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Events;
using ION.Realizer.Orient.Entities;
using Narrative.Participants;

namespace Narrative
{
    /// <summary>
    /// Removes all the episode-level properties from the RemoteCharacter entities and UserEntity and removes the episode-level properties from 
    /// the story facilitator's memory
    /// </summary>
    public class RemoveAllCharacterAndUserPropertiesAction : Action, IEventCallback<ION.Core.Events.PropertyDestroyed>
    {

        public const string ACTION_NAME = "RemoveAllCharacterAndUserPropertiesAction";
        
        //Parameters
        public const string STORY_FACILITATOR = "STORY_FACILITATOR";
        
        
        private StoryFacilitator storyFacilitator;
        private ArrayList properties;
        
        public RemoveAllCharacterAndUserPropertiesAction()
        {
            properties = new ArrayList();
        }
        
        protected override void OnStart(Context context)        
        {
            Arguments arguments = context.Arguments;
            storyFacilitator = arguments[STORY_FACILITATOR] as StoryFacilitator;            

            foreach (Entity entity in Universe.Instance.Entities)
            {
                if (entity is RemoteCharacter || entity is UserEntity)
                {
                    ArrayList characterProperties = storyFacilitator.StoryMemory.AskParticipantProperties(entity.Name);

                    foreach (Property property in entity.Properties)
                    {
                        foreach (ParticipantProperty participantProperty in characterProperties)
                        {
                            if ((participantProperty.Name == property.Name) && (!participantProperty.StoryLevel))
                            {
                                storyFacilitator.StoryMemory.Retract(participantProperty); //Remove from SF's memory (episode-level property)
                                property.Destroy();
                                this.properties.Add(property.UID);
                            }
                        }
                    }
                }
            }
        }


        protected override void OnEnd(Context context)
        {            
            storyFacilitator = null;
            properties = new ArrayList();
        }

        protected override void OnStep(Context context)
        {
            if (HasEnded())
            {
                this.End();
            }
        }

        protected bool HasEnded()
        {
            if (properties.Count == 0)
                return true;
            else 
                return false;            
        }

        protected override void OnCreate(Arguments arguments)
        {
            Universe.Instance.CreateEventListener<PropertyDestroyed>(this);
        }

   

        public void Invoke(PropertyDestroyed e)
        {
            if (e.OldParent is RemoteCharacter)
            {                
                if (properties.Contains(e.Property.UID))
                    properties.Remove(e.Property.UID);
            }
            
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
