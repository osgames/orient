// CharacterParticipant - Class that represents a character in an episode
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//
using System;
using System.Collections;
using Narrative.Participants;

namespace Narrative
{
	/// <summary>
	/// Summary description for CharacterParticipant.
	/// </summary>
	[Serializable()]
	public sealed class ScenarioCharacter 
	{
        public const string NO_VOICE = "NoVoice";

		#region Fields

		private string name;
		private string displayName;
		private string body;
	    private string agentLanguage;
	    private string agentLanguageSet;
	    private string voice;
		private ArrayList properties;

		#endregion
		
		#region Properties

		public string DisplayName
		{
			get
			{
				return displayName;
			}
			set
			{
				displayName = value;
			}
		}


		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		public string Body
		{
			get { return body; }
			set { body = value; }
		}

		public ArrayList Properties
		{
			get { return properties; }
			set { properties = value; }
		}

	    public string AgentLanguage
	    {
	        get { return agentLanguage; }
	        set { agentLanguage = value; }
	    }

	    public string AgentLanguageSet
	    {
	        get { return agentLanguageSet; }
	        set { agentLanguageSet = value; }
	    }

	    public string Voice
	    {
	        get { return voice; }
	        set { voice = value; }
	    }

	    #endregion
		
		public ScenarioCharacter(string name, string displayName, string body, string agentLanguage, string agentLanguageSet)
		{
			Name = name;
			DisplayName = displayName;
			Body = body;
            AgentLanguage = agentLanguage;
            AgentLanguageSet = agentLanguageSet;
            Voice = NO_VOICE;
			Properties = new ArrayList();     			
		}


        public override bool Equals(object obj)
        {
            if (obj is ScenarioCharacter)
            {
                ScenarioCharacter sc = obj as ScenarioCharacter;
                if (sc.Name == this.Name)
                    return true;
                else
                    return false;
            }else return false;
        }

        public override int GetHashCode()
        {
            return (Name + DisplayName + Body + AgentLanguage + AgentLanguageSet + Voice).GetHashCode();
        }
	    
		public override string ToString()
		{
            string res = "SCENARIO_CHARACTER:" + Name + ":" + DisplayName + ":" + Body + ":" + AgentLanguage + ":" + AgentLanguageSet + ":" + Voice + "\n";

			res+="PROPERTIES\n";
			foreach(ParticipantProperty p in Properties)
				res+=p.ToString();

			return res + "\n";
		}

	}
}
