using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;

namespace Narrative
{
    class StoryFaciliatorUpdateAction : Action
    {
        private StoryFacilitator storyFacilitator;    

        public StoryFaciliatorUpdateAction()
        {            
        }
            
        protected override void  OnStart(Context context)
        {           
 	       this.storyFacilitator = context.Arguments[StoryFacilitator.FACILITATOR_NAME] as StoryFacilitator;
        }



        protected override void OnStep(Context context)
        {
            storyFacilitator.Update();
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
            throw new NotImplementedException();
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
