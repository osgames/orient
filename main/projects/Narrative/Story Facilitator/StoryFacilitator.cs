// StoryFacilitator -  Class responsible for:
//					   The selection of episodes
//					   Storing and updating the properties of the characters in the StoryFacilitator
//					   Monitoring the triggers of the current episode
//					   Monitoring the events from the framework and to update the StoryFacilitator with those events	
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
// Jo�o Dias: 19/06/2006 - Removed the Stop Method. It doesn't make sense to be the story manager
//						   to remove all agents from the universe when the story ends because he is
//						   one of the agents that will be removed.
//
// Rui Figueiredo: 24/09/2006 14:48 - SF now inherits from ION.Core.Entity
//                                  - Overrides OnCreate, OnDestroy (same behavior as before)
//                                  - An important diference is that the update method will now be called by
//                                  - an action named StoryFaciliatorUpdateAction
//                                  - The Name (old ID.Name) is now set when the entity (StoryFacilitator)
//                                  - is added to the universe (requirement of the new framework)
//                                  - The new framework does not provide the elapsed time betweem two ticks
//                                  - so in order to determine the elapsed time, the system time of the last update is stored
//                                  - and the diference between the current system time and the previous system time is calculated 
//                                  - in the update method
// NOTE: It is still necessary to remove currentPerspective

using System;
using System.Collections;
using System.Collections.Generic;
using FAtiMA.RemoteAgent;
using FAtiMA.Actions;
using ION.Core;
using ION.Core.Events;
using ION.Core.Extensions;
using ION.Realizer.Orient.Actions.Body.Behaviours;
using ION.Realizer.Orient.Entities;
using Narrative.Actions;
using Narrative.Conditions;
using Narrative.Participants;
using Narrative.States;
using User.Interface;
using Narrator=ION.Realizer.Orient.Entities.Narrator;

namespace Narrative
{
	/// <summary>
	/// Class responsible for:
	/// The selection of episodes
	/// Storing and updating the properties of the characters in the StoryMemory
	/// Monitoring the triggers of the current episode
	/// Monitoring the events from the framework and to update the KnowlegeBase with those events	
	/// </summary>
	[Serializable()]
    public class StoryFacilitator : Entity, 
                                    IEventCallback<ActionStarted>, 
                                    IEventCallback<ActionEnded>, 
                                    IEventCallback<EntityCreated>,
                                    IEventCallback<EntityDestroyed>,
                                    IEventCallback<PropertyChanged<Property<string>>>,
                                    IEventCallback<PropertyDestroyed>, 
                                    IEventCallback<PropertyCreated>                                
    {
		#region Constants
        public const string EPISODE_LIST_FILE = "episode_list.txt";
        public const string FACILITATOR_NAME = "StoryFacilitator";
        public const string NARRATOR_NAME = "NarratorEntity";
        public const string STORY_FACILITATOR_UPDATE_ACTION = "StoryFacilitatorUpdateAction";
        public const string USERS_DIR = "users";
        #endregion

		#region Exceptions

		public class StoryFacilitatorException : Exception
		{
			public StoryFacilitatorException()
			{
			}
			public StoryFacilitatorException(string errorMessage) : base(errorMessage)
			{
			}
			public StoryFacilitatorException(string errorMessage, Exception innerException) :base(errorMessage, innerException)
			{
			}
		}

	    public class NoEpisodesException : Exception
        {
            public NoEpisodesException()
            {
            }
            public NoEpisodesException(string errorMessage)
                : base(errorMessage)
            {
            }
            public NoEpisodesException(string errorMessage, Exception innerException)
                : base(errorMessage, innerException)
            {
            }
        }
	    

		#endregion

		#region Fields

		private Scenario scenario;		
		public ArrayList notSelectedEpisodes;
		private StoryMemory storyMemory;
		public Episode activeEpisode;
        private DateTime previousSystemTime;
        private StoryFaciliatorUpdateAction updateAction;
	    private CharacterManager characterManager;
	    //The userTimeTracker is initialized in the start state
	    private UserTimeTracker userTimeTracker;
	    private SessionInfo userSessionInfo = null;
        public ExcelSessionTimeTracker excelTimeTracker = null;
		
	    //Contains the list of all the event listners where SF is registered
	    [NonSerialized]
        private ArrayList eventListners;

		private State state;

		#endregion

		#region Properties

		public Episode ActiveEpisode
		{
			get
			{
                if (activeEpisode != null)
                {
                    if (!activeEpisode.Validate())
                    {
                        throw new StoryFacilitatorException(
                            "Invalid episode selected (EventConditions with CurrentEpisode flag set to true, but without episodeID info...). Use Episode.GetCopy before setting the ActiveEpisode");
                    }
                    return activeEpisode;
                }else
                    throw new StoryFacilitatorException("No active episode yet!");
			}
			set { activeEpisode = value; }
		}

		public StoryMemory StoryMemory
		{
			get { return storyMemory; }
			set { storyMemory = value; }
		}

		public Scenario Scenario
		{
			get { return scenario; }
			set { scenario = value; }
		}

	    public CharacterManager CharacterManager
	    {
	        get { return characterManager; }
	        set { characterManager = value; }
	    }

	    public UserTimeTracker UserTimeTracker
	    {
	        get { return userTimeTracker; }
	        set { userTimeTracker = value; }
	    }

	    public SessionInfo UserSessionInfo
	    {
	        get {
                if (userSessionInfo == null)
                    throw new StoryFacilitatorException("Undefined user session");
	            else 
	                return userSessionInfo; 
	        }
	        set { userSessionInfo = value; }
	    }

	    #endregion 


		public StoryFacilitator()
		{
            //Note: StoryFacilitator's name is set when the entity is added to the framework
            //The user interface is set right next to the creation of the SF, in RenderWindow constructor
            //See the set method of the Property UserInterface
            activeEpisode = null;
            notSelectedEpisodes = new ArrayList();
            storyMemory = new StoryMemoryDecorator(); //The decorator is used to log the StoryMemory		
            state = StartState.Instance();
            characterManager = new CharacterManager();
            previousSystemTime = DateTime.Now;
		    eventListners = new ArrayList();
        }
	    
	    public void Init()
	    {
            activeEpisode = null;
            notSelectedEpisodes = new ArrayList();
	        if (this.storyMemory != null)
	        {
	            (this.storyMemory as StoryMemoryDecorator).Close();
	        }
	        if (this.UserTimeTracker != null)
	        {
	            this.UserTimeTracker.End();
	        }

            if (this.excelTimeTracker != null)
            {
                try
                {
                    this.excelTimeTracker.End();
                }
                catch (Exception ex)
                {
                    ApplicationLogger.Instance().WriteLine("SF INIT: NON-CRITICAL ERROR: ExcelTimeTracker did not End() well: " + ex.Message);
                }
            }
            storyMemory = new StoryMemoryDecorator(); //The decorator is used to log the StoryMemory		
            state = StartState.Instance();
            characterManager = new CharacterManager();
            previousSystemTime = DateTime.Now;
	        //The episode selection state is reset (Starts by choosing INTRO episodes, then NORMAL, then OUTRO)
	        (SelectNextEpisodeState.Instance() as SelectNextEpisodeState).Init();
	        eventListners = new ArrayList();
	    }


        /// <summary>
		/// Reads the scenario and episodes from scenario
		/// Updates StoryMemory with properties of Cast and Props
		/// </summary>
		public void ReadScenario(string scenarioLocation)
		{
            try
            {
                Scenario = (Scenario) ScenarioParser.Instance.ParseFile(scenarioLocation);
            }catch(Exception e)
            {
                throw new StoryFacilitatorException("Error reading/opening scenario: " + e.Message);
            }
            ApplicationLogger.Instance().Write("Finished loading scenario:");
			ApplicationLogger.Instance().Write(scenario.ToString());

			//Put all properties of characters and objects in the StoryMemory
			foreach(ScenarioCharacter character in Scenario.Cast)
			{
				foreach(ParticipantProperty property in character.Properties)
					this.StoryMemory.Tell(property);
			}
			foreach(ScenarioObject item in Scenario.Props)
			{
				foreach(ParticipantProperty property in item.Properties)
					this.StoryMemory.Tell(property);				
			}

			notSelectedEpisodes = (ArrayList)scenario.Episodes.Clone();			
		}

		/// <summary>
		/// Returns the next episode to be played.
		/// Updates the notSelectedEpisodes list by removing this new episode from there
		/// NOTE: Does not ignore INTERACTION episodes and does not take into account if the episode is an INTRO, NORMAL or OUTRO
		/// </summary>
		/// <returns></returns>
		public void SelectNextEpisode()
		{
			ArrayList selectableEpisodes = new ArrayList();
			foreach(Episode e in notSelectedEpisodes)
			{
                if (e.IsSelectable(storyMemory))
					selectableEpisodes.Add(e);
			}
			
			if (selectableEpisodes.Count == 0)
			{
				string episodeNames = "";
				foreach(Episode e in notSelectedEpisodes)
					episodeNames+=e.Name+"\n";
				throw new NoEpisodesException("No episode meets the conditions necessary for selection:\n" + episodeNames);
			}
			

			Episode selectedEpisode = EpisodeSelectionFuntion(selectableEpisodes);

			notSelectedEpisodes.Remove(selectedEpisode);

            this.ActiveEpisode = selectedEpisode.GetCopy();

            this.TellStoryEvent(new StoryEvent(this.Name, StoryEvent.SELECT_EPISODE, selectedEpisode.EpisodeType + " " + selectedEpisode.Name, StoryEvent.EventType.ACTION_FINISHED));
		}

		/// <summary>
		/// Returns the next episode to be played of type episodeType
		/// Updates the notSelectedEpisodes list by removing this new episode from there
		/// </summary>
		/// <returns></returns>
		/// <param name="episodeType"></param>
		public void SelectNextEpisode(Episode.EpisodeTypeEnum episodeType)
		{
			ArrayList selectableEpisodes = new ArrayList();
			foreach(Episode e in notSelectedEpisodes)
			{
				if ((e.EpisodeType == episodeType)&&(e.IsSelectable(storyMemory))) //Don't select interaction episodes
					selectableEpisodes.Add(e);
			}
			
			if (selectableEpisodes.Count == 0)
			{
				string episodeNames = "";
				foreach(Episode e in notSelectedEpisodes)
					episodeNames+=e.Name+"\n";
                throw new NoEpisodesException("No episode of type " + episodeType + " meets the conditions necessary for selection:\n" + episodeNames);
			}
			

			Episode selectedEpisode = EpisodeSelectionFuntion(selectableEpisodes);

			notSelectedEpisodes.Remove(selectedEpisode);

            this.ActiveEpisode = selectedEpisode.GetCopy();

            this.TellStoryEvent(new StoryEvent(this.Name, StoryEvent.SELECT_EPISODE, selectedEpisode.EpisodeType + " " + selectedEpisode.Name, StoryEvent.EventType.ACTION_FINISHED));
		}


		/// <summary>
		/// Selects the 'episodeName' episode as ActiveEpisode
		/// Does not update the notSelectedEpisodes list
		/// </summary>
		/// <returns></returns>
		public void SelectEpisode(string episodeName)
		{			
			Episode episode = null;
			foreach(Episode e in notSelectedEpisodes)
			{
				if (e.Name.Equals(episodeName))
				{
					episode = e;
					break;
				}
			}
			if (episode == null)
                throw new NoEpisodesException("Episode " + episodeName + " not found or already played before");


            this.ActiveEpisode = episode.GetCopy();

            this.TellStoryEvent(new StoryEvent(this.Name, StoryEvent.SELECT_EPISODE, episode.EpisodeType + " " + episode.Name, StoryEvent.EventType.ACTION_FINISHED));
		}

        #region Episode selection from the candidate set
	    
		private Episode EpisodeSelectionFuntion(ArrayList episodes)
		{
		    Random random = new Random();		    
            
		    //The episodes that contain tests in their preconditions that test for actions
		    //of the user have priority. For example, an episode that has a precondition
		    //that tests if the user suggested the coping strategy tell someone,that episode will always be selected
		    //before than an episode that does not have tests to user's actions in its preconditions.
		    ArrayList firstCandidates = new ArrayList();
		    foreach (Episode episode in episodes)
		    {
		        if (HasCopingStrategyTest(episode))
		        {
                    firstCandidates.Add(episode);
		        }
		    }
		    
		    if (firstCandidates.Count != 0)
		    {
		        return ((Episode)firstCandidates[random.Next(0, firstCandidates.Count)]);
            }
            else return ((Episode)episodes[random.Next(0, episodes.Count)]);
		        
		}
	    
	    private bool HasCopingStrategyTest(Episode episode)
	    {
		        foreach (ConditionList conditionList in episode.PreConditions)
		        {
		            foreach (Condition condition in conditionList.GetConditions())
		            {
		                if (condition is PropertyCondition)
		                {
                            if ((condition as PropertyCondition).PropertyName == ParticipantProperty.COPING_STRATEGY)
		                    {
                                return true;
		                    }
		                }
		            }
		        }
	        return false;
	    }
	    
        #endregion
	    
		#region Agent Methods

        public void Update()
        {
            TimeSpan timeSlice = DateTime.Now.Subtract(previousSystemTime);
            previousSystemTime = DateTime.Now;            
            state.UpdateEvent(this, (int)timeSlice.TotalMilliseconds);
        }

        protected override void OnCreate(Arguments arguments)
        {
            updateAction = this.CreateAction<StoryFaciliatorUpdateAction>(STORY_FACILITATOR_UPDATE_ACTION);
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add(FACILITATOR_NAME, this);
            updateAction.Start(new Arguments(dictionary));

            eventListners.Add(Universe.Instance.CreateEventListener<ActionStarted>(this));
            eventListners.Add(Universe.Instance.CreateEventListener<ActionEnded>(this));
            eventListners.Add(Universe.Instance.CreateEventListener<EntityCreated>(this));
            eventListners.Add(Universe.Instance.CreateEventListener<EntityDestroyed>(this));
            eventListners.Add(Universe.Instance.CreateEventListener<PropertyCreated>(this));
            eventListners.Add(Universe.Instance.CreateEventListener<PropertyChanged<Property<string>>>(this));
            eventListners.Add(Universe.Instance.CreateEventListener<PropertyDestroyed>(this));

            this.CreateAction<CreatePropertyListAction>(CreatePropertyListAction.ACTION_NAME);
            this.CreateAction<CreateCharacterEntitiesAction>(CreateCharacterEntitiesAction.ACTION_NAME);
            this.CreateAction<RemoveAllCharacterAndUserPropertiesAction>(RemoveAllCharacterAndUserPropertiesAction.ACTION_NAME);
            this.CreateAction<PutAllCharactersToSleepAction>(PutAllCharactersToSleepAction.ACTION_NAME);
            this.CreateAction<CreateItemPropertyListAction>(CreateItemPropertyListAction.ACTION_NAME);
            this.CreateAction<RemoveAllItemsAndUserAction>(RemoveAllItemsAndUserAction.ACTION_NAME);
            this.CreateAction<RemoveCharacterEntitiesAction>(RemoveCharacterEntitiesAction.ACTION_NAME);
            this.CreateAction<UpdatePropertyListAction>(UpdatePropertyListAction.ACTION_NAME);
            this.CreateAction<RemoveAllEntitiesAction>(RemoveAllEntitiesAction.ACTION_NAME);
        }

        protected override void OnDestroy()
        {
            foreach (EventListener listner in eventListners)
            {
                Universe.Instance.DestroyEventListener(listner);
            }
        }

	    	    

        #region IEventCallback<ActionStarted> Members

        public void Invoke(ActionStarted e)
        {
            if ((e.Action.Parent is RemoteCharacter) || (e.Action.Parent is StoryFacilitator) || (e.Action.Parent is UserEntity) || (e.Action.Parent is Set))
            {
                Entity ent = (Entity)e.Action.Parent;
                string subject = ent.Name;
                string actionName = e.Action.Name;


                if (e.Action.Name == LoadRemoteAgentAction.ACTION_NAME)
                {
                    object parameter;
                    string characterName = "";
                                        
                    if (e.Context.Arguments.TryGetValue(LoadRemoteAgentAction.NAME, out parameter))
                    {
                        characterName = parameter as string;
                    }
                    StoryEvent ev = new StoryEvent(subject, actionName, characterName, StoryEvent.EventType.ACTION_STARTED);
                    this.TellStoryEvent(ev);
                }              
                else if (e.Action is Behaviour)
                {
                    if (e.Action.Name == "say-to")
                    {
                        string parameters;
                        SpeechAct speech = e.Context.Arguments[RemoteCharacter.REMOTE_ACTION_PARAMETERS] as SpeechAct;
                        parameters = speech.Target + " " + speech.Meaning;
                        StoryEvent ev =
                            new StoryEvent((e.Action.Parent as Entity).Name, e.Action.Name, parameters,
                                           StoryEvent.EventType.ACTION_STARTED);
                        this.TellStoryEvent(ev);                        
                    }
                    else if (e.Action.Name == "saySimple")
                    {
                        RemoteAction remoteAction = e.Context.Arguments[RemoteCharacter.REMOTE_ACTION_PARAMETERS] as RemoteAction;
                        string speech = string.Empty;
                        foreach (string str in remoteAction.Parameters)
                        {
                            speech += str + " ";
                        }

                        if (speech != string.Empty)
                            speech = speech.Substring(0, speech.Length - 1);

                        StoryEvent ev =
                            new StoryEvent((e.Action.Parent as Entity).Name, e.Action.Name, speech,
                                           StoryEvent.EventType.ACTION_STARTED);
                        this.TellStoryEvent(ev);
                    }
                    else
                    {
                        RemoteAction remoteAction = (RemoteAction)e.Context.Arguments[RemoteCharacter.REMOTE_ACTION_PARAMETERS];
                        
                        string parameters = string.Empty;

                        if ((remoteAction.Target != null) && (remoteAction.Target != string.Empty))
                        {
                            parameters = remoteAction.Target; 
                        }

                        foreach (string parameter in remoteAction.Parameters)
                        {
                            if (parameters == string.Empty)
                                parameters = parameter + " ";
                            else
                            {
                                parameters += parameter + " ";
                            }
                        }

                        parameters = parameters.TrimEnd(null);
                        
                        StoryEvent ev = new StoryEvent(remoteAction.Subject, remoteAction.ActionType, parameters,
                                           StoryEvent.EventType.ACTION_STARTED);
                        this.TellStoryEvent(ev);
                    }

                }
                else if (e.Action is CreatePropertyListAction)
                {
                    this.TellStoryEvent(new StoryEvent(this.Name, CreatePropertyListAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_STARTED));
                }
                else if (e.Action is CreateCharacterEntitiesAction)
                {
                    this.TellStoryEvent(new StoryEvent(this.Name, CreateCharacterEntitiesAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_STARTED));
                }
                else if (e.Action is RemoveAllCharacterAndUserPropertiesAction)
                {
                    this.TellStoryEvent(new StoryEvent(this.Name, RemoveAllCharacterAndUserPropertiesAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_STARTED));
                }
                else if (e.Action is PutAllCharactersToSleepAction)
                {
                    this.TellStoryEvent(new StoryEvent(this.Name, PutAllCharactersToSleepAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_STARTED));
                }
                else if (e.Action is CreateItemPropertyListAction)
                {
                    this.TellStoryEvent(new StoryEvent(this.Name, CreateItemPropertyListAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_STARTED));
                }
                else if (e.Action is RemoveAllItemsAndUserAction)
                {
                    this.TellStoryEvent(new StoryEvent(this.Name, RemoveAllItemsAndUserAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_STARTED));
                }
                else if (e.Action is AwakeAction)
                {
                    this.TellStoryEvent(new StoryEvent(subject, actionName, "", StoryEvent.EventType.ACTION_STARTED));
                }
                else if (e.Action is PutToSleepAction)
                {
                    this.TellStoryEvent(new StoryEvent(subject, actionName, "", StoryEvent.EventType.ACTION_STARTED));
                }
                else if (e.Action is RemoveCharacterEntitiesAction)
                {
                    this.TellStoryEvent(new StoryEvent(subject, actionName, "", StoryEvent.EventType.ACTION_STARTED));
                }else if (e.Action is UpdatePropertyListAction)
                {
                    this.TellStoryEvent(new StoryEvent(subject, actionName, "", StoryEvent.EventType.ACTION_STARTED));
                }else if (e.Action is RemoveAllEntitiesAction)
                {
                    this.TellStoryEvent(new StoryEvent(subject, actionName, "", StoryEvent.EventType.ACTION_STARTED));
                }
                else if (e.Action.Parent is UserEntity)
                {
                    string parameters = "";
                    if (e.Context.Arguments.ContainsKey("target"))
                        parameters = e.Context.Arguments["target"] + " ";
                    if (e.Context.Arguments.ContainsKey("parameters"))
                        parameters = parameters + e.Context.Arguments["parameters"];

                    this.TellStoryEvent(new StoryEvent(subject, actionName, parameters, StoryEvent.EventType.ACTION_STARTED));
                }

            }
            else if (e.Action.Parent is Narrator)
            {
                Entity ent = (Entity)e.Action.Parent;
                string subject = ent.Name;
                string actionName = e.Action.Name;

                if (actionName == "say")
                {
                    string utterance = e.Context.Arguments["utterance"] as string;
                    this.TellStoryEvent(new StoryEvent(subject, actionName, utterance, StoryEvent.EventType.ACTION_STARTED));
                }
            }else if (e.Action.Parent is Set)
            {
                this.TellStoryEvent(new StoryEvent(e.Action.Parent.Name, e.Action.Name, e.Context.Arguments.Get<string>(ActForEnvironmentAction.ACTION_PARAMETERS), StoryEvent.EventType.ACTION_STARTED));   
            }
                        
        }

        #endregion

        #region IEventCallback<ActionEnded> Members

        public void Invoke(ActionEnded e)
        {            
            if ((e.Action.Parent is RemoteCharacter) || (e.Action.Parent is StoryFacilitator) || (e.Action.Parent is UserEntity) || (e.Action.Parent is Set))
            {
                Entity ent = (Entity)e.Action.Parent;
                string subject = ent.Name;
                string actionName = e.Action.Name;

                if (actionName == LoadRemoteAgentAction.ACTION_NAME)
                {
                    object parameter;
                    string characterName = "";

                    if (e.Context.Arguments.TryGetValue(LoadRemoteAgentAction.NAME, out parameter))
                    {
                        characterName = parameter as string;
                    }
                    StoryEvent ev = new StoryEvent(subject, actionName, characterName, StoryEvent.EventType.ACTION_FINISHED);
                    this.TellStoryEvent(ev);
                }                
                else if (e.Action is Behaviour)
                {
                    if (e.Action.Name == "say-to")
                    {
                        string parameters;
                        SpeechAct speech = e.Context.Arguments[RemoteCharacter.REMOTE_ACTION_PARAMETERS] as SpeechAct;
                        parameters = speech.Target + " " + speech.Meaning;
                        StoryEvent ev =
                            new StoryEvent((e.Action.Parent as Entity).Name, e.Action.Name, parameters,
                                           StoryEvent.EventType.ACTION_FINISHED);
                        this.TellStoryEvent(ev);                       
                        UserLogger.Instance().WriteLine(speech.ContextVariables["me"] + " said to " + speech.Target + ": " + speech.Utterance);
                        String emotion = "";
                        try
                        {
                            EmotionalState emotionalState = (e.Action.Parent as RemoteCharacter).GetProperty<Property<EmotionalState>>("emotional-state").Value;
                            List<Emotion> strongestEmotions = emotionalState.GetTheThreeStrongestEmotions();
                            if (strongestEmotions.Count == 0)
                            {
                                emotion = "Neutral=0";
                            }
                            else
                            {
                                foreach (Emotion strongestEmotion in strongestEmotions)
                                {
                                    if (emotion != "")
                                        emotion += ";";
                                    emotion += strongestEmotion.Type + "=" + Math.Ceiling(strongestEmotion.Intensity);
                                }
                            }
                        }
                        catch (Exception) //Agent does not have emotional-state
                        {
                            emotion = "Neutral=0";
                        } 
                        ComicsLogger.Instance().WriteLine("EVENT:" + speech.ContextVariables["me"] + ":" + emotion + ":say:" + speech.Target + ":" + speech.Utterance);
                    }
                    else if (e.Action.Name == "saySimple")
                    {
                        RemoteAction remoteAction = e.Context.Arguments[RemoteCharacter.REMOTE_ACTION_PARAMETERS] as RemoteAction;
                        string speech = string.Empty;
                        foreach (string str in remoteAction.Parameters)
                        {
                            speech += str + " ";                            
                        }

                        if (speech != string.Empty)
                            speech = speech.Substring(0, speech.Length - 1);
                        
                        StoryEvent ev =
                            new StoryEvent((e.Action.Parent as Entity).Name, e.Action.Name, speech,
                                           StoryEvent.EventType.ACTION_FINISHED);
                        this.TellStoryEvent(ev);
                        UserLogger.Instance().WriteLine((e.Action.Parent as Entity).Name + " said " + speech);

                        String emotion = "";
                        try
                        {
                            EmotionalState emotionalState = (e.Action.Parent as RemoteCharacter).GetProperty<Property<EmotionalState>>("emotional-state").Value;
                            List<Emotion> strongestEmotions = emotionalState.GetTheThreeStrongestEmotions();
                            if (strongestEmotions.Count == 0)
                            {
                                emotion = "Neutral=0";
                            }else
                            {
                                foreach (Emotion strongestEmotion in strongestEmotions)
                                {
                                    if (emotion != "")
                                        emotion += ";";
                                    emotion += strongestEmotion.Type + "=" + Math.Ceiling(strongestEmotion.Intensity);
                                }
                            }
                        }
                        catch (Exception) //Agent does not have emotional-state
                        {
                            emotion = "Neutral=0";
                        }                    

                        ComicsLogger.Instance().WriteLine("EVENT:" + (e.Action.Parent as Entity).Name + ":" + emotion + ":say:User:" + speech);
                    }
                    else
                    {
                        RemoteAction remoteAction = (RemoteAction)e.Context.Arguments[RemoteCharacter.REMOTE_ACTION_PARAMETERS];

                        string parameters = string.Empty;

                        if ((remoteAction.Target != null) && (remoteAction.Target != string.Empty))
                        {
                            parameters = remoteAction.Target;
                        }

                        foreach (string parameter in remoteAction.Parameters)
                        {
                            if (parameters == string.Empty)
                                parameters = parameter + " ";
                            else
                            {
                                parameters += parameter + " ";
                            }
                        }

                        parameters = parameters.TrimEnd(null);

                        StoryEvent ev = new StoryEvent(remoteAction.Subject, remoteAction.ActionType, parameters,
                                           StoryEvent.EventType.ACTION_FINISHED);
                        this.TellStoryEvent(ev);
                        UserLogger.Instance().WriteLine(remoteAction.Subject + " performed the action " + remoteAction.ActionType + " " + parameters);

                        String emotion = "";
                        try
                        {
                            EmotionalState emotionalState = (e.Action.Parent as RemoteCharacter).GetProperty<Property<EmotionalState>>("emotional-state").Value;
                            List<Emotion> strongestEmotions = emotionalState.GetTheThreeStrongestEmotions();
                            if (strongestEmotions.Count == 0)
                            {
                                emotion = "Neutral=0";
                            }
                            else
                            {
                                foreach (Emotion strongestEmotion in strongestEmotions)
                                {
                                    if (emotion != "")
                                        emotion += ";";
                                    emotion += strongestEmotion.Type + "=" + Math.Ceiling(strongestEmotion.Intensity);
                                }
                            }
                        }
                        catch (Exception) //Agent does not have emotional-state
                        {
                            emotion = "Neutral=0";
                        }                  

                        ComicsLogger.Instance().WriteLine("EVENT:" + remoteAction.Subject + ":" + emotion + ":"  +remoteAction.ActionType + ":" + parameters);                        
                    }

                } 
                else if (e.Action is CreatePropertyListAction)
                {                    
                    this.TellStoryEvent(new StoryEvent(this.Name, CreatePropertyListAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED));
                }
                else if (e.Action is CreateCharacterEntitiesAction)
                {
                    this.TellStoryEvent(new StoryEvent(this.Name, CreateCharacterEntitiesAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED));
                }
                else if (e.Action is RemoveAllCharacterAndUserPropertiesAction)
                {
                    this.TellStoryEvent(new StoryEvent(this.Name, RemoveAllCharacterAndUserPropertiesAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED));                   
                }else if (e.Action is PutAllCharactersToSleepAction)
                {
                    this.TellStoryEvent(new StoryEvent(this.Name, PutAllCharactersToSleepAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED));   
                }
                else if (e.Action is CreateItemPropertyListAction)
                {
                    this.TellStoryEvent(new StoryEvent(this.Name, CreateItemPropertyListAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED));
                }
                else if (e.Action is RemoveAllItemsAndUserAction)
                {
                    this.TellStoryEvent(new StoryEvent(this.Name, RemoveAllItemsAndUserAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED));
                }                
                else if (e.Action is AwakeAction)
                {
                    this.TellStoryEvent(new StoryEvent(subject, actionName, "", StoryEvent.EventType.ACTION_FINISHED));
                }
                else if (e.Action is PutToSleepAction)
                {
                    this.TellStoryEvent(new StoryEvent(subject, actionName, "", StoryEvent.EventType.ACTION_FINISHED));
                }
                else if (e.Action is RemoveCharacterEntitiesAction)
                {
                    this.TellStoryEvent(new StoryEvent(subject, actionName, "", StoryEvent.EventType.ACTION_FINISHED));
                }
                else if (e.Action is UpdatePropertyListAction)
                {
                    this.TellStoryEvent(new StoryEvent(subject, actionName, "", StoryEvent.EventType.ACTION_FINISHED));
                }
                else if (e.Action is RemoveAllEntitiesAction)
                {
                    this.TellStoryEvent(new StoryEvent(subject, actionName, "", StoryEvent.EventType.ACTION_FINISHED));
                }else if (e.Action is DAQueryResponseAction)
                {
                    string actionRepresentaion = (string)e.Context.Arguments[DAQueryResponseAction.ACTION_REPRESENTATION];
                    string triggerID = (string)e.Context.Arguments[DAQueryResponseAction.TRIGGER_ID];
                    string appraisalValue = (string)e.Context.Arguments[DAQueryResponseAction.APPRAISAL_VALUE];

                    this.TellStoryEvent(new StoryEvent(subject, actionName, actionRepresentaion+":"+triggerID+":"+appraisalValue, StoryEvent.EventType.ACTION_FINISHED));
                }
                else if (e.Action.Parent is UserEntity)
                {
                    string parameters = "";
                    if (e.Context.Arguments.ContainsKey("target")) 
                        parameters = e.Context.Arguments["target"] + " ";
                    if (e.Context.Arguments.ContainsKey("parameters")) 
                        parameters = parameters + e.Context.Arguments["parameters"];
                    
                    this.TellStoryEvent(new StoryEvent(subject, actionName, parameters, StoryEvent.EventType.ACTION_FINISHED));                
                }
                else if (e.Action.Parent is Set)
                {
                    this.TellStoryEvent(new StoryEvent(e.Action.Parent.Name, e.Action.Name, e.Context.Arguments.Get<string>(ActForEnvironmentAction.ACTION_PARAMETERS), StoryEvent.EventType.ACTION_FINISHED));
                }
            }

            else if (e.Action.Parent is Narrator)
            {
                Entity ent = (Entity)e.Action.Parent;
                string subject = ent.Name;
                string actionName = e.Action.Name;

                if (actionName == "say")
                {
                    string utterance = e.Context.Arguments["utterance"] as string;
                    this.TellStoryEvent(new StoryEvent(subject, actionName, utterance, StoryEvent.EventType.ACTION_FINISHED));
                }
            }
        }


        #endregion

        #region IEventCallback<EntityCreated> Members

        public void Invoke(EntityCreated e)
        {
            if ((e.Entity is RemoteCharacter) || (e.Entity is Item) || (e.Entity is UserEntity) || (e.Entity is Set) || (e.Entity is Narrator) || (e.Entity is LanguageEngineMaster) || (e.Entity is ModellessItem) || (e.Entity is UserController))
            {
                //The story event generated indicates that the SF has created the entity            
                StoryEvent ev = new StoryEvent(this.Name, StoryEvent.ENTITY_ADDED, e.Name, StoryEvent.EventType.ACTION_FINISHED);
                this.TellStoryEvent(ev);
            }
        }

        public void Invoke(EntityDestroyed e)
        {
            if ((e.Entity is RemoteCharacter) || (e.Entity is Item) || (e.Entity is UserEntity) || (e.Entity is Set) || (e.Entity is Narrator) || (e.Entity is LanguageEngineMaster) || (e.Entity is ModellessItem) || (e.Entity is UserController))
            {
                //The story event generated indicates that the SF has destroied the entity            
                StoryEvent ev = new StoryEvent(this.Name, StoryEvent.ENTITY_REMOVED, e.OldName, StoryEvent.EventType.ACTION_FINISHED);
                this.TellStoryEvent(ev);
            }
        }

	    public void Invoke(PropertyChanged<Property<string>> e)
	    {
            if ((e.Property.Parent is RemoteCharacter) || (e.Property.Parent is UserEntity) || (e.Property.Parent is Item) || e.Property.Parent is ModellessItem)
	        {
                ParticipantProperty participantProperty =
                    this.StoryMemory.GetParticipantProperty((e.Property.Parent as Entity).Name, e.Property.Name);
                if (participantProperty != null) //Only the properties contained in SF's memory (hence the characters' properties matter)
                {
                    //Maintains the property as an episode-level or story-level property
                    ParticipantProperty storyParticipantProperty = new ParticipantProperty((e.Property.Parent as Entity).Name, e.Property.Name, e.Property.Value, participantProperty.StoryLevel);
                    this.TellStoryEvent(storyParticipantProperty);
                    
                    //Launch event that states that the property has changed
                    this.TellStoryEvent(new StoryEvent(e.Property.Parent.Name, StoryEvent.PROPERTY_CHANGED, e.Property.Name + " " + e.Property.Value, StoryEvent.EventType.ACTION_FINISHED));
                    ComicsLogger.Instance().WriteLine("PROPERTY:" + e.Property.Parent.Name + ":" + e.Property.Name + ":" + e.Property.Value);
                }
	        }
	    }	    
        #endregion

        public void Invoke(PropertyDestroyed e)
        {
           if ((e.OldParent is RemoteCharacter) || (e.OldParent is UserEntity) || (e.OldParent is Item) || e.OldParent is ModellessItem)
            {
                this.TellStoryEvent(new StoryEvent(this.Name, StoryEvent.PROPERTY_REMOVED, (e.OldParent as Entity).Name + " " + e.OldName, StoryEvent.EventType.ACTION_FINISHED));
            }
        }

        public void Invoke(PropertyCreated e)
        {
            if ((e.Parent is RemoteCharacter) || (e.Parent is UserEntity) || (e.Parent is Item) || e.Parent is ModellessItem)
            {
                this.TellStoryEvent(new StoryEvent(this.Name, StoryEvent.PROPERTY_CREATED, (e.Parent as Entity).Name + " " + e.Property.Name, StoryEvent.EventType.ACTION_FINISHED));
            }            
        }
	    
	    

		#endregion


		#region StateMachine

		public void ChangeState(State to)
		{
			state = to;
		}

		//Used when an event happens
	    //Actions + Narrative Actions + Framework events (Entities/properties created/destroyed/changed)
	    //See StoryEvent
		public void TellStoryEvent(StoryEvent ev)
		{
			//When the story starts an event is generated TellStoryEvent.STORY_START
			//and there is no episode yet
			if(activeEpisode == null) 							
				storyMemory.Tell(ev, "No Episode (STORY_START)", Episode.EPISODE_ID_NOT_SET);
			else
				storyMemory.Tell(ev, this.ActiveEpisode.Name, this.ActiveEpisode.EpisodeID);
			
            state.StoryEvent(this, ev);


            //---Pshycologists logs----------------
            if (activeEpisode != null)
            {
                if (ev.Action == StoryEvent.SELECT_EPISODE)
                {
                    if ((ev.Parameters.IndexOf("interaction") == -1) && (ev.Parameters.IndexOf("INTRO") == -1) && (ev.Parameters.IndexOf("OUTRO") == -1)) //No INTROs/OUTROs and no interaction
                    {
                        try
                        {
                            ExcelInteractionLog.Instance().saveLog(this.UserSessionInfo.UserCode);
                        }catch(Exception e)
                        {
                            ApplicationLogger.Instance().WriteLine("NON CRITICAL ERROR: ExcelInteractionLog -> " + e.Message);
                        }
                        ExcelInteractionLog.Instance().changeEpisode(ev.Parameters);
                        string copingStrategy = "";
                        if (this.StoryMemory.GetParticipantProperty("John", "copingStrategy") != null)
                            copingStrategy = this.StoryMemory.GetParticipantProperty("John", "copingStrategy").Value;
                        ExcelInteractionLog.Instance().changeCopingStrategy(copingStrategy);
                    }
                }
                if (activeEpisode.EpisodeType == Episode.EpisodeTypeEnum.INTERACTION)
                {
                    if (ev.Type == StoryEvent.EventType.ACTION_FINISHED)
                    {
                        if (ev.Subject == "user")
                        {
                            if (ev.Action == "UserSpeechAction")
                                ExcelInteractionLog.Instance().userSays(ev.Parameters);
                        }
                        else if (ev.Action == "say-to")
                        {
                            ExcelInteractionLog.Instance().agentSays(ev.Parameters);
                        }
                    }
                }
            }
            //------------------------------
		}

		//Used when propery added/changed
		public void TellStoryEvent(ParticipantProperty p)
		{
			storyMemory.Tell(p);
			state.StoryEvent(this, p);

            //---Phsycologists logs
            if (p.Name.ToUpper() == "copingStrategy".ToUpper())
            {
                ExcelInteractionLog.Instance().changeCopingStrategy(p.Value);
            }
            //---------------------
		}

		//O UpdateEvent e' chamado dentro do metodo de update
		
		#endregion

		public void Start()
		{			    
		    this.TellStoryEvent(new StoryEvent(this.Name,StoryEvent.STORY_START, UserSessionInfo.UserCode, StoryEvent.EventType.ACTION_FINISHED));			
		}
	    


    }
}
