// StoryMemoryDecorator- Implementation of the Decorator desgin pattern for the StoryMemory class
//					     Logs the information stored in the StoryMemory
//			    
// 
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.IO;
using Narrative.Conditions;
using Narrative.Participants;

namespace Narrative
{
	/// <summary>
	/// Implementation of the Decorator desgin pattern for the StoryMemory class
	/// Logs the information stored in the StoryMemory
	/// The tell and ask of new information
	/// </summary>
	[Serializable()]
	public class StoryMemoryDecorator : StoryMemory
	{
		public StoryMemory storyMemory = new StoryMemory();
        public string LOG_DIR = "logs\\";
        private string userCode = "default";
		private const string LOG_FILE = "_story_log.txt";
        [NonSerialized()]
		StreamWriter sWriter;


		public StoryMemoryDecorator()
		{			
			//sWriter = File.CreateText(LOG_FILE); // If file exists it is overriden
		}

		~StoryMemoryDecorator()
		{
            try
            {
                sWriter.Close();
            }catch(Exception)
            {
                //ignore
            }
		}
	    
	    public void Close()
	    {
            try
            {
                userCode = "default";
                sWriter.Close();
            }
            catch (Exception)
            {
                //ignore
            }	        
	    }

		public override void Tell(StoryEvent e, string episodeName, int episodeID)
		{
		    string dateTimeStr;
            if ((sWriter == null )||(sWriter.BaseStream == null) || (!sWriter.BaseStream.CanWrite))
            {
                if (!Directory.Exists(LOG_DIR + userCode))
                {
                    Directory.CreateDirectory(LOG_DIR + userCode);
                }
                dateTimeStr = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + "_" +
                     DateTime.Now.Minute + "_" + DateTime.Now.Second;

                sWriter = File.CreateText(LOG_DIR + userCode + "\\" + dateTimeStr + LOG_FILE);
            }

            dateTimeStr = DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + " " + DateTime.Now.Hour + ":" +
                     DateTime.Now.Minute + ":" + DateTime.Now.Second;

			sWriter.WriteLine(dateTimeStr + "->Tell: " + e.ToString());
			storyMemory.Tell(e, episodeName, episodeID);
			sWriter.Flush();
		}
		
		public override void Tell(ParticipantProperty p)
		{
		    string dateTimeStr;
            if ((sWriter == null )||(sWriter.BaseStream == null) || (!sWriter.BaseStream.CanWrite))
            {
                if (!Directory.Exists(LOG_DIR + userCode))
                {
                    Directory.CreateDirectory(LOG_DIR + userCode);
                }

                dateTimeStr = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + "_" +
                    DateTime.Now.Minute + "_" + DateTime.Now.Second;

                sWriter = File.CreateText(LOG_DIR + userCode + "\\" + dateTimeStr + LOG_FILE);
            }

            dateTimeStr = DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + " " + DateTime.Now.Hour + ":" +
                 DateTime.Now.Minute + ":" + DateTime.Now.Second;

			sWriter.WriteLine(dateTimeStr + "->Tell: " + p.ToString());
			storyMemory.Tell(p);
			sWriter.Flush();
		}

		public override bool Ask(EventCondition ec)
		{			
			//sWriter.WriteLine("Ask: " + ec.ToString());
			bool res = storyMemory.Ask(ec);
			//sWriter.WriteLine("Ask Res: " + res);
			//sWriter.Flush();
			return res;
		}

		public override bool Ask(PropertyCondition pc)
		{
			//sWriter.WriteLine("Ask: " + pc.ToString());
			bool res = storyMemory.Ask(pc);
			//sWriter.WriteLine("Ask Res: " + res);
			//sWriter.Flush();
			return res;
		}

		public override System.Collections.ArrayList AskParticipantProperties(string characterName)
		{
			return storyMemory.AskParticipantProperties(characterName);
		}


		public override void TellTriggerFired(EventCondition ec)
		{
            if ((sWriter == null )||(sWriter.BaseStream == null) || (!sWriter.BaseStream.CanWrite))
            {
                if (!Directory.Exists(LOG_DIR + userCode))
                {
                    Directory.CreateDirectory(LOG_DIR + userCode);
                }
                string dateTimeStr = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + "_" +
                     DateTime.Now.Minute + "_" + DateTime.Now.Second;

                sWriter = File.CreateText(LOG_DIR + userCode + "\\" + dateTimeStr + LOG_FILE);
            }
			sWriter.WriteLine("TellFiredTriggerEvent: " + ec.ToString());
			storyMemory.TellTriggerFired(ec);
			sWriter.Flush();
		}


		public override bool Ask(EventCondition ec, int order)
		{			
			return storyMemory.Ask(ec, order);			
		}

		public override int AskOrder(EventCondition ec)
		{
			return storyMemory.AskOrder(ec);
		}


		public override StoryEvent AskLastEvent()
		{
			//sWriter.WriteLine("AskLastEvent: ");
			StoryEvent ev = storyMemory.AskLastEvent();
			/*if (ev == null)
			{
				sWriter.WriteLine("AskLastEventRes: No Events in storyMemory");
			}
			else
			{
				sWriter.WriteLine("AskLastEventRes: " + ev.ToString());
			}*/
			//sWriter.Flush();
			return ev;
		}

        public override void Retract(ParticipantProperty property)
        {
            if ((sWriter == null) || (sWriter.BaseStream == null) || (!sWriter.BaseStream.CanWrite))
            {
                if (!Directory.Exists(LOG_DIR + userCode))
                {
                    Directory.CreateDirectory(LOG_DIR + userCode);
                }
                string dateTimeStr = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + " " + DateTime.Now.Hour + "_" +
                     DateTime.Now.Minute + "_" + DateTime.Now.Second;

                sWriter = File.CreateText(LOG_DIR + userCode + "\\" + dateTimeStr + LOG_FILE);
            }

            sWriter.WriteLine("RetractProperty: " + property);
            storyMemory.Retract(property);
            sWriter.Flush();
        }
	    
	    public override void RetractEpisodeLevelProperties(string participantName)
	    {
            storyMemory.RetractEpisodeLevelProperties(participantName);
	    }

        public override ParticipantProperty GetParticipantProperty(string holder, string propertyName)
        {
            return storyMemory.GetParticipantProperty(holder, propertyName);
        }
	    
	    public void SetUserCode(String userCode)
	    {
	        Close(); //A new stream will be created for the new user
            this.userCode = userCode;
	    }
	}
}
