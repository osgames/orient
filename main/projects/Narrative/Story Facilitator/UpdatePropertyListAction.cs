using System;
using System.Collections;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Events;
using Narrative.Participants;

namespace Narrative
{
    /// <summary>
    /// Action that receives an ArrayList as an argument,PROPERTIES_LIST, which contains ParticipantProperties
    /// For each of those ParticipantProperties this action whill update the property setting its value in the correnponding
    /// RemoteCharacter
    /// </summary>
    public class UpdatePropertyListAction : Action, IEventCallback<ION.Core.Events.PropertyChanged>
    {
        #region Constants        
        
        public const string ACTION_NAME = "UpdatePropertyListAction";
        
        //Parameters
        public const string CHARACTER_PROPERTIES_LIST = "PROPERTIES_LIST";
                
        
        #endregion

        public ArrayList storyProperties;        
        
        
        public UpdatePropertyListAction()
        {
            storyProperties = new ArrayList();            
        }
        protected override void OnStart(Context context)
        {
            Arguments arguments = context.Arguments;
            storyProperties = (arguments[CHARACTER_PROPERTIES_LIST] as ArrayList).Clone() as ArrayList;            
           
            //Assign the properties to that entity
            foreach (ParticipantProperty participantProperty in storyProperties)
            {
                RemoteCharacter remoteCharacter =
                    Universe.Instance.GetEntity<RemoteCharacter>(participantProperty.Holder);                
                remoteCharacter.GetProperty<Property<string>>(participantProperty.Name).Value = participantProperty.Value;
            }
            
        }

        protected override void OnEnd(Context context)
        {
            storyProperties = new ArrayList();            
        }

        protected override void OnStep(Context context)
        {
            if (HasEnded())
            {
                this.End();
            }
        }

        protected bool HasEnded()
        {
            if (storyProperties.Count == 0) //The action will finish upon confirmation of the creation of all properties
                return true;
            else
                return false;
        }

        protected override void OnCreate(Arguments arguments)
        {
            //Register the event listner for property changed
            Universe.Instance.CreateEventListener<PropertyChanged>(this);
        }



        #region Callbacks


        public void Invoke(PropertyChanged e)
        {
            if (e.Property.Parent is RemoteCharacter)
            {
                foreach (ParticipantProperty participantProperty in (ArrayList)storyProperties.Clone())
                {
                    if ((e.Property.Name == participantProperty.Name) && ((e.Property.Parent as RemoteCharacter).Name == participantProperty.Holder))
                    {
                        storyProperties.Remove(participantProperty);//The property was changed, remove it from the list of properties awaiting confirmation                        
                    }
                }
            }
        }
        
        #endregion

        protected override void OnFail(Context context)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
