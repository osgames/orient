// TellAvailableCS - Sends a message to an agent indicating which coping strategies have
//                   produce selectable episodes.
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 29/03/2007
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//


using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using Narrative.Conditions;
using Narrative.Participants;

namespace Narrative.Actions
{
    [Serializable()]
    public class TellAvailableCS :Action
    {
        #region ActionInfo
		[Serializable()]
		public class TellAvailableCSInfo
		{
		    private string characterName;
		    
		    public TellAvailableCSInfo(string characterName)
		    {
                CharacterName = characterName;
		    }
		    
		    public TellAvailableCSInfo(TellAvailableCSInfo actionInfo)
		    {
                this.CharacterName = actionInfo.CharacterName;
		    }

		    public string CharacterName
		    {
		        get { return characterName; }
		        set { characterName = value; }
		    }
		}            
        #endregion
        
        bool isFinished = false;
        TellAvailableCSInfo tellAvailableCSInfo;
        private static string[] COPING_STRATEGIES = { "ignore", "standup", "makenewfriend", "tellfriend", "runaway", "walkaway", "laughoff", "fightback", "insult", "tellparents", "tellteacher" };
        private const string COPYING_STRATEGY_PROPERTY_NAME = "copingStrategy";
        
        public TellAvailableCS(TellAvailableCSInfo actionInfo)
        {
            tellAvailableCSInfo = actionInfo;
        }
        
        public TellAvailableCS(TellAvailableCS action)
        {
            this.tellAvailableCSInfo = new TellAvailableCSInfo(action.tellAvailableCSInfo);
        }
        
        

        public override void Start(StoryFacilitator storyFacilitator)
        {
            StoryMemory storyMemory;
            List<string> availableCopingStrategies = new List<string>();
            
            if (storyFacilitator.StoryMemory is StoryMemoryDecorator)
            {
                storyMemory = ((StoryMemoryDecorator) storyFacilitator.StoryMemory).storyMemory.Clone();
            }else
            {
                storyMemory = storyFacilitator.StoryMemory.Clone();
            }

            ParticipantProperty copyingStrategy =
                storyMemory.GetParticipantProperty(this.tellAvailableCSInfo.CharacterName,
                                                   COPYING_STRATEGY_PROPERTY_NAME);
            
            if (copyingStrategy == null)
            {
                isFinished = false;               
            }else
            {
                //create a string with the coping strategies for which the episodes are selectable
                //for example: availableCopingStrategies="tellsomeone tellafriend hitback"
                foreach (string strategy in COPING_STRATEGIES)
                {
                    storyMemory.Tell(
                        new ParticipantProperty(copyingStrategy.Holder, 
                                                copyingStrategy.Name, strategy, 
                                                copyingStrategy.StoryLevel)); //this replaces the old value
                    foreach (Episode episode in storyFacilitator.notSelectedEpisodes)
                    {
                        if ((episode.EpisodeType != Episode.EpisodeTypeEnum.INTERACTION) && 
                            ((episode.EpisodeType != Episode.EpisodeTypeEnum.INTRO)) && 
                            ((episode.EpisodeType != Episode.EpisodeTypeEnum.OUTRO)) && 
                            (episode.IsSelectable(storyMemory)))
                        {
                            availableCopingStrategies.Add(strategy);
                            break;
                        }
                    }
                }
                
                if (availableCopingStrategies.Count == COPING_STRATEGIES.Length) //No need to update any properties, all coping strategies are available
                {
                    isFinished = true;
                    return;
                }
                
                List<string> unAvailableCopingStrategies = new List<string>();
                
                foreach (string strategy in COPING_STRATEGIES)
                {
                    if (!availableCopingStrategies.Contains(strategy))
                    {
                        unAvailableCopingStrategies.Add(strategy);
                    }
                }
                
                //The number of tries property of the agent when equal to 6 
                //means that the agent should not try that coping strategy again...
                ArrayList propertiesToUpdate = new ArrayList();
                foreach (string strategy in unAvailableCopingStrategies)
                {
                    propertiesToUpdate.Add(
                        new ParticipantProperty(this.tellAvailableCSInfo.CharacterName, "numberOfTries," + strategy, "6",
                                                true));                    
                }

                Dictionary<string, object> actionArguments = new Dictionary<string, object>();
                actionArguments.Add(UpdatePropertyListAction.CHARACTER_PROPERTIES_LIST, propertiesToUpdate);
                storyFacilitator.GetAction<UpdatePropertyListAction>(UpdatePropertyListAction.ACTION_NAME).Start(new Arguments(actionArguments));                
            }
        }

        public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
        {
        }

        public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
        {
            //if accao que actualiza propriedades acabou
            //isFinished=true;            
            if (ev.Equals(new StoryEvent(storyFacilitator.Name, UpdatePropertyListAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED)))
            {
                isFinished = true;
            }
        }

        public override bool IsFinished()
        {
            return isFinished;
        }

        public override Action Clone()
        {
            return new TellAvailableCS(this);
        }

        public override void ApplySubstitution(Substitution substitution)
        {
            throw new System.NotImplementedException();
        }
    }
}
