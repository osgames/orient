// RemoveCharacterAction - Holds the information necessary to execute an action of the
//							   type: RemoveCharacter (Currently not being used)
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Extensions;
using Narrative.Conditions;
using Narrative.Participants;
using System.Collections.Generic;

namespace Narrative.Actions
{
	/// <summary>
	/// Holds the information necessary to execute an action of the
	/// type: RemoveCharacter
	/// </summary>
	[Serializable()]
	public class RemoveCharacterAction : Action
	{	    

        #region ActionInfo
        /// <summary>
        /// Holds the information necessary to execute an action of the
        /// type: RemoveCharacter
        /// </summary>
        [Serializable()]
        public class RemoveCharacterActionInfo
        {
            private string characterName;
            
            public RemoveCharacterActionInfo(){}
            
            public RemoveCharacterActionInfo(RemoveCharacterActionInfo actionInfo)
            {
                this.characterName = actionInfo.characterName;
            }

            #region Properties
            public string CharacterName
            {
                get
                {
                    return characterName;
                }
                set
                {
                    characterName = value;
                }
            }
            #endregion

            public override string ToString()
            {
                return "REMOVE_CHARACTER_AINFO:" + CharacterName + "\n";
            }

        }
        #endregion

        RemoveCharacterActionInfo removeCharacterActionInfo;
	    bool finished = false;

		public RemoveCharacterAction(RemoveCharacterActionInfo removeCharacterActionInfo)
		{
            this.removeCharacterActionInfo = removeCharacterActionInfo;
		}
	    
	    public RemoveCharacterAction(RemoveCharacterAction action)
	    {
	        this.removeCharacterActionInfo = new RemoveCharacterActionInfo(action.removeCharacterActionInfo);
	    }

	    public override void Start(StoryFacilitator storyFacilitator)
	    {
            ApplicationLogger.Instance().WriteLine("Remove Character Narrative Action");
            ApplicationLogger.Instance().Write(this.removeCharacterActionInfo.ToString());            	        
	        
            CharacterParticipant characterParticipant = storyFacilitator.ActiveEpisode.GetCharacter(removeCharacterActionInfo.CharacterName);

            RemoteCharacter rmChar = Universe.Instance.GetEntity<RemoteCharacter>(characterParticipant.Name);
	        

            PutToSleepAction putToSleepAction = rmChar.GetAction<PutToSleepAction>(PutToSleepAction.ACTION_NAME);
	        putToSleepAction.Start();	        	        
	    }

	    public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
	    {
	        //Do nothing
	    }

	    public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
	    {
	        if (ev.Equals(new StoryEvent(removeCharacterActionInfo.CharacterName, PutToSleepAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED)))
	        {
                RemoteCharacter rmChar = Universe.Instance.GetEntity<RemoteCharacter>(removeCharacterActionInfo.CharacterName);
                Dictionary<string, object> args = new Dictionary<string, object>();                
                rmChar.GetAction("leave-stage").Start(new Arguments(args));

	            finished = true;
                storyFacilitator.StoryMemory.Tell(new StoryEvent(storyFacilitator.Name, StoryEvent.REMOVE_CHARACTER_ACTION, removeCharacterActionInfo.CharacterName, StoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeID);
	        }
	    }

	    public override bool IsFinished()
	    {
            return finished;
	    }

        public override Action Clone()
        {
            return new RemoveCharacterAction(this);
        }

	    public override void ApplySubstitution(Substitution substitution)
	    {
	        this.removeCharacterActionInfo.CharacterName = substitution.Apply(removeCharacterActionInfo.CharacterName);
	    }

	    public override string GetActionRepresentation()
        {
            //REMOVE_CHARACTER CHARACTER_NAME
            return "REMOVE_CHARACTER " + removeCharacterActionInfo.CharacterName;
        }
	}
}
