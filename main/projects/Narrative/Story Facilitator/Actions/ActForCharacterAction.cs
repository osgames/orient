// ActForCharacterAction - Implementation of the ActForCharacter Narrative Action.
//						   This action makes a character already present in the world
//						   to perform a certain action described in ActForCharacerActionInfo
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//


using System;
using FAtiMA.RemoteAgent;
using FAtiMA.Actions;
using ION.Core;
using ION.Core.Extensions;
using Narrative.Conditions;

namespace Narrative.Actions
{
    /// <summary>
    /// Implementation of the ActForCharacter Narrative Action.
    /// This action makes a character already present in the world
    /// to perform a certain action described in ActForCharacerActionInfo
    /// </summary>
    [Serializable()]
    public class ActForCharacterAction : Action
    {
        public const string ACT_FOR_CHARACTER_SAY_SIMPLE_ACTION_NAME = "saySimple";
        #region ActionInfo
        /// <summary>
        ///  Holds the information necessary to execute an action of the
        /// type: ActForCharacter
        /// </summary>
        [Serializable()]
        public class ActForCharacterActionInfo
        {

            private string characterName;
            private string actionName;
            private string parameters;

            public ActForCharacterActionInfo() { }

            public ActForCharacterActionInfo(ActForCharacterActionInfo afcAI)
            {
                this.actionName = afcAI.actionName;
                this.characterName = afcAI.characterName;
                this.parameters = afcAI.parameters;
            }

            #region Properties
            public string CharacterName
            {
                get
                {
                    return characterName;
                }
                set
                {
                    characterName = value;
                }
            }

            public string Parameters
            {
                get { return parameters; }
                set { parameters = value; }
            }

            public string ActionName
            {
                get { return actionName; }
                set { actionName = value; }
            }

            #endregion


            public override string ToString()
            {
                return "ACT_FOR_CHARACTER_AINFO:" + CharacterName + ":" + ActionName + ":" + Parameters + "\n";
            }

        }
        #endregion

        bool isFinished = false;
        public ActForCharacterActionInfo actForCharacterActionInfo;

        public ActForCharacterAction(ActForCharacterActionInfo actionInfo)
        {
            actForCharacterActionInfo = actionInfo;
        }

        public ActForCharacterAction(ActForCharacterAction aFCA)
        {
            this.actForCharacterActionInfo = new ActForCharacterActionInfo(aFCA.actForCharacterActionInfo);
        }

        public override void Start(StoryFacilitator storyFacilitator)
        {

            try
            {
                RemoteCharacter rmChar =
                    Universe.Instance.GetEntity<RemoteCharacter>(actForCharacterActionInfo.CharacterName);
                RemoteAction rmAction = new RemoteAction();
                rmAction.Subject = actForCharacterActionInfo.CharacterName;
                rmAction.ActionType = actForCharacterActionInfo.ActionName;
                foreach (string parameter in actForCharacterActionInfo.Parameters.Split(' '))
                {
                    rmAction.Parameters.Add(parameter);
                }

                rmChar.RunRemoteAction(rmAction);

            }
            catch (Exception e)
            {
                throw new ActionException("ActForCharacterAction: Action " + actForCharacterActionInfo.ActionName + " failed for Character " + actForCharacterActionInfo.CharacterName + " in current episode: " + storyFacilitator.ActiveEpisode.Name + " -> " + e.Message);
            }

            ApplicationLogger.Instance().WriteLine("ActForCharacter Narrative Action");
            ApplicationLogger.Instance().Write(actForCharacterActionInfo.ToString());

            //SIMULATE the event necessary to end this action            
            //storyFacilitator.TellStoryEvent(new StoryEvent(actForCharacterActionInfo.CharacterName, actForCharacterActionInfo.ActionName, actForCharacterActionInfo.Parameters, StoryEvent.EventType.ACTION_FINISHED));
        }

        public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
        {
        }

        public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
        {
            if (ev.Equals(new StoryEvent(actForCharacterActionInfo.CharacterName, actForCharacterActionInfo.ActionName, actForCharacterActionInfo.Parameters, StoryEvent.EventType.ACTION_FINISHED)))
            {
                storyFacilitator.StoryMemory.Tell(new StoryEvent(storyFacilitator.Name, StoryEvent.ACT_FOR_CHARACTER_ACTION, actForCharacterActionInfo.CharacterName + " " + actForCharacterActionInfo.ActionName + " " + actForCharacterActionInfo.Parameters, StoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeID);
                isFinished = true;
            }
        }

        public override bool IsFinished()
        {
            return isFinished;
        }

        public override string ToString()
        {
            return actForCharacterActionInfo.ToString();
        }

        public override Action Clone()
        {
            return new ActForCharacterAction(this);
        }

        public override void ApplySubstitution(Substitution substitution)
        {
            actForCharacterActionInfo.CharacterName = substitution.Apply(actForCharacterActionInfo.CharacterName);
            actForCharacterActionInfo.Parameters = substitution.Apply(actForCharacterActionInfo.Parameters);
        }

        public override string GetActionRepresentation()
        {
            //Format: ACT_FOR_CHARACTER CHARACTER_NAME ACTION_NAME ACTION_PARAMETERS
            return "ACT_FOR_CHARACTER " + actForCharacterActionInfo.CharacterName + " " + actForCharacterActionInfo.ActionName + " " + actForCharacterActionInfo.Parameters;
        }

    }
}
