// InsertObjectAction - 	Implementation for the Insert Object Action
//							This action adds an object to the world.
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//
// Note: InitialPosition is read in the episode definition XML but it is not being used here yet.

using System;
using System.Collections;
using System.Collections.Generic;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Extensions;
using ION.Realizer.Orient.Entities;
using Narrative.Conditions;
using Narrative.Participants;

namespace Narrative.Actions
{
	/// <summary>
	/// Summary description for InsertObjectAction.
	/// </summary>
	[Serializable()]
	public class InsertObjectAction : Action
	{
		#region ActionInfo
		/// <summary>
		/// Holds the information necessary to execute an action of the
		/// type: InsertObject
		/// </summary>
		[Serializable()]
		public class InsertObjectActionInfo 
		{
			private string objectName;
            private string positionName;
		    private string container;

			#region Properties
			public string ObjectName
			{
				get
				{
					return objectName;
				}
				set
				{
					objectName = value;
				}
			}

            public string PositionName
            {
                get
                {
                    return positionName;
                }
                set
                {
                    positionName = value;
                }
            }

		    public string Container
		    {
		        get { return container; }
		        set { container = value; }
		    }

		    #endregion

			public InsertObjectActionInfo()
			{				
			}
		    
		    public InsertObjectActionInfo(InsertObjectActionInfo actionInfo)
		    {
                this.objectName = actionInfo.objectName;
		        this.positionName = actionInfo.positionName;
		        this.container = actionInfo.container;
		    }

			public override string ToString()
			{
				return "INSERT_OBJECT_AINFO:"+ObjectName+":" + positionName + ":" + container + "\n";
			}

		}
		#endregion
	    
		bool isFinished;
		InsertObjectActionInfo insertObjectActionInfo;

		public InsertObjectAction(InsertObjectActionInfo insertObjectActionInfo)
		{
			isFinished = false;
			this.insertObjectActionInfo = insertObjectActionInfo;
		}
	    
	    public InsertObjectAction(InsertObjectAction action)
	    {
	        isFinished = false;
	        this.insertObjectActionInfo = new InsertObjectActionInfo(action.insertObjectActionInfo);
	    }

		public override void Start(StoryFacilitator storyFacilitator)
		{
            ApplicationLogger.Instance().WriteLine("Insert Object Narrative Action");
            ApplicationLogger.Instance().Write(this.insertObjectActionInfo.ToString());            
                        
			foreach(Participant participant in storyFacilitator.ActiveEpisode.Participants)
			{

				if (participant is ObjectParticipant)
				{
					ObjectParticipant objectParticipant = participant as ObjectParticipant;
                    ScenarioObject scenarioObject = storyFacilitator.Scenario.GetObject(insertObjectActionInfo.ObjectName);
					if (objectParticipant.Name.Equals(insertObjectActionInfo.ObjectName))
					{

                        foreach (ParticipantProperty property in scenarioObject.Properties)
                        {
                            if (storyFacilitator.StoryMemory.HasProperty(scenarioObject.Name, property.Name))
                                storyFacilitator.StoryMemory.Tell(property); //If the scenario level property doesn't already exist add it
                        }

						foreach(ParticipantProperty property in objectParticipant.GetProperties())
						{							
							storyFacilitator.StoryMemory.Tell(property); //Adds the episode-level object's properties in StoryMemory
						}

                        if (scenarioObject.Model != "")
                        {

                            Dictionary<string, object> actionArguments = new Dictionary<string, object>();

                            actionArguments.Add("type", scenarioObject.Model);

                            Universe.Instance.CreateEntity<Item>(insertObjectActionInfo.ObjectName,
                                                                 new Arguments(actionArguments));
                        }else
                        {
                            Universe.Instance.CreateEntity<ModellessItem>(scenarioObject.Name);
                        }
					    return;
					}
				}
			}
			throw new ActionException("InsertObject: Object " + insertObjectActionInfo.ObjectName + " not found in episode " + storyFacilitator.ActiveEpisode.Name);
		}

		public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
		{            
		}

		public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
		{
            if (ev.Equals(new Narrative.StoryEvent(storyFacilitator.Name, Narrative.StoryEvent.ENTITY_ADDED, insertObjectActionInfo.ObjectName, Narrative.StoryEvent.EventType.ACTION_FINISHED)))
            {
                ArrayList properties = storyFacilitator.StoryMemory.AskParticipantProperties(insertObjectActionInfo.ObjectName);
                CreateItemPropertyListAction createItemPropertyListAction =
                    storyFacilitator.GetAction<CreateItemPropertyListAction>(CreateItemPropertyListAction.ACTION_NAME);
                Dictionary<string, object> actionArguments = new Dictionary<string, object>();
                actionArguments.Add(CreateItemPropertyListAction.ITEM_PROPERTIES_LIST, properties);
                createItemPropertyListAction.Start(new Arguments(actionArguments));

            }else if (ev.Equals(new Narrative.StoryEvent(storyFacilitator.Name, CreateItemPropertyListAction.ACTION_NAME, "", Narrative.StoryEvent.EventType.ACTION_FINISHED)))
            {
                ScenarioObject scenarioObject = storyFacilitator.Scenario.GetObject(insertObjectActionInfo.ObjectName);
                if (scenarioObject.Model != "")
                {
                    Item item = Universe.Instance.GetEntity<Item>(insertObjectActionInfo.ObjectName);

                    Dictionary<string, object> actionArguments = new Dictionary<string, object>();
                    actionArguments.Add("target", insertObjectActionInfo.Container);
                    actionArguments.Add("spot", insertObjectActionInfo.PositionName);
                    item.GetAction("place-item").Start(new Arguments(actionArguments));
                }

                isFinished = true;                
                storyFacilitator.StoryMemory.Tell(new StoryEvent(storyFacilitator.Name, StoryEvent.INSERT_OBJECT_ACTION, insertObjectActionInfo.ObjectName, StoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeID);
            }
		}


		public override bool IsFinished()
		{
			return isFinished;
		}

		public override string ToString()
		{
			return insertObjectActionInfo.ToString();
		}

        public override Action Clone()
        {
            return new InsertObjectAction(this);
        }

	    public override void ApplySubstitution(Substitution substitution)
	    {
	        throw new System.NotImplementedException();
	    }

	    public override string GetActionRepresentation()
        {
            //Format: INSERT_OBJECT CHARACTER_NAME
            return "INSERT_OBJECT " + insertObjectActionInfo.ObjectName;
        }

	}
}
