// NextEpisodeAction - 	Implementation for the NextEpisode action
//						When the actionInfo associated with this action doesn't specify
//						to what episode should we skip to, the StoryFacilitator State is changed
//						to the SelectNextEpisodeState that will select a next episode
//						accordingly to the rules specified in the StoryFacilitator and in that 
//						state to the selection of episodes
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Extensions;
using ION.Realizer.Orient;
using ION.Realizer.Orient.Entities;
using Narrative.Conditions;
using Narrative.States;

namespace Narrative.Actions
{
	/// <summary>
	/// Implementation for the NextEpisode action
	/// When the actionInfo associated with this action doesn't specify
	/// to what episode should we skip to, the StoryFacilitator State is changed
	/// to the SelectNextEpisodeState that will select a next episode
	/// accordingly to the rules specified in the StoryFacilitator and in that 
	/// state to the selection of episodes
	/// </summary>
	[Serializable()]
	public class NextEpisodeAction : Action
	{
		#region ActionInfo
		/// <summary>
		/// Holds the information necessary to execute an action of the
		/// type: NextEpisode
		/// </summary>
        [Serializable()]
		public class NextEpisodeActionInfo
		{
		
			private string episodeName;
		    
		    public NextEpisodeActionInfo(){}
		    
		    public NextEpisodeActionInfo(NextEpisodeActionInfo actionInfo)
		    {
                this.episodeName = actionInfo.episodeName;
		    }
		    

			#region Properties
			
			/// <summary>
			/// Can be empty meaning that the nextEpisode should be chosen by the StoryFacilitator
			/// </summary>
			public string EpisodeName
			{
				get { return episodeName; }
				set { episodeName = value; }
			}

			#endregion


			public override string ToString()
			{
				return "NEXT_EPISODE_AINFO:"+EpisodeName+"\n";
			}

		}
		#endregion

		NextEpisodeActionInfo nextEpisodeActionInfo;
		bool isFinished = false;

		public NextEpisodeAction(NextEpisodeActionInfo actionInfo) 
		{
			nextEpisodeActionInfo = actionInfo;
		}
	    
	    public NextEpisodeAction(NextEpisodeAction action)
	    {
	        this.nextEpisodeActionInfo = new NextEpisodeActionInfo(action.nextEpisodeActionInfo);
	    }

		public override void Start(StoryFacilitator storyFacilitator)
		{            
            ApplicationLogger.Instance().WriteLine("NextEpisode Narrative Action");
            ApplicationLogger.Instance().Write(nextEpisodeActionInfo.ToString());
            ApplicationLogger.Instance().WriteLine("Stoping minds...");
            StopMinds();
            ApplicationLogger.Instance().WriteLine("Performing PutAllCharactersToSleepAction");            
            storyFacilitator.GetAction<PutAllCharactersToSleepAction>(PutAllCharactersToSleepAction.ACTION_NAME).Start();
           
		}

		public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
		{            
		}

		public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
		{
            if (ev.Equals(new Narrative.StoryEvent(storyFacilitator.Name, PutAllCharactersToSleepAction.ACTION_NAME, "", Narrative.StoryEvent.EventType.ACTION_FINISHED)))
            {
                if (nextEpisodeActionInfo.EpisodeName.Equals(""))
                {
                    //Let the decison of what is the next episode be decided in the
                    //SelectNextEpisode State
                    storyFacilitator.ChangeState(SelectNextEpisodeState.Instance());
                    (SelectNextEpisodeState.Instance() as SelectNextEpisodeState).NextEpisode(storyFacilitator);
                    storyFacilitator.StoryMemory.Tell(new StoryEvent(storyFacilitator.Name, StoryEvent.NEXT_EPISODE_ACTION, nextEpisodeActionInfo.EpisodeName, StoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeID);
                    isFinished = true;
                }
                else //Change directly to the episode in the NextEpisodeActionInfo
                {
                    storyFacilitator.ChangeState(SelectNextEpisodeState.Instance());
                    (SelectNextEpisodeState.Instance() as SelectNextEpisodeState).NextEpisode(storyFacilitator, nextEpisodeActionInfo.EpisodeName);
                    storyFacilitator.StoryMemory.Tell(new StoryEvent(storyFacilitator.Name, StoryEvent.NEXT_EPISODE_ACTION, nextEpisodeActionInfo.EpisodeName, StoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeID);
                    isFinished = true;
                }                
            }
		}

		public override bool IsFinished()
		{
			return isFinished;
		}

		public override string ToString()
		{
			return nextEpisodeActionInfo.ToString();
		}

        public override Action Clone()
        {
            return new NextEpisodeAction(this);
        }

	    public override void ApplySubstitution(Substitution substitution)
	    {
	        return;
	    }

        private void StopMinds()
        {
            ApplicationLogger.Instance().WriteLine("SF orders all the agents to STOP");
            foreach (Entity entity in Universe.Instance.Entities)
            {
                if (entity is RemoteCharacter)
                {
                    RemoteCharacter remoteCharacter = entity as RemoteCharacter;
                    if (remoteCharacter.IsActive)
                    {
                        remoteCharacter.Send(RemoteCharacter.STOP_MESSAGE);
                    }
                }
            }
        }
	}
}
