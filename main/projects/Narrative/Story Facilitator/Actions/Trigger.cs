// Trigger - Holds a list of actions to execute.			 
//           A trigger can be fired when the condition is true. The trigger condition becomes true when in the current
//			 episode there is a set of events that was never used by this trigger which make the condition true.
//			 Assigned to each trigger is a priority which is used to select the trigger with highest priority when we can
//           fire several triggers
//			 Assigned to each trigger is an unique ID. When a trigger is fired the events[EventInfo] that were necessary
//			 for its condition to become true are marked with the triggerID so that they are not used again to fire the trigger
//			 or else the trigger condition would always be true and the trigger would never stop firing	
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//
// 8-07-2008: Added expectedOutcome to the trigger's class properties
// The expected outcome represents the event the author is expecting to occur if the trigger is executed.
// The expected outcome is used for the double appraisal process, if the author specifies an expected outcome for the trigger
// the SF will query the agents about the expected outcome and not about the individual Narrative Actions 
// contained in the trigger (normal procedure)
//

using System;
using System.Collections;
using System.Collections.Generic;
using Narrative.Conditions;

namespace Narrative.Actions
{
	/// <summary>
	/// Holds a list of actions to execute: 'actions'
	/// when a certain condition is true: 'Condition'
	/// A trigger can be fired when the condition is true. The trigger condition becomes true when in the current
	/// episode there is a set of events that was never used by this trigger which make the condition true.
	/// Assigned to each trigger is a priority which is used to select the trigger with highest priority when we can
	/// fire several triggers
	/// Assigned to each trigger is an unique ID. When a trigger has is fired the events[EventInfo] that were necessary
	/// for its condition to become true are marked with the triggerID so that they are not used again to fire the trigger
	/// or else the trigger condition would always be true and the trigger would never stop firing	
	/// </summary>
	[Serializable()]
	public class Trigger
	{
	    public const string UNSPECIFIED_OUTCOME = "";

		private static int triggerIDNumber = 0; 
		private TriggerConditionList conditionList;
		private ArrayList actions;
		private int priority;
	    private string expectedOutcome;
		private readonly int triggerID;

		#region Properties
		public TriggerConditionList ConditionList
		{
			get { return conditionList; }
			set { conditionList = value; }
		}

		public int Priority
		{
			get { return priority; }
			set 
			{ 
				if (value > 10)
				{
					priority = 10;					
				}else if (value < 0)
				{
					priority = 0;
				}else					  
					priority = value; 
			}
		}

		public ArrayList Actions
		{
			get { return actions; }
			set { actions = value; }
		}

		public int TriggerID
		{
			get { return triggerID; }
		}

	    public string ExpectedOutcome
	    {
	        get { return expectedOutcome; }
	        set { expectedOutcome = value; }
	    }

	    #endregion

		public Trigger(int priority)
		{
			Priority = priority;
			Actions = new ArrayList();
			triggerID = triggerIDNumber;
			triggerIDNumber++;
		    ExpectedOutcome = UNSPECIFIED_OUTCOME;
		}
	    
	    public Trigger(Trigger trigger)
	    {
            this.conditionList = new TriggerConditionList(trigger.conditionList);
	        actions = new ArrayList();
	        foreach (Action action in trigger.actions)
	        {
                this.actions.Add(action.Clone());
	        }
            this.priority = trigger.priority;
            this.triggerID = trigger.triggerID;
	        this.expectedOutcome = trigger.expectedOutcome;
	    }

        //Makes a copy of the trigger, but with a new ID (it is a different trigger)
        public Trigger(Trigger trigger, bool newID)
        {
            this.conditionList = new TriggerConditionList(trigger.conditionList);
            actions = new ArrayList();
            foreach (Action action in trigger.actions)
            {
                this.actions.Add(action.Clone());
            }
            this.priority = trigger.priority;
            if (!newID)
                this.triggerID = trigger.triggerID;
            else
            {
                triggerID = triggerIDNumber;
                triggerIDNumber++;                
            }
            this.expectedOutcome = trigger.expectedOutcome;
        }

		public void AddAction(Action a)
		{
			Actions.Add(a);
		}

		public bool IsSatisfied(StoryMemory storyMemory)
		{
			return ConditionList.IsSatisfied(storyMemory);
		}

		public override string ToString()
		{
			string res = "TRIGGER:" + TriggerID + ":" + Priority + "\n"  ;
			res+=ConditionList.ToString();
			res+="TRIGGER_ACTION_LIST\n";
			foreach(Action action in Actions)
				res+=action.ToString();
			return res+"\n";
		}

		/// <summary>
		/// This method causes that the events/properties stored in the storyMemory used to fire this trigger		
		/// are updated so they are not used again to satisfy the conditions of this trigger
		/// </summary>
		/// <param name="storyMemory"></param>
		public void UpdateStoryMemory(StoryMemory storyMemory)
		{
			this.conditionList.UpdateConditionsInKnwoledgeBase(storyMemory);
		}
	    
	    public Trigger Clone()
	    {
	        return new Trigger(this);
	    }

        public List<string> GetVariableList()
        {
            List<string> variables = new List<string>();
            foreach (Condition condition in this.ConditionList.GetConditions())
            {
                List<string> conditionVariables = condition.GetVariableList();
                foreach (string variable in conditionVariables)
                {
                    if (!variables.Contains(variable))
                    {
                        variables.Add(variable);
                    }
                }
            }

            return variables;
        }

        public string GetFirstVariable()
        {
            List<string> variables = GetVariableList();
            if (variables.Count == 0)
                return null;
            else
                return variables[0];            
        }

        public void ApplySubstitution(Narrative.Conditions.Substitution substitution)
        {
            ConditionList.ApplySubstitution(substitution);
            foreach (Action action in Actions)
            {
                action.ApplySubstitution(substitution);
            }
        }
    }
}
