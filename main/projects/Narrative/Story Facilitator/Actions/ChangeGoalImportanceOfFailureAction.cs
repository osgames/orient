// ChangeGoalImportanceOfFailureAction - Implementation of the ChangeGoalImportanceOfFailure Narrative Action
//										 This action creates a perception that is sent to the corresponding RemoteCharacter
//										 This perception consists of: CIF GOAL_NAME NEW_VALUE
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//


using System;
using ION.Core.Extensions;
using Narrative.Conditions;

namespace Narrative.Actions
{
	/// <summary>
	/// Implementation of the ChangeGoalImportanceOfFailure Narrative Action
	/// This action creates a perception that is sent to the corresponding RemoteCharacter
	/// This perception consists of: CIF GOAL_NAME NEW_VALUE
	/// </summary>
	[Serializable()]
	public class ChangeGoalImportanceOfFailureAction : Action
	{
		
		#region ActionInfo
		/// <summary>
		/// Holds the information necessary to change the importance
		/// of failure of a Character's goal	
		/// </summary>
		[Serializable()]
		public class ChangeGoalImportanceOfFailureActionInfo
		{
			#region Fields
		
			private string characterName;
			private string goalName;
			private int importanceOfFailure;

			#endregion

			#region Properties

			public string CharacterName
			{
				get { return characterName; }
				set { characterName = value; }
			}

			public string GoalName
			{
				get { return goalName; }
				set { goalName = value; }
			}

			public int ImportanceOfFailure
			{
				get { return importanceOfFailure; }
				set { importanceOfFailure = value; }
			}

			#endregion
		
			public ChangeGoalImportanceOfFailureActionInfo(string characterName, string goalName, int importanceOfFailure)
			{
				CharacterName = characterName;
				GoalName = goalName;
				ImportanceOfFailure = importanceOfFailure;				
			}
		    
		    public ChangeGoalImportanceOfFailureActionInfo(ChangeGoalImportanceOfFailureActionInfo actionInfo)
		    {
                this.characterName = actionInfo.characterName;
                this.goalName = actionInfo.goalName;
                this.importanceOfFailure = actionInfo.importanceOfFailure;
		    }

			public override string ToString()
			{
				return "CHANGE_GOAL_IMPORTANCE_FAILURE_AINFO:"+CharacterName+":"+GoalName+":"+ImportanceOfFailure+"\n";
			}
		}
		#endregion

		private bool isFinished = false;
		private ChangeGoalImportanceOfFailureActionInfo changeGoalImportanceOfFailureAInfo;
		private const string MESSAGE = "CIF "; //CIF-Change Importance Of Failure


		public ChangeGoalImportanceOfFailureAction(ChangeGoalImportanceOfFailureActionInfo changeGoalImportanceOfFailureAInfo)
		{
			this.changeGoalImportanceOfFailureAInfo = changeGoalImportanceOfFailureAInfo;
		}
	    
	    public ChangeGoalImportanceOfFailureAction(ChangeGoalImportanceOfFailureAction action)
	    {
	        this.changeGoalImportanceOfFailureAInfo = new ChangeGoalImportanceOfFailureActionInfo(action.changeGoalImportanceOfFailureAInfo);
	    }

		public override void Start(StoryFacilitator storyFacilitator)
		{
            ApplicationLogger.Instance().WriteLine("Change Goal Importance Of Failure Narrative Action (unfinished)");
            ApplicationLogger.Instance().Write(this.changeGoalImportanceOfFailureAInfo.ToString());            

            //SIMULATION
            storyFacilitator.StoryMemory.Tell(new StoryEvent(storyFacilitator.Name, StoryEvent.CHANGE_GOAL_IMPORTANCE_OF_FAILURE, changeGoalImportanceOfFailureAInfo.CharacterName + " " + changeGoalImportanceOfFailureAInfo.GoalName + " " + changeGoalImportanceOfFailureAInfo.ImportanceOfFailure, StoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeID);
            isFinished = true;

			/*foreach(Agent agent in Universe.Instance.Agents.All)
			{
				if (agent is RemoteCharacter)
				{
					if (agent.ID.Name.Equals(changeGoalImportanceOfFailureAInfo.CharacterName))
					{
						RemoteCharacter rmChar = agent as RemoteCharacter;
						string changeGoalImportanceOfFailureMessage = MESSAGE+changeGoalImportanceOfFailureAInfo.GoalName+" "+changeGoalImportanceOfFailureAInfo.ImportanceOfFailure;
						rmChar.Perception(changeGoalImportanceOfFailureMessage);
						storyFacilitator.StoryMemory.Tell(new TellStoryEvent(storyFacilitator.Name,TellStoryEvent.CHANGE_GOAL_IMPORTANCE_OF_FAILURE, changeGoalImportanceOfFailureAInfo.CharacterName + " " + changeGoalImportanceOfFailureAInfo.GoalName + " " + changeGoalImportanceOfFailureAInfo.ImportanceOfFailure,  TellStoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name);
						isFinished=true;
						return;
					}
				}
			}
			throw new ActionException("ChangeGoalImportanceOfFailureAction: Character " + changeGoalImportanceOfFailureAInfo.CharacterName + " not found in Universe...");*/
		}

		public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
		{            
		}

		public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
		{            
		}

		public override bool IsFinished()
		{
			return isFinished;
		}

		public override string ToString()
		{
			return changeGoalImportanceOfFailureAInfo.ToString();
		}

        public override Action Clone()
        {
            return new ChangeGoalImportanceOfFailureAction(this);
        }

	    public override void ApplySubstitution(Substitution substitution)
	    {
	        throw new System.NotImplementedException();
	    }
	}
}
