// RemoveObjectAction - Implementation of the narrative action Remove Object
//					    Removing an object(item) will remove the corresponding entity from the framework,
//                      and will remove all the episode level properties of the object from the StoryFacilitator's
//                      memory
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using ION.Core;
using ION.Core.Extensions;
using Narrative.Conditions;
using Narrative.Participants;

namespace Narrative.Actions
{
    [Serializable()]
    public class RemoveObjectAction : Action
    {

        #region ActionInfo
        /// <summary>
        /// Holds the information necessary to execute an action of the
        /// type: RemoveObject
        /// </summary>
        [Serializable()]
        public class RemoveObjectActionInfo
        {
            private string objectName;
            
            public RemoveObjectActionInfo(){}
            
            public RemoveObjectActionInfo(RemoveObjectActionInfo actionInfo)
            {
                this.objectName = actionInfo.objectName;
            }

            #region Properties
            public string ObjectName
            {
                get
                {
                    return objectName;
                }
                set
                {
                    objectName = value;
                }
            }
            #endregion

            public override string ToString()
            {
                return "REMOVE_OBJECT_AINFO:" + ObjectName + "\n";
            }

        }
        #endregion
        
        
        RemoveObjectActionInfo removeObjectActionInfo;
        bool finished = false;
        
        public RemoveObjectAction(RemoveObjectActionInfo removeObjectActionInfo)
        {
            this.removeObjectActionInfo = removeObjectActionInfo;
        }
        
        public RemoveObjectAction(RemoveObjectAction action)
        {
            this.removeObjectActionInfo = action.removeObjectActionInfo;
        }
        
        public override void Start(StoryFacilitator storyFacilitator)
        {
            ApplicationLogger.Instance().WriteLine("Remove Object Narrative Action");
            ApplicationLogger.Instance().Write(this.removeObjectActionInfo.ToString());

            ObjectParticipant objectParticipant = storyFacilitator.ActiveEpisode.GetObject(removeObjectActionInfo.ObjectName);
            foreach (ParticipantProperty property in objectParticipant.GetProperties())
            {
                storyFacilitator.StoryMemory.Retract(property); //Retracts the episode-level object's properties in StoryMemory
            }
            Universe.Instance.DestroyEntity(removeObjectActionInfo.ObjectName);                  
        }

        public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
        {
            //do Nothing
        }

        public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
        {
            if (ev.Equals(new Narrative.StoryEvent(storyFacilitator.Name, StoryEvent.ENTITY_REMOVED, String.Empty, StoryEvent.EventType.ACTION_FINISHED)))
            {
                finished = true;
                storyFacilitator.StoryMemory.Tell(new StoryEvent(storyFacilitator.Name, StoryEvent.REMOVE_OBJECT_ACTION, removeObjectActionInfo.ObjectName, StoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeID);
            }
        }

        public override bool IsFinished()
        {
            return finished;
        }

        public override Action Clone()
        {
            return new RemoveObjectAction(this);
        }

        public override void ApplySubstitution(Substitution substitution)
        {
            throw new System.NotImplementedException();
        }

        public override string GetActionRepresentation()
        {
            //REMOVE_OBJECT CHARACTER_NAME
            return "REMOVE_OBJECT " + removeObjectActionInfo.ObjectName;
        }
    }
}
