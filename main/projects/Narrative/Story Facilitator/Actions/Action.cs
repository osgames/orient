// Action - Abstract class for the Narrative actions implementation
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//
using System;
using Narrative.Conditions;

namespace Narrative.Actions
{
	/// <summary>
	/// Abstract class for the Narrative actions implementation
	/// </summary>
	[Serializable()]
	public abstract class Action
	{
	    public const String DOUBLE_APPRAISAL_IGNORE_ACTION = "IgnoreAction";

		#region Exceptions
		public class ActionException : Exception
		{
			public ActionException()
			{
			}
			public ActionException(string errorMessage) : base(errorMessage)
			{
			}
			public ActionException(string errorMessage, Exception innerException) :base(errorMessage, innerException)
			{
			}
		}
		#endregion

		public abstract void Start(StoryFacilitator storyFacilitator);
		public abstract void Update(StoryFacilitator storyFacilitator, int timeSlice);
		public abstract void Update(StoryFacilitator storyFacilitator, StoryEvent ev);
		public abstract bool IsFinished();
        public abstract Action Clone();
	    public abstract void ApplySubstitution(Substitution substitution);


        //Default for an Narrative action is to be ignored in the double appraisal system
        //In order for it not to be ignored  this method should be overriden to return a string
        //that represents the action and that the mind (FAtiMA) can interpret
        public virtual String GetActionRepresentation()
        {
            return DOUBLE_APPRAISAL_IGNORE_ACTION;
        }
        
	}
}
