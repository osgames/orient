// ChangeGoalImportanceOfSuccessAction - Implementation of the ChangeGoalImportanceOfSuccess Narrative Action
//										 This action creates a perception that is sent to the corresponding RemoteCharacter
//										 This perception consists of: CIS:GOAL_NAME:NEW_VALUE
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//


using System;
using ION.Core.Extensions;
using Narrative.Conditions;

namespace Narrative.Actions
{
	/// <summary>
	/// Implementation of the ChangeGoalImportanceOfSuccess Narrative Action
	/// This action creates a perception that is sent to the corresponding RemoteCharacter
	/// This perception consists of: CIS:GOAL_NAME:NEW_VALUE
	/// </summary>
	[Serializable()]
	public class ChangeGoalImportanceOfSuccessAction : Action
	{

		#region ActionInfo
		/// <summary>
		/// Holds the information necessary to change the importance
		/// of success of a Character's goal	
		/// </summary>
		[Serializable()]
		public class ChangeGoalImportanceOfSuccessActionInfo
		{
			#region Fields
		
			private string characterName;
			private string goalName;
			private int importanceOfSuccess;

			#endregion

			#region Properties

			public string CharacterName
			{
				get { return characterName; }
				set { characterName = value; }
			}

			public string GoalName
			{
				get { return goalName; }
				set { goalName = value; }
			}

			public int ImportanceOfSuccess
			{
				get { return importanceOfSuccess; }
				set { importanceOfSuccess = value; }
			}

			#endregion
		
			public ChangeGoalImportanceOfSuccessActionInfo(string characterName, string goalName, int importanceOfSuccess)
			{
				CharacterName = characterName;
				GoalName = goalName;
				ImportanceOfSuccess = importanceOfSuccess;				
			}

			public override string ToString()
			{
				return "CHANGE_GOAL_IMPORTANCE_SUCCESS_AINFO:"+CharacterName+":"+GoalName+":"+ImportanceOfSuccess+"\n";
			}

		}
		#endregion

		private bool isFinished = false;
		private const string MESSAGE = "CIS "; //CIS-Change Importance Of Success

		private ChangeGoalImportanceOfSuccessActionInfo changeGoalImportanceOfSuccessAInfo;
		public ChangeGoalImportanceOfSuccessAction(ChangeGoalImportanceOfSuccessActionInfo changeGoalImportanceOfSuccessAInfo)
		{
			this.changeGoalImportanceOfSuccessAInfo = changeGoalImportanceOfSuccessAInfo;
		}

		public override void Start(StoryFacilitator storyFacilitator)
		{
            ApplicationLogger.Instance().WriteLine("Change Goal Importance Of Success Narrative Action (unfinished)");
            ApplicationLogger.Instance().WriteLine(this.changeGoalImportanceOfSuccessAInfo.ToString());            

            //SIMULATE
            storyFacilitator.StoryMemory.Tell(new StoryEvent(storyFacilitator.Name, StoryEvent.CHANGE_GOAL_IMPORTANCE_OF_SUCCESS, changeGoalImportanceOfSuccessAInfo.CharacterName + " " + changeGoalImportanceOfSuccessAInfo.GoalName + " " + changeGoalImportanceOfSuccessAInfo.ImportanceOfSuccess, StoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeID);
            isFinished = true;

			/*foreach(Agent agent in Universe.Instance.Agents.All)
			{
				if (agent is RemoteCharacter)
				{
					if (agent.ID.Name.Equals(changeGoalImportanceOfSuccessAInfo.CharacterName))
					{
						RemoteCharacter rmChar = agent as RemoteCharacter;
						string changeGoalImportanceOfSuccessMessage = MESSAGE+changeGoalImportanceOfSuccessAInfo.GoalName+" "+changeGoalImportanceOfSuccessAInfo.ImportanceOfSuccess;
						rmChar.Perception(changeGoalImportanceOfSuccessMessage);
						storyFacilitator.StoryMemory.Tell(new TellStoryEvent(storyFacilitator.Name,TellStoryEvent.CHANGE_GOAL_IMPORTANCE_OF_SUCCESS, changeGoalImportanceOfSuccessAInfo.CharacterName + " " + changeGoalImportanceOfSuccessAInfo.GoalName + " " + changeGoalImportanceOfSuccessAInfo.ImportanceOfSuccess, TellStoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name);
						isFinished=true;
						return;
					}
				}
			}
			throw new ActionException("ChangeGoalImportanceOfSuccessAction: Character " + changeGoalImportanceOfSuccessAInfo.CharacterName + " not found in Universe...");*/
		}

		public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
		{
            
		}

		public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
		{
            
		}

		public override bool IsFinished()
		{
			return isFinished;
		}

		public override string ToString()
		{
			return changeGoalImportanceOfSuccessAInfo.ToString();
		}

	    public override Action Clone()
	    {
	        throw new NotImplementedException();
	    }

	    public override void ApplySubstitution(Substitution substitution)
	    {
	        throw new System.NotImplementedException();
	    }
	}
}
