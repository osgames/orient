// NarrateAction - 	Implementation of the action Narrate
//					The narration text will be displayed for NARRATE_TIME milliseconds
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using ION.Core;
using System.Collections.Generic;
using ION.Core.Extensions;
using Narrative.Conditions;
using Narrator=ION.Realizer.Orient.Entities.Narrator;

namespace Narrative.Actions
{
    /// <summary>
    /// Implementation of the action Narrate
    /// The narration text will be displayed for NARRATE_TIME milliseconds
    /// </summary>
    [Serializable()]
    public class NarrateAction : Action
    {
        #region ActionInfo
        /// <summary>
        /// Holds the information necessary to execute an action of the
        /// type: Narrate
        /// </summary>
        [Serializable()]
        public class NarrateActionInfo
        {
            private string text;
            private bool waitForActionEnd = true;
            private string soundFile;

            #region Properties
            public string Text
            {
                get
                {
                    return text;
                }
                set
                {
                    text = value;
                }
            }

            public string SoundFile
            {
                get { return soundFile; }
                set { soundFile = value; }
            }

            public bool WaitForActionEnd
            {
                get { return waitForActionEnd; }
                set { waitForActionEnd = value; }
            }

            #endregion

            public NarrateActionInfo() { }

            public NarrateActionInfo(NarrateActionInfo actionInfo)
            {
                this.text = actionInfo.text;
                this.soundFile = actionInfo.SoundFile;
                this.waitForActionEnd = actionInfo.waitForActionEnd;
            }

            public override string ToString()
            {
                return "NARRATE_AINFO:" + Text + "\n";
            }
        }
        #endregion

        public const int NARRATE_TIME = 3000; //3 seconds		

        public NarrateActionInfo narrateActionInfo;
        bool finished = false;
        int elapsedTime = 0;

        public NarrateAction(NarrateActionInfo narrateActionInfo)
        {
            this.narrateActionInfo = narrateActionInfo;
        }

        public NarrateAction(NarrateAction action)
        {
            this.narrateActionInfo = new NarrateActionInfo(action.narrateActionInfo);
        }
        public override void Start(StoryFacilitator storyFacilitator)
        {
            ApplicationLogger.Instance().WriteLine("Narrate Narrative Action");
            ApplicationLogger.Instance().Write(narrateActionInfo.ToString());
            System.Console.WriteLine("SF says: " + narrateActionInfo.Text);

            Narrator narrator = Universe.Instance.GetEntity<Narrator>(StoryFacilitator.NARRATOR_NAME);
            Dictionary<string, object> actionArguments = new Dictionary<string, object>();
            actionArguments.Add("utterance", narrateActionInfo.Text);
            if (storyFacilitator.Scenario.Language == "EN")
                actionArguments.Add("file", "./data/audio-data/narrator/english/" + narrateActionInfo.SoundFile);
            else if (storyFacilitator.Scenario.Language == "DE")
                actionArguments.Add("file", "./data/audio-data/narrator/german/" + narrateActionInfo.SoundFile);
            else
                throw new StoryFacilitator.StoryFacilitatorException(
                    "Invalid language for scenario (in NarrateAction): " + storyFacilitator.Scenario.Language);
            narrator.GetAction("say").Start(new Arguments(actionArguments));

            UserLogger.Instance().WriteLine("Narrator: " + narrateActionInfo.Text);
            if (!this.narrateActionInfo.WaitForActionEnd)
            {
                finished = true;
            }
        }

        public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
        {
            elapsedTime += timeSlice;
            //if (elapsedTime > NARRATE_TIME)
            //{
            //    storyFacilitator.StoryMemory.Tell(new StoryEvent(storyFacilitator.Name, StoryEvent.NARRATE_ACTION, narrateActionInfo.Text, StoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeID);
            //    finished = true;
            //    storyFacilitator.UserInterface.SetTextThreadSafe("");
            //}	
        }

        public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
        {
            if ((ev.Subject == StoryFacilitator.NARRATOR_NAME) && (ev.Type == StoryEvent.EventType.ACTION_FINISHED))
            {
                finished = true;
            }
        }

        public override bool IsFinished()
        {
            return finished;
        }

        public override string ToString()
        {
            return narrateActionInfo.ToString();
        }

        public override Action Clone()
        {
            return new NarrateAction(this);
        }

        public override void ApplySubstitution(Substitution substitution)
        {
            this.narrateActionInfo.Text = substitution.Apply(narrateActionInfo.Text);
        }
    }
}
