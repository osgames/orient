// RemoveGoalAction - Implementation of the ChangeGoalImportanceOfFailure Narrative Action
//					  This action creates a perception that is sent to the corresponding RemoteCharacter
//					  This perception consists of: REMOVEGOAL GOAL_NAME
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using ION.Core;
using ION.Core.Extensions;
using Narrative.Conditions;

namespace Narrative.Actions
{
	/// <summary>
	/// Summary description for RemoveGoalAction.
	/// </summary>
	[Serializable()]
	public class RemoveGoalAction : Action
	{
		#region ActionInfo
		/// <summary>
		/// Summary description for RemoveGoalActionInfo.
		/// </summary>
		[Serializable()]
		public class RemoveGoalActionInfo
		{
			#region Fields

			private string characterName;
			private string goalName;

			#endregion

			#region Properties

			public string CharacterName
			{
				get { return characterName; }
				set { characterName = value; }
			}

			public string GoalName
			{
				get { return goalName; }
				set { goalName = value; }
			}

			#endregion

			public RemoveGoalActionInfo(string characterName, string goalName)
			{				
				CharacterName = characterName;
				GoalName = goalName;
			}
		}
		#endregion

		private const string REMOVEGOAL = "REMOVEGOAL ";
		RemoveGoalActionInfo removeGoalAInfo;
		bool isFinished = false;

		public RemoveGoalAction(RemoveGoalActionInfo removeGoalAInfo)
		{
			this.removeGoalAInfo = removeGoalAInfo;
		}

        public RemoveGoalAction(RemoveGoalAction removeGoalAction)
        {
            this.removeGoalAInfo = new RemoveGoalActionInfo(removeGoalAction.removeGoalAInfo.CharacterName,
                                                            removeGoalAction.removeGoalAInfo.GoalName);
        }

		public override void Start(StoryFacilitator storyFacilitator)
		{
		    ApplicationLogger.Instance().WriteLine("RemoveGoal Narrative Action");
		    ApplicationLogger.Instance().Write(removeGoalAInfo.ToString());
		    
		    try
		    {
		        FAtiMA.RemoteAgent.RemoteCharacter rmChar =
		            Universe.Instance.GetEntity<FAtiMA.RemoteAgent.RemoteCharacter>(removeGoalAInfo.CharacterName);
		        string removeGoalSTR = REMOVEGOAL + removeGoalAInfo.GoalName;

		        rmChar.Send(removeGoalSTR);
		        isFinished = true;
		    }
		    catch (Exception)
		    {
                throw new ActionException("Error RemoveGoalAction: Character " + removeGoalAInfo.CharacterName);
		    }
	}

		public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
		{            
		}

		public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
		{            
		}

		public override bool IsFinished()
		{
			return isFinished;
		}

		public override string ToString()
		{
			return removeGoalAInfo.ToString();
		}

	    public override Action Clone()
	    {
	        return new RemoveGoalAction(this);
	    }

	    public override void ApplySubstitution(Substitution substitution)
	    {
	        throw new System.NotImplementedException();
	    }
	}
}
