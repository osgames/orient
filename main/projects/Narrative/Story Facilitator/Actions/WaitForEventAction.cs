// WaitForEventAction - Implementation of the WaitForEvent Narrative Action.
//						This action the application halts until a certain event happens in 
//                      the current episode. If the event already happened, the action does nothing.
//                      
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 18/07/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using ION.Core.Extensions;
using Narrative.Conditions;

namespace Narrative.Actions
{
    [Serializable()]
    public class WaitForEventAction : Action
    {       
        #region ActionInfo
        /// <summary>
        ///  Holds the information necessary to execute an action of the
        ///  type:WaitForEvent
        /// </summary>
        [Serializable()]
        public class WaitForEventActionInfo
        {

            private string characterName;
            private string actionName;
            private string parameters;

            public WaitForEventActionInfo() { }

            public WaitForEventActionInfo(WaitForEventActionInfo wfeAI)
            {
                this.actionName = wfeAI.actionName;
                this.characterName = wfeAI.characterName;
                this.parameters = wfeAI.parameters;
            }

            public override string ToString()
            {
                return "WAIT_FOR_EVENT_AINFO:" + CharacterName + ":" + ActionName + ":" + Parameters + "\n";
            }

            #region Properties
            public string CharacterName
            {
                get
                {
                    return characterName;
                }
                set
                {
                    characterName = value;
                }
            }

            public string Parameters
            {
                get { return parameters; }
                set { parameters = value; }
            }

            public string ActionName
            {
                get { return actionName; }
                set { actionName = value; }
            }
            #endregion
        }
        #endregion

        bool isFinished = false;
        public WaitForEventActionInfo waitForEventActionInfo;
        
        public WaitForEventAction(WaitForEventActionInfo waitForEventActionInfo)
        {
            this.waitForEventActionInfo = waitForEventActionInfo;
        }
        
        public WaitForEventAction(WaitForEventAction waitForEventAction)
        {
            this.waitForEventActionInfo = waitForEventAction.waitForEventActionInfo;
        }


        public override void Start(StoryFacilitator storyFacilitator)
        {
            EventCondition condition = new EventCondition(this.waitForEventActionInfo.CharacterName, this.waitForEventActionInfo.ActionName, this.waitForEventActionInfo.Parameters);
            condition.CurrentEpisode = true;
            condition.EpisodeID = storyFacilitator.ActiveEpisode.EpisodeID;
            if (storyFacilitator.StoryMemory.Ask(condition))
                isFinished = true;
            
            ApplicationLogger.Instance().WriteLine("WaitForEvent Narrative Action");
            ApplicationLogger.Instance().Write(waitForEventActionInfo.ToString());
        }

        public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
        {
        }

        public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
        {
            //Necessary to re-test condition because parameters for this narrative action might contain "*" indicating that the parameter
            //should be ignored: for example actionName="*" characterName="John" parameters="*"
            EventCondition condition = new EventCondition(this.waitForEventActionInfo.CharacterName, this.waitForEventActionInfo.ActionName, this.waitForEventActionInfo.Parameters);
            condition.CurrentEpisode = true;
            condition.EpisodeID = storyFacilitator.ActiveEpisode.EpisodeID;
            if (storyFacilitator.StoryMemory.Ask(condition))
                isFinished = true;
        }

        public override bool IsFinished()
        {
            return isFinished;
        }

        public override Action Clone()
        {
            return new WaitForEventAction(this);
        }

        public override void ApplySubstitution(Substitution substitution)
        {
            this.waitForEventActionInfo.CharacterName = substitution.Apply(waitForEventActionInfo.CharacterName);
            this.waitForEventActionInfo.Parameters = substitution.Apply(waitForEventActionInfo.Parameters);
        }
    }
}
