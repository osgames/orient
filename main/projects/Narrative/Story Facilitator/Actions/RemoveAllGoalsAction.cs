// RemoveAllGoalsAction - 	Removes all the goals from a character
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using ION.Core.Extensions;
using Narrative.Conditions;

namespace Narrative.Actions
{
	/// <summary>
	/// Removes all the goals from a character
	/// </summary>
	[Serializable()]
	public class RemoveAllGoalsAction : Action 
	{
		#region ActionInfo
		/// <summary>
		/// Holds the information necessary to perform a RemoveAllGoalsAction
		/// </summary>
		[Serializable()]
		public class RemoveAllGoalsActionInfo 
		{
			private string characterName;

			public string CharacterName
			{
				get { return characterName; }
				set { characterName = value; }
			}

			public RemoveAllGoalsActionInfo(string characterName)
			{				
				CharacterName = characterName;
			}
		}
		#endregion

		private const string REMOVEALLGOALS = "REMOVEALLGOALS";
		RemoveAllGoalsActionInfo removeAllGoalsAInfo;
		bool isFinished = false;

		public RemoveAllGoalsAction(RemoveAllGoalsActionInfo removeAllGoalsAInfo)
		{
			this.removeAllGoalsAInfo = removeAllGoalsAInfo;
		}

		public override void Start(StoryFacilitator storyFacilitator)
		{
            ApplicationLogger.Instance().WriteLine("RemoveAllGoals Narrative Action (unfinished)");
            ApplicationLogger.Instance().Write(removeAllGoalsAInfo.ToString());            

            //SIMULATE
            storyFacilitator.StoryMemory.Tell(new StoryEvent(storyFacilitator.Name, StoryEvent.REMOVE_ALL_GOALS, removeAllGoalsAInfo.CharacterName, StoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeID);
            isFinished = true;

			//Get the instance of the RemoteCharacter to how we are going to remove the goals
            /*
			foreach(Agent ag in Universe.Instance.Agents.All)
			{
				if (ag is RemoteCharacter)
				{
					RemoteCharacter rmChar = ag as RemoteCharacter;
					if (rmChar.ID.Name.Equals(removeAllGoalsAInfo.CharacterName))
					{
						string perc = REMOVEALLGOALS;
						rmChar.Perception(perc);
						storyFacilitator.StoryMemory.Tell(new TellStoryEvent(storyFacilitator.Name,TellStoryEvent.REMOVE_ALL_GOALS, removeAllGoalsAInfo.CharacterName, TellStoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name);
						isFinished = true;
						return;
					}
				}
			}
			throw new ActionException("RemoveAllGoalsAction: Character " + removeAllGoalsAInfo.CharacterName + " not found in Universe...");*/
		}

		public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
		{            
		}

		public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
		{           
		}

		public override bool IsFinished()
		{
			return isFinished;
		}

		public override string ToString()
		{
			return removeAllGoalsAInfo.ToString();
		}

	    public override Action Clone()
	    {
	        throw new NotImplementedException();
	    }

	    public override void ApplySubstitution(Substitution substitution)
	    {
	        throw new System.NotImplementedException();
	    }
	}
}
