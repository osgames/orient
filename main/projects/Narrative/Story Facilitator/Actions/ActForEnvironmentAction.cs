// ActForEnvironmentAction - Implementation of the ActForEnvironment Narrative Action.
//						   This action makes the environment perform an action
//						   described in ActForEnvironmentActionInfo
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 22/10/2008
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//


using System;
using System.Collections.Generic;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Extensions;
using ION.Realizer.Orient.Entities;
using Narrative.Conditions;
using Narrative.States;

namespace Narrative.Actions
{
    /// <summary>
    /// Implementation of the ActForEnvironment Narrative Action.
    /// </summary>
    [Serializable()]
    public class ActForEnvironmentAction : Action
    {
        public const string ACTION_PARAMETERS = "parameters";
        #region ActionInfo
        /// <summary>
        ///  Holds the information necessary to execute an action of the
        /// type: ActForEnvironment
        /// </summary>
        [Serializable()]
        public class ActForEnvironmentActionInfo
        {
            
            private string actionName;
            private string parameters;

            public ActForEnvironmentActionInfo() { }

            public ActForEnvironmentActionInfo(ActForEnvironmentActionInfo afeAI)
            {
                this.actionName = afeAI.actionName;                
                this.parameters = afeAI.parameters;
            }

            #region Properties


            public string Parameters
            {
                get { return parameters; }
                set { parameters = value; }
            }

            public string ActionName
            {
                get { return actionName; }
                set { actionName = value; }
            }

            #endregion


            public override string ToString()
            {
                return "ACT_FOR_ENVIRONMENT_AINFO:" + ActionName + ":" + Parameters + "\n";
            }

        }
        #endregion

        bool isFinished = false;
        public ActForEnvironmentActionInfo actForEnvironmentActionInfo;

        public ActForEnvironmentAction(ActForEnvironmentActionInfo actionInfo)
        {
            actForEnvironmentActionInfo = actionInfo;
        }

        public ActForEnvironmentAction(ActForEnvironmentAction aFEA)
        {
            this.actForEnvironmentActionInfo = new ActForEnvironmentActionInfo(aFEA.actForEnvironmentActionInfo);
        }

        public override void Start(StoryFacilitator storyFacilitator)
        {

            try
            {
                Entity environment = Universe.Instance.GetEntity<Set>(SelectNextEpisodeState.ENTITY_SET_NAME);
                ION.Core.Action action = environment.GetAction(actForEnvironmentActionInfo.ActionName);
                Dictionary<string, object> actionArguments = new Dictionary<string, object>();
                actionArguments.Add(ACTION_PARAMETERS, actForEnvironmentActionInfo.Parameters);
                action.Start(new Arguments(actionArguments));
            }
            catch (Exception e)
            {
                throw new ActionException("ActForEnvironmentAction: Action " + actForEnvironmentActionInfo.ActionName + " failed in current episode: " + storyFacilitator.ActiveEpisode.Name + " -> " + e.Message);
            }

            ApplicationLogger.Instance().WriteLine("ActForEnvironment Narrative Action");
            ApplicationLogger.Instance().Write(actForEnvironmentActionInfo.ToString());
        }

        public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
        {
        }

        public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
        {
            if (ev.Equals(new StoryEvent(SelectNextEpisodeState.ENTITY_SET_NAME, actForEnvironmentActionInfo.ActionName, actForEnvironmentActionInfo.Parameters, StoryEvent.EventType.ACTION_FINISHED)))
            {
                storyFacilitator.StoryMemory.Tell(new StoryEvent(storyFacilitator.Name, StoryEvent.ACT_FOR_ENVIRONMENT_ACTION, actForEnvironmentActionInfo.ActionName + " " + actForEnvironmentActionInfo.Parameters, StoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeID);
                isFinished = true;
            }
        }

        public override bool IsFinished()
        {
            return isFinished;
        }

        public override string ToString()
        {
            return actForEnvironmentActionInfo.ToString();
        }

        public override Action Clone()
        {
            return new ActForEnvironmentAction(this);
        }

        public override void ApplySubstitution(Substitution substitution)
        {            
            actForEnvironmentActionInfo.Parameters = substitution.Apply(actForEnvironmentActionInfo.Parameters);
        }

        public override string GetActionRepresentation()
        {
            //Format: ACT_FOR_ENVIRONMENT ACTION_NAME ACTION_PARAMETERS
            return "ACT_FOR_ENVIRONMENT " + actForEnvironmentActionInfo.ActionName + " " + actForEnvironmentActionInfo.Parameters;
        }

    }
}
