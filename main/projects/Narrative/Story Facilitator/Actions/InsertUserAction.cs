// InsertUserAction - 	Implementation for the Insert User Action
//							This action adds a user to the world.
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 30/09/2008
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Collections;
using System.Collections.Generic;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Extensions;
using ION.Realizer.Orient.Entities;
using Narrative.Conditions;
using Narrative.Participants;
using Narrative.States;
using User;

namespace Narrative.Actions
{
    /// <summary>
    /// Implementation for the Insert User Action
    /// This action adds a User to the world.
    /// </summary>
    [Serializable()]
    public class InsertUserAction : Action
    {

        #region ActionInfo
        /// <summary>
        /// Holds the information necessary to execute an action of the
        /// type: InsertUser
        /// </summary>
        [Serializable()]
        public class InsertUserActionInfo
        {
            public string userName;

            #region Properties
            public string UserName
            {
                get
                {
                    return userName;
                }
                set
                {
                    userName = value;
                }
            }
            #endregion

            public InsertUserActionInfo()
            {
            }

            public InsertUserActionInfo(InsertUserActionInfo actionInfo)
            {
                this.userName = actionInfo.userName;
            }

            public override string ToString()
            {
                return "INSERT_USER_AINFO:" + UserName + "\n";
            }

        }
        #endregion

        bool finished = false;

        InsertUserActionInfo insertUserActionInfo;

        public InsertUserAction(InsertUserActionInfo insertUserActionInfo)
        {
            this.insertUserActionInfo = insertUserActionInfo;
        }

        public InsertUserAction(InsertUserAction action)
        {
            this.insertUserActionInfo = new InsertUserActionInfo(action.insertUserActionInfo);
        }

        public override void Start(StoryFacilitator storyFacilitator)
        {
            ApplicationLogger.Instance().WriteLine("Insert User Narrative Action");
            ApplicationLogger.Instance().Write(this.insertUserActionInfo.ToString());
            Universe.Instance.CreateEntity<UserEntity>(insertUserActionInfo.UserName);            
        }

        public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
        {            
        }


        public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
        {
            if (ev.Equals(new StoryEvent(storyFacilitator.Name, StoryEvent.ENTITY_ADDED, insertUserActionInfo.UserName, StoryEvent.EventType.ACTION_FINISHED)))
            {
                try
                {
                    ScenarioCharacter scenarioCharacter = storyFacilitator.Scenario.GetCharacter(insertUserActionInfo.UserName);
                    CharacterParticipant characterParticipant = storyFacilitator.ActiveEpisode.GetCharacter(insertUserActionInfo.UserName);

                    ArrayList properties = UpdatePropertiesInStoryMemory(storyFacilitator, scenarioCharacter, characterParticipant); //Puts the episode level properties in the SF's memory

                    CreatePropertyListAction createPropertyListAction =
                        storyFacilitator.GetAction<CreatePropertyListAction>(CreatePropertyListAction.ACTION_NAME);
                    Dictionary<string, object> actionArguments = new Dictionary<string, object>();
                    actionArguments.Add(CreatePropertyListAction.PROPERTIES_LIST, properties);
                    createPropertyListAction.Start(new Arguments(actionArguments));
                }
                catch (Scenario.ScenarioException e)
                {
                    throw new ActionException("InsertUser: User " + insertUserActionInfo.UserName + " not found in scenario " + storyFacilitator.Scenario.Name + " (episodeName=" + storyFacilitator.ActiveEpisode.Name + ")", e);
                }
                catch (Episode.EpisodeException e)
                {
                    throw new ActionException("InsertUser: User " + insertUserActionInfo.UserName + " not found in episode " + storyFacilitator.ActiveEpisode.Name, e);
                }                                                                                        
            }else if (ev.Equals(new StoryEvent(storyFacilitator.Name, CreatePropertyListAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED)))
            {
                finished = true;
            }
        }
   

        private ArrayList UpdatePropertiesInStoryMemory(StoryFacilitator storyFacilitator, ScenarioCharacter scenarioCharacter, CharacterParticipant characterParticipant)
        {
            ArrayList updatedProperties = new ArrayList();
            ArrayList storyLevelProperties = scenarioCharacter.Properties;
            ArrayList episodeLevelProperties = characterParticipant.Properties;

            foreach (ParticipantProperty storyLevelProperty in storyLevelProperties)
            {
                if (storyFacilitator.StoryMemory.HasProperty(scenarioCharacter.Name, storyLevelProperty.Name))
                {
                    updatedProperties.Add(storyFacilitator.StoryMemory.GetParticipantProperty(scenarioCharacter.Name, storyLevelProperty.Name));                        
                }
                else
                {
                    updatedProperties.Add(storyLevelProperty);
                    storyFacilitator.StoryMemory.Tell(storyLevelProperty);
                }
            }

            foreach (ParticipantProperty property in episodeLevelProperties)
            {
                updatedProperties.Add(property);
                storyFacilitator.StoryMemory.Tell(property); // If a property already exists, it is updated
            }
            return updatedProperties;
        }

        public override bool IsFinished()
        {
            return finished;
        }

        public override string ToString()
        {
            return insertUserActionInfo.ToString();
        }

        public override Action Clone()
        {
            return new InsertUserAction(this);
        }

        public override void ApplySubstitution(Substitution substitution)
        {
            this.insertUserActionInfo.UserName = substitution.Apply(insertUserActionInfo.UserName);
        }

        public override string GetActionRepresentation()
        {
            //Format: INSERT_USER USER_NAME
            return "INSERT_USER " + insertUserActionInfo.UserName;
        }
    }
}
