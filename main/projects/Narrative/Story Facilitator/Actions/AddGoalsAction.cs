// AddGoalsAction - Implementation of the AddGoals Narrative Action.
//				    This action adds a set of goals to a character already present in the world
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Collections;
using ION.Core.Extensions;
using Narrative.Conditions;
using Narrative.Participants;

namespace Narrative.Actions
{
	/// <summary>
	/// Summary description for AddGoalsAction.
	/// </summary>
	[Serializable()]
	public class AddGoalsAction : Action
	{
		#region ActionInfo
		/// <summary>
		/// Holds the information necessay to perform an action of the type AddGoals
		/// </summary>
		[Serializable()]
		public class AddGoalsActionInfo 
		{

			#region Fields

			private string characterName;
			private ArrayList goals;

			#endregion


			#region Properties
			public string CharacterName
			{
				get { return characterName; }
				set { characterName = value; }
			}

			public ArrayList Goals
			{
				get { return goals; }
				set { goals = value; }
			}

			#endregion

			public AddGoalsActionInfo(string characterName, ArrayList goals)
			{				
				CharacterName = characterName;
				Goals = goals;
			}
		    
		    public AddGoalsActionInfo(AddGoalsActionInfo actionInfo)
		    {
                this.characterName = actionInfo.characterName;
		        this.goals = new ArrayList();
		        foreach (Goal goal in goals)
		        {
                    this.goals.Add(goal.Clone());
		        }
		    }

			public AddGoalsActionInfo(string characterName)
			{			
				CharacterName = characterName;			
			}


			public void AddGoal(Goal goal)
			{
				Goals.Add(goal);
			}
		}
		#endregion

		private const string ADDGOALS = "ADDGOALS ";
		AddGoalsActionInfo addGoalsAInfo;
		bool isFinished = false;		

		public AddGoalsAction(AddGoalsActionInfo addGoalsAInfo)
		{
			this.addGoalsAInfo = addGoalsAInfo;
		}
	    
	    public AddGoalsAction(AddGoalsAction action)
	    {
	        this.addGoalsAInfo = new AddGoalsActionInfo(action.addGoalsAInfo);
	    }
	    

		public override void Start(StoryFacilitator storyFacilitator)
		{
            ApplicationLogger.Instance().WriteLine("Add GOALS Narrative Action (unfinished)");
            ApplicationLogger.Instance().WriteLine(this.addGoalsAInfo.ToString());            

			if (addGoalsAInfo.Goals.Count == 0)
			{
				isFinished = true;
				return;
			}

            //SIMLATE the action finishing
            storyFacilitator.StoryMemory.Tell(new StoryEvent(storyFacilitator.Name, StoryEvent.ADD_GOALS, addGoalsAInfo.CharacterName + " " + addGoalsAInfo.Goals, StoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeID);
            isFinished = true;

			//Get the instance of the RemoteCharacter to how we are going to add the goals
			/*foreach(Agent ag in Universe.Instance.Agents.All)
			{
				if (ag is RemoteCharacter)
				{
					RemoteCharacter rmChar = ag as RemoteCharacter;
					if (rmChar.ID.Name.Equals(addGoalsAInfo.CharacterName))
					{
						//Add the goals:
						//1-Create a string with all the goals with the format: 
						//ADDGOALS <GoalName>,<importanceofsucess>,<importanceoffailure> <GoalName2>,<importanceofsucess2>,<importanceoffailure2>
						string perception = ADDGOALS;
						foreach(Goal g in addGoalsAInfo.GOALS)
						{
							perception+=g.Name+"|"+g.ImportanceOfSucess+"|"+g.ImportanceOfFailure + " ";
						}
						perception = perception.Trim(); //Removes the extra white space in the end...
						//2-Send the perception to the RemoteAgent
						rmChar.Perception(perception);
						storyFacilitator.StoryMemory.Tell(new TellStoryEvent(storyFacilitator.Name,TellStoryEvent.ADD_GOALS, addGoalsAInfo.CharacterName + " " + addGoalsAInfo.GOALS, TellStoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name);
						isFinished = true;
						return;
					}
				}
			}
			throw new ActionException("AddGoalsAction: Character " + addGoalsAInfo.CharacterName + " not found in Universe...");*/
		}

		public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
		{            
		}

		public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
		{
		}

		public override bool IsFinished()
		{
			return isFinished;
		}

		public override string ToString()
		{
			return addGoalsAInfo.ToString();
		}

        public override Action Clone()
        {
            return new AddGoalsAction(this);
        }

	    public override void ApplySubstitution(Substitution substitution)
	    {
	        throw new NotImplementedException();
	    }
	}
}
