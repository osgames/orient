// InsertCharacterAction - 	Implementation for the Insert Character Action
//							This action adds a character to the world.
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Collections;
using System.Collections.Generic;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Extensions;
using Narrative.Conditions;
using Narrative.Participants;
using Narrative.States;
using User;
using AMS.Profile;

namespace Narrative.Actions
{
	/// <summary>
	/// Implementation for the Insert Character Action
	/// This action adds a character to the world.
	/// </summary>
	[Serializable()]
	public class InsertCharacterAction : Action
	{	
		
		#region ActionInfo
		/// <summary>
		/// Holds the information necessary to execute an action of the
		/// type: InsertCharacter
		/// </summary>
		[Serializable()]
		public class InsertCharacterActionInfo
		{
            public const string POSITION_NOT_SET = "";
			private string characterName;
            private string positionName = POSITION_NOT_SET;

			#region Properties
			public string CharacterName
			{
				get
				{
					return characterName;
				}
				set
				{
					characterName = value;
				}
			}
            public string PositionName
            {
                get { return positionName; }
                set { positionName = value; }
            }
			#endregion
		    
		    public InsertCharacterActionInfo()
		    {		        
		    }
		    
		    public InsertCharacterActionInfo(InsertCharacterActionInfo actionInfo)
		    {
                this.characterName = actionInfo.characterName;
		        this.positionName = actionInfo.positionName;
		    }

			public override string ToString()
			{
				return "INSERT_CHARACTER_AINFO:"+CharacterName+"\n";
			}

		}
		#endregion
	
		bool finished = false;        

		InsertCharacterActionInfo insertCharacterActionInfo;	    
	    
		public InsertCharacterAction(InsertCharacterActionInfo insertCharacterActionInfo)
		{			
			this.insertCharacterActionInfo = insertCharacterActionInfo;		    
		}
	    
	    public InsertCharacterAction(InsertCharacterAction action)
	    {
	        this.insertCharacterActionInfo = new InsertCharacterActionInfo(action.insertCharacterActionInfo);
	    }

		public override void Start(StoryFacilitator storyFacilitator)
		{
            ApplicationLogger.Instance().WriteLine("Insert Character Narrative Action");
            ApplicationLogger.Instance().Write(this.insertCharacterActionInfo.ToString());            

			try
			{
				ScenarioCharacter scenarioCharacter = storyFacilitator.Scenario.GetCharacter(insertCharacterActionInfo.CharacterName);
				CharacterParticipant characterParticipant = storyFacilitator.ActiveEpisode.GetCharacter(insertCharacterActionInfo.CharacterName);
				
				UpdatePropertiesInStoryMemory(storyFacilitator, characterParticipant); //Puts the episode level properties in the SF's memory				
			    
                //Episode-Level properties, story-level properties are created when the entity is created (Start-State)
                ArrayList properties =
                    storyFacilitator.ActiveEpisode.GetCharacter(insertCharacterActionInfo.CharacterName).Properties;
                
                CreatePropertyListAction createPropertyListAction =
                    storyFacilitator.GetAction<CreatePropertyListAction>(CreatePropertyListAction.ACTION_NAME);
                Dictionary<string, object> actionArguments = new Dictionary<string, object>();			    
			    actionArguments.Add(CreatePropertyListAction.PROPERTIES_LIST, properties); 
			    createPropertyListAction.Start(new Arguments(actionArguments));
			    
			}catch(Scenario.ScenarioException e)
			{
                throw new ActionException("InsertCharacter: Character " + insertCharacterActionInfo.CharacterName + " not found in scenario " + storyFacilitator.Scenario.Name + " (episodeName=" + storyFacilitator.ActiveEpisode.Name+")", e);
			}catch(Episode.EpisodeException e)
			{
				throw new ActionException("InsertCharacter: Character " + insertCharacterActionInfo.CharacterName + " not found in episode " + storyFacilitator.ActiveEpisode.Name, e);	
			}
		}

        private void LaunchMind(StoryFacilitator storyFacilitator)
        {
            ApplicationLogger.Instance().WriteLine("LOADING the mind: " + insertCharacterActionInfo.CharacterName);
            ScenarioCharacter scenarioCharacter = storyFacilitator.Scenario.GetCharacter(insertCharacterActionInfo.CharacterName);
            
            //Invoke the action that will cause the character assiciated with this entity to load
            Dictionary<string, object> actionParameters = new Dictionary<string, object>();
            
            string debugAgents = "false";
            //Read Orient's profile
            //AMS.Profile.dll -> http://www.codeproject.com/csharp/ReadWriteXmlIni.asp
            Xml profile = new Xml("OrientProfile.xml");
            try
            {
                using (profile.Buffer())
                {
                    debugAgents = (string)profile.GetValue("Application", "DebugAgents");
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.Instance().WriteLine("Error reading orient profile (InsertCharacterAction): " + ex.Message);
            }

            if (debugAgents == "false")
                actionParameters.Add(LoadRemoteAgentAction.DEBUG_MODE, false);
            else
                actionParameters.Add(LoadRemoteAgentAction.DEBUG_MODE, true);

            actionParameters.Add(LoadRemoteAgentAction.DISPLAY_NAME, scenarioCharacter.DisplayName);

            string userLanguage = "language/user/" + storyFacilitator.Scenario.UserLanguage + "/" +
                    storyFacilitator.Scenario.UserLanguageSet;

            string agentLanguage = "language/agent/" + scenarioCharacter.AgentLanguage + "/" +
                    scenarioCharacter.AgentLanguageSet;

            actionParameters.Add(LoadRemoteAgentAction.LANGUAGE_ACTS, agentLanguage);                        
            actionParameters.Add(LoadRemoteAgentAction.USER_LANGUAGE_ACTS, userLanguage);
            actionParameters.Add(LoadRemoteAgentAction.USER_DIRECTORY, storyFacilitator.UserSessionInfo.LogFileDir + "\\" + UserData.Instance.getUserCode() + "\\");
            //Used so that the StoryFacilitator can identify that scenarioCharacter.Name is the name of the character created in this action
            actionParameters.Add(LoadRemoteAgentAction.NAME, scenarioCharacter.Name);
            
            actionParameters.Add(LoadRemoteAgentAction.LOAD_MODE, LoadRemoteAgentAction.LoadMode.CREATENEW);                

            Universe.Instance.GetEntity<RemoteCharacter>(scenarioCharacter.Name).GetAction<LoadRemoteAgentAction>(LoadRemoteAgentAction.ACTION_NAME).Start(new Arguments(actionParameters));            
        }
	    
	    
	    
		public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
		{
            if(ev.Equals(new StoryEvent(storyFacilitator.Name, CreatePropertyListAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED)))
            {
                if (storyFacilitator.CharacterManager.GetCharacterMindStatus(insertCharacterActionInfo.CharacterName)) //Mind already loaded
                {
                    AwakeCharacter(insertCharacterActionInfo.CharacterName, insertCharacterActionInfo.PositionName);
                }else
                {
                    LaunchMind(storyFacilitator);
                    storyFacilitator.CharacterManager.SetCharacterMindStatus(insertCharacterActionInfo.CharacterName, true); //Mind is marked as loaded
                }
            }else if (ev.Equals(new StoryEvent(insertCharacterActionInfo.CharacterName, LoadRemoteAgentAction.ACTION_NAME, insertCharacterActionInfo.CharacterName, StoryEvent.EventType.ACTION_FINISHED)))
            {
                AwakeCharacter(insertCharacterActionInfo.CharacterName, insertCharacterActionInfo.PositionName);
            }else if (ev.Equals(new StoryEvent(insertCharacterActionInfo.CharacterName, AwakeAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED)))
            {
                StopMind(insertCharacterActionInfo.CharacterName);
                SendGoals(storyFacilitator.ActiveEpisode.GetCharacter(insertCharacterActionInfo.CharacterName).Goals,
                          Universe.Instance.GetEntity<RemoteCharacter>(insertCharacterActionInfo.CharacterName));                                                        
                finished = true;
                storyFacilitator.StoryMemory.Tell(new StoryEvent(storyFacilitator.Name, StoryEvent.INSERT_CHARACTER_ACTION, insertCharacterActionInfo.CharacterName, StoryEvent.EventType.ACTION_FINISHED), storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeID);	
            }
		}

		public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
		{            
		}

		private void UpdatePropertiesInStoryMemory(StoryFacilitator storyFacilitator, CharacterParticipant characterParticipant)
		{						
			ArrayList properties = characterParticipant.Properties;

			foreach (ParticipantProperty property in properties)
			{
				storyFacilitator.StoryMemory.Tell(property); // If a property already exists, it is updated
			}
		}

	    private void StopMind(string characterName)
	    {
            RemoteCharacter remoteCharacter =
                Universe.Instance.GetEntity<RemoteCharacter>(characterName);
            remoteCharacter.Send(RemoteCharacter.STOP_MESSAGE);                	        
	    }
	    
	    private void SendGoals(ArrayList goals, RemoteCharacter remoteCharacter)
	    {
            string goalString = RemoteCharacter.ADD_GOALS_MESSAGE_START;
	        	        
	        for (int i=0; i < goals.Count; i++)
	        {
                Goal goal = (Goal) goals[i]; 
                goalString += goal.Name + "|" + goal.ImportanceOfSucess + "|" + goal.ImportanceOfFailure;
                if (i != (goals.Count-1))
                    goalString += " ";
   	        }
            remoteCharacter.Send(goalString);
	    }
	    

        private void AwakeCharacter(string characterName, string positionName)
        {
            RemoteCharacter remoteCharacter =
                Universe.Instance.GetEntity<RemoteCharacter>(characterName);



            Dictionary<string, object> args = new Dictionary<string, object>();
            args["set"] = SelectNextEpisodeState.ENTITY_SET_NAME;
            if(positionName != InsertCharacterActionInfo.POSITION_NOT_SET)
                args["spot"] = positionName;
           
            remoteCharacter.GetAction("enter-stage").Start(new Arguments(args));
            remoteCharacter.GetAction(ION.Realizer.Orient.Entities.Body.ACTION_STAND_STR).Start();


            remoteCharacter.GetAction<AwakeAction>(AwakeAction.ACTION_NAME).Start();
        }

        public override bool IsFinished()
		{
			return finished;
		}

		public override string ToString()
		{
			return insertCharacterActionInfo.ToString();
		}

        public override Action Clone()
        {
            return new InsertCharacterAction(this);
        }

	    public override void ApplySubstitution(Substitution substitution)
	    {
	        this.insertCharacterActionInfo.CharacterName = substitution.Apply(insertCharacterActionInfo.CharacterName);            
	    }

	    public override string GetActionRepresentation()
        {
            //Format: INSERT_CHARACTER CHARACTER_NAME
            return "INSERT_CHARACTER " + insertCharacterActionInfo.CharacterName;
        }
	}
}
