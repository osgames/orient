// ChangeProperty - Implementation for the Change Property Action
//					This action changes a property of an entity in the ION framework.
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using ION.Core;
using ION.Core.Extensions;
using Narrative.Conditions;

namespace Narrative.Actions
{
    /// <summary>
    /// Implementation for the Insert Character Action
    /// This action adds a character to the world.
    /// </summary>
    [Serializable()]
    public class ChangePropertyAction : Action
    {
        #region ActionInfo
        /// <summary>
        /// Holds the information necessary to execute an action of the
        /// type: ChangeProperty
        /// </summary>
        [Serializable()]
        public class ChangePropertyActionInfo
        {
            private string participantName;
            private string propertyName;
            private string newValue;

            #region Properties

            public string ParticipantName
            {
                get { return participantName; }
                set { participantName = value; }
            }

            public string PropertyName
            {
                get { return propertyName; }
                set { propertyName = value; }
            }

            public string NewValue
            {
                get { return newValue; }
                set { newValue = value; }
            }

            #endregion
            
            public ChangePropertyActionInfo(string participantName, string propertyName, string newValue)
            {
                ParticipantName = participantName;
                PropertyName = propertyName;
                NewValue = newValue;
            }
            
            public ChangePropertyActionInfo(ChangePropertyActionInfo actionInfo)
            {
                this.participantName = actionInfo.participantName;
                this.propertyName = actionInfo.propertyName;
                this.newValue = actionInfo.newValue;
            }

            public override string ToString()
            {
                return "CHANGE_PROPERTY_AINFO:" + ParticipantName + ":" + PropertyName + ":" + NewValue + "\n";
            }

        }
        #endregion        
        
        bool isFinished = false;
        ChangePropertyActionInfo changePropertyAInfo;
        
        public ChangePropertyAction(ChangePropertyActionInfo changePropertyAInfo)
        {
            this.changePropertyAInfo = changePropertyAInfo;
        }
        
        public ChangePropertyAction(ChangePropertyAction action)
        {
            this.changePropertyAInfo = new ChangePropertyActionInfo(action.changePropertyAInfo);
        }
        
        public override void Start(StoryFacilitator storyFacilitator)
        {
            ApplicationLogger.Instance().WriteLine("Change Property Narrative Action");
            ApplicationLogger.Instance().Write(this.changePropertyAInfo.ToString());

            try
            {
                Entity entity = Universe.Instance.GetEntity<Entity>(changePropertyAInfo.ParticipantName);
                Property<string> property = entity.GetProperty<Property<string>>(changePropertyAInfo.PropertyName);
                property.Value = changePropertyAInfo.NewValue;
            }catch(Exception e)
            {
                ApplicationLogger.Instance().WriteLine("ChangePropertyAction: Error while accessing and changing the property value in the ION Framework: " +
                    e.Message);
                throw new ActionException(
                    "ChangePropertyAction: Error while accessing and changing the property value in the ION Framework: " +
                    e.Message);
            }
        }

        public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
        {
            
        }

        public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
        {
            if (ev.Equals(new StoryEvent(changePropertyAInfo.ParticipantName, StoryEvent.PROPERTY_CHANGED, changePropertyAInfo.PropertyName + " " + changePropertyAInfo.NewValue, StoryEvent.EventType.ACTION_FINISHED)))
            {
                isFinished = true;
                storyFacilitator.TellStoryEvent(new StoryEvent(storyFacilitator.Name, StoryEvent.CHANGE_PROPERTY, changePropertyAInfo.PropertyName + " " + changePropertyAInfo.NewValue, StoryEvent.EventType.ACTION_FINISHED));
            }
        }

        public override bool IsFinished()
        {
            return isFinished;
        }

        public override Action Clone()
        {
            return new ChangePropertyAction(this);
        }

        public override void ApplySubstitution(Substitution substitution)
        {
            this.changePropertyAInfo.ParticipantName = substitution.Apply(changePropertyAInfo.ParticipantName);
        }
    }
}
