// ChangeStoryModeAction - Narrative Action that changes the GUI
//			
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using ION.Core;
using ION.Realizer.Orient.Entities;
using Narrative.Conditions;
using ION.Realizer.Orient.Actions.User;
using ION.Core.Extensions;

namespace Narrative.Actions
{
	/// <summary>
	/// Summary description for ChangeStoryModeAction.
	/// </summary>
	[Serializable()]
	public class ChangeStoryModeAction : Action
	{
		#region ActionInfo
		/// <summary>
		/// Holds the information necessary to execute an action that changes the 
		/// StoryMode: InteractionMode, StoryMode
		/// </summary>
		[Serializable()]
		public class ChangeStoryModeActionInfo
		{

			#region Exceptions

			public class StoryModeNotImplementedException : Exception
			{
				public StoryModeNotImplementedException()
				{
				}
				public StoryModeNotImplementedException(string errorMessage) : base(errorMessage)
				{
				}
				public StoryModeNotImplementedException(string errorMessage, Exception innerException) :base(errorMessage, innerException)
				{
				}
			}


			#endregion


            public enum StoryModes { InteractionMode, StoryMode, StoryLoadingMode, UserSelectionMode, FreeNavigationMode, FirstPersonNoFreeLookMode, FirstPersonFreeLookMode}
		
			private StoryModes storyMode;

			#region Properties

			public StoryModes StoryMode
			{
				get { return storyMode; }
				set { storyMode = value; }
			}

			#endregion

			public ChangeStoryModeActionInfo()
			{				
			}

			public ChangeStoryModeActionInfo(StoryModes storyMode)
			{	
				StoryMode = storyMode;
			}
		    
		    public ChangeStoryModeActionInfo(ChangeStoryModeActionInfo actionInfo)
		    {
                this.storyMode = actionInfo.storyMode;
		    }


			public override string ToString()
			{
				return "CHANGE_STORY_MODE_AINFO:"+StoryMode+"\n";
			}

		}
		#endregion

		private bool isFinished;
		private ChangeStoryModeActionInfo changeStoryModeAInfo;
		public ChangeStoryModeAction(ChangeStoryModeActionInfo changeStoryModeAInfo)
		{
			this.changeStoryModeAInfo = changeStoryModeAInfo;
		}
	    
	    public ChangeStoryModeAction(ChangeStoryModeAction action)
	    {
	        this.changeStoryModeAInfo = new ChangeStoryModeActionInfo(action.changeStoryModeAInfo);
	    }

        public override void Start(StoryFacilitator storyFacilitator)
        {
            ApplicationLogger.Instance().WriteLine("Change Story Mode Narrative Action");
            ApplicationLogger.Instance().Write(this.changeStoryModeAInfo.ToString());

            if (changeStoryModeAInfo.StoryMode.Equals(ChangeStoryModeActionInfo.StoryModes.InteractionMode))
            {
                Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME).GetAction<AwakeUserAction>(
                    AwakeUserAction.ACTION_NAME).Start();
            }
            else if (changeStoryModeAInfo.StoryMode.Equals(ChangeStoryModeActionInfo.StoryModes.StoryMode))
            {
                Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME).GetAction<AwakeUserAction>(
                    AwakeUserAction.ACTION_NAME).Start();
            }
            else if (changeStoryModeAInfo.StoryMode.Equals(ChangeStoryModeActionInfo.StoryModes.StoryLoadingMode))
            {
                Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME).GetAction<PutToSleepUserAction>(
                    PutToSleepUserAction.ACTION_NAME).Start();
            }
            else if (changeStoryModeAInfo.StoryMode.Equals(ChangeStoryModeActionInfo.StoryModes.UserSelectionMode))
            {
                //Removes all remote characters and user entity
                RemoveCharacterEntitiesAction removeCharacterEntitiesAction =
                    storyFacilitator.GetAction<RemoveCharacterEntitiesAction>(RemoveCharacterEntitiesAction.ACTION_NAME);
                removeCharacterEntitiesAction.Start();
            }
            else if (changeStoryModeAInfo.StoryMode.Equals(ChangeStoryModeActionInfo.StoryModes.FreeNavigationMode))
            {
                //Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME).GetAction<AwakeUserAction>(
                //    AwakeUserAction.ACTION_NAME).Start();

                // place camera
                Camera camera;
                if (Universe.Instance.TryGetEntity("orient-camera", out camera))
                {
                    Context camContext = new Context();
                    camera.GetAction("free-navigate").Start(camContext);
                    isFinished = true;
                }

            }
            else if (changeStoryModeAInfo.StoryMode.Equals(ChangeStoryModeActionInfo.StoryModes.FirstPersonNoFreeLookMode))
            {
                //Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME).GetAction<AwakeUserAction>(
                //    AwakeUserAction.ACTION_NAME).Start();

                // place camera
                Camera camera;
                if (Universe.Instance.TryGetEntity("orient-camera", out camera))
                {
                    Context camContext = new Context();
                    camera.GetAction("firstPersonNoFreeLook-navigate").Start(camContext);
                    isFinished = true;
                }
            }
            else if (changeStoryModeAInfo.StoryMode.Equals(ChangeStoryModeActionInfo.StoryModes.FirstPersonFreeLookMode))
            {
                //Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME).GetAction<AwakeUserAction>(
                //    AwakeUserAction.ACTION_NAME).Start();

                // place camera
                Camera camera;
                if (Universe.Instance.TryGetEntity("orient-camera", out camera))
                {
                    Context camContext = new Context();
                    camera.GetAction("firstPersonFreeLook-navigate").Start(camContext);
                    isFinished = true;
                }
            }
        }

	    public override void Update(StoryFacilitator storyFacilitator, int timeSlice)
		{            
		}

		public override void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
		{
            if (ev.Equals(new Narrative.StoryEvent(UserEntity.ENTITY_NAME, AwakeAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED)))
            {
                isFinished = true;
            }else if (ev.Equals(new Narrative.StoryEvent(UserEntity.ENTITY_NAME, PutToSleepAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED))){
                isFinished = true;
            }
            else if (ev.Equals(new Narrative.StoryEvent(storyFacilitator.Name, RemoveCharacterEntitiesAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED)))
            {
                //After removing all characters, remove all items
                RemoveAllItemsAndUserAction removeAllItemsAndUserAction =
                    storyFacilitator.GetAction<RemoveAllItemsAndUserAction>(RemoveAllItemsAndUserAction.ACTION_NAME);
                removeAllItemsAndUserAction.Start();
            }
            else if (ev.Equals(new Narrative.StoryEvent(storyFacilitator.Name, RemoveAllItemsAndUserAction.ACTION_NAME, "", StoryEvent.EventType.ACTION_FINISHED)))
            {
                //Init SF and finally change the Interface to the user data form
                storyFacilitator.Init();
                isFinished = true;
            }
		}

		public override bool IsFinished()
		{
			return isFinished;
		}

		public override string ToString()
		{
			return changeStoryModeAInfo.ToString();
		}

        public override Action Clone()
        {
            return new ChangeStoryModeAction(this);
        }

	    public override void ApplySubstitution(Substitution substitution)
	    {
	        return;
	    }
	}
}
