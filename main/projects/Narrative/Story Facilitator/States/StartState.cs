// StartState - Represents the state the storyFacilitator is in when the story starts
//				(Implementation of the state design pattern)
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System.Collections;
using System.Collections.Generic;
using FAtiMA.RemoteAgent;
using FAtiMA.Actions;
using ION.Core;
using ION.Core.Extensions;
using ION.Realizer.Orient.Entities;
using Narrator=ION.Realizer.Orient.Entities.Narrator;

namespace Narrative.States
{
	/// <summary>
	/// Summary description for Start.
	/// </summary>
	public class StartState : State
	{

		private static State state = new StartState();
        private string CHARACTERS_DIR = "data\\characters\\minds\\";
		private StartState()
		{
		}

		public static State Instance()
		{
			ApplicationLogger.Instance().WriteLine("Start state");
			return state;
		}

		public override void StoryEvent(StoryFacilitator storyFacilitator, StoryEvent ev)
		{			
			if ((ev.Subject == storyFacilitator.Name) && (ev.Action == Narrative.StoryEvent.STORY_START))
			{
                AgentActionParser.Instance.ParseFile(CHARACTERS_DIR + "Actions.xml");
                AgentActionEffectUpdater.Instance.initialise();
                Dictionary<string, object> actionArguments = new Dictionary<string, object>();
			    ArrayList charactersToCreate = new ArrayList();
			    foreach (ScenarioCharacter scenarioCharacter in storyFacilitator.Scenario.Cast)
			    {
			        if (scenarioCharacter.Body != "")
			        {
			            charactersToCreate.Add(scenarioCharacter);
			        }
			    }
			    actionArguments.Add(CreateCharacterEntitiesAction.CHARACTER_LIST, charactersToCreate);
                storyFacilitator.GetAction<CreateCharacterEntitiesAction>(CreateCharacterEntitiesAction.ACTION_NAME).Start(new Arguments(actionArguments));
			    //Initialize the UserTimeTracker
                storyFacilitator.UserTimeTracker = new UserTimeTracker(storyFacilitator.UserSessionInfo.LogFileDir, ev.Parameters); //The parameters are the userCode			    
			    //Inicialize Story Log
                ((StoryMemoryDecorator)storyFacilitator.StoryMemory).SetUserCode(ev.Parameters);
                ((StoryMemoryDecorator)storyFacilitator.StoryMemory).LOG_DIR = storyFacilitator.UserSessionInfo.LogFileDir;
			    //Initialize application log
			    ApplicationLogger.Instance().SetSubDir(ev.Parameters);
                ApplicationLogger.LOG_DIR = storyFacilitator.UserSessionInfo.LogFileDir;
			    //Initialize User log (session log)
			    UserLogger.Instance().SetUserCode(ev.Parameters);
                UserLogger.LOG_DIR = storyFacilitator.UserSessionInfo.LogFileDir + StoryFacilitator.USERS_DIR + "\\";
                //Initialize comics log
                ComicsLogger.Instance().SetUserCode(ev.Parameters);
                ComicsLogger.LOG_DIR = storyFacilitator.UserSessionInfo.LogFileDir;
			    //Excel time log init
			    storyFacilitator.excelTimeTracker = new ExcelSessionTimeTracker(ev.Parameters);
                storyFacilitator.excelTimeTracker.LogDir = storyFacilitator.UserSessionInfo.LogFileDir;
                ExcelInteractionLog.LOG_DIR = storyFacilitator.UserSessionInfo.LogFileDir;

			    string cameraName = "orient-camera";
                ApplicationLogger.Instance().WriteLine("Creating camera: " + cameraName);
                Universe.Instance.CreateEntity<Camera>(cameraName, null);

			}else if ((ev.Subject == storyFacilitator.Name) && (ev.Action == CreateCharacterEntitiesAction.ACTION_NAME) && (ev.Type == Narrative.StoryEvent.EventType.ACTION_FINISHED))
			{
                foreach (ScenarioCharacter scenarioCharacter in storyFacilitator.Scenario.Cast)
                {    
                    //Mark the RemoteCharacters as not having the mind attached
                    if (scenarioCharacter.Body != "") //only add the characters that have a body (user does not have a body)
                        storyFacilitator.CharacterManager.AddCharacter(scenarioCharacter.Name, false);                    
                }

                ArrayList storyLevelProperties = new ArrayList();
                //Set the story-level properties for all of the RemoteCharacters added			    
                foreach (ScenarioCharacter scenarioCharacter in storyFacilitator.Scenario.Cast)
                {
                   if (scenarioCharacter.Body != "") //Don't add user properties, those are added in InsertUserNarrativeAction
                    storyLevelProperties.AddRange(scenarioCharacter.Properties);
                }
			    
			    Dictionary<string, object> actionArguments = new Dictionary<string, object>();
			    actionArguments.Add(CreatePropertyListAction.PROPERTIES_LIST, storyLevelProperties);
                storyFacilitator.GetAction<CreatePropertyListAction>(CreatePropertyListAction.ACTION_NAME).Start(new Arguments(actionArguments));

            }
            else if (ev.Equals(new StoryEvent(storyFacilitator.Name, CreatePropertyListAction.ACTION_NAME, "", Narrative.StoryEvent.EventType.ACTION_FINISHED)))
            {
                Universe.Instance.CreateEntity<Narrator>(StoryFacilitator.NARRATOR_NAME);
            }
            else if (ev.Equals(new StoryEvent(storyFacilitator.Name, Narrative.StoryEvent.ENTITY_ADDED, StoryFacilitator.NARRATOR_NAME, Narrative.StoryEvent.EventType.ACTION_FINISHED)))
            {
                Dictionary<string, object> actionArguments = new Dictionary<string, object>();
                if (storyFacilitator.Scenario.Cast.Count == 0)
                {
                    throw new StoryFacilitator.StoryFacilitatorException("No cast in scenario...! IntroState");
                }
                ScenarioCharacter scenarioCharacter = (ScenarioCharacter)storyFacilitator.Scenario.Cast[0];
                string agentLanguage = "language/agent/" + scenarioCharacter.AgentLanguage + "/" +
                scenarioCharacter.AgentLanguageSet;
                string userLanguage = "language/user/" + storyFacilitator.Scenario.UserLanguage + "/" +
                storyFacilitator.Scenario.UserLanguageSet;
                actionArguments.Add(LanguageEngineMaster.AGENT_LANGUAGE, agentLanguage);
                actionArguments.Add(LanguageEngineMaster.USER_LANGUAGE, userLanguage);
                actionArguments.Add(LanguageEngineMaster.AGENT_SEX, ((storyFacilitator.Scenario.Name == "Boys") ? "M" : "F"));
                actionArguments.Add(LanguageEngineMaster.USER_SEX, User.UserData.Instance.getSex().ToString());
                Universe.Instance.CreateEntity<LanguageEngineMaster>(LanguageEngineMaster.LANGUAGE_ENGINE, new Arguments(actionArguments));
            }
            else if (ev.Equals(new StoryEvent(storyFacilitator.Name, Narrative.StoryEvent.ENTITY_ADDED, LanguageEngineMaster.LANGUAGE_ENGINE, Narrative.StoryEvent.EventType.ACTION_FINISHED)))
            {
                //UserEntity userEntity = Universe.Instance.CreateEntity<UserEntity>(UserEntity.ENTITY_NAME);
                ChangeState(storyFacilitator, SelectNextEpisodeState.Instance());
                (SelectNextEpisodeState.Instance() as SelectNextEpisodeState).NextEpisode(storyFacilitator);
            }
            /*else if (ev.Equals(new StoryEvent(storyFacilitator.Name, Narrative.StoryEvent.ENTITY_ADDED, UserEntity.ENTITY_NAME, Narrative.StoryEvent.EventType.ACTION_FINISHED)))
            {
                // USER PROPERTIES

                // display name = user name

                Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME).CreateProperty<Property<string>>(UserEntity.DISPLAY_NAME_PROPERTY_NAME);
                //Tell the property to the SF's memory. Especially important because only the properties
                //in SF's memory are updated be PropertyChanged events
                storyFacilitator.StoryMemory.Tell(new Participants.ParticipantProperty(UserEntity.ENTITY_NAME, UserEntity.DISPLAY_NAME_PROPERTY_NAME, "", true));
            }
            else if (ev.Equals(new StoryEvent(storyFacilitator.Name, Narrative.StoryEvent.PROPERTY_CREATED, UserEntity.ENTITY_NAME + " " + UserEntity.DISPLAY_NAME_PROPERTY_NAME, Narrative.StoryEvent.EventType.ACTION_FINISHED))) //The extra space separates the property name from the value, because we know it was created with no value, we put nothing next
            {
                Property<string> displayNameProperty = Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME).GetProperty<Property<string>>(UserEntity.DISPLAY_NAME_PROPERTY_NAME);
                displayNameProperty.Value = User.UserData.Instance.getUserName();

                // hasSoil = False

                Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME).CreateProperty<Property<string>>(UserEntity.HAS_SOIL_PROPERTY_NAME);
                storyFacilitator.StoryMemory.Tell(new Participants.ParticipantProperty(UserEntity.ENTITY_NAME, UserEntity.HAS_SOIL_PROPERTY_NAME, "", true));
            }
            else if (ev.Equals(new StoryEvent(storyFacilitator.Name, Narrative.StoryEvent.PROPERTY_CREATED, UserEntity.ENTITY_NAME + " " + UserEntity.HAS_SOIL_PROPERTY_NAME, Narrative.StoryEvent.EventType.ACTION_FINISHED))) //The extra space separates the property name from the value, because we know it was created with no value, we put nothing next
            {
                Property<string> hasSoilProperty = Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME).GetProperty<Property<string>>(UserEntity.HAS_SOIL_PROPERTY_NAME);
                hasSoilProperty.Value = "False";

                // isPerson = True

                Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME).CreateProperty<Property<string>>(UserEntity.IS_PERSON_PROPERTY_NAME);
                storyFacilitator.StoryMemory.Tell(new Participants.ParticipantProperty(UserEntity.ENTITY_NAME, UserEntity.IS_PERSON_PROPERTY_NAME, "", true));
            }
            else if (ev.Equals(new StoryEvent(storyFacilitator.Name, Narrative.StoryEvent.PROPERTY_CREATED, UserEntity.ENTITY_NAME + " " + UserEntity.IS_PERSON_PROPERTY_NAME, Narrative.StoryEvent.EventType.ACTION_FINISHED))) //The extra space separates the property name from the value, because we know it was created with no value, we put nothing next
            {
                Property<string> isPersonProperty = Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME).GetProperty<Property<string>>(UserEntity.IS_PERSON_PROPERTY_NAME);
                isPersonProperty.Value = "True";
            }*/
		}

        public override void StoryEvent(StoryFacilitator storyFacilitator, Narrative.Participants.ParticipantProperty p)
        {
            //Only change to SelectNextEpisodeState after:
            //Creating all RemoteCharacter entities and setting their properties and creating the user entitiy and setting its
            //all its properties (the property isPerson the last one to be created)
            //The test bellow is for the property isPerson of the user entitiy
            /*if ((p.Holder == UserEntity.ENTITY_NAME) && (p.Name == UserEntity.IS_PERSON_PROPERTY_NAME))
            {                
                ChangeState(storyFacilitator, SelectNextEpisodeState.Instance());
                (SelectNextEpisodeState.Instance() as SelectNextEpisodeState).NextEpisode(storyFacilitator);
            }*/                        
        }
	}
}
