// StopState - Represents the state of the story facilitator when the story finishes
//				(Implementation of the state design pattern)
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//
using ION.Core.Extensions;

namespace Narrative.States
{
	/// <summary>
	/// Summary description for Stop.
	/// </summary>
	public class StopState : State
	{

		private static State state = new StopState();

		private StopState()
		{
		}

		public static State Instance()
		{
			ApplicationLogger.Instance().WriteLine("Stop state selected, the story ends...");		    
			return state;
		}

		public override void UpdateEvent(Narrative.StoryFacilitator storyFacilitator, int timeSlice)
		{
		    
		}

        public override void StoryEvent(StoryFacilitator storyFacilitator, StoryEvent ev)
        {
            if (ev.Equals(new Narrative.StoryEvent(storyFacilitator.Name, RemoveCharacterEntitiesAction.ACTION_NAME, "", Narrative.StoryEvent.EventType.ACTION_FINISHED)))
            {
                //After removing all characters, remove all items
                RemoveAllItemsAndUserAction removeAllItemsAndUserAction =
                    storyFacilitator.GetAction<RemoveAllItemsAndUserAction>(RemoveAllItemsAndUserAction.ACTION_NAME);
                removeAllItemsAndUserAction.Start();
            }
            else if (ev.Equals(new Narrative.StoryEvent(storyFacilitator.Name, RemoveAllItemsAndUserAction.ACTION_NAME, "", Narrative.StoryEvent.EventType.ACTION_FINISHED)))
            {
                //Init SF and finally change the Interface to the user data form
                storyFacilitator.Init();
                System.Console.WriteLine("The Story Ends...");
            }
        }

	    

        //The end...
        public void Stop(Narrative.StoryFacilitator storyFacilitator)
	    {
            //Removes all remote characters and user entity
            RemoveCharacterEntitiesAction removeCharacterEntitiesAction =
            storyFacilitator.GetAction<RemoveCharacterEntitiesAction>(RemoveCharacterEntitiesAction.ACTION_NAME);
            removeCharacterEntitiesAction.Start();
	    }

	}
}
 