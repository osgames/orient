// State - Abstract class that represents a state (of the Story Facilitator)
//				(Implementation of the state design pattern)
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using Narrative.Participants;

namespace Narrative.States
{
	/// <summary>
	/// Summary description for State.
	/// </summary>
	public abstract class State
	{
		public virtual void StoryEvent(StoryFacilitator storyFacilitator, StoryEvent ev) {}
		
		public virtual void StoryEvent(StoryFacilitator storyFacilitator, ParticipantProperty p) {}

		public virtual void UpdateEvent(StoryFacilitator storyFacilitator, int timeSlice) {}

		protected virtual void ChangeState(StoryFacilitator storyFacilitator, State s)
		{
			storyFacilitator.ChangeState(s);	
		}
	}
}
