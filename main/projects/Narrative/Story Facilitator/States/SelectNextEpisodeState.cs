// SelectNextEpisodeState - Represents the state where the storyFacilitator selects the next episode
//							(Implementation of the state design pattern)
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Collections.Generic;
using ION.Core;
using ION.Core.Extensions;
using ION.Realizer.Orient.Entities;

namespace Narrative.States
{
	/// <summary>
	/// Summary description for SelectNextEpisodeState.
	/// </summary>
	public class SelectNextEpisodeState : State
	{
        public const string ENTITY_SET_NAME = "SceneryEntity";
	    
		private Episode.EpisodeTypeEnum episodeType;

		private static State state = new SelectNextEpisodeState();

		private SelectNextEpisodeState()
		{
			episodeType = Episode.EpisodeTypeEnum.INTRO;
		}
	    
	    /// <summary>
	    /// Sets the episode type to INTRO again
	    /// </summary>
	    public void Init()
	    {
            episodeType = Episode.EpisodeTypeEnum.INTRO;
	    }

		public static State Instance()
		{
			ApplicationLogger.Instance().WriteLine("SelectNextEpisode State selected");
			return state;
		}	   	    

		public override void UpdateEvent(StoryFacilitator storyFacilitator, int timeSlice)
		{			    
			//NextEpisode(storyFacilitator);
		}

        public override void StoryEvent(StoryFacilitator storyFacilitator, StoryEvent ev)
        {
            if (ev.Equals(new StoryEvent(storyFacilitator.Name, Narrative.StoryEvent.ENTITY_ADDED, ENTITY_SET_NAME, Narrative.StoryEvent.EventType.ACTION_FINISHED)))
            {
                (ClearStageState.Instance() as ClearStageState).InitClearStageState(storyFacilitator);
                ChangeState(storyFacilitator, ClearStageState.Instance());
                UserLogger.Instance().WriteLine("Episode selected: " + storyFacilitator.ActiveEpisode.Name);
                ComicsLogger.Instance().WriteLine("EVENT:SF:SELECT-EPISODE:" + storyFacilitator.ActiveEpisode.Name + ":" + storyFacilitator.ActiveEpisode.Scenery);
            }else if (ev.Equals(new StoryEvent(storyFacilitator.Name, Narrative.StoryEvent.ENTITY_REMOVED, ENTITY_SET_NAME, Narrative.StoryEvent.EventType.ACTION_FINISHED)))
            {
                Console.WriteLine("New episode selected: " + storyFacilitator.ActiveEpisode.Name);
                Console.WriteLine("Loading Scenario: " + storyFacilitator.ActiveEpisode.Scenery);
                
                Dictionary<string, object> args = new Dictionary<string, object>();
                // criar o cen�rio
                args.Add("type", storyFacilitator.ActiveEpisode.Scenery);
                Universe.Instance.CreateEntity<Set>(ENTITY_SET_NAME, new Arguments(args));               
            }
        }

		public void NextEpisode(StoryFacilitator storyFacilitator)
		{
            string oldEpisode = string.Empty; 
		    
			//If the ActiveEpisode has in its definition which should be the specific next episode to be selected,
			//select that episode
			try
			{
                oldEpisode = storyFacilitator.ActiveEpisode.Name;
				if (storyFacilitator.ActiveEpisode.NextEpisode != Episode.NEXT_EPISODE_NOT_DEFINED)
				{
                    storyFacilitator.UserTimeTracker.EpisodeEnd(storyFacilitator.ActiveEpisode.Name);
                    storyFacilitator.excelTimeTracker.EpisodeEnd(storyFacilitator.ActiveEpisode.Name);

					NextEpisode(storyFacilitator, storyFacilitator.ActiveEpisode.NextEpisode);                    
					return;
				}
			}catch(StoryFacilitator.StoryFacilitatorException)
			{
				//Ignore
				//This exception occurs when there is no ActiveEpisode, meaning the story is starting
			}
           
		    
		   
			try
			{               
				storyFacilitator.SelectNextEpisode(episodeType);
			}catch(StoryFacilitator.NoEpisodesException)
			{
				if (episodeType == Episode.EpisodeTypeEnum.INTRO) //No more episodes of type INTRO
				{
					episodeType = Episode.EpisodeTypeEnum.NORMAL;
					NextEpisode(storyFacilitator);
					return;
				}else if (episodeType == Episode.EpisodeTypeEnum.NORMAL) //No more episodes of type NORMAL
				{					
					episodeType = Episode.EpisodeTypeEnum.OUTRO;
					NextEpisode(storyFacilitator);
					return;
				}else //No more OUTRO episodes				    
				{
				    //And the story ends...
                    if (oldEpisode != string.Empty)
                        storyFacilitator.UserTimeTracker.EpisodeEnd(oldEpisode);
                        storyFacilitator.excelTimeTracker.EpisodeEnd(oldEpisode);
				    
				    (StopState.Instance() as StopState).Stop(storyFacilitator); //reset SF
				    ChangeState(storyFacilitator, StopState.Instance());
				    return;
				}
			}
		    if (oldEpisode != string.Empty)
		        storyFacilitator.UserTimeTracker.EpisodeEnd(oldEpisode);
                storyFacilitator.excelTimeTracker.EpisodeEnd(oldEpisode);
		    
		    storyFacilitator.UserTimeTracker.EpisodeStart(storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeType.ToString());
			
		    
		    if (Universe.Instance.HasEntity(ENTITY_SET_NAME))
		    {
		        Universe.Instance.DestroyEntity(ENTITY_SET_NAME);
                //After the confirmation of the destruction the new 
		        //scenery is loaded (see StoryEvent(StoryFacilitator storyFacilitator, StoryEvent ev))
		    }else
		    {
                Console.WriteLine("New episode selected: " + storyFacilitator.ActiveEpisode.Name);
                Console.WriteLine("Loading Scenario: " + storyFacilitator.ActiveEpisode.Scenery);
		        
                Dictionary<string, object> args = new Dictionary<string, object>();
                // criar o cen�rio
                args.Add("type", storyFacilitator.ActiveEpisode.Scenery);
                Universe.Instance.CreateEntity<Set>(ENTITY_SET_NAME, new Arguments(args));                
		    }

            ////BYPASS OGRE (if de cima tem de estar comentado)
            //(ClearStageState.Instance() as ClearStageState).InitClearStageState(storyFacilitator);
            //ChangeState(storyFacilitator, ClearStageState.Instance());	

	    
		    ApplicationLogger.Instance().WriteLine("New episode selected: " + storyFacilitator.ActiveEpisode.Name);
            ApplicationLogger.Instance().WriteLine(storyFacilitator.ActiveEpisode.ToString());
		       
		}

		
		/// <summary>
		/// Used by the NextEpisodeAction or if the episode specifies which is the next episode
		/// The reason why an episode sometimes specifies which specific episode should be played next is that
		/// sometimes we need to guarentee that then next episode is an interaction
		/// An important note in this method is that it uses the SF's method SelectEpisode that doesn't update
		/// the notSelectedEpisodes list
		/// </summary>
		/// <param name="storyFacilitator"></param>
		/// <param name="episodeName"></param>
		public void NextEpisode(StoryFacilitator storyFacilitator, string episodeName)
		{			
			Console.WriteLine("SelectNextEpisodeState: Passing directly to episode: " + episodeName);
			//Set world;
            storyFacilitator.UserTimeTracker.EpisodeEnd(storyFacilitator.ActiveEpisode.Name);
            storyFacilitator.excelTimeTracker.EpisodeEnd(storyFacilitator.ActiveEpisode.Name);           
			storyFacilitator.SelectEpisode(episodeName);
            storyFacilitator.UserTimeTracker.EpisodeStart(storyFacilitator.ActiveEpisode.Name, storyFacilitator.ActiveEpisode.EpisodeType.ToString());

            if (Universe.Instance.HasEntity(ENTITY_SET_NAME))
            {
                Universe.Instance.DestroyEntity(ENTITY_SET_NAME);
                //After the confirmation of the destruction the new 
                //scenery is loaded (see StoryEvent(StoryFacilitator storyFacilitator, StoryEvent ev))
            }
            else
            {
                Console.WriteLine("New episode selected: " + storyFacilitator.ActiveEpisode.Name);
                Console.WriteLine("Loading Scenario: " + storyFacilitator.ActiveEpisode.Scenery);

                Dictionary<string, object> args = new Dictionary<string, object>();
                // criar o cen�rio
                args.Add("type", storyFacilitator.ActiveEpisode.Scenery);
                Universe.Instance.CreateEntity<Set>(ENTITY_SET_NAME, new Arguments(args));
            }

            ////BYPASS OGRE (if de cima tem de estar comentado)
            //(ClearStageState.Instance() as ClearStageState).InitClearStageState(storyFacilitator);
            //ChangeState(storyFacilitator, ClearStageState.Instance());	
		    	    
            ApplicationLogger.Instance().WriteLine("New episode selected: " + storyFacilitator.ActiveEpisode.Name);
            ApplicationLogger.Instance().WriteLine(storyFacilitator.ActiveEpisode.ToString());		    
		}
		
	}
}