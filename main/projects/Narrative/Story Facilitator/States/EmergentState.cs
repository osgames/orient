// EmergentState - Represents the state where the characters are controled by their minds
//				   (Implementation of the state design pattern)
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Collections;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Extensions;
using Narrative.Actions;
using Action=ION.Core.Action;

namespace Narrative.States
{
	/// <summary>
	/// Summary description for EmergentState.
	/// </summary>
	public class EmergentState : State
	{
	    /// <summary>
	    /// This constant is used to count the time that the minds have to be IDLE after the
	    /// finish conditions of an episode are satisfied before the SF decides to choose another one
	    /// </summary>
	    private const int MINIMAL_IDLE_TIME = 2000; //2 sec
		
	    private static State state = new EmergentState();
		bool startMinds = true;
	    bool putMindsToSleep = false;
        /// <summary>
        /// idleTimeElapsed is used to generate the IDLE_TIME_LIMIT_PASSED event,
        /// after that event is generated the idleTimeElapsed counter is reset to zero.
        /// </summary>
        private int idleTimeElapsed; //Milliseconds
        private int totalTimeElapsed; //Milliseconds

        /// <summary>
        /// NUMBER_OF_IDLES represents the number o idle time events generated in this episode
        /// It is reset in the IntroState (calls method ResetTime)
        /// </summary>
	    public static int NUMBER_OF_IDLES = 0;
        /// <summary>
        /// NUMBER_OF_TOTAL_TIMES represents the number o total time events generated in this episode
        /// It is reset in the IntroState (calls method ResetTime)
        /// </summary>
        public static int NUMBER_OF_TOTAL_TIMES = 0;
        
	    /// <summary>
        /// The same as idleTimeElapsed, only this timer is only reset when the minds act, and not when the minds act or 
        /// when the IDLE_TIME_LIMIT_PASSED event is generated. 
        /// </summary>
        private int idleTime;

		private EmergentState()
		{
		    InitEmergentState();
		}

	    private void InitEmergentState()
	    {
            idleTimeElapsed = 0;
            totalTimeElapsed = 0;
            idleTime = 0;
	        putMindsToSleep = false;
	        startMinds = true;
	    }
	    
		public static State Instance()
		{
			ApplicationLogger.Instance().WriteLine("Emergent State selected");
			return state;
		}

		public override void UpdateEvent(StoryFacilitator storyFacilitator, int timeSlice)
		{
            totalTimeElapsed += timeSlice;
            idleTimeElapsed += timeSlice;
            idleTime += timeSlice;
   
			if (startMinds)
			{
                StartMinds();
				startMinds = false;
			}
		    
		    if (!MindsIDLE()) //If the minds are not idle, then set the timer to zero
		    {
                idleTimeElapsed = 0;
                idleTime = 0;
		    }

			if (FirableTriggers(storyFacilitator)) //Test for active triggers in which case go to TriggerState and then back to this one			
			{
				StopMinds();
				startMinds = true;
				ChangeState(storyFacilitator, TriggerState.Instance());			    
			    return;
			}
            //Test if the episode has finished and the minds have stoped all the actions they have started (MindsIDLE for at least MINIMAL_IDLE_TIME) in which case go to SelectNextEpisodeState 
		    //If we have already put the minds to sleep we ignore this and wait for the action to end
			else if ((storyFacilitator.ActiveEpisode.HasFinished(storyFacilitator.StoryMemory)) && (!putMindsToSleep) && (MindsIDLE()) && (idleTime >= MINIMAL_IDLE_TIME))
			{
			    putMindsToSleep = true;
                RemoveAllGoals();
                StopMinds();
			    ResetMinds();
			    storyFacilitator.GetAction<PutAllCharactersToSleepAction>(PutAllCharactersToSleepAction.ACTION_NAME).Start();
            }
            else if (!putMindsToSleep)
            {
                if ((storyFacilitator.ActiveEpisode.IdleTimeLimit != Episode.NO_IDLE_TIME_LIMIT) &&
                    ((idleTimeElapsed / 1000) >= storyFacilitator.ActiveEpisode.IdleTimeLimit))
                {
                    storyFacilitator.TellStoryEvent(
                        new StoryEvent(storyFacilitator.Name,
                                       Narrative.StoryEvent.IDLE_TIME_LIMIT_REACHED,
                                       storyFacilitator.ActiveEpisode.Name + " " + ++NUMBER_OF_IDLES,
                                       Narrative.StoryEvent.EventType.ACTION_FINISHED));
                    idleTimeElapsed = 0;
                    ApplicationLogger.Instance().WriteLine("IDLE_TIME_LIMIT_PASSED for episode " + storyFacilitator.ActiveEpisode.Name);
                }

                if ((storyFacilitator.ActiveEpisode.TimeLimit != Episode.NO_TIME_LIMIT) &&
                    ((totalTimeElapsed / 1000) >= storyFacilitator.ActiveEpisode.TimeLimit))
                {
                    storyFacilitator.TellStoryEvent(
                        new StoryEvent(storyFacilitator.Name,
                                       Narrative.StoryEvent.TIME_LIMIT_REACHED,
                                       storyFacilitator.ActiveEpisode.Name + " " + ++NUMBER_OF_TOTAL_TIMES,
                                       Narrative.StoryEvent.EventType.ACTION_FINISHED));
                    totalTimeElapsed = 0;
                    ApplicationLogger.Instance().WriteLine("TIME_LIMIT_PASSED for episode " + storyFacilitator.ActiveEpisode.Name);
                }
            }
		}

	    private void RemoveAllGoals()
	    {
            foreach (Entity entity in Universe.Instance.Entities)
            {
                if (entity is RemoteCharacter)
                {
                    RemoteCharacter remoteCharacter = entity as RemoteCharacter;
                    if (remoteCharacter.IsActive)
                    {
                        remoteCharacter.Send(RemoteCharacter.REMOVE_ALL_GOALS_MESSAGE);
                    }
                }
            }
	    }

	    public override void StoryEvent(StoryFacilitator storyFacilitator, StoryEvent ev)
        {            

	        if (ev.Equals(new Narrative.StoryEvent(storyFacilitator.Name, PutAllCharactersToSleepAction.ACTION_NAME, "", Narrative.StoryEvent.EventType.ACTION_FINISHED))) //Episode ended, select next
	        {
                //SaveMindsState();	                
                ChangeState(storyFacilitator, SelectNextEpisodeState.Instance());
	            (SelectNextEpisodeState.Instance() as SelectNextEpisodeState).NextEpisode(storyFacilitator);
                InitEmergentState();	            	            	                
	        }
        }

        public override void StoryEvent(StoryFacilitator storyFacilitator, Narrative.Participants.ParticipantProperty p)
        {            
        }
		
		/// <summary>
		/// Returns true when a there is a trigger that can be fired in this episode
		/// </summary>
		/// <param name="storyFacilitator"></param>
		/// <returns></returns>
		private bool FirableTriggers(StoryFacilitator storyFacilitator)
		{
			ArrayList triggers = storyFacilitator.ActiveEpisode.Triggers;

			foreach (Trigger trigger in triggers)
			{
				if (trigger.IsSatisfied(storyFacilitator.StoryMemory))
				{
					return true;
				}
			}

			return false;			
		}

		private void StopMinds()
		{
            ApplicationLogger.Instance().WriteLine("SF orders all the agents to STOP");
            foreach (Entity entity in Universe.Instance.Entities)
            {
                if (entity is RemoteCharacter)
                {
                    RemoteCharacter remoteCharacter = entity as RemoteCharacter;
                    if (remoteCharacter.IsActive)
                    {
                        remoteCharacter.Send(RemoteCharacter.STOP_MESSAGE);
                    }
                }
            }
        }

        private void StartMinds()
		{
            ApplicationLogger.Instance().WriteLine("SF orders all the agents to START");
            foreach (Entity entity in Universe.Instance.Entities)
            {
                if (entity is RemoteCharacter)
                {
                    RemoteCharacter remoteCharacter = entity as RemoteCharacter;
                    if (remoteCharacter.IsActive)
                    {
                        remoteCharacter.Send(RemoteCharacter.START_MESSAGE);
                    }
                }
            }
        }

        private void SaveMindsState()
        {
            ApplicationLogger.Instance().WriteLine("SF orders all the agents to SAVE");
            foreach (Entity entity in Universe.Instance.Entities)
            {
                if (entity is RemoteCharacter)
                {                    
                    RemoteCharacter remoteCharacter = entity as RemoteCharacter;
                    if (remoteCharacter.ReceiverAlive) //Only sends message to Online Agents
                        remoteCharacter.Send(RemoteCharacter.SAVE_MESSAGE);
                }
            }
        }	    
	    
	    private void ResetMinds()
	    {
            ApplicationLogger.Instance().WriteLine("SF orders all the agents to RESET");
            foreach (Entity entity in Universe.Instance.Entities)
            {
                if (entity is RemoteCharacter)
                {
                    RemoteCharacter remoteCharacter = entity as RemoteCharacter;
                    if (remoteCharacter.IsActive)
                    {
                        remoteCharacter.Send(RemoteCharacter.RESET_MESSAGE);
                    }
                }
            }	        
	    }

        #region Filters
	    
	    //returns only RemoteCharacterEntities
	    private class EntityFilter : IFilter<Entity>
	    {
	        public bool Blocks(Entity obj)
	        {
                return (!(obj is RemoteCharacter));
	        }
	    }
	    
        #endregion

        private bool MindsIDLE()
	    {
	        foreach (Entity entity in Universe.Instance.Entities.FilteredBy(new EntityFilter()))
	        {
                RemoteCharacter rmChar = (RemoteCharacter) entity;
	            foreach (Action action in rmChar.Actions)
	            {
	                if (action.IsRunning)
	                    return false;
	            }
	        }
            return true;
	    }

        public void ResetTime()
        {
            NUMBER_OF_IDLES = 0;
            NUMBER_OF_TOTAL_TIMES = 0;
        }

	}
}
