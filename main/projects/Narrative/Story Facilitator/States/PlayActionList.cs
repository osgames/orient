// PlayActionList - Executes a list of narrative actions
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//
using System.Collections;
using Narrative.Actions;

namespace Narrative.States
{
	/// <summary>
	/// 
	/// </summary>
	public class PlayActionList
	{		
		private ArrayList actionList = new ArrayList();		
		private Action activeAction = null;
		bool finished = false;

		public PlayActionList(ArrayList actionList)
		{
			this.actionList = (ArrayList) actionList.Clone();
		}

		public void Update(StoryFacilitator storyFacilitator, StoryEvent ev)
		{
			if ((activeAction == null) || (activeAction.IsFinished()))
			{
				if (actionList.Count != 0)
				{
					updateActiveAction(actionList);
					activeAction.Start(storyFacilitator);
				}else
				{
					StopPlayingActionList();
				}
			}else
			{
				activeAction.Update(storyFacilitator, ev);
			}
		}

		public void Update(StoryFacilitator storyFacilitator, int timeSlice)
		{
			if ((activeAction == null) || (activeAction.IsFinished()))
			{
				if (actionList.Count != 0)
				{
					updateActiveAction(actionList);
					activeAction.Start(storyFacilitator);
				}
				else
				{
					StopPlayingActionList();
				}
			}
			else
			{
				activeAction.Update(storyFacilitator, timeSlice);
			}			
		}


		public void StopPlayingActionList()
		{						
			finished = true;			
		}

		/// <summary>
		/// Sets the field activeAction to the next action on the list, and updates the actionList
		/// by removing the selected action
		/// </summary>
		/// <param name="list"></param>
		private void updateActiveAction(ArrayList list)
		{
			activeAction = ((Action)list[0]).Clone();

			list.RemoveAt(0); //Removes the selected action
		}

		public bool IsFinised()
		{
			return finished;
		}

	}
}
