// QueryMindsState - Represents a state where the Story Facilitator queries the minds to select the most "dramatic" trigger
//to execute (double appraisal)
//
// Copyright (C) 2008 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 15/03/2008
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//
// NOTES:
//For each trigger send a query with all the triggers' actions to all active agents to know how they'll react to the actions, update arousal table with answerd queries
//
//Test if arousal table has all the queries with answers
//Yes:
//  Determine the highest generating (arousal/mood/??) trigger
//  TriggerState.ActiveTrigger = trigger that generates more arousal
//  Story Memory update for all the triggers that have the same preconditions that the trigger that was selected (including the selected trigger)
//  Change to TriggerState
//No:
//  Taking too much time?
//  Yes:
//    Select the highest priority trigger
//    Story Memory update for all the triggers that have the same preconditions that the trigger that was selected (including the selected trigger)
//    Change to trigger state
//  No:
//    Wait some more

using System;
using System.Collections.Generic;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Extensions;
using Narrative.Actions;
using Action=Narrative.Actions.Action;

namespace Narrative.States
{
    class QueryMindsState : State
    {
        private const int TIME_OUT = 3000; //3 secs

        #region ArousalTable
        
        class ArousalTable
        {
            #region Query  Class
            class Query
            {
                public const int NOT_ANSWERED_YET = -1;
                private string agentID = "";
                private string actionRepresentation = "";                
                private int result = NOT_ANSWERED_YET;

                public Query(string agentID, string actionRepresentation) 
                {
                    AgentID = agentID;
                    ActionRepresentation = actionRepresentation;
                    result = NOT_ANSWERED_YET;
                }

                public string AgentID
                {
                    get { return agentID; }
                    set { agentID = value; }
                }

                public string ActionRepresentation
                {
                    get { return actionRepresentation; }
                    set { actionRepresentation = value; }
                }

                public int Result
                {
                    get { return result; }
                    set { result = value; }
                }
            }
            #endregion
            //(TriggerID, Query List)
            readonly Dictionary<int, List<Query>> queries = new Dictionary<int, List<Query>>();
            private bool queriesClosed = false;

            public ArousalTable()
            {
                queries = new Dictionary<int, List<Query>>();
            }

            public void AddQuery(string agentID, string actionRepresentation, int triggerID)
            {
                ApplicationLogger.Instance().WriteLine("QUERY: " + agentID + " " + actionRepresentation + " " + triggerID);
                Query query = new Query(agentID, actionRepresentation);
                if (queries.ContainsKey(triggerID))
                {
                    List<Query> triggerQueries = queries[triggerID];
                    triggerQueries.Add(query);
                }else
                {
                    List<Query> triggerQueries = new List<Query>();
                    triggerQueries.Add(query);
                    queries.Add(triggerID, triggerQueries);
                }
            }


            public void AddResponse(string agentID, string actionRepresentation, int triggerID, int response)
            {
                ApplicationLogger.Instance().WriteLine("QUERY_ANSWER: " + agentID + " " + actionRepresentation + " " + triggerID + "->" + response);
                List<Query> triggerQueries = queries[triggerID];
                for(int i = 0; i < triggerQueries.Count; i++)
                {
                    Query query = triggerQueries[i];
                    
                    //the last test in the "if" is for the cases where there are two actions that are equal in the trigger
                    //If the test was not there only the first one would be assigned a result
                    if ((query.AgentID == agentID) && (query.ActionRepresentation == actionRepresentation) && (query.Result == Query.NOT_ANSWERED_YET))
                    {
                        query.Result = response;
                        return;
                    }
                }
                throw new QueryMindsException("ArousalTable::AddResponse: (AGENTID, ACTION_REPRESENTATION, TRIGGERID) (" + agentID + "," + actionRepresentation + "," + triggerID + ") not found");
            }
            
            /**
             * Inidicates that no more queires should be added, only answers to the queries posed
             */
            public void SetQueriesClosed(bool close)
            {
                queriesClosed = close;
            }

            public bool AllQueriesAnswered()
            {
                if (!queriesClosed)
                {                    
                    return false;
                }else
                {                                        
                    foreach(List<Query> triggerQueries in queries.Values)
                    {
                        foreach (Query query in triggerQueries)
                        {
                            if (query.Result == Query.NOT_ANSWERED_YET)
                            {
                                return false;
                            }
                        }
                    }
                    return true;
                }
            }

            public int GetMaxArousalTrigger()
            {
                if (queries.Keys.Count == 0)
                {
                    throw new QueryMindsException("GetMaxArousalTrigger::No triggers in ArousalTable");
                }

                int maxArousalTriggerID = -1;
                int maxArousal = -1;
                foreach (KeyValuePair<int, List<Query>> pair in queries)
                {                    
                    if (maxArousalTriggerID == -1)
                    {
                        maxArousalTriggerID = pair.Key;
                        maxArousal = GetTriggerArousalValue(pair.Value);
                        continue;             
                    }else
                    {
                        int triggerGeneratedArousal = GetTriggerArousalValue(pair.Value);
                        if (triggerGeneratedArousal > maxArousal)
                        {
                            maxArousalTriggerID = pair.Key;
                            maxArousal = triggerGeneratedArousal;
                        }
                    }                    
                }
                return maxArousalTriggerID;
            }

            private static int GetTriggerArousalValue(IEnumerable<Query> triggerQueries)
            {
                int result = 0;
                foreach (Query query in triggerQueries)
                {
                    if (query.Result == Query.NOT_ANSWERED_YET)
                    {
                        throw new QueryMindsException("GetTriggerArousalValue: Query (AGENTID, ACTION_REPRESENTATION): (" +
                                                      query.AgentID + ", " + query.ActionRepresentation + ") not defined."); 
                    }

                    result += query.Result;
                }
                return result;
            }
        }


        #endregion

        #region Exceptions
        public class QueryMindsException : Exception
        {
            public QueryMindsException()
            {
            }
            public QueryMindsException(string errorMessage)
                : base(errorMessage)
            {
            }
            public QueryMindsException(string errorMessage, Exception innerException)
                : base(errorMessage, innerException)
            {
            }
        }
        #endregion

        private static readonly State state = new QueryMindsState();
        public List<Narrative.Actions.Trigger> triggerList = null;
        private static ArousalTable arousalTable = null;
        private int timeElapsed = 0;

        public static State Instance()
        {
            ApplicationLogger.Instance().WriteLine("QueryMinds stage state Instance() method called...");
            return state;
        }

        private QueryMindsState(){}

        
        public void Init(List<Narrative.Actions.Trigger> triggers)
        {
            triggerList = triggers;
            arousalTable = null;
            timeElapsed = 0;
            ApplicationLogger.Instance().WriteLine("QueryMindsState: Init trigger list");
            ApplicationLogger.Instance().WriteLine("QueryMindsState: Init Arousal Table");
        }
        
        public void Start()
        {            
            ApplicationLogger.Instance().WriteLine(
                "QueryMindsState::Start(): Send query to all active agents...");

            arousalTable = new ArousalTable();
            ApplicationLogger.Instance().WriteLine("SF Queries all active agents...");
            foreach (Entity entity in Universe.Instance.Entities)
            {
                if (entity is RemoteCharacter)
                {
                    RemoteCharacter remoteCharacter = entity as RemoteCharacter;
                    if (remoteCharacter.IsActive)
                    {
                        foreach (Trigger trigger in triggerList)
                        {
                            if (trigger.ExpectedOutcome != Trigger.UNSPECIFIED_OUTCOME){

                                foreach (Action action in trigger.Actions)
                                {
                                    if (action.GetActionRepresentation() != Action.DOUBLE_APPRAISAL_IGNORE_ACTION)
                                    {
                                        arousalTable.AddQuery(remoteCharacter.Name, action.GetActionRepresentation(),
                                                              trigger.TriggerID);
                                        //Format: CMD DA_QUERY [TRIGGER_ID] [ACTION_REPRESENTION]
                                        ApplicationLogger.Instance().WriteLine("SENDING QUERY TO AGENT " +
                                                                               remoteCharacter.Name + ": " +
                                                                               "CMD DA_QUERY " +
                                                                               trigger.TriggerID + " " +
                                                                               action.GetActionRepresentation());
                                        remoteCharacter.Send("CMD DA_QUERY " + trigger.TriggerID + " " +
                                                             action.GetActionRepresentation());
                                        //TEST: Simulate agents answers
                                        //arousalTable.AddResponse(remoteCharacter.Name, action.GetActionRepresentation(),
                                        //                       trigger.TriggerID, 1);
                                    }
                                    else
                                    {
                                        ApplicationLogger.Instance().WriteLine("Narrative Action being ignored: " +
                                                                               action.GetType());
                                        arousalTable.AddQuery(remoteCharacter.Name, action.GetActionRepresentation(),
                                                              trigger.TriggerID);
                                        //When the action is to be ignored it is automatically given an answer of 0
                                        ApplicationLogger.Instance().WriteLine("Narrative Action being ignored (" +
                                                                               action.GetType() + ") answer is 0");
                                        arousalTable.AddResponse(remoteCharacter.Name, action.GetActionRepresentation(),
                                                                 trigger.TriggerID, 0);
                                    }
                                }

                            }
                            else //Trigger has a author specified expected outcome
                            {
                                arousalTable.AddQuery(remoteCharacter.Name, trigger.ExpectedOutcome,
                                                      trigger.TriggerID);
                                //Format: CMD EXPECTED_OUTCOME_QUERY [TRIGGER_ID] [ACTION_REPRESENTION]
                                ApplicationLogger.Instance().WriteLine("SENDING QUERY TO AGENT " +
                                                                       remoteCharacter.Name + ": " +
                                                                       "CMD EXPECTED_OUTCOME_QUERY " +
                                                                       trigger.TriggerID + " " +
                                                                       trigger.ExpectedOutcome);
                                remoteCharacter.Send("CMD EXPECTED_OUTCOME_QUERY " + trigger.TriggerID + " " +
                                                     trigger.ExpectedOutcome);                                  
                            }
                        }
                    }
                }
            }

            ApplicationLogger.Instance().WriteLine(
                "QueryMindsState::Start(): All queries sent, waiting for answers...");
            arousalTable.SetQueriesClosed(true);
        }       


        public override void StoryEvent(StoryFacilitator storyFacilitator, StoryEvent ev)
        {
            //Verify if an event represents a response to a query
            //If so, execute the AddResponse to update the arousal table
            if ((ev.Action == DAQueryResponseAction.ACTION_NAME) && (ev.Type == Narrative.StoryEvent.EventType.ACTION_FINISHED))
            {
                string agentID = ev.Subject;
                string eventParameters = ev.Parameters;
                string actionRepresentation = eventParameters.Split(':')[0];
                string triggerID = eventParameters.Split(':')[1];
                string appraisalValue = eventParameters.Split(':')[2];
                arousalTable.AddResponse(agentID, actionRepresentation,
                                         Int32.Parse(triggerID), Int32.Parse(appraisalValue));
            }

        }

        public override void UpdateEvent(StoryFacilitator storyFacilitator, int timeSlice)
        {
            timeElapsed += timeSlice;

            if (arousalTable.AllQueriesAnswered())
            {
                ApplicationLogger.Instance().WriteLine(
                  "QueryMindsState::Start(): All queries answered.");

                int triggerID = arousalTable.GetMaxArousalTrigger();
                ApplicationLogger.Instance().WriteLine("Double Appraisal selected trigger ID: " + triggerID);
                foreach (Trigger trigger in triggerList)
                {
                    if (trigger.TriggerID == triggerID)
                    {
                        ApplicationLogger.Instance().WriteLine("Selected Trigger:\n" + trigger);
                        ((TriggerState)TriggerState.Instance()).SetActiveTrigger(trigger);

                        //Update knowledge base(Events (EventInfo) used by this trigger are updated so
                        //this trigger doesn't use these events again to fire (if the events were used again
                        //the trigger would fire indefinitly)
                        trigger.UpdateStoryMemory(storyFacilitator.StoryMemory);
                        //Update the triggers that have the same conditions that this one:
                        //Used for the cases where you have triggers like "if A then do B";"if A then do C"
                        //and after the right trigger is selected based on the agents' responses, you don't
                        //want the other ones to be used (or else you wouldn't be able to have triggers with
                        //the same condition and different actions (in those cases double appraisal would only decide
                        //which trigger would fire first [all the others would also fire].
                        TellTriggerFired(trigger, storyFacilitator.StoryMemory);
                        ChangeState(storyFacilitator, TriggerState.Instance());
                    }
                }
            }
            else if (timeElapsed >= TIME_OUT) //Timed out, select the highest priority trigger and update the story memory the same way as above
            {
                ApplicationLogger.Instance().WriteLine("QueryMindsState: Timed out, selecting highest priority trigger...\n");
                Trigger trigger = ((TriggerState)TriggerState.Instance()).SelectHighestPriorityTrigger(
                    TriggerState.SelectableTriggers(storyFacilitator));
                ((TriggerState)TriggerState.Instance()).SetActiveTrigger(trigger);
                ApplicationLogger.Instance().WriteLine("QueryMindsState: Highest priority trigger: \n" + trigger);
                trigger.UpdateStoryMemory(storyFacilitator.StoryMemory);
                TellTriggerFired(trigger, storyFacilitator.StoryMemory);
                ChangeState(storyFacilitator, TriggerState.Instance());
            }
        }

        private void TellTriggerFired(Trigger selectedTrigger, StoryMemory storyMemory)
        {
            if (triggerList == null)
                throw new QueryMindsException("QueryMindsState::TellTriggersFired: triggerList not initialized properly");
            
            foreach (Trigger trigger in triggerList)
            {
                if (trigger.TriggerID == selectedTrigger.TriggerID) //don't update for the selected trigger, already updated
                {
                    continue;
                }else if(trigger.ConditionList.Equals(selectedTrigger.ConditionList))
                {
                    trigger.UpdateStoryMemory(storyMemory);
                }
            }
        }

    }
}
