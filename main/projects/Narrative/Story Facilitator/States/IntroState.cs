// IntroState - Represents the state where the storyFacilitator performs the narrative actions for the intro
//				(Implementation of the state design pattern)
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System.Collections;
using ION.Core.Extensions;

namespace Narrative.States
{
	/// <summary>
	/// 
	/// </summary>
	public class IntroState : State
	{

		private static State state = new IntroState();
		private PlayActionList playActionList = null;

		private IntroState()
		{
		}

		public static State Instance()
		{
			ApplicationLogger.Instance().WriteLine("Intro state selected");
			return state;
		}

		public override void StoryEvent(StoryFacilitator storyFacilitator, StoryEvent ev)
		{
			if (playActionList == null)
			{
				playActionList = new PlayActionList((ArrayList)storyFacilitator.ActiveEpisode.Intro.Clone());
			}

			if (!playActionList.IsFinised())
			{
				playActionList.Update(storyFacilitator, ev);
			}
			else
			{
				InitIntro();
			    ((EmergentState)EmergentState.Instance()).ResetTime();
				ChangeState(storyFacilitator, EmergentState.Instance());
			}
		}


		public override void UpdateEvent(Narrative.StoryFacilitator storyFacilitator, int timeSlice)
		{
			if (playActionList == null)
			{
				playActionList = new PlayActionList((ArrayList)storyFacilitator.ActiveEpisode.Intro.Clone());
			}

			if (!playActionList.IsFinised())
			{
				playActionList.Update(storyFacilitator, timeSlice);
			}
			else
			{
				InitIntro();
                ((EmergentState)EmergentState.Instance()).ResetTime();
				ChangeState(storyFacilitator, EmergentState.Instance());
			}
		}

		//Used by the NextEpisodeAction [which calls the method Nextepisode(StoruManager,String episodeName)]
		//so when we change directly to another episode this state is initialized
		public void InitIntro()
		{
			playActionList = null;
		}


	}
}