
using System;
using System.Collections.Generic;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Extensions;
using ION.Realizer.Orient.Entities;


namespace Narrative.States{
    
    public class ClearStageState : State
    {

        #region Exceptions
        public class ClearStageException :Exception
        {
			public ClearStageException()
			{
			}
			public ClearStageException(string errorMessage) : base(errorMessage)
			{
			}
            public ClearStageException(string errorMessage, Exception innerException)
                : base(errorMessage, innerException)
			{
			}               
        }
        #endregion
        
        private static State state = new ClearStageState();        
        

		private ClearStageState()
		{		    
		}

		public static State Instance()
		{
			ApplicationLogger.Instance().WriteLine("Clear stage state selected");
			return state;
		}


        //Check if all properties are removed and upon confirmation of each episode-level property remove it from the SF's memory
        public override void StoryEvent(StoryFacilitator storyFacilitator, StoryEvent ev)
        {
            if (ev.Equals(new StoryEvent(storyFacilitator.Name, RemoveAllCharacterAndUserPropertiesAction.ACTION_NAME, "", Narrative.StoryEvent.EventType.ACTION_FINISHED)))
            {
                //Now that all episode-level character properties are removed
                //Remove all episode-level properties of the objects/items from SF's memory
                foreach (Entity entity in Universe.Instance.Entities)
                {
                    if (entity is Item)
                    {
                        //Removes all episode-level properties of the entities from SF's memory
                        //(The character's properties are removed from the SF's memory in RemoveAllCharacterAndUserPropertiesAction)
                        storyFacilitator.StoryMemory.RetractEpisodeLevelProperties(entity.Name);                         
                    }
                }                
                storyFacilitator.GetAction<RemoveAllItemsAndUserAction>(RemoveAllItemsAndUserAction.ACTION_NAME).Start();
                
            }else if(ev.Equals(new Narrative.StoryEvent(storyFacilitator.Name, RemoveAllItemsAndUserAction.ACTION_NAME, "", Narrative.StoryEvent.EventType.ACTION_FINISHED)))
            {
                (IntroState.Instance() as IntroState).InitIntro(); //Reset IntroState
                ChangeState(storyFacilitator, IntroState.Instance());                                
            }
        }

        public void InitClearStageState(StoryFacilitator storyFacilitator)
        {
            //Removes all of the entities' properties and retracts the episode-level properties from the sf's memory
            RemoveAllCharacterAndUserPropertiesAction removeAllCharacterPropertesAction = 
                storyFacilitator.GetAction<RemoveAllCharacterAndUserPropertiesAction>(RemoveAllCharacterAndUserPropertiesAction.ACTION_NAME);
            Dictionary<string, object> actionArguments = new Dictionary<string, object>();
            actionArguments.Add(RemoveAllCharacterAndUserPropertiesAction.STORY_FACILITATOR, storyFacilitator);
            removeAllCharacterPropertesAction.Start(new Arguments(actionArguments));            
        }
        
    }
}
