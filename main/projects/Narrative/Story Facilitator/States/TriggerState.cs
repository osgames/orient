// TriggerState - Represents the state of the story facilitator when a trigger is being executed
//				(Implementation of the state design pattern)
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Collections;
using System.Collections.Generic;
using ION.Core.Extensions;
using Narrative.Actions;

namespace Narrative.States
{
	/// <summary>
	/// Summary description for TriggerState.
	/// </summary>
	public class TriggerState : State
	{
        #region Exceptions
        public class TriggerStateException : Exception
        {
            public TriggerStateException()
            {
            }
            public TriggerStateException(string errorMessage)
                : base(errorMessage)
            {
            }
            public TriggerStateException(string errorMessage, Exception innerException)
                : base(errorMessage, innerException)
            {
            }
        }
        #endregion

		private static State state = new TriggerState();
		private Trigger activeTrigger = null;
		private PlayActionList playActionList = null;

		private TriggerState()
		{
		}

		public static State Instance()
		{
			ApplicationLogger.Instance().WriteLine("Trigger State selected");
			return state;
		}

		public override void StoryEvent(StoryFacilitator storyFacilitator, StoryEvent ev)
		{
            if ((playActionList != null)&&(!playActionList.IsFinised()))
            {
                playActionList.Update(storyFacilitator, ev);
            }           			
		}


		public override void UpdateEvent(StoryFacilitator storyFacilitator, int timeSlice)
		{
            if (activeTrigger != null)
            {
                if (playActionList == null)
                    playActionList = new PlayActionList((ArrayList)activeTrigger.Actions.Clone());

                if (!playActionList.IsFinised())
                {
                    playActionList.Update(storyFacilitator, timeSlice);
                }
                else
                {
                    playActionList = null;
                    activeTrigger = null;
                    ChangeState(storyFacilitator, EmergentState.Instance());
                }
            }
            else //Check if more than one trigger can be selected, if so Init QueryMindsState and change to it (see QueryMindsState)
            {
                if (SelectableTriggers(storyFacilitator).Count > 1)
                {
                    ApplicationLogger.Instance().WriteLine(
                        "TriggerState: Detected that more than one trigger can be selected");                   
                    List<Trigger> selectableTriggers = new List<Trigger>();
                    foreach (Trigger trigger in SelectableTriggers(storyFacilitator))
                    {
                        selectableTriggers.Add(trigger);
                    }
                    ((QueryMindsState) QueryMindsState.Instance()).Init(selectableTriggers);
                    ((QueryMindsState) QueryMindsState.Instance()).Start();
                    ChangeState(storyFacilitator, QueryMindsState.Instance());
                }
                else
                {
                    activeTrigger = SelectHighestPriorityTrigger(SelectableTriggers(storyFacilitator));
                    ApplicationLogger.Instance().WriteLine(
                        "TriggerState: Only one trigger can be selected: \n"  + activeTrigger);
                    //Update knowledge base(Events (EventInfo) used by this trigger are updated so
                    //this trigger doesn't use these events again to fire (if the events were used again
                    //the trigger would fire indefinitly)
                    activeTrigger.UpdateStoryMemory(storyFacilitator.StoryMemory);
                }
            }
		}

		public static ArrayList SelectableTriggers(StoryFacilitator storyFacilitator)
		{
			ArrayList triggers = storyFacilitator.ActiveEpisode.Triggers;
			ArrayList selectableTriggers = new ArrayList();

			foreach (Trigger trigger in triggers)
			{
				if (trigger.IsSatisfied(storyFacilitator.StoryMemory))
				{
					selectableTriggers.Add(trigger);
				}
			}

			return selectableTriggers;
		}

               
		public Trigger SelectHighestPriorityTrigger(ArrayList triggers)
		{
            int maxPriority = 0;
            Trigger candidate = null;
            foreach (Trigger trigger in triggers)
            {
                if ((candidate == null) || (maxPriority <= trigger.Priority))
                {
                    candidate = trigger;
                    maxPriority = trigger.Priority;
                }
            }
			return candidate;
		}

        public void SetActiveTrigger(Trigger trigger)
        {
            if (activeTrigger != null)
                throw new TriggerStateException(
                    "TriggerState::SetActiveTrigger: Attempt to set the ActiveTrigger while ActiveTrigger was already set.");
            activeTrigger = trigger;
        }

	}
}
