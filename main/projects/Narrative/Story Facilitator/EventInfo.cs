// EventInfo - Class that stores an event as well as contextual information about that event
//			   such as in what episode was it generated or was it used to fire which trigger
// 
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Collections;

namespace Narrative
{
	/// <summary>
	/// Class that stores an event as well as contextual information about that event
	/// like in what episode was it generated or was it used to fire which trigger
	/// </summary>
	[Serializable()]
	public class EventInfo
	{
		#region Fields

		private string episodeName;
		private ArrayList firedTriggersIDs;
		private StoryEvent ev;
		private int order;
	    private int episodeID;

		#endregion

		#region Properties

		public ArrayList FiredTriggersIDs
		{
			get { return firedTriggersIDs; }
			set { firedTriggersIDs = value; }
		}

		public string EpisodeName
		{
			get { return episodeName; }
			set { episodeName = value; }
		}

		public StoryEvent StoryEvent
		{
			get { return ev; }
			set { ev = value; }
		}

		public int Order
		{
			get { return order; }
			set { order = value; }
		}

	    public int EpisodeID
	    {
	        get { return episodeID; }
	        set { episodeID = value; }
	    }

	    #endregion

		public EventInfo(StoryEvent ev, string episodeName, int episodeID, int order)
		{
			this.StoryEvent = ev;
			this.EpisodeName = episodeName;
			firedTriggersIDs = new ArrayList();
			Order = order;
            EpisodeID = episodeID; 
		}
	    
	    public EventInfo(EventInfo eventInfo)
	    {
            this.episodeName = eventInfo.episodeName;
	        this.firedTriggersIDs = new ArrayList();
	        foreach (int triggerID in eventInfo.firedTriggersIDs)
	        {
                this.firedTriggersIDs.Add(triggerID);
	        }
	        this.ev = new StoryEvent(eventInfo.StoryEvent.Subject, eventInfo.StoryEvent.Action, eventInfo.StoryEvent.Parameters, eventInfo.StoryEvent.Type);
            this.order = eventInfo.order;
            this.episodeID = eventInfo.episodeID;
	    }
	    
	    public EventInfo Clone()
	    {
	        return new EventInfo(this);
	    }
	}
}
