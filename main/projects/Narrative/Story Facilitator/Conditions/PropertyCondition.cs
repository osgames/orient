// PropertyCondition - Represents a condition over a property
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Narrative.Conditions
{
	/// <summary>
	/// Summary description for PropertyCondition.
	/// </summary>
	[Serializable()]
	public sealed class PropertyCondition : Condition
	{
		#region Exceptions

		public class ParticipantNotFoundException : Exception
		{
			public ParticipantNotFoundException()
			{
			}
			public ParticipantNotFoundException(string errorMessage) : base(errorMessage)
			{
			}
			public ParticipantNotFoundException(string errorMessage, Exception innerException) :base(errorMessage, innerException)
			{
			}
		}

		public class OperatorNotImplementedException : Exception
		{
			public OperatorNotImplementedException()
			{
			}
			public OperatorNotImplementedException(string errorMessage) : base(errorMessage)
			{
			}
			public OperatorNotImplementedException(string errorMessage, Exception innerException) :base(errorMessage, innerException)
			{
			}
		}


		#endregion

		#region Fields

		private string participantName;
		private string propertyName;
		private string val;
		private Operators oper;
		private int triggerID;

		#endregion

		#region Properties
		
		public string ParticipantName
		{
			get
			{
				return participantName;
			}
			set
			{
				participantName = value;
			}
		}

		public string PropertyName
		{
			get
			{
				return propertyName;
			}
			set
			{
				propertyName = value;
			}
		}

		public string Value
		{
			get
			{
				return val;
			}
			set
			{
				val = value;
			}
		}

		public Operators Operator
		{
			get
			{
				return oper;
			}
			set
			{
				oper = value;
			}
		}

		public int TriggerID
		{
			get { return triggerID; }
			set { triggerID = value; }
		}

		#endregion

		public enum Operators{Equal, NotEqual, LesserThan, GreaterThan};

		public PropertyCondition()
		{
			triggerID = -1;
		}
	    
	    public PropertyCondition(PropertyCondition propertyCondition)
	    {
            this.participantName = propertyCondition.participantName;
            this.propertyName = propertyCondition.propertyName;
            this.val = propertyCondition.val;
            this.oper = propertyCondition.oper;
            this.triggerID = propertyCondition.triggerID;
	    }

		public override string ToString()
		{
			return "PROPERTY_CONDITION:" + ParticipantName + ":" + PropertyName + ":" + Value + ":" + Operator + "\n";
		}

        public override Condition Clone()
        {
            return new PropertyCondition(this);
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || (!(this.GetType() == obj.GetType())))
                return false;

            PropertyCondition p = (PropertyCondition)obj;

            return
                propertyName == p.propertyName && participantName == p.participantName && val == p.val && oper == p.oper;
        }

        public override int GetHashCode()
        {
            return (propertyName + participantName + val + oper).GetHashCode();
        }

        public override List<string> GetVariableList()
        {
            List<string> variables = new List<string>();
            foreach (Match match in Substitution.VARIABLE_REGEX.Matches(ParticipantName))
            {
                variables.Add(match.Value);
            }
            return variables;
        }

        public override void ApplySubstitution(Substitution substitution)
        {
            this.ParticipantName = substitution.Apply(ParticipantName);            
        }
	}
}
