// PropertyCondition - Represents a trigger's condition list
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//
using System;
using System.Collections;

namespace Narrative.Conditions
{
	/// <summary>
	/// Summary description for TriggerConditionList.
	/// </summary>
    [Serializable()]
	public class TriggerConditionList : ConditionList
	{
		public TriggerConditionList() : base()
		{						
		}
	    
	    public TriggerConditionList(TriggerConditionList triggerCL)
	    {
	        this.conditionList = new ArrayList();
	        foreach (Condition condition in triggerCL.conditionList)
	        {
                this.conditionList.Add(condition.Clone());
	        }
	    }

		/// <summary>
		/// When a trigger fires, the information in the Story Memory has to be updated
		/// the events and properties that were used to satisfy the conditions of the
		/// trigger must be marked with the triggerID so that they are not used again by 
		/// the trigger [if they could be used more than once the trigger would never stop firing]
		/// </summary>
		/// <param name="storyMemory"></param>
		public void UpdateConditionsInKnwoledgeBase(StoryMemory storyMemory)
		{
			foreach(Condition condition in conditionList)
			{
				if (condition is EventCondition)
					storyMemory.TellTriggerFired(condition as EventCondition);				
			}
		}

        public override bool Equals(object obj)
        {
            if ((obj == null) || (!(this.GetType() == obj.GetType())))
                return false;
            
            TriggerConditionList triggerCL = (TriggerConditionList) obj;
            if (conditionList.Count != triggerCL.conditionList.Count)
                return false;

            for (int i = 0; i < conditionList.Count; i++ )
            {
                if (!conditionList[i].Equals(triggerCL.conditionList[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            string res = "";
            foreach (Condition condition in conditionList)
            {
                res += condition.GetHashCode();
            }
            return res.GetHashCode();
        }

        public void ApplySubstitution(Substitution substitution)
        {
            foreach (Condition condition in conditionList)
            {
                condition.ApplySubstitution(substitution);
            }           
        }
	    
	}
}
