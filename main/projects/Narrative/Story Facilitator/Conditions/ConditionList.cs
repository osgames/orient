// ConditionList - This class represents a set of conditions
//				   For this set of conditions to be considered satisfied all
//				   the conditions on the set must be satisfied
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Collections;

namespace Narrative.Conditions
{
	/// <summary>
	/// Contains a list of conditions that is true
	/// when all the conditions inside it are true.
	/// Used to represent the PreConditions and FinishConditions of an episode
	/// </summary>
	[Serializable()]
	public class ConditionList
	{

		#region Exceptions
		public class ConditionListException : Exception
		{
			public ConditionListException()
			{
			}
			public ConditionListException(string errorMessage) : base(errorMessage)
			{
			}
			public ConditionListException(string errorMessage, Exception innerException) :base(errorMessage, innerException)
			{
			}
		}
		#endregion
		
		protected ArrayList conditionList;

		public ConditionList()
		{
			conditionList = new ArrayList();
		}
	    
	    public ConditionList(ConditionList cl)
	    {
	        conditionList = new ArrayList();
	        foreach (Condition condition in cl.conditionList)
	        {
                conditionList.Add(condition.Clone());
	        }
	    }

		public void AddCondition(Condition c)
		{
			conditionList.Add(c);
		}

		/// <summary>
		/// Tests a set of conditions
		/// If one of them is false then the ConditionList is false
		/// There can only be one test to an action start per condition list
		/// If there is a test to an action-start, all the other events must
		/// have happened before the action-start event.
		/// </summary>
		/// <param name="storyMemory"></param>
		/// <returns></returns>
		public bool IsSatisfied(StoryMemory storyMemory)
		{
			bool res = true;

			EventCondition eventConditionActionStart = null;
			foreach(Condition c in conditionList)
			{
				if ((c is EventCondition)&&((c as EventCondition).Type == StoryEvent.EventType.ACTION_STARTED))
				{
					if (eventConditionActionStart == null)
                        eventConditionActionStart = c as EventCondition;
					else
						throw new ConditionListException("More than one EventCondition of type Action-Start per ConditionList");
				}
				
			}

			int order = -1;
			if(eventConditionActionStart != null)
			{
				order = storyMemory.AskOrder(eventConditionActionStart); //Order of the last event (more recent) that satisfies the event-condition
				if (order == -1) //Means that no event satisfies the ActionStart event condition
					return false;
			}

			foreach(Condition c in conditionList)
			{
				if (c is EventCondition)
				{
					if(eventConditionActionStart == null)
						res = res && storyMemory.Ask(c as EventCondition);
					else
					{
						if ((c as EventCondition).Type != StoryEvent.EventType.ACTION_STARTED)
						{
							res = res && storyMemory.Ask(c as EventCondition, order);
						}

					}
				}else //ParticipantProperty condition
					res = res && storyMemory.Ask(c as PropertyCondition);

				if (!res)
					return res;
			}
			return true;
		}
	    
	    public ArrayList GetConditions()
	    {
            return conditionList;
	    }


		public override string ToString()
		{
			string res = "CONDITION_LIST:\n";
			foreach (Condition c in conditionList)
				res+=c.ToString();
			return res+"\n";
		}
	    
	    public ConditionList Clone()
	    {
	        return new ConditionList(this);
	    }

	}
}
