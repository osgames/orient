// Condition - This abstract class represents a condition
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//
using System;
using System.Collections.Generic;

namespace Narrative.Conditions
{
	/// <summary>
	/// Represents a condition, that may be used to test the Story Memory,
	/// or the properties of the participants
	/// </summary>
	[Serializable()]
	public abstract class Condition
	{
		/// <summary>
		/// Means that the field of the condition with the value ANY_VALUE is not tested.
		/// </summary>
		public const string ANY_VALUE = "*";

        public abstract Condition Clone();

	    public abstract List<string> GetVariableList();

	    public abstract void ApplySubstitution(Substitution substitution);
		
	}
}
