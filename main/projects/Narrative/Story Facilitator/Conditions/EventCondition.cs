// EventCondition - Represents a condition over an event
//
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Narrative.Conditions
{
	/// <summary>
	/// Tests the StoryMemory to see if a certain event
	/// has occurred or not
	/// </summary>
	[Serializable()]
	public sealed class EventCondition : Condition
	{
		public static string ANY_EPISODE = ""; //Episode names should never be empty
		public static int ANY_TRIGGER = -1; //triggersID are all >=1
		private string subject;
		private string action;
		private string parameters;
		private string episodeName;
		private int triggerID;
		private StoryEvent.EventType type;
		private bool negated = false;
	    private bool currentEpisode = false;
	    private int episodeID = Episode.EPISODE_ID_NOT_SET;
	    				

		#region Properties
		
		public string Subject
		{
			get
			{
				return subject;
			}
			set
			{
				subject = value;
			}
		}
		
		public string Action
		{
			get
			{
				return action;
			}
			set
			{
				action = value;
			}
		}
		
		public string Parameters
		{
			get
			{
				return parameters;
			}
			set
			{
				parameters = value;
			}
		}

		/// <summary>
		/// Used to test if a trigger can become active
		/// </summary>
		public string EpisodeName
		{
			get { return episodeName; }
			set { episodeName = value; }
		}

		/// <summary>
		/// Used to test if a trigger already used
		/// the event to be tested to fire
		/// </summary>
		public int TriggerID
		{
			get { return triggerID; }
			set { triggerID = value; }
		}

		public StoryEvent.EventType Type
		{
			get { return type; }
			set { type = value; }
		}

		public bool Negated
		{
			get { return negated; }
			set { negated = value; }
		}

	    public bool CurrentEpisode
	    {
	        get { return currentEpisode; }
	        set { currentEpisode = value; }
	    }

	    public int EpisodeID
	    {
	        get { return episodeID; }
	        set { episodeID = value; }
	    }

	    #endregion

		public EventCondition()
		{
			this.subject = "";
			this.action = "";
			this.parameters = "";
			this.triggerID = ANY_TRIGGER;
			this.episodeName = ANY_EPISODE;
			Type = StoryEvent.EventType.ACTION_FINISHED;
		}

		public EventCondition(string subject, string action, string parameters)
		{
			this.subject = subject;
			this.action = action;
			this.parameters = parameters;
			this.triggerID = ANY_TRIGGER;
			this.episodeName = ANY_EPISODE;
			Type = StoryEvent.EventType.ACTION_FINISHED;
		}

		public EventCondition(string subject, string action, string parameters, StoryEvent.EventType type)
		{
			this.subject = subject;
			this.action = action;
			this.parameters = parameters;
			this.triggerID = ANY_TRIGGER;
			this.episodeName = ANY_EPISODE;
            Type = type;
		}
	    
	    public EventCondition(EventCondition ec)
	    {
            this.subject = ec.subject;
            this.action = ec.action;
            this.parameters = ec.parameters;
            this.episodeName = ec.episodeName;
            this.triggerID = ec.triggerID;
            this.type = ec.type;
            this.negated = ec.negated;
            this.episodeID = ec.episodeID;
            this.currentEpisode = ec.currentEpisode;
	    }

		public override string ToString()
		{
			string res = "EVENT_CONDITION:" + Subject + ":" + Action + ":" + Parameters +":" +EpisodeName + ":" + TriggerID + ":" + Type + ":" + ":" + Negated + ":" + CurrentEpisode + ":" + EpisodeID + "\n";
			return res;
		}

        public override Condition Clone()
        {
            return new EventCondition(this);
        }

        public override bool Equals(object obj)
        {
            if ((obj== null) || (!(this.GetType() == obj.GetType())))
                return false;

            EventCondition ec = (EventCondition) obj;
            
            return
                subject == ec.subject && action == ec.action && parameters == ec.parameters &&
                episodeName == ec.episodeName && negated == ec.negated && type == ec.type;
        }

        public override int GetHashCode()
        {
            return (subject + action + parameters + episodeName + negated + type).GetHashCode();
        }

        public override List<string> GetVariableList()
        {
            List<string> variables = new List<string>();
            foreach (Match match in Substitution.VARIABLE_REGEX.Matches(Subject))
            {
                variables.Add(match.Value);
            }

            foreach (Match match in Substitution.VARIABLE_REGEX.Matches(Parameters))
            {
                if (!variables.Contains(match.Value))
                    variables.Add(match.Value);
            }
            return variables;            
        }

	    public override void ApplySubstitution(Substitution substitution)
	    {
	        this.Subject = substitution.Apply(Subject);
	        this.Parameters = substitution.Apply(Parameters);
	    }


	}
}
