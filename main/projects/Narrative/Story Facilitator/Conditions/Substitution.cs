
using System.Text.RegularExpressions;

namespace Narrative.Conditions
{
    public class Substitution
    {
        public static readonly Regex VARIABLE_REGEX = new Regex("\\[\\w+\\]");

        private string variableName;
        private string variableValue;

        #region Properties
        public string VariableName
        {
            get { return variableName; }
            set { variableName = value; }
        }
        public string VariableValue
        {
            get { return variableValue; }
            set { variableValue = value; }
        }        
        #endregion

        public Substitution(string variablename, string variablevalue)
        {
            VariableName = variablename;
            VariableValue = variablevalue;
        }

        public string Apply(string source)
        {
            Regex variable = new Regex(VariableName.Replace("[", "\\[").Replace("]", "\\]"));
            return variable.Replace(source, VariableValue);
        }
    }
}
