using System;
using System.Collections.Generic;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Events;
using ION.Realizer.Orient.Entities;
using Narrator=ION.Realizer.Orient.Entities.Narrator;

namespace Narrative
{
    /// <summary>
    /// Removes all remote charactes and the user entity
    /// </summary>
    public class RemoveAllEntitiesAction : Action, IEventCallback<ION.Core.Events.EntityDestroyed>
    {
       #region Constants
       public const string ACTION_NAME = "RemoveAllEntitiesAction";
       #endregion

        public List<ulong> entitiesIDs;

        public RemoveAllEntitiesAction()
        {
            entitiesIDs = new List<ulong>();
        }

        protected override void OnStart(Context context)
        {
            foreach (Entity entity in Universe.Instance.Entities)
            {
                if ((entity is RemoteCharacter) || (entity is UserEntity) || (entity is UserController) || (entity is Narrator) || (entity is FAtiMA.RemoteAgent.LanguageEngineMaster) || (entity is Item) || (entity is ModellessItem))
                {
                    entitiesIDs.Add(entity.UID);
                    Universe.Instance.DestroyEntity(entity.Name);
                }
            }
        }


        protected override void OnEnd(Context context)
        {
            entitiesIDs = new List<ulong>();
        }

       protected bool HasEnded()
        {
            return (entitiesIDs.Count == 0);
        }

        protected override void OnCreate(Arguments arguments)
        {
            Universe.Instance.CreateEventListener<ION.Core.Events.EntityDestroyed>(this);
        }

        protected override void OnDestroy()
        {            
        }

        public void Invoke(EntityDestroyed e)
        {
            if (entitiesIDs.Contains(e.Entity.UID))
            {
                entitiesIDs.Remove(e.Entity.UID);
            }
        }

        protected override void OnStep(Context context)
        {
            if (HasEnded())
            {
                this.End();
            }
        }

        protected override void OnFail(Context context)
        {
            throw new NotImplementedException();
        }
    }
}
