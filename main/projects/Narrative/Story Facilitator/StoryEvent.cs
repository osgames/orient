// TellStoryEvent - This class represents an event in the virtual environment.
//		   An event contains information about an action; if 
//		   that action is starting or finishing and with which parameters
//         was the action executed.
// 
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;

namespace Narrative
{
	/// <summary>	
	/// Holds the information about an event that occurred in the agents framework.
	/// </summary>
	[Serializable()]
	public sealed class StoryEvent
	{
		public const string ENTITY_ADDED = "ENTITY-ADDED";
        public const string ENTITY_REMOVED = "ENTITY-REMOVED";
	    public const string PROPERTY_CHANGED = "PROPERTY-CHANGED";
		public const string STORY_START = "START-STORY";
		public const string NARRATE_ACTION = "NARRATE-TEXT";
        public const string INSERT_CHARACTER_ACTION = "INSERT-CHARACTER";
        public const string REMOVE_CHARACTER_ACTION = "REMOVE-CHARACTER";
		public const string INSERT_OBJECT_ACTION = "INSERT-OBJECT";
        public const string REMOVE_OBJECT_ACTION = "REMOVE-OBJECT";
		public const string ACT_FOR_CHARACTER_ACTION = "ACT-FOR-CHARACTER";
        public const string ACT_FOR_ENVIRONMENT_ACTION = "ACT-FOR-ENVIRONMENT";
		public const string NEXT_EPISODE_ACTION = "NEXT-EPISODE";
        public const string SELECT_EPISODE = "SELECT-EPISODE";
		public const string CHANGE_STORY_MODE = "CHANGE-STORY-MODE";
		public const string ADD_GOALS = "ADD-GOALS";
		public const string REMOVE_GOAL = "REMOVE-GOAL";
		public const string REMOVE_ALL_GOALS = "REMOVE-ALL-GOALS";
		public const string CHANGE_PERSPECTIVE = "CHANGE-PERSPECTIVE";
        public const string CHANGE_GOAL_IMPORTANCE_OF_SUCCESS = "CHANGE-GOAL-IMPORTANCE-OF-SUCCESS";
		public const string CHANGE_GOAL_IMPORTANCE_OF_FAILURE = "CHANGE-GOAL-IMPORTANCE-OF-FAILURE";
        public const string CHANGE_PROPERTY = "CHANGE-PROPERTY";

        public const string IDLE_TIME_LIMIT_REACHED = "IDLE-TIME-LIMIT-REACHED";
        public const string TIME_LIMIT_REACHED = "TIME-LIMIT-REACHED";
	    

        //Framework actions
        public const string PROPERTY_CREATED = "PROPERTY_CREATED";
        public const string PROPERTY_REMOVED = "PROPERTY_REMOVED"; 
       


		public enum EventType {ACTION_FINISHED, ACTION_STARTED};

		private string subject;
		private string action;
		private string parameters;
		private EventType type;		

		#region Properties
		
		public string Subject
		{
			get
			{
				return subject;
			}
			set
			{
				subject = value;
			}
		}
		
		public string Action
		{
			get
			{
				return action;
			}
			set
			{
				action = value;
			}
		}
		
		public string Parameters
		{
			get
			{
				return parameters;
			}
			set
			{
				parameters = value;
			}
		}

		public EventType Type
		{
			get { return type; }
			set { type = value; }
		}

		#endregion

		public StoryEvent(string subject, string action, string parameters, EventType type)
		{
			Subject = subject;
			Action = action;
			Parameters = parameters;
			Type = type;
		}


		public override bool Equals(object obj)
		{
			if ((obj == null)||(obj.GetType() != this.GetType()))
				return false;
			StoryEvent e = obj as StoryEvent;
			return ((e.Subject).Equals(Subject) && (e.Action.Equals(Action)) && (e.Parameters.Equals(Parameters)) && (e.Type == Type));
		}

		public override int GetHashCode()
		{			
			return (subject+action+parameters+Type.ToString()).GetHashCode();
		}

		public override string ToString()
		{
			string res = "EVENT:" + Subject + ":" + Action + ":" + Parameters + ":" + Type + "\n";
			return res;
		}

	}
}
