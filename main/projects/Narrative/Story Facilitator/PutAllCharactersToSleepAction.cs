using System;
using System.Collections.Generic;
using FAtiMA.RemoteAgent;
using ION.Core;
using ION.Core.Events;
using ION.Core.Extensions;

namespace Narrative
{
    public class PutAllCharactersToSleepAction : Action, 
                                                 IEventCallback<ION.Core.Events.ActionEnded>
    {
        public const string ACTION_NAME = "PutAllCharactersToSleepAction";
        List<ulong> actionsWaitingToEnd;        
        
        public PutAllCharactersToSleepAction()
        {
            actionsWaitingToEnd = new List<ulong>();            
        }
        
        protected override void OnStart(Context context)
        {
            foreach (Entity entity in Universe.Instance.Entities)
            {
                if (entity is RemoteCharacter)
                {
                    RemoteCharacter rmChar = entity as RemoteCharacter;
                    if (rmChar.IsActive)
                    {
                        PutToSleepAction putToSleepAction = rmChar.GetAction<PutToSleepAction>(PutToSleepAction.ACTION_NAME);                        
                        putToSleepAction.Start();

                        Dictionary<string, object> args = new Dictionary<string, object>();
                        //args["visibility"] = false;
                        rmChar.GetAction("leave-stage").Start(new Arguments(args));

                        actionsWaitingToEnd.Add(putToSleepAction.UID);
                    }
                }
            }
        }



        protected override void OnEnd(Context context)
        {
            actionsWaitingToEnd = new List<ulong>();
        }

        protected override void OnStep(Context context)
        {
            if (HasEnded())
            {
                this.End();
            }
        }

        protected bool HasEnded()
        {
            return (actionsWaitingToEnd.Count == 0);
        }

        protected override void OnCreate(Arguments arguments)
        {
            Universe.Instance.CreateEventListener<ION.Core.Events.ActionEnded>(this);
        }


        public void Invoke(ActionEnded e)
        {
            if (actionsWaitingToEnd.Contains(e.Action.UID))
            {
                actionsWaitingToEnd.Remove(e.Action.UID);                
            }
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
