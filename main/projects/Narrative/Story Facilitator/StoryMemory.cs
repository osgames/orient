// StoryMemory- Class that stores information about all the events (through eventInfo's) and
//			    all the properties of the characters and objects.
// 
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 06/04/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//

using System;
using System.Collections;
using Narrative.Conditions;
using Narrative.Participants;

namespace Narrative
{
	/// <summary>
	/// Summary description for StoryMemory.
	/// </summary>
	[Serializable()]
	public class StoryMemory
	{
		/// <summary>
		/// Used to order the events, if you want to test a condition that has an event of type ACTION-START
		/// you must check if all the other events in the condition happened before that event
		/// </summary>
		private static int order = 1;

		private ArrayList properties;
		private ArrayList events;

		public StoryMemory()
		{
			properties = new ArrayList();
			events = new ArrayList();
		}
	    
	    public StoryMemory(StoryMemory storyMemory)
	    {
	        properties = new ArrayList();
	        events = new ArrayList();

	        foreach (ParticipantProperty property in storyMemory.properties)
	        {
                properties.Add(property.Clone());
	        }

	        foreach (EventInfo eventInfo in storyMemory.events)
	        {
                events.Add(eventInfo.Clone());
	        }	        	        
	    }
	    
	    public virtual StoryMemory Clone()
	    {
	        return new StoryMemory(this);
	    }

		public virtual void Tell(StoryEvent e, String episodeName, int episodeID)
		{
			events.Add(new EventInfo(e, episodeName, episodeID, ++order));
		}

		public virtual void Tell(ParticipantProperty p)
		{
			if(properties.Contains(p))
			{
				//The equals method of the property class only tests the holder
				//and the name of the property. So if the property list already 
				//contains this property, we have to update. By using the remove
				//method we remove the old property with the old value (because the 
				//equals method doesn't test the property value) and then we add the
				//same property, but with a new value.
				properties.Remove(p);
				properties.Add(p);
			}
			else
				properties.Add(p);
		}
		
		public virtual bool Ask(PropertyCondition pc)
		{
			//All properties have to be tested all times, because there may be several properties
			//that satisfy part of the condition(ParticipantName = "*", PropertyName="X", Value="5", Operator=GREATER_THAN)
			//but don't satisfy the value conditions. (This happens because of the use of Condition.ANY_VALUE)
			foreach(ParticipantProperty property in  properties)
			{
				if ((pc.ParticipantName == Condition.ANY_VALUE) || (property.Holder == pc.ParticipantName))
					if ((pc.PropertyName == Condition.ANY_VALUE) || (property.Name == pc.PropertyName))
						if((pc.Value == Condition.ANY_VALUE))
							return true;
						else
						{							
							if (pc.Operator == PropertyCondition.Operators.Equal)
							{
								if (pc.Value == property.Value) 
									return true;	
							}
							else if (pc.Operator == PropertyCondition.Operators.GreaterThan)
							{
								try
								{
									if (int.Parse(property.Value) > int.Parse(pc.Value)) 
										return true;
								}
								catch(FormatException)
								{
									//If the propertyName in PropertyCondition has the
									//Condition.ANY_VALUE value some filds that are not
									//integer may be tested, causing this exception, that
									//in this context is no error.
								}
							}
							else if (pc.Operator == PropertyCondition.Operators.LesserThan)
							{
								try
								{
									if (int.Parse(property.Value) < int.Parse(pc.Value)) 
										return true;
								}
								catch(FormatException){}
							}
							else if (pc.Operator == PropertyCondition.Operators.NotEqual)
							{
								if (pc.Value != property.Value) 
									return true;
							}
							else 
								throw new PropertyCondition.OperatorNotImplementedException("Test for operator: " + pc.Operator + " not implemented in StoryMemory");						}							
			}
			return false;
		}
	    
	    public virtual ParticipantProperty GetParticipantProperty(string holder, string propertyName)
	    {
	        foreach (ParticipantProperty property in properties)
	        {
	            if ((property.Name == propertyName)&&(holder == property.Holder))
	            {
                    return property;
	            }
	        }
	        return null;
	    }

        public virtual bool HasProperty(string holder, string propertyName)
        {
            return (GetParticipantProperty(holder, propertyName) != null);
        }

		public virtual bool Ask(EventCondition ec)
		{
		
			foreach(EventInfo eventInfo in events)
			{
				StoryEvent e = eventInfo.StoryEvent;
			    
			    if ((ec.CurrentEpisode)&&(ec.EpisodeID != eventInfo.EpisodeID)) 
			        continue;

				if ((ec.Subject == Condition.ANY_VALUE) || (ec.Subject == e.Subject))
					if ((ec.Action == Condition.ANY_VALUE) || (ec.Action == e.Action))						
						if (TestParameters(e.Parameters, ec.Parameters))
							if (ec.Type == e.Type)
							{
								if ((ec.EpisodeName.Equals(EventCondition.ANY_EPISODE)) && (ec.TriggerID.Equals(EventCondition.ANY_TRIGGER)))
									if(!ec.Negated)
                                        return true;
									else
										return false;
								else if ((!ec.EpisodeName.Equals(EventCondition.ANY_EPISODE)) && (ec.TriggerID.Equals(EventCondition.ANY_TRIGGER)))
								{
									if (ec.EpisodeName.Equals(eventInfo.EpisodeName))
										if (!ec.Negated)
											return true;
										else
											return false;
								}
								else
								{
									if ((ec.EpisodeName.Equals(eventInfo.EpisodeName)) &&(!eventInfo.FiredTriggersIDs.Contains(ec.TriggerID)))
									{
										if (!ec.Negated)
											return true;
										else
											return false;
									}
								}
							}							
			}
			if (!ec.Negated)
				return false;
			else
				return true;
		}

		/// <summary>
		/// Tests if a certain Eventcondition is satisfied and if the 
		/// event that satisfies it has an order prior to param order
		/// </summary>
		/// <param name="ec"></param>
		/// <param name="order"></param>
		/// <returns></returns>
		public virtual bool Ask(EventCondition ec, int order)
		{
			foreach(EventInfo eventInfo in events)
			{
				if (eventInfo.Order >= order) //only test events that happened prior to 'order'
					continue;
			    
                if ((ec.CurrentEpisode) && (ec.EpisodeID != eventInfo.EpisodeID))
                    continue;

                StoryEvent e = eventInfo.StoryEvent;

				if ((ec.Subject == Condition.ANY_VALUE) || (ec.Subject == e.Subject))
					if ((ec.Action == Condition.ANY_VALUE) || (ec.Action == e.Action))						
						if (TestParameters(e.Parameters, ec.Parameters))
							if (ec.Type == e.Type)
							{
								if ((ec.EpisodeName.Equals(EventCondition.ANY_EPISODE)) && (ec.TriggerID.Equals(EventCondition.ANY_TRIGGER)))
									if(!ec.Negated)
										return true;
									else
										return false;
								else if ((!ec.EpisodeName.Equals(EventCondition.ANY_EPISODE)) && (ec.TriggerID.Equals(EventCondition.ANY_TRIGGER)))
								{
									if (ec.EpisodeName.Equals(eventInfo.EpisodeName))
										if (!ec.Negated)
											return true;
										else
											return false;
								}
								else
								{
									if ((ec.EpisodeName.Equals(eventInfo.EpisodeName)) &&(!eventInfo.FiredTriggersIDs.Contains(ec.TriggerID)))
									{
										if (!ec.Negated)
											return true;
										else
											return false;
									}
								}
							}							
			}
			if (!ec.Negated)
				return false;
			else
				return true;
		}

		/// <summary>
		/// Returns the order number of the last event that satisfies the EventCondition
		/// Returns -1 if no event satisfies the condition
		/// </summary>
		/// <param name="ec"></param>
		/// <returns></returns>
		public virtual int AskOrder(EventCondition ec)
		{
			int order = -1;
			foreach(EventInfo eventInfo in events)
			{
				StoryEvent e = eventInfo.StoryEvent;

                if ((ec.CurrentEpisode) && (ec.EpisodeID != eventInfo.EpisodeID))
                    continue;

				if ((ec.Subject == Condition.ANY_VALUE) || (ec.Subject == e.Subject))
					if ((ec.Action == Condition.ANY_VALUE) || (ec.Action == e.Action))						
						if (TestParameters(e.Parameters, ec.Parameters))
							if (ec.Type == e.Type)
							{
								if ((ec.EpisodeName.Equals(EventCondition.ANY_EPISODE)) && (ec.TriggerID.Equals(EventCondition.ANY_TRIGGER)))
									order = eventInfo.Order;
								else if ((!ec.EpisodeName.Equals(EventCondition.ANY_EPISODE)) && (ec.TriggerID.Equals(EventCondition.ANY_TRIGGER)))
								{
									if (ec.EpisodeName.Equals(eventInfo.EpisodeName))
										order = eventInfo.Order;
								}
								else
								{
									if ((ec.EpisodeName.Equals(eventInfo.EpisodeName)) &&(!eventInfo.FiredTriggersIDs.Contains(ec.TriggerID))) //The EventConditions for the triggers always have an EpisodeName != from ANY_EPISODE (see the parsing of the TirggerConditionList in EpisodeParser)
									{
										order = eventInfo.Order;
									}
								}
							}							
			}
			return order;
		}

		/// <summary>
		/// Returns true if the eventParameters satisfy the testParameters
		/// Example:
		/// if testParameters == "*" then the test is always true
		/// eventParameters == "John say fightback" and testParameters == "John say *" returns true
        /// or eventParameters == "John say fightback" and testParameters == "John *" also returns true
		/// </summary>
		/// <param name="eventParameters">The parameters of the event</param>
		/// <param name="testParameters">The parameters for the test</param>
		/// <returns></returns>
		private bool TestParameters(string eventParameters, string testParameters)
		{
			if (testParameters == Condition.ANY_VALUE)
				return true;

			
			string[] evParamenters = eventParameters.Split(' ');
			string[] tstParameters = testParameters.Split(' ');

            if (tstParameters.Length > evParamenters.Length)
                return false;
			
			for(int i=0; i<tstParameters.Length; i++)
				if ((tstParameters[i] != Condition.ANY_VALUE)&&(tstParameters[i] != evParamenters[i]))
					return false;

			return true;
		}
		


		/// <summary>
		/// Returns the last event in the Story Memory
		/// if no event is present in the Story Memory return null
		/// </summary>
		/// <returns></returns>
		public virtual StoryEvent AskLastEvent()
		{
			if (events.Count == 0)
				return null;
			return ((EventInfo)events[events.Count-1]).StoryEvent;
		}

		public virtual ArrayList AskParticipantProperties(string characterName)
		{
			ArrayList res = new ArrayList();

			foreach (ParticipantProperty property in properties)
			{
				if (property.Holder == characterName)
				{
					res.Add(property);
				}
			}
			return res;
		}


        public virtual void Retract(ParticipantProperty property)
        {
            this.properties.Remove(property);
        }
	    	    
	    public virtual void RetractEpisodeLevelProperties(string participantName)
	    {
	        foreach (ParticipantProperty property in (ArrayList)properties.Clone())
	        {
	            if ((property.Holder == participantName)&&(!property.StoryLevel))
	            {
	                this.Retract(property);
	            }
	        }
	    }


		public override string ToString()
		{
			string res ="KNOWLEDGE_BASE:\n";
			res+="PROPERTIES:\n";
			foreach(ParticipantProperty p in properties)
			{
				res+=p.ToString();
			}
			res+="EVENTS:\n";
			foreach(StoryEvent e in events)
			{
				res+=e.ToString();
			}
			return res;
		}

		/// <summary>
		/// Updates the EventInfo.FiredTriggersIDs list with the TriggerID of the EventCondition
		/// </summary>
		/// <param name="ec"></param>
		public virtual void TellTriggerFired(EventCondition ec)
		{
			foreach(EventInfo eventInfo in events)
			{
				StoryEvent e = eventInfo.StoryEvent;
				if ((ec.Subject == Condition.ANY_VALUE) || (ec.Subject == e.Subject))
					if ((ec.Action == Condition.ANY_VALUE) || (ec.Action == e.Action))						
                        if (TestParameters(e.Parameters, ec.Parameters))
							if ((ec.EpisodeName.Equals(eventInfo.EpisodeName)) &&(!eventInfo.FiredTriggersIDs.Contains(ec.TriggerID)))
								if(ec.Type == e.Type)
								{
                                    //Only the first event that satisfies the condition is updated the ID of the trigger in the 
                                    //story memory, because it will be the one used to satisfy the trigger condition.
									eventInfo.FiredTriggersIDs.Add(ec.TriggerID);
									return;
								}
			}
		}						
							
	}
}
