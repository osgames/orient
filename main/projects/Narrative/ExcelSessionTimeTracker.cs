using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Narrative
{
    public class ExcelSessionTimeTracker
    {
        #region Constants
        public const string LOG_FILE_NAME = "excel_time_log";
        public const string LOG_FILE_SUFIX = ".csv";
        #endregion

        #region Fields

        private string logDir = "logs\\";
        private string userCode;
        private StreamWriter sWriter;
        private DateTime previousTime;
        private DateTime startTime;
        private List<EpisodeTime> episodesSeen;

        #endregion

        #region Properties

        public string UserCode
        {
            get { return userCode; }
        }

        public string LogDir
        {
            get { return logDir; }
            set { logDir = value; }
        }

        #endregion

        private class EpisodeTime
        {
            public readonly String episodeName;
            public readonly TimeSpan episodeTime;

            public EpisodeTime(String episodeName, TimeSpan episodeTimeLength)
            {
                this.episodeName = episodeName;
                this.episodeTime = episodeTimeLength;
            }
        }

        public ExcelSessionTimeTracker(string userCode)
        {
            this.userCode = userCode;
            previousTime = DateTime.Now;
            startTime = DateTime.Now;
            episodesSeen = new List<EpisodeTime>();
        }
        
        public void EpisodeEnd(String episodeName)
        {
            if (episodeName.Equals(""))
                return;

            TimeSpan elapsedTime = DateTime.Now - previousTime;           
            previousTime = DateTime.Now;           
            this.episodesSeen.Add(new EpisodeTime(episodeName, elapsedTime));
        }

        public void End()
        {
            if (!Directory.Exists(LogDir + StoryFacilitator.USERS_DIR))
            {
                Directory.CreateDirectory(LogDir + StoryFacilitator.USERS_DIR);
            }

            if (File.Exists(LogDir + StoryFacilitator.USERS_DIR + "\\" + LOG_FILE_NAME + LOG_FILE_SUFIX))
            {
                sWriter = File.AppendText(LogDir + StoryFacilitator.USERS_DIR + "\\" + LOG_FILE_NAME + LOG_FILE_SUFIX);
                sWriter.WriteLine();
                sWriter.Flush();
            }
            else
            {
                sWriter = File.CreateText(LogDir + StoryFacilitator.USERS_DIR + "\\" + LOG_FILE_NAME + LOG_FILE_SUFIX);
                sWriter.WriteLine("SUBJECT ID;DATE;TOTAL TIME;EPISODE TIME...");
                sWriter.WriteLine();
                sWriter.Flush();
            }

            TimeSpan totalTime = DateTime.Now - startTime;
            String firstExcelLine = userCode + ";" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;            
            firstExcelLine += ";" + totalTime.TotalSeconds + ";";
            String secondExcelLine = ";;;";
            foreach (EpisodeTime episodeTime in episodesSeen)
            {
                if (episodeTime.episodeTime.TotalSeconds != 0)
                {
                    firstExcelLine += episodeTime.episodeName + ";";
                    secondExcelLine += episodeTime.episodeTime.TotalSeconds + ";";                    
                }
            }

            sWriter.WriteLine(firstExcelLine);
            sWriter.WriteLine(secondExcelLine);
            sWriter.WriteLine();
            sWriter.Flush();

            sWriter.Close();
        }
    }
}
