// Serializer - 
//			   
// 
// Copyright (C) 2006 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 14/12/2006
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//
using System;
using ION.Core.Extensions;

namespace Narrative
{
    public class Serializer
    {
        public enum SerializationFormat { Xml, Binary }

        public static System.Boolean SerializeToDisk(string path, object request, SerializationFormat format)
        {
            try
            {
                System.IO.MemoryStream oStream = Serialize(request, format);
                if (System.IO.File.Exists(path)) System.IO.File.Delete(path);
                System.IO.FileStream output = System.IO.File.Open(path, System.IO.FileMode.CreateNew);
                output.Write(oStream.ToArray(), 0, Convert.ToInt32(oStream.Length));
                output.Close();
                oStream.Close();
                return true;
            }
            catch (Exception e)
            {
                ApplicationLogger.Instance().WriteLine("Error serializing to disk: " + e.Message);
                return false;
            }
        }

        public static object DeSerializeFromDisk(string path, SerializationFormat format)
        {
            try
            {
                if (System.IO.File.Exists(path))
                {
                    System.IO.FileStream input = System.IO.File.Open(path, System.IO.FileMode.Open);
                    byte[] filecontents = new byte[Convert.ToInt32(input.Length)];
                    input.Read(filecontents, 0, Convert.ToInt32(input.Length));
                    input.Close();
                    //System.IO.File.Delete(path);
                    System.IO.MemoryStream inStream = new System.IO.MemoryStream(filecontents);
                    object newObj = DeSerialize(inStream, format);
                    inStream.Close();
                    return newObj;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        #region wrappers for binary and xml serialization
        public static System.IO.MemoryStream Serialize(object request, SerializationFormat format)
        {
            if (format == SerializationFormat.Binary)
                return SerializeBinary(request);
            else
                return SerializeSOAP(request);
        }
        public static object DeSerialize(System.IO.MemoryStream memStream, SerializationFormat format)
        {
            if (format == SerializationFormat.Binary)
                return DeSerializeBinary(memStream);
            else
                return DeSerializeSOAP(memStream);
        }
        #endregion

        #region Binary Serializers
        public static System.IO.MemoryStream SerializeBinary(object request)
        {
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter serializer = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            System.IO.MemoryStream memStream = new System.IO.MemoryStream();
            serializer.Serialize(memStream, request);
            return memStream;
        }

        public static object DeSerializeBinary(System.IO.MemoryStream memStream)
        {
            memStream.Position = 0;
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter deserializer = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            object newobj = deserializer.Deserialize(memStream);
            memStream.Close();
            return newobj;
        }
        #endregion

        #region XML Serializers

        public static System.IO.MemoryStream SerializeSOAP(object request)
        {
            System.Runtime.Serialization.Formatters.Soap.SoapFormatter serializer = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();
            System.IO.MemoryStream memStream = new System.IO.MemoryStream();
            serializer.Serialize(memStream, request);
            return memStream;
        }

        public static object DeSerializeSOAP(System.IO.MemoryStream memStream)
        {
            object sr;
            System.Runtime.Serialization.Formatters.Soap.SoapFormatter deserializer = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();
            memStream.Position = 0;
            sr = deserializer.Deserialize(memStream);
            memStream.Close();
            return sr;
        }
        #endregion

        #region Miscellaneous
        public static string ConvertStreamToString(System.IO.Stream theStream)
        {
            string theString = "";
            theStream.Position = 0;
            using (System.IO.StreamReader sr = new System.IO.StreamReader(theStream))
            {
                theString = sr.ReadToEnd();
                // Close and clean up the StreamReader
                sr.Close();
            }
            return theString;
        }
        #endregion
    }

}