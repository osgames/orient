using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using ION.Core;
using ION.Core.Events;
using ION.Realizer.Orient.Actions.Body.Behaviours;
using ION.Realizer.Orient.Actions.User;
using ION.Realizer.Orient.Entities;
using ION.Realizer.Orient.Actions.UserController;
using FAtiMA.Actions;
using FAtiMA.RemoteAgent;

namespace Oracle
{



    public class OracleClient : IEventCallback<ActionEnded>
    {
        public static OracleClient Instance = new OracleClient();

        private IPEndPoint ipep;
        private Socket server;
        private bool oracleConnected = false;

        public void initialise(string oracleIP, int oraclePort)
        {
            try
            {
                IPAddress ip;
                if (oracleIP.Equals("localhost"))
                    ip = IPAddress.Loopback;
                else
                    ip = IPAddress.Parse(oracleIP);

                ipep = new IPEndPoint(ip, oraclePort);

                server = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);
                server.Connect(ipep);
                Universe.Instance.CreateEventListener<ActionEnded>(this);
                oracleConnected = true;
                
            }
            catch (SocketException)
            {
                oracleConnected = false;
            }
        
        }


        #region IEventCallback<ActionEnded> Members

        public void Invoke(ActionEnded evt)
        {
            if (evt.Action is Behaviour) 
            {
                if (oracleConnected) 
                    sendEventAgent(evt);
            }
            else if (evt.Action is UserStoryAction)
            {
                if (oracleConnected) 
                    sendEventUser(evt);
            }
            else if (evt.Action.Parent is UserController)
            {
                if (oracleConnected) 
                    sendInput(evt);
            }        
        }

        #endregion

        // sends a message regarding a world event caused by an agent to the oracle
        private void sendEventAgent(ActionEnded evt)
        {
            string subject = evt.Action.Parent.Name;
            string action = evt.Action.Name;
            string target = "";
            string parameters = "";
            string meaningStr = "";

            if (evt.Context.Contains(RemoteCharacter.REMOTE_ACTION_PARAMETERS))
            {
                // get the remote action object for this behaviour
                RemoteAction remoteAction = (RemoteAction)evt.Context[RemoteCharacter.REMOTE_ACTION_PARAMETERS];
                
                // get the target of the action
                target = " " + remoteAction.Target;

                // get the parameters of the action
                foreach (string parameter in remoteAction.Parameters)
                    parameters = parameters + " " + parameter;

                // special case (if this is a Speech Act) get the meaning (i.e. type of speech act)
                if (remoteAction is SpeechAct) 
                    meaningStr = (remoteAction as SpeechAct).Meaning;
            }

            string message = "ORIENT " + subject + " " + action + meaningStr + target + parameters +"\0";
            send(message);        
        }


        // sends a message regarding a world event caused by the user to the oracle
        private void sendEventUser(ActionEnded evt)
        {
            string action = evt.Action.Name;
            string target = "";
            string parameters = "";
            
            // get target
            if (evt.Context.Contains(UserStoryAction.TARGET_NAME)) target = " " + (string)evt.Context[UserStoryAction.TARGET_NAME];
            
            // get parameters
            if (evt.Context.Contains(UserStoryAction.PARAMETERS_NAME)) parameters = " " + (string)evt.Context[UserStoryAction.PARAMETERS_NAME];

            string message = "ORIENT user " + action + target + parameters +"\0";
            send(message);
        }


        // send a message regarding user input to the oracle
        private void sendInput(ActionEnded evt)
        {
            string action = evt.Action.Name;
            string parameter = "";

            // check for possible parameters

            // gesture action: parameter is name of gesture
            if (evt.Context.Contains(UserGestureAction.GESTURE_NAME))
                parameter = " " + (string)evt.Context[UserGestureAction.GESTURE_NAME];
            else if (evt.Context.Contains(UserWiiButtonAction.BUTTON_NAME))
                parameter = " " + (string)evt.Context[UserWiiButtonAction.BUTTON_NAME];
            else if (evt.Context.Contains(UserMobilePhoneButtonAction.BUTTON_NAME))
                parameter = " " + (string)evt.Context[UserMobilePhoneButtonAction.BUTTON_NAME];
            else if (evt.Context.Contains(UserNavigationAction.DIRECTION))
                parameter = " " + (string)evt.Context[UserNavigationAction.DIRECTION];
            else if (evt.Context.Contains(UserSelectItemAction.ITEM_NAME))
                parameter = " " + (string)evt.Context[UserSelectItemAction.ITEM_NAME];
            else if (evt.Context.Contains(UserSpeechRecogAction.CHAR_NAME))
                parameter = " " + (string)evt.Context[UserSpeechRecogAction.CHAR_NAME];

            string message = "USER " + action + parameter + "\0";
            send(message);
        }


        // send the message off
        private void send(string message)
        {
            if (server != null && server.Connected) 
            server.Send(ASCIIEncoding.UTF8.GetBytes(message));
        }
    }
}
