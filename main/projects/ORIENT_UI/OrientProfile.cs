
// OrientProfile - This class stores information about the fixed FearNot parameters (See RenderWindow::Main)
//				     
//
// Copyright (C) 2007 GAIPS/INESC-ID
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Company: GAIPS/INESC-ID
// Project: FearNot!
// Created: 31/01/2007
// Created by: Rui Figueiredo
// Email to: rui.figueiredo@tagus.ist.utl.pt
// 
// History:
//
namespace ION.App
{
    /// <summary>
    /// This class stores information about the fixed FearNot parameters
    /// </summary>
    public class OrientProfile
    {
        private static OrientProfile orientProfile = new OrientProfile();

        private string logsDir;
        private string usersDir;
        private string scenarioLocation;
        private string oracleServerAddress;
        private string oracleServerPort;

        #region Properties
        public string UsersDir
        {
            get { return usersDir; }
            set { usersDir = value; }
        }

        public string LogsDir
        {
            get { return logsDir; }
            set { logsDir = value; }
        }
        
        public string ScenarioLocation
        {
            get { return scenarioLocation; }
            set { scenarioLocation = value; }
        }

        public string OracleServerAddress
        {
            get { return oracleServerAddress; }
            set { oracleServerAddress = value; }
        }

        public string OracleServerPort
        {
            get { return oracleServerPort; }
            set { oracleServerPort = value; }
        }


        #endregion
        
        OrientProfile()
        {
        }


        public static OrientProfile Instance()
        {
            return orientProfile;
        }
    }
}
