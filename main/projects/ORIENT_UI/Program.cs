/* ****************************************************
 * Name: Program.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/07/07 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using AMS.Profile;
using ION.App;
using ION.Core;
using ION.Core.Extensions;
using ION.Realizer.Orient;
using Mogre;
using PAGE.Orient;

namespace ORIENT_UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Read Orient's profile
            //AMS.Profile.dll -> http://www.codeproject.com/csharp/ReadWriteXmlIni.asp
            Xml profile = new Xml("OrientProfile.xml");

            try
            {
                using (profile.Buffer())
                {
                    OrientProfile.Instance().ScenarioLocation =
                        (string) profile.GetValue("Application", "ScenarioLocation");
                    OrientProfile.Instance().UsersDir = (string) profile.GetValue("Application", "UsersDir");
                    OrientProfile.Instance().LogsDir = (string) profile.GetValue("Application", "LogsDir");
                    OrientProfile.Instance().OracleServerAddress = (string)profile.GetValue("Application", "OracleServerAddress");
                    OrientProfile.Instance().OracleServerPort = (string)profile.GetValue("Application", "OracleServerPort");
                }

                Process[] processes = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);
                if (processes.Length > 1)
                {
                    MessageBox.Show("Orient already running...");
                    Application.Exit();
                    return;
                }

                using (RenderWindow renderWindow = new RenderWindow())
                {
                    renderWindow.Show();

                    LogManager.Singleton.CreateLog(Constants.ION_LOG_FILE, false, true, false);

                    DateTime timeBeforeION = DateTime.Now;
                    DateTime timeBeforeOGRE = DateTime.Now;

                    while (renderWindow.Created)
                    {
                        if ((DateTime.Now - timeBeforeOGRE).Milliseconds > 15)
                        {
                            timeBeforeOGRE = DateTime.Now;
                            OrientGraphicsRealizer.Instance.Update();
                        }

                        if ((DateTime.Now - timeBeforeION).Milliseconds > 60)
                        {
                            timeBeforeION = DateTime.Now;
                            Universe.Instance.Update();
                            User.Interface.UserInterface.Instance.update();
                        }

                        Application.DoEvents();
                    }
                }

                ApplicationLogger.Instance().WriteLine("Exiting aplication...");
                LogManager.Singleton.DestroyLog(Constants.ION_LOG_FILE);
                OrientGraphicsRealizer.Instance.Destroy();
            }
            catch (SEHException)
            {
                if (OgreException.IsThrown)
                {
                    MessageBox.Show(OgreException.LastException.FullDescription);
                    ApplicationLogger.Instance().WriteLine("Ogre exception thrown: " +
                                                           OgreException.LastException.FullDescription);
                }
                else throw;
            }
            //catch (Exception e)
            //{
            //    MessageBox.Show(e.Message);
            //    ApplicationLogger.Instance().WriteLine("Exception thrown: " + e.Message);
            //}
        }
    }
}