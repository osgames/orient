using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using ION.App;
using ION.Core;
using ION.Core.Extensions;
using Narrative;
using PAGE.Orient.GUI.Windows.Forms.Forms;
using User;
using ION.Realizer.Orient.Actions.UserController;
using ION.Realizer.Orient.Entities;

namespace ORIENT_UI
{
    public partial class RenderWindow : FullscreenWindow
    {

        private StoryFacilitator storyFacilitator;

        public RenderWindow()
            : base("./data/resources.xml")
        {
            this.InitializeComponent();
            this.InitializeOrientScreen();

            //creates story facilitator
            this.storyFacilitator = Universe.Instance.CreateEntity<StoryFacilitator>(
                StoryFacilitator.FACILITATOR_NAME);

            User.Interface.UserInterface.Instance.initialise(
                PAGE.Orient.OrientGraphicsRealizer.Instance.OgreApplication.ConfigScreenWidth,
                PAGE.Orient.OrientGraphicsRealizer.Instance.OgreApplication.ConfigScreenHeight);

            User.Interface.CameraController.Instance.initialise();
            // start oracle client
            try
            {
                int port = int.Parse(OrientProfile.Instance().OracleServerPort);
                Oracle.OracleClient.Instance.initialise(OrientProfile.Instance().OracleServerAddress, port);
            }
            catch (Exception ex)
            {
                ApplicationLogger.Instance().WriteLine("Error reading oracle client port, not an int: " + ex.Message);
            }

            // start User Controller
            //UserController userController = 
            Universe.Instance.CreateEntity<UserController>(UserController.ENTITY_NAME);                
        }

        protected void StartOrient()
        {
            try
            {
                // switch off user controller's training mode:
                Universe.Instance.GetEntity<UserController>(UserController.ENTITY_NAME).EndTrainingMode();

                UserData.Instance.setUserName("ORIENT Test");
                UserData.Instance.setUserCode("te000");
                UserData.Instance.setAge(10);
                UserData.Instance.setSex('M');

                SessionInfo sessionInfo = new SessionInfo(
                    UserData.Instance.getUserName(), UserData.Instance.getUserCode(), OrientProfile.Instance().LogsDir);
                if ((sessionInfo.LogFileDir.Length > 0) && (sessionInfo.LogFileDir[sessionInfo.LogFileDir.Length - 1] != '\\'))
                {
                    sessionInfo.LogFileDir += "\\";
                }
                storyFacilitator.UserSessionInfo = sessionInfo;

                if ((sessionInfo.LogFileDir.Length > 0) && (!Directory.Exists(sessionInfo.LogFileDir)))
                {
                    try
                    {
                        Directory.CreateDirectory(sessionInfo.LogFileDir);
                    }
                    catch (Exception)
                    {
                        throw new StoryFacilitator.StoryFacilitatorException("Cannot access/create logs folder: " +
                                                                             sessionInfo.LogFileDir);
                    }
                }

                storyFacilitator.ReadScenario(OrientProfile.Instance().ScenarioLocation);
                storyFacilitator.Start();
                btUserData.Visible = false;
                User.Interface.UserInterface.Instance.updateSystemButtontoBusy();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btUserData_Click(object sender, EventArgs e)
        {
            this.StartOrient();
        }

        private void RenderWindow_Closing(object sender, CancelEventArgs e)
        {
            if (storyFacilitator.UserTimeTracker != null) //The UserTimeTracker is initialized in the start state
                storyFacilitator.UserTimeTracker.End();

            if (storyFacilitator.excelTimeTracker != null)
            {
                try
                {
                    storyFacilitator.excelTimeTracker.End();
                }
                catch (Exception ex)
                {
                    ApplicationLogger.Instance().WriteLine("RenderWindow_closing: NON CRITICAL ERROR: ExcelTimeTracker did not End() well: " + ex.Message);
                }
            }

            foreach (Entity entity in Universe.Instance.Entities)
            {
                if (!(entity is StoryFacilitator))
                {
                    entity.Destroy();
                    ApplicationLogger.Instance().WriteLine("Destroyed: " + entity.Name);
                }
            }
            try
            {
                Universe.Instance.Update();
                //The SF has to be the last entity to be removed, because it generates a StoryEvent every time an agent is destroyed,
                //indicating that he (the SF) has destroyed the entity.  
                storyFacilitator.Destroy();
                Universe.Instance.Update();
            }
            catch (Exception)
            {
                //Ignore
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            //code to quit application goes here
            Close();
        }

        // keyboard keys as debug shortcuts for user interface devices
        private void RenderWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (Universe.Instance.HasEntity<UserController>(UserController.ENTITY_NAME))
            {

                UserGestureAction userGestureAction =
                    Universe.Instance.GetEntity<UserController>(UserController.ENTITY_NAME).GetAction<UserGestureAction>(
                        UserGestureAction.ACTION_NAME);
                UserSpeechRecogAction userSpeechRecogAction =
                    Universe.Instance.GetEntity<UserController>(UserController.ENTITY_NAME).GetAction<UserSpeechRecogAction>
                        (UserSpeechRecogAction.ACTION_NAME);
                UserSelectItemAction userSelectItemAction =
                    Universe.Instance.GetEntity<UserController>(UserController.ENTITY_NAME).GetAction<UserSelectItemAction>(
                        UserSelectItemAction.ACTION_NAME);
                UserNavigationAction userNavigationAction =
                        Universe.Instance.GetEntity<UserController>(UserController.ENTITY_NAME).GetAction
                            <UserNavigationAction>(UserNavigationAction.ACTION_NAME);
                UserWiiButtonAction userWiiButtonAction =
                        Universe.Instance.GetEntity<UserController>(UserController.ENTITY_NAME).GetAction
                            <UserWiiButtonAction>(UserWiiButtonAction.ACTION_NAME);


                Dictionary<string, object> arguments = new Dictionary<string, object>();

                switch (e.KeyCode)
                {
                    // numeric keys are debug shortcuts for wii controls 
                    case Keys.D1: //shortcut for numeric key "1" - greet gesture
                        arguments.Add(UserGestureAction.GESTURE_NAME, UserGestureAction.GESTURE_GREETBACK); // name of the gesture goes here
                        if (!userGestureAction.IsRunning) userGestureAction.Start(new Arguments(arguments));
                        break;
                    case Keys.D2: //shortcut for numeric key "2" - eat gesture
                        arguments.Add(UserGestureAction.GESTURE_NAME, UserGestureAction.GESTURE_EATDRINK); // name of the gesture goes here
                        if (!userGestureAction.IsRunning) userGestureAction.Start(new Arguments(arguments));
                        break;
                    case Keys.D3: //shortcut for numeric key "3" - user pick gesture
                        arguments.Add(UserGestureAction.GESTURE_NAME, UserGestureAction.GESTURE_USERPICK); // name of the gesture goes here
                        if (!userGestureAction.IsRunning) userGestureAction.Start(new Arguments(arguments));
                        break;
                    case Keys.D4: //shortcut for numeric key "4" - user accept gesture
                        arguments.Add(UserGestureAction.GESTURE_NAME, UserGestureAction.GESTURE_ACCEPT); // name of the gesture goes here
                        if (!userGestureAction.IsRunning) userGestureAction.Start(new Arguments(arguments));
                        break;
                    case Keys.D5: //shortcut for numeric key "5" - user reject gesture
                        arguments.Add(UserGestureAction.GESTURE_NAME, UserGestureAction.GESTURE_REJECT); // name of the gesture goes here
                        if (!userGestureAction.IsRunning) userGestureAction.Start(new Arguments(arguments));
                        break;
                    case Keys.D7: //shortcut for numeric key "7" - ask questions (gardening or recycling)
                        arguments.Add(UserGestureAction.GESTURE_NAME, UserGestureAction.GESTURE_ASK); // name of the gesture goes here
                        if (!userGestureAction.IsRunning) userGestureAction.Start(new Arguments(arguments));
                        break;
                    case Keys.D8: //shortcut for numeric key "8" - user apologise gesture
                        arguments.Add(UserGestureAction.GESTURE_NAME, UserGestureAction.GESTURE_APOLOGISE); // name of the gesture goes here
                        if (!userGestureAction.IsRunning) userGestureAction.Start(new Arguments(arguments));
                        break;
                    case Keys.D9: //shortcut for numeric key "9" - user give gesture
                        arguments.Add(UserGestureAction.GESTURE_NAME, UserGestureAction.GESTURE_GIVE); // name of the gesture goes here
                        if (!userGestureAction.IsRunning) userGestureAction.Start(new Arguments(arguments));
                        break;

                    // Ctrl is Wii Button A

                    case Keys.ControlKey: // Ctrl is shortcut for Wii button "A" (launch user action)
                        arguments.Add(UserWiiButtonAction.BUTTON_NAME, UserWiiButtonAction.BUTTON_A); //name of button
                        if (!userWiiButtonAction.IsRunning) userWiiButtonAction.Start(new Arguments(arguments));
                        break;

                    // O and P keys are shortcuts for wii buttons 1 and 2
                    case Keys.O:
                        arguments.Add(UserWiiButtonAction.BUTTON_NAME, UserWiiButtonAction.BUTTON_1);
                        if (!userWiiButtonAction.IsRunning) userWiiButtonAction.Start(new Arguments(arguments));
                        break;

                    case Keys.P:
                        arguments.Add(UserWiiButtonAction.BUTTON_NAME, UserWiiButtonAction.BUTTON_2);
                        if (!userWiiButtonAction.IsRunning) userWiiButtonAction.Start(new Arguments(arguments));
                        break;

                    // L key is shortcut for wii button +
                    case Keys.L:
                        arguments.Add(UserWiiButtonAction.BUTTON_NAME, UserWiiButtonAction.BUTTON_PLUS);
                        if (!userWiiButtonAction.IsRunning) userWiiButtonAction.Start(new Arguments(arguments));
                        break;

                    // Function keys are used for mobile phone actions

                    case Keys.F1: // F1 is shortcut for SpeechRecognition "Abbuk"
                        string [] charArray = new string [1];
                        charArray[0] = "Abbuk"; //name of character
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME_ARRAY, charArray);
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME, charArray[0]); 
                        if (!userSpeechRecogAction.IsRunning) userSpeechRecogAction.Start(new Arguments(arguments));
                        break;
                    case Keys.F2: // F2 is shortcut for SpeechRecognition "Errep"
                        charArray = new string[1];
                        charArray[0] = "Errep"; //name of character
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME_ARRAY, charArray);
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME, charArray[0]); 
                        if (!userSpeechRecogAction.IsRunning) userSpeechRecogAction.Start(new Arguments(arguments));
                        break;
                    case Keys.F3: // F3 is shortcut for SpeechRecognition "Evui"
                        charArray = new string[1];
                        charArray[0] = "Evui"; //name of character
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME_ARRAY, charArray);
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME, charArray[0]); 
                        if (!userSpeechRecogAction.IsRunning) userSpeechRecogAction.Start(new Arguments(arguments));
                        break;
                    case Keys.F4: // F4 is shortcut for SpeechRecognition "Ikop"
                        charArray = new string[1];
                        charArray[0] = "Ikop"; //name of character
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME_ARRAY, charArray);
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME, charArray[0]); 
                        if (!userSpeechRecogAction.IsRunning) userSpeechRecogAction.Start(new Arguments(arguments));
                        break;
                    case Keys.F5: // F5 is shortcut for SpeechRecognition "Ardoot"
                        charArray = new string[1];
                        charArray[0] = "Ardoot"; //name of character
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME_ARRAY, charArray);
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME, charArray[0]); 
                        if (!userSpeechRecogAction.IsRunning) userSpeechRecogAction.Start(new Arguments(arguments));
                        break;
                    case Keys.F6: // F6 is shortcut for SpeechRecognition "Cerkith"
                        charArray = new string[1];
                        charArray[0] = "Cerkith"; //name of character
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME_ARRAY, charArray);
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME, charArray[0]); 
                        if (!userSpeechRecogAction.IsRunning) userSpeechRecogAction.Start(new Arguments(arguments));
                        break;
                    case Keys.F7: // F7 is shortcut for SpeechRecognition "Tigrit"
                        charArray = new string[1];
                        charArray[0] = "Tigrit"; //name of character
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME_ARRAY, charArray);
                        arguments.Add(UserSpeechRecogAction.CHAR_NAME, charArray[0]); 
                        if (!userSpeechRecogAction.IsRunning) userSpeechRecogAction.Start(new Arguments(arguments));
                        break;


                    case Keys.F8: // F8 is shortcut for Item Selection "meal"
                        arguments.Add(UserSelectItemAction.ITEM_NAME, "meal"); // name of the gesture goes here
                        if (!userSelectItemAction.IsRunning) userSelectItemAction.Start(new Arguments(arguments));
                        break;
                    case Keys.F9: // F9 is shortcut for Item Selection "gardening"
                        arguments.Add(UserSelectItemAction.ITEM_NAME, "gardening"); //name of item
                        if (!userSelectItemAction.IsRunning) userSelectItemAction.Start(new Arguments(arguments));
                        break;                               
                    case Keys.F11: // F11 is shortcut for Item Selection "recycling"
                        arguments.Add(UserSelectItemAction.ITEM_NAME, "recycling"); //name of item
                        if (!userSelectItemAction.IsRunning) userSelectItemAction.Start(new Arguments(arguments));
                        break;
                    case Keys.F12: // F12 is shortcut for Item Selection "drink"
                        arguments.Add(UserSelectItemAction.ITEM_NAME, "drink"); //name of item
                        if (!userSelectItemAction.IsRunning) userSelectItemAction.Start(new Arguments(arguments));
                        break;
                    

                    case Keys.T: // T is shortcut for Item Selection "seedpod"
                        arguments.Add(UserSelectItemAction.ITEM_NAME, "seedpod"); //name of item
                        if (!userSelectItemAction.IsRunning) userSelectItemAction.Start(new Arguments(arguments));
                        break;      
                    case Keys.Y: // Y is shortcut for Item Selection "soil"
                        arguments.Add(UserSelectItemAction.ITEM_NAME, "soil"); //name of item
                        if (!userSelectItemAction.IsRunning) userSelectItemAction.Start(new Arguments(arguments));
                        break;
                    case Keys.U: // U is shortcut for Item Selection "greenDrink"
                        arguments.Add(UserSelectItemAction.ITEM_NAME, "greenDrink"); //name of item
                        if (!userSelectItemAction.IsRunning) userSelectItemAction.Start(new Arguments(arguments));
                        break;
                    case Keys.H: // H is shortcut for Item Selection "heal"
                        arguments.Add(UserSelectItemAction.ITEM_NAME, "heal"); //name of item
                        if (!userSelectItemAction.IsRunning) userSelectItemAction.Start(new Arguments(arguments));
                        break;
                
                    // the circle of 8 keys going clockwise from w to q is used as a debug shortcut for user direction input (originally through dance mat)    
                    case Keys.W:
                        arguments.Add(UserNavigationAction.DIRECTION, UserNavigationAction.DIR_F);
                        // direction code goes here
                        if (!userNavigationAction.IsRunning) userNavigationAction.Start(new Arguments(arguments));
                        break;
                    case Keys.E:
                        arguments.Add(UserNavigationAction.DIRECTION, UserNavigationAction.DIR_FR);
                        // direction code goes here
                        if (!userNavigationAction.IsRunning) userNavigationAction.Start(new Arguments(arguments));
                        break;
                    case Keys.D:
                        arguments.Add(UserNavigationAction.DIRECTION, UserNavigationAction.DIR_R);
                        // direction code goes here
                        if (!userNavigationAction.IsRunning) userNavigationAction.Start(new Arguments(arguments));
                        break;
                    case Keys.C:
                        arguments.Add(UserNavigationAction.DIRECTION, UserNavigationAction.DIR_BR);
                        // direction code goes here
                        if (!userNavigationAction.IsRunning) userNavigationAction.Start(new Arguments(arguments));
                        break;
                    case Keys.X:
                        arguments.Add(UserNavigationAction.DIRECTION, UserNavigationAction.DIR_B);
                        // direction code goes here
                        if (!userNavigationAction.IsRunning) userNavigationAction.Start(new Arguments(arguments));
                        break;
                    case Keys.Z:
                        arguments.Add(UserNavigationAction.DIRECTION, UserNavigationAction.DIR_BL);
                        // direction code goes here
                        if (!userNavigationAction.IsRunning) userNavigationAction.Start(new Arguments(arguments));
                        break;
                    case Keys.A:
                        arguments.Add(UserNavigationAction.DIRECTION, UserNavigationAction.DIR_L);
                        // direction code goes here
                        if (!userNavigationAction.IsRunning) userNavigationAction.Start(new Arguments(arguments));
                        break;
                    case Keys.Q:
                        arguments.Add(UserNavigationAction.DIRECTION, UserNavigationAction.DIR_FL);
                        // direction code goes here
                        if (!userNavigationAction.IsRunning) userNavigationAction.Start(new Arguments(arguments));
                        break;

                    // the s key in the middle of the circle is a debug shortcut for the dancemat start key
                    case Keys.S:
                        arguments.Add(UserNavigationAction.DIRECTION, UserNavigationAction.START_BTN);
                        // direction code goes here
                        if (!userNavigationAction.IsRunning) userNavigationAction.Start(new Arguments(arguments));
                        break;

                }
            }
        }
    }
}
