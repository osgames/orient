using System.Windows.Forms;

namespace ORIENT_UI
{
    partial class RenderWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.btUserData = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(729, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(38, 38);
            this.btnExit.TabIndex = 31;
            this.btnExit.TabStop = false;
            this.btnExit.Text = "X";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btUserData
            // 
            this.btUserData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btUserData.BackColor = System.Drawing.Color.White;
            this.btUserData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btUserData.FlatAppearance.BorderSize = 0;
            this.btUserData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btUserData.Location = new System.Drawing.Point(729, 56);
            this.btUserData.Name = "btUserData";
            this.btUserData.Size = new System.Drawing.Size(38, 38);
            this.btUserData.TabIndex = 3;
            this.btUserData.TabStop = false;
            this.btUserData.Text = "GO";
            this.btUserData.UseVisualStyleBackColor = false;
            this.btUserData.Click += new System.EventHandler(this.btUserData_Click);
            // 
            // RenderWindow
            // 
            this.ClientSize = new System.Drawing.Size(779, 532);
            this.Controls.Add(this.btUserData);
            this.Controls.Add(this.btnExit);
            this.Name = "RenderWindow";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.RenderWindow_Closing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RenderWindow_KeyDown);
            this.Controls.SetChildIndex(this.btnExit, 0);
            this.Controls.SetChildIndex(this.btUserData, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private Button btUserData;
        private Button btnExit;
    }
}