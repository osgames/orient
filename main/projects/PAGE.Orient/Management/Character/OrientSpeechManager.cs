/* ****************************************************
 * Name: OgreSpeechManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/04 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using PAGE.OGRE.Management.Character;
using PAGE.Orient.Domain.Assets;
using PAGE.Orient.Resources;
using PAGE.Orient.Util;
using PAGE.Util;
using WMPLib;

namespace PAGE.Orient.Management.Character
{
    /// <summary>
    /// Orient speech manager uses facial animation and lip-flap to represent
    /// a character speaking. Can use giberish or other pre-defined audio.
    /// </summary>
    public class OrientSpeechManager : OgreSpeechManager
    {
        #region Fields

        // constants
        protected const ushort MINIMAL_DURATION = 2000;
        protected const ushort MILLISECONDS_PER_WORD = 500;
        protected const float FADE_IN_TIME = 0.25f;
        protected const float FADE_OUT_TIME = 0.09f;
        protected const float MAXIMAL_WEIGHT = 0.5f;
        protected const string AUDIO_FILE_EXT = "wav";

        //phoneme change variables
        protected int numWords = 10;
        protected int modifyTime;
        protected DateTime modifyStart = DateTime.Now;
        protected DateTime speechStart = DateTime.Now;
        protected List<string> phonemePoses = new List<string>();
        protected string currentPhonemePose;

        //timer used to play silence
        protected Timer silenceTimer;

        //time seconds per word heuristic
        protected Random random = new Random(DateTime.Now.Millisecond);

        //player used for audio playback
        protected WindowsMediaPlayer audioPlayer = new WindowsMediaPlayer();

        //the resources manager containing gibberish and phoneme config
        protected OrientResourcesManager resourcesManager;

        //used to calculate index key for utterances
        protected string magicWordsRegexPattern;

        #endregion

        #region Properties

        public new OrientCharacter Character
        {
            get { return base.Character as OrientCharacter; }
        }

        #endregion

        #region Constructors

        public OrientSpeechManager(
            OrientCharacter character, OrientResourcesManager resourcesManager)
            : base(character)
        {
            this.resourcesManager = resourcesManager;

            //creates timer for failure cases
            TimerCallback timerDelegate = this.TimerReachedEnd;
            this.silenceTimer = new Timer(
                timerDelegate, null, Timeout.Infinite, Timeout.Infinite);

            //checks phoneme poses and adds them to list
            foreach (KeyValuePair kvp in this.Character.ResourceAsset.Phonemes)
                this.phonemePoses.Add(kvp.value);

            //calculate magic words reg ex pattern
            StringArrayList magicWords = this.resourcesManager.VoicesConfig.MagicWords;
            this.magicWordsRegexPattern = "(";
            foreach (string word in magicWords)
            {
                this.magicWordsRegexPattern += word;
                if (!word.Equals(magicWords[magicWords.Count - 1]))
                    this.magicWordsRegexPattern += "|";
            }
            this.magicWordsRegexPattern += ")+";

            //audio player handler
            this.audioPlayer.PlayStateChange += AudioPlayer_PlayStateChange;
        }

        #endregion

        #region Public Methods

        public override void Update(float millisSinceLastUpdate)
        {
            base.Update(millisSinceLastUpdate);

            if (this.isTalking)
            {
                //verifies phoneme change
                TimeSpan span = DateTime.Now.Subtract(this.modifyStart);
                if ((span.TotalMilliseconds >= this.modifyTime) ||
                    this.RandomPhonemeChange(span.TotalMilliseconds))
                {
                    //gets new phoneme pose
                    string phonemePose =
                        this.phonemePoses[random.Next(0, this.phonemePoses.Count - 1)];

                    //resets old and sets new one
                    if (this.currentPhonemePose != null)
                    {
                        this.Character.ExpressionManager.ChangeExpression(
                            this.currentPhonemePose, 0, FADE_OUT_TIME);
                    }
                    this.Character.ExpressionManager.ChangeExpression(phonemePose, MAXIMAL_WEIGHT, FADE_IN_TIME);
                    this.currentPhonemePose = phonemePose;

                    this.modifyStart = DateTime.Now;
                }

                this.speechTimeLeft = this.speechDuration - (float)span.TotalSeconds;
            }
        }

        public virtual void ExecuteAction(IActionCallback actionCallback, string actionName, string utterance,
                                          bool gibberish)
        {
            base.ExecuteAction(actionCallback, actionName);
            this.StartSpeech(utterance, gibberish);
        }

        public override void ExecuteAction(IActionCallback actionCallback, string actionName, string utterance)
        {
            this.ExecuteAction(callback, actionName, utterance, false);
        }

        public override void StartSpeech(string utterance)
        {
            this.StartSpeech(utterance, false);
        }

        /// <summary>
        /// Starts the speech playback.
        /// </summary>
        /// <param name="utterance">the utterance to be spoken.</param>
        /// <param name="gibberish">whether or not to generate gibberish.</param>
        public virtual void StartSpeech(string utterance, bool gibberish)
        {
            //calculates phoneme change time
            this.numWords = Math.Max(2, utterance.Split(new char[] {' '}).GetLength(0));

            if (gibberish && !string.IsNullOrEmpty(this.Character.ResourceAsset.VoiceID))
            {
                //gets best gibberish audio file and play it
                if (!this.PlayAudioFile(this.GetGibberishFile(utterance)))
                    //starts speech timer (silence)
                    this.StartSpeechTimer();
            }
            else
            {
                //starts speech timer (silence)
                this.StartSpeechTimer();
            }

            //phoneme change variables
            this.speechStart = this.modifyStart = DateTime.Now;
            this.modifyTime = 500;
            this.isTalking = true;
        }

        public override void Stop()
        {
            base.Stop();

            //stops audio playback thread
            if (this.audioPlayer != null)
            {
                try
                {
                    this.audioPlayer.controls.stop();
                }
                finally
                {
                    this.audioPlayer.close();
                }
            }

            //resets character face
            if (this.currentPhonemePose != null)
                this.Character.ExpressionManager.ChangeExpression(
                    this.currentPhonemePose, 0, FADE_OUT_TIME);
            this.currentPhonemePose = null;
        }

        public override void Dispose()
        {
            this.audioPlayer.close();
            this.Stop();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Calculates the probability of occuring a phoneme change.
        /// </summary>
        /// <param name="milliseconds">the number of milliseconds since
        /// the last phoneme change.</param>
        /// <returns>true if phonemes is to be changed, false otherwise.</returns>
        protected bool RandomPhonemeChange(double milliseconds)
        {
            return (random.Next(1) == 0) && (milliseconds >= this.modifyTime*0.5);
        }

        #region Utterance Index Key Methods

        /// <summary>
        /// Computes an index key based on the given string. The operation involves
        /// deleting all spaces, apostrophes and digits from the given utterance, 
        /// capitalizing all magic words and replacing all other characters with '0's.
        /// </summary>
        /// <param name="utterance">the utterance to compute the index key.</param>
        /// <returns>a string representing an index key for sentence comparison.</returns>
        protected string ComputeIndexKey(string utterance)
        {
            //1 - delete all spaces, apostrophes and digits
            string noDigitsSpaces = Regex.Replace(
                utterance, @"(\d|\s|')+", "");

            //2 - replace magic words with '@'s
            string noNamesZeros = Regex.Replace(noDigitsSpaces, this.magicWordsRegexPattern,
                new MatchEvaluator(ReplaceWithAt), RegexOptions.IgnoreCase);

            //3 - replace all other characters with '0's
            noNamesZeros = Regex.Replace(noNamesZeros, "\\w", "0", 
                RegexOptions.IgnoreCase);

            //4 - replace '@'s with magic words back again
            string indexKey = ReplaceWithNamesCap(noNamesZeros, noDigitsSpaces);

            return indexKey;
        }

        /// <summary>
        /// Replaces all the characters from the given string with @'s.
        /// </summary>
        /// <param name="m">the regex match with the string to replace.</param>
        /// <returns>a string containing @'s with same lenght as the given one.</returns>
        protected static string ReplaceWithAt(Match m)
        {
            return new string('@', m.ToString().Length);
        }

        /// <summary>
        /// Replaces all @ characters from the first string with the respective 
        /// characters taken from the second one.
        /// </summary>
        /// <param name="noNamesZeros">the string containing the @ characters
        /// to be replaced.</param>
        /// <param name="names">the string containing the characters to replace
        /// the @'s from the first string.</param>
        /// <returns>a new string containing the characters of the first given 
        /// string with @ characters replaced with the respective capitalized 
        /// characters taken from the second one.</returns>
        protected static string ReplaceWithNamesCap(string noNamesZeros, string names)
        {
            StringBuilder capNamesZeros = new StringBuilder(noNamesZeros);
            for (int i = 0; i < capNamesZeros.Length; i++)
            {
                if (capNamesZeros[i] == '@')
                    capNamesZeros[i] = char.ToUpper(names[i]);
            }
            return capNamesZeros.ToString();
        } 

        /// <summary>
        /// Returns the name of the gibberish audio file corresponding to the given 
        /// utterance id and to the character's voice configuration.
        /// </summary>
        /// <param name="utteranceID">the id number of the selected utterance.</param>
        /// <returns>the path to the gibberish audio file corresponding to the given 
        /// utterance id.</returns>
        protected string GetAudioFilePath(string utteranceID)
        {
            return Path.GetFullPath(
                Path.GetDirectoryName(this.resourcesManager.ResourcesConfig.VoicesFile) +
                "\\" + this.resourcesManager.VoicesConfig.Voices[this.Character.ResourceAsset.VoiceID]) +
                "\\utt-" + utteranceID + "-" + this.Character.ResourceAsset.VoiceID +
                "." + AUDIO_FILE_EXT;
        }

        #endregion

        #region Audio Methods

        /// <summary>
        /// Returns the path to the gibberish audio file that best matches the 
        /// given utterance.
        /// </summary>
        /// <param name="utterance">the utterance to search for the gibberish.</param>
        /// <returns>the path to the gibberish audio file that best matches the 
        /// given utterance.</returns>
        protected string GetGibberishFile(string utterance)
        {
            //checks if utterance was already computed
            if(this.resourcesManager.VoicesConfig.IndexedAudioFiles.ContainsKey(utterance))
            {
                return this.GetAudioFilePath(this.resourcesManager.VoicesConfig.
                    IndexedAudioFiles[utterance].UtteranceID);
            }

            //if not, compute index key from utterance
            string indexKey = this.ComputeIndexKey(utterance);

            //compare Levenshtein edit distance between all stored keys
            int shortestEditDistance = int.MaxValue;
            string bestUtteranceID = "";

            foreach(VoicesConfig.UtteranceIndexKeyPair uikp in 
                this.resourcesManager.VoicesConfig.IndexedAudioFiles.Values)
            {
                //compute edit distance between the keys
                int editDistance = TextMath.EditDistance.YetiLevenshtein(
                    indexKey, uikp.IndexKey);

                if(editDistance < shortestEditDistance)
                {
                    //shorter distance found
                    shortestEditDistance = editDistance;
                    bestUtteranceID = uikp.UtteranceID;
                }
            }

            return this.GetAudioFilePath(bestUtteranceID);
        }

        /// <summary>
        /// Plays the given audio file.
        /// </summary>
        /// <param name="fileName">the name of the audio file to play.</param>
        /// <returns>true if the given file exists, false otherwise.</returns>
        protected bool PlayAudioFile(string fileName)
        {
            //file to play doesn't exist
            if ((fileName == null) || !File.Exists(fileName)) return false;

            //plays syntethized audio file in another thread
            this.audioPlayer.URL = fileName;
            return true;
        }

        protected void AudioPlayer_PlayStateChange(int NewState)
        {
            //checks if play ended
            if (NewState == (int)WMPPlayState.wmppsStopped)
                this.SpeechPlaybackEnded();
        }

        /// <summary>
        /// Starts the timer corresponding to the speech of the current utterance.
        /// </summary>
        protected void StartSpeechTimer()
        {
            //start speech timer
            long speechduration = MINIMAL_DURATION + (this.numWords*MILLISECONDS_PER_WORD);
            this.silenceTimer.Change(speechduration, Timeout.Infinite);
            this.speechDuration = this.speechTimeLeft = speechduration*0.001f;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Speech timer end handler.
        /// </summary>
        /// <param name="stateInfo">the object containing the state info.</param>
        protected void TimerReachedEnd(Object stateInfo)
        {
            this.SpeechPlaybackEnded();

            //disables timer
            this.silenceTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        /// <summary>
        /// Called when speech is finished. Sends success and stops manager.
        /// </summary>
        protected void SpeechPlaybackEnded()
        {
            //done talking
            this.speechDuration = this.speechTimeLeft = 0;
            this.ActionSuccess();
            this.Stop();
        }

        #endregion

        #endregion
    }
}