/* ****************************************************
 * Name: OrientExpressionManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/23
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections.Generic;
using Mogre;
using PAGE.Generic.Management.Character;
using PAGE.Orient.Domain.Assets;
using PAGE.Orient.Resources.Assets;

namespace PAGE.Orient.Management.Character
{
    /// <summary>
    /// Expression manager manages the characters facial expressions throughout time
    /// balancing the weight associated with each expression pose. It is also responsible
    /// to make the character's eyes blink once in a while.
    /// </summary>
    public class OrientExpressionManager : CharacterManager
    {
        #region Expression Transition

        /// <summary>
        /// Struct representing an expression transtion from one value to another.
        /// </summary>
        protected struct ExpressionTransition
        {
            /// <summary>
            /// The name of the expression.
            /// </summary>
            public string expressionName;

            /// <summary>
            /// The final weight to put the expression in.
            /// </summary>
            public float finalWeight;

            /// <summary>
            /// The transition speed (weight per milliseconds).
            /// </summary>
            public float transitionSpeed;

            /// <summary>
            /// The event that tells when the expression transition ends.
            /// </summary>
            public event EventHandler TransitionEnded;

            public ExpressionTransition(
                string expressionName, float finalWeight, float transitionSpeed)
            {
                this.expressionName = expressionName;
                this.finalWeight = finalWeight;
                this.transitionSpeed = transitionSpeed;
                TransitionEnded = null;
            }

            public void OnTransitionEnded()
            {
                if (this.TransitionEnded != null)
                    this.TransitionEnded(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Fields

        //blink interval
        protected const ushort MAX_BLINK_INTERVAL_MILLIS = 7000;
        protected const ushort MIN_BLINK_INTERVAL_MILLIS = 1500;
        protected const ushort BLINK_TIME_MILLIS = 90;
        protected const string EYES_CLOSED_EXPR_NAME = "eyes-closed";

        //used to determine blink instant
        protected Random random = new Random(DateTime.Now.Millisecond);
        protected float millisSinceLastBlink;

        //manual pose variables
        protected AnimationState manualAnimState;
        protected VertexPoseKeyFrame manualKeyFrame;

        //the pose indexes indexed by expression name (expression - pose index)
        protected Dictionary<string, ushort> poseIndexes = new Dictionary<string, ushort>();

        //the active expressions transitions list
        protected List<ExpressionTransition> activeTransitions = new List<ExpressionTransition>();

        #endregion

        #region Properties

        public new OrientCharacter Character
        {
            get { return base.Character as OrientCharacter; }
        }

        #endregion

        #region Constructor

        public OrientExpressionManager(
            OrientCharacter character, VertexPoseKeyFrame manualKeyFrame)
            : base(character)
        {
            this.manualKeyFrame = manualKeyFrame;

            if (this.Character.Entity == null) return;

            //gets manual animation state
            this.manualAnimState = this.Character.Entity.GetAnimationState(
                OrientResourceCharacter.MANUAL_FACE_ANIM_NAME);
            this.manualAnimState.TimePosition = 0;

            //builds poses index dictionary based on existent expressions and phonemes
            for (ushort i = 0; i < this.Character.Entity.GetMesh().PoseCount; i++)
            {
                string poseName = this.Character.Entity.GetMesh().GetPose(i).Name;
                if (this.Character.ResourceAsset.FacialExpressions.Contains(poseName))
                    this.poseIndexes.Add(this.Character.ResourceAsset.FacialExpressions[poseName], i);
                else if (this.Character.ResourceAsset.Phonemes.Contains(poseName))
                    this.poseIndexes.Add(this.Character.ResourceAsset.Phonemes[poseName], i);
            }

            this.manualAnimState.Enabled = true;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Changes the character's current facial expression. The facial pose will be 
        /// added to the current ones if weight is greater than 0, or reset if weight is 0.
        /// </summary>
        /// <param name="expressionName">the name of the expression to change the weigth to.</param>
        /// <param name="weight">the weight to change the given facial expression.</param>
        /// <param name="transitionTime">the time in seconds for the expression weight 
        /// transition.</param>
        public virtual void ChangeExpression(string expressionName, float weight, float transitionTime)
        {
            if (!this.poseIndexes.ContainsKey(expressionName)) return;

            //gets weight difference and speed for given expression
            float weightDiff = weight - this.manualKeyFrame.GetPoseReferences()
                                            [this.poseIndexes[expressionName]].Influence;

            float speed = weightDiff/(transitionTime*1000);

            //adds to active transition list
            ExpressionTransition transition = new ExpressionTransition(expressionName, weight, speed);
            transition.TransitionEnded += TransitionEnded;
            this.activeTransitions.Add(transition);
        }

        /// <summary>
        /// Resets all available expressions, sets its weights to 0.
        /// </summary>
        public virtual void ResetAllExpressions()
        {
            foreach (string expression in this.poseIndexes.Keys)
            {
                this.manualKeyFrame.UpdatePoseReference(
                    this.poseIndexes[expression], 0);
                this.manualAnimState.Parent._notifyDirty();
            }
        }

        public override void Update(float millisSinceLastUpdate)
        {
            base.Update(millisSinceLastUpdate);

            //updates expressions and eyes blink
            this.UpdateExpressionTransitions(millisSinceLastUpdate);
            //this.UpdateEyeBlink(millisSinceLastUpdate);
        }

        public override void Stop()
        {
            this.manualAnimState.Enabled = false;
        }

        public override void Dispose()
        {
        }

        #endregion

        #region Protected Methods

        protected void UpdateExpressionTransitions(float millisSinceLastUpdate)
        {
            foreach (ExpressionTransition transition in
                new List<ExpressionTransition>(this.activeTransitions))
            {
                //updates weight for expression
                float weight = this.manualKeyFrame.GetPoseReferences()
                    [this.poseIndexes[transition.expressionName]].Influence;
                weight += millisSinceLastUpdate*transition.transitionSpeed;

                //checks transition end
                if (((transition.transitionSpeed >= 0) && (weight >= transition.finalWeight)) ||
                    ((transition.transitionSpeed < 0) && (weight <= transition.finalWeight)))
                {
                    weight = transition.finalWeight;
                    this.activeTransitions.Remove(transition);
                    transition.OnTransitionEnded();
                }

                //updates expression
                this.manualKeyFrame.UpdatePoseReference(
                    this.poseIndexes[transition.expressionName], weight);
                this.manualAnimState.Parent._notifyDirty();
            }
        }

        protected void UpdateEyeBlink(float millisSinceLastUpdate)
        {
            this.millisSinceLastBlink += millisSinceLastUpdate;

            //if max time interval was reached or blink chance happens
            if ((this.millisSinceLastBlink >= MAX_BLINK_INTERVAL_MILLIS) ||
                this.BlinkChance())
            {
                //starts expression eye-close
                this.ChangeExpression(EYES_CLOSED_EXPR_NAME, 1, BLINK_TIME_MILLIS/1000f);
                this.millisSinceLastBlink = 0;
            }
        }

        protected bool BlinkChance()
        {
            return
                (this.millisSinceLastBlink >= MIN_BLINK_INTERVAL_MILLIS) &&
                (this.random.Next(15) == 0);
        }

        protected void TransitionEnded(object sender, EventArgs e)
        {
            ExpressionTransition transition = (ExpressionTransition) sender;
            if ((transition.expressionName == EYES_CLOSED_EXPR_NAME) &&
                (transition.finalWeight == 1))
            {
                //starts expression eye-open
                this.ChangeExpression(EYES_CLOSED_EXPR_NAME, 0, BLINK_TIME_MILLIS/1000f);
            }
        }

        #endregion
    }
}