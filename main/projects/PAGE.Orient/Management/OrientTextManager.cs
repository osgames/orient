/* ****************************************************
 * Name: OrientTextManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/04 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.GUI;
using PAGE.Generic.Management;
using PAGE.OGRE.Domain.Assets;
using PAGE.Orient.Domain.Cameras;
using PAGE.Orient.GUI;

namespace PAGE.Orient.Management
{
    public class OrientTextManager : TextManager
    {
        #region Fields

        protected OrientFollowAssetText nameText;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the text associated with the name of the asset.
        /// </summary>
        public IFollowAssetText NameText
        {
            get { return nameText; }
        }

        public override bool Visible
        {
            set
            {
                this.mainText.Visible = this.nameText.Visible = value; 
            }
        }

        #endregion

        #region Constructor

        public OrientTextManager(IOgreInteractionAsset asset, OrientCamera camera)
            : base(asset)
        {
            this.mainText = new OrientFollowAssetText(asset, camera);
            this.nameText = new OrientFollowAssetText(asset, camera);
        }

        #endregion

        #region Public Methods

        public override void Update(float millisSinceLastUpdate)
        {
            base.Update(millisSinceLastUpdate);
            if (this.nameText != null) this.nameText.Update(millisSinceLastUpdate);
        }

        public override void Stop()
        {
            this.MainText.Visible = false;
        }

        public override void Dispose()
        {
            //do nothing
        }

        #endregion
    }
}