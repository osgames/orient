using System;
using System.Collections.Generic;
using System.Text;
using PAGE.Generic.Domain.Cameras;
using PAGE.Generic.Domain;
using PAGE.OGRE.Math;
using PAGE.Orient.Domain.Cameras;


namespace PAGE.Orient.Domain.Cameras
{
    public class FirstPersonNoFreeLookAlgorithm : TargetAlgorithm
    {

        FirstPersonAlgorithm firstPersonAlgorithm;

        public FirstPersonNoFreeLookAlgorithm(GenericCamera camera, IGraphicalElement targetElement)
            :base(camera)
        {
            this.target = targetElement;
            firstPersonAlgorithm = new FirstPersonAlgorithm(camera);
        }

        public FirstPersonNoFreeLookAlgorithm(GenericCamera camera)
            : this(camera, null)
        {
        }

        #region Public Methods

        public override void Update(float millisSinceLastUpdate)
        {
            firstPersonAlgorithm.Update(millisSinceLastUpdate);
            
            //follows target
            if (this.Target != null)
            {
                // stop any rotations that might be going on and send user to nextDestination if he is not there yet
                OrientCamera orientCam = (OrientCamera)camera;
                orientCam.newFacingAngle = 0.00f;
                if (orientCam.nextDestination != null)
                {
                  orientCam.currentWayPoint = orientCam.nextDestination;
                  orientCam.nextDestination = null;
                  orientCam.AbsolutePosition = orientCam.addFirstPersonHeightOffset(orientCam.currentWayPoint.AbsolutePosition);
                }
                
                // for first person camera ignore offset (just set y component to camera height)
                OgrePosition targetPos = (OgrePosition) this.target.AbsolutePosition;
                OgrePosition camPos = (OgrePosition) orientCam.AbsolutePosition;
                targetPos.Y = camPos.Y;
                orientCam.LookAt(targetPos);
                this.Target = null;

            }

        }

        #endregion    
    
    }


}
