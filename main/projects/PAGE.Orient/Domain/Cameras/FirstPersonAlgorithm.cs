using System;
using System.Collections.Generic;
using System.Text;
using PAGE.Generic.Domain.Cameras;
using PAGE.OGRE.Math;
using PAGE.Generic.Math;

namespace PAGE.Orient.Domain.Cameras
{
    class FirstPersonAlgorithm : CameraAlgorithm
    {

        private OrientCamera orientCam;

        #region Constructors

        public FirstPersonAlgorithm(GenericCamera camera)
            : base(camera)
        {
         orientCam = (OrientCamera) camera;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// rotates a vector by the given angle clockwise in the given direction around the origin (0,0)
        /// of the XZ plane and returns the result of the rotation
        /// </summary>
        /// <param name="dir_in">the vector to rotate. It is assumed that its y component is 0.</param>
        /// <param name="angle_in">how many degrees to rotate in radians</param>
        private OgreDirection rotateVectorXZPlaneClockWise(OgreDirection dir_in, float angle_in)
        {
            OgreDirection movingDirection = new OgreDirection();
            movingDirection.Y = 0;
            movingDirection.X = dir_in.X * (float)Math.Cos(angle_in) - dir_in.Z * (float) Math.Sin(angle_in);
            movingDirection.Z = dir_in.X * (float)Math.Sin(angle_in) + dir_in.Z * (float) Math.Cos(angle_in);
            movingDirection.Vector3.Normalise();
            return movingDirection;
        }

        public override void Update(float millisSinceLastUpdate)
        {
            if ((orientCam.nextDestination != null) && (orientCam.newFacingAngle == 0.0f))
            {   
                
                float distToTarget = (float)(OgreDistance) orientCam.AbsolutePosition.DistanceTo(orientCam.addFirstPersonHeightOffset(orientCam.nextDestination.AbsolutePosition));
                float distMoveThisUpdate = millisSinceLastUpdate * OrientCamera.userSpeed;

                if (distMoveThisUpdate >= distToTarget) // target reached
                {
                    orientCam.currentWayPoint = orientCam.nextDestination;
                    orientCam.nextDestination = null;
                    orientCam.AbsolutePosition = orientCam.addFirstPersonHeightOffset(orientCam.currentWayPoint.AbsolutePosition);
                }
                else  // target not reached yet
                {
                    OgreDirection moveVec = (OgreDirection)orientCam.AbsolutePosition.DirectionTo(orientCam.addFirstPersonHeightOffset(orientCam.nextDestination.AbsolutePosition));
                    moveVec.Vector3.Normalise();
                    //multiply by length moved this update
                    moveVec.X = moveVec.X * distMoveThisUpdate;
                    moveVec.Y = moveVec.Y * distMoveThisUpdate;
                    moveVec.Z = moveVec.Z * distMoveThisUpdate;
                    OgrePosition movePosOffset = new OgrePosition(moveVec.Vector3);
                    orientCam.AbsolutePosition = orientCam.AbsolutePosition.Add(movePosOffset);
                
                }
            }

            if (orientCam.newFacingAngle != 0.0f)
            {
              float rotThisUpdate = OrientCamera.userRotationSpeed * millisSinceLastUpdate;
              if (orientCam.newFacingAngle > 0.0f) // clockwise rotation
              {
                  if (rotThisUpdate >= orientCam.newFacingAngle)
                  { //rotation is finished
                      orientCam.Yaw(-orientCam.newFacingAngle*180/Math.PI);
                      orientCam.newFacingAngle = 0.0f;
                  }
                  else 
                  { //rotation is not finished
                      orientCam.Yaw(-rotThisUpdate * 180 / Math.PI);
                      orientCam.newFacingAngle -= rotThisUpdate;
                  }

              } else // counterclockwise rotation
              {
                  rotThisUpdate *= -1;
                  if (rotThisUpdate <= orientCam.newFacingAngle)
                  { //rotation is finished
                      orientCam.Yaw(-orientCam.newFacingAngle * 180 / Math.PI);
                      orientCam.newFacingAngle = 0.0f;                    
                  }
                  else
                  { //rotation is not finished
                      orientCam.Yaw(-rotThisUpdate * 180 / Math.PI);
                      orientCam.newFacingAngle -= rotThisUpdate;
                  }               
              }
                
             
            
            }

        }

        #endregion
    }


}
