/* ****************************************************
 * Name: FearNotCamera.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using Mogre;
using PAGE.Generic.Domain;
using PAGE.Generic.Domain.Cameras;
using PAGE.OGRE.Domain.Assets;
using PAGE.OGRE.Domain.Cameras;
using PAGE.OGRE.Math;
using PAGE.Orient.GUI.Windows.Forms.Controls;
using PAGE.Generic.Domain.Spots;
using PAGE.Orient.Domain.Assets;
using PAGE.Generic.Math;

namespace PAGE.Orient.Domain.Cameras
{
    public enum CameraMode { DynamicTargeted, Free, StaticTargeted, Static, FirstPersonNoFreeLook, FirstPersonFreeLook }

    public class OrientCamera : OgreCamera
    {

        #region constants
        public const float userHeight = 20f; // height of the camera in first person mode (will be recalculated in each set, according to set scale) 
        public const float userSpeed = 0.05f; // speed of the camera in first person mode (will be recalculated in each set according to set scale)
        public const float userRotationSpeed = 0.001f; // rotation speed of the camera in first person mode 
        #endregion

        #region Fields

        //spots tags parameters
        protected string faceCameraTag = "face-camera";
        protected string bodyCameraTag = "body-camera";

        //camera algorithms
        protected CameraMode cameraMode = CameraMode.Free;
        protected SetActionAlgorithm actionAlgorithm = null;
        protected CameraAlgorithm genericAlgorithm = null;
        protected TargetAlgorithm targetAlgorithm = null;
        protected FirstPersonNoFreeLookAlgorithm firstpersonNoLookAlgorithm = null;
        protected FirstPersonFreeLookAlgorithm firstpersonLookAlgorithm = null;

        //the panel used to display the camera scene
        protected PreviewPanel displayPanel = null;

        #endregion

        #region first person animation variables
        public GenericWaypoint currentWayPoint; //if first person camera mode is used this is the currentWaypoint
        public GenericWaypoint nextDestination; // if first person camera mode is used this is the next target destination
        public float newFacingAngle; // in first person camera mode turn the camera into this direction
        public OgreSet currentSet;
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the current target associated with the camera.
        /// </summary>
        public IGraphicalElement Target
        {
            get { this.VerifyDestruction(); return this.actionAlgorithm.Target; }
            set
            {
                this.VerifyDestruction();
                this.targetAlgorithm.Target = this.actionAlgorithm.Target = value;
            }
        }

        /// <summary>
        /// Gets or sets the current target associated with the camera.
        /// </summary>
        public OgrePosition TargetOffset
        {
            get
            {
                this.VerifyDestruction();
                return (OgrePosition) this.actionAlgorithm.TargetOffset;
            }
            set
            {
                this.VerifyDestruction();
                this.targetAlgorithm.TargetOffset = this.actionAlgorithm.TargetOffset = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the tag associated with the characters face camera 
        /// spot.
        /// </summary>
        public string FaceCameraTag
        {
            get { this.VerifyDestruction(); return faceCameraTag; }
            set { this.VerifyDestruction(); faceCameraTag = value; }
        }

        /// <summary>
        /// Gets or sets the name of the tag associated with the characters face body spot.
        /// </summary>
        public string BodyCameraTag
        {
            get { this.VerifyDestruction(); return bodyCameraTag; }
            set { this.VerifyDestruction(); bodyCameraTag = value; }
        }

        /// <summary>
        /// Gets or sets a property that teels if the camera should capture mouse events
        /// for navigation purposes.
        /// </summary>
        public bool CaptureMouse
        {
            get { return this.displayPanel.CaptureMouse; }
            set { this.displayPanel.CaptureMouse = value; }
        }

        /// <summary>
        /// Gets or sets a property that teels if the camera should capture keyboard events
        /// for navigation purposes.
        /// </summary>
        public bool CaptureKeyboard
        {
            get { return this.displayPanel.CaptureKeyboard; }
            set { this.displayPanel.CaptureKeyboard = value; }
        }

        /// <summary>
        /// Gets or sets a property that teels if the camera should react to events
        /// for navigation purposes.
        /// </summary>
        public bool AllowNavigation
        {
            get { return this.displayPanel.AllowNavigation; }
            set { this.displayPanel.AllowNavigation = value; }
        }

        /// <summary>
        /// Gets or sets the current camera mode and changes the camera algorithm 
        /// accordingly.
        /// </summary>
        public CameraMode CameraMode
        {
            get { return cameraMode; }
            set
            {
                cameraMode = value;
                switch(value)
                {
                    case CameraMode.DynamicTargeted:
                        this.algorithm = this.actionAlgorithm;
                        this.AllowNavigation = false;
                        break;
                    case CameraMode.Free:
                        this.algorithm = this.genericAlgorithm;
                        this.AllowNavigation = true;
                        break;
                    case CameraMode.FirstPersonNoFreeLook:
                        this.algorithm = this.firstpersonNoLookAlgorithm;
                        this.AllowNavigation = true;
                        break;
                    case CameraMode.FirstPersonFreeLook:
                        this.algorithm = this.firstpersonLookAlgorithm;
                        this.AllowNavigation = true;
                        break;
                    case CameraMode.StaticTargeted:
                        this.algorithm = this.targetAlgorithm;
                        this.AllowNavigation = false;
                        break;
                    case CameraMode.Static:
                        this.algorithm = this.genericAlgorithm;
                        this.AllowNavigation = false;
                        break;
                    default:
                        this.algorithm = this.genericAlgorithm;
                        this.AllowNavigation = false;
                        break;
                }
            }
        }

        #endregion

        #region Constructors

        public OrientCamera(Camera camera, PreviewPanel displayPanel)
            :base(camera)
        {
            this.displayPanel = displayPanel;
            this.actionAlgorithm = new SetActionAlgorithm(this);
            this.genericAlgorithm = new CameraAlgorithm(this);
            this.targetAlgorithm = new TargetAlgorithm(this);
            this.firstpersonLookAlgorithm = new FirstPersonFreeLookAlgorithm(this);
            this.firstpersonNoLookAlgorithm = new FirstPersonNoFreeLookAlgorithm(this);
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Freezes the camera by changing it's mode to static.
        /// </summary>
        public void Freeze()
        {
            this.VerifyDestruction();
            this.CameraMode = CameraMode.Free;
        }

        /// <summary>
        /// Places the camera in the pre-defined camera face spot of the given character.
        /// </summary>
        /// <param name="character">the character where to place the camera.</param>
        public void PlaceOnCharacterFaceCamera(OgreCharacter character)
        {
            this.VerifyDestruction();
            this.Place(null, character, this.faceCameraTag, false);
        }

        /// <summary>
        /// Makes the camera focus the character's face without changing its current 
        /// position or camera mode.
        /// </summary>
        /// <param name="character">the character to focus the face.</param>
        /// <remarks>changes the camera Target parameter.</remarks>
        public void FocusCharacterFace(OgreCharacter character)
        {
            this.VerifyDestruction();

            this.targetAlgorithm.Target = this.actionAlgorithm.Target = 
            this.firstpersonNoLookAlgorithm.Target = character;
            
            this.targetAlgorithm.TargetOffset = this.actionAlgorithm.TargetOffset =
                new OgrePosition(0, (float)(OgreDistance)character.Height + 20, 0);

            this.targetAlgorithm.Update(0); //look at character
        }

        public void FocusAsset(IGraphicalElement character)
        {
            this.VerifyDestruction();

            this.targetAlgorithm.Target = this.actionAlgorithm.Target =
            this.firstpersonNoLookAlgorithm.Target = character;

            this.targetAlgorithm.Update(0); //look at asset
        }


        /// <summary>
        /// Places the camera in the pre-defined camera body spot of the given character.
        /// </summary>
        /// <param name="character">the character where to place the camera.</param>
        public void PlaceOnCharacterBodyCamera(OgreCharacter character)
        {
            this.VerifyDestruction();
            this.Place(null, character, this.bodyCameraTag, false);
        }

        /// <summary>
        /// Makes the camera focus the character's body without changing its current 
        /// position or camera mode.
        /// </summary>
        /// <param name="character">the character to focus the body.</param>
        /// <remarks>changes the camera Target parameter.</remarks>
        public void FocusCharacterBody(OgreCharacter character)
        {
            this.VerifyDestruction();

            this.targetAlgorithm.Target = this.actionAlgorithm.Target =
            this.firstpersonNoLookAlgorithm.Target = character;
            this.targetAlgorithm.TargetOffset = this.actionAlgorithm.TargetOffset =
                new OgrePosition(0, (float)((OgreDistance)character.Height * 0.5f), 0);

            this.targetAlgorithm.Update(0); //look at character
        }

        /// <summary>
        /// Makes the camera focus the given character without changing its current 
        /// position or camera mode.
        /// <param name="character">the character for the camera to focus.</param>
        /// <remarks>changes the camera Target parameter.</remarks>
        public void FocusCharacterHeight(OgreCharacter character)
        {
            this.VerifyDestruction();

            this.targetAlgorithm.Target = this.actionAlgorithm.Target =
                character;
            this.targetAlgorithm.TargetOffset = this.actionAlgorithm.TargetOffset =
                new OgrePosition(0, (float)(OgreDistance)character.Height, 0);

            this.targetAlgorithm.Update(0); //look at character
        }

        /// <summary>
        /// Makes the camera focus the given item without changing its current 
        /// position or camera mode.
        /// <param name="item">the item for the camera to focus.</param>
        /// <remarks>changes the camera Target parameter.</remarks>
        public void FocusItemHeight(OgreItem item)
        {
            this.VerifyDestruction();

            this.targetAlgorithm.Target = this.actionAlgorithm.Target =
                item;
            this.targetAlgorithm.TargetOffset = this.actionAlgorithm.TargetOffset =
                new OgrePosition(0, (float)(OgreDistance)item.Height, 0);

            this.targetAlgorithm.Update(0); //look at item
        }

        /// <summary>
        /// Makes the camera focus the given asset without changing its current 
        /// position or camera mode.
        /// <param name="asset">the asset for the camera to focus.</param>
        /// <remarks>changes the camera Target parameter.</remarks>
        public void FocusAsset(IOgreInteractionAsset asset)
        {
            this.VerifyDestruction();

            this.targetAlgorithm.Target = this.actionAlgorithm.Target =
                asset;
            this.targetAlgorithm.TargetOffset = new OgrePosition(0, 0, 0);

            this.targetAlgorithm.Update(0); //look at asset
        }

        /// <summary>
        /// Places the camera on one of its camera spots.
        /// </summary>
        /// <param name="set">the set to put the camera on to.</param>
        public void PlaceOnSetCamera(OgreSet set)
        {
            this.actionAlgorithm.Set = set;
            currentSet = set;
            //set.ShowAllDebuggingSpots();
        }


        /// <summary>
        /// rotates a vector by the given angle clockwise in the given direction around the origin (0,0)
        /// of the XZ plane and returns the result of the rotation
        /// </summary>
        /// <param name="dir_in">the vector to rotate. It is assumed that its y component is 0.</param>
        /// <param name="angle_in">how many degrees to rotate in radians</param>
        public OgreDirection rotateVectorXZPlaneClockWise(OgreDirection dir_in, float angle_in)
        {
         OgreDirection movingDirection = new OgreDirection();
         movingDirection.Y = 0;
         movingDirection.X = dir_in.X * Math.Cos(angle_in) - dir_in.Z * Math.Sin(angle_in);
         movingDirection.Z = dir_in.X * Math.Sin(angle_in) + dir_in.Z * Math.Cos(angle_in);
         movingDirection.Vector3.Normalise();
         return movingDirection;
        }

        /// <summary>
        /// adds the height vector to a position on ground level that is necessary to achieve 
        /// a first person view
        /// <param name="basePosition">a position on ground level (y=0) to which to add the y-offset</param>
        public OgrePosition addFirstPersonHeightOffset(Position basePosition)
        { 
         OgrePosition heightOffset = new OgrePosition(0.0f, userHeight, 0.0f);
         return (OgrePosition) basePosition.Add(heightOffset); 
        }


        #endregion

    }
}