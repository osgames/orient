using System;
using System.Collections.Generic;
using System.Text;
using PAGE.Generic.Domain.Cameras;

namespace PAGE.Orient.Domain.Cameras
{
    public class FirstPersonFreeLookAlgorithm : CameraAlgorithm
    {
    
       #region Constructors

        private FirstPersonAlgorithm firstPersonAlgorithm;

        public FirstPersonFreeLookAlgorithm(GenericCamera camera)
            : base(camera)
        {
            firstPersonAlgorithm = new FirstPersonAlgorithm(camera);
        }

        #endregion

        #region Public Methods

        public override void Update(float millisSinceLastUpdate)
        {
            firstPersonAlgorithm.Update(millisSinceLastUpdate);  
        }

        #endregion    
    
    }
}
