/* ****************************************************
 * Name: OrientCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using Mogre;
using PAGE.Generic.Management;
using PAGE.Generic.Management.Character;
using PAGE.OGRE.Domain.Assets;
using PAGE.OGRE.Math;
using PAGE.Orient.Domain.Cameras;
using PAGE.Orient.Management;
using PAGE.Orient.Management.Character;
using PAGE.Orient.Resources;
using PAGE.Orient.Resources.Assets;

namespace PAGE.Orient.Domain.Assets
{
    public class OrientCharacter : OgreCharacter
    {
        #region Fields

        protected OrientResourcesManager resourcesManager;

        public const string NATURAL_EXPRESSION = "natural";
        protected const float EXPRESSION_TRANSITION_TIME = 0.5f;

        protected OrientExpressionManager expressionManager;
        protected VertexPoseKeyFrame manualVPKeyFrame;

        #endregion

        #region Properties

        public new OrientResourceCharacter ResourceAsset
        {
            get { return (OrientResourceCharacter) this.resourceAsset; }
        }

        /// <summary>
        /// Gets the facial expression manager associated with this character.
        /// </summary>
        public OrientExpressionManager ExpressionManager
        {
            get { return this.expressionManager; }
        }

        public new OrientSpeechManager SpeechManager
        {
            get { return base.SpeechManager as OrientSpeechManager; }
        }

        public new OrientTextManager TextManager
        {
            get { return base.TextManager as OrientTextManager; }
        }

        #endregion

        #region Constructors 

        protected OrientCharacter(
            OrientResourceCharacter resource,
            OrientResourcesManager resourcesManager,
            OrientCamera camera, Entity entity,
            VertexPoseKeyFrame manualVPKeyFrame, bool init)
            : base(resource, camera, entity, false)
        {
            this.manualVPKeyFrame = manualVPKeyFrame;
            this.resourcesManager = resourcesManager;

            if (init) this.Init();
        }

        public OrientCharacter(
            OrientResourceCharacter resource,
            OrientResourcesManager resourcesManager,
            OrientCamera camera, Entity entity,
            VertexPoseKeyFrame manualVPKeyFrame)
            : this(resource, resourcesManager, camera, entity, manualVPKeyFrame, true)
        {
        }

        #endregion

        #region Public Methods

        public override void SaySpeech(IActionCallback callback, string utterance)
        {
            this.SaySpeech(callback, utterance, true);
        }

        /// <summary>
        /// Makes the character say the given utterance.
        /// </summary>
        /// <param name="callback">the callback to receive the speaking events from:
        /// Succeeded when the speak animation finishes, Failed if the character can't
        /// express the given utterance or there is no action defined with the given 
        /// name, ActionTrigger when pre-defined animation times are reached.
        /// </param>
        /// <param name="utterance">the utterance for the character to say.</param>
        /// <param name="gibberish">if set to true, a gibberish sound will be reproduced
        /// during the speech.</param>
        public virtual void SaySpeech(IActionCallback callback, string utterance, bool gibberish)
        {
            this.VerifyDestruction();
            this.SpeechManager.ExecuteAction(callback, "", utterance, gibberish);
        }

        /// <summary>
        /// Makes the character say the given utterance with a gibberish sound being 
        /// reproduced during the speech.
        /// </summary>
        /// <param name="callback">the callback to receive the speaking events from:
        /// Succeeded when the speak animation finishes, Failed if the character can't
        /// express the given utterance or there is no action defined with the given 
        /// name, ActionTrigger when pre-defined animation times are reached.
        /// </param>
        /// <param name="utterance">the utterance for the character to say.</param>
        public virtual void SayGibberish(IActionCallback callback, string utterance)
        {
            this.SaySpeech(callback, utterance, true);
        }

        /// <summary>
        /// Changes the current texture of the character's body (clothes).
        /// </summary>
        /// <param name="textureName">the name of the texture to set in the given
        /// body part.</param>
        public virtual void ChangeTexture(string textureName)
        {
            this.ChangeTexture(0, textureName);
        }

        /// <summary>
        /// Changes the character's current facial expression. The facial pose will be 
        /// added to the current ones if weight is greater than 0, or reset if weight is 0.
        /// </summary>
        /// <param name="expression">the name of the expression to change the weigth to.</param>
        /// <param name="weight">the weight to change the given facial expression.</param>
        public virtual void ChangeFace(string expression, float weight)
        {
            this.VerifyDestruction();
            if (expression == NATURAL_EXPRESSION)
                this.expressionManager.ResetAllExpressions();
            else
                this.expressionManager.ChangeExpression(
                    expression, weight, EXPRESSION_TRANSITION_TIME);
        }

        public virtual void ShowName(string name)
        {
            this.VerifyDestruction();
            if (this.TextManager == null) return;
            this.TextManager.NameText.Visible = true;
            this.TextManager.NameText.Text = name;
        }

        public virtual void HideName()
        {
            this.VerifyDestruction();
            if (this.TextManager != null) 
                this.TextManager.NameText.Visible = false;
        }

        #endregion

        #region Protected Methods

        protected override void CreateManagers()
        {
            base.CreateManagers();

            this.expressionManager = this.CreateExpressionManager();
            if (this.expressionManager != null) this.managers.Add(this.expressionManager);
        }

        protected virtual OrientExpressionManager CreateExpressionManager()
        {
            return new OrientExpressionManager(this, this.manualVPKeyFrame);
        }

        protected override SpeechManager CreateSpeechManager()
        {
            return new OrientSpeechManager(this, this.resourcesManager);
        }

        protected override TextManager CreateTextManager()
        {
            OrientTextManager newTextManager = new OrientTextManager(this, (OrientCamera) this.camera);

            //main text for subtitles
            newTextManager.MainText.Color = this.ResourceAsset.Color;
            newTextManager.MainText.CharHeight = 28;
            newTextManager.MainText.RelativeTextPosition =
                new OgrePosition(0, (float) (OgreDistance) this.Height + SUBTITLE_OFFSET, 0);

            //name text
            newTextManager.MainText.Color = this.ResourceAsset.Color;
            newTextManager.NameText.CharHeight = 28;
            newTextManager.NameText.RelativeTextPosition =
                new OgrePosition(0, (float) (OgreDistance) this.Height * 0.5f, 0);

            newTextManager.MainText.Update(0);
            return newTextManager;
        }

        #endregion
    }
}