/* ****************************************************
 * Name: OrientSet.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/05/07 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.IO;
using Mogre;
using PAGE.OGRE.Domain.Assets;
using PAGE.OGRE.Util;
using PAGE.Orient.Resources.Assets;
using PAGE.Util;
using WMPLib;

namespace PAGE.Orient.Domain.Assets
{
    public class OrientSet : OgreSet
    {
        #region Fields

        //background sound
        private WindowsMediaPlayer backgroundPlayer;

        #endregion

        #region Properties

        public new OrientResourceSet ResourceAsset
        {
            get { return base.ResourceAsset as OrientResourceSet; }
        }

        public override bool Visible
        {
            get { return base.Visible; }
            set
            {
                if (!this.Visible && value) this.backgroundPlayer.controls.play();
                else if (this.Visible && !value) this.backgroundPlayer.controls.pause();
                base.Visible = value;
            }
        }

        #endregion

        #region Constructors 

        public OrientSet(OrientResourceSet resource, OSMLoader loader, SceneNode rootNode)
            : base(resource, loader, rootNode)
        {
            this.CreateMediaPlayer();
        }

        #endregion

        #region Public Methods

        public override void Destroy()
        {
            base.Destroy();
            this.backgroundPlayer.close();
            this.backgroundPlayer = null;
        }

        public override void Reset()
        {
            base.Reset();
            if (this.Destroyed || (this.backgroundPlayer == null))
                this.CreateMediaPlayer();
        }

        #endregion

        #region Protected Methods

        protected void CreateMediaPlayer()
        {
            this.backgroundPlayer = new WindowsMediaPlayer();
            this.backgroundPlayer.PlayStateChange += BackgroundPlayer_PlayStateChange;

            //opens background sound file
            string soundFileName = PathUtil.ReturnFullPath(this.ResourceAsset.BackgroundSoundFile);
            if (File.Exists(soundFileName))
            {
                this.backgroundPlayer.URL = soundFileName;
                this.backgroundPlayer.controls.stop();
            }
        }

        protected void BackgroundPlayer_PlayStateChange(int NewState)
        {
            //loops background file
            if (NewState == (int) WMPPlayState.wmppsStopped)
                this.backgroundPlayer.controls.play();
        }

        #endregion
    }
}