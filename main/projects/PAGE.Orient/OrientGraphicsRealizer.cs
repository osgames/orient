/* ****************************************************
 * Name: OrientGraphicsRealizer.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/14 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Mogre;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Cameras;
using PAGE.Generic.GUI;
using PAGE.Generic.Resources;
using PAGE.OGRE;
using PAGE.OGRE.GUI;
using PAGE.OGRE.Math;
using PAGE.OGRE.Util;
using PAGE.Orient.Domain.Assets;
using PAGE.Orient.Domain.Cameras;
using PAGE.Orient.GUI.Windows.Forms.Controls;
using PAGE.Orient.Resources;
using PAGE.Orient.Resources.Assets;

namespace PAGE.Orient
{
    public class OrientGraphicsRealizer : OgreGraphicsRealizer
    {
        #region Fields

        protected const int CHARACTER_WIDTH = 4;
        private bool characterNamesShown = true;

        //singleton instance
        public new static OrientGraphicsRealizer Instance = new OrientGraphicsRealizer();

        #endregion

        #region Properties

        public new OrientResourcesManager ResourcesManager
        {
            get { return base.ResourcesManager as OrientResourcesManager; }
        }

        public new OrientCamera Camera
        {
            get { return base.Camera as OrientCamera; }
        }

        public new WindowsFormsGUIManager GUIManager
        {
            get { return guiManager as WindowsFormsGUIManager; }
        }

        public bool CharacterNamesShown
        {
            get { return characterNamesShown; }
        }


        #endregion

        #region Public Methods

        public override void Setup(string resourcesFileName, Control control)
        {
            if (!(control is PreviewPanel))
            {
                throw new ArgumentException("Control must be a preview panel to start Orient");
            }
            base.Setup(resourcesFileName, control);
        }

        public override GenericCharacter LoadCharacter(string idResourceToken)
        {
            //checks resources
            OrientResourceCharacter resource = (OrientResourceCharacter)
                                               this.resourcesManager.Characters[idResourceToken.ToLower()];
            if (resource == null)
            {
                throw new ArgumentException("There is no character resource with that name: "
                                            + idResourceToken);
            }

            string characterId = idResourceToken + ++assetCounter;
            OrientCharacter character = null;

            lock (this.locker)
            {
                //creates character scene node
                SceneNode node = this.CreateAssetSceneNode(resource);

                try
                {
                    //load's ogre character entity
                    Entity entity = null;
                    VertexPoseKeyFrame vpKeyFrame = null;
                    if (resource.FileName != "")
                    {
                        //changes mesh to support manual pose animations
                        vpKeyFrame = OgreUtil.CreateManualPoseAnimation(
                            resource.FileName, OrientResourceCharacter.MANUAL_FACE_ANIM_NAME);

                        entity = this.OgreApplication.SceneManager.CreateEntity(characterId, resource.FileName);
                        entity.CastShadows = true;
                        OgreUtil.SetReceiveShadows(entity, false);
                        OgreUtil.CloneMaterial(entity);
                        node.AttachObject(entity);
                    }

                    //creates and adds character to internal db
                    character = new OrientCharacter(resource, this.ResourcesManager,
                                                    this.Camera, entity, vpKeyFrame);
                    
                    //sets new bounding box
                    entity.GetMesh()._setBounds(new AxisAlignedBox(
                        -CHARACTER_WIDTH, 0, -CHARACTER_WIDTH, 
                        CHARACTER_WIDTH, (float)(OgreDistance)character.Height, CHARACTER_WIDTH));

                    this.characters.Add(character);

                    character.Reset();
                }
                catch (SEHException)
                {
                    if (OgreException.IsThrown) this.OgreApplication.ThrowOgreException();
                    else throw;
                }
            }

            return character;
        }

        public override GenericSet LoadSet(string idResourceToken)
        {
            //checks resources
            OrientResourceSet resource = (OrientResourceSet)
                                         this.resourcesManager.Sets[idResourceToken.ToLower()];
            if (resource == null)
            {
                throw new ArgumentException("There is no set resource with that name: "
                                            + idResourceToken);
            }

            OrientSet set = null;
            assetCounter++;

            lock (this.locker)
            {
                //Set Loading
                SceneNode rootNode = this.CreateAssetSceneNode(resource);

                try
                {
                    OSMLoader loader = new OSMLoader(this.OgreApplication.SceneManager,
                                                     this.OgreApplication.RenderWindow);
                    if (resource.FileName != "")
                    {
                        loader.Initialize(resource.FileName);
                        loader.CreateScene(rootNode);
                    }

                    //creates and adds set to internal db
                    set = new OrientSet(resource, loader, rootNode);
                    set.Reset();
                    this.sets.Add(set);

                    //loads set elements and contained assets
                    this.LoadSetElements(set);
                }
                catch (SEHException)
                {
                    if (OgreException.IsThrown) this.OgreApplication.ThrowOgreException();
                    else throw;
                }
            }

            return set;
        }

        /// <summary>
        /// Shows all the texts of the characters names.
        /// </summary>
        public virtual void ShowAllCharacterNames()
        {
            characterNamesShown = true;
            foreach(OrientCharacter character in this.characters)
                if (character.Visible)
                    character.TextManager.NameText.Visible = true;
        }

        /// <summary>
        /// Hides all the texts of the characters names.
        /// </summary>
        public virtual void HideAllCharacterNames()
        {
            characterNamesShown = false;
            foreach (OrientCharacter character in this.characters)
                character.TextManager.NameText.Visible = false;
        }

        /// <summary>
        /// Hides/Shows all the texts of the characters names, depending on whether they are visible or invisible when the method is called
        /// </summary>
        public virtual void ToggleCharacterNames()
        {
            if (characterNamesShown)
                HideAllCharacterNames();
            else
                ShowAllCharacterNames();
        }


        #endregion

        #region Protected Methods

        protected override GenericCamera CreateCamera()
        {
            return new OrientCamera(this.OgreApplication.Camera,
                                    this.OgreApplication.Control as PreviewPanel);
        }

        protected override ResourcesManager CreateResourcesManager(string resourcesFileName)
        {
            return new OrientResourcesManager(resourcesFileName);
        }

        protected override GUIManager CreateGUIManager()
        {
            return new WindowsFormsGUIManager(this.OgreApplication);
        }

        #endregion
    }
}