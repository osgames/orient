/* ****************************************************
 * Name: OrientResourcesManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/26 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.IO;
using PAGE.Generic.Resources.Assets;
using PAGE.OGRE.Resources;
using PAGE.Util;

namespace PAGE.Orient.Resources
{
    public class OrientResourcesManager : OgreResourcesManager
    {
        #region Fields

        //voices config
        protected VoicesConfig voicesConfig = new VoicesConfig();

        #endregion

        #region Properties

        public new OrientResourcesConfig ResourcesConfig
        {
            get { return (OrientResourcesConfig) base.ResourcesConfig; }
        }

        public new OrientResourcesFactory ResourcesFactory
        {
            get { return base.ResourcesFactory as OrientResourcesFactory; }
        }

        public new TokenObjectList<ResourceHumanLikeCharacter> Characters
        {
            get
            {
                TokenObjectList<ResourceHumanLikeCharacter> list = new
                    TokenObjectList<ResourceHumanLikeCharacter>();
                foreach (ResourceHumanLikeCharacter c in base.Characters)
                    list.Add(c);
                return list;
            }
        }

        /// <summary>
        /// Gets the gibberish voices configuration.
        /// </summary>
        public VoicesConfig VoicesConfig
        {
            get { return voicesConfig; }
        }

        #endregion

        #region Constructors

        public OrientResourcesManager(string configFileName)
            : this(configFileName, new OrientResourcesFactory())
        {
        }

        public OrientResourcesManager(
            string configFileName, OrientResourcesFactory resourcesFactory)
            : base(configFileName, resourcesFactory)
        {
        }

        #endregion

        #region Public Methods

        public override void LoadAll()
        {
            base.LoadAll();

            //loads gibberish configuration
            this.LoadVoices(this.ResourcesConfig.VoicesFile);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Loads the gibberish voice configuration including utterance index table.
        /// </summary>
        /// <param name="voicesConfigFile">the file path to the voice configuration.</param>
        protected virtual void LoadVoices(string voicesConfigFile)
        {
            if (!File.Exists(voicesConfigFile))
            {
                throw new ApplicationException(
                    "Could not find " + voicesConfigFile + " file.");
            }

            //loads configuration file
            this.voicesConfig.LoadFromXml(voicesConfigFile);

            //loads index audio files configuration
            this.voicesConfig.IndexFile =
                Path.GetDirectoryName(this.ResourcesConfig.VoicesFile) + "\\" +
                this.voicesConfig.IndexFile;
            this.voicesConfig.ReadAudioIndexInfo();
        }

        #endregion
    }
}