/* ****************************************************
 * Name: OrientResourcesFactory.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/23
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Generic.Resources;
using PAGE.Generic.Resources.Assets;
using PAGE.OGRE.Resources;
using PAGE.Orient.Resources.Assets;

namespace PAGE.Orient.Resources
{
    public class OrientResourcesFactory : OgreResourcesFactory
    {
        #region Public Methods

        public override ResourceCharacter CreateNewCharacter(string characterID)
        {
            return new OrientResourceCharacter(characterID, this);
        }

        public override ResourceSet CreateNewSet(string setID)
        {
            return new OrientResourceSet(setID, this);
        }

        public override ResourcesConfig CreateNewResourcesConfig()
        {
            return new OrientResourcesConfig();
        }

        #endregion
    }
}