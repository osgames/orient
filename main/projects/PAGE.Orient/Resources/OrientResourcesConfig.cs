/* ****************************************************
 * Name: OrientResourcesConfig.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/05 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Xml;
using PAGE.OGRE.Resources;

namespace PAGE.Orient.Resources
{
    /// <summary>
    /// Contains information concerning all the realizer resources and files.
    /// </summary>
    public class OrientResourcesConfig : OgreResourcesConfig
    {
        protected const string VOICES_FILE_XML_ELEM = "VoicesFile";

        #region Fields

        //resource files
        protected string voicesFile = "voices.xml";

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the path of the resource voices config file.
        /// </summary>
        public string VoicesFile
        {
            get { return voicesFile; }
            set { voicesFile = value; }
        }

        #endregion

        #region Serializable Methods

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            XmlElement childElement = (XmlElement) element.SelectSingleNode(VOICES_FILE_XML_ELEM);
            if (childElement != null) this.VoicesFile = childElement.InnerXml;
        }

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            XmlElement childElement = element.OwnerDocument.CreateElement(VOICES_FILE_XML_ELEM);
            childElement.InnerXml = this.VoicesFile;
            element.AppendChild(childElement);
        }

        #endregion
    }
}