/* ****************************************************
 * Name: VoicesConfig.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/09/07 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using PAGE.Util;

namespace PAGE.Orient.Resources
{
    /// <summary>
    /// Contains the voices configuration and info for Orient.
    /// </summary>
    public class VoicesConfig : IXmlSerializable
    {
        #region UtteranceIndexKeyPair Struct

        public struct UtteranceIndexKeyPair
        {
            /// <summary>
            /// The utterance file id.
            /// </summary>
            public string UtteranceID;

            /// <summary>
            /// The string representing the utterance index key.
            /// </summary>
            public string IndexKey;

            public UtteranceIndexKeyPair(string utteranceID, string indexKey)
            {
                this.UtteranceID = utteranceID;
                this.IndexKey = indexKey;
            }
        }

        #endregion

        #region Fields

        //index file name
        protected string indexFile;

        //magic words list
        protected StringArrayList magicWords = new StringArrayList();

        //existent voices (voice-id - audio files path)
        protected KeyValueList voices = new KeyValueList();

        //audio files
        protected Dictionary<string, UtteranceIndexKeyPair> indexedAudioFiles =
            new Dictionary<string, UtteranceIndexKeyPair>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the utterances index file name.
        /// </summary>
        public string IndexFile
        {
            get { return indexFile; }
            set { indexFile = value; }
        }

        /// <summary>
        /// Gets the magic words list.
        /// </summary>
        public StringArrayList MagicWords
        {
            get { return magicWords; }
        }

        /// <summary>
        /// Gets the available voices (voice-id - audio files path).
        /// </summary>
        public KeyValueList Voices
        {
            get { return voices; }
        }

        /// <summary>
        /// Gets the available audio files indexed by utterance (english).
        /// </summary>
        public Dictionary<string, UtteranceIndexKeyPair> IndexedAudioFiles
        {
            get { return indexedAudioFiles; }
        }

        #endregion

        #region Serializable Methods

        /// <summary>
        /// Saves (serializes) the resource object to the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name to save the object info into.</param>
        public virtual void SaveToXml(string xmlFileName)
        {
            this.SaveToXml(xmlFileName, "voices-config");
        }

        /// <summary>
        /// Saves (serializes) the resource object to the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name to save the object info into.</param>
        /// <param name="rootName">the name for the root XML element.</param>
        public void SaveToXml(string xmlFileName, string rootName)
        {
            try
            {
                //creates xml document
                XmlDocument xd = new XmlDocument();

                //root element
                XmlElement rootElement = xd.CreateElement(rootName);
                xd.CreateXmlDeclaration("1.0", Encoding.UTF8.EncodingName, "");
                xd.AppendChild(rootElement);

                //write contents
                this.WriteXml(rootElement);

                //verifies file existence
                if (File.Exists(xmlFileName)) File.Delete(xmlFileName);

                //save the document to file
                StreamWriter writer = new StreamWriter(xmlFileName, false, Encoding.UTF8);
                xd.Save(writer);
                writer.Close();
            }
            catch (XmlException) //do nothing...
            {
            }
            catch (IOException) //do nothing...
            {
            }
            catch (InvalidOperationException) //do nothing...
            {
            }
        }

        /// <summary>
        /// Loads (deserializes) the resource object from the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name containing the object info.</param>
        /// <returns>the ResourceAsset loaded from the XML file.</returns>
        public virtual void LoadFromXml(string xmlFileName)
        {
            this.LoadFromXml(xmlFileName, "voices-config");
        }

        /// <summary>
        /// Loads (deserializes) the resource object from the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name containing the object info.</param>
        /// <param name="rootName">the name for the root XML element.</param>
        public void LoadFromXml(string xmlFileName, string rootName)
        {
            if (!File.Exists(xmlFileName)) return;

            XmlDocument xd = new XmlDocument();
            try
            {
                xd.Load(xmlFileName);

                //loads root element
                XmlElement rootElement = (XmlElement) xd.SelectSingleNode(rootName);
                if (rootElement == null) return;

                this.ReadXml(rootElement);
            }
            catch (XmlException) //do nothing...
            {
            }
            catch (XPathException) //do nothing...
            {
            }
        }

        public virtual void ReadXml(XmlElement element)
        {
            //index file
            this.IndexFile = element.GetAttribute("index-file");

            //voices
            foreach (XmlElement voiceElement in element.SelectNodes("voices/voice"))
            {
                KeyValuePair kvp = new KeyValuePair();
                kvp.ReadXml(voiceElement);
                if (kvp.key != null) this.Voices.Add(kvp);
            }

            //magic words
            foreach (XmlElement wordElement in element.SelectNodes("magic-words/magic-word"))
            {
                string word = wordElement.InnerXml;
                if (word != "") this.MagicWords.Add(word);
            }
        }

        public virtual void WriteXml(XmlElement element)
        {
            //index file id
            element.SetAttribute("index-file", this.IndexFile);

            //voices
            XmlElement childElement = element.OwnerDocument.CreateElement("voices");
            element.AppendChild(childElement);
            foreach (KeyValuePair kvp in this.Voices)
            {
                XmlElement voiceElement = element.OwnerDocument.CreateElement("voice");
                kvp.WriteXml(voiceElement);
                childElement.AppendChild(voiceElement);
            }

            //magic words
            childElement = element.OwnerDocument.CreateElement("magic-words");
            element.AppendChild(childElement);
            foreach (string word in this.MagicWords)
            {
                XmlElement wordElement = element.OwnerDocument.CreateElement("magic-word");
                wordElement.InnerXml = word.ToLower();
                childElement.AppendChild(wordElement);
            }
        }

        /// <summary>
        /// Reads the voices utterance indexes file and creates the audio
        /// indexes table.
        /// </summary>
        public virtual void ReadAudioIndexInfo()
        {
            if(!File.Exists(this.IndexFile)) return;

            //clears previous index
            this.indexedAudioFiles.Clear();

            //reads csv file
            StreamReader sr = new StreamReader(this.IndexFile);
            string line;

            //separate and read index info from each line
            while ((line = sr.ReadLine()) != null)
            {
                string[] fields = line.Split(new char[] {';'});
                string utteranceID = fields[0];
                string utterance = fields[1];
                string indexKey = fields[3];

                //adds new utterance index info
                this.indexedAudioFiles.Add(
                    utterance, new UtteranceIndexKeyPair(utteranceID, indexKey));
            }

            sr.Close();
        }

        #endregion
    }
}