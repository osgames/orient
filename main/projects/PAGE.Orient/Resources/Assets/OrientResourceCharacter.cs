/* ****************************************************
 * Name: OrientResourceCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/09/03
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Xml;
using PAGE.Generic.Resources.Assets;
using PAGE.Util;

namespace PAGE.Orient.Resources.Assets
{
    /// <summary>
    /// Contains the resource information for a human-like character.
    /// Includes information about phonemes and facial expressions.
    /// </summary>
    public class OrientResourceCharacter : ResourceHumanLikeCharacter
    {
        #region Fields

        //manual facial expression animation name
        public const string MANUAL_FACE_ANIM_NAME = "ManualPoseFacialAnimation";

        //phonemes e facial expressions
        protected KeyValueList phonemes = new KeyValueList();
        protected KeyValueList facialExpressions = new KeyValueList();

        //voice id
        protected string voiceID;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the character's available phonemes list.
        /// </summary>
        public KeyValueList Phonemes
        {
            get { return phonemes; }
            set { phonemes = value; }
        }

        /// <summary>
        /// Gets or sets the character's available expressions list.
        /// </summary>
        public KeyValueList FacialExpressions
        {
            get { return facialExpressions; }
            set { facialExpressions = value; }
        }

        /// <summary>
        /// Gets or sets the character's voice id.
        /// </summary>
        public string VoiceID
        {
            get { return voiceID; }
            set { voiceID = value; }
        }

        #endregion

        #region Constructors

        public OrientResourceCharacter(string idToken, OrientResourcesFactory resourcesFactory)
            : base(idToken, resourcesFactory)
        {
        }

        #endregion

        #region Serialization Methods

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            //voice id
            element.SetAttribute("voice-id", this.VoiceID);

            //facial expressions
            XmlElement childElement = element.OwnerDocument.CreateElement("expressions");
            element.AppendChild(childElement);
            foreach (KeyValuePair kvp in this.facialExpressions)
            {
                XmlElement expressionElement = element.OwnerDocument.CreateElement("expression");
                kvp.WriteXml(expressionElement);
                childElement.AppendChild(expressionElement);
            }

            //phonemes
            childElement = element.OwnerDocument.CreateElement("phonemes");
            element.AppendChild(childElement);
            foreach (KeyValuePair kvp in this.phonemes)
            {
                XmlElement phonemeElement = element.OwnerDocument.CreateElement("phoneme");
                kvp.WriteXml(phonemeElement);
                childElement.AppendChild(phonemeElement);
            }
        }

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            //voice id
            this.VoiceID = element.GetAttribute("voice-id");

            //facial expressions
            foreach (XmlElement expressionElement in element.SelectNodes("expressions/expression"))
            {
                KeyValuePair kvp = new KeyValuePair();
                kvp.ReadXml(expressionElement);
                if (kvp.key != null) this.facialExpressions.Add(kvp);
            }

            //phonemes
            foreach (XmlElement phonemeElement in element.SelectNodes("phonemes/phoneme"))
            {
                KeyValuePair kvp = new KeyValuePair();
                kvp.ReadXml(phonemeElement);
                if (kvp.key != null) this.phonemes.Add(kvp);
            }
        }

        #endregion
    }
}