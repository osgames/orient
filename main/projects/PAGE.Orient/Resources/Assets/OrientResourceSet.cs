/* ****************************************************
 * Name: OrientResourceSet.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/05/07
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Xml;
using PAGE.OGRE.Resources.Assets;

namespace PAGE.Orient.Resources.Assets
{
    /// <summary>
    /// Contains the resource information for a walkable set.
    /// Includes particle system, skybox and background sounds info.
    /// </summary>
    public class OrientResourceSet: OgreResourceSet
    {
        #region Fields

        protected string backgroundSoundFile = "";

        #endregion 

        #region Properties

        /// <summary>
        /// Gets or sets the name of the particle system associated with 
        /// the resource set.
        /// </summary>
        public string BackgroundSoundFile
        {
            get { return backgroundSoundFile; }
            set { backgroundSoundFile = value; }
        }

        #endregion

        #region Constructors

        public OrientResourceSet(string idToken, OrientResourcesFactory resourcesFactory)
            : base(idToken, resourcesFactory)
        {
        } 

        #endregion

        #region Serialization Methods

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            //writes set attributes
            element.SetAttribute("background-sound", this.backgroundSoundFile);
        }

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            //reads set attributes
            this.backgroundSoundFile= element.GetAttribute("background-sound");
        }

        #endregion
    }
}