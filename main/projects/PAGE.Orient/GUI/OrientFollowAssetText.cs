/* ****************************************************
 * Name: OrientFollowAssetText.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/04 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.OGRE.Domain.Assets;
using PAGE.OGRE.GUI;
using PAGE.OGRE.Math;
using PAGE.Orient.Domain.Cameras;

namespace PAGE.Orient.GUI
{
    /// <summary>
    /// Represents a subtitle 2D text that follows an asset and its displayed above
    /// the assets's node. The size of the text will be a function of the distance
    /// of the asset to the camera.
    /// </summary>
    public class OrientFollowAssetText : OgreFollowAssetText
    {
        #region Fields

        public uint maxCharHeight = 32;
        public uint minCharHeight = 3;
        public float maxDistance = 300;
        public float minDistance = 20;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the max char size of the text.
        /// </summary>
        public uint MaxCharHeight
        {
            get { return maxCharHeight; }
            set { maxCharHeight = value; }
        }

        /// <summary>
        /// Gets or sets the min char size of the text.
        /// </summary>
        public uint MinCharHeight
        {
            get { return minCharHeight; }
            set { minCharHeight = value; }
        }

        /// <summary>
        /// Gets or sets the distance between the character and the camera from 
        /// which the char size is MinCharSize.
        /// </summary>
        public float MaxDistance
        {
            get { return maxDistance; }
            set { maxDistance = value; }
        }

        /// <summary>
        /// Gets or sets the distance between the character and the camera from 
        /// which the char size is MaxCharSize.
        /// </summary>
        public float MinDistance
        {
            get { return minDistance; }
            set { minDistance = value; }
        }

        #endregion

        #region Constructor

        public OrientFollowAssetText(IOgreInteractionAsset asset, OrientCamera cam)
            : base(asset, cam)
        {
        }

        #endregion

        #region Public Members

        public override void Update(float millisSinceLastUpdate)
        {
            base.Update(millisSinceLastUpdate);

            if (!this.Visible || !this.IsCameraFacingCharacter)
                return;

            //calculates distance between subtitle and camera
            double distanceCharCam = (OgreDistance)
                this.camera.AbsolutePosition.DistanceTo(
                    this.Asset.AbsolutePosition.Add(this.RelativeTextPosition));

            if (distanceCharCam <= this.minDistance)
                this.CharHeight = this.MaxCharHeight;
            else if (distanceCharCam >= this.MaxDistance)
                this.CharHeight = this.MinCharHeight;
            else
                this.CharHeight = (float) (this.MaxCharHeight -
                    (((distanceCharCam - this.MinDistance)* (this.MaxCharHeight - this.MinCharHeight))/
                        (this.MaxDistance - this.MinDistance)));
        }

        #endregion
    }
}