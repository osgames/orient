/* ****************************************************
 * Name: ItemSpotListComboBox.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/26
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.ComponentModel;
using PAGE.Generic.Resources.Spots;
using PAGE.Util;

namespace PAGE.Orient.GUI.Windows.Forms.ComboBox
{
    public partial class ItemSpotListComboBox : SpotListComboBox<ResourceItemSpot>
    {
        #region Properties

        [Browsable(false)]
        protected override TokenObjectList<ResourceSpot> IncludedSpots
        {
            get
            {
                if (this.asset == null) return new TokenObjectList<ResourceSpot>();
                else
                {
                    TokenObjectList<ResourceSpot> list = new TokenObjectList<ResourceSpot>();
                    foreach (ResourceItemSpot spot in this.asset.ResourceItemSpots)
                        list.Add(spot);
                    return list;
                }
            }
        }

        [Browsable(false)]
        public new ResourceItemSpot SelectedSpot
        {
            get { return this.selectedSpot; }
        }

        #endregion

        #region Constructors

        public ItemSpotListComboBox()
        {
            InitializeComponent();
        }

        public ItemSpotListComboBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        } 

        #endregion
    }
}