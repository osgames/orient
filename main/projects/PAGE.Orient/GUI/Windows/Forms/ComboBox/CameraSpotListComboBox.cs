/* ****************************************************
 * Name: CameraSpotListComboBox.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/06/23
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.ComponentModel;
using PAGE.Generic.Resources.Spots;
using PAGE.Util;

namespace PAGE.Orient.GUI.Windows.Forms.ComboBox
{
    public partial class CameraSpotListComboBox : SpotListComboBox<ResourceCameraSpot>
    {
        #region Properties

        [Browsable(false)]
        protected override TokenObjectList<ResourceSpot> IncludedSpots
        {
            get
            {
                if (this.asset == null) return new TokenObjectList<ResourceSpot>();
                else
                {
                    TokenObjectList<ResourceSpot> list = new TokenObjectList<ResourceSpot>();
                    foreach (ResourceCameraSpot spot in this.asset.ResourceCameraSpots)
                        list.Add(spot);
                    return list;
                }
            }
        }

        [Browsable(false)]
        public new ResourceCameraSpot SelectedSpot
        {
            get { return this.selectedSpot; }
        }

        #endregion

        #region Constructors

        public CameraSpotListComboBox()
        {
            InitializeComponent();
        }

        public CameraSpotListComboBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        } 

        #endregion
    }
}