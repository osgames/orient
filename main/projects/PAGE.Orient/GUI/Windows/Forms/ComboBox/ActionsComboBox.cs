/* ****************************************************
 * Name: ActionsComboBox.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/09
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using PAGE.Generic.Resources.Actions;
using PAGE.Generic.Resources.Assets;
using PAGE.Orient.GUI.Windows.Forms.Controls;
using PAGE.Orient.Resources;
using PAGE.Util;

namespace PAGE.Orient.GUI.Windows.Forms.ComboBox
{
    public partial class ActionsComboBox : ResourcesComboBox<ResourceAction>
    {
        #region Fields

        protected ResourceCharacter character;

        #endregion

        #region Properties

        [Browsable(false)]
        public new OrientResourcesManager ResourcesManager
        {
            get { return this.manager; }
            set { this.manager = value; }
        }

        [Browsable(false)]
        public ResourceCharacter Character
        {
            get { return this.character; }
            set
            {
                this.character = value;
                this.Enabled = this.character != null;

                this.RefreshResourceList();

                if (this.TokenObjectControl != null)
                {
                    this.TokenObjectControl.TokenObjectList = this.Resources;
                    this.TokenObjectControl.Character = value;
                }
            }
        }

        [Browsable(true)]
        public new ActionControl TokenObjectControl
        {
            get { return (ActionControl) this.tokenControl; }
            set { base.TokenObjectControl = value; }
        }

        protected override TokenObjectList<ResourceAction> Resources
        {
            get { return this.character == null ? null : this.character.Actions; }
        }

        #endregion

        #region Constructors

        public ActionsComboBox()
        {
            InitializeComponent();
        }

        public ActionsComboBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        #endregion

        #region Override Methods

        public new void RefreshResourceList()
        {
            if (this.manager == null) return;

            //refreshes list
            this.Items.Clear();
            foreach (ResourceAction resource in this.Resources)
            {
                this.Items.Add(resource.IdToken);
            }

            //refreshes current spot
            if (this.selectedResource == null)
            {
                this.SelectedItem = null;
                this.Text = "select...";
            }
            else if (this.Resources.Contains(this.selectedResource))
            {
                this.SelectedItem = this.selectedResource;
                this.Text = this.selectedResource.IdToken;
            }
            else
            {
                this.SelectedItem = null;
                this.SelectedIndex = -1;
                this.selectedResource = null;
                if (this.tokenControl != null) this.tokenControl.TokenObject = null;
                this.Text = "select...";
            }
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            base.OnSelectedIndexChanged(e);

            if (this.TokenObjectControl != null)
            {
                this.TokenObjectControl.TokenObject = this.SelectedResource;
            }
        }

        #endregion
    }
}