/* ****************************************************
 * Name: ItemsComboBox.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/09
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using PAGE.Generic.Resources.Assets;
using PAGE.Orient.GUI.Windows.Forms.Controls;
using PAGE.Util;

namespace PAGE.Orient.GUI.Windows.Forms.ComboBox
{
    public partial class ItemsComboBox : ResourcesComboBox<ResourceItem>
    {
        #region Properties

        [Browsable(true)]
        public new ItemControl TokenObjectControl
        {
            get { return (ItemControl)this.tokenControl; }
            set { base.TokenObjectControl = value; }
        }

        protected override TokenObjectList<ResourceItem> Resources
        {
            get { return this.manager.Items; }
        }

        #endregion

        #region Constructors

        public ItemsComboBox()
        {
            InitializeComponent();
        }

        public ItemsComboBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        } 

        #endregion

        #region Override Methods
        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            base.OnSelectedIndexChanged(e);

            if (this.TokenObjectControl != null)
            {
                this.TokenObjectControl.TokenObject = this.SelectedResource;
            }
        }

        #endregion
    }
}