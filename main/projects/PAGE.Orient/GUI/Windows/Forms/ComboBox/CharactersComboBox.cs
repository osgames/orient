/* ****************************************************
 * Name: CharactersComboBox.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/09
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using PAGE.Generic.Resources.Assets;
using PAGE.Orient.GUI.Windows.Forms.Controls;
using PAGE.Util;

namespace PAGE.Orient.GUI.Windows.Forms.ComboBox
{
    public partial class CharactersComboBox : ResourcesComboBox<ResourceHumanLikeCharacter>
    {
        #region Properties

        [Browsable(true)]
        public new CharacterControl TokenObjectControl
        {
            get { return (CharacterControl)this.tokenControl; }
            set { base.TokenObjectControl = value; }
        }

        protected override TokenObjectList<ResourceHumanLikeCharacter> Resources
        {
            get { return this.manager.Characters; }
        }

        #endregion

        #region Constructors

        public CharactersComboBox()
        {
            InitializeComponent();
        }

        public CharactersComboBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        } 

        #endregion

        #region Override Methods
        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            base.OnSelectedIndexChanged(e);

            if (this.TokenObjectControl != null)
            {
                this.TokenObjectControl.TokenObject = this.SelectedResource;
            }
        }

        #endregion
    }
}