/* ****************************************************
 * Name: SpotListComboBox.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/05
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using PAGE.Generic.Resources.Assets;
using PAGE.Generic.Resources.Spots;
using PAGE.Orient.GUI.Windows.Forms.Controls;
using PAGE.Util;

namespace PAGE.Orient.GUI.Windows.Forms.ComboBox
{
    public partial class SpotListComboBox<SpotType> : System.Windows.Forms.ComboBox
        where SpotType : ResourceSpot
    {
        public event EventHandler SelectedSpotChanged;

        #region Fields

        protected SpotControl spotControl;
        protected SpotType selectedSpot = null;
        protected ResourceAsset asset = null;

        protected bool includeAllSpots = false;

        #endregion

        #region Properties

        [Browsable(false)]
        protected virtual TokenObjectList<ResourceSpot> IncludedSpots
        {
            get
            {
                if (this.includeAllSpots) return this.asset.AllSpots;
                return new TokenObjectList<ResourceSpot>();
            }
        }

        public new bool Enabled
        {
            get { return base.Enabled; }
            set
            {
                if (this.asset == null)
                    base.Enabled = false;
                else
                    base.Enabled = value;
            }
        }

        [Browsable(false)]
        public ResourceAsset Asset
        {
            get { return asset; }
            set
            {
                this.asset = value;
                this.Enabled = this.asset != null;

                this.RefreshSpotList();

                if (this.spotControl != null)
                {
                    this.spotControl.Asset = this.asset;
                }
            }
        }

        [Browsable(false)]
        public virtual ResourceSpot SelectedSpot
        {
            get { return this.selectedSpot; }
        }

        [Browsable(true)]
        public SpotControl SpotControl
        {
            get { return this.spotControl; }
            set
            {
                this.spotControl = value;
                if (this.spotControl != null)
                {
                    this.spotControl.SpotListChanged += spotControl_SpotListChanged;
                    this.spotControl.SpotChanged += spotControl_SpotChanged;
                }
            }
        }

        [Browsable(true)]
        public bool IncludeAllSpots
        {
            get { return includeAllSpots; }
            set { includeAllSpots = value; }
        }

        #endregion

        #region Constructors

        public SpotListComboBox()
        {
            InitializeComponent();

            this.Sorted = true;
        }

        public SpotListComboBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();

            this.Sorted = true;
        } 

        #endregion

        #region Override Methods
        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            if ((this.SelectedItem == null) || (this.SelectedIndex == -1))
            {
                this.selectedSpot = null;
            }
            else if (this.IncludedSpots.Contains((string)this.SelectedItem))
            {
                this.selectedSpot = (SpotType)this.IncludedSpots[(string)this.SelectedItem];
            }
            else
            {
                this.selectedSpot = null;
            }

            if (this.spotControl != null)
            {
                this.spotControl.Spot = this.selectedSpot;
            }

            if (this.SelectedSpotChanged != null) 
                this.SelectedSpotChanged(this, EventArgs.Empty);

            base.OnSelectedIndexChanged(e);
        }
        #endregion

        #region Private Methods

        private void spotControl_SpotChanged(object sender, EventArgs e)
        {
            this.selectedSpot = (SpotType)this.spotControl.Spot;
        }

        private void spotControl_SpotListChanged(object sender, EventArgs e)
        {
            this.RefreshSpotList();
        }

        #endregion

        #region Public Methods

        public void RefreshSpotList()
        {
            if (this.asset == null) return;

            //refreshes list
            this.Items.Clear();
            foreach (ResourceSpot spot in this.IncludedSpots)
            {
                this.Items.Add(spot.IdToken);
            }

            //refreshes current spot
            if ((this.selectedSpot != null) && this.IncludedSpots.Contains(this.selectedSpot))
            {
                this.SelectedItem = this.selectedSpot;
                this.Text = this.selectedSpot.IdToken;
            }
            else
            {
                if (this.spotControl != null) this.spotControl.Spot = null;
                this.selectedSpot = null;
                this.SelectedItem = null;
                this.SelectedIndex = -1;
                this.Text = "select...";
            }
        }
        #endregion
    }
}