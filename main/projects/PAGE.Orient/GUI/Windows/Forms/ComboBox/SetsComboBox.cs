/* ****************************************************
 * Name: CharactersComboBox.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/09
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using PAGE.Generic.Resources.Assets;
using PAGE.OGRE.Resources.Assets;
using PAGE.Orient.GUI.Windows.Forms.Controls;
using PAGE.Util;

namespace PAGE.Orient.GUI.Windows.Forms.ComboBox
{
    public partial class SetsComboBox : ResourcesComboBox<ResourceSet>
    {
        #region Properties

        [Browsable(true)]
        public new SetControl TokenObjectControl
        {
            get { return (SetControl)this.tokenControl; }
            set { base.TokenObjectControl = value; }
        }

        protected override TokenObjectList<ResourceSet> Resources
        {
            get { return this.manager.Sets; }
        }

        #endregion

        #region Constructors

        public SetsComboBox()
        {
            InitializeComponent();
        }

        public SetsComboBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        } 

        #endregion

        #region Override Methods
        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            base.OnSelectedIndexChanged(e);

            if (this.TokenObjectControl != null)
            {
                this.TokenObjectControl.TokenObject = (OgreResourceSet)this.SelectedResource;
            }
        }

        #endregion
    }
}