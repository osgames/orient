/* ****************************************************
 * Name: WaypointListComboBox.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/06/23
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.ComponentModel;
using PAGE.Generic.Resources.Spots;
using PAGE.Util;

namespace PAGE.Orient.GUI.Windows.Forms.ComboBox
{
    public partial class WaypointListComboBox : SpotListComboBox<ResourceWaypoint>
    {
        #region Properties

        [Browsable(false)]
        protected override TokenObjectList<ResourceSpot> IncludedSpots
        {
            get
            {
                if (this.asset == null) return new TokenObjectList<ResourceSpot>();
                else
                {
                    TokenObjectList<ResourceSpot> list = new TokenObjectList<ResourceSpot>();
                    foreach (ResourceWaypoint spot in this.asset.ResourceWaypoints)
                        list.Add(spot);
                    return list;
                }
            }
        }

        #endregion

        #region Constructors

        public WaypointListComboBox()
        {
            InitializeComponent();
        }

        public WaypointListComboBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        } 

        #endregion
    }
}