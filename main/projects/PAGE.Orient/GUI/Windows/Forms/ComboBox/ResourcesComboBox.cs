/* ****************************************************
 * Name: ResourcesComboBox.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/08
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using PAGE.Orient.GUI.Windows.Forms.Controls;
using PAGE.Orient.Resources;
using PAGE.Util;

namespace PAGE.Orient.GUI.Windows.Forms.ComboBox
{
    public partial class ResourcesComboBox<ObjectType> : System.Windows.Forms.ComboBox
        where ObjectType : TokenObject
    {
        public event EventHandler SelectedResourceChanged;

        #region Fields

        protected TokenObjectControl<ObjectType> tokenControl;
        protected ObjectType selectedResource;
        protected OrientResourcesManager manager;

        #endregion

        #region Properties

        public new bool Enabled
        {
            get { return base.Enabled; }
            set
            {
                if (this.manager == null)
                    base.Enabled = false;
                else
                    base.Enabled = value;
            }
        }

        [Browsable(false)]
        public OrientResourcesManager ResourcesManager
        {
            get { return manager; }
            set
            {
                this.manager = value;
                this.Enabled = this.manager != null;

                this.RefreshResourceList();

                if (this.TokenObjectControl != null)
                {
                    this.TokenObjectControl.TokenObjectList = this.Resources;
                    this.TokenObjectControl.ResourcesManager = value;
                }
            }
        }

        [Browsable(false)]
        public ObjectType SelectedResource
        {
            get { return this.selectedResource; }
        }

        [Browsable(true)]
        public virtual TokenObjectControl<ObjectType> TokenObjectControl
        {
            get { return this.tokenControl; }
            set
            {
                this.tokenControl = value;
                if (this.tokenControl != null)
                {
                    this.tokenControl.TokenListChanged += tokenControl_TokenListChanged;
                    this.tokenControl.TokenChanged += tokenControl_TokenChanged;
                }
            }
        }

        protected virtual TokenObjectList<ObjectType> Resources
        {
            get { return new TokenObjectList<ObjectType>(); }
        }

        #endregion

        #region Constructors

        public ResourcesComboBox()
        {
            InitializeComponent();

            this.Sorted = true;
        }

        public ResourcesComboBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        #endregion

        #region Override Methods

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            if ((this.SelectedItem == null) || (this.SelectedIndex == -1))
            {
                this.selectedResource = null;
            }
            else if (this.Resources.Contains((string) this.SelectedItem))
            {
                this.selectedResource = this.Resources[(string) this.SelectedItem];
            }
            else
            {
                this.selectedResource = null;
            }

            if (this.TokenObjectControl != null)
            {
                this.TokenObjectControl.TokenObject = this.SelectedResource;
            }

            if (this.SelectedResourceChanged != null)
                this.SelectedResourceChanged(this, EventArgs.Empty);

            base.OnSelectedIndexChanged(e);
        }

        #endregion

        #region Private Methods

        protected virtual void tokenControl_TokenChanged(object sender, EventArgs e)
        {
            this.selectedResource = this.tokenControl.TokenObject;
        }

        protected virtual void tokenControl_TokenListChanged(object sender, EventArgs e)
        {
            this.RefreshResourceList();
        }

        #endregion

        #region Public Methods

        public void RefreshResourceList()
        {
            if (this.manager == null) return;

            //refreshes list
            this.Items.Clear();
            foreach (ObjectType resource in this.Resources)
            {
                this.Items.Add(resource.IdToken);
            }

            //refreshes current spot
            if (this.selectedResource == null)
            {
                this.SelectedItem = null;
                this.Text = "select...";
            }
            else if (this.Resources.Contains(this.selectedResource))
            {
                this.SelectedItem = this.selectedResource;
                this.Text = this.selectedResource.IdToken;
            }
            else
            {
                this.SelectedItem = null;
                this.SelectedIndex = -1;
                this.selectedResource = null;
                if (this.tokenControl != null) this.tokenControl.TokenObject = null;
                this.Text = "select...";
            }
        }

        #endregion
    }
}