/* ****************************************************
 * Name: PreviewPanel.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/06/19 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using PAGE.OGRE.Math;
using PAGE.Orient.Domain.Cameras;
using Timer=System.Threading.Timer;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    public partial class PreviewPanel : Panel
    {
        #region Fields

        //navigation on/off
        protected bool allowNavigation;

        //timer to control navigation
        protected Timer timer;

        //timer update interval, milliseconds
        protected long updateInterval = 10;

        //last update time
        protected DateTime lastTime = DateTime.Now;

        //movement variables
        protected int roll;
        protected int pitch;
        protected int yaw;
        protected int moveCam; //bits: 1=forward, 2=backward, 4=left, 8=right, 16=up, 32=down
        protected float moveSpeed = 50.0f;
        protected float rotateSpeed = 50.0f;

        //mouse variables
        protected int oldX;
        protected int oldY;

        //device capture
        protected bool captureMouse = true;
        protected bool captureKeyboard = true;

        #endregion

        #region Properties

        [Browsable(true)]
        public float MoveSpeed
        {
            get { return moveSpeed; }
            set { moveSpeed = value; }
        }

        [Browsable(true)]
        public float RotateSpeed
        {
            get { return rotateSpeed; }
            set { rotateSpeed = value; }
        }

        [Browsable(false)]
        public bool AllowNavigation
        {
            get { return allowNavigation; }
            set
            {
                this.allowNavigation = value;

                if (value && (this.timer != null))
                {
                    this.timer.Change(0, this.updateInterval); //enables timer
                    this.lastTime = DateTime.Now;
                }
                else
                    this.timer.Change(Timeout.Infinite, Timeout.Infinite); //disables timer
            }
        }

        [Browsable(true)]
        public long UpdateInterval
        {
            get { return updateInterval; }
            set { updateInterval = value; }
        }

        [Browsable(true)]
        public bool CaptureMouse
        {
            get { return captureMouse; }
            set { captureMouse = value; }
        }

        [Browsable(true)]
        public bool CaptureKeyboard
        {
            get { return captureKeyboard; }
            set { captureKeyboard = value; }
        }

        #endregion

        #region Constructors

        public PreviewPanel()
        {
            InitializeComponent();

            this.Init();
        }

        public PreviewPanel(IContainer container)
        {
            container.Add(this);

            InitializeComponent();

            this.Init();
        }

        #endregion

        #region Protected Methods

        protected void Init()
        {
            //creates timer
            TimerCallback timerDelegate = this.TimerReachedEnd;
            this.timer = new Timer(timerDelegate, null, Timeout.Infinite, Timeout.Infinite);
        }

        protected override void OnParentChanged(EventArgs e)
        {
            Form parentForm = this.FindForm();
            if (parentForm != null)
            {
                parentForm.KeyPreview = true;
                parentForm.KeyDown += parentForm_KeyDown;
                parentForm.KeyUp += parentForm_KeyUp;
            }
        }

        protected void TimerReachedEnd(Object stateInfo)
        {
            float timeSinceLastUpdate = (float) DateTime.Now.Subtract(this.lastTime).TotalSeconds;
            this.lastTime = DateTime.Now;

            //update camera direction
            if ((this.roll & 1) > 0)
                OrientGraphicsRealizer.Instance.Camera.Roll(this.rotateSpeed*timeSinceLastUpdate);
            if ((this.roll & 2) > 0)
                OrientGraphicsRealizer.Instance.Camera.Roll(-this.rotateSpeed*timeSinceLastUpdate);
            if ((this.pitch & 1) > 0)
                OrientGraphicsRealizer.Instance.Camera.Pitch(this.rotateSpeed*timeSinceLastUpdate);
            if ((this.pitch & 2) > 0)
                OrientGraphicsRealizer.Instance.Camera.Pitch(-this.rotateSpeed*timeSinceLastUpdate);
            if ((this.yaw & 1) > 0)
                OrientGraphicsRealizer.Instance.Camera.Yaw(this.rotateSpeed*timeSinceLastUpdate);
            if ((this.yaw & 2) > 0)
                OrientGraphicsRealizer.Instance.Camera.Yaw(-this.rotateSpeed*timeSinceLastUpdate);

            //update camera navigation
            if (this.moveCam > 0)
            {
                //mMoveCam bits: 1=forward, 2=backward, 4=left, 8=right, 16=up, 32=down
                OgrePosition camMove = new OgrePosition(0, 0, 0);
                float mvscale = this.moveSpeed*timeSinceLastUpdate;

                if ((this.moveCam & 1) > 0)
                    camMove = (OgrePosition) camMove.Add(new OgrePosition(0, 0, -1));
                if ((this.moveCam & 2) > 0)
                    camMove = (OgrePosition) camMove.Add(new OgrePosition(0, 0, 1));
                if ((this.moveCam & 4) > 0)
                    camMove = (OgrePosition) camMove.Add(new OgrePosition(-1, 0, 0));
                if ((this.moveCam & 8) > 0)
                    camMove = (OgrePosition) camMove.Add(new OgrePosition(1, 0, 0));
                if ((this.moveCam & 16) > 0)
                    camMove = (OgrePosition) camMove.Add(new OgrePosition(0, 1, 0));
                if ((this.moveCam & 32) > 0)
                    camMove = (OgrePosition) camMove.Add(new OgrePosition(0, -1, 0));

                camMove = new OgrePosition(camMove.X*mvscale, camMove.Y*mvscale, camMove.Z*mvscale);
                OrientGraphicsRealizer.Instance.Camera.MoveRelative(camMove);
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            //in navigation mode and with middle button pressed
            if (!this.allowNavigation || !this.captureMouse || (e.Button != MouseButtons.Middle)) return;

            //calculates delta movement
            int deltaX = this.oldX - e.X;
            int deltaY = this.oldY - e.Y;

            OrientGraphicsRealizer.Instance.Camera.Pitch(deltaY*this.rotateSpeed*0.005);
            OrientGraphicsRealizer.Instance.Camera.Yaw(deltaX*this.rotateSpeed*0.005);

            this.oldX = e.X;
            this.oldY = e.Y;

            if (this.oldX <= 0) this.yaw |= 1;
            else if (this.oldX >= this.Width) this.yaw |= 2;
            else this.yaw = 0;

            if (this.oldY <= 0) this.pitch |= 1;
            else if (this.oldY >= this.Height) this.pitch |= 2;
            else this.pitch = 0;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            if (!this.allowNavigation || !this.captureMouse) return;

            switch (e.Button)
            {
                case MouseButtons.Left:
                    this.moveCam |= 1;
                    break;
                case MouseButtons.Right:
                    this.moveCam |= 2;
                    break;
                case MouseButtons.Middle:
                    this.oldX = e.X;
                    this.oldY = e.Y;
                    break;
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);

            if (!this.allowNavigation || !this.captureMouse) return;

            switch (e.Button)
            {
                case MouseButtons.Left:
                    this.moveCam &= ~1;
                    break;
                case MouseButtons.Right:
                    this.moveCam &= ~2;
                    break;
                case MouseButtons.Middle:
                    this.pitch = this.yaw = 0;
                    break;
            }
        }

        protected void parentForm_KeyDown(object sender, KeyEventArgs e)
        {

            if (!this.allowNavigation || !this.captureKeyboard) return;

            // keyboard mappings for free camera mode
            if (OrientGraphicsRealizer.Instance.Camera.CameraMode == CameraMode.Free)
            {
                switch (e.KeyCode)
                {
                    case Keys.W:
                        this.moveCam |= 1;
                        break;
                    case Keys.S:
                        this.moveCam |= 2;
                        break;
                    case Keys.A:
                        this.moveCam |= 4;
                        break;
                    case Keys.D:
                        this.moveCam |= 8;
                        break;
                    case Keys.PageUp:
                    case Keys.Q:
                        this.moveCam |= 16;
                        break;
                    case Keys.PageDown:
                    case Keys.Z:
                        this.moveCam |= 32;
                        break;

                    case Keys.Insert:
                        this.roll |= 1;
                        break;
                    case Keys.Delete:
                        this.roll |= 2;
                        break;
                    case Keys.Up:
                        this.pitch |= 1;
                        break;
                    case Keys.Down:
                        this.pitch |= 2;
                        break;
                    case Keys.Left:
                        this.yaw |= 1;
                        break;
                    case Keys.Right:
                        this.yaw |= 2;
                        break;
                }

            }
        }

        protected void parentForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (!this.allowNavigation || !this.captureKeyboard) return;

            // keyboard mappings for free camera mode
            if (OrientGraphicsRealizer.Instance.Camera.CameraMode == CameraMode.Free)              
            {
                switch (e.KeyCode)
                {
                    case Keys.W:
                        this.moveCam &= ~1;
                        break;
                    case Keys.S:
                        this.moveCam &= ~2;
                        break;
                    case Keys.A:
                        this.moveCam &= ~4;
                        break;
                    case Keys.D:
                        this.moveCam &= ~8;
                        break;
                    case Keys.PageUp:
                    case Keys.Q:
                        this.moveCam &= ~16;
                        break;
                    case Keys.PageDown:
                    case Keys.Z:
                        this.moveCam &= ~32;
                        break;

                    case Keys.Insert:
                        this.roll &= ~1;
                        break;
                    case Keys.Delete:
                        this.roll &= ~2;
                        break;
                    case Keys.Up:
                        this.pitch &= ~1;
                        break;
                    case Keys.Down:
                        this.pitch &= ~2;
                        break;
                    case Keys.Left:
                        this.yaw &= ~1;
                        break;
                    case Keys.Right:
                        this.yaw &= ~2;
                        break;
                }
            }
        }

        #endregion
    }
}