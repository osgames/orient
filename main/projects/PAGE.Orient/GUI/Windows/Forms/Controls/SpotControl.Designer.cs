using System;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    partial class SpotControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.spotsGroupBox = new System.Windows.Forms.GroupBox();
            this.spotTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.spotIDTxtBox = new System.Windows.Forms.TextBox();
            this.addSTagBtn = new System.Windows.Forms.Button();
            this.removeSTagBtn = new System.Windows.Forms.Button();
            this.removeAllSTagBtn = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.spotTagsListBox = new System.Windows.Forms.ListView();
            this.dsZtxtBox = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.dsYtxtBox = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.dsXtxtBox = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tsZtxtBox = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.tsYtxtBox = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.tsXtxtBox = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.spotsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsZtxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsYtxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsXtxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tsZtxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tsYtxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tsXtxtBox)).BeginInit();
            this.SuspendLayout();
            // 
            // spotsGroupBox
            // 
            this.spotsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.spotsGroupBox.Controls.Add(this.spotTypeComboBox);
            this.spotsGroupBox.Controls.Add(this.label1);
            this.spotsGroupBox.Controls.Add(this.label29);
            this.spotsGroupBox.Controls.Add(this.spotIDTxtBox);
            this.spotsGroupBox.Controls.Add(this.addSTagBtn);
            this.spotsGroupBox.Controls.Add(this.removeSTagBtn);
            this.spotsGroupBox.Controls.Add(this.removeAllSTagBtn);
            this.spotsGroupBox.Controls.Add(this.label9);
            this.spotsGroupBox.Controls.Add(this.spotTagsListBox);
            this.spotsGroupBox.Controls.Add(this.dsZtxtBox);
            this.spotsGroupBox.Controls.Add(this.label5);
            this.spotsGroupBox.Controls.Add(this.dsYtxtBox);
            this.spotsGroupBox.Controls.Add(this.label6);
            this.spotsGroupBox.Controls.Add(this.dsXtxtBox);
            this.spotsGroupBox.Controls.Add(this.label7);
            this.spotsGroupBox.Controls.Add(this.label8);
            this.spotsGroupBox.Controls.Add(this.tsZtxtBox);
            this.spotsGroupBox.Controls.Add(this.label23);
            this.spotsGroupBox.Controls.Add(this.tsYtxtBox);
            this.spotsGroupBox.Controls.Add(this.label25);
            this.spotsGroupBox.Controls.Add(this.tsXtxtBox);
            this.spotsGroupBox.Controls.Add(this.label26);
            this.spotsGroupBox.Controls.Add(this.label27);
            this.spotsGroupBox.Location = new System.Drawing.Point(3, 3);
            this.spotsGroupBox.Name = "spotsGroupBox";
            this.spotsGroupBox.Size = new System.Drawing.Size(318, 221);
            this.spotsGroupBox.TabIndex = 13;
            this.spotsGroupBox.TabStop = false;
            this.spotsGroupBox.Text = "Interaction Spot";
            // 
            // spotTypeComboBox
            // 
            this.spotTypeComboBox.FormattingEnabled = true;
            this.spotTypeComboBox.Items.AddRange(new object[] {
            "Camera",
            "Item",
            "Waypoint"});
            this.spotTypeComboBox.Location = new System.Drawing.Point(82, 95);
            this.spotTypeComboBox.Name = "spotTypeComboBox";
            this.spotTypeComboBox.Size = new System.Drawing.Size(151, 21);
            this.spotTypeComboBox.Sorted = true;
            this.spotTypeComboBox.TabIndex = 102;
            this.spotTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.spotTypeComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 101;
            this.label1.Text = "Spot Type:";
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(31, 22);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(46, 13);
            this.label29.TabIndex = 99;
            this.label29.Text = "Spot ID:";
            // 
            // spotIDTxtBox
            // 
            this.spotIDTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.spotIDTxtBox.Location = new System.Drawing.Point(82, 19);
            this.spotIDTxtBox.Name = "spotIDTxtBox";
            this.spotIDTxtBox.Size = new System.Drawing.Size(230, 20);
            this.spotIDTxtBox.TabIndex = 17;
            this.spotIDTxtBox.Validated += new System.EventHandler(this.spotIDTxtBox_Validated);
            // 
            // addSTagBtn
            // 
            this.addSTagBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.addSTagBtn.Location = new System.Drawing.Point(238, 135);
            this.addSTagBtn.Name = "addSTagBtn";
            this.addSTagBtn.Size = new System.Drawing.Size(75, 23);
            this.addSTagBtn.TabIndex = 25;
            this.addSTagBtn.Text = "Add";
            this.addSTagBtn.UseVisualStyleBackColor = true;
            this.addSTagBtn.Click += new System.EventHandler(this.addSTagBtn_Click);
            // 
            // removeSTagBtn
            // 
            this.removeSTagBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.removeSTagBtn.Enabled = false;
            this.removeSTagBtn.Location = new System.Drawing.Point(238, 164);
            this.removeSTagBtn.Name = "removeSTagBtn";
            this.removeSTagBtn.Size = new System.Drawing.Size(75, 23);
            this.removeSTagBtn.TabIndex = 26;
            this.removeSTagBtn.Text = "Remove";
            this.removeSTagBtn.UseVisualStyleBackColor = true;
            this.removeSTagBtn.Click += new System.EventHandler(this.removeSTagBtn_Click);
            // 
            // removeAllSTagBtn
            // 
            this.removeAllSTagBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.removeAllSTagBtn.Enabled = false;
            this.removeAllSTagBtn.Location = new System.Drawing.Point(238, 193);
            this.removeAllSTagBtn.Name = "removeAllSTagBtn";
            this.removeAllSTagBtn.Size = new System.Drawing.Size(75, 23);
            this.removeAllSTagBtn.TabIndex = 27;
            this.removeAllSTagBtn.Text = "Remove All";
            this.removeAllSTagBtn.UseVisualStyleBackColor = true;
            this.removeAllSTagBtn.Click += new System.EventHandler(this.removeAllSTagBtn_Click);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 92;
            this.label9.Text = "Spot tags:";
            // 
            // spotTagsListBox
            // 
            this.spotTagsListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.spotTagsListBox.GridLines = true;
            this.spotTagsListBox.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.spotTagsListBox.LabelEdit = true;
            this.spotTagsListBox.Location = new System.Drawing.Point(9, 133);
            this.spotTagsListBox.Name = "spotTagsListBox";
            this.spotTagsListBox.Size = new System.Drawing.Size(223, 82);
            this.spotTagsListBox.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.spotTagsListBox.TabIndex = 91;
            this.spotTagsListBox.UseCompatibleStateImageBehavior = false;
            this.spotTagsListBox.View = System.Windows.Forms.View.List;
            this.spotTagsListBox.Validated += new System.EventHandler(this.spotTagsListBox_Validated);
            // 
            // dsZtxtBox
            // 
            this.dsZtxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dsZtxtBox.DecimalPlaces = 2;
            this.dsZtxtBox.Location = new System.Drawing.Point(258, 71);
            this.dsZtxtBox.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.dsZtxtBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.dsZtxtBox.Name = "dsZtxtBox";
            this.dsZtxtBox.Size = new System.Drawing.Size(55, 20);
            this.dsZtxtBox.TabIndex = 23;
            this.dsZtxtBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.dsZtxtBox.ValueChanged += new System.EventHandler(this.dsZtxtBox_ValueChanged);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(239, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 13);
            this.label5.TabIndex = 90;
            this.label5.Text = "z = ";
            // 
            // dsYtxtBox
            // 
            this.dsYtxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dsYtxtBox.DecimalPlaces = 2;
            this.dsYtxtBox.Location = new System.Drawing.Point(178, 71);
            this.dsYtxtBox.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.dsYtxtBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.dsYtxtBox.Name = "dsYtxtBox";
            this.dsYtxtBox.Size = new System.Drawing.Size(55, 20);
            this.dsYtxtBox.TabIndex = 22;
            this.dsYtxtBox.ValueChanged += new System.EventHandler(this.dsYtxtBox_ValueChanged);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(159, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 89;
            this.label6.Text = "y = ";
            // 
            // dsXtxtBox
            // 
            this.dsXtxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dsXtxtBox.DecimalPlaces = 2;
            this.dsXtxtBox.Location = new System.Drawing.Point(98, 71);
            this.dsXtxtBox.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.dsXtxtBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.dsXtxtBox.Name = "dsXtxtBox";
            this.dsXtxtBox.Size = new System.Drawing.Size(55, 20);
            this.dsXtxtBox.TabIndex = 21;
            this.dsXtxtBox.ValueChanged += new System.EventHandler(this.dsXtxtBox_ValueChanged);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(79, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 88;
            this.label7.Text = "x = ";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 87;
            this.label8.Text = "Direction:";
            // 
            // tsZtxtBox
            // 
            this.tsZtxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tsZtxtBox.DecimalPlaces = 2;
            this.tsZtxtBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.tsZtxtBox.Location = new System.Drawing.Point(258, 45);
            this.tsZtxtBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.tsZtxtBox.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.tsZtxtBox.Name = "tsZtxtBox";
            this.tsZtxtBox.Size = new System.Drawing.Size(55, 20);
            this.tsZtxtBox.TabIndex = 20;
            this.tsZtxtBox.ValueChanged += new System.EventHandler(this.tsZtxtBox_ValueChanged);
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(239, 47);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(24, 13);
            this.label23.TabIndex = 86;
            this.label23.Text = "z = ";
            // 
            // tsYtxtBox
            // 
            this.tsYtxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tsYtxtBox.DecimalPlaces = 2;
            this.tsYtxtBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.tsYtxtBox.Location = new System.Drawing.Point(178, 45);
            this.tsYtxtBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.tsYtxtBox.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.tsYtxtBox.Name = "tsYtxtBox";
            this.tsYtxtBox.Size = new System.Drawing.Size(55, 20);
            this.tsYtxtBox.TabIndex = 19;
            this.tsYtxtBox.ValueChanged += new System.EventHandler(this.tsYtxtBox_ValueChanged);
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(159, 47);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(24, 13);
            this.label25.TabIndex = 85;
            this.label25.Text = "y = ";
            // 
            // tsXtxtBox
            // 
            this.tsXtxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tsXtxtBox.DecimalPlaces = 2;
            this.tsXtxtBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.tsXtxtBox.Location = new System.Drawing.Point(98, 45);
            this.tsXtxtBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.tsXtxtBox.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.tsXtxtBox.Name = "tsXtxtBox";
            this.tsXtxtBox.Size = new System.Drawing.Size(55, 20);
            this.tsXtxtBox.TabIndex = 18;
            this.tsXtxtBox.ValueChanged += new System.EventHandler(this.tsXtxtBox_ValueChanged);
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(79, 47);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(24, 13);
            this.label26.TabIndex = 83;
            this.label26.Text = "x = ";
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(30, 47);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(47, 13);
            this.label27.TabIndex = 80;
            this.label27.Text = "Position:";
            // 
            // SpotControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.spotsGroupBox);
            this.Name = "SpotControl";
            this.Size = new System.Drawing.Size(323, 229);
            this.spotsGroupBox.ResumeLayout(false);
            this.spotsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsZtxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsYtxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsXtxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tsZtxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tsYtxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tsXtxtBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox spotsGroupBox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox spotIDTxtBox;
        private System.Windows.Forms.Button addSTagBtn;
        private System.Windows.Forms.Button removeSTagBtn;
        private System.Windows.Forms.Button removeAllSTagBtn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListView spotTagsListBox;
        private System.Windows.Forms.NumericUpDown dsZtxtBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown dsYtxtBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown dsXtxtBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown tsZtxtBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown tsYtxtBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.NumericUpDown tsXtxtBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox spotTypeComboBox;
    }
}