/* ****************************************************
 * Name: TokenObjectControl.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/08
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using System.Windows.Forms;
using PAGE.Orient.Resources;
using PAGE.Util;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    public partial class TokenObjectControl<ResourceType> :
        UserControl where ResourceType : TokenObject
    {
        public event EventHandler TokenChanged;
        public event EventHandler TokenListChanged;

        #region Fields

        protected ResourceType tokenObject;
        protected TokenObjectList<ResourceType> tokenObjectList;
        protected OrientResourcesManager manager;

        #endregion

        #region Properties

        public new bool Enabled
        {
            get { return base.Enabled; }
            set
            {
                if (this.tokenObjectList == null)
                    base.Enabled = false;
                else
                    base.Enabled = value;
            }
        }

        [Browsable(false)]
        public TokenObjectList<ResourceType> TokenObjectList
        {
            get { return tokenObjectList; }
            set
            {
                this.tokenObjectList = value;
                this.Enabled = this.tokenObjectList != null;
            }
        }

        [Browsable(false)]
        public OrientResourcesManager ResourcesManager
        {
            get { return manager; }
            set { this.manager = value; }
        }

        [Browsable(false)]
        public virtual ResourceType TokenObject
        {
            get { return this.tokenObject; }
            set
            {
                this.tokenObject = value;
                if (this.tokenObject == null)
                {
                    this.tokenGroupBox.Enabled = false;

                    this.idTxtBox.Text = "";
                    this.descTxtBox.Text = "";
                }
                else
                {
                    this.tokenGroupBox.Enabled = true;

                    this.idTxtBox.Text = this.tokenObject.IdToken;
                    this.descTxtBox.Text = this.tokenObject.Description;
                }
            }
        }

        [Browsable(true)]
        public new string Text
        {
            get { return this.tokenGroupBox.Text; }
            set { this.tokenGroupBox.Text = value; }
        }

        #endregion

        #region Constructor

        public TokenObjectControl()
        {
            InitializeComponent();
            base.Enabled = false;
        }

        #endregion

        #region Controls

        protected virtual void spotIDTxtBox_Validated(object sender, EventArgs e)
        {
            string newSpotID = idTxtBox.Text;
            if (newSpotID != this.tokenObject.IdToken)
            {
                if (this.tokenObjectList.Contains(newSpotID))
                {
                    MessageBox.Show(this.ExistentIDMessage, "",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.idTxtBox.SelectAll();
                    this.idTxtBox.Focus();
                }
                else
                {
                    this.tokenObjectList.Remove(this.tokenObject);
                    this.tokenObject.IdToken = newSpotID;
                    this.tokenObjectList.Add(this.tokenObject);

                    this.RaiseTokenChanged();
                    this.RaiseTokenListChanged();
                }
            }
        }

        protected virtual void descTxtBox_TextChanged(object sender, EventArgs e)
        {
            this.tokenObject.Description = this.descTxtBox.Text;
            this.RaiseTokenChanged();
        }

        #endregion

        #region Abstract & Virtual Methods

        protected virtual string ExistentIDMessage
        {
            get
            {
                return "There is already a token with that ID\n" +
                       "Please choose another ID.";
            }
        }

        protected virtual void RaiseTokenListChanged()
        {
            if (this.TokenListChanged != null) this.TokenListChanged(this, EventArgs.Empty);
        }

        protected virtual void RaiseTokenChanged()
        {
            if (this.TokenChanged != null) this.TokenChanged(this, EventArgs.Empty);
        }

        #endregion
    }
}