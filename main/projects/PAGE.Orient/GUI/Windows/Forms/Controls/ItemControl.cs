/* ****************************************************
 * Name: ItemControl.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/09
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using System.Windows.Forms;
using PAGE.Generic.Resources.Assets;
using PAGE.OGRE.Domain.Assets;
using PAGE.Orient.Resources;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    public partial class ItemControl : ResourceAssetControl<ResourceItem>
    {
        #region Fields

        protected OgreItem genericItem = null;

        #endregion

        #region Properties

        [Browsable(false)]
        public OgreItem GenericItem
        {
            get { return genericItem; }
            set { genericItem = value; }
        }

        [Browsable(false)]
        public new ResourceItem TokenObject
        {
            get { return this.tokenObject; }
            set { base.TokenObject = value; }
        }

        [Browsable(false)]
        public new OrientResourcesManager ResourcesManager
        {
            get { return base.ResourcesManager; }
            set
            {
                base.ResourcesManager = value;
                if (value != null) this.TokenObjectList = ResourcesManager.Items;
            }
        }

        #endregion

        #region Constructor
        public ItemControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Private Methods

        protected override string ExistentIDMessage
        {
            get
            {
                return "There is already an item with that name.\n" +
                       "Please choose another item ID.";
            }
        }
        #endregion
    }
}