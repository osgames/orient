/* ****************************************************
 * Name: SpotControl.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/05
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using System.Windows.Forms;
using PAGE.Generic.Resources.Assets;
using PAGE.Generic.Resources.Spots;
using PAGE.OGRE.Math;
using PAGE.Orient.GUI.Windows.Forms.Util;
using PAGE.Util;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    public partial class SpotControl : UserControl
    {
        public event EventHandler SpotChanged;
        public event EventHandler SpotListChanged; 

        #region Fields

        protected ResourceSpot spot = null;
        protected ResourceAsset asset = null;

        #endregion

        #region Properties

        public new bool Enabled
        {
            get { return base.Enabled; }
            set
            {
                if (this.asset == null)
                    base.Enabled = false;
                else
                    base.Enabled = value;
            }
        }

        [Browsable(false)]
        public ResourceAsset Asset
        {
            get { return asset; }
            set
            {
                this.asset = value;
                this.Enabled = this.asset != null;
            }
        }

        [Browsable(false)]
        public ResourceSpot Spot
        {
            get { return this.spot; }
            set
            {
                this.spot = value;
                if (this.spot == null)
                {
                    this.spotsGroupBox.Enabled = false;
                }
                else
                {
                    this.spotsGroupBox.Enabled = true;
                    OgrePosition position = (OgrePosition)this.spot.RelativePosition;
                    this.tsXtxtBox.Value = (decimal) position.X;
                    this.tsYtxtBox.Value = (decimal) position.Y;
                    this.tsZtxtBox.Value = (decimal) position.Z;
                    OgreDirection direction = (OgreDirection)this.spot.RelativeDirection;
                    this.dsXtxtBox.Value = (decimal) direction.X;
                    this.dsYtxtBox.Value = (decimal) direction.Y;
                    this.dsZtxtBox.Value = (decimal) direction.Z;

                    this.spotIDTxtBox.Enabled = true;
                    this.spotIDTxtBox.Text = this.spot.IdToken;

                    if (this.spot is ResourceCameraSpot)
                    {
                        this.spotTypeComboBox.Text = "Camera";
                    }
                    else if (this.spot is ResourceWaypoint)
                    {
                        this.spotTypeComboBox.Text = "Waypoint";
                    }
                    else if (this.spot is ResourceItemSpot)
                    {
                        this.spotTypeComboBox.Text = "Item";
                    }
                }

                this.RefreshSpotTags();
            }
        }

        [Browsable(false)]
        protected TokenObjectList<ResourceCameraSpot> ResourceCameraSpots
        {
            get { return this.asset.ResourceCameraSpots; }
        }

        [Browsable(false)]
        protected TokenObjectList<ResourceWaypoint> ResourceWaypoints
        {
            get { return this.asset.ResourceWaypoints; }
        }

        [Browsable(false)]
        protected TokenObjectList<ResourceItemSpot> ResourceItemSpots
        {
            get { return this.asset.ResourceItemSpots; }
        }

        [Browsable(true)]
        public new string Text
        {
            get { return this.spotsGroupBox.Text; }
            set { this.spotsGroupBox.Text = value; }
        }

        [Browsable(true)]
        public bool EditableSpotType
        {
            get { return this.spotTypeComboBox.Enabled; }
            set { this.spotTypeComboBox.Enabled = value; }
        }

        #endregion

        #region Constructor
        public SpotControl()
        {
            InitializeComponent();
            base.Enabled = false;
        }

        #endregion

        #region Controls

        private void spotIDTxtBox_Validated(object sender, EventArgs e)
        {
            string newSpotID = spotIDTxtBox.Text;
            if (newSpotID != this.spot.IdToken)
            {
                if (this.ResourceCameraSpots.Contains(newSpotID) ||
                    this.ResourceWaypoints.Contains(newSpotID))
                {
                    MessageBox.Show("There is already a selectedSpot with that name.\n" +
                                    "Please choose another one.", "Spot Editor", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                    this.spotIDTxtBox.SelectAll();
                    this.spotIDTxtBox.Focus();
                }
                else
                {
                    this.spot.Tags.Remove(this.spot.IdToken);

                    if (this.spot is ResourceWaypoint)
                    {
                        this.ResourceWaypoints.Remove(this.spot.IdToken);
                        this.spot.IdToken = newSpotID;
                        this.ResourceWaypoints.Add((ResourceWaypoint)this.spot);
                    }
                    else if (this.spot is ResourceCameraSpot)
                    {
                        this.ResourceCameraSpots.Remove(this.spot.IdToken);
                        this.spot.IdToken = newSpotID;
                        this.ResourceCameraSpots.Add((ResourceCameraSpot)this.spot);
                    }
                    else if (this.spot is ResourceItemSpot)
                    {
                        this.ResourceItemSpots.Remove(this.spot.IdToken);
                        this.spot.IdToken = newSpotID;
                        this.ResourceItemSpots.Add((ResourceItemSpot)this.spot);
                    }

                    this.spot.Tags.Add(newSpotID);

                    this.RaiseSpotChanged();
                    this.RaiseSpotListChanged();
                }
            }
        }

        private void tsXtxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.spot.RelativePosition = new OgrePosition(
                GUIUtil.ReadVector(this.tsXtxtBox, this.tsYtxtBox, this.tsZtxtBox));
            this.RaiseSpotChanged();
        }

        private void tsYtxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.spot.RelativePosition = new OgrePosition(
                GUIUtil.ReadVector(this.tsXtxtBox, this.tsYtxtBox, this.tsZtxtBox));
            this.RaiseSpotChanged();
        }

        private void tsZtxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.spot.RelativePosition = new OgrePosition(
                GUIUtil.ReadVector(this.tsXtxtBox, this.tsYtxtBox, this.tsZtxtBox));
            this.RaiseSpotChanged();
        }

        private void dsXtxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.spot.RelativeDirection = new OgreDirection(
                GUIUtil.ReadVector(this.dsXtxtBox, this.dsYtxtBox, this.dsZtxtBox));
            this.RaiseSpotChanged();
        }

        private void dsYtxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.spot.RelativeDirection = new OgreDirection(
                GUIUtil.ReadVector(this.dsXtxtBox, this.dsYtxtBox, this.dsZtxtBox));
            this.RaiseSpotChanged();
        }

        private void dsZtxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.spot.RelativeDirection = new OgreDirection(
                GUIUtil.ReadVector(this.dsXtxtBox, this.dsYtxtBox, this.dsZtxtBox));
            this.RaiseSpotChanged();
        }

        private void spotTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((this.ResourceWaypoints.Contains(this.spot.IdToken) ||
                 this.ResourceItemSpots.Contains(this.spot.IdToken)) &&
                this.spotTypeComboBox.Text == "Camera")
            {
                this.ResourceWaypoints.Remove(this.spot.IdToken);
                this.ResourceItemSpots.Remove(this.spot.IdToken);

                ResourceCameraSpot newSpot = 
                    new ResourceCameraSpot(this.spot.IdToken, 
                    OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
                newSpot.RelativePosition = this.spot.RelativePosition;
                newSpot.Description = this.spot.Description;
                newSpot.RelativeDirection = this.spot.RelativeDirection;
                foreach (string tag in this.spot.Tags)
                {
                    newSpot.Tags.Add(tag);
                }
                this.ResourceCameraSpots.Add(newSpot);
                this.spot = newSpot;

                this.RaiseSpotChanged();
            }
            else if ((this.ResourceCameraSpots.Contains(this.spot.IdToken) ||
                this.ResourceItemSpots.Contains(this.spot.IdToken)) &&
                this.spotTypeComboBox.Text == "Waypoint")
            {
                this.ResourceCameraSpots.Remove(this.spot.IdToken);
                this.ResourceItemSpots.Remove(this.spot.IdToken);

                ResourceWaypoint newSpot = new ResourceWaypoint(this.spot.IdToken,
                    OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
                newSpot.RelativePosition = this.spot.RelativePosition;
                newSpot.Description = this.spot.Description;
                newSpot.RelativeDirection = this.spot.RelativeDirection;
                foreach (string tag in this.spot.Tags)
                {
                    newSpot.Tags.Add(tag);
                }
                this.ResourceWaypoints.Add(newSpot);
                this.spot = newSpot;

                this.RaiseSpotChanged();
            }
            else if ((this.ResourceCameraSpots.Contains(this.spot.IdToken) ||
                this.ResourceWaypoints.Contains(this.spot.IdToken)) &&
                this.spotTypeComboBox.Text == "Item")
            {
                this.ResourceCameraSpots.Remove(this.spot.IdToken);
                this.ResourceWaypoints.Remove(this.spot.IdToken);

                ResourceItemSpot newSpot = new ResourceItemSpot(this.spot.IdToken,
                    OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
                newSpot.RelativePosition = this.spot.RelativePosition;
                newSpot.Description = this.spot.Description;
                newSpot.RelativeDirection = this.spot.RelativeDirection;
                foreach (string tag in this.spot.Tags)
                {
                    newSpot.Tags.Add(tag);
                }
                this.ResourceItemSpots.Add(newSpot);
                this.spot = newSpot;

                this.RaiseSpotChanged();
            }
        }

        private void spotTagsListBox_Validated(object sender, EventArgs e)
        {
            if (this.spot == null) return;
            this.spot.Tags.Clear();
            foreach (ListViewItem tag in this.spotTagsListBox.Items)
            {
                if (!this.spot.Tags.Contains(tag.Text))
                    this.spot.Tags.Add(tag.Text);
            }
        }
        #endregion

        #region Buttons

        private void addSTagBtn_Click(object sender, EventArgs e)
        {
            if (this.spot == null) return;
            int count = 0;
            while (this.spot.Tags.Contains("tag " + count))
            {
                count++;
            }
            this.spot.Tags.Add("tag " + count);
            this.RefreshSpotTags();
        }

        private void removeSTagBtn_Click(object sender, EventArgs e)
        {
            if (this.spot == null) return;
            foreach (ListViewItem tag in this.spotTagsListBox.SelectedItems)
            {
                this.spot.Tags.Remove(tag.Text);
            }
            this.RefreshSpotTags();
        }

        private void removeAllSTagBtn_Click(object sender, EventArgs e)
        {
            if (this.spot == null) return;
            this.spot.Tags.Clear();
            this.RefreshSpotTags();
        }

        #endregion

        #region Private Methods

        private void RefreshSpotTags()
        {
            this.spotTagsListBox.Items.Clear();

            if ((this.spot == null) || (this.spot.Tags.Count == 0))
            {
                this.removeSTagBtn.Enabled = false;
                this.removeAllSTagBtn.Enabled = false;
            }
            else
            {
                this.removeSTagBtn.Enabled = true;
                this.removeAllSTagBtn.Enabled = true;

                if (!this.spot.Tags.Contains(this.spot.IdToken))
                {
                    this.spot.Tags.Add(this.spot.IdToken);
                }
                if (this.spot.Tags.Count > 1)
                {
                    this.removeSTagBtn.Enabled = true;
                    this.removeAllSTagBtn.Enabled = true;
                }
                foreach (string tag in this.spot.Tags)
                {
                    this.spotTagsListBox.Items.Add(tag);
                }
            }
        }

        private void RaiseSpotListChanged()
        {
            if (this.SpotListChanged != null) this.SpotListChanged(this, EventArgs.Empty);
        }

        private void RaiseSpotChanged()
        {
            if (this.SpotChanged != null) this.SpotChanged(this, EventArgs.Empty);
        }
        #endregion
    }
}