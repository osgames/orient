/* ****************************************************
 * Name: CharacterControl.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/08
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using System.Windows.Forms;
using PAGE.Generic.Resources.Assets;
using PAGE.OGRE.Domain.Assets;
using PAGE.Orient.Resources;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    public partial class CharacterControl : ResourceAssetControl<ResourceHumanLikeCharacter>
    {
        #region Fields

        protected OgreCharacter genericCharacter = null;

        #endregion

        #region Properties

        [Browsable(false)]
        public new ResourceHumanLikeCharacter TokenObject
        {
            get { return this.tokenObject; }
            set
            {
                base.TokenObject = value;
                this.RefreshTags();
            }
        }

        [Browsable(false)]
        public OgreCharacter GenericCharacter
        {
            get { return genericCharacter; }
            set { genericCharacter = value; }
        }

        [Browsable(false)]
        public new OrientResourcesManager ResourcesManager
        {
            get { return base.ResourcesManager; }
            set
            {
                base.ResourcesManager = value;
                if (value != null) this.TokenObjectList = ResourcesManager.Characters;
            }
        }
        #endregion

        #region Constructor
        public CharacterControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Control Events

        private void charTagsListBox_Validated(object sender, EventArgs e)
        {
            this.TokenObject.Tags.Clear();
            foreach (ListViewItem tag in this.charTagsListBox.Items)
            {
                if (!this.TokenObject.Tags.Contains(tag.Text))
                    this.TokenObject.Tags.Add(tag.Text);
            }
        }

        private void addTagBtn_Click(object sender, EventArgs e)
        {
            int count = 0;
            while (this.TokenObject.Tags.Contains("tag " + count))
            {
                count++;
            }
            this.TokenObject.Tags.Add("tag " + count);
            this.RefreshTags();
        }

        private void removeTagBtn_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem tag in this.charTagsListBox.SelectedItems)
            {
                this.TokenObject.Tags.Remove(tag.Text);
            }
            this.RefreshTags();
        }

        private void removeAllTagBtn_Click(object sender, EventArgs e)
        {
            this.TokenObject.Tags.Clear();
            this.RefreshTags();
        }

        #endregion

        #region Private Methods

        private void RefreshTags()
        {
            this.charTagsListBox.Items.Clear();

            if ((this.TokenObject == null) || (this.TokenObject.Tags.Count == 0))
            {
                this.removeTagBtn.Enabled = false;
                this.removeAllTagBtn.Enabled = false;
            }
            else
            {
                this.removeTagBtn.Enabled = true;
                this.removeAllTagBtn.Enabled = true;
                if (this.TokenObject.Tags.Count > 0)
                {
                    this.removeTagBtn.Enabled = true;
                    this.removeAllTagBtn.Enabled = true;
                }
                foreach (string tag in this.TokenObject.Tags)
                {
                    this.charTagsListBox.Items.Add(tag);
                }
            }
        }

        protected override string ExistentIDMessage
        {
            get
            {
                return "There is already a character with that name.\n" +
                       "Please choose another one.";
            }
        }

        #endregion
    }
}