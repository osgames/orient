using PAGE.Orient.GUI.Windows.Forms.Util;
using ListViewEx=PAGE.Orient.GUI.Windows.Forms.Util.ListViewEx;
using SubItemEndEditingEventHandler=PAGE.Orient.GUI.Windows.Forms.Util.SubItemEndEditingEventHandler;
using SubItemEventHandler=PAGE.Orient.GUI.Windows.Forms.Util.SubItemEventHandler;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    partial class ActionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.removeBtn = new System.Windows.Forms.Button();
            this.addBtn = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.triggerIDTxtBox = new System.Windows.Forms.TextBox();
            this.triggerTimeNUD = new System.Windows.Forms.NumericUpDown();
            this.triggerListView = new ListViewEx();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.stepDistNUD = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.animSpeedNUD = new System.Windows.Forms.NumericUpDown();
            this.animNameCBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.moveRadBtn = new System.Windows.Forms.RadioButton();
            this.animRadBtn = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.blendCheckBox = new System.Windows.Forms.CheckBox();
            this.tokenGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.triggerTimeNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepDistNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.animSpeedNUD)).BeginInit();
            this.SuspendLayout();
            // 
            // tokenIDLbl
            // 
            this.tokenIDLbl.Location = new System.Drawing.Point(27, 22);
            this.tokenIDLbl.Size = new System.Drawing.Size(54, 13);
            this.tokenIDLbl.Text = "Action ID:";
            // 
            // descTxtBox
            // 
            this.descTxtBox.TabIndex = 2;
            // 
            // tokenGroupBox
            // 
            this.tokenGroupBox.Controls.Add(this.blendCheckBox);
            this.tokenGroupBox.Controls.Add(this.removeBtn);
            this.tokenGroupBox.Controls.Add(this.addBtn);
            this.tokenGroupBox.Controls.Add(this.label9);
            this.tokenGroupBox.Controls.Add(this.triggerIDTxtBox);
            this.tokenGroupBox.Controls.Add(this.triggerTimeNUD);
            this.tokenGroupBox.Controls.Add(this.triggerListView);
            this.tokenGroupBox.Controls.Add(this.stepDistNUD);
            this.tokenGroupBox.Controls.Add(this.label8);
            this.tokenGroupBox.Controls.Add(this.animSpeedNUD);
            this.tokenGroupBox.Controls.Add(this.animNameCBox);
            this.tokenGroupBox.Controls.Add(this.label7);
            this.tokenGroupBox.Controls.Add(this.label6);
            this.tokenGroupBox.Controls.Add(this.moveRadBtn);
            this.tokenGroupBox.Controls.Add(this.animRadBtn);
            this.tokenGroupBox.Controls.Add(this.label4);
            this.tokenGroupBox.Size = new System.Drawing.Size(323, 323);
            this.tokenGroupBox.Text = "Action Info";
            this.tokenGroupBox.Controls.SetChildIndex(this.idTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.tokenIDLbl, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.descTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label1, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label4, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.animRadBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.moveRadBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label6, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label7, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.animNameCBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.animSpeedNUD, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label8, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.stepDistNUD, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.triggerListView, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.triggerTimeNUD, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.triggerIDTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label9, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.addBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.removeBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.blendCheckBox, 0);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Trigger ID";
            this.columnHeader1.Width = 165;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Time";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader2.Width = 106;
            // 
            // removeBtn
            // 
            this.removeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.removeBtn.Location = new System.Drawing.Point(169, 294);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(85, 23);
            this.removeBtn.TabIndex = 116;
            this.removeBtn.Text = "Remove";
            this.removeBtn.UseVisualStyleBackColor = true;
            this.removeBtn.Click += new System.EventHandler(this.removeBtn_Click);
            // 
            // addBtn
            // 
            this.addBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addBtn.Location = new System.Drawing.Point(57, 294);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(74, 23);
            this.addBtn.TabIndex = 115;
            this.addBtn.Text = "Add";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 178);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 13);
            this.label9.TabIndex = 114;
            this.label9.Text = "Triggers:";
            // 
            // triggerIDTxtBox
            // 
            this.triggerIDTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.triggerIDTxtBox.Location = new System.Drawing.Point(57, 223);
            this.triggerIDTxtBox.Multiline = true;
            this.triggerIDTxtBox.Name = "triggerIDTxtBox";
            this.triggerIDTxtBox.Size = new System.Drawing.Size(80, 16);
            this.triggerIDTxtBox.TabIndex = 112;
            this.triggerIDTxtBox.Visible = false;
            // 
            // triggerTimeNUD
            // 
            this.triggerTimeNUD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.triggerTimeNUD.DecimalPlaces = 3;
            this.triggerTimeNUD.Increment = new decimal(new int[] {
                                                                      5,
                                                                      0,
                                                                      0,
                                                                      196608});
            this.triggerTimeNUD.Location = new System.Drawing.Point(174, 219);
            this.triggerTimeNUD.Maximum = new decimal(new int[] {
                                                                    12,
                                                                    0,
                                                                    0,
                                                                    0});
            this.triggerTimeNUD.Name = "triggerTimeNUD";
            this.triggerTimeNUD.Size = new System.Drawing.Size(80, 20);
            this.triggerTimeNUD.TabIndex = 113;
            this.triggerTimeNUD.Value = new decimal(new int[] {
                                                                  2006,
                                                                  0,
                                                                  0,
                                                                  196608});
            this.triggerTimeNUD.Visible = false;
            // 
            // triggerListView
            // 
            this.triggerListView.AllowColumnReorder = true;
            this.triggerListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                                 | System.Windows.Forms.AnchorStyles.Left)
                                                                                | System.Windows.Forms.AnchorStyles.Right)));
            this.triggerListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                                                                              this.columnHeader3,
                                                                                              this.columnHeader4});
            this.triggerListView.DoubleClickActivation = false;
            this.triggerListView.FullRowSelect = true;
            this.triggerListView.GridLines = true;
            this.triggerListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.triggerListView.Location = new System.Drawing.Point(14, 194);
            this.triggerListView.Name = "triggerListView";
            this.triggerListView.Size = new System.Drawing.Size(303, 94);
            this.triggerListView.TabIndex = 111;
            this.triggerListView.UseCompatibleStateImageBehavior = false;
            this.triggerListView.View = System.Windows.Forms.View.Details;
            this.triggerListView.SubItemClicked += new SubItemEventHandler(this.triggerListView_SubItemClicked);
            this.triggerListView.SelectedIndexChanged += new System.EventHandler(this.triggerListView_SelectedIndexChanged);
            this.triggerListView.SubItemEndEditing += new SubItemEndEditingEventHandler(this.triggerListView_SubItemEndEditing);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Trigger ID";
            this.columnHeader3.Width = 165;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Time";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader4.Width = 106;
            // 
            // stepDistNUD
            // 
            this.stepDistNUD.DecimalPlaces = 2;
            this.stepDistNUD.Location = new System.Drawing.Point(110, 147);
            this.stepDistNUD.Maximum = new decimal(new int[] {
                                                                 20,
                                                                 0,
                                                                 0,
                                                                 0});
            this.stepDistNUD.Minimum = new decimal(new int[] {
                                                                 20,
                                                                 0,
                                                                 0,
                                                                 -2147483648});
            this.stepDistNUD.Name = "stepDistNUD";
            this.stepDistNUD.Size = new System.Drawing.Size(60, 20);
            this.stepDistNUD.TabIndex = 110;
            this.stepDistNUD.ValueChanged += new System.EventHandler(this.stepDistNUD_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 149);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 109;
            this.label8.Text = "Step Distance:";
            // 
            // animSpeedNUD
            // 
            this.animSpeedNUD.DecimalPlaces = 2;
            this.animSpeedNUD.Increment = new decimal(new int[] {
                                                                    5,
                                                                    0,
                                                                    0,
                                                                    65536});
            this.animSpeedNUD.Location = new System.Drawing.Point(110, 121);
            this.animSpeedNUD.Maximum = new decimal(new int[] {
                                                                  20,
                                                                  0,
                                                                  0,
                                                                  0});
            this.animSpeedNUD.Minimum = new decimal(new int[] {
                                                                  20,
                                                                  0,
                                                                  0,
                                                                  -2147483648});
            this.animSpeedNUD.Name = "animSpeedNUD";
            this.animSpeedNUD.Size = new System.Drawing.Size(60, 20);
            this.animSpeedNUD.TabIndex = 108;
            this.animSpeedNUD.ValueChanged += new System.EventHandler(this.animSpeedNUD_ValueChanged);
            // 
            // animNameCBox
            // 
            this.animNameCBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                                                                             | System.Windows.Forms.AnchorStyles.Right)));
            this.animNameCBox.FormattingEnabled = true;
            this.animNameCBox.Location = new System.Drawing.Point(110, 94);
            this.animNameCBox.Name = "animNameCBox";
            this.animNameCBox.Size = new System.Drawing.Size(207, 21);
            this.animNameCBox.Sorted = true;
            this.animNameCBox.TabIndex = 107;
            this.animNameCBox.Text = "select...";
            this.animNameCBox.SelectedIndexChanged += new System.EventHandler(this.animNameCBox_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 123);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 106;
            this.label7.Text = "Animation Speed:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 105;
            this.label6.Text = "Animation Name:";
            // 
            // moveRadBtn
            // 
            this.moveRadBtn.AutoSize = true;
            this.moveRadBtn.Location = new System.Drawing.Point(204, 71);
            this.moveRadBtn.Name = "moveRadBtn";
            this.moveRadBtn.Size = new System.Drawing.Size(68, 17);
            this.moveRadBtn.TabIndex = 104;
            this.moveRadBtn.TabStop = true;
            this.moveRadBtn.Text = "Move-To";
            this.moveRadBtn.UseVisualStyleBackColor = true;
            this.moveRadBtn.CheckedChanged += new System.EventHandler(this.moveRadBtn_CheckedChanged);
            // 
            // animRadBtn
            // 
            this.animRadBtn.AutoSize = true;
            this.animRadBtn.Location = new System.Drawing.Point(110, 71);
            this.animRadBtn.Name = "animRadBtn";
            this.animRadBtn.Size = new System.Drawing.Size(71, 17);
            this.animRadBtn.TabIndex = 103;
            this.animRadBtn.TabStop = true;
            this.animRadBtn.Text = "Animation";
            this.animRadBtn.UseVisualStyleBackColor = true;
            this.animRadBtn.CheckedChanged += new System.EventHandler(this.animRadBtn_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 102;
            this.label4.Text = "Action Type:";
            // 
            // blendCheckBox
            // 
            this.blendCheckBox.AutoSize = true;
            this.blendCheckBox.Location = new System.Drawing.Point(204, 124);
            this.blendCheckBox.Name = "blendCheckBox";
            this.blendCheckBox.Size = new System.Drawing.Size(102, 17);
            this.blendCheckBox.TabIndex = 117;
            this.blendCheckBox.Text = "Blend Animation";
            this.blendCheckBox.UseVisualStyleBackColor = true;
            this.blendCheckBox.CheckedChanged += new System.EventHandler(this.blendCheckBox_CheckedChanged);
            // 
            // ActionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ActionControl";
            this.Size = new System.Drawing.Size(323, 323);
            this.tokenGroupBox.ResumeLayout(false);
            this.tokenGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.triggerTimeNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepDistNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.animSpeedNUD)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button removeBtn;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox triggerIDTxtBox;
        private System.Windows.Forms.NumericUpDown triggerTimeNUD;
        private ListViewEx triggerListView;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.NumericUpDown stepDistNUD;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown animSpeedNUD;
        private System.Windows.Forms.ComboBox animNameCBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton moveRadBtn;
        private System.Windows.Forms.RadioButton animRadBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox blendCheckBox;


    }
}