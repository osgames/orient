/* ****************************************************
 * Name: SetControl.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/09
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using PAGE.Generic.Resources.Assets;
using PAGE.OGRE.Resources.Assets;
using PAGE.Orient.Resources;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    public partial class SetControl : ResourceAssetControl<ResourceSet>
    {
        #region Properties

        [Browsable(false)]
        public new OgreResourceSet TokenObject
        {
            get { return (OgreResourceSet)this.tokenObject; }
            set { 
                base.TokenObject = value;
                if(value != null)
                {
                    this.skyTxtBox.Text = value.SkyBox;
                    this.particleTxtBox.Text = value.ParticleSystem;
                }
            }
        }

        [Browsable(false)]
        public new OrientResourcesManager ResourcesManager
        {
            get { return base.ResourcesManager; }
            set
            {
                base.ResourcesManager = value;
                if(value != null) this.TokenObjectList = ResourcesManager.Sets;
            }
        }

        #endregion

        #region Constructor
        public SetControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Private Methods

        protected override string ExistentIDMessage
        {
            get
            {
                return "There is already an item with that name.\n" +
                       "Please choose another item ID.";
            }
        }
        #endregion

        #region Control Events

        private void particleTxtBox_TextChanged(object sender, EventArgs e)
        {
            this.TokenObject.ParticleSystem = this.particleTxtBox.Text;
        }

        private void skyTxtBox_TextChanged(object sender, EventArgs e)
        {
            this.TokenObject.SkyBox = this.skyTxtBox.Text;
        } 

        #endregion
    }
}