using System;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    partial class ItemControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.dZTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dYTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dXTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sZTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sYTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sXTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tZtxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tYtxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tXtxtBox)).BeginInit();
            this.tokenGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(22, 74);
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.Text = "Mesh File:";
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "*.mesh";
            this.openFileDialog.FileName = "item.mesh";
            this.openFileDialog.Filter = "Mesh files (*.mesh)|*.mesh";
            this.openFileDialog.Title = "Open Mesh File";
            // 
            // tokenIDLbl
            // 
            this.tokenIDLbl.Location = new System.Drawing.Point(33, 22);
            this.tokenIDLbl.Size = new System.Drawing.Size(44, 13);
            this.tokenIDLbl.Text = "Item ID:";
            // 
            // tokenGroupBox
            // 
            this.tokenGroupBox.Enabled = false;
            this.tokenGroupBox.Text = "Item Info";
            // 
            // ItemControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ItemControl";
            ((System.ComponentModel.ISupportInitialize)(this.dZTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dYTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dXTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sZTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sYTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sXTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tZtxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tYtxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tXtxtBox)).EndInit();
            this.tokenGroupBox.ResumeLayout(false);
            this.tokenGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

    }
}