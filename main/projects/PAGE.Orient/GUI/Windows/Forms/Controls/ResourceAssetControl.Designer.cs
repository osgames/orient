
namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    public partial class ResourceAssetControl<AssetType>
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorBtn = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.dZTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.dYTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.dXTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.sZTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.sYTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.sXTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tZtxtBox = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.tYtxtBox = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.tXtxtBox = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.openFileBtn = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.fileTxtBox = new System.Windows.Forms.TextBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tokenGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dZTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dYTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dXTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sZTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sYTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sXTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tZtxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tYtxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tXtxtBox)).BeginInit();
            this.SuspendLayout();
            // 
            // tokenIDLbl
            // 
            this.tokenIDLbl.Location = new System.Drawing.Point(27, 22);
            this.tokenIDLbl.Size = new System.Drawing.Size(50, 13);
            this.tokenIDLbl.Text = "Asset ID:";
            // 
            // descTxtBox
            // 
            this.descTxtBox.TabIndex = 2;
            // 
            // tokenGroupBox
            // 
            this.tokenGroupBox.Controls.Add(this.colorBtn);
            this.tokenGroupBox.Controls.Add(this.label30);
            this.tokenGroupBox.Controls.Add(this.dZTxtBox);
            this.tokenGroupBox.Controls.Add(this.label19);
            this.tokenGroupBox.Controls.Add(this.dYTxtBox);
            this.tokenGroupBox.Controls.Add(this.label20);
            this.tokenGroupBox.Controls.Add(this.dXTxtBox);
            this.tokenGroupBox.Controls.Add(this.label21);
            this.tokenGroupBox.Controls.Add(this.label22);
            this.tokenGroupBox.Controls.Add(this.sZTxtBox);
            this.tokenGroupBox.Controls.Add(this.label2);
            this.tokenGroupBox.Controls.Add(this.sYTxtBox);
            this.tokenGroupBox.Controls.Add(this.label16);
            this.tokenGroupBox.Controls.Add(this.sXTxtBox);
            this.tokenGroupBox.Controls.Add(this.label17);
            this.tokenGroupBox.Controls.Add(this.label18);
            this.tokenGroupBox.Controls.Add(this.tZtxtBox);
            this.tokenGroupBox.Controls.Add(this.label14);
            this.tokenGroupBox.Controls.Add(this.tYtxtBox);
            this.tokenGroupBox.Controls.Add(this.label13);
            this.tokenGroupBox.Controls.Add(this.tXtxtBox);
            this.tokenGroupBox.Controls.Add(this.label12);
            this.tokenGroupBox.Controls.Add(this.label11);
            this.tokenGroupBox.Controls.Add(this.openFileBtn);
            this.tokenGroupBox.Controls.Add(this.label10);
            this.tokenGroupBox.Controls.Add(this.fileTxtBox);
            this.tokenGroupBox.Size = new System.Drawing.Size(323, 206);
            this.tokenGroupBox.Text = "Resource Asset";
            this.tokenGroupBox.Controls.SetChildIndex(this.idTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.tokenIDLbl, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.descTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label1, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.fileTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label10, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.openFileBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label11, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label12, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.tXtxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label13, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.tYtxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label14, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.tZtxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label18, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label17, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.sXTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label16, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.sYTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label2, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.sZTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label22, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label21, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.dXTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label20, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.dYTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label19, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.dZTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label30, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.colorBtn, 0);
            // 
            // colorBtn
            // 
            this.colorBtn.Location = new System.Drawing.Point(81, 175);
            this.colorBtn.Name = "colorBtn";
            this.colorBtn.Size = new System.Drawing.Size(75, 23);
            this.colorBtn.TabIndex = 13;
            this.colorBtn.Text = "Change...";
            this.colorBtn.UseVisualStyleBackColor = true;
            this.colorBtn.Click += new System.EventHandler(this.colorBtn_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(43, 180);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(34, 13);
            this.label30.TabIndex = 127;
            this.label30.Text = "Color:";
            // 
            // dZTxtBox
            // 
            this.dZTxtBox.DecimalPlaces = 2;
            this.dZTxtBox.Location = new System.Drawing.Point(257, 149);
            this.dZTxtBox.Maximum = new decimal(new int[] {
                                                              1,
                                                              0,
                                                              0,
                                                              0});
            this.dZTxtBox.Minimum = new decimal(new int[] {
                                                              1,
                                                              0,
                                                              0,
                                                              -2147483648});
            this.dZTxtBox.Name = "dZTxtBox";
            this.dZTxtBox.Size = new System.Drawing.Size(55, 20);
            this.dZTxtBox.TabIndex = 12;
            this.dZTxtBox.Value = new decimal(new int[] {
                                                            1,
                                                            0,
                                                            0,
                                                            -2147483648});
            this.dZTxtBox.ValueChanged += new System.EventHandler(this.dZTxtBox_ValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(238, 151);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(24, 13);
            this.label19.TabIndex = 126;
            this.label19.Text = "z = ";
            // 
            // dYTxtBox
            // 
            this.dYTxtBox.DecimalPlaces = 2;
            this.dYTxtBox.Location = new System.Drawing.Point(177, 149);
            this.dYTxtBox.Maximum = new decimal(new int[] {
                                                              1,
                                                              0,
                                                              0,
                                                              0});
            this.dYTxtBox.Minimum = new decimal(new int[] {
                                                              1,
                                                              0,
                                                              0,
                                                              -2147483648});
            this.dYTxtBox.Name = "dYTxtBox";
            this.dYTxtBox.Size = new System.Drawing.Size(55, 20);
            this.dYTxtBox.TabIndex = 11;
            this.dYTxtBox.ValueChanged += new System.EventHandler(this.dYTxtBox_ValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(158, 151);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(24, 13);
            this.label20.TabIndex = 125;
            this.label20.Text = "y = ";
            // 
            // dXTxtBox
            // 
            this.dXTxtBox.DecimalPlaces = 2;
            this.dXTxtBox.Location = new System.Drawing.Point(97, 149);
            this.dXTxtBox.Maximum = new decimal(new int[] {
                                                              1,
                                                              0,
                                                              0,
                                                              0});
            this.dXTxtBox.Minimum = new decimal(new int[] {
                                                              1,
                                                              0,
                                                              0,
                                                              -2147483648});
            this.dXTxtBox.Name = "dXTxtBox";
            this.dXTxtBox.Size = new System.Drawing.Size(55, 20);
            this.dXTxtBox.TabIndex = 10;
            this.dXTxtBox.ValueChanged += new System.EventHandler(this.dXTxtBox_ValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(78, 151);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(24, 13);
            this.label21.TabIndex = 124;
            this.label21.Text = "x = ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(25, 151);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 13);
            this.label22.TabIndex = 123;
            this.label22.Text = "Direction:";
            // 
            // sZTxtBox
            // 
            this.sZTxtBox.DecimalPlaces = 2;
            this.sZTxtBox.Increment = new decimal(new int[] {
                                                                5,
                                                                0,
                                                                0,
                                                                131072});
            this.sZTxtBox.Location = new System.Drawing.Point(257, 123);
            this.sZTxtBox.Name = "sZTxtBox";
            this.sZTxtBox.Size = new System.Drawing.Size(55, 20);
            this.sZTxtBox.TabIndex = 9;
            this.sZTxtBox.Value = new decimal(new int[] {
                                                            1,
                                                            0,
                                                            0,
                                                            0});
            this.sZTxtBox.ValueChanged += new System.EventHandler(this.sZTxtBox_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(238, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 122;
            this.label2.Text = "z = ";
            // 
            // sYTxtBox
            // 
            this.sYTxtBox.DecimalPlaces = 2;
            this.sYTxtBox.Increment = new decimal(new int[] {
                                                                5,
                                                                0,
                                                                0,
                                                                131072});
            this.sYTxtBox.Location = new System.Drawing.Point(177, 123);
            this.sYTxtBox.Name = "sYTxtBox";
            this.sYTxtBox.Size = new System.Drawing.Size(55, 20);
            this.sYTxtBox.TabIndex = 8;
            this.sYTxtBox.Value = new decimal(new int[] {
                                                            1,
                                                            0,
                                                            0,
                                                            0});
            this.sYTxtBox.ValueChanged += new System.EventHandler(this.sYTxtBox_ValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(158, 125);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(24, 13);
            this.label16.TabIndex = 121;
            this.label16.Text = "y = ";
            // 
            // sXTxtBox
            // 
            this.sXTxtBox.DecimalPlaces = 2;
            this.sXTxtBox.Increment = new decimal(new int[] {
                                                                5,
                                                                0,
                                                                0,
                                                                131072});
            this.sXTxtBox.Location = new System.Drawing.Point(97, 123);
            this.sXTxtBox.Name = "sXTxtBox";
            this.sXTxtBox.Size = new System.Drawing.Size(55, 20);
            this.sXTxtBox.TabIndex = 7;
            this.sXTxtBox.Value = new decimal(new int[] {
                                                            1,
                                                            0,
                                                            0,
                                                            0});
            this.sXTxtBox.ValueChanged += new System.EventHandler(this.sXTxtBox_ValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(78, 125);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(24, 13);
            this.label17.TabIndex = 120;
            this.label17.Text = "x = ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(40, 125);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(37, 13);
            this.label18.TabIndex = 119;
            this.label18.Text = "Scale:";
            // 
            // tZtxtBox
            // 
            this.tZtxtBox.DecimalPlaces = 2;
            this.tZtxtBox.Increment = new decimal(new int[] {
                                                                5,
                                                                0,
                                                                0,
                                                                131072});
            this.tZtxtBox.Location = new System.Drawing.Point(257, 97);
            this.tZtxtBox.Maximum = new decimal(new int[] {
                                                              1000,
                                                              0,
                                                              0,
                                                              0});
            this.tZtxtBox.Minimum = new decimal(new int[] {
                                                              1000,
                                                              0,
                                                              0,
                                                              -2147483648});
            this.tZtxtBox.Name = "tZtxtBox";
            this.tZtxtBox.Size = new System.Drawing.Size(55, 20);
            this.tZtxtBox.TabIndex = 6;
            this.tZtxtBox.ValueChanged += new System.EventHandler(this.tZtxtBox_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(238, 99);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(24, 13);
            this.label14.TabIndex = 118;
            this.label14.Text = "z = ";
            // 
            // tYtxtBox
            // 
            this.tYtxtBox.DecimalPlaces = 2;
            this.tYtxtBox.Increment = new decimal(new int[] {
                                                                5,
                                                                0,
                                                                0,
                                                                131072});
            this.tYtxtBox.Location = new System.Drawing.Point(177, 97);
            this.tYtxtBox.Maximum = new decimal(new int[] {
                                                              1000,
                                                              0,
                                                              0,
                                                              0});
            this.tYtxtBox.Minimum = new decimal(new int[] {
                                                              1000,
                                                              0,
                                                              0,
                                                              -2147483648});
            this.tYtxtBox.Name = "tYtxtBox";
            this.tYtxtBox.Size = new System.Drawing.Size(55, 20);
            this.tYtxtBox.TabIndex = 5;
            this.tYtxtBox.ValueChanged += new System.EventHandler(this.tYtxtBox_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(158, 99);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 13);
            this.label13.TabIndex = 117;
            this.label13.Text = "y = ";
            // 
            // tXtxtBox
            // 
            this.tXtxtBox.DecimalPlaces = 2;
            this.tXtxtBox.Increment = new decimal(new int[] {
                                                                5,
                                                                0,
                                                                0,
                                                                131072});
            this.tXtxtBox.Location = new System.Drawing.Point(97, 97);
            this.tXtxtBox.Maximum = new decimal(new int[] {
                                                              1000,
                                                              0,
                                                              0,
                                                              0});
            this.tXtxtBox.Minimum = new decimal(new int[] {
                                                              1000,
                                                              0,
                                                              0,
                                                              -2147483648});
            this.tXtxtBox.Name = "tXtxtBox";
            this.tXtxtBox.Size = new System.Drawing.Size(55, 20);
            this.tXtxtBox.TabIndex = 4;
            this.tXtxtBox.ValueChanged += new System.EventHandler(this.tXtxtBox_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(78, 99);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(24, 13);
            this.label12.TabIndex = 116;
            this.label12.Text = "x = ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 99);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 115;
            this.label11.Text = "Translate:";
            // 
            // openFileBtn
            // 
            this.openFileBtn.Location = new System.Drawing.Point(242, 69);
            this.openFileBtn.Name = "openFileBtn";
            this.openFileBtn.Size = new System.Drawing.Size(75, 23);
            this.openFileBtn.TabIndex = 3;
            this.openFileBtn.Text = "Open...";
            this.openFileBtn.UseVisualStyleBackColor = true;
            this.openFileBtn.Click += new System.EventHandler(this.openFileBtn_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(51, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 114;
            this.label10.Text = "File:";
            // 
            // fileTxtBox
            // 
            this.fileTxtBox.Location = new System.Drawing.Point(82, 71);
            this.fileTxtBox.Name = "fileTxtBox";
            this.fileTxtBox.ReadOnly = true;
            this.fileTxtBox.Size = new System.Drawing.Size(154, 20);
            this.fileTxtBox.TabIndex = 113;
            this.fileTxtBox.TabStop = false;
            // 
            // openFileDialog
            // 
            this.openFileDialog.RestoreDirectory = true;
            this.openFileDialog.Title = "Open File";
            // 
            // ResourceAssetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ResourceAssetControl";
            this.Size = new System.Drawing.Size(323, 206);
            this.tokenGroupBox.ResumeLayout(false);
            this.tokenGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dZTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dYTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dXTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sZTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sYTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sXTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tZtxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tYtxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tXtxtBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Button colorBtn;
        protected System.Windows.Forms.Label label30;
        protected System.Windows.Forms.NumericUpDown dZTxtBox;
        protected System.Windows.Forms.Label label19;
        protected System.Windows.Forms.NumericUpDown dYTxtBox;
        protected System.Windows.Forms.Label label20;
        protected System.Windows.Forms.NumericUpDown dXTxtBox;
        protected System.Windows.Forms.Label label21;
        protected System.Windows.Forms.Label label22;
        protected System.Windows.Forms.NumericUpDown sZTxtBox;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.NumericUpDown sYTxtBox;
        protected System.Windows.Forms.Label label16;
        protected System.Windows.Forms.NumericUpDown sXTxtBox;
        protected System.Windows.Forms.Label label17;
        protected System.Windows.Forms.Label label18;
        protected System.Windows.Forms.NumericUpDown tZtxtBox;
        protected System.Windows.Forms.Label label14;
        protected System.Windows.Forms.NumericUpDown tYtxtBox;
        protected System.Windows.Forms.Label label13;
        protected System.Windows.Forms.NumericUpDown tXtxtBox;
        protected System.Windows.Forms.Label label12;
        protected System.Windows.Forms.Label label11;
        protected System.Windows.Forms.Button openFileBtn;
        protected System.Windows.Forms.Label label10;
        protected System.Windows.Forms.TextBox fileTxtBox;
        protected System.Windows.Forms.ColorDialog colorDialog1;
        protected System.Windows.Forms.OpenFileDialog openFileDialog;

    }
}