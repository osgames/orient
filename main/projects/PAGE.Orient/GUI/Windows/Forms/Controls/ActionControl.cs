/* ****************************************************
 * Name: ActionControl.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/08
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;
using PAGE.Generic.Resources.Actions;
using PAGE.Generic.Resources.Assets;
using PAGE.OGRE.Math;
using PAGE.Orient.GUI.Windows.Forms.Util;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    public partial class ActionControl : TokenObjectControl<ResourceAction>
    {
        #region Fields

        protected ResourceCharacter character;
        protected ArrayList animationNames;
        protected float triggerTime;
        protected Control[] editors;

        #endregion

        #region Properties

        [Browsable(false)]
        public ResourceCharacter Character
        {
            get { return this.character; }
            set
            {
                this.character = value;
                if (value != null)
                    this.TokenObjectList = this.character.Actions;
            }
        }

        [Browsable(false)]
        public new ResourceAction TokenObject
        {
            get { return this.tokenObject; }
            set
            {
                base.TokenObject = value;
                if (value != null)
                {
                    if (value is WalkToAction)
                    {
                        WalkToAction moveAction = (WalkToAction) value;
                        this.moveRadBtn.Select();
                        this.animNameCBox.Enabled = true;
                        this.animNameCBox.Text = moveAction.AnimationName;
                        this.animSpeedNUD.Enabled = true;
                        this.animSpeedNUD.Value = (decimal) moveAction.AnimationSpeed;
                        this.stepDistNUD.Enabled = true;
                        this.stepDistNUD.Value = (decimal) ((OgreDistance) moveAction.WalkStepDist).Value;
                        this.blendCheckBox.Checked = moveAction.Blend;
                    }
                    else if (value is AnimationAction)
                    {
                        AnimationAction animAction = (AnimationAction) value;
                        this.animRadBtn.Select();
                        this.animNameCBox.Enabled = true;
                        this.animNameCBox.Text = animAction.AnimationName;
                        this.animSpeedNUD.Enabled = true;
                        this.animSpeedNUD.Value = (decimal) animAction.AnimationSpeed;
                        this.stepDistNUD.Enabled = false;
                        this.blendCheckBox.Checked = animAction.Blend;
                    }
                }

                this.RefreshTriggers();
            }
        }

        [Browsable(false)]
        public ArrayList AnimationNames
        {
            get { return animationNames; }
            set
            {
                animationNames = value;
                this.RefreshAnimations();
            }
        }

        public float TriggerTime
        {
            get { return triggerTime; }
            set { triggerTime = value; }
        }

        public float TriggerMaxTime
        {
            get { return (float) this.triggerTimeNUD.Maximum; }
            set { this.triggerTimeNUD.Maximum = (decimal) value; }
        }

        #endregion

        #region Constructor

        public ActionControl()
        {
            InitializeComponent();

            this.editors = new Control[]
                               {
                                   this.triggerIDTxtBox, // for column 0
                                   this.triggerTimeNUD // for column 1
                               };

            this.triggerListView.DoubleClickActivation = true;
        }

        #endregion

        #region Private Methods

        protected override string ExistentIDMessage
        {
            get
            {
                return "There is already an action with that ID.\n" +
                       "Please choose another action ID.";
            }
        }

        private void RefreshTriggers()
        {
            if (this.TokenObject == null) return;
            this.triggerListView.Items.Clear();
            foreach (float trigTime in this.TokenObject.Triggers.Times)
            {
                ActionTrigger trigger = this.TokenObject.Triggers[trigTime];
                ListViewItem lvi = new ListViewItem(trigger.IdToken);
                lvi.Tag = trigger;
                lvi.SubItems.Add(trigger.Time.ToString("0.000", CultureInfo.InvariantCulture));
                this.triggerListView.Items.Add(lvi);
            }
            this.removeBtn.Enabled = false;
        }

        private void RefreshAnimations()
        {
            this.animNameCBox.Items.Clear();
            this.animNameCBox.SelectedIndex = -1;
            this.animNameCBox.SelectedValue = null;
            this.animNameCBox.Invalidate();
            this.animNameCBox.Text = "select...";

            if (this.animationNames == null) return;
            foreach (string anim in this.animationNames)
            {
                this.animNameCBox.Items.Add(anim);
            }
        }

        #endregion

        #region Control Events

        private void animRadBtn_CheckedChanged(object sender, EventArgs e)
        {
            string[] fullTypeNames = this.TokenObject.GetType().ToString().Split(new char[] {'.'});
            string actionType = fullTypeNames[fullTypeNames.Length - 1];

            if (this.animRadBtn.Checked && (actionType == "WalkToAction"))
            {
                WalkToAction moveAction = (WalkToAction) this.tokenObject;
                this.TokenObjectList.Remove(this.tokenObject);
                this.tokenObject = new AnimationAction(moveAction.IdToken, moveAction.AnimationName);
                ((AnimationAction) this.TokenObject).AnimationSpeed = moveAction.AnimationSpeed;
                this.TokenObjectList.Add(this.TokenObject);

                this.RaiseTokenChanged();
                this.RaiseTokenListChanged();
            }
            else if (this.moveRadBtn.Checked &&
                     (actionType == "AnimationAction"))
            {
                AnimationAction animAction = (AnimationAction) this.tokenObject;
                this.TokenObjectList.Remove(this.tokenObject);
                this.tokenObject = new WalkToAction(
                    animAction.IdToken, animAction.AnimationName, new OgreDistance(0));
                ((WalkToAction) this.TokenObject).AnimationSpeed = animAction.AnimationSpeed;
                ((WalkToAction) this.TokenObject).WalkStepDist = new OgreDistance(4);
                this.TokenObjectList.Add(this.TokenObject);

                this.RaiseTokenChanged();
                this.RaiseTokenListChanged();
            }
        }

        private void moveRadBtn_CheckedChanged(object sender, EventArgs e)
        {
            this.animRadBtn_CheckedChanged(this, e);
        }

        private void animNameCBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((AnimationAction) this.TokenObject).AnimationName =
                (string) this.animNameCBox.SelectedItem;
            this.RaiseTokenChanged();
        }

        private void animSpeedNUD_ValueChanged(object sender, EventArgs e)
        {
            ((AnimationAction) this.TokenObject).AnimationSpeed =
                (float) this.animSpeedNUD.Value;
            this.RaiseTokenChanged();
        }

        private void stepDistNUD_ValueChanged(object sender, EventArgs e)
        {
            ((WalkToAction) this.TokenObject).WalkStepDist =
                new OgreDistance((double) this.stepDistNUD.Value);
            this.RaiseTokenChanged();
        }

        private void blendCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            ((AnimationAction) this.TokenObject).Blend = this.blendCheckBox.Checked;
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            int id = 0;
            while (this.TokenObject.Triggers.Contains("trigger " + id))
            {
                id++;
            }
            float time = this.triggerTime;
            ActionTrigger trigger = new ActionTrigger("trigger " + id, time);
            this.TokenObject.Triggers.Add(trigger);
            this.RefreshTriggers();
        }

        private void removeBtn_Click(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection lvsi = this.triggerListView.SelectedItems;
            foreach (ListViewItem item in lvsi)
            {
                ActionTrigger trigger = (ActionTrigger) item.Tag;
                this.TokenObject.Triggers.Remove(trigger);
            }
            this.RefreshTriggers();
        }

        private void triggerListView_SubItemClicked(object sender, SubItemEventArgs e)
        {
            ActionTrigger trigger = (ActionTrigger) e.Item.Tag;
            this.triggerTimeNUD.Value = (decimal) trigger.Time;
            this.triggerListView.StartEditing(this.editors[e.SubItem], e.Item, e.SubItem);
        }

        private void triggerListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection lvsi = this.triggerListView.SelectedItems;
            if (lvsi.Count == 0)
                this.removeBtn.Enabled = false;
            else
                this.removeBtn.Enabled = true;
        }

        private void triggerListView_SubItemEndEditing(object sender, SubItemEndEditingEventArgs e)
        {
            ActionTrigger trigger = (ActionTrigger) e.Item.Tag;
            if (e.SubItem == 0)
            {
                string triggerID = e.Item.SubItems[0].Text;
                if (trigger.IdToken != triggerID)
                {
                    if (this.TokenObject.Triggers.Contains(triggerID))
                    {
                        MessageBox.Show("There is already a trigger with that ID.\n" +
                                        "Please choose another ID.", "Trigger ID",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                        e.Item.SubItems[0].Text = trigger.IdToken;
                    }
                    else
                    {
                        this.TokenObject.Triggers.Remove(trigger);
                        trigger.IdToken = triggerID;
                        this.TokenObject.Triggers.Add(trigger);
                        this.RefreshTriggers();
                    }
                }
            }
            else
            {
                string triggertime = e.Item.SubItems[1].Text;
                trigger.Time = float.Parse(triggertime.Replace(",", "."), NumberStyles.Number,
                                           CultureInfo.InvariantCulture);
                this.RefreshTriggers();
            }
        }

        #endregion
    }
}