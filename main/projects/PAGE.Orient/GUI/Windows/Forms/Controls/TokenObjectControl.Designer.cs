using System;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    public partial class TokenObjectControl<ResourceType> 
        where ResourceType : TokenObject
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tokenGroupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.descTxtBox = new System.Windows.Forms.TextBox();
            this.tokenIDLbl = new System.Windows.Forms.Label();
            this.idTxtBox = new System.Windows.Forms.TextBox();
            this.tokenGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // tokenGroupBox
            // 
            this.tokenGroupBox.Controls.Add(this.label1);
            this.tokenGroupBox.Controls.Add(this.descTxtBox);
            this.tokenGroupBox.Controls.Add(this.tokenIDLbl);
            this.tokenGroupBox.Controls.Add(this.idTxtBox);
            this.tokenGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tokenGroupBox.Location = new System.Drawing.Point(0, 0);
            this.tokenGroupBox.Name = "tokenGroupBox";
            this.tokenGroupBox.Size = new System.Drawing.Size(323, 74);
            this.tokenGroupBox.TabIndex = 13;
            this.tokenGroupBox.TabStop = false;
            this.tokenGroupBox.Text = "Token Object";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                        | System.Windows.Forms.AnchorStyles.Left)
                                                                       | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 101;
            this.label1.Text = "Description:";
            // 
            // descTxtBox
            // 
            this.descTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                            | System.Windows.Forms.AnchorStyles.Left)
                                                                           | System.Windows.Forms.AnchorStyles.Right)));
            this.descTxtBox.Location = new System.Drawing.Point(82, 45);
            this.descTxtBox.Name = "descTxtBox";
            this.descTxtBox.Size = new System.Drawing.Size(235, 20);
            this.descTxtBox.TabIndex = 1;
            this.descTxtBox.TextChanged += new System.EventHandler(this.descTxtBox_TextChanged);
            // 
            // tokenIDLbl
            // 
            this.tokenIDLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                            | System.Windows.Forms.AnchorStyles.Left)
                                                                           | System.Windows.Forms.AnchorStyles.Right)));
            this.tokenIDLbl.AutoSize = true;
            this.tokenIDLbl.Location = new System.Drawing.Point(22, 22);
            this.tokenIDLbl.Name = "tokenIDLbl";
            this.tokenIDLbl.Size = new System.Drawing.Size(55, 13);
            this.tokenIDLbl.TabIndex = 99;
            this.tokenIDLbl.Text = "Token ID:";
            // 
            // idTxtBox
            // 
            this.idTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                          | System.Windows.Forms.AnchorStyles.Left)
                                                                         | System.Windows.Forms.AnchorStyles.Right)));
            this.idTxtBox.Location = new System.Drawing.Point(82, 19);
            this.idTxtBox.Name = "idTxtBox";
            this.idTxtBox.Size = new System.Drawing.Size(235, 20);
            this.idTxtBox.TabIndex = 0;
            this.idTxtBox.Validated += new System.EventHandler(this.spotIDTxtBox_Validated);
            // 
            // TokenObjectControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tokenGroupBox);
            this.Name = "TokenObjectControl";
            this.Size = new System.Drawing.Size(323, 74);
            this.tokenGroupBox.ResumeLayout(false);
            this.tokenGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Label tokenIDLbl;
        protected System.Windows.Forms.TextBox idTxtBox;
        protected System.Windows.Forms.TextBox descTxtBox;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.GroupBox tokenGroupBox;
    }
}