using System.ComponentModel;
using System.Windows.Forms;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    partial class SetControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label15 = new System.Windows.Forms.Label();
            this.skyTxtBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.particleTxtBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dZTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dYTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dXTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sZTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sYTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sXTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tZtxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tYtxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tXtxtBox)).BeginInit();
            this.tokenGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // colorBtn
            // 
            this.colorBtn.Location = new System.Drawing.Point(81, 226);
            this.colorBtn.TabIndex = 15;
            // 
            // label30
            // 
            this.label30.Location = new System.Drawing.Point(43, 231);
            // 
            // dZTxtBox
            // 
            this.dZTxtBox.Location = new System.Drawing.Point(257, 200);
            this.dZTxtBox.TabIndex = 14;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(238, 202);
            // 
            // dYTxtBox
            // 
            this.dYTxtBox.Location = new System.Drawing.Point(177, 200);
            this.dYTxtBox.TabIndex = 13;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(158, 202);
            // 
            // dXTxtBox
            // 
            this.dXTxtBox.Location = new System.Drawing.Point(97, 200);
            this.dXTxtBox.TabIndex = 12;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(78, 202);
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(25, 202);
            // 
            // sZTxtBox
            // 
            this.sZTxtBox.Location = new System.Drawing.Point(257, 174);
            this.sZTxtBox.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(238, 176);
            // 
            // sYTxtBox
            // 
            this.sYTxtBox.Location = new System.Drawing.Point(177, 174);
            this.sYTxtBox.TabIndex = 10;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(158, 176);
            // 
            // sXTxtBox
            // 
            this.sXTxtBox.Location = new System.Drawing.Point(97, 174);
            this.sXTxtBox.TabIndex = 9;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(78, 176);
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(40, 176);
            // 
            // tZtxtBox
            // 
            this.tZtxtBox.Location = new System.Drawing.Point(257, 148);
            this.tZtxtBox.TabIndex = 8;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(238, 150);
            // 
            // tYtxtBox
            // 
            this.tYtxtBox.Location = new System.Drawing.Point(177, 148);
            this.tYtxtBox.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(158, 150);
            // 
            // tXtxtBox
            // 
            this.tXtxtBox.Location = new System.Drawing.Point(97, 148);
            this.tXtxtBox.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(78, 150);
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(23, 150);
            // 
            // openFileBtn
            // 
            this.openFileBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.openFileBtn.Location = new System.Drawing.Point(242, 120);
            this.openFileBtn.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(24, 125);
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.Text = "OSM File:";
            // 
            // fileTxtBox
            // 
            this.fileTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                                                                           | System.Windows.Forms.AnchorStyles.Right)));
            this.fileTxtBox.Location = new System.Drawing.Point(82, 122);
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "*.osm";
            this.openFileDialog.FileName = "set.osm";
            this.openFileDialog.Filter = "OSM files (*.osm)|*.osm";
            this.openFileDialog.Title = "Open OSM File";
            // 
            // tokenIDLbl
            // 
            this.tokenIDLbl.Location = new System.Drawing.Point(37, 22);
            this.tokenIDLbl.Size = new System.Drawing.Size(40, 13);
            this.tokenIDLbl.Text = "Set ID:";
            // 
            // tokenGroupBox
            // 
            this.tokenGroupBox.Controls.Add(this.label15);
            this.tokenGroupBox.Controls.Add(this.skyTxtBox);
            this.tokenGroupBox.Controls.Add(this.label3);
            this.tokenGroupBox.Controls.Add(this.particleTxtBox);
            this.tokenGroupBox.Enabled = false;
            this.tokenGroupBox.Size = new System.Drawing.Size(323, 256);
            this.tokenGroupBox.Text = "Set Info";
            this.tokenGroupBox.Controls.SetChildIndex(this.idTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.tokenIDLbl, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.descTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label1, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.fileTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label10, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.openFileBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label11, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label12, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.tXtxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label13, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.tYtxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label14, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.tZtxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label18, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label17, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.sXTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label16, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.sYTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label2, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.sZTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label22, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label21, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.dXTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label20, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.dYTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label19, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.dZTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label30, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.colorBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.particleTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label3, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.skyTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label15, 0);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(28, 99);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 138;
            this.label15.Text = "Sky Box:";
            // 
            // skyTxtBox
            // 
            this.skyTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                                                                          | System.Windows.Forms.AnchorStyles.Right)));
            this.skyTxtBox.Location = new System.Drawing.Point(82, 96);
            this.skyTxtBox.Name = "skyTxtBox";
            this.skyTxtBox.Size = new System.Drawing.Size(235, 20);
            this.skyTxtBox.TabIndex = 4;
            this.skyTxtBox.TextChanged += new System.EventHandler(this.skyTxtBox_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(-2, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 137;
            this.label3.Text = "Particle System:";
            // 
            // particleTxtBox
            // 
            this.particleTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                                                                               | System.Windows.Forms.AnchorStyles.Right)));
            this.particleTxtBox.Location = new System.Drawing.Point(82, 71);
            this.particleTxtBox.Name = "particleTxtBox";
            this.particleTxtBox.Size = new System.Drawing.Size(235, 20);
            this.particleTxtBox.TabIndex = 3;
            this.particleTxtBox.TextChanged += new System.EventHandler(this.particleTxtBox_TextChanged);
            // 
            // SetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "SetControl";
            this.Size = new System.Drawing.Size(323, 256);
            ((System.ComponentModel.ISupportInitialize)(this.dZTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dYTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dXTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sZTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sYTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sXTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tZtxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tYtxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tXtxtBox)).EndInit();
            this.tokenGroupBox.ResumeLayout(false);
            this.tokenGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Label label15;
        private TextBox skyTxtBox;
        private Label label3;
        private TextBox particleTxtBox;
    }
}