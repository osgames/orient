/* ****************************************************
 * Name: ResourceAssetControl.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/08
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using PAGE.Generic.Resources.Assets;
using PAGE.OGRE;
using PAGE.OGRE.Math;
using PAGE.Orient.GUI.Windows.Forms.Util;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    public partial class ResourceAssetControl<AssetType> : TokenObjectControl<AssetType> 
        where AssetType : ResourceAsset
    {
        #region Properties

        [Browsable(false)]
        public new AssetType TokenObject
        {
            get { return this.tokenObject; }
            set
            {
                base.TokenObject = value;
                if(value == null)
                {
                    this.fileTxtBox.Text = "";
                    this.colorDialog1.Color = SystemColors.Control;
                }
                else
                {
                    this.fileTxtBox.Text = value.FileName;
                    this.colorDialog1.Color = this.colorBtn.BackColor = 
                        GUIUtil.ConvertToColor((OgreColor)value.Color);

                    OgrePosition translation = (OgrePosition) value.Translation;
                    this.tXtxtBox.Value = (decimal)translation.X;
                    this.tYtxtBox.Value = (decimal)translation.Y;
                    this.tZtxtBox.Value = (decimal)translation.Z;

                    OgreScale scale = (OgreScale) value.Scale;
                    this.sXTxtBox.Value = (decimal)scale.X;
                    this.sYTxtBox.Value = (decimal)scale.Y;
                    this.sZTxtBox.Value = (decimal)scale.Z;

                    OgreDirection direction = (OgreDirection) value.Direction;
                    this.dXTxtBox.Value = (decimal)direction.X;
                    this.dYTxtBox.Value = (decimal)direction.Y;
                    this.dZTxtBox.Value = (decimal)direction.Z;
                }
            }
        }

        #endregion

        #region Constructor
        public ResourceAssetControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Control Events

        protected virtual void openFileBtn_Click(object sender, EventArgs e)
        {
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = Path.GetFileName(this.openFileDialog.FileName);
                this.TokenObject.FileName = fileName;
            }
            this.RaiseTokenChanged();
        }

        protected virtual void tXtxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.TokenObject.Translation = new OgrePosition(
                GUIUtil.ReadVector(this.tXtxtBox, this.tYtxtBox, this.tZtxtBox));
            this.RaiseTokenChanged();
        }

        protected virtual void tYtxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.TokenObject.Translation = new OgrePosition(
                GUIUtil.ReadVector(this.tXtxtBox, this.tYtxtBox, this.tZtxtBox));
            this.RaiseTokenChanged();
        }

        protected virtual void tZtxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.TokenObject.Translation = new OgrePosition(
                GUIUtil.ReadVector(this.tXtxtBox, this.tYtxtBox, this.tZtxtBox));
            this.RaiseTokenChanged();
        }

        protected virtual void sXTxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.TokenObject.Scale = new OgreScale(
                GUIUtil.ReadVector(this.sXTxtBox, this.sYTxtBox, this.sZTxtBox));
            this.RaiseTokenChanged();
        }

        protected virtual void sYTxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.TokenObject.Scale = new OgreScale(
                GUIUtil.ReadVector(this.sXTxtBox, this.sYTxtBox, this.sZTxtBox));
            this.RaiseTokenChanged();
        }

        protected virtual void sZTxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.TokenObject.Scale = new OgreScale(
                GUIUtil.ReadVector(this.sXTxtBox, this.sYTxtBox, this.sZTxtBox));
            this.RaiseTokenChanged();
        }

        protected virtual void dXTxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.TokenObject.Direction = new OgreDirection(
                GUIUtil.ReadVector(this.dXTxtBox, this.dYTxtBox, this.dZTxtBox));
            this.RaiseTokenChanged();
        }

        protected virtual void dYTxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.TokenObject.Direction = new OgreDirection(
                GUIUtil.ReadVector(this.dXTxtBox, this.dYTxtBox, this.dZTxtBox));
            this.RaiseTokenChanged();
        }

        protected virtual void dZTxtBox_ValueChanged(object sender, EventArgs e)
        {
            this.TokenObject.Direction = new OgreDirection(
                GUIUtil.ReadVector(this.dXTxtBox, this.dYTxtBox, this.dZTxtBox));
            this.RaiseTokenChanged();
        }

        protected virtual void colorBtn_Click(object sender, EventArgs e)
        {
            if (this.colorDialog1.ShowDialog() == DialogResult.OK)
            {
                this.TokenObject.Color = GUIUtil.ConvertToOgreColor(
                    this.colorDialog1.Color);
                this.colorBtn.BackColor = this.colorDialog1.Color;
            }
            this.RaiseTokenChanged();
        } 

        #endregion
    }
}