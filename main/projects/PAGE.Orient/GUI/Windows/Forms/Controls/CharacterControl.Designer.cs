using ListViewEx=PAGE.Orient.GUI.Windows.Forms.Util.ListViewEx;
using SubItemEndEditingEventHandler=PAGE.Orient.GUI.Windows.Forms.Util.SubItemEndEditingEventHandler;
using SubItemEventHandler=PAGE.Orient.GUI.Windows.Forms.Util.SubItemEventHandler;

namespace PAGE.Orient.GUI.Windows.Forms.Controls
{
    partial class CharacterControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.addTagBtn = new System.Windows.Forms.Button();
            this.removeTagBtn = new System.Windows.Forms.Button();
            this.removeAllTagBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.charTagsListBox = new System.Windows.Forms.ListView();
            this.label31 = new System.Windows.Forms.Label();
            this.addVoiceBtn = new System.Windows.Forms.Button();
            this.removeVoiceBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.languageTxtBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dZTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dYTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dXTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sZTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sYTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sXTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tZtxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tYtxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tXtxtBox)).BeginInit();
            this.tokenGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(22, 74);
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.Text = "Mesh File:";
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "*.mesh";
            this.openFileDialog.FileName = "character.mesh";
            this.openFileDialog.Filter = "Mesh files (*.mesh)|*.mesh";
            this.openFileDialog.Title = "Open Mesh File";
            // 
            // tokenIDLbl
            // 
            this.tokenIDLbl.Location = new System.Drawing.Point(7, 22);
            this.tokenIDLbl.Size = new System.Drawing.Size(70, 13);
            this.tokenIDLbl.Text = "Character ID:";
            // 
            // tokenGroupBox
            // 
            this.tokenGroupBox.AutoSize = true;
            this.tokenGroupBox.Controls.Add(this.languageTxtBox);
            this.tokenGroupBox.Controls.Add(this.addVoiceBtn);
            this.tokenGroupBox.Controls.Add(this.removeVoiceBtn);
            this.tokenGroupBox.Controls.Add(this.label3);
            this.tokenGroupBox.Controls.Add(this.label31);
            this.tokenGroupBox.Controls.Add(this.addTagBtn);
            this.tokenGroupBox.Controls.Add(this.removeTagBtn);
            this.tokenGroupBox.Controls.Add(this.removeAllTagBtn);
            this.tokenGroupBox.Controls.Add(this.label4);
            this.tokenGroupBox.Controls.Add(this.charTagsListBox);
            this.tokenGroupBox.Enabled = false;
            this.tokenGroupBox.Size = new System.Drawing.Size(323, 405);
            this.tokenGroupBox.Text = "Character Info";
            this.tokenGroupBox.Controls.SetChildIndex(this.idTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.tokenIDLbl, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.descTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label1, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.fileTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label10, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.openFileBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label11, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label12, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.tXtxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label13, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.tYtxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label14, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.tZtxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label18, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label17, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.sXTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label16, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.sYTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label2, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.sZTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label22, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label21, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.dXTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label20, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.dYTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label19, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.dZTxtBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label30, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.colorBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.charTagsListBox, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label4, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.removeAllTagBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.removeTagBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.addTagBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label31, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.label3, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.removeVoiceBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.addVoiceBtn, 0);
            this.tokenGroupBox.Controls.SetChildIndex(this.languageTxtBox, 0);
            // 
            // addTagBtn
            // 
            this.addTagBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.addTagBtn.Location = new System.Drawing.Point(242, 214);
            this.addTagBtn.Name = "addTagBtn";
            this.addTagBtn.Size = new System.Drawing.Size(75, 23);
            this.addTagBtn.TabIndex = 14;
            this.addTagBtn.Text = "Add";
            this.addTagBtn.UseVisualStyleBackColor = true;
            this.addTagBtn.Click += new System.EventHandler(this.addTagBtn_Click);
            // 
            // removeTagBtn
            // 
            this.removeTagBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.removeTagBtn.Enabled = false;
            this.removeTagBtn.Location = new System.Drawing.Point(242, 243);
            this.removeTagBtn.Name = "removeTagBtn";
            this.removeTagBtn.Size = new System.Drawing.Size(75, 23);
            this.removeTagBtn.TabIndex = 15;
            this.removeTagBtn.Text = "Remove";
            this.removeTagBtn.UseVisualStyleBackColor = true;
            this.removeTagBtn.Click += new System.EventHandler(this.removeTagBtn_Click);
            // 
            // removeAllTagBtn
            // 
            this.removeAllTagBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.removeAllTagBtn.Enabled = false;
            this.removeAllTagBtn.Location = new System.Drawing.Point(242, 272);
            this.removeAllTagBtn.Name = "removeAllTagBtn";
            this.removeAllTagBtn.Size = new System.Drawing.Size(75, 23);
            this.removeAllTagBtn.TabIndex = 16;
            this.removeAllTagBtn.Text = "Remove All";
            this.removeAllTagBtn.UseVisualStyleBackColor = true;
            this.removeAllTagBtn.Click += new System.EventHandler(this.removeAllTagBtn_Click);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 198);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 133;
            this.label4.Text = "Character tags:";
            // 
            // charTagsListBox
            // 
            this.charTagsListBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.charTagsListBox.GridLines = true;
            this.charTagsListBox.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.charTagsListBox.LabelEdit = true;
            this.charTagsListBox.Location = new System.Drawing.Point(10, 214);
            this.charTagsListBox.Name = "charTagsListBox";
            this.charTagsListBox.Size = new System.Drawing.Size(226, 81);
            this.charTagsListBox.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.charTagsListBox.TabIndex = 17;
            this.charTagsListBox.UseCompatibleStateImageBehavior = false;
            this.charTagsListBox.View = System.Windows.Forms.View.List;
            this.charTagsListBox.Validated += new System.EventHandler(this.charTagsListBox_Validated);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(162, 180);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(92, 13);
            this.label31.TabIndex = 134;
            this.label31.Text = "(used for subtitles)";
            // 
            // addVoiceBtn
            // 
            this.addVoiceBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.addVoiceBtn.Location = new System.Drawing.Point(242, 317);
            this.addVoiceBtn.Name = "addVoiceBtn";
            this.addVoiceBtn.Size = new System.Drawing.Size(75, 23);
            this.addVoiceBtn.TabIndex = 135;
            this.addVoiceBtn.Text = "Add";
            this.addVoiceBtn.UseVisualStyleBackColor = true;
            // 
            // removeVoiceBtn
            // 
            this.removeVoiceBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.removeVoiceBtn.Enabled = false;
            this.removeVoiceBtn.Location = new System.Drawing.Point(242, 346);
            this.removeVoiceBtn.Name = "removeVoiceBtn";
            this.removeVoiceBtn.Size = new System.Drawing.Size(75, 23);
            this.removeVoiceBtn.TabIndex = 136;
            this.removeVoiceBtn.Text = "Remove";
            this.removeVoiceBtn.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 301);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 139;
            this.label3.Text = "Character voices:";
            // 
            // columnHeader1
            // 
            this.columnHeader1.Tag = "";
            this.columnHeader1.Text = "Language";
            this.columnHeader1.Width = 102;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Voice";
            this.columnHeader2.Width = 119;
            // 
            // languageTxtBox
            // 
            this.languageTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.languageTxtBox.Location = new System.Drawing.Point(22, 353);
            this.languageTxtBox.Multiline = true;
            this.languageTxtBox.Name = "languageTxtBox";
            this.languageTxtBox.Size = new System.Drawing.Size(80, 16);
            this.languageTxtBox.TabIndex = 140;
            this.languageTxtBox.Visible = false;
            // 
            // CharacterControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "CharacterControl";
            this.Size = new System.Drawing.Size(323, 405);
            ((System.ComponentModel.ISupportInitialize)(this.dZTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dYTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dXTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sZTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sYTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sXTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tZtxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tYtxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tXtxtBox)).EndInit();
            this.tokenGroupBox.ResumeLayout(false);
            this.tokenGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addTagBtn;
        private System.Windows.Forms.Button removeTagBtn;
        private System.Windows.Forms.Button removeAllTagBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView charTagsListBox;
        protected System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button addVoiceBtn;
        private System.Windows.Forms.Button removeVoiceBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.TextBox languageTxtBox;
    }
}