/* ****************************************************
 * Name: GUIUtil.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/24 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Drawing;
using System.Windows.Forms;
using Mogre;
using PAGE.OGRE;

namespace PAGE.Orient.GUI.Windows.Forms.Util
{
    public class GUIUtil
    {
        public static Color ConvertToColor(OgreColor color)
        {
            return Color.FromArgb(
                (int) (color.A*255), (int) (color.R*255),
                (int) (color.G*255), (int) (color.B*255));
        }

        public static OgreColor ConvertToOgreColor(Color color)
        {
            return new OgreColor(
                color.R / 255.0f, color.G / 255.0f, color.B / 255.0f, color.A / 255.0f);
        }

        public static Vector3 ReadVector(NumericUpDown xTxtBox, NumericUpDown yTxtBox, NumericUpDown zTxtBox)
        {
            return new Vector3((float)xTxtBox.Value, (float)yTxtBox.Value, (float)zTxtBox.Value);
        }
    }
}