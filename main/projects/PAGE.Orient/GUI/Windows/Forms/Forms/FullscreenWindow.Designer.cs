/* ****************************************************
 * Name: FullscreenWindow.Designer.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/07/08
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Orient.GUI.Windows.Forms.Controls;

namespace PAGE.Orient.GUI.Windows.Forms.Forms
{
    partial class FullscreenWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.storyPanel = new PAGE.Orient.GUI.Windows.Forms.Controls.PreviewPanel(this.components);
            this.SuspendLayout();
            // 
            // storyPanel
            // 
            this.storyPanel.AllowNavigation = false;
            this.storyPanel.CaptureKeyboard = true;
            this.storyPanel.CaptureMouse = true;
            this.storyPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.storyPanel.Location = new System.Drawing.Point(0, 0);
            this.storyPanel.MoveSpeed = 50F;
            this.storyPanel.Name = "storyPanel";
            this.storyPanel.RotateSpeed = 50F;
            this.storyPanel.Size = new System.Drawing.Size(1016, 744);
            this.storyPanel.TabIndex = 27;
            this.storyPanel.UpdateInterval = ((long)(10));
            // 
            // FullscreenWindow
            // 
            this.ClientSize = new System.Drawing.Size(1016, 744);
            this.Controls.Add(this.storyPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.KeyPreview = true;
            this.Name = "FullscreenWindow";
            this.ResumeLayout(false);

        }

        #endregion

        private PreviewPanel storyPanel;
    }
}