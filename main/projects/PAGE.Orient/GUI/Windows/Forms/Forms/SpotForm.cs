using System.ComponentModel;
using System.Windows.Forms;
using PAGE.Generic.Resources.Assets;
using PAGE.Generic.Resources.Spots;
using PAGE.Orient.GUI.Windows.Forms.ComboBox;

namespace PAGE.Orient.GUI.Windows.Forms.Forms
{
    public partial class SpotForm : Form
    {
        protected bool closable = false;

        public SpotForm(ResourceSpot spot, ResourceAsset asset)
        {
            InitializeComponent();

            this.spotControl1.Asset = asset;
            this.spotControl1.Spot = spot;
            this.spotControl1.Text = "Edit spot: " + spot.IdToken;
            this.spotControl1.EditableSpotType = false;
        }

        public SpotForm(SpotListComboBox<ResourceSpot> spotCombo)
        {
            InitializeComponent();

            spotCombo.SpotControl = this.spotControl1;
        }

        [Browsable(true)]
        public bool Closable
        {
            get { return closable; }
            set { closable = value; }
        }

        [Browsable(true)]
        public bool EditableSpotType
        {
            get { return this.spotControl1.EditableSpotType; }
            set { this.spotControl1.EditableSpotType = value; }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = !this.closable;

            base.OnClosing(e);
        }
    }
}