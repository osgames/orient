/* ****************************************************
 * Name: FullscreenWindow.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/07/08
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using PAGE.Orient.GUI.Windows.Forms.Util;

namespace PAGE.Orient.GUI.Windows.Forms.Forms
{
    public partial class FullscreenWindow : Form
    {
        #region Fields

        protected string resourcesFile = "./data/resources.xml";
        protected uint originalScreenWidth;
        protected uint originalScreenHeight;
        protected uint screenHeight;
        protected uint screenWidth;

        #endregion

        #region Properties

        public uint ScreenHeight
        {
            get { return screenHeight; }
        }

        public uint ScreenWidth
        {
            get { return screenWidth; }
        }

        #endregion

        #region Constructors

        public FullscreenWindow(string resourcesFile)
            : this()
        {
            this.resourcesFile = resourcesFile;
        }

        public FullscreenWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Protected Methods

        protected void InitializeOrientScreen()
        {
            //stores current screen resolution
            Screen Srn = Screen.PrimaryScreen;
            this.originalScreenWidth = (uint) Srn.Bounds.Width;
            this.originalScreenHeight = (uint) Srn.Bounds.Height;

            //starts graphical realizer
            if ((this.resourcesFile != null) && (this.resourcesFile != ""))
                OrientGraphicsRealizer.Instance.Setup(this.resourcesFile, this.storyPanel);

            //if set to fullscreen
            if (OrientGraphicsRealizer.Instance.OgreApplication.ConfigFullscreen)
            {
                //changes to new screen resolution and maximizes window
                this.ChangeResolution(
                    OrientGraphicsRealizer.Instance.OgreApplication.ConfigScreenWidth,
                    OrientGraphicsRealizer.Instance.OgreApplication.ConfigScreenHeight);

                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = FormBorderStyle.None;
            }
            else
            {
                //changes only window size
                this.Size = new Size(
                    (int) OrientGraphicsRealizer.Instance.OgreApplication.ConfigScreenWidth,
                    (int) OrientGraphicsRealizer.Instance.OgreApplication.ConfigScreenHeight);
            }
        }

        protected void ChangeResolution(uint width, uint height)
        {
            this.screenWidth = width;
            this.screenHeight = height;
            this.Size = new Size((int) width, (int) height);
            Resolution.ChangeResolution(this.screenWidth, this.screenHeight);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            //changes screen resolution to original
            this.ChangeResolution(this.originalScreenWidth, this.originalScreenHeight);
        }

        #endregion
    }
}