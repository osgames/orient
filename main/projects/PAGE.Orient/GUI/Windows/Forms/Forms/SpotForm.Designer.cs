using PAGE.Orient.GUI.Windows.Forms.Controls;
using SpotControl=PAGE.Orient.GUI.Windows.Forms.Controls.SpotControl;

namespace PAGE.Orient.GUI.Windows.Forms.Forms
{
    partial class SpotForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        public SpotControl spotControl1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.spotControl1 = new SpotControl();
            this.SuspendLayout();
            // 
            // spotControl1
            // 
            this.spotControl1.Asset = null;
            this.spotControl1.EditableSpotType = false;
            this.spotControl1.Enabled = false;
            this.spotControl1.Location = new System.Drawing.Point(2, 2);
            this.spotControl1.Name = "spotControl1";
            this.spotControl1.Size = new System.Drawing.Size(323, 229);
            this.spotControl1.Spot = null;
            this.spotControl1.TabIndex = 37;
            // 
            // SpotForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 231);
            this.Controls.Add(this.spotControl1);
            this.MaximizeBox = false;
            this.Name = "SpotForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Spot Edition";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.ResumeLayout(false);

        }

        #endregion
    }
}