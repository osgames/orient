#region License

// <copyright owner="Cetin Sert">
//
//      Tenka Text
//      Open-source Corpus Analysis Software
//      Copyright (c) 2006, 2007 Cetin Sert.
//
//
//
//      GNU General Public License Version 3 Banner
//
//      This program is free software: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//      Cetin Sert
//      INF 521, 4-6-2
//      69120 Heidelberg
//      Baden-Wuerttemberg
//      Germany
//      
//      cetin.sert@gmail.com
//      
//      +49 151 177 610 28
//
// </copyright>

#endregion

using System;

namespace PAGE.Orient.Util
{
    public static class TextMath
    {
        public static class EditDistance
        {
            /// <summary>
            /// Compute Levenshtein distance
            /// Single Dimensional array vector unsafe version
            /// Memory efficient version
            /// Cetin Sert, Sten Hjelmqvist
            /// http://www.codeproject.com/cs/algorithms/Levenshtein.asp
            /// </summary>
            /// <returns>Levenshtein edit distance</returns>
            public static unsafe int UnsafeVectorLevenshtein(string string1, string string2)
            {
                fixed (char* s1 = string1)
                fixed (char* s2 = string2)
                {
                    int l1 = string1.Length; // length of s
                    int l2 = string2.Length; // length of t
                    int cost; // cost

                    /*
                    char* p1 = s1;
                    char* p2 = s2;
                    int ss = 0;
                    int es = 0;

                    // skip equal start sequence, if any
                    while (*p1 == *p2)
                    {
                        if (*p1 == '\0') break;
                        p1++; p2++; ss++;
                    }

                    l1 -= ss;
                    l2 -= ss;

                    // if we already used up one string, then
                    // the result is the length of the other
                    if (*p1 == '\0') return l2;
                    if (*p2 == '\0') return l1;

                    p1 += l1;
                    p2 += l2;

                    // cut of equal tail sequence, if any
                    if (l1 > l2)
                    {
                        while (p2 > s2 + ss && *--p1 == *--p2)
                        {
                            es++;
                        }
                    }
                    else
                    {
                        while (p1 > s1 + ss && *--p1 == *--p2)
                        {
                            es++;
                        }
                    }

                    l1 -= es;
                    l2 -= es;

                    // reset pointers, adjust length
                    p1 = s1 + ss;
                    p2 = s2 + ss;
                    */

                    // Step 1
                    if (l1 == 0) return l2;
                    if (l2 == 0) return l1;

                    /// Create the two vectors
                    int* v0 = stackalloc int[l1 + 1];
                    int* v1 = stackalloc int[l1 + 1];
                    int* vTmp;


                    // Step 2
                    // Initialize the first vector
                    for (int i = 1; i <= l1; i++) v0[i] = i;


                    // Step 3
                    // For each column - unsafe
                    for (int j = 1; j <= l2; j++)
                    {
                        v1[0] = j;

                        // Step 4
                        for (int i = 1; i <= l1; i++)
                        {
                            // Step 5
                            cost = (s1[i - 1] == s2[j - 1]) ? 0 : 1;

                            // Step 6
                            int m_min = v0[i] + 1;
                            int b = v1[i - 1] + 1;
                            int c = v0[i - 1] + cost;

                            if (b < m_min) m_min = b;
                            if (c < m_min) m_min = c;

                            v1[i] = m_min;
                        }

                        /// Swap the vectors
                        vTmp = v0;
                        v0 = v1;
                        v1 = vTmp;
                    }

                    // Step 7
                    return v0[l1];
                }
            }


            /// <summary>
            /// Compute Levenshtein distance
            /// Single Dimensional array vector version
            /// Memory efficient version
            /// Sten Hjelmqvist
            /// http://www.codeproject.com/cs/algorithms/Levenshtein.asp
            /// </summary>
            /// <returns>Levenshtein edit distance</returns>
            public static int VectorLevenshtein(string s, string t)
            {
                int n = s.Length; // length of s
                int m = t.Length; // length of t
                int cost; // cost

                // Step 1
                if (n == 0) return m;
                if (m == 0) return n;

                /// Create the two vectors
                int[] v0 = new int[n + 1];
                int[] v1 = new int[n + 1];
                int[] vTmp;


                /// Step 2
                /// Initialize the first vector
                for (int i = 1; i <= n; i++) v0[i] = i;


                // Step 3
                // Fore each column
                for (int j = 1; j <= m; j++)
                {
                    /// Set the 0'th element to the column number
                    v1[0] = j;

                    // Step 4
                    /// Fore each row
                    for (int i = 1; i <= n; i++)
                    {
                        // Step 5
                        cost = (s[i - 1] == t[j - 1]) ? 0 : 1;

                        // Step 6
                        /// Find minimum
                        int m_min = v0[i] + 1;
                        int b = v1[i - 1] + 1;
                        int c = v0[i - 1] + cost;

                        if (b < m_min) m_min = b;
                        if (c < m_min) m_min = c;

                        v1[i] = m_min;
                    }

                    /// Swap the vectors
                    vTmp = v0;
                    v0 = v1;
                    v1 = vTmp;
                }

                // Step 7
                return v0[n];
            }


            /// <summary>
            /// Compute Levenshtein distance
            /// 2 Dimensional array matrix version
            /// </summary>
            /// <returns>Levenshtein edit distance</returns>
            public static int MatrixLevenshtein(string s, string t)
            {
                int[,] matrix; // matrix
                int n = s.Length; // length of s
                int m = t.Length; // length of t
                int cost; // cost

                // Step 1
                if (n == 0) return m;
                if (m == 0) return n;

                // Create matirx
                matrix = new int[n + 1,m + 1];

                // Step 2
                // Initialize
                for (int i = 0; i <= n; i++) matrix[i, 0] = i;
                for (int j = 0; j <= m; j++) matrix[0, j] = j;

                // Step 3
                for (int i = 1; i <= n; i++)
                {
                    // Step 4
                    for (int j = 1; j <= m; j++)
                    {
                        // Step 5
                        if (s[i - 1] == t[j - 1])
                            cost = 0;
                        else
                            cost = 1;

                        // Step 6
                        // Find minimum
                        int min = matrix[i - 1, j] + 1;
                        int b = matrix[i, j - 1] + 1;
                        int c = matrix[i - 1, j - 1] + cost;

                        if (b < min) min = b;
                        if (c < min) min = c;

                        matrix[i, j] = min;
                    }
                }

                // Step 7
                return matrix[n, m];
            }


            /// <summary>
            /// Compute Levenshtein distance
            /// Jagged array matrix version
            /// </summary>
            /// <returns>Levenshtein edit distance</returns>
            public static int JaggedLevenshtein(string s, string t)
            {
                int[][] matrix; // matrix
                int n = s.Length; // length of s
                int m = t.Length; // length of t
                int cost; // cost

                // Step 1
                if (n == 0) return m;
                if (m == 0) return n;

                // Create matirx
                matrix = new int[n + 1][];


                // Step 2
                // Initialize
                for (int i = 0; i <= n; i++)
                {
                    matrix[i] = new int[m + 1];
                    matrix[i][0] = i;
                }
                for (int j = 0; j <= m; j++) matrix[0][j] = j;

                // Step 3
                for (int i = 1; i <= n; i++)
                {
                    // Step 4
                    for (int j = 1; j <= m; j++)
                    {
                        // Step 5
                        if (s[i - 1] == t[j - 1])
                            cost = 0;
                        else
                            cost = 1;

                        // Step 6
                        // Find minimum
                        int min = matrix[i - 1][j] + 1;
                        int b = matrix[i][j - 1] + 1;
                        int c = matrix[i - 1][j - 1] + cost;

                        if (b < min) min = b;
                        if (c < min) min = c;

                        matrix[i][j] = min;
                    }
                }

                // Step 7
                return matrix[n][m];
            }


            /// <summary>
            /// this is not needed for a couple of reasons:
            /// - string .Length is an Int32
            /// - not all the 32 bits of the .Length are used, some are control bits,
            ///   so you end up with 29 bits
            /// - CLR limits *all* reference type objects to 2GB of memory (on both 32 and 64 OS)
            /// </summary>
            private static void TestStringLength(string s, string t)
            {
                // Test string length
                if (Math.Max(s.Length, t.Length) > Math.Pow(2, 31))
                    throw new Exception("\nMaximum string length in Levenshtein.LD is "
                                        + Math.Pow(2, 31)
                                        + ".\nYours is "
                                        + Math.Max(s.Length, t.Length) + ".");
            }

            public static unsafe int YetiLevenshtein(string s1, string s2)
            {
                fixed (char* p1 = s1)
                fixed (char* p2 = s2)
                {
                    return YetiLevenshtein(p1, s1.Length, p2, s2.Length, 0); // substitutionCost = 1
                }
            }

            public static unsafe int YetiLevenshtein(string s1, string s2, int substitionCost)
            {
                int xc = substitionCost - 1;
                if (xc < 0 || xc > 1)
                {
                    throw new ArgumentException("", "substitionCost");
                }

                fixed (char* p1 = s1)
                fixed (char* p2 = s2)
                {
                    return YetiLevenshtein(p1, s1.Length, p2, s2.Length, xc);
                }
            }

            /// <summary>
            /// Cetin Sert, David Necas
            /// http://webcleaner.svn.sourceforge.net/viewvc/webcleaner/trunk/webcleaner2/wc/levenshtein.c?revision=6015&view=markup
            /// </summary>
            /// <param name="s1"></param>
            /// <param name="l1"></param>
            /// <param name="s2"></param>
            /// <param name="l2"></param>
            /// <param name="xcost"></param>
            /// <returns></returns>
            public static unsafe int YetiLevenshtein(char* s1, int l1, char* s2, int l2, int xcost)
            {
                int i;
                //int *row;  /* we only need to keep one row of costs */
                int* end;
                int half;

                /* strip common prefix */
                while (l1 > 0 && l2 > 0 && *s1 == *s2)
                {
                    l1--;
                    l2--;
                    s1++;
                    s2++;
                }

                /* strip common suffix */
                while (l1 > 0 && l2 > 0 && s1[l1 - 1] == s2[l2 - 1])
                {
                    l1--;
                    l2--;
                }

                /* catch trivial cases */
                if (l1 == 0)
                    return l2;
                if (l2 == 0)
                    return l1;

                /* make the inner cycle (i.e. string2) the longer one */
                if (l1 > l2)
                {
                    int nx = l1;
                    char* sx = s1;
                    l1 = l2;
                    l2 = nx;
                    s1 = s2;
                    s2 = sx;
                }

                //check len1 == 1 separately
                if (l1 == 1)
                {
                    //throw new NotImplementedException();
                    if (xcost > 0)
                        //return l2 + 1 - 2*(memchr(s2, *s1, l2) != NULL);
                        return l2 + 1 - 2*memchrRPLC(s2, *s1, l2);
                    else
                        //return l2 - (memchr(s2, *s1, l2) != NULL);
                        return l2 - memchrRPLC(s2, *s1, l2);
                }

                l1++;
                l2++;
                half = l1 >> 1;

                /* initalize first row */
                //row = (int*)malloc(l2*sizeof(int));
                int* row = stackalloc int[l2];
                if (l2 < 0)
                    //if (!row)
                    return (-1);
                end = row + l2 - 1;
                for (i = 0; i < l2 - (xcost > 0 ? 0 : half); i++)
                    row[i] = i;

                /* go through the matrix and compute the costs.  yes, this is an extremely
                 * obfuscated version, but also extremely memory-conservative and
                 * relatively fast.
                 */
                if (xcost > 0)
                {
                    for (i = 1; i < l1; i++)
                    {
                        int* p = row + 1;
                        char char1 = s1[i - 1];
                        char* char2p = s2;
                        int D = i;
                        int x = i;
                        while (p <= end)
                        {
                            if (char1 == *(char2p++))
                                x = --D;
                            else
                                x++;
                            D = *p;
                            D++;
                            if (x > D)
                                x = D;
                            *(p++) = x;
                        }
                    }
                }
                else
                {
                    /* in this case we don't have to scan two corner triangles (of size len1/2)
                     * in the matrix because no best path can go throught them. note this
                     * breaks when len1 == len2 == 2 so the memchr() special case above is
                     * necessary */
                    row[0] = l1 - half - 1;
                    for (i = 1; i < l1; i++)
                    {
                        int* p;
                        char char1 = s1[i - 1];
                        char* char2p;
                        int D, x;
                        /* skip the upper triangle */
                        if (i >= l1 - half)
                        {
                            int offset = i - (l1 - half);
                            int c3;

                            char2p = s2 + offset;
                            p = row + offset;
                            c3 = *(p++) + ((char1 != *(char2p++)) ? 1 : 0);
                            x = *p;
                            x++;
                            D = x;
                            if (x > c3)
                                x = c3;
                            *(p++) = x;
                        }
                        else
                        {
                            p = row + 1;
                            char2p = s2;
                            D = x = i;
                        }
                        /* skip the lower triangle */
                        if (i <= half + 1)
                            end = row + l2 + i - half - 2;
                        /* main */
                        while (p <= end)
                        {
                            int c3 = --D + ((char1 != *(char2p++)) ? 1 : 0);
                            x++;
                            if (x > c3)
                                x = c3;
                            D = *p;
                            D++;
                            if (x > D)
                                x = D;
                            *(p++) = x;
                        }
                        /* lower triangle sentinel */
                        if (i <= half)
                        {
                            int c3 = --D + ((char1 != *char2p) ? 1 : 0);
                            x++;
                            if (x > c3)
                                x = c3;
                            *p = x;
                        }
                    }
                }

                i = *end;
                return i;
            }

            private static unsafe int memchrRPLC(char* buffer, char c, int count)
            {
                char* p = buffer;
                char* e = buffer + count;
                while (p++ < e)
                {
                    if (*p == c)
                        return 1;
                }
                return 0;
            }
        } // class EditDistance
    }
}