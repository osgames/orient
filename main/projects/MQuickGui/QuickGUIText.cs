using System.Collections.Generic;
using Mogre;

namespace MQuickGUI
{
    public class TextWidget : Widget
    {
		private TruncateMode mTruncateMode;
		private string mFeedbackString;

		private TextAreaOverlayElement mTextAreaOverlayElement;

		// Stores pixel offsets of where text characters begin and end.
		// Useful for placing a text cursor widget.
		private List<int> mCursorPositions = new List<int>();

		private TextCursor mTextCursor;
		private int mCursorIndex;
		// Offset applied to cursor index position to appear in between character;
		private int mCursorOffset;
		private bool mTextCursorHidden;

        public enum TruncateMode
		{
			LEFT	,
			RIGHT   ,
			NONE
		};
        
        public TextWidget(string name, Vector3 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, OverlayContainer overlayContainer, Widget ParentWidget) : base(name,new Vector4(dimensions.x,dimensions.y,0,dimensions.z), positionMode, sizeMode,"",overlayContainer,ParentWidget)
	    {
		    mTextCursorHidden = true;
		    mCursorIndex = 0;
		    mCursorOffset = -3;
                        
            mWidgetType = WidgetType.QGUI_TYPE_TEXT;
		    mTruncateMode = TruncateMode.RIGHT;
		    mFeedbackString = "...";
		    mCursorPositions.Add(-5);
    		
		    mTextAreaOverlayElement = CreateTextAreaOverlayElement(mInstanceName+".Caption",mPixelDimensions,"");
		    mOverlayElement = mTextAreaOverlayElement;
		    mOverlayContainer.AddChild(mOverlayElement);
		    mTextAreaOverlayElement.Show();
		    mTextAreaOverlayElement.FontName = mFont;
		    mTextAreaOverlayElement.CharHeight = mPixelDimensions.w;

		    string tcMaterial = Sheet.DefaultSkin + ".cursor.text";
		    mTextCursor = new TextCursor(mInstanceName+".TextCursor",new Vector4(0,0,0,1), QGuiMetricsMode.QGUI_GMM_RELATIVE, QGuiMetricsMode.QGUI_GMM_RELATIVE, tcMaterial,mChildrenContainer,this);
		    mTextCursor.SetZOrderOffset(1,false);
		    mTextCursor.Hide();
	    }

	    ~TextWidget()
	    {
		    mTextCursor = null;
		    mTextAreaOverlayElement = null;
	    }
	    
	    public override void DestroyWidget()
		{
	    	if (mTextCursor!=null) {
	    		mTextCursor.DestroyWidget();
	    	}
			base.DestroyWidget();
		}

	    string _adjustWidth()
	    {
		    string displayText = mText;
		    // Adding a 5 pixel buffer, which helps textBoxes which have borders
		    int parentWidth = (int)mParentWidget.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).x - 5;


		    if( GetTextWidth(displayText) > parentWidth )
		    {
			    // calling getTextWidth indexes the text, so cursor can be placed within text.  Only reason
			    // we don't break out early with truncate mode set to NONE.
			    if(mTruncateMode != TruncateMode.NONE)
			    {
				    // get width of feedback string
				    float feedbackWidth = GetTextWidth(mFeedbackString);
				    float allotedWidth = parentWidth - feedbackWidth;
                    while ((GetTextWidth(displayText) > allotedWidth) && !string.Empty.Equals(displayText))
				    {
					    if (mTruncateMode == TruncateMode.RIGHT)
                            displayText = displayText.Substring(0,displayText.Length-1); // GDZ Converted
					    else if(mTruncateMode == TruncateMode.LEFT)
                            displayText = displayText.Substring(1); // GDZ Converted
				    }
				    // concatenate
                    if ((mTruncateMode == TruncateMode.RIGHT) && !string.Empty.Equals(mFeedbackString))
                        displayText =  displayText + mFeedbackString; // GDZ Converted
				    else if( (mTruncateMode == TruncateMode.LEFT) && !string.Empty.Equals(mFeedbackString) )
                        displayText = mFeedbackString + displayText; // GDZ Converted
			    }
		    }

		    return displayText;
	    }

        protected override void _applyDimensions()
	    {
		    mTextAreaOverlayElement.SetPosition(mPixelDimensions.x,mPixelDimensions.y);
		    mTextAreaOverlayElement.SetDimensions(mPixelDimensions.z,mPixelDimensions.w);
		    mTextAreaOverlayElement.CharHeight = mPixelDimensions.w;
	    }

        internal override void _notifyDimensionsChanged()
        {
            _updateDimensions(mRelativeDimensions);
            _applyDimensions();
        }

        protected override void _notifyTextChanged()
	    {
		    // adjust width to match the width of text
		    mRelativeDimensions.z = (TextWidth / mParentWidget.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).x);
		    _notifyDimensionsChanged();
	    }

	    private float ConvertPosToIndex(Vector2 p)
	    {
            int numIndices = mCursorPositions.Count;
            float xPos = p.x;

            if (numIndices == 1) // If there is only 1 index, we return that index.
                return 0;
            else
                if (xPos < mCursorPositions[0] + mPixelDimensions.x) // If position is to the left of the left most index, return first index.
                    return 0; 
                else
                    if (xPos > mCursorPositions[numIndices - 1] + mPixelDimensions.x) // If position is to the right of the right most index, return last index.
                        return (numIndices - 1);

            // If we make it here we know that the mouse position is between the beginning and end of our index positions.

		    int index;
            for (index = 1; index < numIndices; ++index)
            {
                if (xPos < mCursorPositions[index] + mPixelDimensions.x)
                {
                    // determine if point is closer to the left or right index
                    int range = mCursorPositions[index] - mCursorPositions[index - 1];
                    if ((mCursorPositions[index] - xPos) >= (range / 2))
                        return index;
                    else
                        return index - 1;
                }
            }

            // Not supposed to be able to reach this point.
            return 0;
        }

        //new protected void Deactivate(EventArgs e)
        //{
        //    mParentWidget.Deactivate(e);
        //}

        internal void PlaceCursorBeginning()
        {
            this.mCursorIndex = 0;
            this.DecrementCursorIndex();
        }

        internal void PlaceCursorEnd()
        {
            this.mCursorIndex = mCursorPositions.Count - 1;
            this.IncrementCursorIndex();
        }

        internal void IncrementCursorIndex()
        {
            ++mCursorIndex;
            if (mCursorIndex >= mCursorPositions.Count)
                --mCursorIndex;
            mTextCursor.Show();

            int parentWidth = (int)mParentWidget.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).x - 5;
            if (GetTextWidth(this.mText) < parentWidth)
                this.mTextCursor.SetPixelPosition(GetCursorPosition(mCursorIndex), mPixelDimensions.y);

            //mTextCursor.SetPosition(GetCursorPosition(mCursorIndex), mPixelDimensions.y, QGuiMetricsMode.QGUI_GMM_PIXELS);
        }

	    internal void DecrementCursorIndex()
	    {
		    if( mCursorIndex > 0 ) --mCursorIndex;
		    mTextCursor.Show();

            int parentWidth = (int)mParentWidget.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).x - 5;
            if (GetTextWidth(this.mText) < parentWidth)
                this.mTextCursor.SetPixelPosition(GetCursorPosition(mCursorIndex), mPixelDimensions.y);

            //mTextCursor.SetPosition(GetCursorPosition(mCursorIndex), mPixelDimensions.y, QGuiMetricsMode.QGUI_GMM_PIXELS);
	    }

	    float GetCursorPosition(int index)
	    {
            int numCursorPositions = mCursorPositions.Count;

            // check bounds
            if (index >= numCursorPositions)
                return (mPixelDimensions.x + mCursorPositions[numCursorPositions - 1] + mCursorOffset);
            else
                if (index < 0)
                    return (mPixelDimensions.x + mCursorPositions[0] + mCursorOffset);
            
            return (mPixelDimensions.x + mCursorPositions[index] + mCursorOffset);
	    }

        internal int CursorIndex
        {
            get { return mCursorIndex; }
        }

        public float TextWidth
        {
            get { return GetTextWidth(mText); }
        }

        public float GetTextWidth(string text)
	    {
		    mCursorPositions.Clear();
		    // Store first cursor position
		    mCursorPositions.Add(0);

            if (string.Empty.Equals(text))
                return 0.0f;

		    FontManager fm = FontManager.Singleton;
            FontPtr f = (FontPtr)fm.GetByName(mFont);
		    float width = 0.0f;
		    for( int index = 0; index < text.Length; ++index )
		    {
			    if( text[index] == ' ' ) width += (f.GetGlyphAspectRatio('0') * mPixelDimensions.w);
			    else width += (f.GetGlyphAspectRatio(text[index]) * mPixelDimensions.w);
			    mCursorPositions.Add((int)width);
		    }

		    // now we know the text width in pixels, and have index positions at the start/end of each character.

		    return width;
	    }

	    internal void HideTextCursor()
	    {
		    mTextCursor.Hide();
		    mTextCursorHidden = true;
	    }

        public override float CharacterHeight
        {
            set
            {
                // Enforce the Text Widget's dimensions to match the Actual Text Dimensions,
                // as Text is not bounded to it's overlay element size

                SetHeight(value);
            }
        }

        internal void SetTruncationFeedback(string visualFeedback)
	    {
		    mFeedbackString = visualFeedback;
		    Text = mText;
	    }

        public override string Font
        {
            set
            {
                mFont = value;
                _notifyTextChanged();
            }
        }

        public override void SetHeight(float relativeHeight)
	    {
		    mRelativeDimensions.w = relativeHeight;
		    _notifyTextChanged();
	    }

        public override string Text
        {
            set
            {
                mText = value;

                mTextAreaOverlayElement.FontName = mFont;
                mTextAreaOverlayElement.CharHeight = mPixelDimensions.w;
                mTextAreaOverlayElement.ColourTop = mTextTopColor;
                mTextAreaOverlayElement.ColourBottom = mTextBotColor;
                mTextAreaOverlayElement.Caption = _adjustWidth();

                _notifyTextChanged();
            }
            get { return base.Text; }
        }

        internal void SetTextCursorPosition(Vector2 p)
	    {
		    mCursorIndex = (int)ConvertPosToIndex(p);
            //mTextCursor.setPosition(getCursorPosition(mCursorIndex),mPixelDimensions.y,QGUI_GMM_PIXELS);
            mTextCursor.SetPixelPosition(GetCursorPosition(mCursorIndex), mPixelDimensions.y);
	    }

        internal void SetTruncateMode(TruncateMode MODE)
	    {
		    mTruncateMode = MODE;
		    Text = mText;
	    }

        public override void Show()
	    {
		    if(!mTextCursorHidden)
                mTextCursor.Show();
		    base.Show();
	    }

        protected void ShowTextCursor()
	    {
		    mTextCursor.Show();
		    mTextCursorHidden = false;
	    }

	    internal void ToggleTextCursorVisibility()
	    {
		    mTextCursor.ToggleVisibility();
		    mTextCursorHidden = !mTextCursorHidden;
	    }

    }
}
