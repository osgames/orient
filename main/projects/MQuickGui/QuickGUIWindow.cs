using Mogre;

namespace MQuickGUI
{
    public class Window : Panel
    {
		protected Overlay mOverlay;
		protected bool mTitleBarHidden;
		protected OverlayContainer mTitleBarContainer;
		protected TitleBar mTitleBar;

	    public Window(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material, Widget parentWidget) : base(name,dimensions, positionMode, sizeMode, material, null, parentWidget)
	    {
            mTitleBar = null;
		    mTitleBarHidden = false;


		    mWidgetType = WidgetType.QGUI_TYPE_WINDOW;
		    mOverlay = OverlayManager.Singleton.Create(mInstanceName+".Overlay");
		    mOverlay.ZOrder = 0;
		    mOverlay.Show();
		    mZOrderValues.Add(0);

		    // mChildrenContainer already created in Widget constructor
		    mTitleBarContainer = CreateOverlayContainer(mInstanceName+".TitleBarContainer","");
    		
		    mOverlay.Add2D(mOverlayContainer);
		    mChildrenContainer.AddChildImpl(mTitleBarContainer);
    		
		    mOverlayContainer.Show();
		    mChildrenContainer.Show();
		    mTitleBarContainer.Show();

		    // Create TitleBar - tradition titlebar dimensions: across the top of the window
		    Vector4 defaultTitleBarDimensions = new Vector4(0f,0f,1f,0.05f / GetSize(QGuiMetricsMode.QGUI_GMM_ABSOLUTE).y);
		    mTitleBar = new TitleBar(mInstanceName+".Titlebar",defaultTitleBarDimensions, QGuiMetricsMode.QGUI_GMM_RELATIVE, QGuiMetricsMode.QGUI_GMM_RELATIVE, mWidgetMaterial+".titlebar",mTitleBarContainer,this);
		    mTitleBar.EnableDragging = true;
		    mTitleBar.DraggingWidget = this;
		    mTitleBar.TextWidget.EnableDragging = true;
		    mTitleBar.TextWidget.DraggingWidget = this;
		    mTitleBar.SetZOrderOffset(1);
		    _addChildWidget(mTitleBar);

            // Now that mOverlayContainer has been created (via _init() function) we can create the borders
		    _createBorders();

            OnActivate += new ActivateEventHandler(Window_OnActivate);
	    }

        void Window_OnActivate(object source, EventArgs e)
        {
            Sheet.SetActiveWindow(this);
        }

	    public override void DestroyWidget()
	    {
	    	_destroyBorders();
		    OverlayManager om = OverlayManager.Singleton;

		    // Delete all child widgets before deleting the window widget
		    RemoveAndDestroyAllChildWidgets();
    		
            //// destroy background overlay element
            mOverlayContainer.RemoveChild(mOverlayElement.Name);
            om.DestroyOverlayElement(mOverlayElement);
            mOverlayElement = null;

            // destroy TitleBar container
            mChildrenContainer.RemoveChild(mTitleBarContainer.Name);
            om.DestroyOverlayElement(mTitleBarContainer);
            mTitleBarContainer = null;
    	
            // destroy Children container
            mOverlayContainer.RemoveChild(mChildrenContainer.Name);
            om.DestroyOverlayElement(mChildrenContainer);
            mChildrenContainer = null;
    		
            // destroy default container
            mOverlay.Remove2D(mOverlayContainer);
            om.DestroyOverlayElement(mOverlayContainer);
            mOverlayContainer = null;

            // destroy overlay
            om.Destroy(mOverlay);
//            base.DestroyWidget();
			
			UnregisterZOrder(false);
			mParentWidget = null;

			GUIManager.Singleton.RemoveWidgetName(mInstanceName);
	    }

        public TitleBar TitleBar
        {
            get { return mTitleBar; }
        }

        protected void HideCloseButton()
	    {
		    if(mTitleBar!=null)
                mTitleBar.HideCloseButton();
	    }

	    public void HideTitlebar()
	    {
		    if(mTitleBar!=null) 
		    {
			    mTitleBar.Hide();
			    mTitleBarHidden = true;
		    }
	    }

        public override string Text
        {
            set
            {
                mText = value;
                mTitleBar.Font = mFont;
                mTitleBar.CharacterHeight = mCharacterHeight;
                mTitleBar.SetTextColor(mTextTopColor, mTextBotColor);
                mTitleBar.Text = mText;
            }
            get { return base.Text; }
        }

        public override void SetTextColor(ColourValue topColor, ColourValue botColor)
	    {
		    mTextTopColor = topColor;
		    mTextBotColor = botColor;
		    mTitleBar.Font = mFont;
		    mTitleBar.CharacterHeight = mCharacterHeight;
		    mTitleBar.SetTextColor(mTextTopColor,mTextBotColor);
		    mTitleBar.Text = mText;
	    }

        public float TitleBarHeight
        {
            set
            {
                if (value > mOverlayElement.Height)
                    value = mOverlayElement.Height;

                if (mTitleBar != null)
                    mTitleBar.SetHeight(value);
            }
        }

        public new int ZOrder
        {
            set
            {
                // offset relative to parent sheet, which is using an overlay of zOrder 0.
                int previousZOrderOffset = mZOrderOffset;
                mZOrderOffset = value;

                int difference = mZOrderOffset - previousZOrderOffset;
                mOverlay.ZOrder = (ushort) mZOrderOffset;

                // Update zOrder Value List
                //std::list<int>::iterator it;
                //for( it = mZOrderValues.begin(); it != mZOrderValues.end(); ++it )
                for (int j = 0; j < mZOrderValues.Count; j++)
                {
                    mZOrderValues[j] += difference;
                }
            }
            get { return base.ZOrder; }
        }

        public override void Show()
	    {
		    base.Show();

            if (mTitleBarHidden)
                HideTitlebar(); 
	    }

        public void ShowCloseButton()
	    {
		    if(mTitleBar != null)
                    mTitleBar.ShowCloseButton();
	    }

        public void ShowTitlebar()
	    {
            if (mTitleBar != null) 
		    {
			    mTitleBar.Show();
			    mTitleBarHidden = false;
		    }
	    }
    }
}
