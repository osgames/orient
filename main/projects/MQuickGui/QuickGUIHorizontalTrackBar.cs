using System.Collections.Generic;
using Mogre;

namespace MQuickGUI
{
    public delegate void ValueEventHandler(object source, ValueEventArgs e);

    public class HorizontalTrackBar : Widget
    {
		public event ValueEventHandler OnValueChanged;

        protected int mCurrentValue = 0;
        // Value added to current value when user clicks on the track bar.
        protected int mLargeChange = 3;

        // ----- TRACK PROPERTIES --------------------
        // number of available positions for positioning the slider.
        protected int mNumRegions = 5;
        // Length between a region begin and endpoint;
        protected float mRegionLength;

        protected List<float> mSliderPositions = new List<float>();

        protected Button mSliderButton = null;
        protected string mSliderTextureName;


        public HorizontalTrackBar(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material, OverlayContainer overlayContainer, Widget ParentWidget)
            : base(name, dimensions, positionMode, sizeMode, material, overlayContainer, ParentWidget)
        {
            this.mWidgetType = WidgetType.QGUI_TYPE_HORIZONTAL_TRACKBAR;
            this.mWidgetMaterial = this.Sheet.DefaultSkin + ".trackbar.horizontal";

            this.mOverlayElement = CreatePanelOverlayElement(mInstanceName + "_Background", mPixelDimensions, "");
            this.mOverlayContainer.AddChild(mOverlayElement);
            this.mOverlayElement.Show();
            this.Material = this.mWidgetMaterial;

            OnMouseButtonDown += new MouseButtonDownEventHandler(QuickGUIHorizontalTrackBar_OnMouseButtonDown);

            // Create slider button at the beginning of the HorizontalTrackBar.
            this.mSliderButton =
                new Button(this.mInstanceName + ".Slider", new Vector4(0, 0, 0.1f, 1),
                           QGuiMetricsMode.QGUI_GMM_RELATIVE, QGuiMetricsMode.QGUI_GMM_RELATIVE,
                           this.mWidgetMaterial + ".slider", mChildrenContainer, this);
            this.mSliderButton.SetZOrderOffset(1);
            _addChildWidget(this.mSliderButton);

            this.mSliderButton.EnableDragging = true;
            this.mSliderButton.OnDragged += new DraggedEventHandler(mSliderButton_OnDragged);

            this._getSliderPositions();
            this.Value = 0;
        }

        protected void mSliderButton_OnDragged(object source, WidgetEventArgs e)
        {
            int indexFromCursor = getClosestSliderPosition(GUIManager.Singleton.MouseCursor.PixelPosition);
            this.Value = indexFromCursor;
        }

        protected virtual void QuickGUIHorizontalTrackBar_OnMouseButtonDown(object source, MouseEventArgs e)
        {
            Vector2 mousePos = e.position - this.GetPosition(QGuiMetricsMode.QGUI_GMM_PIXELS);

            if ((mousePos.x > (mSliderPositions[mCurrentValue] + ((mRegionLength + mSliderButton.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).x)/2.0f))) &&
                (mCurrentValue < mNumRegions))
                this.Value = mCurrentValue + mLargeChange;
            else if ((mousePos.x < (mSliderPositions[mCurrentValue] - ((mRegionLength - mSliderButton.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).x)/2.0))) &&
                     (mCurrentValue > 0))
                this.Value = mCurrentValue - mLargeChange;
        }

        public void Dispose()
        {
            this.RemoveAndDestroyAllChildWidgets();
            this.mSliderButton = null;
        }

        protected virtual void _getSliderPositions()
        {
            this.mSliderPositions.Clear();

            float sliderPixelWidth = this.mSliderButton.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).x;
            float trackLength = this.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).x - sliderPixelWidth;
            this.mRegionLength = trackLength/this.mNumRegions;

            float xStart = (sliderPixelWidth/2.0f);
            mSliderPositions.Add(xStart);
            for (int i = 0; i < mNumRegions; ++i)
            {
                this.mSliderPositions.Add(xStart + ((i + 1)*mRegionLength));
            }
        }

	    protected virtual int getClosestSliderPosition(Vector2 pixelPoint)
	    {
	        Vector2 p = pixelPoint - this.GetPosition(QGuiMetricsMode.QGUI_GMM_PIXELS);

	        if (p.x <= mSliderPositions[0])
	            return 0;
	        if (p.x >= mSliderPositions[mSliderPositions.Count - 1])
	            return (mSliderPositions.Count - 1);

	        int i;
	        for (i = 1; i < mSliderPositions.Count; ++i)
	        {
	            if (p.x < (mSliderPositions[i] - (mRegionLength/2.0)))
	                return (i - 1);
	        }

	        return i;
	    }

        public int LargeChange
	    {
		    get { return this.mLargeChange; }
            set { this.mLargeChange = value; }
	    }

	    public int NumRegions
	    {
		    get { return mNumRegions; }
            set
            {
                this.mNumRegions = value;
                _getSliderPositions();
            }
	    }
	
	    public int NumTicks
	    {
		    get { return (mNumRegions + 1); }
            set { this.NumRegions = value - 1; }
	    }

	    public virtual int Value
        {
            get { return mCurrentValue; }
            set
            {
                if (value < 0) value = 0;
                else if (value > this.mNumRegions) value = mNumRegions;

                this.mCurrentValue = value;

                this.mSliderButton.SetPosition(
                    (this.mSliderPositions[mCurrentValue] - (this.mSliderButton.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).x/2.0f)) /
                    this.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).x, 0);

                ValueEventArgs e = new ValueEventArgs(this, this.mCurrentValue);
                e.handled = false;
                if (this.OnValueChanged != null) this.OnValueChanged(this, e);
            }
        }

	    public float Percentage
	    {
            get { return (float)mCurrentValue / (float)mNumRegions; }
	    }


        public Button SliderButton
	    {
    		get { return this.mSliderButton; }
        }
    }
}
