using System;
using System.Collections.Generic;
using Mogre;

namespace MQuickGUI
{
    /** Represents a Widget Container.
        @remarks
        The Panel class has the ability to create the majority of defined Widgets.
        The Sheet and Window Widgets derive from this widget (Panel), giving them the
        same abilities.
        @note
        Panels cannot create the TitleBar, Window, or Sheet widget.
        @note
        Panels are meant to be created via the Window and Sheet widget.
    */
    public class Panel : Widget
    {
		protected List<int>					mZOrderValues = new List<int>();

		protected int						mAutoNameWidgetCounter;

		// Recording the number of widgets created
        protected int                       mNumHorizontalTrackBars;
        protected int                       mNumVerticalTrackBars;
        protected int                       mNumCheckBoxes;
		protected int						mNumButtons;
		protected int						mNumComboBoxes;
		protected int						mNumImages;
		protected int						mNumLabels;
		protected int						mNumLists;		
		protected int						mNumMenus;
		protected int						mNumNStateButtons;
		protected int						mNumPanels;
		protected int						mNumProgressBars;
		protected int						mNumTextBoxes;
        protected int                       mNumTrackBars;

        /** Constructor
            @param
                name The name to be given to the widget (must be unique).
            @param
                dimensions The x Position, y Position, width, and height of the widget.
            @param
                positionMode The GuiMetricsMode for the values given for the position. (absolute/relative/pixel)
            @param
                sizeMode The GuiMetricsMode for the values given for the size. (absolute/relative/pixel)
            @param
                material Ogre material defining the widget image.
            @param
                overlayContainer associates the internal OverlayElement with a specified zOrder.
            @param
                parentWidget parent widget which created this widget.
        */
        public Panel(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material, OverlayContainer overlayContainer, Widget parentWidget)
            : base(name, dimensions, positionMode, sizeMode, material, overlayContainer, parentWidget)
	    {
		    mAutoNameWidgetCounter = 0;
		    mNumButtons = 0;
            mNumCheckBoxes = 0;
		    mNumComboBoxes = 0;
            mNumHorizontalTrackBars = 0;
		    mNumImages = 0;
		    mNumLabels = 0;
		    mNumLists = 0;
		    mNumMenus = 0;
		    mNumNStateButtons = 0;
		    mNumPanels = 0;
		    mNumProgressBars = 0;
		    mNumTextBoxes = 0;
            mNumTrackBars = 0;

            mWidgetType = WidgetType.QGUI_TYPE_PANEL;
		    mZOrderValues.Clear();

		    if( overlayContainer == null )
		    {
			    mOverlayContainer = CreateOverlayContainer(mInstanceName+".OverlayContainer","");
			    mOverlayContainer.AddChildImpl(mChildrenContainer);

			    mOverlayContainer.Show();
			    mChildrenContainer.Show();
		    }

		    mOverlayElement = CreatePanelOverlayElement(mInstanceName+".Background",mPixelDimensions,"");
		    mOverlayContainer.AddChild(mOverlayElement);
		    mOverlayElement.Show();
            Material = mWidgetMaterial;

            OnActivate += new ActivateEventHandler(Panel_OnActivate);
            OnDeactivate += new DeactivateEventHandler(Panel_OnDeactivate);
	    }
        
        

        public void _addZOrderValue(int zOrder)
	    {
		    mZOrderValues.Add(zOrder);  //REPLACED: ->push_back(zOrder)
		    mZOrderValues.Sort();
	    }

        public void _removeZOrderValue(int zOrder)
        {
            //std::list<int>::iterator it;
            //for( it = mZOrderValues.begin(); it != mZOrderValues.end(); ++it )
            //{
            //    if( *it == zOrder ) mZOrderValues.erase(it);
            //    return;
            //}
            int item;
            for (int index = mZOrderValues.Count; index > 0; index--)
            {
                item = mZOrderValues[index - 1];
                if (item == zOrder)
                    mZOrderValues.Remove(item);
            }

        }

        /** 
        * Private functions preventing users from setting the Widget Instance Name.  Names
        * can be given to Windows using the "setReferenceName()" function.
        */
        Button _createButton(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            Button newButton = new Button(name,dimensions, positionMode, sizeMode, material,mChildrenContainer,this);
            newButton.SetZOrderOffset(1);
            if(!mVisible)
                newButton.Hide();
            _addChildWidget(newButton);
            // update count
            ++mNumButtons;
    		
            return newButton;
        }

        CheckBox _createCheckBox(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            CheckBox checkBox = new CheckBox(name, dimensions, positionMode, sizeMode, material, mChildrenContainer, this);
            checkBox.SetZOrderOffset(1);
            if (!mVisible)
                checkBox.Hide();
            _addChildWidget(checkBox);
            // update count
            ++mNumCheckBoxes;

            return checkBox;
        }

        ProgressBar _createProgressBar(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            ProgressBar progressBar = new ProgressBar(name, dimensions, positionMode, sizeMode, material, mChildrenContainer, this);
            progressBar.SetZOrderOffset(1);
            if (!mVisible)
                progressBar.Hide();
            _addChildWidget(progressBar);
            // update count
            ++mNumProgressBars;

            return progressBar;
        }

        HorizontalTrackBar _createHorizontalTrackBar(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            HorizontalTrackBar horizontalTrackBar = new HorizontalTrackBar(name, dimensions, positionMode, sizeMode, material, mChildrenContainer, this);
            horizontalTrackBar.SetZOrderOffset(1);
            if (!mVisible)
                horizontalTrackBar.Hide();
            _addChildWidget(horizontalTrackBar);
            // update count
            ++mNumHorizontalTrackBars;

            return horizontalTrackBar;
        }

        VerticalTrackBar _createVerticalTrackBar(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            VerticalTrackBar verticalTrackBar = new VerticalTrackBar(name, dimensions, positionMode, sizeMode, material, mChildrenContainer, this);
            verticalTrackBar.SetZOrderOffset(1);
            if (!mVisible)
                verticalTrackBar.Hide();
            _addChildWidget(verticalTrackBar);
            // update count
            ++mNumVerticalTrackBars;

            return verticalTrackBar;
        }

        ComboBox _createComboBox(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            ComboBox newComboBox = new ComboBox(name,dimensions, positionMode, sizeMode, material,mChildrenContainer,this);
            newComboBox.SetZOrderOffset(1);
            if(!mVisible)
                newComboBox.Hide();
            _addChildWidget(newComboBox);
            // update count
            ++mNumComboBoxes;
    		
            return newComboBox;
        }

       	Image _createImage(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material, bool texture)
	    {
		    string defaultMaterial = Sheet.DefaultSkin + ".image";
		    Image newImage = new Image(name,dimensions,positionMode,sizeMode,defaultMaterial,mChildrenContainer,this);
		    if (texture)
                newImage.SetMaterial(material,true);
		    else
                newImage.Material = material;

		    newImage.SetZOrderOffset(1);
		    if(!mVisible)
                newImage.Hide();

		    _addChildWidget(newImage);
		    // update count
		    ++mNumImages;

		    return newImage;
	    }

	    Label _createLabel(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
	    {
		    Label newLabel = new Label(name,dimensions,positionMode,sizeMode,material,mChildrenContainer,this);
		    newLabel.SetZOrderOffset(1);
		    if(!mVisible)
                newLabel.Hide();
		    _addChildWidget(newLabel);
		    // update count
		    ++mNumLabels;

		    return newLabel;
	    }

	    List _createList(string name, Vector3 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
	    {
		    List newList = new List(name,dimensions,positionMode,sizeMode,material,mChildrenContainer,this);
		    newList.SetZOrderOffset(1);
		    if(!mVisible)
                newList.Hide();
		    _addChildWidget(newList);
		    // update count
		    ++mNumLists;

		    return newList;
	    }

	    List _createList(string name, Vector3 dimensions, Label itemTemplate, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
	    {
		    List newList = new List(name,dimensions,itemTemplate,positionMode,sizeMode,material,mChildrenContainer,this);
		    newList.SetZOrderOffset(1);
		    if(!mVisible)
                newList.Hide();
		    _addChildWidget(newList);
		    // update count
		    ++mNumLists;

		    return newList;
	    }

	    Menu _createMenu(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
	    {
		    Menu newMenu = new Menu(name,dimensions,positionMode,sizeMode,material,mChildrenContainer,this);
		    newMenu.SetZOrderOffset(1);
		    if(!mVisible)
                newMenu.Hide();
		    _addChildWidget(newMenu);
		    // update count
		    ++mNumMenus;

		    return newMenu;
	    }
    
	    NStateButton _createNStateButton(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    NStateButton newNStateButton = new NStateButton(name,dimensions,positionMode,sizeMode,mChildrenContainer,this);
		    newNStateButton.SetZOrderOffset(1);
		    if(!mVisible)
                newNStateButton.Hide();
		    _addChildWidget(newNStateButton);
		    // update count
		    ++mNumNStateButtons;

		    return newNStateButton;
	    }

		Panel _createPanel(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
		{
			Panel newPanel = new Panel(name,dimensions,positionMode,sizeMode,material,mChildrenContainer,this);
			newPanel.SetZOrderOffset(1);
			if(!mVisible) {
				newPanel.Hide();
			}
			_addChildWidget(newPanel);
			// update count
			++mNumPanels;
	
			return newPanel;
		}

	    
        TextBox _createTextBox(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
	    {
		    TextBox newTextBox = new TextBox(name,dimensions,positionMode,sizeMode,material,mChildrenContainer,this);
		    newTextBox.SetZOrderOffset(1);
		    if(!mVisible)
                newTextBox.Hide();
		    _addChildWidget(newTextBox);
		    // update count
		    ++mNumTextBoxes;

		    return newTextBox;
	    }


	    bool _destroyWidget(WidgetType type, int index)
	    {
		    int counter = -1;
            //std::vector<Widget*>::iterator it;
            //for( it = mChildWidgets.begin(); it != mChildWidgets.end(); ++it )
            //{
            //    if( (*it)->getWidgetType() == type )
            //    {
            //        ++counter;
            //        if( counter == index )
            //        {
            //            Widget* w = (*it);
            //            mChildWidgets.erase(it);
            //            delete w;
            //            return true;
            //        }
            //    }
            //}

            // TODO: Verificar que puedo hacer el remove de mChildWidgets sin romper el foreach // GDZ
            foreach(Widget w in mChildWidgets) {
                if (w.WidgetType == type)
                {
                    ++counter;
                    if (counter == index) {
                        mChildWidgets.Remove(w);
                        return true;
                    }
                }
            }

		    return false;
	    }

	    bool _destroyWidget(string name)
	    {
            //std::vector<Widget*>::iterator it;
            //for( it = mChildWidgets.begin(); it != mChildWidgets.end(); ++it )
            //{
            //    if( (*it)->getInstanceName() == name )
            //    {
            //        Widget* w = (*it);
            //        mChildWidgets.erase(it);
            //        delete w;
            //        return true;
            //    }
            //}
            // TODO: Verificar que puedo hacer el remove de mChildWidgets sin romper el foreach // GDZ
            foreach(Widget w in mChildWidgets) {
                if (w.InstanceName.Equals(name))
                {
                	w.DestroyWidget();
                    mChildWidgets.Remove(w);
                    return true;
                }
            }
		    return false;
	    }

   	    Widget _getWidget(WidgetType type, int index)
	    {
		    int counter = -1;
            foreach(Widget w in mChildWidgets) {
                if (w.WidgetType == type) {
                    ++counter;
                    if (counter == index) {
                        return w;
                    }
                }
            }
		    return null;
	    }

	    Widget _getWidget(string name)
	    {
            foreach(Widget w in mChildWidgets) {
                if (w.InstanceName == name) {
                    return w;
                }
            }
		    return null;
	    }

        void Panel_OnActivate(object source, EventArgs e)
        {
   		    if(!mEnabled)
                return;

            foreach(Widget w in mChildWidgets) {
                w.Activate(e);
            }
        }
    
        void Panel_OnDeactivate(object source, EventArgs e)
        {
		    if(!mEnabled)
                return;

            foreach(Widget w in mChildWidgets) {
                w.Deactivate(e);
            }
        }


        public Button CreateButton(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            if (!(GUIManager.Singleton.ValidWidgetName(name)))
                return null;

            return _createButton(name, dimensions, positionMode, sizeMode, material);
        }

        public Button CreateButton(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
            ++mAutoNameWidgetCounter;

            return _createButton(name, dimensions, positionMode, sizeMode, material);
        }

        public Button CreateButton(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
        {
            if (!(GUIManager.Singleton.ValidWidgetName(name)))
                return null;

            string material = Sheet.DefaultSkin + ".button";

            return _createButton(name, dimensions, positionMode, sizeMode, material);
        }

        public Button CreateButton(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
        {
            string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
            ++mAutoNameWidgetCounter;

            string material = Sheet.DefaultSkin + ".button";

            return _createButton(name, dimensions, positionMode, sizeMode, material);
        }

        public CheckBox CreateCheckBox(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            if (!(GUIManager.Singleton.ValidWidgetName(name)))
                return null;

            return _createCheckBox(name, dimensions, positionMode, sizeMode, material);
        }

        public CheckBox CreateCheckBox(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
            ++mAutoNameWidgetCounter;

            return _createCheckBox(name, dimensions, positionMode, sizeMode, material);
        }

        public CheckBox CreateCheckBox(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
        {
            if (!(GUIManager.Singleton.ValidWidgetName(name)))
                return null;

            string material = Sheet.DefaultSkin + ".checkbox.unchecked";

            return _createCheckBox(name, dimensions, positionMode, sizeMode, material);
        }

        public CheckBox CreateCheckBox(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
        {
            string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
            ++mAutoNameWidgetCounter;

            string material = Sheet.DefaultSkin + ".checkbox.unchecked";

            return _createCheckBox(name, dimensions, positionMode, sizeMode, material);
        }

        public ProgressBar CreateProgressBar(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            if (!(GUIManager.Singleton.ValidWidgetName(name)))
                return null;

            return _createProgressBar(name, dimensions, positionMode, sizeMode, material);
        }

        public ProgressBar CreateProgressBar(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
            ++mAutoNameWidgetCounter;

            return _createProgressBar(name, dimensions, positionMode, sizeMode, material);
        }

        public ProgressBar CreateProgressBar(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
        {
            if (!(GUIManager.Singleton.ValidWidgetName(name)))
                return null;

            string material = Sheet.DefaultSkin + ".progressbar";

            return _createProgressBar(name, dimensions, positionMode, sizeMode, material);
        }

        public ProgressBar CreateProgressBar(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
        {
            string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
            ++mAutoNameWidgetCounter;

            string material = Sheet.DefaultSkin + ".progressbar";

            return _createProgressBar(name, dimensions, positionMode, sizeMode, material);
        }

        public HorizontalTrackBar CreateHorizontalTrackBar(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
            ++mAutoNameWidgetCounter;

            return _createHorizontalTrackBar(name, dimensions, positionMode, sizeMode, material);
        }

        public HorizontalTrackBar CreateHorizontalTrackBar(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
        {
            if (!(GUIManager.Singleton.ValidWidgetName(name)))
                return null;

            string material = Sheet.DefaultSkin + ".trackbar.horizontal";

            return _createHorizontalTrackBar(name, dimensions, positionMode, sizeMode, material);
        }

        public HorizontalTrackBar CreateHorizontalTrackBar(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
        {
            string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
            ++mAutoNameWidgetCounter;

            string material = Sheet.DefaultSkin + ".trackbar.horizontal";

            return _createHorizontalTrackBar(name, dimensions, positionMode, sizeMode, material);
        }

        public VerticalTrackBar CreateVerticalTrackBar(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
            ++mAutoNameWidgetCounter;

            return _createVerticalTrackBar(name, dimensions, positionMode, sizeMode, material);
        }

        public VerticalTrackBar CreateVerticalTrackBar(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
        {
            if (!(GUIManager.Singleton.ValidWidgetName(name)))
                return null;

            string material = Sheet.DefaultSkin + ".trackbar.vertical";

            return _createVerticalTrackBar(name, dimensions, positionMode, sizeMode, material);
        }

        public VerticalTrackBar CreateVerticalTrackBar(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
        {
            string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
            ++mAutoNameWidgetCounter;

            string material = Sheet.DefaultSkin + ".trackbar.vertical";

            return _createVerticalTrackBar(name, dimensions, positionMode, sizeMode, material);
        }

        public ComboBox CreateComboBox(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            if (!(GUIManager.Singleton.ValidWidgetName(name)))
                return null;

            return _createComboBox(name, dimensions, positionMode, sizeMode, material);
        }

        public ComboBox CreateComboBox(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material)
        {
            string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
            ++mAutoNameWidgetCounter;

            return _createComboBox(name, dimensions, positionMode, sizeMode, material);
        }

        public ComboBox CreateComboBox(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
        {
            if (!(GUIManager.Singleton.ValidWidgetName(name)))
                return null;

            string material = Sheet.DefaultSkin + ".combobox";

            return _createComboBox(name, dimensions, positionMode, sizeMode, material);
        }

        public ComboBox CreateComboBox(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
        {
            string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
            ++mAutoNameWidgetCounter;

            String material = Sheet.DefaultSkin + ".combobox";

            return _createComboBox(name, dimensions, positionMode, sizeMode, material);
        }

        public Image CreateImage(string name, Vector4 dimensions, string material, bool texture, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    if( !(GUIManager.Singleton.ValidWidgetName(name)) )
                return null;

		    return _createImage(name,dimensions,positionMode,sizeMode,material,texture);
	    }

        public Image CreateImage(Vector4 dimensions, string material, bool texture, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
		    ++mAutoNameWidgetCounter;

		    return _createImage(name,dimensions,positionMode,sizeMode,material,texture);
	    }

        public Image CreateImage(string name, Vector4 dimensions, bool texture, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    if( !(GUIManager.Singleton.ValidWidgetName(name)) )
                return null;

		    string material = Sheet.DefaultSkin + ".image";

		    return _createImage(name,dimensions,positionMode,sizeMode,material,texture);
	    }

        public Image CreateImage(Vector4 dimensions, bool texture, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
		    ++mAutoNameWidgetCounter;

		    string material = Sheet.DefaultSkin + ".image";

		    return _createImage(name,dimensions,positionMode,sizeMode,material,texture);
	    }

        public Label CreateLabel(string name, Vector4 dimensions, string material, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    if( !(GUIManager.Singleton.ValidWidgetName(name)) )
                return null;

		    return _createLabel(name,dimensions,positionMode,sizeMode,material);
	    }


        public Label CreateLabel(Vector4 dimensions, string material, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
		    ++mAutoNameWidgetCounter;

		    return _createLabel(name,dimensions,positionMode,sizeMode,material);
	    }

        public Label CreateLabel(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    if( !(GUIManager.Singleton.ValidWidgetName(name)) )
                return null;

		    string material = Sheet.DefaultSkin + ".label";

		    return _createLabel(name,dimensions,positionMode,sizeMode,material);
	    }

        public Label CreateLabel(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
		    ++mAutoNameWidgetCounter;

		    string material = Sheet.DefaultSkin + ".label";

		    return _createLabel(name,dimensions,positionMode,sizeMode,material);
	    }

        public List CreateList(string name, Vector3 dimensions, Label itemTemplate, string material, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    if( !(GUIManager.Singleton.ValidWidgetName(name)) )
                return null;

		    return _createList(name,dimensions,itemTemplate,positionMode,sizeMode,material);
	    }


        public List CreateList(Vector3 dimensions, Label itemTemplate, string material, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
		    ++mAutoNameWidgetCounter;

		    return _createList(name,dimensions,itemTemplate,positionMode,sizeMode,material);
	    }

        public List CreateList(string name, Vector3 dimensions, Label itemTemplate, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    if( !(GUIManager.Singleton.ValidWidgetName(name)) )
                return null;

		    string material = Sheet.DefaultSkin + ".list";

		    return _createList(name,dimensions,itemTemplate,positionMode,sizeMode,material);
	    }

        public List CreateList(Vector3 dimensions, Label itemTemplate, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
		    ++mAutoNameWidgetCounter;

		    string material = Sheet.DefaultSkin + ".list";

		    return _createList(name,dimensions,itemTemplate,positionMode,sizeMode,material);
	    }

        public Menu CreateMenu(string name, Vector4 dimensions, string material, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    if( !(GUIManager.Singleton.ValidWidgetName(name)) )
                return null;

		    return _createMenu(name,dimensions,positionMode,sizeMode,material);
	    }

        public Menu CreateMenu(Vector4 dimensions, string material, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
		    ++mAutoNameWidgetCounter;

		    return _createMenu(name,dimensions,positionMode,sizeMode,material);
	    }

        public Menu CreateMenu(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    if( !(GUIManager.Singleton.ValidWidgetName(name)) )
                return null;

		    string material = Sheet.DefaultSkin + ".menu";

		    return _createMenu(name,dimensions,positionMode,sizeMode,material);
	    }

        public Menu CreateMenu(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
		    ++mAutoNameWidgetCounter;

		    string material = Sheet.DefaultSkin + ".menu";

		    return _createMenu(name,dimensions,positionMode,sizeMode,material);
	    }

        public NStateButton CreateNStateButton(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    if( !(GUIManager.Singleton.ValidWidgetName(name)) )
                return null;

		    return _createNStateButton(name,dimensions,positionMode,sizeMode);
	    }

        public NStateButton CreateNStateButton(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
		    ++mAutoNameWidgetCounter;

		    return _createNStateButton(name,dimensions,positionMode,sizeMode);
	    }

	        
		public Panel CreatePanel(string name, Vector4 dimensions, string material, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
		{
		    if( !(GUIManager.Singleton.ValidWidgetName(name)) )
                return null;
	
			return _createPanel(name,dimensions,positionMode,sizeMode,material);
		}
	
		public Panel CreatePanel(Vector4 dimensions, string material, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
		{
			string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
			++mAutoNameWidgetCounter;
	
			return _createPanel(name,dimensions,positionMode,sizeMode,material);
		}
	
		public Panel CreatePanel(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
		{
		    if( !(GUIManager.Singleton.ValidWidgetName(name)) )
                return null;
	
		    string material = Sheet.DefaultSkin + ".panel";
	
			return _createPanel(name,dimensions,positionMode,sizeMode,material);
		}
	
		public Panel CreatePanel(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
		{
			string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
			++mAutoNameWidgetCounter;
	
		    string material = Sheet.DefaultSkin + ".panel";
	
			return _createPanel(name,dimensions,positionMode,sizeMode,material);
		}

        
	    public TextBox CreateTextBox(string name, Vector4 dimensions, string material, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    if( !(GUIManager.Singleton.ValidWidgetName(name)) )
                return null;

		    return _createTextBox(name,dimensions,positionMode,sizeMode,material);
	    }

        public TextBox CreateTextBox(Vector4 dimensions, string material, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
		    ++mAutoNameWidgetCounter;

		    return _createTextBox(name,dimensions,positionMode,sizeMode,material);
	    }

        public TextBox CreateTextBox(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    if( !(GUIManager.Singleton.ValidWidgetName(name)) )
                return null;

		    string material = Sheet.DefaultSkin + ".textbox";

		    return _createTextBox(name,dimensions,positionMode,sizeMode,material);
	    }

        public TextBox CreateTextBox(Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode)
	    {
		    string name = mInstanceName + ".ChildWidget" + mAutoNameWidgetCounter;
		    ++mAutoNameWidgetCounter;

		    string material = Sheet.DefaultSkin + ".textbox";

		    return _createTextBox(name,dimensions,positionMode,sizeMode,material);
	    }

	    public void DestroyButton(int index)
	    {
		    if( index >= mNumButtons )
                return;

		    if(_destroyWidget(WidgetType.QGUI_TYPE_BUTTON, index))
			    --mNumButtons;
	    }

        public void DestroyButton(string name)
	    {
		    if(_destroyWidget(name))
			    --mNumButtons;
	    }

        public void DestroyButton(Button b)
	    {
		    if(_destroyWidget(b.InstanceName))
			    --mNumButtons;
	    }

        public void DestroyCheckBox(int index)
        {
            if (index >= mNumCheckBoxes)
                return;

            if (_destroyWidget(WidgetType.QGUI_TYPE_CHECKBOX, index))
                --mNumCheckBoxes;
        }

        public void DestroyCheckBox(string name)
        {
            if (_destroyWidget(name))
                --mNumCheckBoxes;
        }

        public void DestroyCheckBox(CheckBox cb)
        {
            if (_destroyWidget(cb.InstanceName))
                --mNumCheckBoxes;
        }

        public void DestroyProgressBar(int index)
        {
            if (index >= mNumProgressBars)
                return;

            if (_destroyWidget(WidgetType.QGUI_TYPE_PROGRESSBAR, index))
                --mNumProgressBars;
        }

        public void DestroyProgressBar(string name)
        {
            if (_destroyWidget(name))
                --mNumProgressBars;
        }

        public void DestroyProgressBar(ProgressBar pb)
        {
            if (_destroyWidget(pb.InstanceName))
                --mNumProgressBars;
        }

        public void DestroyHorizontalTrackBar(int index)
        {
            if (index >= mNumHorizontalTrackBars)
                return;

            if (_destroyWidget(WidgetType.QGUI_TYPE_HORIZONTAL_TRACKBAR, index))
                --mNumHorizontalTrackBars;
        }

        public void DestroyHorizontalTrackBar(string name)
        {
            if (_destroyWidget(name))
                --mNumHorizontalTrackBars;
        }

        public void DestroyHorizontalTrackBar(HorizontalTrackBar htb)
        {
            if (_destroyWidget(htb.InstanceName))
                --mNumHorizontalTrackBars;
        }

        public void DestroyVerticalTrackBar(int index)
        {
            if (index >= mNumVerticalTrackBars)
                return;

            if (_destroyWidget(WidgetType.QGUI_TYPE_VERTICAL_TRACKBAR, index))
                --mNumVerticalTrackBars;
        }

        public void DestroyVerticalTrackBar(string name)
        {
            if (_destroyWidget(name))
                --mNumVerticalTrackBars;
        }

        public void DestroyVerticalTrackBar(VerticalTrackBar vtb)
        {
            if (_destroyWidget(vtb.InstanceName))
                --mNumVerticalTrackBars;
        }

        public void DestroyComboBox(int index)
	    {
		    if( index >= mNumComboBoxes ) return;

		    if(_destroyWidget(WidgetType.QGUI_TYPE_COMBOBOX, index))
			    --mNumComboBoxes;
	    }

        public void DestroyComboBox(string name)
	    {
		    if(_destroyWidget(name))
			    --mNumComboBoxes;
	    }

        public void DestroyComboBox(ComboBox b)
	    {
		    if(_destroyWidget(b.InstanceName))
			    --mNumComboBoxes;;
	    }

        public void DestroyImage(int index)
	    {
		    if( index >= mNumImages )
                return;

		    if(_destroyWidget(WidgetType.QGUI_TYPE_IMAGE,index))
			    --mNumImages;
	    }

        public void DestroyImage(string name)
	    {
		    if(_destroyWidget(name))
			    --mNumImages;
	    }

        public void DestroyImage(Image i)
	    {
		    if(_destroyWidget(i.InstanceName))
			    --mNumImages;
	    }

        public void DestroyLabel(int index)
	    {
		    if( index >= mNumLabels )
                return;

		    if(_destroyWidget(WidgetType.QGUI_TYPE_LABEL,index))
			    --mNumLabels;
	    }

        public void DestroyLabel(string name)
	    {
		    if(_destroyWidget(name))
			    --mNumLabels;
	    }

        public void DestroyLabel(Label l)
	    {
		    if(_destroyWidget(l.InstanceName))
			    --mNumLabels;
	    }

        public void DestroyTextBox(int index)
	    {
		    if( index >= mNumTextBoxes )
                return;

		    if(_destroyWidget(WidgetType.QGUI_TYPE_TEXTBOX,index))
			    --mNumTextBoxes;
	    }

        public void DestroyTextBox(string name)
	    {
		    if(_destroyWidget(name))
			    --mNumTextBoxes;
	    }

        public void DestroyTextBox(TextBox b)
	    {
		    if(_destroyWidget(b.InstanceName))
			    --mNumTextBoxes;
	    }

        public Button GetButton(int index)
	    {
		    if( index >= mNumButtons )
                return null;

		    Widget w = _getWidget(WidgetType.QGUI_TYPE_BUTTON, index);
		    if( w != null)
                return (Button)w;
            return null;
	    }

        public Button GetButton(string name)
	    {
		    Widget w = _getWidget(name);
		    if( w != null )
                return (Button)w;
		    return null;
	    }

        public CheckBox GetCheckBox(int index)
        {
            if (index >= mNumCheckBoxes)
                return null;

            Widget w = _getWidget(WidgetType.QGUI_TYPE_CHECKBOX, index);
            if (w != null)
                return (CheckBox)w;
            return null;
        }

        public CheckBox GetCheckBox(string name)
        {
            Widget w = _getWidget(name);
            if (w != null)
                return (CheckBox)w;
            return null;
        }

        public ProgressBar GetProgressBar(int index)
        {
            if (index >= mNumCheckBoxes)
                return null;

            Widget w = _getWidget(WidgetType.QGUI_TYPE_PROGRESSBAR, index);
            if (w != null)
                return (ProgressBar)w;
            return null;
        }

        public ProgressBar GetProgressBar(string name)
        {
            Widget w = _getWidget(name);
            if (w != null)
                return (ProgressBar)w;
            return null;
        }

        public HorizontalTrackBar GetHorizontalTrackBar(int index)
        {
            if (index >= mNumHorizontalTrackBars)
                return null;

            Widget w = _getWidget(WidgetType.QGUI_TYPE_HORIZONTAL_TRACKBAR, index);
            if (w != null)
                return (HorizontalTrackBar)w;
            return null;
        }

        public HorizontalTrackBar GetHorizontalTrackBar(string name)
        {
            Widget w = _getWidget(name);
            if (w != null)
                return (HorizontalTrackBar)w;
            return null;
        }

        public VerticalTrackBar GetVerticalTrackBar(int index)
        {
            if (index >= mNumVerticalTrackBars)
                return null;

            Widget w = _getWidget(WidgetType.QGUI_TYPE_VERTICAL_TRACKBAR, index);
            if (w != null)
                return (VerticalTrackBar)w;
            return null;
        }

        public VerticalTrackBar GetVerticalTrackBar(string name)
        {
            Widget w = _getWidget(name);
            if (w != null)
                return (VerticalTrackBar)w;
            return null;
        }

        public ComboBox GetComboBox(int index)
	    {
		    if( index >= mNumComboBoxes )
                return null;

		    Widget w = _getWidget(WidgetType.QGUI_TYPE_COMBOBOX, index);
		    if( w != null)
                return (ComboBox)w;
            return null;
	    }

        public ComboBox GetComboBox(string name)
	    {
		    Widget w = _getWidget(name);
		    if( w != null )
                return (ComboBox)w;
		    return null;
	    }

        public Image GetImage(int index)
	    {
		    if( index >= mNumImages )
                return null;

		    Widget w = _getWidget(WidgetType.QGUI_TYPE_IMAGE,index);
		    if( w != null )
                return (Image)w;
		    else
                return null;
	    }

        public Image GetImage(string name)
	    {
		    Widget w = _getWidget(name);
		    if( w != null )
                return (Image)w;
		    else
                return null;
	    }

        public Label GetLabel(int index)
	    {
		    if( index >= mNumLabels )
                return null;

		    Widget w = _getWidget(WidgetType.QGUI_TYPE_LABEL,index);
		    if( w != null )
                return (Label)w;
		    else
                return null;
	    }

        public Label GetLabel(string name)
	    {
		    Widget w = _getWidget(name);
		    if( w != null )
                return (Label)w;
		    else
                return null;
	    }

	   public TextBox GetTextBox(int index)
	    {
		    if( index >= mNumTextBoxes )
                return null;

		    Widget w = _getWidget(WidgetType.QGUI_TYPE_TEXTBOX,index);
		    if( w != null )
                return (TextBox)w;
		    else
                return null;
	    }

	    public TextBox GetTextBox(string name)
	    {
		    Widget w = _getWidget(name);
		    if( w != null )
                return (TextBox)w;
		    else
                return null;
	    }

        internal int getMaxZOrderValue()
        {
            return mZOrderValues[mZOrderValues.Count - 1];
        }

    }
}
