using Mogre;

namespace MQuickGUI
{
    public class Label : Widget
    {
		// Default Label material, displayed in its original state.
        protected string    mMaterial;
        protected TextWidget      mTextWidget;

        public Label(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material, OverlayContainer overlayContainer, Widget ParentWidget)
            : base(name, dimensions, positionMode, sizeMode, material, overlayContainer, ParentWidget)
	    {
		    mWidgetType = WidgetType.QGUI_TYPE_LABEL;

		    mOverlayElement = CreatePanelOverlayElement(mInstanceName+".Background",mPixelDimensions,"");
		    mOverlayContainer.AddChild(mOverlayElement);
		    mOverlayElement.Show();
            Material = mWidgetMaterial;

		    mCharacterHeight = 0.8f;
		    mTextWidget = new TextWidget(mInstanceName+".Text", new Vector3(0,0,mCharacterHeight), QGuiMetricsMode.QGUI_GMM_RELATIVE, QGuiMetricsMode.QGUI_GMM_RELATIVE, mChildrenContainer, this);
		    mTextWidget.SetZOrderOffset(1,false);
		    _addChildWidget(mTextWidget);

		    mHorizontalAlignment = GuiHorizontalAlignment.GHA_CENTER;
		    mVerticalAlignment = GuiVerticalAlignment.GVA_CENTER;

            OnActivate += new ActivateEventHandler(Label_OnActivate);
	    }

        void Label_OnActivate(object source, EventArgs e)
        {
            if (!mEnabled)
                return;

            mTextWidget.Activate(e);
        }

        protected override void _notifyTextChanged()
	    {
		    mTextWidget.Font = mFont;
		    mTextWidget.CharacterHeight = mCharacterHeight;
		    mTextWidget.SetTextColor(mTextTopColor,mTextBotColor);
		    mTextWidget.Text = mText;

		    AlignText(mHorizontalAlignment,mVerticalAlignment);
	    }

        public void AlignText(GuiHorizontalAlignment ha, GuiVerticalAlignment va)
	    {
		    mHorizontalAlignment = ha;
		    mVerticalAlignment = va;

            Vector2 relativeLabelPos = mTextWidget.GetPosition(QGuiMetricsMode.QGUI_GMM_RELATIVE);
            Vector2 labelPos = mTextWidget.GetPosition(QGuiMetricsMode.QGUI_GMM_ABSOLUTE);
            Vector2 labelSize = mTextWidget.GetSize(QGuiMetricsMode.QGUI_GMM_ABSOLUTE);

		    if( mHorizontalAlignment == GuiHorizontalAlignment.GHA_LEFT) 
		    {
			    // We should add a 5 pixel buffer
			    float buffer = 5.0f / mPixelDimensions.z;
			    mTextWidget.SetPosition(buffer, relativeLabelPos.y);
		    }
		    else if( mHorizontalAlignment == GuiHorizontalAlignment.GHA_CENTER )
		    {
			    mTextWidget.SetPosition(((mAbsoluteDimensions.z / 2) - (labelSize.x / 2)) / mAbsoluteDimensions.z, relativeLabelPos.y);
		    }
		    else if( mHorizontalAlignment == GuiHorizontalAlignment.GHA_RIGHT )
		    {
			    // We should add a 5 pixel buffer
			    float buffer = 5.0f / mPixelDimensions.z;
			    mTextWidget.SetPosition(((mAbsoluteDimensions.z) - (labelSize.x) - buffer) / mAbsoluteDimensions.z, relativeLabelPos.y);
		    }

		    // Make sure to update the position, in case alignment has moved it
            relativeLabelPos = mTextWidget.GetPosition(QGuiMetricsMode.QGUI_GMM_RELATIVE);

		    if( mVerticalAlignment == GuiVerticalAlignment.GVA_TOP )
                mTextWidget.SetPosition(relativeLabelPos.x,0.0f);
		    else if( mVerticalAlignment == GuiVerticalAlignment.GVA_CENTER )
		    {
                mTextWidget.SetPosition(relativeLabelPos.x,((mAbsoluteDimensions.w / 2) - (labelSize.y / 2)) / mAbsoluteDimensions.w);
		    }
		    else if( mVerticalAlignment == GuiVerticalAlignment.GVA_BOTTOM )
		    {
			    mTextWidget.SetPosition(relativeLabelPos.x,((mAbsoluteDimensions.w) - (labelSize.y)) / mAbsoluteDimensions.w);
		    }
	    }

        public TextWidget TextWidget
        {
            get { return mTextWidget; }
        }
    }
}
