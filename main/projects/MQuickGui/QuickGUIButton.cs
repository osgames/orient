using Mogre;

namespace MQuickGUI
{
    public class Button : Label
    {
        protected bool mOverMaterialExists;
        protected bool mDownMaterialExists;

        public Button(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material, OverlayContainer overlayContainer, Widget ParentWidget) : base(name, dimensions, positionMode, sizeMode, material, overlayContainer, ParentWidget) {
		    mWidgetType = WidgetType.QGUI_TYPE_BUTTON;
		    CharacterHeight = 0.5f;

		    MaterialManager mm = MaterialManager.Singleton;

		    mOverMaterialExists = mm.ResourceExists(mWidgetMaterial+".over");
		    mDownMaterialExists = mm.ResourceExists(mWidgetMaterial+".down");

            OnMouseEnter += new MouseEnterEventHandler(Button_OnMouseEnter);
            OnMouseButtonDown += new MouseButtonDownEventHandler(Button_OnMouseButtonDown);
            OnMouseButtonUp += new MouseButtonUpEventHandler(Button_OnMouseButtonUp);
            OnMouseLeaves += new MouseLeavesEventHandler(Button_OnMouseLeaves);
        }

        public virtual void ApplyButtonDownMaterial()
	    {
		    if(mDownMaterialExists) 
		    {
			    // apply button ".down" material
		        mOverlayElement.MaterialName = mWidgetMaterial+".down";
		    }
	    }

	    public virtual void ApplyDefaultMaterial()
	    {
		    mOverlayElement.MaterialName = mWidgetMaterial;
	    }

        protected void Button_OnMouseButtonDown(object source, MouseEventArgs e)
        {
            if (!mEnabled)
                return;

            ApplyButtonDownMaterial();
        }

        protected void Button_OnMouseButtonUp(object source, MouseEventArgs e)
        {
            if (!mEnabled)
                return;
            if (mOverMaterialExists)
                mOverlayElement.MaterialName = mWidgetMaterial + ".over";
        }

        protected void Button_OnMouseEnter(object source, MouseEventArgs e)
        {
            if (!mEnabled)
                return;
            if (mGrabbed)
                ApplyButtonDownMaterial();
            else
                if (mOverMaterialExists)
                    mOverlayElement.MaterialName = mWidgetMaterial + ".over";
        }

        protected void Button_OnMouseLeaves(object source, MouseEventArgs e)
        {
       		if(!mEnabled)
                return;
		    mOverlayElement.MaterialName = mWidgetMaterial;
        }
    }
}
