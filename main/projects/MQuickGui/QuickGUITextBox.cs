using System.Text;
using Mogre;
using MOIS;
using Vector3=Mogre.Vector3;

namespace MQuickGUI
{
	/** Represents a TextBox.
		@remarks
		TextBoxes allow the user to input data on the screen,
		which can be used for other purposes.  The TextBox class 
		requires at least 2 materials to define it's image:
		Background Image, Border.  For example, if you pass
		the constructor "sample.textbox" as its arguement for the 
		material, the class will expect "sample.textbox.border"
		to exist.
		@note
		TextBoxes must be created by the Window widget.
	*/
    public class TextBox : Widget
    {
		protected bool		mMaskUserInput;
		protected string    mMaskSymbol;

		protected float     mBackSpaceTimer;
		protected bool		mBackSpaceDown;
		protected float 	mDeleteTimer;
		protected bool		mDeleteDown;
		protected float     mMoveCursorTimer;
		protected bool		mLeftArrowDown;
		protected bool		mRightArrowDown;
		protected float     mCursorVisibilityTimer;
		protected bool      mReadOnly;

		protected TextWidget		mTextWidget;
		protected bool		mInputMode;

        /** Constructor
            @param
                name The name to be given to the widget (must be unique).
            @param
                dimensions The x Position, y Position, width, and height of the widget.
			@param
				positionMode The GuiMetricsMode for the values given for the position. (absolute/relative/pixel)
			@param
				sizeMode The GuiMetricsMode for the values given for the size. (absolute/relative/pixel)
			@param
				material Ogre material defining the widget image.
			@param
				overlayContainer associates the internal OverlayElement with a specified zOrder.
			@param
				ParentWidget parent widget which created this widget.
        */
        public TextBox(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material, OverlayContainer overlayContainer, Widget ParentWidget)
            : base(name, dimensions, positionMode, sizeMode, material, overlayContainer, ParentWidget)
	    {
		    mWidgetType = WidgetType.QGUI_TYPE_TEXTBOX;

       		mMaskUserInput = false;
		    mBackSpaceDown = false;
		    mBackSpaceTimer = 0.0f;
		    mDeleteDown = false;
		    mDeleteTimer = 0.0f;
		    mLeftArrowDown = false;
		    mRightArrowDown = false;
		    mMoveCursorTimer = 0.0f;
		    mCursorVisibilityTimer = 0.0f;
		    mReadOnly = false;
		    mInputMode = false;
                

		    // Border Overlay gives us ability to assign material to TextBox border and Panel separately.
		    mOverlayElement = CreatePanelOverlayElement(mInstanceName+".Background",mPixelDimensions,"");
		    mOverlayContainer.AddChild(mOverlayElement);
		    mOverlayElement.Show();
            Material = mWidgetMaterial;

            mCharacterHeight = 0.75f;
            Vector3 textDimensions = new Vector3(0, 0, mCharacterHeight);
		    // Label has no material, since it directly overlaps the textbox overlay element
		    mTextWidget = new TextWidget(mInstanceName+".Text",textDimensions,QGuiMetricsMode.QGUI_GMM_RELATIVE,QGuiMetricsMode.QGUI_GMM_RELATIVE,mChildrenContainer,this);
		    mTextWidget.SetTruncateMode(TextWidget.TruncateMode.LEFT);
		    mTextWidget.SetTruncationFeedback("");
		    mTextWidget.SetZOrderOffset(1);
		    _addChildWidget(mTextWidget);

		    mHorizontalAlignment = GuiHorizontalAlignment.GHA_LEFT;
		    mVerticalAlignment = GuiVerticalAlignment.GVA_CENTER;

		    AlignText(mHorizontalAlignment,mVerticalAlignment);

            OnDeactivate += new DeactivateEventHandler(TextBox_OnDeactivate);
            OnCharacter += new CharacterEventHandler(TextBox_OnCharacter);
            OnKeyDown += new KeyDownEventHandler(TextBox_OnKeyDown);
            OnKeyUp +=new KeyUpEventHandler(TextBox_OnKeyUp);
            OnMouseClicked += TextBox_OnMouseClicked;
            this.Sheet.OnTimeElapsed +=new TimeElapsedHandler(TextBox_OnTimeElapsed);
            OnMouseEnter += new MouseEnterEventHandler(TextBox_OnMouseEnter);
            OnMouseLeaves += new MouseLeavesEventHandler(TextBox_OnMouseLeaves);
	    }

		/**
		* When user has changed the font, character height, or text,
		* the label must be updated and aligned according to its parent.
		*/
		protected override void _notifyTextChanged()
        {
		    mTextWidget.Font = mFont;
		    mTextWidget.CharacterHeight = mCharacterHeight;
		    mTextWidget.SetTextColor(mTextTopColor,mTextBotColor);
		    mTextWidget.Text = mTextWidget.Text;

		    AlignText(mHorizontalAlignment,mVerticalAlignment);
        }

		/**
		* Adds a character to the textBox right before text cursor.
		*/
		void AddCharacter(string c)
        {
		    if(mReadOnly)
                return;

		    // Insert a cter right before the text cursor.
		    int index = mTextWidget.CursorIndex;

            mText = mText.Insert(index, c);

		    // Handle adding character to password box.
		    string s = string.Empty;
            if (mMaskUserInput)
            {
		    	StringBuilder sb = new StringBuilder();
		    	sb.Append(mMaskSymbol.ToCharArray()[0], mText.Length);
		    	s = sb.ToString();
            }
            else
            {
                s = mText;
            }

		    mTextWidget.Text = s;
		    _notifyTextChanged();
		    mTextWidget.IncrementCursorIndex();
        }

		/**
		* Add user defined event that will be called when user presses Enter key with Textbox Activated.
		*/
        public delegate void EnterPressedEventHandler(object source, KeyEventArgs e);
        public event EnterPressedEventHandler OnEnterPressed;

        /**
		* Aligns the child Label widget horizontally and vertically.
		*/
		void AlignText(GuiHorizontalAlignment ha, GuiVerticalAlignment va)
        {
		    mHorizontalAlignment = ha;
		    mVerticalAlignment = va;

            Vector2 relativeLabelPos = mTextWidget.GetPosition(QGuiMetricsMode.QGUI_GMM_RELATIVE);
		    Vector2 labelPos = mTextWidget.GetPosition(QGuiMetricsMode.QGUI_GMM_ABSOLUTE);
		    Vector2 labelSize = mTextWidget.GetSize(QGuiMetricsMode.QGUI_GMM_ABSOLUTE);

		    if( mHorizontalAlignment == GuiHorizontalAlignment.GHA_LEFT ) 
		    {
			    float widthBuffer = 5.0f / mPixelDimensions.z;
			    mTextWidget.SetPosition(widthBuffer,relativeLabelPos.y);
		    }
		    if( mHorizontalAlignment == GuiHorizontalAlignment.GHA_CENTER )
		    {
			    mTextWidget.SetPosition(((mAbsoluteDimensions.z / 2) - (labelSize.x / 2)) / mAbsoluteDimensions.z,relativeLabelPos.y);
		    }
		    if( mHorizontalAlignment == GuiHorizontalAlignment.GHA_RIGHT )
		    {
			    float widthBuffer = 5.0f / mPixelDimensions.z;
			    mTextWidget.SetPosition(((mAbsoluteDimensions.z) - (labelSize.x) - widthBuffer) / mAbsoluteDimensions.z,relativeLabelPos.y);
		    }

		    // Make sure to update the position, in case alignment has moved it
		    relativeLabelPos = mTextWidget.GetPosition(QGuiMetricsMode.QGUI_GMM_RELATIVE);

		    if( mVerticalAlignment == GuiVerticalAlignment.GVA_TOP ) 
		    {
			    float heightBuffer = 3.0f / mPixelDimensions.w;
			    mTextWidget.SetPosition(relativeLabelPos.x,heightBuffer);
		    }
		    if( mVerticalAlignment == GuiVerticalAlignment.GVA_CENTER )
		    {
			    mTextWidget.SetPosition(relativeLabelPos.x,((mAbsoluteDimensions.w / 2) - (labelSize.y / 2)) / mAbsoluteDimensions.w);
		    }
		    if( mVerticalAlignment == GuiVerticalAlignment.GVA_BOTTOM )
		    {
			    float heightBuffer = 3.0f / mPixelDimensions.w;
			    mTextWidget.SetPosition(relativeLabelPos.x,((mAbsoluteDimensions.w) - (labelSize.y) - heightBuffer) / mAbsoluteDimensions.w);
		    }

        }
		/**
		* Method to erase the character right before the text cursor.
		*/
		void BackSpace()
        {
		    if( string.Empty.Equals(mText) )
                return;

		    // remove character from cursor position
		    int index = mTextWidget.CursorIndex;
		    if( index == 0 )
                return;
            mText = mText.Remove(index - 1, 1);

		    // Handle removing character to password box.
		    string s = string.Empty;
		    if(mMaskUserInput) {
		    	StringBuilder sb = new StringBuilder();
		    	sb.Append(mMaskSymbol.ToCharArray()[0], mText.Length);
		    	s = sb.ToString();
		    }
		    else {
                s = mText;
		    }

		    mTextWidget.Text = s;
		    _notifyTextChanged();
		    mTextWidget.DecrementCursorIndex();
        }

		/**
		* Method to erase the character right after the text cursor.
		*/
		void DeleteCharacter()
        {
		    if( string.Empty.Equals(mText) )
                return;

		    // remove character in front of cursor position
		    int index = mTextWidget.CursorIndex;
		    if ( index >= mText.Length ){
		    	return ;
		    }
            mText = mText.Remove(index, 1);

		    // Handle removing character to password box.
            string s = string.Empty;
            if (mMaskUserInput) {
		    	StringBuilder sb = new StringBuilder();
		    	sb.Append(mMaskSymbol.ToCharArray()[0], mText.Length);
		    	s = sb.ToString();
            }
            else {
                s = mText;
            }

		    mTextWidget.Text = s;
		    _notifyTextChanged();
        }

		/**
		* Sets focus to the widget, displaying the text cursor.
		*/
		public override void Focus()
        {
		    if(!mEnabled)
                return;

		    mTextWidget.SetTextCursorPosition(Vector2.ZERO);
		    mInputMode = true;

		    mGUIManager.SetActiveWidget(this);
       }

		/**
		* Gets a handle to the Label widget used for this Widget.
		*/

        public TextWidget TextWidget
        {
            get { return mTextWidget; }
        }

        public bool ReadOnly
        {
            get { return mReadOnly; }
            set { mReadOnly = value; }
        }

        /**
		* Hides the actual user input and writes the designated character
		* in its place.  Great for visible password protection.
		*/
		public void MaskUserInput(string symbol)
        {
		    mMaskSymbol = symbol;

		    // if there was previously text, we now need to mask it.
		    if( !string.Empty.Equals(mText) && !mMaskUserInput )
                Text = mText;

		    mMaskUserInput = true;
        }

		/**
		* If set to true, cannot input text to textbox
		*/

        public override string Text
        {
            set
            {
                string t = value;
                if (mMaskUserInput)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(mMaskSymbol.ToCharArray()[0], value.Length);
                    t = sb.ToString();
                }
                mTextWidget.Text = t;
                // TODO: Revisar que esto llame realmente al m�todo de Widget
                base.Text = value;
            }
            get { return base.Text; }
        }

        #region Event Handlers
        
        // Overridden Event Handling functions
		/**
		* Handler for the QGUI_EVENT_DEACTIVATED event, and deactivates all child widgets (if exist)
		*/
        void  TextBox_OnDeactivate(object source, EventArgs e)
        {
		    if(!mEnabled)
                return;

		    mTextWidget.HideTextCursor();
		    mInputMode = false;
		    mBackSpaceDown = false;
		    mLeftArrowDown = false;
		    mRightArrowDown = false;
        }

		/**
		* User defined event handler called when user presses Enter.
		* Note that this event is not passed to its parents, the event is specific to this widget.
		*/
        public void EnterPressed(KeyEventArgs e)
        {
            if (!mEnabled)
                return;

            this.mTextWidget.ToggleTextCursorVisibility();
            this.mInputMode = false;

            if (OnEnterPressed != null)
                OnEnterPressed(this, e);
        }

		/**
		* Handler for the QGUI_EVENT_KEY_DOWN event.  If not handled, it will be passed
		* to the parent widget (if exists)
		*/
        void TextBox_OnKeyDown(object source, KeyEventArgs e)
        {
		    if(!mEnabled)
                return;

		    if( (e.scancode == KeyCode.KC_BACK) && !mReadOnly ) 
		    {
			    mBackSpaceDown = true;
			    mLeftArrowDown = false;
			    mRightArrowDown = false;
			    mBackSpaceTimer = 0.0f;
			    BackSpace();
		    }
		    else if( e.scancode == KeyCode.KC_LEFT )
		    {
			    mMoveCursorTimer = 0.0f;
			    mLeftArrowDown = true;
			    mRightArrowDown = false;
			    mTextWidget.DecrementCursorIndex();
		    }
		    else if( e.scancode == KeyCode.KC_RIGHT )
		    {
			    mMoveCursorTimer = 0.0f;
			    mRightArrowDown = true;
			    mLeftArrowDown = false;
			    mTextWidget.IncrementCursorIndex();
		    }
		    else if( e.scancode == KeyCode.KC_DELETE )
		    {
			    mDeleteTimer = 0.0f;
			    mDeleteDown = true;
			    mBackSpaceDown = false;
			    mLeftArrowDown = false;
			    mRightArrowDown = false;
			    DeleteCharacter();
            }
            else if ((e.scancode == KeyCode.KC_HOME) || (e.scancode == KeyCode.KC_PGUP))
            {
                this.mTextWidget.PlaceCursorBeginning();
            }
            else if ((e.scancode == KeyCode.KC_END) || (e.scancode == KeyCode.KC_PGDOWN))
            {
                this.mTextWidget.PlaceCursorEnd();
            }
        }

		/**
		* Handler for the QGUI_EVENT_KEY_UP event.  If not handled, it will be passed
		* to the parent widget (if exists)
		*/
        void TextBox_OnKeyUp(object source, KeyEventArgs e)
        {
		    if(!mEnabled)
                return;

		    if( (e.scancode == KeyCode.KC_BACK) && !mReadOnly )
			    mBackSpaceDown = false;
		    else if( e.scancode == KeyCode.KC_LEFT )
                mLeftArrowDown = false;
		    else if( e.scancode == KeyCode.KC_RIGHT )
                mRightArrowDown = false;
		    else if( e.scancode == KeyCode.KC_DELETE )
                mDeleteDown = false;
		    else if( e.scancode == KeyCode.KC_RETURN )
                EnterPressed(e);
        }

		/**
		* Handler for the QGUI_EVENT_CHARACTER_KEY event.  Appends character to the end of the Label's text.
		* If not handled, it will be passed to the parent widget (if exists)
		*/
        void TextBox_OnCharacter(object source, KeyEventArgs e)
        {
		    if(!mInputMode || !mEnabled)
                return;

		    if(!mReadOnly) 
		    {
			    mBackSpaceDown = false;
			    mLeftArrowDown = false;
			    mRightArrowDown = false;

			    AddCharacter(e.codepoint.ToString());
		    }
        }

        void TextBox_OnMouseLeaves(object source, MouseEventArgs e)
        {
            GUIManager.Singleton.MouseCursor.Material = this.Sheet.DefaultSkin + ".cursor";
        }

        void TextBox_OnMouseEnter(object source, MouseEventArgs e)
        {
            GUIManager.Singleton.MouseCursor.Material = this.Sheet.DefaultSkin + ".cursor.text";
        }

		/**
		* Default Handler for the QGUI_EVENT_MOUSE_BUTTON_DOWN event.  If not handled, it will be passed
		* to the parent widget (if exists)
		*/
        void TextBox_OnMouseClicked(object source, MouseEventArgs e)
        {
		    if(this.mInputMode || !mEnabled) return;

		    if(e.button == MouseButtonID.MB_Left) 
		    {
                mTextWidget.ToggleTextCursorVisibility();
			    mTextWidget.SetTextCursorPosition(e.position);
			    mInputMode = true;
		    }
        }

		/**
		* Default Handler for injecting Time.
		*/
        void TextBox_OnTimeElapsed(object source, TimeEventArgs e)
        {

            if (!mEnabled)
                return;

            mBackSpaceTimer += e.time;
            mCursorVisibilityTimer += e.time;
            mMoveCursorTimer += e.time;
            mDeleteTimer += e.time;

            // Hard coding the time to allow repetitive operations to be every .5 seconds
            if (mBackSpaceTimer > 0.5)
            {
                if (mBackSpaceDown && !mReadOnly)
                    BackSpace();
                mBackSpaceTimer = 0.0f;
            }

            if (mCursorVisibilityTimer > 0.5)
            {
                if (mInputMode && !mReadOnly && !(mLeftArrowDown || mRightArrowDown))
                    mTextWidget.ToggleTextCursorVisibility();
                mCursorVisibilityTimer = 0.0f;
            }

            if (mMoveCursorTimer > 0.5)
            {
                if (mLeftArrowDown)
                    mTextWidget.DecrementCursorIndex();
                else if (mRightArrowDown)
                    mTextWidget.IncrementCursorIndex();
                mMoveCursorTimer = 0.0f;
            }

            if (mDeleteTimer > 0.5)
            {
                if (mDeleteDown && !mReadOnly)
                    DeleteCharacter();
                mDeleteTimer = 0.0f;
            }

        }

        #endregion

    }
}
