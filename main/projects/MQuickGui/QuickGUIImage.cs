using Mogre;

namespace MQuickGUI
{
    public class Image : Widget
    {
        protected MaterialPtr mMaterialPtr;

        public Image(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material, OverlayContainer overlayContainer, Widget ParentWidget) : base(name,dimensions,positionMode, sizeMode, material,overlayContainer,ParentWidget)
	    {
		    mWidgetType = WidgetType.QGUI_TYPE_IMAGE;
		    mMaterialPtr = null;

		    mOverlayElement = CreatePanelOverlayElement(mInstanceName+"_Background",mPixelDimensions,"");
		    mOverlayContainer.AddChild(mOverlayElement);
		    mOverlayElement.Show();
            Material = mWidgetMaterial;
	    }

	    ~Image()
	    {
		    // If a material was created (for rendertexture use), destroy and unload it
            if (mMaterialPtr != null)
		    {
			    string name = mMaterialPtr.Name;
			    MaterialManager.Singleton.Remove(name);
		    }
	    }

        public void SetMaterial(RenderTexture texture)
	    {
		    SetMaterial(texture.Name,true);
	    }

        public void SetMaterial(string name, bool texture)
	    {
		    if(!texture)
		    {
                base.Material = name;
		    }
		    else
		    {
                // If material already exists, use it!
                if (mMaterialPtr == null) // GDZ, codigo original: 			if(mMaterialPtr.isNull())
			    {
                    mMaterialPtr = MaterialManager.Singleton.Create(mInstanceName+"_RenderTextureMaterial",
                        ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME);
				    mMaterialPtr.GetTechnique(0).GetPass(0).CreateTextureUnitState(name);
                }
			    mOverlayElement.MaterialName = mMaterialPtr.Name;
		    }
	    }

	    public void SetUV(float u1, float v1, float u2, float v2)
	    {
		    ((PanelOverlayElement)mOverlayElement).SetUV(u1,v1,u2,v2);
	    }
    }
}
