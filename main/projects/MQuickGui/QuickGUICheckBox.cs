using System;
using Mogre;

namespace MQuickGUI
{
    public class CheckBox : Button
    {
        protected bool mChecked = false;

        public event EventHandler OnCheckedChanged;

        public CheckBox(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material, OverlayContainer overlayContainer, Widget ParentWidget)
            : base(name, dimensions, positionMode, sizeMode, material, overlayContainer, ParentWidget)
        {
            mWidgetType = WidgetType.QGUI_TYPE_CHECKBOX;
            mWidgetMaterial = this.Sheet.DefaultSkin + ".checkbox.uncheked";

            OnMouseClicked += new MouseClickedEventHandler(CheckBox_OnMouseClicked);
        }

        void CheckBox_OnMouseClicked(object source, MouseEventArgs e)
        {
            this.ToogleCheckedValue();
        }

        public void ToogleCheckedValue()
        {
            this.mChecked = !this.mChecked;

            // change skin component and apply skin
            if (this.mChecked)
                this.mWidgetMaterial = this.Sheet.DefaultSkin + ".checkbox.checked";
            else
                this.mWidgetMaterial = this.Sheet.DefaultSkin + ".checkbox.unchecked";

            this.Button_OnMouseEnter(this, new MouseEventArgs(this));

            if (this.OnCheckedChanged != null)
                this.OnCheckedChanged(this, System.EventArgs.Empty);
        }

        public bool Checked
        {
            get { return mChecked; }
            set
            {
                if((value  && !this.mChecked) || (!value && this.mChecked))
                {
                    this.ToogleCheckedValue();
                }
            }
        }
    }
}
