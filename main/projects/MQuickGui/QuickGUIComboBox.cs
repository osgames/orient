using Mogre;
using MOIS;
using Vector3=Mogre.Vector3;

namespace MQuickGUI
{
    /** Represents a traditional ComboBox.
    @remarks
    The ComboBox class requires at least 3 materials to define it's image:
    Normal State, Mouse Over, and Mouse Down.  For example, if you pass
    the constructor "sample.combobox" as its arguement for the material,
    the class will expect "sample.combobox.over" and "sample.combobox.down"
    to exist.  The ComboBox supplies a list of items from which the user
    can choose.
    @note
    In order to get the most use out of ComboBox, you will need to add
    ListItems.
    @note
    ComboBoxes are meant to be created via the Window widget.
    */
    public class ComboBox : Label
    {
		// Button that shows the drop down list.
		protected Button mButton;
		// Drop down list.
		protected List mList;

		GuiHorizontalAlignment mListItemHorizontalAlignment;
		GuiVerticalAlignment mListItemVerticalAlignment;

		// User defined event handlers that are called when a Selection is made.
        // Reemplazado por Manejo de Eventos // GDZ
		//List<MemberFunctionSlot> mOnSelectUserEventHandlers = new List<MemberFunctionSlot>();

        
        internal ComboBox(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material, OverlayContainer overlayContainer, Widget ParentWidget) : base(name,dimensions,positionMode,sizeMode,material,overlayContainer,ParentWidget)
	    {
            mListItemHorizontalAlignment = GuiHorizontalAlignment.GHA_CENTER;
            mListItemVerticalAlignment = GuiVerticalAlignment.GVA_CENTER;
		    mWidgetType = WidgetType.QGUI_TYPE_COMBOBOX;

		    // Create CloseButton - remember to position it relative to it's parent (TitleBar)
		    // Height of the Title Bar
		    float height = (mAbsoluteDimensions.w / mAbsoluteDimensions.z);
		    // Button has same height as width - Make the button slightly smaller that the titlebar height
		    float buttonHeight = 0.8f;
		    float buttonWidth = (height * buttonHeight);
		    // Make a 5 pixel buffer
		    float buffer = 5.0f / mPixelDimensions.z;
            Vector4 bDimensions = new Vector4((1 - (buttonWidth + buffer)), 0.1f, buttonWidth, buttonHeight);
		    mButton = new Button(mInstanceName+".button",bDimensions, QGuiMetricsMode.QGUI_GMM_RELATIVE, QGuiMetricsMode.QGUI_GMM_RELATIVE, mWidgetMaterial+".button",mChildrenContainer,this);
		    mButton.SetZOrderOffset(1);
		    _addChildWidget(mButton);

		    mList = new List(mInstanceName+".List",new Vector3(0,1.0f,1.0f), QGuiMetricsMode.QGUI_GMM_RELATIVE, QGuiMetricsMode.QGUI_GMM_RELATIVE, mWidgetMaterial+".list",Sheet.MenuContainer,this);
		    mList.CharacterHeight = mCharacterHeight;
		    mList.Font = mFont;
		    mList.SetTextColor(mTextTopColor,mTextBotColor);
		    mList.Hide();
		    int derivedZOrder = Sheet.MenuOverlayZOrder + 1;
            Window w = Window;
            if (w != null)
                mList.SetZOrderOffset(derivedZOrder - Window.ZOrder, false);
            else
                mList.SetZOrderOffset(derivedZOrder, false);
            _addChildWidget(mList);

            this.AlignListItemText(GuiHorizontalAlignment.GHA_LEFT, GuiVerticalAlignment.GVA_CENTER);
            this.AlignText(GuiHorizontalAlignment.GHA_LEFT, GuiVerticalAlignment.GVA_CENTER);

            OnDeactivate += new DeactivateEventHandler(ComboBox_OnDeactivate);
            OnMouseEnter += new MouseEnterEventHandler(ComboBox_OnMouseEnter);
            OnMouseLeaves += new MouseLeavesEventHandler(ComboBox_OnMouseLeaves);
            OnMouseClicked += new MouseClickedEventHandler(ComboBox_OnMouseClicked);
            OnMouseButtonUp += new MouseButtonUpEventHandler(ComboBox_OnMouseButtonUp);
	    }

        void ComboBox_OnDeactivate(object source, EventArgs e)
        {
            if (!mEnabled)
                return;

            //Restore default material
            mOverlayElement.MaterialName = mWidgetMaterial;
            mButton.ApplyDefaultMaterial();
            //If the Mouse has clicked on the ComboBox's List or ListItems, the widget should not *deactivate*.
            //As for hiding the list, this will be taken care of in the onMouseButtonDown handler.  The list needs
            //to remain visible so that ListItem picking works correctly. (If list is hidden, you can't click the ListItem..)
            Vector2 p = MouseCursor.Singleton.PixelPosition;
            if (GetTargetWidget(p) != null)
                return;

            mList.Hide();
        }

        void ComboBox_OnMouseEnter(object source, MouseEventArgs e)
        {
            if (!mEnabled)
                return;

            if (!mList.Visible)
                mOverlayElement.MaterialName = mWidgetMaterial + ".over";

            if (mList.Visible)
                mButton.ApplyButtonDownMaterial();
        }

        void ComboBox_OnMouseLeaves(object source, MouseEventArgs e)
        {
            if (!mEnabled)
                return;

            if (!mList.Visible)
                mOverlayElement.MaterialName = mWidgetMaterial;
        }

        void ComboBox_OnMouseClicked(object source, MouseEventArgs e)
        {
            if (!mEnabled)
                return;

            if (e.button == MouseButtonID.MB_Left)
            {
                Widget w = mList.GetTargetWidget(e.position);
                if (w != null)
                {
                    //item selection
                    e.widget = w;
                    Selection(e);
                    return;
                }
                else
                {
                    if (!mList.Visible)
                    {
                        // apply button ".down" material
                        mOverlayElement.MaterialName = mWidgetMaterial + ".down";
                        mButton.ApplyButtonDownMaterial();
                        mList.Show();
                    }
                    else
                    {
                        mOverlayElement.MaterialName = mWidgetMaterial + ".over";
                        mButton.ApplyDefaultMaterial();
                        mList.Hide();
                    }
                }
            }
        }

        void ComboBox_OnMouseButtonUp(object source, MouseEventArgs e)
        {
            if (!mEnabled)
                return; 
            
            if (mList.Visible) mButton.ApplyButtonDownMaterial();
            else mButton.ApplyDefaultMaterial();
        }

	    ~ComboBox()
	    {
		    //RemoveAndDestroyAllChildWidgets();
	    }

        public void AddListItem(string text)
	    {
		    this.mList.AddListItem(text);
	    }

        public void AddListItem(string tag, string text)
	    {
		    this.mList.AddListItem(tag,text);
	    }

        public void AlignListItemText(GuiHorizontalAlignment ha, GuiVerticalAlignment va)
	    {
            this.mListItemHorizontalAlignment = ha;
            this.mListItemVerticalAlignment = va;
            this.mList.AlignListItemText(ha, va);
	    }

	    public void ClearList()
	    {
            this.mList.ClearList();
	    }

        public int NumberOfListItems
        {
            get { return mList.NumberOfListItems; }
        }

        public void RemoveListItem(int index)
	    {
		    mList.RemoveListItem(index);
	    }

        public override float CharacterHeight
        {
            set
            {
                base.CharacterHeight = value;
                mList.CharacterHeight = value;
            }
        }

        public override GuiHorizontalAlignment HorizontalAlignment
        {
            set
            {
                base.HorizontalAlignment = value;
                this.mList.HorizontalAlignment = value;
            }
        }

        public override GuiVerticalAlignment VerticalAlignment
        {
            set
            {
                base.VerticalAlignment = value;
                this.mList.VerticalAlignment = value;
            }
        }

        public string DropListHighlightMaterial
        {
            set { mList.HighlightMaterial = value; }
        }

        public float DropListWidth
        {
            set { mList.SetWidth(value); }
        }

        public override void Show()
	    {
		    base.Show();
		    mList.Hide();
	    }


        public delegate void SelectionEventHandler(object source, WidgetEventArgs e);
        public event SelectionEventHandler OnSelection;

        public void Selection(WidgetEventArgs e)
        {
            if (!mEnabled)
                return;

            SetSelectedItem(e.widget.Text);
        }
        
        public void SetSelectedItem(string itemText){
        	
        	Widget selectedItem = mList.GetItem(itemText);
        	
        	if (selectedItem!=null) {
        		Text = selectedItem.Text;
	            mOverlayElement.MaterialName = mWidgetMaterial;
	            mButton.ApplyDefaultMaterial();
	            mList.Hide();
	            if (OnSelection!=null){
		            WidgetEventArgs wea = new WidgetEventArgs(selectedItem);
	                OnSelection(this, wea);
	            }
        	}
        }


    }
}
