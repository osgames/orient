using Mogre;

namespace MQuickGUI
{
    public enum FillDirection
    {
        FILLS_NEGATIVE_TO_POSITIVE = 0,
        FILLS_POSITIVE_TO_NEGATIVE
    }

    public enum Layout
    {
        LAYOUT_HORIZONTAL = 0,
        LAYOUT_VERTICAL
    }

    public class ProgressBar : Widget
    {
        protected float mProgress = 1;
        protected Image mBarPanel = null;

        // Stores how this bar should fill as progress increases
        protected FillDirection mFillDirection = FillDirection.FILLS_NEGATIVE_TO_POSITIVE;
        protected Layout mLayout = Layout.LAYOUT_HORIZONTAL;

        public ProgressBar(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material, OverlayContainer overlayContainer, Widget ParentWidget)
            :
            base(name, dimensions, positionMode, sizeMode, material, overlayContainer, ParentWidget)
        {
            this.mWidgetType = WidgetType.QGUI_TYPE_PROGRESSBAR;

            this.mWidgetMaterial = this.Sheet.DefaultSkin + ".progressbar";
            this.mOverlayElement = CreatePanelOverlayElement(mInstanceName + "_Background", mPixelDimensions, "");
            this.mOverlayContainer.AddChild(mOverlayElement);
            this.mOverlayElement.Show();
            this.Material = this.mWidgetMaterial;

            if (dimensions.z > dimensions.w)
                mLayout = Layout.LAYOUT_HORIZONTAL;
            else
                mLayout = Layout.LAYOUT_VERTICAL;


            //_getBarExtents();

            //bar image fully loaded
            this.mBarPanel = new Image(mInstanceName + ".Image", new Vector4(0, 0, 1, 1),
                QGuiMetricsMode.QGUI_GMM_RELATIVE, QGuiMetricsMode.QGUI_GMM_RELATIVE, material, mChildrenContainer, this);
            this.mBarPanel.SetZOrderOffset(1, false);
            this.mBarPanel.SetMaterial(this.mWidgetMaterial + ".bar", false);
            this.mBarPanel.SetZOrderOffset(1);
            _addChildWidget(this.mBarPanel);
        }

        public delegate void ProgressEventHandler(object source, WidgetEventArgs e);
        public event ProgressEventHandler OnProgressChanged;

        public float Progress
        {
            get { return mProgress; }
            set
            {
                // Check to make sure we get acceptable values
                if (value >= 0.99f)
                    this.mProgress = 1;
                else if (value < 0.01f)
                    mProgress = 0;
                else
                    mProgress = value;

                // update the bar's texture to match the progress.
                if (this.mLayout == Layout.LAYOUT_HORIZONTAL)
                {
                    this.mBarPanel.SetDimensions(new Vector4(0, 0, this.Progress, 1),
                        QGuiMetricsMode.QGUI_GMM_RELATIVE, QGuiMetricsMode.QGUI_GMM_RELATIVE);
                }
                else
                {
                    this.mBarPanel.SetDimensions(new Vector4(0, 0, 1, this.Progress),
                        QGuiMetricsMode.QGUI_GMM_RELATIVE, QGuiMetricsMode.QGUI_GMM_RELATIVE);
                }

                // fire event and call any user defined handlers
                WidgetEventArgs e = new WidgetEventArgs(this);
                e.handled = false;

                if (this.OnProgressChanged != null) this.OnProgressChanged(this, e);
            }
        }

        public FillDirection FillDirection
        {
            get { return this.mFillDirection; }
            set
            {
                mFillDirection = value;
                this.Progress = mProgress;
            }
        }

        public Layout Layout
        {
            set
            {
                this.mLayout = value;
                this.Progress = mProgress;
            }
            get { return this.mLayout; }
        }


    }
}
