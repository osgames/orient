using Mogre;

namespace MQuickGUI
{
    public class MenuList : Button
    {
        protected List mList;

	    public MenuList(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material, OverlayContainer overlayContainer, Widget ParentWidget) : base(name,dimensions,positionMode, sizeMode,material+".button",overlayContainer,ParentWidget)
	    {
		    mWidgetType = WidgetType.QGUI_TYPE_MENULIST;
	        CharacterHeight = ParentWidget.CharacterHeight;

		    // create list
		    mList = new List(mInstanceName+".List",new Vector3(0f,1.0f,1.0f), QGuiMetricsMode.QGUI_GMM_RELATIVE, QGuiMetricsMode.QGUI_GMM_RELATIVE, material+".list",Sheet.MenuContainer,this);
		    mList.CharacterHeight = mCharacterHeight;
		    mList.Font = mFont;
		    mList.SetTextColor(mTextTopColor,mTextBotColor);
		    mList.Hide();
		    // give the list the same zOrder as the button representing this menulist.
		    mList.SetZOrderOffset(mZOrderOffset,false);
		    _addChildWidget(mList);

            this.AlignText(GuiHorizontalAlignment.GHA_LEFT, GuiVerticalAlignment.GVA_CENTER);
            this.AlignListItemText(GuiHorizontalAlignment.GHA_LEFT, GuiVerticalAlignment.GVA_CENTER);

            OnDeactivate += new DeactivateEventHandler(MenuList_OnDeactivate);
	    }

        void MenuList_OnDeactivate(object source, EventArgs e)
        {
        	if(!mEnabled)
                return;

		    // If the Mouse has clicked on any of the menu's List or ListItems, the widget should not *deactivate*.
		    // As for hiding the list, this will be taken care of in the onMouseButtonDown handler.  The list needs
		    // to remain visible so that ListItem picking works correctly. (If list is hidden, you can't click the ListItem..)
		    if(GetTargetWidget(MouseCursor.Singleton.PixelPosition) != null)
                return;
        }

	    ~MenuList()
	    {
		    //RemoveAndDestroyAllChildWidgets();
	    }

	    public ListItem AddListItem(string text)
	    {
		    return mList.AddListItem(text);
	    }

	    public ListItem AddListItem(string name, string text)
	    {
		    return mList.AddListItem(name,text);
	    }

        public void AlignListItemText(GuiHorizontalAlignment ha, GuiVerticalAlignment va)
	    {
		    mList.AlignListItemText(ha,va);
	    }

        public void ClearList()
	    {
		    mList.ClearList();
	    }

        public ListItem GetListItem(int index)
	    {
		    return mList.GetListItem(index);
	    }

        public ListItem GetListItem(string name)
	    {
		    return mList.GetListItem(name);
	    }

        public int NumberOfListItems
        {
            get { return mList.NumberOfListItems; }
        }

        public void HideHighlight()
	    {
		    mList.HideHighlight();
	    }

        public void HighlightListItem(ListItem i)
	    {
		    mList.HighlightListItem(i);
	    }

	    public void HideList()
	    {
		    mList.Hide();
	    }

        public bool ListVisible
        {
            get { return mList.Visible; }
        }

        public bool MouseOverButton()
	    {
		    return IsPointWithinBounds(MouseCursor.Singleton.PixelPosition);
	    }

        public void RemoveListItem(int index)
	    {
		    mList.RemoveListItem(index);
	    }

        public float ListCharacterHeight
        {
            set { mList.CharacterHeight = value; }
            get { return mList.CharacterHeight; }
        }

        public string ListHighlightMaterial
        {
            set { mList.HighlightMaterial = value; }
            get { return mList.HighlightMaterial; }
        }

        public void SetListWidth(float relativeWidth)
	    {
		    // make it seem like the width is relative to the menu, and not the menu list
            float numPixels = relativeWidth * mParentWidget.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).x; 
		    mList.SetWidth(numPixels / mPixelDimensions.z);
	    }

	    public void ShowList()
	    {
		    mList.Show();
	    }
    }
}
