using Mogre;

namespace MQuickGUI
{
    public class VerticalTrackBar : HorizontalTrackBar
    {
        public new event ValueEventHandler OnValueChanged;

        public VerticalTrackBar(string name, Vector4 dimensions, QGuiMetricsMode positionMode, QGuiMetricsMode sizeMode, string material, OverlayContainer overlayContainer, Widget ParentWidget)
            : base(name, dimensions, positionMode, sizeMode, material, overlayContainer, ParentWidget)
        {
            this.mSliderButton.SetDimensions(new Vector4(0, 0, 1, 0.1f));

            this.mWidgetType = WidgetType.QGUI_TYPE_VERTICAL_TRACKBAR;
            this.mWidgetMaterial = this.Sheet.DefaultSkin + ".trackbar.vertical";
            this.Material = this.mWidgetMaterial;

            this._getSliderPositions();
            this.Value = 0;
        }

        protected override void QuickGUIHorizontalTrackBar_OnMouseButtonDown(object source, MouseEventArgs e)
        {
            Vector2 mousePos = e.position - this.GetPosition(QGuiMetricsMode.QGUI_GMM_PIXELS);

            if ((mousePos.y > (mSliderPositions[mCurrentValue] + ((mRegionLength + mSliderButton.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).y)/2.0f))) &&
                (mCurrentValue < mNumRegions))
                this.Value = mCurrentValue + mLargeChange;
            else if ((mousePos.y < (mSliderPositions[mCurrentValue] - ((mRegionLength - mSliderButton.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).y)/2.0))) &&
                     (mCurrentValue > 0))
                this.Value = mCurrentValue - mLargeChange;
        }

        protected override void _getSliderPositions()
        {
            this.mSliderPositions.Clear();

            float sliderPixelHeight = this.mSliderButton.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).y;
            float trackLength = this.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).y - sliderPixelHeight;
            this.mRegionLength = trackLength/this.mNumRegions;

            float yStart = (sliderPixelHeight/2.0f);
            this.mSliderPositions.Add(yStart);
            for (int i = 0; i < this.mNumRegions; ++i)
            {
                this.mSliderPositions.Add(yStart + ((i + 1)*mRegionLength));
            }
        }

	    protected override int getClosestSliderPosition(Vector2 pixelPoint)
	    {
	        Vector2 p = pixelPoint - this.GetPosition(QGuiMetricsMode.QGUI_GMM_PIXELS);

	        if (p.y <= mSliderPositions[0])
	            return 0;
	        if (p.y >= mSliderPositions[mSliderPositions.Count - 1])
	            return (mSliderPositions.Count - 1);

	        int i;
	        for (i = 1; i < mSliderPositions.Count; ++i)
	        {
	            if (p.y < (mSliderPositions[i] - (mRegionLength/2.0)))
	                return (i - 1);
	        }

	        return i;
	    }

	    public override int Value
        {
            get { return mCurrentValue; }
            set
            {
                if (value < 0) value = 0;
                else if (value > this.mNumRegions) value = mNumRegions;

                this.mCurrentValue = value;

                this.mSliderButton.SetPosition(0,
                    (this.mSliderPositions[mCurrentValue] - (this.mSliderButton.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).y/2.0f)) /
                    this.GetSize(QGuiMetricsMode.QGUI_GMM_PIXELS).y);

                ValueEventArgs e = new ValueEventArgs(this, this.mCurrentValue);
                e.handled = false;
                if (this.OnValueChanged != null) this.OnValueChanged(this, e);
            }
        }
    }
}
