/* ****************************************************
 * Name: OgreCameraSpot.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using Mogre;
using PAGE.Generic.Domain;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Resources.Spots;
using PAGE.OGRE.Domain.Cameras;
using PAGE.OGRE.Domain.Helpers;

namespace PAGE.OGRE.Domain.Spots
{
    public class OgreCameraSpot : GenericCameraSpot, IOgreSpot
    {
        #region Properties

        protected new OgreContainerElementHelper ElementHelper
        {
            get { return this.elementHelper as OgreContainerElementHelper; }
        }

        IGraphicalElement IContainerElement.ParentElement
        {
            get { return base.ParentElement; }
        }

        public new IOgreGraphicalElement ParentElement
        {
            get { return base.ParentElement as IOgreGraphicalElement; }
        }

        public SceneNode Node
        {
            get
            {
                this.VerifyDestruction();
                return ((OgreContainerElementHelper)this.elementHelper).Node;
            }
        }

        #endregion

        #region Constructors

        public OgreCameraSpot(
            ResourceCameraSpot resourceSpot, IOgreGraphicalElement parentElement)
            : base(resourceSpot, parentElement)
        {
            this.Reset();
        } 

        #endregion

        #region Override Methods

        protected override ContainerElementHelper CreateElementHelper(
            IGraphicalElement parentElement)
        {
            if (!(parentElement is IOgreGraphicalElement))
                throw new ArgumentException("parent element must be IOgreGraphicalElement", "parentElement");
            IOgreGraphicalElement ogreParentElement = parentElement as IOgreGraphicalElement;

            return new OgreContainerElementHelper(this,
                ogreParentElement, ogreParentElement.Node.CreateChildSceneNode());
        }

        public override void Reset()
        {
            base.Reset();

            this.AbsoluteDirection = this.ResourceSpot.RelativeDirection;
            this.AbsolutePosition = this.ResourceSpot.RelativePosition;
        }

        #endregion
    }
}