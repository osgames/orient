/* ****************************************************
 * Name: IOgreSpot.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/22 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Domain.Spots;

namespace PAGE.OGRE.Domain.Spots
{
    public interface IOgreSpot : IOgreGraphicalElement, IGenericSpot
    {

    }
}