/* ****************************************************
 * Name: IOgreGraphicalElement.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/12 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using Mogre;
using PAGE.Generic.Domain;

namespace PAGE.OGRE.Domain
{
    /// <summary>
    /// Represents a graphical element in Ogre, an element with a scene node in the
    /// Ogre scene manager.
    /// </summary>
    public interface IOgreGraphicalElement : IGraphicalElement
    {
        /// <summary>
        /// Gets the Ogre scene node associated with this element.
        /// </summary>
        SceneNode Node
        {
            get;
        }
    }
}