/* ****************************************************
 * Name: OgreCamera.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using Mogre;
using PAGE.Generic.Domain.Cameras;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Math;
using PAGE.OGRE.Domain.Helpers;
using PAGE.OGRE.Math;

namespace PAGE.OGRE.Domain.Cameras
{
    public class OgreCamera : Generic3DCamera, IOgreGraphicalElement
    {
        #region Fields

        //the ogre camera
        protected Camera camera = null;

        #endregion

        #region Properties

        protected new OgrePlaceableElementHelper ElementHelper
        {
            get { return base.ElementHelper as OgrePlaceableElementHelper; }
        }

        public override Direction AbsoluteDirection
        {
            get { return new OgreDirection(this.camera.RealDirection); }
            set
            {
                this.camera.Direction = Vector3.UNIT_Z;
                base.AbsoluteDirection = value;
            }
        }

        public override Position AbsolutePosition
        {
            get { return new OgrePosition(this.camera.RealPosition); }
            set
            {
                this.camera.Position = Vector3.ZERO;
                base.AbsolutePosition = value;
            }
        }

        public override Orientation AbsoluteOrientation
        {
            get { return new OgreOrientation(this.camera.RealOrientation); }
            set
            {
                this.camera.Orientation = Quaternion.IDENTITY;
                base.AbsoluteOrientation = value;
            }
        }

        public override bool Visible
        {
            get { return base.Visible; }
            set
            {
                base.Visible = value;
                this.ElementHelper.Visible = value;
            }
        }

        public SceneNode Node
        {
            get { return this.ElementHelper.Node; }
        }

        public Camera Camera
        {
            get { return this.camera; }
        }

        #endregion

        #region Constructors

        public OgreCamera(Camera camera)
        {
            this.camera = camera;
            this.ElementHelper.Node = 
                this.Camera.SceneManager.RootSceneNode.CreateChildSceneNode();
            this.Node.AttachObject(camera);
        }

        #endregion

        #region Protected Methods

        protected override PlaceableElementHelper CreateElementHelper()
        {
            return new OgrePlaceableElementHelper(this, null);
        }

        #endregion

        #region Public Methods

        public override void Destroy()
        {
            base.Destroy();
            this.ElementHelper.Destroy();
        }

        public override void Reset()
        {
            this.VerifyDestruction();

            //puts camera on initial position
            this.AbsolutePosition = new OgrePosition(0, 0, 0);

            this.ElementHelper.Reset();
        }

        public override void Zoom(float delta)
        {
            this.VerifyDestruction();

            Vector3 direction = (OgreDirection)this.AbsoluteDirection;
            direction.Normalise();
            direction *= delta;
            this.camera.Move(direction);
        }

        public override void LookAt(Position at)
        {
            this.VerifyDestruction();
            this.camera.LookAt((OgrePosition)at);
        }

        public override void Move(Position position)
        {
            this.VerifyDestruction();
            this.camera.Move((OgrePosition) position);
        }

        public override void MoveRelative(Position relativePos)
        {
            this.VerifyDestruction();
            this.camera.MoveRelative((OgrePosition) relativePos);
        }

        public override void Roll(double angleDeg)
        {
            this.VerifyDestruction();
            this.camera.Roll(new Radian(new Degree((float)angleDeg)));
        }

        public override void Yaw(double angleDeg)
        {
            this.VerifyDestruction();
            this.camera.Yaw(new Radian(new Degree((float)angleDeg)));
        }

        public override void Pitch(double angleDeg)
        {
            this.VerifyDestruction();
            this.camera.Pitch(new Radian(new Degree((float)angleDeg)));
        }

        #endregion
    }
}