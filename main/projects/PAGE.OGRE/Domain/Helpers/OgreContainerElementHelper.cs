/* ****************************************************
 * Name: OgreContainerElementHelper.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/05 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using Mogre;
using PAGE.Generic.Domain;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Math;
using PAGE.OGRE.Domain.Helpers;

namespace PAGE.OGRE.Domain.Helpers
{
    /// <summary>
    /// Represents an Ogre graphical element that can attach other elements to it. If the 
    /// element is also "carrier", the attached elements will also change their position 
    /// and orientation according to the element's graphical properties.
    /// </summary>
    public class OgreContainerElementHelper : ContainerElementHelper, IOgreGraphicalElement
    {
        #region Fields

        protected OgreGraphicalElementHelper graphicalElementHelper = null;

        //attached assets (asset - old parent node)
        protected Dictionary<IPlaceableElement, SceneNode> attachedElements =
            new Dictionary<IPlaceableElement, SceneNode>();

        #endregion

        #region Properties

        protected new IOgreGraphicalElement HelpedElement
        {
            get { return this.helpedElement as IOgreGraphicalElement; }
        }

        public new IOgreGraphicalElement ParentElement
        {
            get { this.VerifyDestruction(); return (IOgreGraphicalElement)parentElement; }
        }

        public override Direction AbsoluteDirection
        {
            get { return this.graphicalElementHelper.AbsoluteDirection; }
            set { this.graphicalElementHelper.AbsoluteDirection = value; }
        }

        public override Position AbsolutePosition
        {
            get { return this.graphicalElementHelper.AbsolutePosition; }
            set { this.graphicalElementHelper.AbsolutePosition = value; }
        }

        public override Orientation AbsoluteOrientation
        {
            get { return this.graphicalElementHelper.AbsoluteOrientation; }
            set { this.graphicalElementHelper.AbsoluteOrientation = value; }
        }

        public override bool Visible
        {
            get { return base.Visible; }
            set
            {
                base.Visible = value;
                this.graphicalElementHelper.Visible = value;
            }
        }

        public SceneNode Node
        {
            get { return this.graphicalElementHelper.Node; }
        }

        #endregion

        #region Constructors

        public OgreContainerElementHelper(
            IContainerElement helpedElement, IOgreGraphicalElement parentElement, SceneNode node)
            : base(helpedElement, parentElement)
        {
            if (!(helpedElement is IOgreGraphicalElement))
                throw new ArgumentException("helped element must be IOgreGraphicalElement", "helpedElement");

            this.graphicalElementHelper = new OgreGraphicalElementHelper(
                (IOgreGraphicalElement)helpedElement, node);
            this.Reset();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Resets this container by unplacing all the elements attached.
        /// </summary>
        public override void Reset()
        {
            base.Reset();

            if (this.Node == null)
            {
                //creates new node and ogre graphical element
                SceneNode node = this.ParentElement.Node.CreateChildSceneNode();
                this.graphicalElementHelper = new OgreGraphicalElementHelper(
                    this.HelpedElement, node);
            }

            //clears attached elements db
            this.attachedElements.Clear();

            //resets ogre graphical element
            this.graphicalElementHelper.Reset();
        }

        public override void Destroy()
        {
            base.Destroy();
            this.graphicalElementHelper.Destroy();
        }

        #endregion

        #region Protected Methods

        protected override void AttachElement(IPlaceableElement element)
        {
            this.VerifyDestruction();

            if (!(element is IOgreGraphicalElement))
                throw new ArgumentException("element is not an IOgreGraphicalElement");

            if (!this.attachedElements.ContainsKey(element))
            {
                //gets asset node
                SceneNode assetNode = ((IOgreGraphicalElement)element).Node;

                //stores old parent node
                this.attachedElements.Add(element, assetNode.ParentSceneNode);

                //detaches from old parent node
                assetNode.ParentSceneNode.RemoveChild(assetNode);

                //attaches asset to spot node
                this.Node.AddChild(assetNode);

                //sets asset position to 0 because now it is attached to the spot node
                assetNode.Position = Vector3.ZERO;
            }
        }

        protected override void DetachElement(IPlaceableElement element)
        {
            this.VerifyDestruction();

            if (!(element is IOgreGraphicalElement))
                throw new ArgumentException("element is not an IOgreGraphicalElement");

            if (this.attachedElements.ContainsKey(element))
            {
                //gets stored old parent node
                SceneNode assetParentNode = this.attachedElements[element];

                //gets asset node
                SceneNode assetNode = ((IOgreGraphicalElement)element).Node;

                //detaches from spot node
                this.Node.RemoveChild(assetNode);

                //attaches asset to old node
                assetParentNode.AddChild(assetNode);

                //removes asset from db
                this.attachedElements.Remove(element);
            }
        }

        #endregion
    }
}