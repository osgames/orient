/* ****************************************************
 * Name: OgreGraphicalElementHelper.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/05 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using Mogre;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Math;
using PAGE.OGRE.Math;

namespace PAGE.OGRE.Domain.Helpers
{
    /// <summary>
    /// Represents a graphical element to be represented by the ogre realizer.
    /// </summary>
    public class OgreGraphicalElementHelper : GraphicalElementHelper, IOgreGraphicalElement
    {
        #region Fields

        protected SceneNode node = null;

        #endregion

        #region Properties

        protected new IOgreGraphicalElement HelpedElement
        {
            get { return this.helpedElement as IOgreGraphicalElement; }
        }

        public override Direction AbsoluteDirection
        {
            get
            {
                this.VerifyDestruction();
                return new OgreDirection(this.node.WorldOrientation*Vector3.UNIT_Z);
            }
            set
            {
                this.VerifyDestruction();

                OgreDirection direction = value as OgreDirection;
                direction.Y = 0;

                //update rotation
                Vector3 src = this.node.WorldOrientation*Vector3.UNIT_Z;
                if (1.0f + src.DotProduct(direction) < 0.0001f)
                {
                    this.node.Yaw(new Degree(180.0f));
                }
                else
                {
                    Quaternion quat = src.GetRotationTo(direction);
                    this.node.Rotate(quat);
                }
            }
        }

        public override Position AbsolutePosition
        {
            get { this.VerifyDestruction(); return new OgrePosition(this.node.WorldPosition); }
            set { this.VerifyDestruction(); this.node.Position = (OgrePosition)value; }
        }

        public override Orientation AbsoluteOrientation
        {
            get { this.VerifyDestruction(); return new OgreOrientation(this.node.WorldOrientation); }
            set { this.VerifyDestruction(); this.node.Orientation = (OgreOrientation)value; }
        }

        public override bool Visible
        {
            get { return base.Visible; }
            set
            {
                base.Visible = value;
                this.node.SetVisible(value, false);
            }
        }

        public SceneNode Node
        {
            get { return this.node; }
            set { this.node = value; }
        }

        #endregion

        #region Constructors

        public OgreGraphicalElementHelper(
            IOgreGraphicalElement helpedElement, SceneNode node)
            :base(helpedElement)
        {
            this.node = node;
        } 

        #endregion

        #region Public Methods

        public override void Destroy()
        {
            base.Destroy();

            if (this.node != null)
            {
                ///removes node
                this.node.RemoveAndDestroyAllChildren();
                this.node.ParentSceneNode.RemoveAndDestroyChild(this.node.Name);
            }
        } 

        #endregion
    }
}