/* ****************************************************
 * Name: OgreItemElement.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/25 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using Mogre;
using PAGE.Generic.Domain;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Collections;
using PAGE.Generic.Domain.Spots;

namespace PAGE.OGRE.Domain.Helpers
{
    /// <summary>
    /// Represents a graphical item element to be represented by the ogre 
    /// realizer.
    /// </summary>
    public class OgreItemHelper : OgrePlaceableElementHelper
    {
        #region Constructors

        public OgreItemHelper(IPlaceableElement helpedElement, SceneNode node)
            : base(helpedElement, node)
        {
        } 

        #endregion

        #region Public Methods

        public override GenericSpotList<IGenericSpot> GetPlaceableSpots(IInteractionAsset targetSpotContainer)
        {
            GenericSpotList<IGenericSpot> placeableSpots = new GenericSpotList<IGenericSpot>();
            foreach (GenericItemSpot spot in targetSpotContainer.ItemSpots)
                placeableSpots.Add(spot);
            return placeableSpots;
        }

        #endregion
    }
}