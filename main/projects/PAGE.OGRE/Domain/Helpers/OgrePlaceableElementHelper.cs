/* ****************************************************
 * Name: OgrePlaceableElementHelper.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/05 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using Mogre;
using PAGE.Generic.Domain;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Collections;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Math;
using PAGE.OGRE.Domain.Helpers;
using PAGE.OGRE.Math;

namespace PAGE.OGRE.Domain.Helpers
{
    /// <summary>
    /// Represents a graphical placeable element to be represented by the ogre realizer.
    /// </summary>
    public class OgrePlaceableElementHelper : PlaceableElementHelper, IOgreGraphicalElement
    {
        #region Fields

        protected OgreGraphicalElementHelper graphicalElementHelper = null;

        #endregion

        #region Properties

        protected new IOgreGraphicalElement HelpedElement
        {
            get { return base.HelpedElement as IOgreGraphicalElement; }
        }

        public override Direction AbsoluteDirection
        {
            get { return this.graphicalElementHelper.AbsoluteDirection; }
            set { this.graphicalElementHelper.AbsoluteDirection = value; }
        }

        public override Position AbsolutePosition
        {
            get { return this.graphicalElementHelper.AbsolutePosition; }
            set { this.graphicalElementHelper.AbsolutePosition = value; }
        }

        public override Orientation AbsoluteOrientation
        {
            get { return this.graphicalElementHelper.AbsoluteOrientation; }
            set { this.graphicalElementHelper.AbsoluteOrientation = value; }
        }

        public override bool Visible
        {
            get { return base.Visible; }
            set 
            {
                base.Visible = value;
                this.graphicalElementHelper.Visible = value;
            }
        }

        public SceneNode Node
        {
            get { return this.graphicalElementHelper.Node; }
            set { this.graphicalElementHelper.Node = value; }
        }

        #endregion

        #region Constructors

        public OgrePlaceableElementHelper(
            IPlaceableElement helpedElement, SceneNode node)
            :base(helpedElement)
        {
            if (! (helpedElement is IOgreGraphicalElement))
                throw new ArgumentException("helped element must be IOgreGraphicalElement", "helpedElement");

            this.graphicalElementHelper = new OgreGraphicalElementHelper(
                (IOgreGraphicalElement)helpedElement, node);
        } 

        #endregion

        #region Public Methods

        public override GenericSpotList<IGenericSpot> GetPlaceableSpots(IInteractionAsset targetSpotContainer)
        {
            GenericSpotList<IGenericSpot> placeableSpots = new GenericSpotList<IGenericSpot>();
            foreach (GenericWaypoint spot in targetSpotContainer.Waypoints)
                placeableSpots.Add(spot);
            return placeableSpots;
        }

        public override void Reset()
        {
            base.Reset();
            this.graphicalElementHelper.Reset();
        }

        public override void Destroy()
        {
            base.Destroy();
            this.graphicalElementHelper.Destroy();
        }

        public override void Unplace()
        {
            IGenericSpot spot = this.CurrentSpot;           
            base.Unplace();
            bool resetOrientation = true;

            // check if orientation should be reset (not if spot belongs to a character)
            if (spot != null && (spot.ParentElement is OgreAssetHelper))
            {
                if ((spot.ParentElement as OgreAssetHelper).HelpedAsset is OGRE.Domain.Assets.OgreCharacter)
                    resetOrientation = false;
            }
                
            // reset orientation    
            if (resetOrientation) this.graphicalElementHelper.Node.ResetOrientation();
        }

        public override void TurnTo(Position absoluteTargetPosition)
        {
            ((OgrePosition) absoluteTargetPosition).Y = 0;
            base.TurnTo(absoluteTargetPosition);
        }

        #endregion
    }
}