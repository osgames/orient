/* ****************************************************
 * Name: OgreAssetHelper.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/25 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using Mogre;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Management;
using PAGE.Generic.Resources.Spots;
using PAGE.OGRE.Domain.Assets;
using PAGE.OGRE.Domain.Spots;
using PAGE.OGRE.Util;
using PAGE.OGRE.Math;

namespace PAGE.OGRE.Domain.Helpers
{
    /// <summary>
    /// Contains code to control the behavior of OGRE interaction assets.
    /// </summary>
    public class OgreAssetHelper : GenericAsset, IOgreInteractionAsset
    {
        #region Fields

        protected IOgreInteractionAsset helpedAsset;

        protected SpotsDebug<GenericWaypoint> waypointsDebug;
        protected SpotsDebug<GenericCameraSpot> camerasDebug;
        protected SpotsDebug<GenericItemSpot> itemsDebug;

        #endregion

        #region Properties

        public SceneNode Node
        {
            get { return this.HelpedAsset.Node; }
        }

        /// <summary>
        /// Gets the asset using this help code.
        /// </summary>
        public IOgreInteractionAsset HelpedAsset
        {
            get { return this.helpedAsset; }
        }

        #endregion

        #region Constructors

        public OgreAssetHelper(IOgreInteractionAsset helpedAsset)
            : base(helpedAsset.ResourceAsset)
        {
            this.helpedAsset = helpedAsset;
        }

        #endregion

        #region Protected Methods

        protected override void CreateManagers()
        {
            //this helper has nothing to do with managing assets
        }

        protected override GraphicalElementHelper CreateElementHelper()
        {
            return null; //this helper has nothing to do with graphical properties
        }

        protected override PositionManager CreatePositionManager()
        {
            return null; //this helper has nothing to do with graphical properties
        }

        protected override TextManager CreateTextManager()
        {
            return null; //this helper has nothing to do with graphical properties
        }

        #endregion

        #region Public Methods

        public void CreateAssetSpots()
        {
            this.CreateSpots();
        }

        protected override void CreateSpots()
        {
            //creates camera, waypoint and item spots
            foreach (ResourceCameraSpot camSpot in this.HelpedAsset.ResourceAsset.ResourceCameraSpots)
            {
                this.HelpedAsset.CameraSpots.Add(new OgreCameraSpot(camSpot, this));
            }
            foreach (ResourceWaypoint waypoint in this.HelpedAsset.ResourceAsset.ResourceWaypoints)
            {
                this.HelpedAsset.Waypoints.Add(new OgreWaypoint(waypoint, this));
            }
            foreach (ResourceItemSpot itemSpot in this.HelpedAsset.ResourceAsset.ResourceItemSpots)
            {
                this.HelpedAsset.ItemSpots.Add(new OgreItemSpot(itemSpot, this));
            }
        }

        public override void Destroy()
        {
            this.HideAllDebuggingSpots();
        }

        public override void VerifyDestruction()
        {
            //no destruction to be checked here
        }

        #endregion

        #region Debug Methods

        //returns a factor for scaling
        private float getScaleFactor()
        {
            // get the scale vector for the set
            OgreScale setScale = (OgreScale)this.HelpedAsset.ResourceAsset.Scale;

            // for a mysterious reason spryteset1 at this point returned (5,1,1) when it should have been (5,5,5)
            // other sets were ok, nevertheless for this reason, return only the biggest number
            if (setScale.X >= setScale.Y && setScale.X >= setScale.Z)
                return setScale.X; // x is the biggest (or all equal)
            if (setScale.Y >= setScale.Z)
                return setScale.Y; // y is the biggest
            return setScale.Z; // z must be the biggest
        }


        public void ShowWaypoints()
        {
            if (this.waypointsDebug == null)
            {
                float scale = getScaleFactor();
                this.waypointsDebug = new SpotsDebug<GenericWaypoint>(
                    this.HelpedAsset.Node.Creator, "blueball.mesh",
                    new Vector3(0.2f/scale, 0.2f/scale, 0.2f/scale));              
            }
            this.waypointsDebug.ShowSpots(this.HelpedAsset.Waypoints);
        }

        public void HideWaypoints()
        {
            if (this.waypointsDebug != null)
                this.waypointsDebug.HideSpots();
        }

        public void ShowCameraSpots()
        {
            if (this.camerasDebug == null)
            {
                float scale = getScaleFactor();
                this.camerasDebug = new SpotsDebug<GenericCameraSpot>(
                    this.HelpedAsset.Node.Creator, "greenball.mesh",
                    new Vector3(0.2f/scale, 0.2f/scale, 0.2f/scale));
            }
            this.camerasDebug.ShowSpots(this.HelpedAsset.CameraSpots);
        }

        public void HideCameraSpots()
        {
            if (this.camerasDebug != null)
                this.camerasDebug.HideSpots();
        }

        public void ShowItemSpots()
        {
            if (this.itemsDebug == null)
            {
                float scale = getScaleFactor();
                this.itemsDebug = new SpotsDebug<GenericItemSpot>(
                    this.HelpedAsset.Node.Creator, "whiteball.mesh",
                    new Vector3(0.2f/scale, 0.2f/scale, 0.2f/scale));
            }
            this.itemsDebug.ShowSpots(this.HelpedAsset.ItemSpots);
        }

        public void HideItemSpots()
        {
            if (this.itemsDebug != null)
                this.itemsDebug.HideSpots();
        }

        public void ShowAllDebuggingSpots()
        {
            this.ShowWaypoints();
            this.ShowCameraSpots();
            this.ShowItemSpots();
        }

        public void HideAllDebuggingSpots()
        {
            this.HideWaypoints();
            this.HideCameraSpots();
            this.HideItemSpots();
        }

        #endregion
    }
}