/* ****************************************************
 * Name: OgreCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using Mogre;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Management;
using PAGE.Generic.Management.Character;
using PAGE.Generic.Math;
using PAGE.Generic.Resources.Assets;
using PAGE.OGRE.Domain.Cameras;
using PAGE.OGRE.Domain.Helpers;
using PAGE.OGRE.Management;
using PAGE.OGRE.Management.Character;
using PAGE.OGRE.Math;
using TextureManager=PAGE.Generic.Management.Character.TextureManager;

namespace PAGE.OGRE.Domain.Assets
{
    public class OgreCharacter : HumanLikeCharacter, IOgreEntityAsset
    {
        #region Fields

        protected const int SUBTITLE_OFFSET = 7;

        protected Entity entity;
        protected OgreCamera camera;
        protected OgreVisibilityManager visibilityManager;

        protected OgreAssetHelper ogreAssetHelper;

        #endregion

        #region Properties

        protected new OgrePlaceableElementHelper ElementHelper
        {
            get { return base.ElementHelper as OgrePlaceableElementHelper; }
        }

        public OgreVisibilityManager VisibilityManager
        {
            get { return this.visibilityManager; }
        }

        public SceneNode Node
        {
            get { return this.ElementHelper.Node; }
        }

        public override Position AbsolutePosition
        {
            get { return base.AbsolutePosition; }
            set
            {
                //characters don't climb
                ((OgrePosition) value).Y = ((OgrePosition) this.AbsolutePosition).Y;
                base.AbsolutePosition = value;
            }
        }

        /// <summary>
        /// Gets or sets the set that is currently associated with the character and 
        /// destroys the navigation manager if the set is null, or creates a new one
        /// otherwise.
        /// </summary>
        public override GenericSet Set
        {
            get { return base.Set; }
            set
            {
                base.Set = value;
                if (value != null)
                {
                    this.navigationManager = new OgreWalkingNavigationManager(this);
                    this.managers.Add(this.navigationManager);
                }
            }
        }

        public Entity Entity
        {
            get
            {
                this.VerifyDestruction();
                return this.entity;
            }
        }

        public override Distance Height
        {
            get
            {
                this.VerifyDestruction();
                if (this.entity != null)
                    return new OgreDistance(this.entity.BoundingBox.Maximum.y);
                return new OgreDistance(0);
            }
        }

        public override float CurrentAnimationLenght
        {
            get
            {
                return this.animManager == null ? 0 : 
                    this.animManager.AnimationDuration;
            }
        }

        public override float CurrentAnimationTimeLeft
        {
            get
            {
                return this.animManager == null ? 0 : 
                    this.animManager.AnimationTimeLeft;
            }
        }

        public override float CurrentSpeechLenght
        {
            get
            {
                return this.speechManager == null ? 0 :
                    this.speechManager.SpeechDuration;
            }
        }

        public override float CurrentSpeechTimeLeft
        {
            get
            {
                return this.speechManager == null ? 0 :
                    this.speechManager.SpeechTimeLeft;
            }
        }

        public new OgreTextureManager TextureManager
        {
            get { return base.TextureManager as OgreTextureManager; }
        }

        public new OgreWalkingNavigationManager NavigationManager
        {
            get { return base.NavigationManager as OgreWalkingNavigationManager; }
        }

        public new OgreAnimationManager AnimationManager
        {
            get { return base.AnimationManager as OgreAnimationManager; }
        }

        public new OgreSpeechManager SpeechManager
        {
            get { return base.SpeechManager as OgreSpeechManager; }
        }

        public new OgrePositionManager PositionManager
        {
            get { return base.PositionManager as OgrePositionManager; }
        }

        #endregion

        #region Constructors 

        protected OgreCharacter(
            ResourceHumanLikeCharacter resource,
            OgreCamera camera, Entity entity, bool init)
            : base(resource, false)
        {
            if (camera == null)
                throw new ArgumentNullException("camera",
                                                "Could not create OgreCharacter: camera can't be null");

            this.camera = camera;
            this.entity = entity;

            if (init) this.Init();
        }

        public OgreCharacter(
            ResourceHumanLikeCharacter resource,
            OgreCamera camera, Entity entity)
            : this(resource, camera, entity, true)
        {
        }

        #endregion

        #region Public Methods

        public override void ChangeTexture(string bodyPartName, string textureName)
        {
            this.VerifyDestruction();
            this.ChangeTexture(0, textureName);
        }

        public override void ChangeTexture(int bodyPartIndex, string textureName)
        {
            this.VerifyDestruction();
            if (this.TextureManager != null)
                this.TextureManager.ChangeEntityTexture((uint) bodyPartIndex, textureName);
        }

        public virtual void FadeIn(float transitionTime)
        {
            this.VerifyDestruction();
            this.visibilityManager.FadeIn(transitionTime, true);
        }

        public virtual void FadeOut(float transitionTime)
        {
            this.VerifyDestruction();
            this.visibilityManager.FadeOut(transitionTime, true);
        }

        #endregion

        #region Protected Methods

        protected override void Init()
        {
            base.Init();
            this.ogreAssetHelper = this.CreateAssetHelper();
        }

        /// <summary>
        /// Creates a new object containing help code for this asset.
        /// </summary>
        /// <returns>the OgreAssetHelper to be used by this asset.</returns>
        protected virtual OgreAssetHelper CreateAssetHelper()
        {
            return new OgreAssetHelper(this);
        }

        protected override GraphicalElementHelper CreateElementHelper()
        {
            return new OgrePlaceableElementHelper(this, this.entity.ParentSceneNode);
        }

        protected override void CreateManagers()
        {
            base.CreateManagers();

            this.visibilityManager = this.CreateVisibilityManager();
            if (this.visibilityManager != null) this.managers.Add(this.visibilityManager);
        }

        /// <summary>
        /// Creates the manager to control the character's visibility 
        /// in the realizer.
        /// </summary>
        /// <returns>the OgreVisibilityManager associated to the character.</returns>
        protected virtual OgreVisibilityManager CreateVisibilityManager()
        {
            return new OgreVisibilityManager(this);
        }

        protected override AnimationManager CreateAnimationManager()
        {
            return new OgreAnimationManager(this);
        }

        protected override WalkingNavigationManager CreateNavigationManager()
        {
            return new OgreWalkingNavigationManager(this);
        }

        protected override SpeechManager CreateSpeechManager()
        {
            return new OgreSpeechManager(this);
        }

        protected override TextManager CreateTextManager()
        {
            OgreTextManager newTextManager = new OgreTextManager(this, this.camera);
            if (newTextManager.MainText != null)
            {
                newTextManager.MainText.Color = this.ResourceAsset.Color;
                newTextManager.MainText.CharHeight = 28;
                newTextManager.MainText.RelativeTextPosition =
                    new OgrePosition(0, (float)(OgreDistance)this.Height + SUBTITLE_OFFSET, 0);
            }
            newTextManager.MainText.Update(0);
            return newTextManager;
        }

        protected override TextureManager CreateTextureManager()
        {
            return new OgreTextureManager(this);
        }

        protected override PositionManager CreatePositionManager()
        {
            return new OgrePositionManager(this);
        }

        protected override void CreateSpots()
        {
            this.ogreAssetHelper.CreateAssetSpots();
        }

        public override void Destroy()
        {
            base.Destroy();
            this.ogreAssetHelper.Destroy();
        }

        #endregion

        #region Debug Members

        public void ShowWaypoints()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.ShowWaypoints();
        }

        public void ShowCameraSpots()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.ShowCameraSpots();
        }

        public void ShowItemSpots()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.ShowItemSpots();
        }

        public void HideCameraSpots()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.HideCameraSpots();
        }

        public void HideWaypoints()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.HideWaypoints();
        }

        public void HideItemSpots()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.HideItemSpots();
        }

        public void ShowAllDebuggingSpots()
        {
            this.ogreAssetHelper.ShowAllDebuggingSpots();
        }

        public void HideAllDebuggingSpots()
        {
            this.ogreAssetHelper.HideAllDebuggingSpots();
        }

        #endregion
    }
}