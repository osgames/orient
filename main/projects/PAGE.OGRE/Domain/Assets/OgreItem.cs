/* ****************************************************
 * Name: OgreItem.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using Mogre;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Management;
using PAGE.Generic.Math;
using PAGE.Generic.Resources.Assets;
using PAGE.OGRE.Domain.Cameras;
using PAGE.OGRE.Domain.Helpers;
using PAGE.OGRE.Management;
using PAGE.OGRE.Math;

namespace PAGE.OGRE.Domain.Assets
{
    public class OgreItem : GenericItem, IOgreEntityAsset
    {
        #region Fields

        //the item's ogre Entity
        protected Entity entity;
        protected OgreCamera camera;
        protected OgreAssetHelper ogreAssetHelper;
        protected OgreVisibilityManager visibilityManager;

        #endregion

        #region Properties

        protected new OgrePlaceableElementHelper ElementHelper
        {
            get { return base.ElementHelper as OgrePlaceableElementHelper; }
        }

        public Entity Entity
        {
            get
            {
                this.VerifyDestruction();
                return this.entity;
            }
        }

        public OgreVisibilityManager VisibilityManager
        {
            get { return this.visibilityManager; }
        }

        public new OgrePositionManager PositionManager
        {
            get { return base.PositionManager as OgrePositionManager; }
        }

        public SceneNode Node
        {
            get { return ((IOgreGraphicalElement) this.ElementHelper).Node; }
        }

        public override Distance Height
        {
            get
            {
                this.VerifyDestruction();
                if (this.entity != null)
                    return new OgreDistance(this.entity.BoundingBox.Maximum.y);
                return new OgreDistance(0);
            }
        }

        #endregion

        #region Constructors

        public OgreItem(
            ResourceItem resource, OgreCamera camera, Entity entity, bool init)
            : base(resource, false)
        {
            this.entity = entity;
            this.camera = camera;

            if (init) this.Init();
        }

        public OgreItem(
            ResourceItem resource, OgreCamera camera, Entity entity)
            : this(resource, camera, entity, true)
        {
        }

        #endregion

        #region Public Methods

        public virtual void FadeIn(float transitionTime)
        {
            this.VerifyDestruction();
            this.visibilityManager.FadeIn(transitionTime, true);
        }

        public virtual void FadeOut(float transitionTime)
        {
            this.VerifyDestruction();
            this.visibilityManager.FadeOut(transitionTime, true);
        }

        #endregion

        #region Override and Virtual Methods

        protected override void Init()
        {
            base.Init();

            this.ogreAssetHelper = this.CreateAssetHelper();
            this.ElementHelper.Node = this.entity.ParentSceneNode;
        }

        protected override void CreateManagers()
        {
            base.CreateManagers();

            this.visibilityManager = this.CreateVisibilityManager();
            if (this.visibilityManager != null) this.managers.Add(this.visibilityManager);
        }

        protected override PositionManager CreatePositionManager()
        {
            return new OgrePositionManager(this);
        }

        protected override TextManager CreateTextManager()
        {
            OgreTextManager newTextManager = new OgreTextManager(this, this.camera);
            if (newTextManager.MainText != null)
            {
                newTextManager.MainText.Color = this.ResourceAsset.Color;
                newTextManager.MainText.CharHeight = 28;
                newTextManager.MainText.RelativeTextPosition =
                    new OgrePosition(0, (float)(OgreDistance)this.Height, 0);
            }
            newTextManager.MainText.Update(0);
            return newTextManager;
        }

        /// <summary>
        /// Creates the manager to control the item's visibility 
        /// in the realizer.
        /// </summary>
        /// <returns>the OgreVisibilityManager associated to the item.</returns>
        protected virtual OgreVisibilityManager CreateVisibilityManager()
        {
            return new OgreVisibilityManager(this);
        }

        /// <summary>
        /// Creates a new object containing help code for this asset.
        /// </summary>
        /// <returns>the OgreAssetHelper to be used by this asset.</returns>
        protected virtual OgreAssetHelper CreateAssetHelper()
        {
            return new OgreAssetHelper(this);
        }

        protected override GraphicalElementHelper CreateElementHelper()
        {
            return new OgreItemHelper(this, null);
        }

        protected override void CreateSpots()
        {
            this.ogreAssetHelper.CreateAssetSpots();
        }

        public override void Destroy()
        {
            base.Destroy();
            this.ogreAssetHelper.Destroy();
        }

        #endregion

        #region Debug Members

        public void ShowWaypoints()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.ShowWaypoints();
        }

        public void ShowCameraSpots()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.ShowCameraSpots();
        }

        public void ShowItemSpots()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.ShowItemSpots();
        }

        public void HideCameraSpots()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.HideCameraSpots();
        }

        public void HideWaypoints()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.HideWaypoints();
        }

        public void HideItemSpots()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.HideItemSpots();
        }

        public void ShowAllDebuggingSpots()
        {
            this.ogreAssetHelper.ShowAllDebuggingSpots();
        }

        public void HideAllDebuggingSpots()
        {
            this.ogreAssetHelper.HideAllDebuggingSpots();
        }

        #endregion
    }
}