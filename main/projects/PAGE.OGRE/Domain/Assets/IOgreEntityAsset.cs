/* ****************************************************
 * Name: IOgreEntityAsset.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/08/28 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using Mogre;
using PAGE.OGRE.Management;

namespace PAGE.OGRE.Domain.Assets
{
    /// <summary>
    /// Represents an ogre interaction asset with an Entity representation 
    /// in the ogre graphics engine.
    /// </summary>
    public interface IOgreEntityAsset : IOgreInteractionAsset
    {
        Entity Entity { get;}

        /// <summary>
        /// Gets the visibility manager associated with this entity asset.
        /// </summary>
        OgreVisibilityManager VisibilityManager { get; }

        /// <summary>
        /// Fades the visibility of the asset, going from invisible to visible state
        /// in the given duration time. In the end the asset will be visible.
        /// </summary>
        /// <param name="transitionTime">the time in seconds to perform the transition
        /// between visibility states.</param>
        void FadeIn(float transitionTime);

        /// <summary>
        /// Fades the visibility of the asset, going from visble to invisible state
        /// in the given duration time. In the end the asset will be invisible.
        /// </summary>
        /// <param name="transitionTime">the time in seconds to perform the transition
        /// between visibility states.</param>
        void FadeOut(float transitionTime);
    }
}