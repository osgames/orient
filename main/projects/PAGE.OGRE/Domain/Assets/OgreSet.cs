/* ****************************************************
 * Name: OgreSet.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using Mogre;
using PAGE.Generic.Domain;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Management;
using PAGE.Generic.Math;
using PAGE.OGRE.Domain.Helpers;
using PAGE.OGRE.Management;
using PAGE.OGRE.Math;
using PAGE.OGRE.Resources.Assets;
using PAGE.OGRE.Util;
using PAGE.Util;

namespace PAGE.OGRE.Domain.Assets
{
    public class OgreSet : GenericSet, IOgreInteractionAsset
    {
        #region Fields

        //the height to test obstacles in path
        protected const int OPEN_PATH_TEST_HEIGHT = 6;

        //object names that make the set
        protected StringArrayList staticObjectNames = new StringArrayList();

        //osm file loader object containing ogre set information
        protected OSMLoader osmLoader;

        //the set's associated particle system
        protected ParticleSystem particleSystem;

        protected OgreAssetHelper ogreAssetHelper;

        #endregion

        #region Properties

        protected new OgreGraphicalElementHelper ElementHelper
        {
            get { return base.ElementHelper as OgreGraphicalElementHelper; }
        }

        /// <summary>
        /// Gets or set the particle system associated with the ogre set.
        /// </summary>
        public ParticleSystem ParticleSystem
        {
            get
            {
                this.VerifyDestruction();
                return this.particleSystem;
            }
            set
            {
                this.VerifyDestruction();
                this.particleSystem = value;
            }
        }

        /// <summary>
        /// Gets or set the set's visibility attribute. Shows/hides the skybox and
        /// particle system associated with the ogre set.
        /// </summary>
        public override bool Visible
        {
            get { return base.Visible; }
            set
            {
                this.VerifyDestruction();
                this.Node.SetVisible(value);
                if (this.ResourceAsset.SkyBox != "")
                    this.osmLoader.SceneManager.SetSkyBox(value, this.ResourceAsset.SkyBox);
                if (this.particleSystem != null)
                    this.particleSystem.Visible = value;
                base.Visible = value;
            }
        }

        /// <summary>
        /// Gets the OSM loader object associated with this set.
        /// </summary>
        public OSMLoader OSMLoader
        {
            get
            {
                this.VerifyDestruction();
                return osmLoader;
            }
        }

        public SceneNode Node
        {
            get { return ((IOgreGraphicalElement) this.ElementHelper).Node; }
        }

        public new OgreResourceSet ResourceAsset
        {
            get { return base.ResourceAsset as OgreResourceSet; }
        }

        public new OgrePositionManager PositionManager
        {
            get { return base.PositionManager as OgrePositionManager; }
        }

        #endregion

        #region Contructors

        public OgreSet(
            OgreResourceSet resource, OSMLoader loader, SceneNode rootNode, bool init)
            : base(resource, false)
        {
            if (loader == null)
            {
                throw new ArgumentNullException("loader", "Loader can't be null");
            }
            this.osmLoader = loader;

            if (init) this.Init();
            this.ElementHelper.Node = rootNode;

            //creates list of static entities names
            foreach (Entity ent in loader.Entities.Values)
            {
                this.staticObjectNames.Add(ent.Name);
            }
        }

        public OgreSet(OgreResourceSet resource, OSMLoader loader, SceneNode rootNode)
            : this(resource, loader, rootNode, true)
        {
        }

        #endregion

        #region Override and Virtual Methods

        protected override void Init()
        {
            base.Init();
            this.ogreAssetHelper = this.CreateAssetHelper();
        }

        protected override PositionManager CreatePositionManager()
        {
            return new OgrePositionManager(this);
        }

        protected override TextManager CreateTextManager()
        {
            //no text manager for sets
            return null;
        }

        /// <summary>
        /// Creates a new object containing help code for this asset.
        /// </summary>
        /// <returns>the OgreAssetHelper to be used by this asset.</returns>
        protected virtual OgreAssetHelper CreateAssetHelper()
        {
            return new OgreAssetHelper(this);
        }

        protected override GraphicalElementHelper CreateElementHelper()
        {
            return new OgreGraphicalElementHelper(this, null);
        }

        protected override void CreateSpots()
        {
            this.ogreAssetHelper.CreateAssetSpots();
        }

        public override bool OpenPath(
            Position source, Position target, List<IGraphicalElement> ignoreElements,
            bool allObjectsCount)
        {
            this.VerifyDestruction();

            if (!(source is OgrePosition) || !(target is OgrePosition))
                throw new ArgumentException("source and target must be OgrePositions");

            //creates list of ignoring entities names
            StringArrayList ignoreObjectNames = new StringArrayList();
            foreach (IOgreGraphicalElement elem in ignoreElements)
            {
                ignoreObjectNames.Add(elem.Node.Name);
            }

            Vector3 sourceVec = (OgrePosition) source;
            Vector3 targetVec = (OgrePosition) target;

            return
                this.OpenPath(sourceVec, targetVec,
                              ignoreObjectNames, allObjectsCount) &&
                this.OpenPath(targetVec, sourceVec,
                              ignoreObjectNames, allObjectsCount);
        }

        public override void Destroy()
        {
            base.Destroy();
            this.ogreAssetHelper.Destroy();
        }

        #endregion

        #region Private Methods

        protected bool OpenPath(Vector3 sourceVec, Vector3 targetVec,
            StringArrayList ignoreObjectNames, bool allObjectsCount)
        {
            this.VerifyDestruction();

            SceneManager sceneManager = this.OSMLoader.SceneManager;

            targetVec.y = sourceVec.y = OPEN_PATH_TEST_HEIGHT;
            Vector3 direction = targetVec - sourceVec;
            float distanceToTarget = direction.Normalise();

            Ray ray = new Ray(sourceVec, direction);
            RaySceneQuery query = sceneManager.CreateRayQuery(ray);
            if (query == null) return false;
            RaySceneQueryResult result = query.Execute();
            if (result == null) return false;

            //if the ray intersects something, then there is no open path
            foreach (RaySceneQueryResultEntry entry in result)
            {
                //there is an obstacle only if the distance is greater than 0
                //for a line that goes from source to target, and ignores 
                //objects contained in the list
                if (entry.movable.Visible &&
                    (entry.distance > 0) &&
                    (entry.distance < distanceToTarget) &&
                    (entry.movable.MovableType == "Entity") &&
                    !ignoreObjectNames.Contains(entry.movable.ParentSceneNode.Name) &&
                    (allObjectsCount || this.staticObjectNames.Contains(entry.movable.Name)))
                {
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region Debug Members

        public void ShowWaypoints()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.ShowWaypoints();
        }

        public void ShowCameraSpots()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.ShowCameraSpots();
        }

        public void ShowItemSpots()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.ShowItemSpots();
        }

        public void HideCameraSpots()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.HideCameraSpots();
        }

        public void HideWaypoints()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.HideWaypoints();
        }

        public void HideItemSpots()
        {
            this.VerifyDestruction();
            this.ogreAssetHelper.HideItemSpots();
        }

        public void ShowAllDebuggingSpots()
        {
            this.ogreAssetHelper.ShowAllDebuggingSpots();
        }

        public void HideAllDebuggingSpots()
        {
            this.ogreAssetHelper.HideAllDebuggingSpots();
        }

        #endregion
    }
}