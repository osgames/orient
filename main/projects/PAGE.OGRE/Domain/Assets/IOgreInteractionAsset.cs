/* ****************************************************
 * Name: IOgreInteractionAsset.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/22 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Domain.Assets;

namespace PAGE.OGRE.Domain.Assets
{
    public interface IOgreInteractionAsset : IOgreGraphicalElement, IInteractionAsset
    {
        void ShowWaypoints();
        void ShowCameraSpots();
        void ShowItemSpots();

        void HideCameraSpots();
        void HideWaypoints();
        void HideItemSpots();
        
        void ShowAllDebuggingSpots();
        void HideAllDebuggingSpots();
    }
}