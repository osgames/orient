/* ****************************************************
 * Name: GraphicsConfig.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/06/22 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;

namespace PAGE.OGRE.Resources
{
    /// <summary>
    /// Contains the graphical options for a the Ogre realizer.
    /// </summary>
    public class GraphicsConfig : IXmlSerializable
    {
        #region Fields

        //graphical options
        protected OgreColor backgroundColor = new OgreColor(0, 0, 0);

        protected bool lights = true;
        protected OgreColor ambientColor = new OgreColor(0.5f, 0.5f, 0.5f);

        protected bool shadows = true;
        protected OgreColor shadowsColor = new OgreColor(0.5f, 0.5f, 0.5f, 0.5f);

        #endregion

        #region Events

        public event EventHandler BackgroundColorChanged;
        public event EventHandler AmbientColorChanged;
        public event EventHandler LightsChanged;
        public event EventHandler ShadowsColorChanged;
        public event EventHandler ShadowsChanged;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the OGRE realizer background color.
        /// </summary>
        public OgreColor BackgroundColor
        {
            get { return backgroundColor; }
            set
            {
                backgroundColor = value;
                if (this.BackgroundColorChanged != null)
                    this.BackgroundColorChanged(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets or sets the color of the shadows in the realizer.
        /// </summary>
        public OgreColor ShadowsColor
        {
            get { return shadowsColor; }
            set
            {
                shadowsColor = value;
                if (this.ShadowsColorChanged != null)
                    this.ShadowsColorChanged(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets or sets the realizer's ambient color.
        /// </summary>
        public OgreColor AmbientColor
        {
            get { return ambientColor; }
            set
            {
                ambientColor = value;
                if (this.AmbientColorChanged!= null)
                    this.AmbientColorChanged(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating if the shadows are active.
        /// </summary>
        public bool Shadows
        {
            get { return shadows; }
            set
            {
                shadows = value;
                if (this.ShadowsChanged != null)
                    this.ShadowsChanged(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating if the lights are active.
        /// </summary>
        public bool Lights
        {
            get { return lights; }
            set
            {
                lights = value;
                if (this.LightsChanged != null)
                    this.LightsChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Serializable Methods

        /// <summary>
        /// Saves (serializes) the resource object to the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name to save the object info into.</param>
        public virtual void SaveToXml(string xmlFileName)
        {
            this.SaveToXml(xmlFileName, "graphics-config");
        }

        /// <summary>
        /// Saves (serializes) the resource object to the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name to save the object info into.</param>
        /// <param name="rootName">the name for the root XML element.</param>
        public void SaveToXml(string xmlFileName, string rootName)
        {
            try
            {
                //creates xml document
                XmlDocument xd = new XmlDocument();

                //root element
                XmlElement rootElement = xd.CreateElement(rootName);
                xd.CreateXmlDeclaration("1.0", Encoding.UTF8.EncodingName, "");
                xd.AppendChild(rootElement);

                //write contents
                this.WriteXml(rootElement);

                //verifies file existence
                if (File.Exists(xmlFileName)) File.Delete(xmlFileName);

                //save the document to file
                StreamWriter writer = new StreamWriter(xmlFileName, false, Encoding.UTF8);
                xd.Save(writer);
                writer.Close();
            }
            catch (XmlException)    //do nothing...
            {
            }
            catch (IOException)    //do nothing...
            {
            }
            catch (InvalidOperationException)    //do nothing...
            {
            }
        }

        /// <summary>
        /// Loads (deserializes) the resource object from the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name containing the object info.</param>
        /// <returns>the ResourceAsset loaded from the XML file.</returns>
        public virtual void LoadFromXml(string xmlFileName)
        {
            this.LoadFromXml(xmlFileName, "graphics-config");
        }

        /// <summary>
        /// Loads (deserializes) the resource object from the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name containing the object info.</param>
        /// <param name="rootName">the name for the root XML element.</param>
        public void LoadFromXml(string xmlFileName, string rootName)
        {
            if (!File.Exists(xmlFileName)) return;

            XmlDocument xd = new XmlDocument();
            try
            {
                xd.Load(xmlFileName);

                //loads root element
                XmlElement rootElement = (XmlElement)xd.SelectSingleNode(rootName);
                if (rootElement == null) return;

                this.ReadXml(rootElement);
            }
            catch (XmlException)      //do nothing...
            {
            }
            catch (XPathException)    //do nothing...
            {
            }
        }

        public virtual void ReadXml(XmlElement element)
        {
            XmlElement childElement = (XmlElement)element.SelectSingleNode("BackgroundColor");
            if (childElement != null) this.backgroundColor.ReadXml(childElement);

            childElement = (XmlElement)element.SelectSingleNode("ShadowsColor");
            if (childElement != null) this.shadowsColor.ReadXml(childElement);

            childElement = (XmlElement)element.SelectSingleNode("AmbientColor");
            if (childElement != null) this.ambientColor.ReadXml(childElement);

            childElement = (XmlElement)element.SelectSingleNode("Shadows");
            if (childElement != null) 
                this.shadows = Convert.ToBoolean(childElement.InnerXml, CultureInfo.InvariantCulture);

            childElement = (XmlElement)element.SelectSingleNode("Lights");
            if (childElement != null)
                this.lights = Convert.ToBoolean(childElement.InnerXml, CultureInfo.InvariantCulture);
        }

        public virtual void WriteXml(XmlElement element)
        {
            XmlElement childElement = element.OwnerDocument.CreateElement("BackgroundColor");
            this.backgroundColor.WriteXml(childElement);
            element.AppendChild(childElement);

            childElement = element.OwnerDocument.CreateElement("ShadowsColor");
            this.shadowsColor.WriteXml(childElement);
            element.AppendChild(childElement);

            childElement = element.OwnerDocument.CreateElement("AmbientColor");
            this.ambientColor.WriteXml(childElement);
            element.AppendChild(childElement);

            childElement = element.OwnerDocument.CreateElement("Shadows");
            childElement.InnerXml = Convert.ToString(this.shadows, CultureInfo.InvariantCulture);
            element.AppendChild(childElement);

            childElement = element.OwnerDocument.CreateElement("Lights");
            childElement.InnerXml = Convert.ToString(this.lights, CultureInfo.InvariantCulture);
            element.AppendChild(childElement);
        }

        #endregion
    }
}