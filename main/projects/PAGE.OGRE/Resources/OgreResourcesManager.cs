/* ****************************************************
 * Name: OgreResourcesManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/26 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Resources;

namespace PAGE.OGRE.Resources
{
    public class OgreResourcesManager : ResourcesManager
    {
        #region Properties

        public new OgreResourcesConfig ResourcesConfig
        {
            get { return base.ResourcesConfig as OgreResourcesConfig; }
        }

        public new OgreResourcesFactory ResourcesFactory
        {
            get { return base.ResourcesFactory as OgreResourcesFactory; }
        }

        #endregion

        #region Constructors

        public OgreResourcesManager(string configFileName)
            : base(configFileName, new OgreResourcesFactory())
        {
        }

        public OgreResourcesManager(
            string configFileName, OgreResourcesFactory resourcesFactory) 
            : base(configFileName, resourcesFactory)
        {
        }

        #endregion

        #region Override Methods

        public override void LoadAll()
        {
            //loads configuration
            this.resourcesConfig = this.ResourcesFactory.CreateNewResourcesConfig();
            this.resourcesConfig.LoadFromXml(configFileName);

            //loads elements by correct order
            this.LoadCharacters(this.ResourcesConfig.CharactersPath);
            this.LoadItems(this.ResourcesConfig.ItemsPath);
            this.LoadSets(this.ResourcesConfig.SetsPath);
        }

        #endregion
    }
}