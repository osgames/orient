/* ****************************************************
 * Name: OgreResourcesFactory.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/23
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Generic;
using PAGE.Generic.Math;
using PAGE.Generic.Navigation;
using PAGE.Generic.Resources;
using PAGE.Generic.Resources.Actions;
using PAGE.Generic.Resources.Assets;
using PAGE.OGRE.Math;
using PAGE.OGRE.Resources.Actions;
using PAGE.OGRE.Resources.Assets;

namespace PAGE.OGRE.Resources
{
    public class OgreResourcesFactory : ResourcesFactory
    {
        #region Public Methods

        public override ResourceSet CreateNewSet(string setID)
        {
            return new OgreResourceSet(setID, this);
        }

        public override Scale CreateNewScale()
        {
            return new OgreScale();
        }

        public override Position CreateNewPosition()
        {
            return new OgrePosition();
        }

        public override Direction CreateNewDirection()
        {
            return new OgreDirection();
        }

        public override Orientation CreateNewOrientation()
        {
            return new OgreOrientation();
        }

        public override Color CreateNewColor()
        {
            return new OgreColor();
        }

        public override ResourcesConfig CreateNewResourcesConfig()
        {
            return new OgreResourcesConfig();
        }

        public override ActionList CreateNewActionList()
        {
            return new OgreActionList();
        }

        public override VertexCostPair CreateNewVertexCostPair()
        {
            VertexCostPair vertexCost = new VertexCostPair();
            vertexCost.Cost = new OgreDistance();
            return vertexCost;
        }

        #endregion
    }
}