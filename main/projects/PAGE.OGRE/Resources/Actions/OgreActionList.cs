/* ****************************************************
 * Name: OgreActionList.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/16 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using PAGE.Generic.Resources.Actions;

namespace PAGE.OGRE.Resources.Actions
{
    /// <summary>
    /// Represents a list of ResourceActions with indexing (by action id) functionality.
    /// </summary>
    [Serializable]
    public class OgreActionList : ActionList
    {
        protected override ResourceAction GetTypeResourceAction(string type)
        {
            ResourceAction action = base.GetTypeResourceAction(type);
            switch (type)
            {
                case "walk-to":     return new OgreWalkToAction();
            }
            return action;
        }
    }
}