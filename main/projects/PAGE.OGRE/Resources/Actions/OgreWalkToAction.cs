/* ****************************************************
 * Name: OgreWalkToAction.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/16
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Generic.Resources.Actions;
using PAGE.OGRE.Math;

namespace PAGE.OGRE.Resources.Actions
{
    /// <summary>
    /// Represents an action whose animation is used when a walking character moves 
    /// to some place.
    /// </summary>
    public class OgreWalkToAction : WalkToAction
    {
        #region Constructors

        public OgreWalkToAction()
            : this("walk-action0", "anim", new OgreDistance(0))
        {
        }

        public OgreWalkToAction(string idToken, string animationName, OgreDistance walkStepDist)
            : base(idToken, animationName, walkStepDist)
        {
        } 

        #endregion
    }
}