/* ****************************************************
 * Name: OgreResourcesConfig.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Xml;
using PAGE.Generic.Resources;

namespace PAGE.OGRE.Resources
{
    /// <summary>
    /// Contains information concerning all the realizer resources and files.
    /// </summary>
    public class OgreResourcesConfig : ResourcesConfig
    {
        #region Fields

        //resource files
        protected string graphicsXMLFile = "graphics.xml";

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the path of the graphical config file.
        /// </summary>
        public string GraphicsXMLFile
        {
            get { return graphicsXMLFile; }
            set { graphicsXMLFile = value; }
        }

        #endregion

        #region Serializable Methods

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            XmlElement childElement = (XmlElement)element.SelectSingleNode("GraphicsXMLFile");
            if (childElement != null) this.graphicsXMLFile = childElement.InnerXml;
        }

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            XmlElement childElement = element.OwnerDocument.CreateElement("GraphicsXMLFile");
            childElement.InnerXml = this.graphicsXMLFile;
            element.AppendChild(childElement);
        }

        #endregion
    }
}