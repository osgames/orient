/* ****************************************************
 * Name: OgreResourceSet.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/16 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Xml;
using PAGE.Generic.Resources.Assets;

namespace PAGE.OGRE.Resources.Assets
{
    /// <summary>
    /// Contains the resource information for a walkable set.
    /// Includes particle system and skybox info.
    /// </summary>
    public class OgreResourceSet: ResourceSet
    {
        #region Fields

        protected string particleSystem = "";
        protected string skyBox = "";

        #endregion 

        #region Properties

        /// <summary>
        /// Gets or sets the name of the particle system associated with 
        /// the resource set.
        /// </summary>
        public string ParticleSystem
        {
            get { return particleSystem; }
            set { particleSystem = value; }
        }

        /// <summary>
        /// Gets or sets the name of the skybox associated with 
        /// the resource set.
        /// </summary>
        public string SkyBox
        {
            get { return skyBox; }
            set { skyBox = value; }
        }

        #endregion

        #region Constructors

        public OgreResourceSet(string idToken, OgreResourcesFactory resourcesFactory)
            : base(idToken, resourcesFactory)
        {
        } 

        #endregion

        #region Serialization Methods

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            //writes set attributes
            element.SetAttribute("particle-system", this.particleSystem);
            element.SetAttribute("skybox", this.skyBox);
        }

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            //reads set attributes
            this.particleSystem = element.GetAttribute("particle-system");
            this.skyBox = element.GetAttribute("skybox");
        }

        #endregion
    }
}