/* ****************************************************
 * Name: OgreApplication.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Mogre;
using MOIS;
using Type=MOIS.Type;
using Vector3=Mogre.Vector3;

namespace PAGE.OGRE
{
    public class NewFrameEventArgs : EventArgs
    {
        public float MillisSinceLastUpdate;

        public NewFrameEventArgs(float millisSinceLastUpdate)
        {
            this.MillisSinceLastUpdate = millisSinceLastUpdate;
        }
    }

    public class WindowResizedEventArgs : EventArgs
    {
        public uint Width, Height;

        public WindowResizedEventArgs(uint widht, uint height)
        {
            this.Width = widht;
            this.Height = height;
        }
    }

    /// <summary>
    /// This class provides methods to manage the OGRE's rendering
    /// engine through MOGRE.
    /// </summary>
    public class OgreApplication : IDisposable
    {
        #region Events

        /// <summary>
        /// Window resized event (only works in win forms mode).
        /// </summary>
        public event EventHandler<WindowResizedEventArgs> WindowResized;

        /// <summary>
        /// New frame event. Called when ogre engine renders one frame.
        /// </summary>
        public event EventHandler<NewFrameEventArgs> NewFrame;

        #endregion

        #region Fields

        //config variables
        protected uint configScreenWidth;
        protected uint configScreenHeight;
        protected bool configFullscreen;

        //app variables
        protected ColourValue backgroundColor = new ColourValue(0, 0, 1);
        protected bool shutdown;
        protected bool setup;
        protected float millisSinceLastFrame;

        //app elements
        protected Control control;
        protected Root root;
        protected RenderWindow renderWindow;
        protected SceneManager sceneManager;
        protected Camera camera;
        protected Viewport viewport;
        protected Keyboard inputKeyboard;
        protected Mouse inputMouse;

        //stats overlay
        protected Overlay statsOverlay;
        protected OverlayElement averageFPS;
        protected OverlayElement bestFPS;
        protected OverlayElement worstFPS;
        protected OverlayElement currentFPS;
        protected OverlayElement triangleCount;

        //debug overlay
        protected OverlayContainer panel;
        protected Overlay fourLineDebugOverlay;
        protected TextAreaOverlayElement oDebugText1;
        protected TextAreaOverlayElement oDebugText2;
        protected TextAreaOverlayElement oDebugText3;
        protected TextAreaOverlayElement oDebugText4;

        //constants
        protected const string OGRE_CONFIG_FILE = "ogre.cfg";
        protected const string OGRE_RESOURCES_FILE = "resources.cfg";
        protected const TextureFilterOptions TEX_FILTERING = TextureFilterOptions.TFO_BILINEAR;
        protected const UInt32 ANISOTROPY = 1;
        protected const int NUM_MIPMAPS = 5;
        protected const int FSAA = 4;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the ogre root element.
        /// </summary>
        public Root Root
        {
            get { return this.root; }
        }

        /// <summary>
        /// Gets ogre scene manager.
        /// </summary>
        public SceneManager SceneManager
        {
            get { return this.sceneManager; }
        }

        /// <summary>
        /// Gets ogre render window.
        /// </summary>
        public RenderWindow RenderWindow
        {
            get { return this.renderWindow; }
        }

        /// <summary>
        /// Gets ogre viewport.
        /// </summary>
        public Viewport Viewport
        {
            get { return this.viewport; }
        }

        /// <summary>
        /// Gets the default application camera.
        /// </summary>
        public Camera Camera
        {
            get { return this.camera; }
        }

        /// <summary>
        /// Gets the milliseconds occured since last frame was rendered.
        /// </summary>
        public float MillisSinceLastFrame
        {
            get { return this.millisSinceLastFrame; }
        }

        /// <summary>
        /// Gets or sets the render window width.
        /// </summary>
        public uint WindowWidth
        {
            get { return this.renderWindow.Width; }
            set { this.Resize(value, this.WindowHeight); }
        }

        /// <summary>
        /// Gets or sets the render window height.
        /// </summary>
        public uint WindowHeight
        {
            get { return this.renderWindow.Height; }
            set { this.Resize(this.WindowWidth, value); }
        }

        /// <summary>
        /// Gets or sets fullscreen mode, only works if window 
        /// was self-created (not win forms).
        /// </summary>
        public bool Fullscreen
        {
            get
            {
                if (this.renderWindow != null)
                    return this.renderWindow.IsFullScreen;
                return false;
            }
            set
            {
                if (this.renderWindow != null)
                    this.renderWindow.SetFullscreen(value, this.WindowWidth, this.WindowHeight);
            }
        }

        /// <summary>
        /// Gets or sets the viewport's background color.
        /// </summary>
        public ColourValue BackgroundColor
        {
            get { return this.backgroundColor; }
            set
            {
                this.backgroundColor = value;
                this.camera.Viewport.BackgroundColour = value;
            }
        }

        /// <summary>
        /// Gets the keyboard object attached to the app.
        /// </summary>
        public Keyboard Keyboard
        {
            get { return inputKeyboard; }
        }

        /// <summary>
        /// Gets the mouse object attached to the app.
        /// </summary>
        public Mouse Mouse
        {
            get { return inputMouse; }
        }

        /// <summary>
        /// Gets the win forms control created by the ogre app.
        /// </summary>
        /// <remarks>Returns null if render window was self-created.</remarks>
        public Control Control
        {
            get { return control; }
        }

        /// <summary>
        /// Gets the configuration file screen width.
        /// </summary>
        public uint ConfigScreenWidth
        {
            get { return configScreenWidth; }
        }

        /// <summary>
        /// Gets the configuration file screen height.
        /// </summary>
        public uint ConfigScreenHeight
        {
            get { return configScreenHeight; }
        }

        /// <summary>
        /// Gets the configuration for fullscreen mode.
        /// </summary>
        public bool ConfigFullscreen
        {
            get { return configFullscreen; }
        }

        #endregion

        #region Public Methods

        #region Application Handling

        /// <summary>
        /// Updates the OGRE engine in one frame.
        /// </summary>
        /// <returns>true if successful render, false otherwise.</returns>
        public virtual bool UpdateRenderer()
        {
            if (this.renderWindow.IsClosed) return false;

            //renders one frame
            this.root.RenderOneFrame();
            this.renderWindow.Update();

            //pump messages in all registered RenderWindow windows
            WindowEventUtilities.MessagePump();

            return !this.shutdown;
        }

        /// <summary>
        /// Starts rendering OGRE engine which enters in a infinite cycle.
        /// </summary>
        /// <remarks>The function does not return unless the OGRE engine is 
        /// shutdown by some behavior.</remarks>
        public void StartRender()
        {
            if (!this.setup) this.Setup(null);
            this.root.StartRendering();
        }

        /// <summary>
        /// Setups the OGRE engine, loading resources and creating 
        /// scene manager, camera, viewport, etc.
        /// </summary>
        public bool Setup()
        {
            return this.Setup(null);
        }

        /// <summary>
        /// Setups the OGRE engine, loading resources and creating 
        /// scene manager, camera, viewport, etc.
        /// </summary>
        /// <param name="winControl">the Windows Form Control to render the scene. 
        /// If null a new window will be created.</param>
        /// <remarks>Loads resources according the default ogre resources
        /// file (resources.cfg).</remarks>
        public bool Setup(Control winControl)
        {
            return this.Setup(winControl, OGRE_RESOURCES_FILE);
        }

        /// <summary>
        /// Setups the OGRE engine, loading resources and creating 
        /// scene manager, camera, viewport, etc.
        /// </summary>
        /// <param name="winControl">the Windows Form Control to render the scene. 
        /// If null a new window will be created.</param>
        /// <param name="ogreResourcesFile">The path to the ogre resources config file.</param>
        /// <returns>True if successful setup, false otherwise.</returns>
        public virtual bool Setup(
            Control winControl, string ogreResourcesFile)
        {
            this.root = new Root();
            this.control = winControl;

            this.SetupResources(ogreResourcesFile);

            //restore/open ogre render config
            if (!this.root.RestoreConfig() && !this.root.ShowConfigDialog()) return false;

            if (winControl != null)
            {
                //sets the ogre render window in the win form control
                this.root.Initialise(false);
                NameValuePairList misc = new NameValuePairList();
                misc["externalWindowHandle"] = winControl.Handle.ToString();
                misc["FSAA"] = FSAA.ToString();
                this.renderWindow = this.root.CreateRenderWindow("Render Window", 0, 0, false, misc);
                winControl.SizeChanged += Control_SizeChanged;
            }
            else
            {
                //creates a new window
                this.renderWindow = this.root.Initialise(true, "Render Window");

                //sets up input
                this.CreateInput();
            }

            //gets config parameters
            ConfigOptionMap configOptions = this.root.RenderSystem.GetConfigOptions();
            string videoMode = configOptions["Video Mode"].currentValue;
            string fullscreen = configOptions["Full Screen"].currentValue;
            string[] parameters = videoMode.Split(new char[] {' ', 'x', 'X'}, StringSplitOptions.RemoveEmptyEntries);
            this.configScreenWidth = uint.Parse(parameters[0]);
            this.configScreenHeight = uint.Parse(parameters[1]);
            this.configFullscreen = fullscreen.ToLower() == "yes" ? true : false;

            //sets default parameters
            ResourceGroupManager.Singleton.InitialiseAllResourceGroups();
            TextureManager.Singleton.DefaultNumMipmaps = NUM_MIPMAPS;
            MaterialManager.Singleton.SetDefaultTextureFiltering(TEX_FILTERING);
            MaterialManager.Singleton.DefaultAnisotropy = ANISOTROPY;

            //creates app elements
            this.CreateSceneManager();
            this.CreateCamera();
            this.CreateViewPort();
            this.CreateStatsOverlay();
            this.Create4LineDebugOverLay();

            this.root.FrameStarted += new FrameListener.FrameStartedHandler(FrameStarted);
            this.setup = true;

            return true;
        }

        /// <summary>
        /// Called when the control window is resized.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Control_SizeChanged(object sender, EventArgs e)
        {
            //updates RenderWindow size
            Size s = this.control.ClientSize;
            this.ResizeRenderWindow((uint) s.Width, (uint) s.Height);
        }

        protected void ResizeRenderWindow(uint width, uint height)
        {
            //updates RenderWindow size
            this.renderWindow.Resize(width, height);
            this.renderWindow.WindowMovedOrResized();

            //updates aspect ratio (must have float cast)
            this.camera.AspectRatio =
                this.viewport.ActualWidth/(float) this.viewport.ActualHeight;

            //raises window resized event
            if (this.WindowResized != null)
                this.WindowResized(this,
                                   new WindowResizedEventArgs(this.renderWindow.Width, this.renderWindow.Height));
        }

        /// <summary>
        /// Changes the size of the render window if in windowed mode, 
        /// or the screen resolution if in fullscreen mode.
        /// </summary>
        /// <param name="width">window width</param>
        /// <param name="height">window height</param>
        public void Resize(uint width, uint height)
        {
            if (this.control != null)
            {
                //if the window is a control, change its size
                //render window will be resized through resize event
                this.control.Width = (int) width;
                this.control.Height = (int) height;
                this.control.Refresh();
            }
            else
            {
                //resize render window only
                this.ResizeRenderWindow(width, height);
            }
        }

        /// <summary>
        /// Disposes root and clears scene.
        /// </summary>
        public void Dispose()
        {
            this.sceneManager.ClearScene();

            this.panel = null;
            this.fourLineDebugOverlay = null;
            this.oDebugText1 = null;
            this.oDebugText2 = null;
            this.oDebugText3 = null;
            this.oDebugText4 = null;

            this.renderWindow = null;
            this.camera = null;
            this.viewport = null;
            this.statsOverlay = null;
            this.averageFPS = null;
            this.bestFPS = null;
            this.worstFPS = null;
            this.currentFPS = null;
            this.triangleCount = null;

            this.sceneManager = null;

            this.root.Dispose();
        }

        #endregion

        #region Debug Methods

        /// <summary>
        /// Creates an Overlay with 4 text lines at the top left corner.
        /// </summary>
        protected void Create4LineDebugOverLay()
        {
            // Create a panel
            this.panel = (OverlayContainer) OverlayManager.Singleton.CreateOverlayElement("Panel", "DebugStatus/Panel");
            this.panel.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            this.panel.SetPosition(10, 10);
            this.panel.SetDimensions(500, 100);

            // Create a text area
            this.oDebugText1 =
                (TextAreaOverlayElement)
                OverlayManager.Singleton.CreateOverlayElement("TextArea", "DebugStatus/DebugText1");
            this.panel.AddChild(this.oDebugText1);
            this.oDebugText1.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            this.oDebugText1.SetPosition(0, 0);
            this.oDebugText1.SetDimensions(100, 100);
            this.oDebugText1.Caption = "";
            this.oDebugText1.CharHeight = 16;
            this.oDebugText1.FontName = "BlueHighway";
            this.oDebugText1.ColourBottom = new ColourValue(0.3f, 0.5f, 0.3f);
            this.oDebugText1.ColourTop = new ColourValue(0.5f, 0.7f, 0.5f);


            // Create a text area
            this.oDebugText2 =
                (TextAreaOverlayElement)
                OverlayManager.Singleton.CreateOverlayElement("TextArea", "DebugStatus/DebugText2");
            this.panel.AddChild(this.oDebugText2);
            this.oDebugText2.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            this.oDebugText2.SetPosition(0, 20);
            this.oDebugText2.SetDimensions(100, 100);
            this.oDebugText2.Caption = "";
            this.oDebugText2.CharHeight = 16;
            this.oDebugText2.FontName = "BlueHighway";
            this.oDebugText2.ColourBottom = new ColourValue(0.3f, 0.5f, 0.3f);
            this.oDebugText2.ColourTop = new ColourValue(0.5f, 0.7f, 0.5f);


            // Create a text area
            this.oDebugText3 =
                (TextAreaOverlayElement)
                OverlayManager.Singleton.CreateOverlayElement("TextArea", "DebugStatus/DebugText3");
            this.panel.AddChild(this.oDebugText3);
            this.oDebugText3.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            this.oDebugText3.SetPosition(0, 40);
            this.oDebugText3.SetDimensions(100, 100);
            this.oDebugText3.Caption = "";
            this.oDebugText3.CharHeight = 16;
            this.oDebugText3.FontName = "BlueHighway";
            this.oDebugText3.ColourBottom = new ColourValue(0.3f, 0.5f, 0.3f);
            this.oDebugText3.ColourTop = new ColourValue(0.5f, 0.7f, 0.5f);


            // Create a text area
            this.oDebugText4 =
                (TextAreaOverlayElement)
                OverlayManager.Singleton.CreateOverlayElement("TextArea", "DebugStatus/DebugText4");
            this.panel.AddChild(this.oDebugText4);
            this.oDebugText4.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            this.oDebugText4.SetPosition(0, 60);
            this.oDebugText4.SetDimensions(100, 100);
            this.oDebugText4.Caption = "";
            this.oDebugText4.CharHeight = 16;
            this.oDebugText4.FontName = "BlueHighway";
            this.oDebugText4.ColourBottom = new ColourValue(0.3f, 0.5f, 0.3f);
            this.oDebugText4.ColourTop = new ColourValue(0.5f, 0.7f, 0.5f);

            // Create an overlay, and add the panel
            this.fourLineDebugOverlay = OverlayManager.Singleton.Create("Status/Overlay");
            this.fourLineDebugOverlay.Add2D(this.panel);
        }

        /// <summary>
        /// Shows the 4-line debug overlay.
        /// </summary>
        public void Show4LineDebugOverLay()
        {
            if (this.fourLineDebugOverlay != null)
                this.fourLineDebugOverlay.Show();
        }

        /// <summary>
        /// Hides the 4-line debug overlay.
        /// </summary>
        public void Hide4LineDebugOverLay()
        {
            if (this.fourLineDebugOverlay != null)
                this.fourLineDebugOverlay.Hide();
        }

        /// <summary>
        /// Sets debug text in the 4-line debug overlay.
        /// </summary>
        /// <param name="line">The line to set text.</param>
        /// <param name="text">The text to display on the line</param>
        public void SetDebugCaption(int line, string text)
        {
            switch (line)
            {
                case 0:
                    if (oDebugText1 != null)
                        oDebugText1.Caption = text;
                    break;
                case 1:
                    if (oDebugText2 != null)
                        oDebugText2.Caption = text;
                    break;
                case 2:
                    if (oDebugText3 != null)
                        oDebugText3.Caption = text;
                    break;
                case 3:
                    if (oDebugText4 != null)
                        oDebugText4.Caption = text;
                    break;
            }
        }

        /// <summary>
        /// Shows the graphic stats overlay.
        /// </summary>
        public void ShowStatsOverlay()
        {
            this.statsOverlay.Show();
        }

        /// <summary>
        /// Hides the graphic stats overlay.
        /// </summary>
        public void HideStatsOverlay()
        {
            this.statsOverlay.Hide();
        }

        /// <summary>
        /// Sets all overlays visible.
        /// </summary>
        public void ShowAllOverlays()
        {
            this.ShowStatsOverlay();
            this.Show4LineDebugOverLay();
        }

        /// <summary>
        /// Hides all overlays.
        /// </summary>
        public void HideAllOverlays()
        {
            this.HideStatsOverlay();
            this.Hide4LineDebugOverLay();
        }

        #endregion

        /// <summary>
        /// Throws an exception with the information about an ogre engine
        /// exception if one ocurred.
        /// </summary>
        /// <exception cref="ApplicationException">If ogre engine threw an
        /// exception, an ApplicationException is thrown containing the 
        /// exception information.</exception>
        public void ThrowOgreException()
        {
            if (OgreException.IsThrown)
                throw new ApplicationException("Ogre Exception: " + OgreException.LastException.FullDescription);
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Called in Setup when Resources should be added to ResourceGroupManager
        /// Method which will define the source of resources (other than current folder)
        /// </summary>
        protected void SetupResources(string sFileName)
        {
            if (!File.Exists(sFileName)) return;

            //Initialiser.SetupResources(sFileName);
            using (StreamReader sr = new StreamReader(sFileName))
            {
                string secName = "";
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    int x = line.IndexOf("#");
                    if (x > -1)
                        line = line.Substring(0, x);
                    line = line.Trim();
                    if (line.Length > 0)
                    {
                        if (line[0] == '[')
                        {
                            secName = line.Substring(1, line.Length - 2);
                        }
                        else if (secName.Length > 0)
                        {
                            x = line.IndexOf("=");
                            if (x <= 0)
                                throw new Exception("Invalid line in resource file " + sFileName);
                            string sLocType = line.Substring(0, x);
                            string sarchName = line.Substring(x + 1);
                            ResourceGroupManager.Singleton.AddResourceLocation(sarchName, sLocType, secName);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates the scene manager.
        /// </summary>
        protected void CreateSceneManager()
        {
            this.sceneManager = this.root.CreateSceneManager(SceneType.ST_GENERIC);
        }

        /// <summary>
        /// Called when the default camera needs to be created.
        /// </summary>
        protected void CreateCamera()
        {
            this.camera = this.sceneManager.CreateCamera("MainCamera");
            this.camera.SetPosition(0, 0, -100);
            this.camera.LookAt(new Vector3(0, 0, 0));
            this.camera.NearClipDistance = 5;
        }

        /// <summary>
        /// Called when the default ViewPort needs to be created.
        /// </summary>
        protected void CreateViewPort()
        {
            this.renderWindow.RemoveViewport(0);
            this.viewport = this.renderWindow.AddViewport(this.camera);
            this.viewport.BackgroundColour = this.backgroundColor;
            //must have float cast
            this.camera.AspectRatio = this.viewport.ActualWidth/(float) this.viewport.ActualHeight;
        }

        /// <summary>
        /// Called when a new frame is rendered.
        /// </summary>
        /// <param name="e">FrameEvent from Ogre engine.</param>
        /// <returns>False if shutdown was requested or window is closed, 
        /// true otherwise.</returns>
        protected bool FrameStarted(FrameEvent e)
        {
            if (this.renderWindow.IsClosed || this.shutdown) return false;

            this.millisSinceLastFrame = e.timeSinceLastFrame*1000;

            //updates stats
            if (this.statsOverlay.IsVisible) this.UpdateStatsOverlay();

            //sends new frame event
            if (this.NewFrame != null) this.NewFrame(this, new NewFrameEventArgs(this.millisSinceLastFrame));

            //only handle input if window was self-created
            if (this.control == null) this.HandleInput(e);

            return true;
        }

        /// <summary>
        /// Creates the input capture system, only works if in winforms mode.
        /// </summary>
        protected virtual void CreateInput()
        {
            //creates input manager
            IntPtr windowHnd;
            this.renderWindow.GetCustomAttribute("WINDOW", out windowHnd);
            ParamList pl = new ParamList();
            pl.Insert("WINDOW", windowHnd.ToString());

            InputManager inputManager = InputManager.CreateInputSystem(pl);

            //create devices
            this.inputKeyboard = (Keyboard) inputManager.CreateInputObject(Type.OISKeyboard, false);
            this.inputMouse = (Mouse) inputManager.CreateInputObject(Type.OISMouse, false);
        }

        /// <summary>
        /// Handles mouse and keyboard events from the input system.
        /// </summary>
        /// <param name="evt">Ogre's frame event.</param>
        protected virtual void HandleInput(FrameEvent evt)
        {
            if (this.inputKeyboard != null) this.inputKeyboard.Capture();
            if (this.inputMouse != null) this.inputMouse.Capture();
        }

        /// <summary>
        /// Creates the render stats overlay.
        /// </summary>
        protected virtual void CreateStatsOverlay()
        {
            this.statsOverlay = OverlayManager.Singleton.GetByName("Core/DebugOverlay");
            this.averageFPS = OverlayManager.Singleton.GetOverlayElement("Core/AverageFps");
            this.currentFPS = OverlayManager.Singleton.GetOverlayElement("Core/CurrFps");
            this.bestFPS = OverlayManager.Singleton.GetOverlayElement("Core/BestFps");
            this.worstFPS = OverlayManager.Singleton.GetOverlayElement("Core/WorstFps");
            this.triangleCount = OverlayManager.Singleton.GetOverlayElement("Core/NumTris");
        }


        /// <summary>
        /// Updates the debug overlay with the current frame rate statistics and triangle count.
        /// </summary>
        protected void UpdateStatsOverlay()
        {
            this.currentFPS.Caption = "Current FPS: " + this.renderWindow.LastFPS;
            this.averageFPS.Caption = "Average FPS: " + this.renderWindow.AverageFPS;
            this.bestFPS.Caption = "Best FPS: " + this.renderWindow.BestFPS;
            this.worstFPS.Caption = "Worst FPS: " + this.renderWindow.WorstFPS;
            this.triangleCount.Caption = "Triangle Count: " + this.renderWindow.TriangleCount;
        }

        #endregion
    }
}