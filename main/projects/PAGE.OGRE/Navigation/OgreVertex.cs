/* ****************************************************
 * Name: OgreVertex.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/16 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using PAGE.Generic.Navigation;
using PAGE.OGRE.Math;
using PAGE.OGRE.Resources;

namespace PAGE.OGRE.Navigation
{
    /// <summary>
    /// Represents a waypoint vertex for navigation purposes. Contains 
    /// information about paths and edges from/to the specified waypoint.
    /// </summary>
    [Serializable]
    public class OgreVertex: GenericVertex
    {
        #region Constructors

        public OgreVertex(string waypointID, OgreResourcesFactory resourcesFactory)
            : base(waypointID, resourcesFactory)
        {
        }

        #endregion
    }
}