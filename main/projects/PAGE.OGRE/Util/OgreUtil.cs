/* ****************************************************
 * Name: OgreUtil.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/28 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using Mogre;

namespace PAGE.OGRE.Util
{
    /// <summary>
    /// This class provides a set of utility methods to manage some ogre components.
    /// </summary>
    public class OgreUtil
    {
        /// <summary>
        /// Indicates if an entity and all its sub-entities should receive shadows.
        /// </summary>
        /// <param name="entity">the entity to set receive shadows attribute.</param>
        /// <param name="receive">if set to true, the given entity will receive
        /// shadows casted from other objects.</param>
        public static void SetReceiveShadows(Entity entity, bool receive)
        {
            uint subEntityNumber = entity.NumSubEntities;
            for(uint i = 0; i< subEntityNumber; i++)
            {
                entity.GetSubEntity(i).GetMaterial().ReceiveShadows = receive;
            }
        }

        /// <summary>
        /// Used for cloned material naming.
        /// </summary>
        protected static int MaterialNumber = 1;

        /// <summary>
        /// Copies the material of the given entity so that it uses a cloned
        /// material of the original.
        /// </summary>
        /// <param name="entity">the entity to clone the material to.</param>
        public static void CloneMaterial(Entity entity)
        {
            uint subEntityNumber = entity.NumSubEntities;
            for (uint i = 0; i < subEntityNumber; i++)
            {
                MaterialPtr oldMat = entity.GetSubEntity(i).GetMaterial();
                entity.GetSubEntity(i).MaterialName =
                    oldMat.Clone(oldMat.Name + MaterialNumber++).Name;
            }
        }

        /// <summary>
        /// Creates a manual pose animation to the given mesh. Creates a vertex
        /// animation track and adds all the pose references to it.
        /// </summary>
        /// <param name="meshName">the name of the mesh to add the manual animation to.</param>
        /// <param name="manualAnimationName">the name of the manual animation to create.</param>
        /// <returns>The VertexPoseKeyFrame that was created and that can further be
        /// used to manipulate the poses influences (weights).</returns>
        public static VertexPoseKeyFrame CreateManualPoseAnimation(
            string meshName, string manualAnimationName)
        {
            Animation anim;
            VertexAnimationTrack track;
            VertexPoseKeyFrame manualKeyFrame;

            //loads mesh
            MeshPtr mesh = MeshManager.Singleton.Load(
                meshName, ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME);

            //checks animation existence
            if (mesh.HasAnimation(manualAnimationName))
            {
                //gets existent manual keyframe
                anim = mesh.GetAnimation(manualAnimationName);
                track = anim.GetVertexTrack(mesh.GetPose(0).Target);
                manualKeyFrame = track.GetVertexPoseKeyFrame(0);
            }
            else
            {
                //creates manual pose animation and keyframe
                anim = mesh.CreateAnimation(manualAnimationName, 0);
                track = anim.CreateVertexTrack(mesh.GetPose(0).Target, VertexAnimationType.VAT_POSE);
                manualKeyFrame = track.CreateVertexPoseKeyFrame(0);

                //creates pose references, initially zero
                for (ushort i = 0; i < mesh.PoseCount; i++) 
                    manualKeyFrame.AddPoseReference(i, 0.0f);
            }

            return manualKeyFrame;
        }
    }
}
