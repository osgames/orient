/* ****************************************************** 
 * Name: OSMLoader 
 * Desc: The OSMLoader class load a scene from a xml document 
 *       which contains scene data that oFusion exported from 
 *       3DSMax. It was converted from c++ source file: oSceneLoader. 
 * Code: ChengYangSheng 
 * Date: 2006/06/18 
 * 
 * Modified: 2007/01/09 by Pedro Sequeira 
 * ******************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using Mogre;

namespace PAGE.OGRE.Util
{
    public class OSMLoader
    {
        #region Events

        public event LoadedSceneObjectEventHandler OnCameraCreate;
        public event LoadedSceneObjectEventHandler OnNodeCreate;
        public event LoadedSceneObjectEventHandler OnLightCreate;
        public event LoadedSceneObjectEventHandler OnEntityCreate; 

        #endregion

        #region Fields

        protected XmlDocument xd = null;
        protected SceneManager mSceneMgr = null;
        protected RenderWindow mWindow = null;
        protected SceneNode node = null;

        protected const int SCENE_SKYPLANE = 1;
        protected const int SCENE_SKYDOME = 2;
        protected const int SCENE_SKYBOX = 3;

        protected Dictionary<string, Camera> cameras = new Dictionary<string, Camera>();
        protected Dictionary<string, Light> lights = new Dictionary<string, Light>();
        protected Dictionary<string, Entity> entities = new Dictionary<string, Entity>();

        #endregion

        #region Properties

        public Dictionary<string, Camera> Cameras
        {
            get { return cameras; }
        }

        public Dictionary<string, Light> Lights
        {
            get { return lights; }
        }

        public Dictionary<string, Entity> Entities
        {
            get { return entities; }
        }

        public SceneManager SceneManager
        {
            get { return this.mSceneMgr; }
        }

        public SceneNode Node
        {
            get { return this.node; }
        }

        public RenderQueueGroupID RenderQueueGroupID
        {
            set
            {
                foreach (Entity entity in this.entities.Values)
                {
                    entity.RenderQueueGroup = (byte)value;
                }
            }
        } 

        #endregion

        #region Constructors

        public OSMLoader(SceneManager mgr, RenderWindow wnd)
        {
            this.mSceneMgr = mgr;
            this.mWindow = wnd;
        }


        #endregion

        #region Public Methods

        public void Initialize(string xmlFile)
        {
            LogManager.Singleton.LogMessage(@" 
********************************************** 
**            OSM Scene Loader              ** 
********************************************** 
");
            LogManager.Singleton.LogMessage("OSMLoader loading scene form file: " + xmlFile);
            xd = new XmlDocument();
            try
            {
                string filename = ResourceGroupManager.Singleton.FindResourceFileInfo(
                                      ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME,
                                      xmlFile)[0].archive.Name + "/" + xmlFile;
                xd.Load(filename);
            }
            catch (XmlException ex)
            {
                xd = null;
                LogManager.Singleton.LogMessage("Can't parse scene data, detail error message:\r\n" + ex.Message);

                throw new ArgumentException("Could not load OSM file: " + ex.Message, "xmlFile");
            }
        }

        public bool CreateScene()
        {
            return this.CreateScene(null);
        }

        public bool CreateScene(SceneNode parent)
        {
            return this.CreateScene(parent, RenderQueueGroupID.RENDER_QUEUE_MAIN);
        }

        public bool CreateScene(SceneNode parent, RenderQueueGroupID renderQueueGroupID)
        {
            if (xd == null) return false;

            LogManager.Singleton.LogMessage("OSMLoader: Creating scene on '" +
                                            (parent == null ? "Root" : parent.Name) + "' node.");

            if (parent == null)
            {
                parent = mSceneMgr.RootSceneNode.CreateChildSceneNode();
            }

            this.node = parent;

            if (mSceneMgr == null)
            {
                if (xd.SelectSingleNode("*/sceneManager") != null)
                    SetSceneProperties(xd);
                else
                    mSceneMgr = Root.Singleton.CreateSceneManager(SceneType.ST_GENERIC);
            }

            try
            {
                this.CreateCameras(xd.SelectNodes("*/cameras/*"), parent);
                this.CreateLights(xd.SelectNodes("*/lights/*"), parent);
                this.CreateEntities(xd.SelectNodes("*/entities/*"), parent, renderQueueGroupID);
            }
            catch(XmlException)
            {
            }

            LogManager.Singleton.LogMessage(@" 
******************************** 
** oSceneLoader: Scene loaded ** 
******************************** 
");
            return true;
        } 

        public void DestroyAll()
        {
            foreach (Entity ent in this.entities.Values)
            {
                this.mSceneMgr.DestroyEntity(ent);
            }
            foreach (Camera cam in this.cameras.Values)
            {
                this.mSceneMgr.DestroyCamera(cam);
            }
            foreach (Light light in this.lights.Values)
            {
                this.mSceneMgr.DestroyLight(light);
            }
        }

        public void EnableAllLights()
        {
            this.CreateLights(xd.SelectNodes("*/lights/*"), this.node);
        }

        public void DisableAllLights()
        {
            foreach (string lightName in new ArrayList(this.lights.Keys))
            {
                this.lights.Remove(lightName);
                this.mSceneMgr.DestroyLight(lightName);
            }
        }

        public void ShowAllEntities()
        {
            foreach (Entity e in this.entities.Values) e.Visible = true;
        }

        public void HideAllEntities()
        {
            foreach (Entity e in this.entities.Values) e.Visible = false;
        }

        public void ShowAll()
        {
            this.ShowAllEntities();
            this.EnableAllLights();
        }

        public void HideAll()
        {
            this.HideAllEntities();
            this.DisableAllLights();
        }

        public void EnableReceiveShadows()
        {
            foreach (Entity e in this.entities.Values) OgreUtil.SetReceiveShadows(e, true);
        }

        public void DisableReceiveShadows()
        {
            foreach (Entity e in this.entities.Values) OgreUtil.SetReceiveShadows(e, false);
        }

        public void Update(float millisSinceLastUpdate)
        {
            //do nothing..
        }

        public void Dispose()
        {
            //destroy elements
            foreach (Entity ent in this.entities.Values) this.mSceneMgr.DestroyEntity(ent);
            foreach (Camera cam in this.cameras.Values) this.mSceneMgr.DestroyCamera(cam);
            foreach (Light l in this.lights.Values) this.mSceneMgr.DestroyLight(l);

            //destroy node
            this.node.RemoveAndDestroyAllChildren();
            this.node.ParentSceneNode.RemoveAndDestroyChild(this.node.Name);
        }

        #endregion

        #region Protected Load Methods

        private ColourValue ParseColor(string r, string g, string b)
        {
            return new ColourValue(
                Convert.ToSingle(r, CultureInfo.InvariantCulture),
                Convert.ToSingle(g, CultureInfo.InvariantCulture),
                Convert.ToSingle(b, CultureInfo.InvariantCulture));
        }

        protected void SetSceneProperties(XmlDocument xd)
        {
            // Scene manager 
            XmlElement sceneMgrElem = (XmlElement)xd.SelectSingleNode("sceneManager");
            /* 
               ST_GENERIC = 1, 
               ST_EXTERIOR_CLOSE = 2, 
               ST_EXTERIOR_FAR = 4, 
               ST_EXTERIOR_REAL_FAR = 8, 
               ST_INTERIOR = 16 
             */
            int type = int.Parse(sceneMgrElem.GetAttribute("type"));
            SceneType sceneType = (SceneType)type;
            mSceneMgr = Root.Singleton.CreateSceneManager(sceneType);

            string worldGeometry = sceneMgrElem.GetAttribute("worldGeometry");
            if (worldGeometry != null)
                mSceneMgr.SetWorldGeometry(worldGeometry);

            // Background Color 
            XmlElement colorElem = (XmlElement)xd.SelectSingleNode("bkgcolor");
            if (colorElem != null && mWindow != null)
            {
                ColourValue color = ParseColor(
                    colorElem.GetAttribute("r"),
                    colorElem.GetAttribute("g"),
                    colorElem.GetAttribute("b"));

                mWindow.GetViewport(0).BackgroundColour = color;
            }

            // Ambient light Color 
            colorElem = (XmlElement)xd.SelectSingleNode("lightColor");
            if (colorElem != null)
            {
                ColourValue color = ParseColor(
                    colorElem.GetAttribute("r"),
                    colorElem.GetAttribute("g"),
                    colorElem.GetAttribute("b"));
                mSceneMgr.AmbientLight = color;
            }


            // Scene shadows 
            XmlElement shadowsElem = (XmlElement)xd.SelectSingleNode("shadowTechnique");
            if (shadowsElem != null)
            {
                type = int.Parse(shadowsElem.GetAttribute("type"));
                ShadowTechnique shadowType = (ShadowTechnique)type;

                mSceneMgr.ShadowTechnique = shadowType;

                int tex_size = int.Parse(shadowsElem.GetAttribute("tex_size"));
                int tex_count = int.Parse(shadowsElem.GetAttribute("tex_count"));

                mSceneMgr.SetShadowTextureSettings((ushort)tex_size, (ushort)tex_count);

                // Shadow Color 
                colorElem = (XmlElement)shadowsElem.SelectSingleNode("color");
                if (colorElem != null)
                {
                    ColourValue color = ParseColor(
                        colorElem.GetAttribute("r"),
                        colorElem.GetAttribute("g"),
                        colorElem.GetAttribute("b"));

                    mSceneMgr.ShadowColour = color;
                }
            }

            // Scene sky 
            XmlElement skyElem = (XmlElement)xd.SelectSingleNode("skyTechnique");
            if (skyElem != null)
            {
                type = int.Parse(skyElem.GetAttribute("type"));
                String materialName = skyElem.GetAttribute("material");

                if (materialName != " ")
                {
                    bool drawFirst = bool.Parse(skyElem.GetAttribute("drawFirst"));
                    float tiling = Convert.ToSingle(skyElem.GetAttribute("tiling"), CultureInfo.InvariantCulture);
                    float scale = Convert.ToSingle(skyElem.GetAttribute("scale"), CultureInfo.InvariantCulture);
                    float dist = Convert.ToSingle(skyElem.GetAttribute("dist"), CultureInfo.InvariantCulture);
                    float bow = Convert.ToSingle(skyElem.GetAttribute("bow"), CultureInfo.InvariantCulture);
                    int xSegments = int.Parse(skyElem.GetAttribute("xSegments"));
                    int ySegments = int.Parse(skyElem.GetAttribute("ySegments"));
                    Quaternion quat = Quaternion.IDENTITY;
                    Plane plane = new Plane();
                    plane.d = dist;

                    plane.normal = -Vector3.UNIT_Y;


                    switch (type)
                    {
                        case SCENE_SKYPLANE:

                            mSceneMgr.SetSkyPlane(true, plane, materialName, scale,
                                                  tiling, drawFirst, bow, xSegments, ySegments);

                            mSceneMgr.SetSkyBox(false, "");
                            mSceneMgr.SetSkyDome(false, "");

                            break;

                        case SCENE_SKYBOX:

                            mSceneMgr.SetSkyBox(true, materialName, dist, drawFirst, quat);
                            mSceneMgr.SetSkyPlane(false, plane, "");
                            mSceneMgr.SetSkyDome(false, "");

                            break;

                        case SCENE_SKYDOME:

                            mSceneMgr.SetSkyDome(true, materialName, bow, tiling, dist,
                                                 drawFirst, quat, xSegments, ySegments);

                            mSceneMgr.SetSkyPlane(false, plane, "");
                            mSceneMgr.SetSkyBox(false, "");

                            break;

                    }
                }
            }
        }

        protected SceneNode CreateNode(XmlElement elet, SceneNode parent)
        {
            SceneNode pNode = null;

            // Try to find the parent node 
            string pszName = elet.GetAttribute("name");
            if (pszName == null) return null;

            // Check if this node has a parent 
            string pszParent = elet.GetAttribute("parent");
            if (pszParent == "")
            {
                // Check if the scene node has already been created by a child 
                try
                {
                    pNode = mSceneMgr.GetSceneNode(pszName);
                }
                catch
                {
                    pNode = parent.CreateChildSceneNode(pszName);
                }
            }
            else
            {
                SceneNode pParent = null;
                try
                {
                    // Try to find parent scenenode 
                    pParent = mSceneMgr.GetSceneNode(pszParent);
                }
                catch
                {
                    // We try to create the parent node as child of root node. 
                    // Later when the parent (hopefully) is created, we can adjust it, 
                    // if it is child of another node. 
                    pParent = parent.CreateChildSceneNode(pszParent);
                }


                try
                {
                    // Check if the scene node has already been created by a child 
                    // In this case we would have to change the parent. 
                    pNode = mSceneMgr.GetSceneNode(pszName);

                    // Get old parent (probably scene root) 
                    SceneNode pOldParent = pNode.ParentSceneNode;

                    // Remove this node 
                    pOldParent.RemoveChild(pNode);

                    // Insert us as child on the "real" parent 
                    pParent.AddChild(pNode);
                }
                catch
                {
                    pNode = pParent.CreateChildSceneNode(pszName);
                }
            }

            // Position 
            XmlElement posElem = (XmlElement)elet.SelectSingleNode("position");
            if (posElem != null)
            {
                Vector3 pos;
                pos.x = Convert.ToSingle(posElem.GetAttribute("x"), CultureInfo.InvariantCulture);
                pos.y = Convert.ToSingle(posElem.GetAttribute("y"), CultureInfo.InvariantCulture);
                pos.z = Convert.ToSingle(posElem.GetAttribute("z"), CultureInfo.InvariantCulture);
                pNode.Position = pos;
            }

            // Rotation 
            XmlElement rotElem = (XmlElement)elet.SelectSingleNode("rotation");
            if (rotElem != null)
            {
                pNode.SetOrientation(
                    Convert.ToSingle(rotElem.GetAttribute("w"), CultureInfo.InvariantCulture),
                    Convert.ToSingle(rotElem.GetAttribute("x"), CultureInfo.InvariantCulture),
                    Convert.ToSingle(rotElem.GetAttribute("y"), CultureInfo.InvariantCulture),
                    Convert.ToSingle(rotElem.GetAttribute("z"), CultureInfo.InvariantCulture));

            }

            // Scale 
            XmlElement scaleElem = (XmlElement)elet.SelectSingleNode("scale");
            if (scaleElem != null)
            {
                Vector3 scale;
                scale.x = Convert.ToSingle(scaleElem.GetAttribute("x"), CultureInfo.InvariantCulture);
                scale.y = Convert.ToSingle(scaleElem.GetAttribute("y"), CultureInfo.InvariantCulture);
                scale.z = Convert.ToSingle(scaleElem.GetAttribute("z"), CultureInfo.InvariantCulture);
                pNode.SetScale(scale);
            }

            // Notify 
            if (OnNodeCreate != null)
                OnNodeCreate(pNode, elet);

            // Animation 
            XmlElement animList = (XmlElement)elet.SelectSingleNode("animations");
            if (animList != null)
            {
                // 
                foreach (XmlElement animElem in animList.ChildNodes)
                {
                    // Get name of animation 
                    string aniName = animElem.GetAttribute("name");

                    Animation pAnim = null;
                    try
                    {
                        pAnim = mSceneMgr.GetAnimation(aniName);
                    }
                    catch
                    {
                    }

                    // If this animation has not been created yet, we create it 
                    if (pAnim == null)
                    {
                        float fLength = Convert.ToSingle(animElem.GetAttribute("length"), CultureInfo.InvariantCulture);
                        pAnim = mSceneMgr.CreateAnimation(aniName, fLength);
                        pAnim.SetInterpolationMode(Animation.InterpolationMode.IM_LINEAR);
                    }

                    // Create animation track for this node 
                    NodeAnimationTrack pTrack = pAnim.CreateNodeTrack((ushort)(pAnim.NumNodeTracks + 1), pNode);

                    // Iterate all keyframes for this node 
                    foreach (XmlElement pKeyframeElem in animElem.ChildNodes)
                    {
                        float fTime = Convert.ToSingle(pKeyframeElem.GetAttribute("time"), CultureInfo.InvariantCulture);
                        TransformKeyFrame pKeyFrame = pTrack.CreateNodeKeyFrame(fTime);

                        // Position 
                        posElem = (XmlElement)pKeyframeElem.SelectSingleNode("position");
                        if (posElem != null)
                        {
                            Vector3 trans;
                            trans.x = Convert.ToSingle(posElem.GetAttribute("x"), CultureInfo.InvariantCulture);
                            trans.y = Convert.ToSingle(posElem.GetAttribute("y"), CultureInfo.InvariantCulture);
                            trans.z = Convert.ToSingle(posElem.GetAttribute("z"), CultureInfo.InvariantCulture);
                            pKeyFrame.Translate = trans;
                        }

                        // Rotation 
                        rotElem = (XmlElement)pKeyframeElem.SelectSingleNode("rotation");
                        if (rotElem != null)
                        {
                            Quaternion qRot;
                            qRot.x = Convert.ToSingle(rotElem.GetAttribute("x"), CultureInfo.InvariantCulture);
                            qRot.y = Convert.ToSingle(rotElem.GetAttribute("y"), CultureInfo.InvariantCulture);
                            qRot.z = Convert.ToSingle(rotElem.GetAttribute("z"), CultureInfo.InvariantCulture);
                            qRot.w = Convert.ToSingle(rotElem.GetAttribute("w"), CultureInfo.InvariantCulture);
                            pKeyFrame.Rotation = qRot;
                        }

                        // Scale 
                        scaleElem = (XmlElement)pKeyframeElem.SelectSingleNode("scale");
                        if (scaleElem != null)
                        {
                            Vector3 scale;
                            scale.x = Convert.ToSingle(scaleElem.GetAttribute("x"), CultureInfo.InvariantCulture);
                            scale.y = Convert.ToSingle(scaleElem.GetAttribute("y"), CultureInfo.InvariantCulture);
                            scale.z = Convert.ToSingle(scaleElem.GetAttribute("z"), CultureInfo.InvariantCulture);
                            pKeyFrame.Scale = scale;
                        }
                    }
                }
            }

            return pNode;
        }

        protected void CreateCameras(XmlNodeList list, SceneNode parent)
        {
            // Iterate all Cameras, creating them. We do not attach them yet, since 
            // we need to make sure all potential item entities have been created. 
            foreach (XmlElement item in list)
            {
                // Ogre could cast an exception, in which case we just try to 
                // continue reading the other Cameras 
                try
                {
                    // Create camera 
                    Camera pCamera = mSceneMgr.CreateCamera(item.GetAttribute("name"));
                    if (pCamera == null) continue;

                    // Set Field of View on camera 
                    pCamera.FOVy = new Radian(Convert.ToSingle(item.GetAttribute("FOV"), CultureInfo.InvariantCulture));
                    pCamera.NearClipDistance = 5;

                    // Create node with full information 
                    SceneNode pCameraNode = CreateNode(item, parent);

                    // Attach the Camera entity to node 
                    pCameraNode.AttachObject(pCamera);

                    // Target 
                    XmlElement targetElem = (XmlElement)item.SelectSingleNode("target");
                    if (targetElem != null)
                    {
                        // Create node with full information 
                        SceneNode pTargetNode = CreateNode(targetElem, parent);
                        pCameraNode.SetAutoTracking(true, pTargetNode);
                    }

                    // Notify 
                    if (OnCameraCreate != null)
                        OnCameraCreate(pCamera, item);

                    // Add to camera list 
                    cameras.Add(pCamera.Name, pCamera);
                }
                catch
                {
                    continue;
                }
            }
        }

        protected void CreateLights(XmlNodeList list, SceneNode parent)
        {
            // Iterate all Lights, creating them. We do not attach them yet, since 
            // we need to make sure all potential parent entities have been created. 
            foreach (XmlElement pLightElem in list)
            {
                // Ogre could cast an exception, in which case we just try to 
                // continue reading the other Lights 
                try
                {
                    string pszName = pLightElem.GetAttribute("name");

                    Light pLight = mSceneMgr.CreateLight(pszName);
                    if (pLight == null) continue;

                    // Figure out which type of light we are using 
                    string pszType = pLightElem.GetAttribute("type");
                    if (pszType == "omni")
                    {
                        pLight.Type = Light.LightTypes.LT_POINT;
                    }
                    else if (pszType == "spot")
                    {
                        pLight.Type = Light.LightTypes.LT_SPOTLIGHT;
                        pLight.SetSpotlightRange(
                            new Radian(new Degree(Convert.ToSingle(pLightElem.GetAttribute("hotspot"), CultureInfo.InvariantCulture))),
                            new Radian(new Degree(Convert.ToSingle(pLightElem.GetAttribute("falloff"), CultureInfo.InvariantCulture))));
                        pLight.SetDirection(0, 0, -1);

                    }
                    else if (pszType == "directional")
                    {
                        pLight.Type = Light.LightTypes.LT_DIRECTIONAL;
                    }

                    // Check if the light should be on 
                    string pszOn = pLightElem.GetAttribute("on");
                    if (pszOn == "true")
                        pLight.Visible = true;
                    else
                        pLight.Visible = false;

                    // Check if the object should cast shadows 
                    string pszCastShadows = pLightElem.GetAttribute("CastShadows");
                    if (pszCastShadows == "no")
                        pLight.CastShadows = false;
                    else
                        pLight.CastShadows = true;

                    // Diffuse Color 
                    XmlElement colorElem = (XmlElement)pLightElem.SelectSingleNode("color");
                    if (colorElem != null)
                    {
                        pLight.SetDiffuseColour(
                            Convert.ToSingle(colorElem.GetAttribute("r"), CultureInfo.InvariantCulture),
                            Convert.ToSingle(colorElem.GetAttribute("g"), CultureInfo.InvariantCulture),
                            Convert.ToSingle(colorElem.GetAttribute("b"), CultureInfo.InvariantCulture));
                    }

                    // Specular Color 
                    XmlElement specularElem = (XmlElement)pLightElem.SelectSingleNode("specular");
                    if (specularElem != null)
                    {
                        pLight.SetSpecularColour(
                            Convert.ToSingle(specularElem.GetAttribute("r"), CultureInfo.InvariantCulture),
                            Convert.ToSingle(specularElem.GetAttribute("g"), CultureInfo.InvariantCulture),
                            Convert.ToSingle(specularElem.GetAttribute("b"), CultureInfo.InvariantCulture));
                    }

                    // Attenuation 
                    XmlElement attenElem = (XmlElement)pLightElem.SelectSingleNode("attenuation");
                    if (attenElem != null)
                    {
                        pLight.SetAttenuation(
                            Convert.ToSingle(attenElem.GetAttribute("range"), CultureInfo.InvariantCulture),
                            Convert.ToSingle(attenElem.GetAttribute("constant"), CultureInfo.InvariantCulture),
                            Convert.ToSingle(attenElem.GetAttribute("linear"), CultureInfo.InvariantCulture),
                            Convert.ToSingle(attenElem.GetAttribute("quadratic"), CultureInfo.InvariantCulture));
                    }


                    // Create node with full information 
                    SceneNode pLightNode = CreateNode(pLightElem, parent);

                    //#if DEBUG

                    //Entity ent = this.mSceneMgr.CreateEntity("lamp" + pszName, "whiteball.mesh");
                    //ent.CastShadows = false;
                    //this.entities.Add("Debug" + ent.Name, ent);
                    //pLightNode.AttachObject(ent);
                    //pLightNode.Scale(0.3f, 0.3f, 0.3f);

                    //#endif

                    // Attach the Light entity to node 
                    pLightNode.AttachObject(pLight);

                    // Target 
                    XmlElement targetElem = (XmlElement)pLightElem.SelectSingleNode("target");
                    if (targetElem != null)
                    {
                        // Create node with full information 
                        SceneNode pTargetNode = CreateNode(targetElem, parent);
                        pLightNode.SetAutoTracking(true, pTargetNode);
                    }

                    // Notify 
                    if (OnLightCreate != null)
                        OnLightCreate(pLight, pLightElem);

                    // Add to light list 
                    lights.Add(pszName, pLight);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }
            }
        }

        protected void CreateEntities(XmlNodeList list, SceneNode parent,
                                      RenderQueueGroupID renderQueueGroupID)
        {
            // Iterate all meshes, creating them. 
            foreach (XmlElement pMeshElem in list)
            {
                // Ogre could cast an exception, in which case we just try to 
                // continue reading the other meshes 
                try
                {
                    string pszName = pMeshElem.GetAttribute("name");
                    string pszFileName = pMeshElem.GetAttribute("filename");

                    // try to create the mesh 
                    Entity pEntity = mSceneMgr.CreateEntity(pszName, pszFileName);
                    if (pEntity == null) continue;

                    // Check if the object should cast shadows 
                    string pszCastShadows = pMeshElem.GetAttribute("CastShadows");
                    if (pszCastShadows == "no")
                        pEntity.CastShadows = false;
                    else if (pszCastShadows == "yes")
                        pEntity.CastShadows = true;

                    // Check if the object should receive shadows 
                    string pszReceiveShadows = pMeshElem.GetAttribute("ReceiveShadows");
                    if (pszReceiveShadows == "no")
                        OgreUtil.SetReceiveShadows(pEntity, false);
                    else if (pszReceiveShadows == "yes")
                        OgreUtil.SetReceiveShadows(pEntity, true);

                    // Create node with full information 
                    SceneNode pObjNode = CreateNode(pMeshElem, parent);

                    // Attach the mesh entity to node 
                    pObjNode.AttachObject(pEntity);

                    //set rendering queue group
                    pEntity.RenderQueueGroup = (byte)renderQueueGroupID;

                    // Notify 
                    if (OnEntityCreate != null)
                        OnEntityCreate(pEntity, pMeshElem);

                    // Add to entity list 
                    entities.Add(pszName, pEntity);
                }
                catch
                {
                    continue;
                }
            }
        } 

        #endregion
    }

    public delegate void LoadedSceneObjectEventHandler(Object obj, XmlElement xmlSrc);
}