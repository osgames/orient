/* ****************************************************
 * Name: SpotsDebug.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/28 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections.Generic;
using Mogre;
using PAGE.Generic.Domain.Collections;
using PAGE.Generic.Domain.Spots;
using PAGE.OGRE.Domain.Spots;

namespace PAGE.OGRE.Util
{
    public class SpotsDebug<SpotType> where SpotType : IGenericSpot
    {
        #region Fields

        protected static int SpotCount = 1;
        protected bool showingSpots;
        protected SceneManager sceneManager;

        protected Vector3 scale = Vector3.UNIT_SCALE;
        protected string meshName = "";

        protected List<Node> nodes = new List<Node>();
        protected List<Entity> entities = new List<Entity>();

        #endregion

        #region Constructors

        public SpotsDebug(SceneManager manager, string meshName, Vector3 meshScale)
        {
            this.sceneManager = manager;
            this.meshName = meshName;
            this.scale = meshScale;
        }

        #endregion

        #region Public Methods

        public void ShowSpots(GenericSpotList<SpotType> spots)
        {
            if (this.showingSpots)
            {
                this.HideSpots();
            }

            string id = "spot" + SpotCount++;
            this.nodes.Clear();
            this.entities.Clear();

            foreach (IOgreSpot spot in spots)
            {
                Entity ent = this.sceneManager.CreateEntity(spot.ResourceSpot.IdToken + id, this.meshName);
                ent.CastShadows = false;
                OgreUtil.SetReceiveShadows(ent, false);
                SceneNode node = spot.Node.CreateChildSceneNode(spot.ResourceSpot.IdToken + id);
                node.AttachObject(ent);
                node.SetScale(this.scale);
                this.nodes.Add(node);
                this.entities.Add(ent);
            }

            this.showingSpots = true;
        }

        public void HideSpots()
        {
            if (!this.showingSpots) return;

            foreach (SceneNode node in this.nodes)
            {
                try
                {
                    node.ParentSceneNode.RemoveAndDestroyChild(node.Name);
                }
                catch (AccessViolationException)
                {
                }
            }
            this.nodes.Clear();
            foreach (Entity entity in this.entities)
            {
                this.sceneManager.DestroyEntity(entity);
            }
            this.entities.Clear();

            this.showingSpots = false;
        }

        #endregion
    }
}