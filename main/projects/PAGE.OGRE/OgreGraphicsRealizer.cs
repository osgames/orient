/* ****************************************************
 * Name: OgreGraphicsRealizer.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Mogre;
using PAGE.Generic;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Cameras;
using PAGE.Generic.GUI;
using PAGE.Generic.Resources;
using PAGE.Generic.Resources.Assets;
using PAGE.OGRE.Domain.Assets;
using PAGE.OGRE.Domain.Cameras;
using PAGE.OGRE.GUI;
using PAGE.OGRE.Math;
using PAGE.OGRE.Resources;
using PAGE.OGRE.Resources.Assets;
using PAGE.OGRE.Util;

namespace PAGE.OGRE
{
    public class OgreGraphicsRealizer : GraphicsRealizer
    {
        #region Fields

        //maximum millis between an update
        protected const float MAX_UPDATE_TIME = 1000;

        //singleton instance
        public static OgreGraphicsRealizer Instance = new OgreGraphicsRealizer();

        //ogre application
        protected OgreApplication ogreApp = new OgreApplication();

        //graphical options
        protected GraphicsConfig graphicsConfig;

        protected static int assetCounter;

        #endregion

        #region Properties

        public new OgreCamera Camera
        {
            get { return base.Camera as OgreCamera; }
        }

        public new OgreResourcesManager ResourcesManager
        {
            get { return base.ResourcesManager as OgreResourcesManager; }
        }

        protected override float LastUpdateTime
        {
            get { return this.lastUpdateTime; }
            set { this.lastUpdateTime = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating if the realizer is displaying in fullscreen.
        /// </summary>
        public bool Fullscreen
        {
            get { return this.ogreApp.Fullscreen; }
            set
            {
                try
                {
                    this.ogreApp.Fullscreen = value;
                }
                catch (SEHException)
                {
                    if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                    else throw;
                }
            }
        }

        /// <summary>
        /// Sets the visibility property of the graphical statistics.
        /// </summary>
        public bool GraphicStatsVisible
        {
            set
            {
                try
                {
                    if (value) this.ogreApp.ShowStatsOverlay();
                    else this.ogreApp.HideStatsOverlay();
                }
                catch (SEHException)
                {
                    if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                    else throw;
                }
            }
        }

        /// <summary>
        /// Gets the graphical configuration info.
        /// </summary>
        public GraphicsConfig GraphicsConfig
        {
            get { return this.graphicsConfig; }
        }

        public new MOISGUIManager GUIManager
        {
            get { return guiManager as MOISGUIManager; }
        }

        /// <summary>
        /// Gets the Ogre application object responsible to manage all ogre 
        /// initialization and management.
        /// </summary>
        public OgreApplication OgreApplication
        {
            get { return ogreApp; }
        }

        #endregion

        #region Public Methods

        #region App handling

        public override void Setup(string resourceFileName)
        {
            this.Setup(resourceFileName, null);
        }

        public virtual void Setup(string resourcesFileName, Control control)
        {
            lock (this.locker)
            {
                try
                {
                    //resources
                    if (!File.Exists(resourcesFileName))
                    {
                        throw new ArgumentException(
                            "Could not start realizer: invalid file name path given: " +
                            resourcesFileName);
                    }
                    this.resourcesManager = this.CreateResourcesManager(resourcesFileName);
                    this.resourcesManager.LoadAll();

                    //sets up ogre app
                    if (!this.ogreApp.Setup(control))
                    {
                        throw new InvalidProgramException("Could not setup Ogre application");
                    }
                    this.ogreApp.HideAllOverlays();
                    this.ogreApp.WindowResized += AppWindowChanged;

                    //graphics
                    string graphicsConfigFile =
                        ((OgreResourcesConfig) this.resourcesManager.ResourcesConfig).GraphicsXMLFile;
                    this.graphicsConfig = new GraphicsConfig();
                    this.graphicsConfig.LoadFromXml(graphicsConfigFile);

                    this.graphicsConfig.AmbientColorChanged += graphicsConfig_AmbientColorChanged;
                    this.graphicsConfig.BackgroundColorChanged += graphicsConfig_BackgroundColorChanged;
                    this.graphicsConfig.ShadowsColorChanged += graphicsConfig_ShadowsColorChanged;
                    this.graphicsConfig.ShadowsChanged += graphicsConfig_ShadowsChanged;
                    this.graphicsConfig.LightsChanged += graphicsConfig_LightsChanged;

                    this.RefreshAllGraphics();

                    //gui manager
                    this.guiManager = this.CreateGUIManager();

                    //camera
                    this.ogreApp.Camera.SetPosition(0, 0, 0);
                    this.camera = this.CreateCamera();
                }
                catch (SEHException)
                {
                    if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                    else throw;
                }
                catch (ApplicationException ae)
                {
                    throw new InvalidProgramException("Could not start realizer: " + ae);
                }
            }
        }

        protected override ResourcesManager CreateResourcesManager(string resourcesFileName)
        {
            return new OgreResourcesManager(resourcesFileName);
        }

        protected override GenericCamera CreateCamera()
        {
            return new OgreCamera(this.ogreApp.Camera);
        }

        protected override GUIManager CreateGUIManager()
        {
            return new MOISGUIManager(this.ogreApp);
        }

        public override void Update()
        {
            this.lastUpdateTime = System.Math.Min(
                this.ogreApp == null ? 0 : this.ogreApp.MillisSinceLastFrame,
                MAX_UPDATE_TIME);

            try
            {
                base.Update();

                lock (this.locker)
                {
                    if (!this.Destroyed) this.ogreApp.UpdateRenderer();

                    if ((this.camera == null) || this.camera.Destroyed)
                    {
                        this.camera = this.CreateCamera();
                    }
                }
            }
            catch (SEHException)
            {
                if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                else throw;
            }
        }

        public override void Destroy()
        {
            if (this.Destroyed) return;

            try
            {
                base.Destroy();

                lock (this.locker)
                {
                    this.resourcesManager.Dispose();
                    this.ogreApp.Dispose();
                }
            }
            catch (SEHException)
            {
                if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                else throw;
            }
        }

        /// <summary>
        /// Saves a screenshot containing the current graphical content of the realizer.
        /// </summary>
        /// <param name="fileName">the name of the image file to save the screenshot in.
        /// </param>
        public void SaveScreenshot(string fileName)
        {
            lock (this.locker)
            {
                try
                {
                    this.ogreApp.RenderWindow.WriteContentsToFile(fileName);
                }
                catch (SEHException)
                {
                    if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                    else throw;
                }
            }
        }

        /// <summary>
        /// Saves a screenshot image of the given size containing the current graphical 
        /// content of the realizer.
        /// </summary>
        /// <param name="width">the width of the screenshot image.</param>
        /// <param name="height">the height of the screenshot image.</param>
        /// <param name="fileName">the name of the image file to save the screenshot in.
        /// </param>
        public void SaveScreenshot(int width, int height, string fileName)
        {
            lock (this.locker)
            {
                try
                {
                    uint oldWidth = this.ogreApp.WindowWidth;
                    uint oldHeight = this.ogreApp.WindowHeight;
                    this.ogreApp.Resize((uint) width, (uint) height);
                    this.ogreApp.Root.RenderOneFrame();
                    this.ogreApp.Root.RenderOneFrame();
                    this.ogreApp.RenderWindow.WriteContentsToFile(fileName);
                    this.ogreApp.Resize(oldWidth, oldHeight);
                }
                catch (SEHException)
                {
                    if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                    else throw;
                }
            }
        }

        #endregion

        #region Loads

        public override GenericCharacter LoadCharacter(string idResourceToken)
        {
            //checks resources
            ResourceHumanLikeCharacter resource = (ResourceHumanLikeCharacter)
                                                  this.resourcesManager.Characters[idResourceToken.ToLower()];
            if (resource == null)
            {
                throw new ArgumentException("There is no character resource with that name: "
                                            + idResourceToken);
            }

            string characterId = idResourceToken + ++assetCounter;
            OgreCharacter character = null;

            lock (this.locker)
            {
                //creates character scene node
                SceneNode node = this.CreateAssetSceneNode(resource);

                try
                {
                    //load's ogre character entity
                    Entity entity = null;
                    if (resource.FileName != "")
                    {
                        entity = this.ogreApp.SceneManager.CreateEntity(characterId, resource.FileName);
                        entity.CastShadows = true;
                        OgreUtil.SetReceiveShadows(entity, false);
                        OgreUtil.CloneMaterial(entity);
                        node.AttachObject(entity);
                    }

                    //creates and adds character to internal db
                    character = new OgreCharacter(resource, this.Camera, entity);
                    this.characters.Add(character);

                    character.Reset();
                }
                catch (SEHException)
                {
                    if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                    else throw;
                }
            }

            return character;
        }

        public override GenericItem LoadItem(string idResourceToken)
        {
            //checks resources
            ResourceItem resource =
                this.resourcesManager.Items[idResourceToken.ToLower()];
            if (resource == null)
            {
                throw new ArgumentException("There is no item resource with that name: "
                                            + idResourceToken);
            }

            string itemId = idResourceToken + ++assetCounter;
            OgreItem item = null;

            lock (this.locker)
            {
                try
                {
                    //load's ogre character entity
                    Entity entity = this.ogreApp.SceneManager.CreateEntity(itemId, resource.FileName);
                    entity.CastShadows = true;
                    OgreUtil.CloneMaterial(entity);

                    //creates item scene node
                    SceneNode node = this.CreateAssetSceneNode(resource);
                    node.AttachObject(entity);

                    //creates and adds item to internal db
                    item = new OgreItem(resource, this.Camera, entity);
                    this.items.Add(item);

                    item.Reset();
                }
                catch (SEHException)
                {
                    if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                    else throw;
                }
            }

            return item;
        }

        public override GenericSet LoadSet(string idResourceToken)
        {
            //checks resources
            OgreResourceSet resource = (OgreResourceSet)
                                       this.resourcesManager.Sets[idResourceToken.ToLower()];
            if (resource == null)
            {
                throw new ArgumentException("There is no set resource with that name: "
                                            + idResourceToken);
            }

            OgreSet set = null;
            assetCounter++;

            lock (this.locker)
            {
                //Set Loading
                SceneNode rootNode = this.CreateAssetSceneNode(resource);

                try
                {
                    OSMLoader loader = new OSMLoader(this.ogreApp.SceneManager, this.ogreApp.RenderWindow);
                    if (resource.FileName != "")
                    {
                        loader.Initialize(resource.FileName);
                        loader.CreateScene(rootNode);
                    }

                    //creates and adds set to internal db
                    set = new OgreSet(resource, loader, rootNode);
                    set.Reset();
                    this.sets.Add(set);

                    //loads set elements and contained assets
                    this.LoadSetElements(set);
                }
                catch (SEHException)
                {
                    if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                    else throw;
                }
            }

            return set;
        }

        #endregion

        #region Removes

        public override void RemoveCharacter(GenericCharacter character)
        {
            lock (this.locker)
            {
                //removes and destroys character...
                if (this.characters.Contains(character))
                {
                    try
                    {
                        //...from ogre
                        if (((OgreCharacter) character).Entity != null)
                        {
                            this.ogreApp.SceneManager.DestroyEntity(((OgreCharacter) character).Entity.Name);
                        }

                        //destroy character
                        character.Destroy();
                        this.ogreApp.SceneManager.RootSceneNode.RemoveAndDestroyChild(
                            ((OgreCharacter) character).Node.Name);
                    }
                    catch (SEHException)
                    {
                        if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                        else throw;
                    }

                    //...from internal db
                    this.characters.Remove(character);
                }
            }
        }

        public override void RemoveItem(DynamicAsset item)
        {
            lock (this.locker)
            {
                //removes and destroys item...
                if (this.items.Contains(item))
                {
                    try
                    {
                        //...from ogre
                        if (((OgreItem) item).Entity != null)
                        {
                            this.ogreApp.SceneManager.DestroyEntity(((OgreItem) item).Entity.Name);
                        }

                        //dispose item
                        item.Destroy();
                        this.ogreApp.SceneManager.RootSceneNode.RemoveAndDestroyChild(
                            ((OgreItem) item).Node.Name);
                    }
                    catch (SEHException)
                    {
                        if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                        else throw;
                    }

                    //...from internal db
                    this.items.Remove(item);
                }
            }
        }

        public override void RemoveSet(GenericSet set)
        {
            lock (this.locker)
            {
                OgreSet ogreSet = (OgreSet) set;

                try
                {
                    //...from ogre
                    ogreSet.OSMLoader.DestroyAll();
                    this.ogreApp.SceneManager.SetSkyBox(false, "");
                    if (ogreSet.ParticleSystem != null)
                    {
                        this.ogreApp.SceneManager.DestroyParticleSystem(ogreSet.ParticleSystem);
                    }

                    //dispose set
                    set.Destroy();
                    this.ogreApp.SceneManager.RootSceneNode.RemoveAndDestroyChild(ogreSet.Node.Name);
                }
                catch (SEHException)
                {
                    if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                    else throw;
                }

                //...from internal db
                this.sets.Remove(ogreSet);
            }
        }

        #endregion

        #endregion

        #region Protected Methods

        protected virtual void LoadSetElements(OgreSet set)
        {
            //particle system
            if (set.ResourceAsset.ParticleSystem != "")
            {
                ParticleSystem particleSystem =
                    this.ogreApp.SceneManager.CreateParticleSystem(
                        set.ResourceAsset.IdToken, set.ResourceAsset.ParticleSystem);
                set.OSMLoader.Node.AttachObject(particleSystem);
            }

            //skybox
            if (set.ResourceAsset.SkyBox != "") this.ogreApp.SceneManager.SetSkyBox(true, set.ResourceAsset.SkyBox);
        }

        protected virtual SceneNode CreateAssetSceneNode(ResourceAsset resource)
        {
            try
            {
                string assetId = resource.IdToken + assetCounter;
                SceneNode node = this.ogreApp.SceneManager.RootSceneNode.CreateChildSceneNode(assetId);

                //initial asset parametres
                node.Position = Vector3.ZERO;
                node.SetScale((OgreScale) resource.Scale);
                node.Translate((OgrePosition) resource.Translation);
                node.SetDirection((OgreDirection) resource.Direction);

                return node;
            }
            catch (SEHException)
            {
                if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                else throw;
            }

            return null;
        }

        protected virtual void RefreshAllGraphics()
        {
            this.RefreshBackground();
            this.RefreshLights();
            this.RefreshShadows();
        }

        protected virtual void RefreshBackground()
        {
            try
            {
                this.ogreApp.Camera.Viewport.BackgroundColour =
                    this.GraphicsConfig.BackgroundColor;
            }
            catch (SEHException)
            {
                if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                else throw;
            }
        }

        protected virtual void RefreshShadows()
        {
            try
            {
                //shadows
                this.ogreApp.SceneManager.ShadowTechnique = this.GraphicsConfig.Shadows ?
                    ShadowTechnique.SHADOWTYPE_STENCIL_MODULATIVE :
                    ShadowTechnique.SHADOWTYPE_NONE;

                this.ogreApp.SceneManager.ShadowColour = this.GraphicsConfig.ShadowsColor;
            }
            catch (SEHException)
            {
                if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                else throw;
            }
        }

        protected virtual void RefreshLights()
        {
            try
            {
                //lights
                this.ogreApp.SceneManager.AmbientLight = this.GraphicsConfig.AmbientColor;
            }
            catch (SEHException)
            {
                if (OgreException.IsThrown) this.ogreApp.ThrowOgreException();
                else throw;
            }
        }

        protected virtual void AppWindowChanged(object sender, EventArgs e)
        {
            this.RefreshBackground();
        }

        #region Event Handling Methods

        private void graphicsConfig_ShadowsChanged(object sender, EventArgs e)
        {
            this.RefreshShadows();
        }

        private void graphicsConfig_ShadowsColorChanged(object sender, EventArgs e)
        {
            this.RefreshShadows();
        }

        private void graphicsConfig_BackgroundColorChanged(object sender, EventArgs e)
        {
            this.RefreshBackground();
        }

        private void graphicsConfig_AmbientColorChanged(object sender, EventArgs e)
        {
            this.RefreshLights();
        }

        private void graphicsConfig_LightsChanged(object sender, EventArgs e)
        {
            this.RefreshLights();
        }

        #endregion

        #endregion
    }
}