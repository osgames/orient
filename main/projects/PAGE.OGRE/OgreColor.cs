/* ****************************************************
 * Name: Color.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/30 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;
using Mogre;
using PAGE.Generic;

namespace PAGE.OGRE
{
    /// <summary>
    /// Represents an Ogre color in ARGB space.
    /// </summary>
    [XmlRoot(ElementName = "color", IsNullable = false)]
    public class OgreColor : Color
    {
        #region Fields

        protected ColourValue color = ColourValue.Black; 

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the color's x component.
        /// </summary>
        [XmlAttribute("r")]
        public float R
        {
            get { return this.color.r; }
            set { this.color.r = value; }
        }

        /// <summary>
        /// Gets or sets the color's g component.
        /// </summary>
        [XmlAttribute("g")]
        public float G
        {
            get { return this.color.g; }
            set { this.color.g = value; }
        }

        /// <summary>
        /// Gets or sets the color's b component.
        /// </summary>
        [XmlAttribute("b")]
        public float B
        {
            get { return this.color.b; }
            set { this.color.b = value; }
        }

        /// <summary>
        /// Gets or sets the color's a component.
        /// </summary>
        [XmlAttribute("a")]
        public float A
        {
            get { return this.color.a; }
            set { this.color.a = value; }
        }

        /// <summary>
        /// Gets the ColourValue representing this color.
        /// </summary>
        [XmlIgnore]
        public ColourValue Color
        {
            get { return color; }
            set { color = value; }
        } 

        #endregion

        #region Constructors

        public OgreColor(){}

        public OgreColor(ColourValue color)
        {
            this.color = color;
        }

        public OgreColor(float r, float g, float b, float a)
        {
            this.color = new ColourValue(r, g, b, a);
        }

        public OgreColor(float r, float g, float b)
            : this(r, g, b, 1)
        {
        }

        #endregion

        #region Static Operators

        public static implicit operator OgreColor(ColourValue color)
        {
            return new OgreColor(color);
        }

        public static implicit operator ColourValue(OgreColor color)
        {
            return color == null ? new ColourValue() : color.Color;
        }

        #endregion

        #region Serialize Methods

        public override void ReadXml(XmlElement element)
        {
            this.R = Convert.ToSingle(element.GetAttribute("r"),
                CultureInfo.InvariantCulture);
            this.G = Convert.ToSingle(element.GetAttribute("g"),
                CultureInfo.InvariantCulture);
            this.B = Convert.ToSingle(element.GetAttribute("b"),
                CultureInfo.InvariantCulture);
            this.A = Convert.ToSingle(element.GetAttribute("a"),
                CultureInfo.InvariantCulture);
        }

        public override void WriteXml(XmlElement element)
        {
            element.SetAttribute("r", Convert.ToString(this.R, CultureInfo.InvariantCulture));
            element.SetAttribute("g", Convert.ToString(this.G, CultureInfo.InvariantCulture));
            element.SetAttribute("b", Convert.ToString(this.B, CultureInfo.InvariantCulture));
            element.SetAttribute("a", Convert.ToString(this.A, CultureInfo.InvariantCulture));
        }

        #endregion
    }
}