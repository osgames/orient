using System;
using System.Windows.Forms;
using MOIS;
using MQuickGUI;
using PAGE.OGRE.GUI;
using MouseEventArgs=System.Windows.Forms.MouseEventArgs;

namespace PAGE.OGRE.GUI
{
    public class WindowsFormsGUIManager : MQuickGUIManager
    {
        #region Fields

        //the managed win forms control
        protected Control control = null;

        #endregion

        #region Constructor

        public WindowsFormsGUIManager(OgreApplication ogreApp) : base(ogreApp)
        {
            //tests mouse and keyboard
            if((ogreApp == null) || (ogreApp.Control == null))
            {
                throw new ArgumentException("Invalid app given, control can't be null", 
                                            "ogreApp");
            }

            this.control = ogreApp.Control;

            //handles control events
            this.control.MouseMove += ControlMouseMoved;
            this.control.MouseDown += ControlMouseDown;
            this.control.MouseUp += ControlMouseUp;
        }

        #endregion

        #region Protected Methods

        protected void ControlMouseUp(object sender, MouseEventArgs e)
        {
            if (!this.handleInput) return;

            //sets mouse button up information
            if (e.Button == MouseButtons.Right)
                GUIManager.Singleton.InjectMouseButtonUp(MouseButtonID.MB_Right);
            else if (e.Button == MouseButtons.Left)
                GUIManager.Singleton.InjectMouseButtonUp(MouseButtonID.MB_Left);
            else if (e.Button == MouseButtons.Middle)
                GUIManager.Singleton.InjectMouseButtonUp(MouseButtonID.MB_Middle);
        }

        protected void ControlMouseDown(object sender, MouseEventArgs e)
        {
            if (!this.handleInput) return;

            //sets mouse button down information
            if (e.Button == MouseButtons.Right)
                GUIManager.Singleton.InjectMouseButtonDown(MouseButtonID.MB_Right);
            else if (e.Button == MouseButtons.Left)
                GUIManager.Singleton.InjectMouseButtonDown(MouseButtonID.MB_Left);
            else if (e.Button == MouseButtons.Middle)
                GUIManager.Singleton.InjectMouseButtonDown(MouseButtonID.MB_Middle);
        }

        protected void ControlMouseMoved(object sender, MouseEventArgs e)
        {
            if (!this.handleInput) return;

            //sets mouse position information
            GUIManager.Singleton.InjectMousePosition(e.X, e.Y);
        }

        #endregion
    }
}