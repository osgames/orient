/* ****************************************************
 * Name: OgreStaticDisplayText.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/26 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using Mogre;
using PAGE.Generic;
using PAGE.Generic.GUI;
using PAGE.Generic.Math;
using PAGE.OGRE.Math;

namespace PAGE.OGRE.GUI
{
    public class OgreStaticDisplayText : IStaticDisplayText
    {
        #region Fields

        protected static uint counter = 0;
        protected OverlayContainer panelOverlay = null;
        protected TextAreaOverlayElement textOverlay = null;
        protected TextAreaOverlayElement textShadowOverlay = null;
        protected Overlay overlay = null;
        protected int lineLenght = 50; 

        #endregion

        #region Properties

        public ScreenPosition Position
        {
            get { return new OgreScreenPosition(this.panelOverlay.Left, this.panelOverlay.Top); }
            set
            {
                if (!(value is OgreScreenPosition))
                    throw new ArgumentException("given value is not OgreScreenPosition");

                this.panelOverlay.SetPosition(
                    ((OgreScreenPosition) value).X, ((OgreScreenPosition) value).Y);
            }
        }

        public int LineLenght
        {
            get { return lineLenght; }
            set { lineLenght = value; }
        }

        public string Text
        {
            get { return this.textOverlay.Caption; }
            set
            {
                string correctText = this.CorrectText(value);
                this.textOverlay.Caption = correctText;
                this.textShadowOverlay.Caption = correctText;
            }
        }

        public bool Visible
        {
            get { return this.overlay.IsVisible; }
            set
            {
                if (value) this.overlay.Show();
                else this.overlay.Hide();
            }
        }

        public float CharHeight
        {
            get { return this.textOverlay.CharHeight; }
            set { 
                this.textOverlay.CharHeight = value;
                this.textShadowOverlay.CharHeight = value;
            }
        }

        public Color Color
        {
            get { return new OgreColor(this.textOverlay.ColourTop); }
            set
            {
                if (!(value is OgreColor))
                    throw new ArgumentException("given value is not OgreColor");

                this.textOverlay.ColourTop =
                    this.textOverlay.ColourBottom =
                    this.textOverlay.Colour = (OgreColor)value;
            }
        }

        public bool Shadow
        {
            get { return this.textShadowOverlay.IsVisible; }
            set
            {
                if (value) this.textShadowOverlay.Show();
                else this.textShadowOverlay.Hide();
            }
        }

        public TextAlignment Alignment
        {
            get
            {
                switch (this.textOverlay.GetAlignment())
                {
                    case TextAreaOverlayElement.Alignment.Center:
                        return TextAlignment.Center;
                    case TextAreaOverlayElement.Alignment.Left:
                        return TextAlignment.Left;
                    case TextAreaOverlayElement.Alignment.Right:
                        return TextAlignment.Right;
                }
                return TextAlignment.Left;
            }
            set
            {
                switch (value)
                {
                    case TextAlignment.Center:
                        this.textOverlay.SetAlignment(TextAreaOverlayElement.Alignment.Center);
                        this.textShadowOverlay.SetAlignment(TextAreaOverlayElement.Alignment.Center);
                        break;
                    case TextAlignment.Left:
                        this.textOverlay.SetAlignment(TextAreaOverlayElement.Alignment.Left);
                        this.textShadowOverlay.SetAlignment(TextAreaOverlayElement.Alignment.Left);
                        break;
                    case TextAlignment.Right:
                        this.textOverlay.SetAlignment(TextAreaOverlayElement.Alignment.Right);
                        this.textShadowOverlay.SetAlignment(TextAreaOverlayElement.Alignment.Right);
                        break;
                }
            }
        }
        #endregion

        #region Constructor

        public OgreStaticDisplayText(string text)
        {
            string uniqueString = "StaticText/" + counter++;

            //creates panel
            this.panelOverlay = (OverlayContainer)OverlayManager.Singleton.CreateOverlayElement("Panel", uniqueString + "/Panel");
            this.panelOverlay.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            this.panelOverlay.SetPosition(0, 0);
            this.panelOverlay.SetDimensions(500, 100);

            //creates main text area with properties
            this.textShadowOverlay = (TextAreaOverlayElement)OverlayManager.Singleton.CreateOverlayElement("TextArea", uniqueString + "/Text");
            this.textShadowOverlay.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            this.textShadowOverlay.SetPosition(0.8f, 0.8f);
            this.textShadowOverlay.SetDimensions(50, 50);
            this.textShadowOverlay.Caption = text;
            this.textShadowOverlay.CharHeight = 30;
            this.textShadowOverlay.SetAlignment(TextAreaOverlayElement.Alignment.Center);
            this.textShadowOverlay.FontName = "SpeechFont";

            //creates text shadow area with properties, has to come first
            this.textOverlay = (TextAreaOverlayElement)OverlayManager.Singleton.CreateOverlayElement("TextArea", uniqueString + "/TextShadow");
            this.textOverlay.MetricsMode = GuiMetricsMode.GMM_PIXELS;
            this.textOverlay.SetPosition(0, 0);
            this.textOverlay.SetDimensions(50, 50);
            this.textOverlay.Caption = text;
            this.textOverlay.CharHeight = 30;
            this.textOverlay.SetAlignment(TextAreaOverlayElement.Alignment.Center);
            this.textOverlay.FontName = "SpeechFont";

            this.textShadowOverlay.ColourTop = this.textShadowOverlay.ColourBottom =
                                               this.textShadowOverlay.Colour = 
                                               new ColourValue(0, 0, 0);
            this.textOverlay.ColourTop = this.textOverlay.ColourBottom = 
                                         this.textOverlay.Colour =
                                         new ColourValue(1, 1, 1);
            

            this.panelOverlay.AddChild(this.textOverlay);
            this.panelOverlay.AddChild(this.textShadowOverlay);
            

            //creates overlay, and add the panel
            this.overlay = OverlayManager.Singleton.Create(uniqueString + "/Overlay");
            this.overlay.Add2D(this.panelOverlay);
            this.overlay.Show();
        } 
        #endregion

        #region Private Methods

        protected string CorrectText(string text)
        {
            string finalString = "";
            int charLineCount = 0;
            string[] words = text.Split(new char[] { ' ' });
            foreach (string word in words)
            {
                if (charLineCount + word.Length > this.lineLenght)
                {
                    finalString += "\n";
                    charLineCount = 0;
                }
                finalString += word + " ";
                charLineCount += word.Length + 1;
            }
            return finalString;
        }
        #endregion
    }
}