/* ****************************************************
 * Name: OgreFollowCharacterText.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/26 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using Mogre;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.GUI;
using PAGE.Generic.Math;
using PAGE.OGRE.Domain.Assets;
using PAGE.OGRE.Domain.Cameras;
using PAGE.OGRE.Math;

namespace PAGE.OGRE.GUI
{
    /// <summary>
    /// Represents a subtitle 2D text that follows a character and its displayed above
    /// the character's head.
    /// </summary>
    public class OgreFollowAssetText : OgreStaticDisplayText, IFollowAssetText
    {
        #region Fields

        protected OgreCamera camera;
        protected IOgreInteractionAsset asset;
        protected OgrePosition relativeTextPosition;

        #endregion

        #region Properties

        public IInteractionAsset Asset
        {
            get { return this.asset; }
        }

        public Position RelativeTextPosition
        {
            get { return this.relativeTextPosition; }
            set
            {
                if (value is OgrePosition)
                    this.relativeTextPosition = (OgrePosition) value;
            }
        }

        protected bool IsCameraFacingCharacter
        {
            get
            {
                Vector3 dirCharacterCam = (OgreDirection)
                    this.asset.AbsolutePosition.DirectionTo(this.camera.AbsolutePosition);
                return dirCharacterCam.DotProduct(
                    ((OgreDirection) this.camera.AbsoluteDirection).Vector3) < 0;
            }
        }

        #endregion

        #region Constructor

        public OgreFollowAssetText(IOgreInteractionAsset asset, OgreCamera cam)
            : base("")
        {
            this.asset = asset;
            this.camera = cam;
        }

        #endregion

        #region Public Members

        /// <summary>
        /// Updates text position in screen according to node position in 
        /// the world.
        /// </summary>
        /// <param name="millisSinceLastUpdate"></param>
        public virtual void Update(float millisSinceLastUpdate)
        {
            if (!this.Visible) return;

            //checks wether camera is facing the character
            if(!this.IsCameraFacingCharacter)
            {
                this.Position = new OgreScreenPosition(float.MinValue, float.MinValue);
                return;
            }

            //calculates the 3D point where the text target is situated
            Vector3 targetPoint =
                this.asset.Node.Position + this.relativeTextPosition;

            //calculates point in 2D screen "normalized" coordinates
            Matrix4 mat = this.camera.Camera.GetViewMatrix(false);
            Vector3 screenPoint = mat*targetPoint;

            //sets text in screen absolute coordinates
            float width = this.camera.Camera.Viewport.ActualWidth;
            float height = this.camera.Camera.Viewport.ActualHeight;
            float x = (-(screenPoint.x/screenPoint.z) + .5f)*width;
            float y = ((screenPoint.y/screenPoint.z) + .5f)*height;
            this.Position = new OgreScreenPosition(x, y);
        }

        #endregion
    }
}
