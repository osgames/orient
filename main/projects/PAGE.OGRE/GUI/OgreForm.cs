/* ****************************************************
 * Name: OgreForm.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/01/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using MQuickGUI;
using PAGE.Generic.GUI;
using GUIManager=MQuickGUI.GUIManager;

namespace PAGE.OGRE.GUI
{
    public abstract class OgreForm : Form
    {
        #region Fields

        protected Sheet sheet; //the form's MQuickGUI sheet
        protected static int FormCount = 1;

        #endregion

        #region Properties

        public override bool Visible
        {
            get
            {
                this.VerifyDestruction();
                return this.sheet.Visible;
            }
            set
            {
                this.VerifyDestruction();
                if (value) this.sheet.Show();
                else this.sheet.Hide();
            }
        }

        /// <summary>
        /// Gets the form's MQuickGUI Sheet object.
        /// </summary>
        public Sheet Sheet
        {
            get { return sheet; }
        }

        #endregion

        #region Constructor

        public OgreForm(string name) : base(name)
        {
            string uniqueID = "OgreForm" + this.name + FormCount++;
            this.sheet = GUIManager.Singleton.CreateSheet(uniqueID);
            this.sheet.Hide();
        }

        #endregion

        #region Public Methods

        public override void Activate()
        {
            GUIManager.Singleton.SetActiveSheet(this.sheet);
        }

        public override void Destroy()
        {
            GUIManager.Singleton.DestroySheet(this.sheet);
            base.Destroy();
        }

        #endregion
    }
}