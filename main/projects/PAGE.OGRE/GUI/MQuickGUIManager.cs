/* ****************************************************
 * Name: MQuickGUIManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/01/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using Mogre;
using MQuickGUI;
using GUIManager=PAGE.Generic.GUI.GUIManager;

namespace PAGE.OGRE.GUI
{
    public abstract class MQuickGUIManager : GUIManager
    {
        #region Fields

        //the gui's mouse cursor
        protected MouseCursor mouseCursor = null;

        //the managed ogreApp
        protected OgreApplication ogreApp = null;

        #endregion

        #region Properties

        public override bool CursorVisible
        {
            get { return this.mouseCursor.Visible; }
            set
            {
                if (value) this.mouseCursor.Show();
                else this.mouseCursor.Hide();
            }
        } 

        #endregion

        #region Constructor

        public MQuickGUIManager(OgreApplication ogreApp)
        {
            //tests ogre app
            if (ogreApp == null)
            {
                throw new ArgumentException("Invalid app given", "ogreApp");
            }

            this.ogreApp = ogreApp;

            //creates mouse cursor
            this.mouseCursor = MQuickGUI.GUIManager.Singleton.CreateMouseCursor(
                new Vector2(20, 20), "qgui.cursor");
            this.mouseCursor.Hide();

            //updates gui window size
            this.WindowResized(ogreApp.WindowWidth, ogreApp.WindowHeight);

            //handles ogre app events
            ogreApp.WindowResized += OgreAppWindowResized;
        } 

        #endregion

        #region Protected Methods

        /// <summary>
        /// Called when ogre app window is resized. Updates gui's window size.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Window size event args.</param>
        protected void OgreAppWindowResized(object sender, WindowResizedEventArgs e)
        {
            //updates window size information
            this.WindowResized(e.Width, e.Height);
        }

        protected override void WindowResized(uint width, uint heigth)
        {
            MQuickGUI.GUIManager.Singleton._notifyWindowDimensions((int)width, (int)heigth);
        }

        public override void Update(float millisSinceLastUpdate)
        {
            //updates input information
            MQuickGUI.GUIManager.Singleton.InjectTime(millisSinceLastUpdate / 1000f);

            if( (this.ogreApp.WindowWidth != MQuickGUI.GUIManager.Singleton.RenderWindowWidth) ||
                (this.ogreApp.WindowHeight != MQuickGUI.GUIManager.Singleton.RenderWindowHeight))
                this.WindowResized(this.ogreApp.WindowWidth, this.ogreApp.WindowHeight);
        }


        #endregion
    }
}