/* ****************************************************
 * Name: MOISGUIManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/01/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using MOIS;

namespace PAGE.OGRE.GUI
{
    public class MOISGUIManager : MQuickGUIManager
    {
        #region Fields

        protected const int KEY_NUM = 155;
        protected const float MILLIS_BETWEEN_KEY = 150;

        protected bool[] mouseButtonsDown = {false, false, false};
        protected bool[] keysDown = new bool[KEY_NUM];
        protected bool wasKeyDown = false;
        protected DateTime keyDownTime = DateTime.Now;

        #endregion

        #region Constructor

        public MOISGUIManager(OgreApplication ogreApp) : base (ogreApp)
        {
            //tests mouse and keyboard
            if((ogreApp == null) || (ogreApp.Keyboard == null) || (ogreApp.Mouse == null))
            {
                throw new ArgumentException("Invalid ogre app given", "ogreApp");
            }

            for (int i = 0; i < KEY_NUM; i++) this.keysDown[i] = false;
        }

        #endregion

        #region Protected Methods

        public override void Update(float millisSinceLastUpdate)
        {
            base.Update(millisSinceLastUpdate);

            if(this.handleInput)
            {
                MQuickGUI.GUIManager.Singleton.InjectTime(millisSinceLastUpdate / 1000f);
                this.UpdateMouseState();
                this.UpdateKeyboardState();
            }
        }

        /// <summary>
        /// Injects MOIS mouse information into MQuickGUI.
        /// </summary>
        protected void UpdateKeyboardState()
        {
            //if a key was hit and interval didn't yet passed, return
            if(this.wasKeyDown)
            {
                TimeSpan ts = DateTime.Now.Subtract(this.keyDownTime);
                if (ts.TotalMilliseconds < MILLIS_BETWEEN_KEY) return;
            }

            this.wasKeyDown = false;

            //injects mouse button information
            for (int i = 0; i < KEY_NUM; i++)
            {
                if (this.ogreApp.Keyboard.IsKeyDown((KeyCode)i))
                {
                    char c;
                    if (IsModifierKey((KeyCode)i))
                    {
                        continue;   //ignore modifier keys
                    }
                    else if ((c = IsTextElement((KeyCode)i)) != '\0')
                    {
                        MQuickGUI.GUIManager.Singleton.InjectChar(c);
                    }
                    else if (IsSpecialKey((KeyCode)i))
                    {
                        MQuickGUI.GUIManager.Singleton.InjectKeyDown((KeyCode) i);
                    }
                    else if (this.ogreApp.Keyboard.IsModifierDown(Keyboard.Modifier.Shift))
                    {
                        MQuickGUI.GUIManager.Singleton.InjectChar(this.ogreApp.Keyboard.GetAsString((KeyCode)i).ToUpper()[0]);
                    }
                    else
                    {
                        MQuickGUI.GUIManager.Singleton.InjectChar(this.ogreApp.Keyboard.GetAsString((KeyCode)i).ToLower()[0]);
                    }

                    this.keysDown[i] = true;
                    this.keyDownTime = DateTime.Now;
                    this.wasKeyDown = true;

                    return;         //do not allow multiple keys at the same time
                }
                else
                {
                    //only inject key up if key was down
                    if (this.keysDown[i]) 
                        MQuickGUI.GUIManager.Singleton.InjectKeyUp((KeyCode) i);
                    this.keysDown[i] = false;
                }
            }
        }

        protected static char IsTextElement(KeyCode keyCode)
        {
            switch(keyCode)
            {
                case KeyCode.KC_SPACE:
                    return ' ';
                case KeyCode.KC_PERIOD:
                    return '.';
                case KeyCode.KC_COMMA:
                    return ',';
                case KeyCode.KC_COLON:
                    return ':';
            }
            return '\0';
        }

        protected static bool IsSpecialKey(KeyCode keyCode)
        {
            return 
                (keyCode == KeyCode.KC_BACK) ||
                (keyCode == KeyCode.KC_RETURN) ||
                (keyCode == KeyCode.KC_RIGHT) ||
                (keyCode == KeyCode.KC_LEFT) ||
                (keyCode == KeyCode.KC_UP) ||
                (keyCode == KeyCode.KC_DOWN) ||
                (keyCode == KeyCode.KC_DELETE) ||
                (keyCode == KeyCode.KC_HOME) ||
                (keyCode == KeyCode.KC_END) ||
                (keyCode == KeyCode.KC_PGUP) ||
                (keyCode == KeyCode.KC_PGDOWN) ||
                (keyCode == KeyCode.KC_SYSRQ) ||
                (keyCode == KeyCode.KC_TAB) ||
                (keyCode == KeyCode.KC_ESCAPE);
        }

        protected static bool IsModifierKey(KeyCode keyCode)
        {
            return
                (keyCode == KeyCode.KC_RSHIFT) ||
                (keyCode == KeyCode.KC_LSHIFT) ||
                (keyCode == KeyCode.KC_LCONTROL) ||
                (keyCode == KeyCode.KC_RCONTROL) ||
                (keyCode == KeyCode.KC_ABNT_C1) ||
                (keyCode == KeyCode.KC_APPS) ||
                (keyCode == KeyCode.KC_AX) ||
                (keyCode == KeyCode.KC_CAPITAL) ||
                (keyCode == KeyCode.KC_LMENU) ||
                (keyCode == KeyCode.KC_RMENU) ||
                (keyCode == KeyCode.KC_GRAVE) ||
                (keyCode == KeyCode.KC_ABNT_C2);
        }

        /// <summary>
        /// Injects MOIS mouse information into MQuickGUI.
        /// </summary>
        protected void UpdateMouseState()
        {
            //gets MOIS mouse information
            MouseState_NativePtr mouseState = this.ogreApp.Mouse.MouseState;
            mouseState.height = (int)this.ogreApp.WindowHeight;
            mouseState.width = (int)this.ogreApp.WindowWidth;

            //injects mouse position
            MQuickGUI.GUIManager.Singleton.InjectMousePosition(mouseState.X.abs, mouseState.Y.abs);

            //injects mouse button information
            for (int i = 2; i >= 0; i--)
            {
                if (mouseState.ButtonDown((MouseButtonID)i))
                {
                    MQuickGUI.GUIManager.Singleton.InjectMouseButtonDown((MouseButtonID)i);
                    this.mouseButtonsDown[i] = true;
                }
                else
                {
                    //only inject mouse up if mouse was down
                    if(this.mouseButtonsDown[i])
                        MQuickGUI.GUIManager.Singleton.InjectMouseButtonUp((MouseButtonID)i);
                    this.mouseButtonsDown[i] = false;
                }
            }
        }

        #endregion
    }
}