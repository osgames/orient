/* ****************************************************
 * Name: OgreAngle.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/02
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Globalization;
using System.Xml;
using Mogre;
using Angle=PAGE.Generic.Math.Angle;

namespace PAGE.OGRE.Math
{
    /// <summary>
    /// Represents an Mogre angle in degrees.
    /// </summary>
    public class OgreAngle : Angle
    {
        #region Fields

        protected Degree value = new Degree(0);

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the degree value representing this angle.
        /// </summary>
        public Degree Value
        {
            get { return value; }
            set { this.value = value; }
        } 

        #endregion

        #region Constructors

        public OgreAngle(){}

        public OgreAngle(OgreDirection direction1, OgreDirection direction2)
        {
            this.value = (OgreAngle)direction1.AngleBetween(direction2);
        }

        public OgreAngle(Degree value)
        {
            this.value = value;
        }

        public OgreAngle(float value)
        {
            this.value = new Degree(value);
        }

        #endregion

        #region Static Operators

        public static implicit operator OgreAngle(float value)
        {
            return new OgreAngle(value);
        }

        public static implicit operator float(OgreAngle angle)
        {
            return angle == null ? 0 : angle.Value.ValueDegrees;
        }

        public static implicit operator OgreAngle(Degree value)
        {
            return new OgreAngle(value);
        }

        public static implicit operator Degree(OgreAngle angle)
        {
            return angle == null ? 0 : angle.Value;
        }

        #endregion

        #region Public Methods

        public override int CompareTo(object obj)
        {
            if (!(obj is OgreAngle))
                throw new ArgumentException("Object is not an OgreAngle.");

            return this.Value.CompareTo(((OgreAngle)obj).Value);
        }

        public override void ReadXml(XmlElement element)
        {
            this.value = Convert.ToSingle(element.GetAttribute("value"),
                CultureInfo.InvariantCulture);
        }

        public override void WriteXml(XmlElement element)
        {
            element.SetAttribute("value", Convert.ToString(this.value,
                 CultureInfo.InvariantCulture));
        }

        #endregion
    }
}