/* ****************************************************
 * Name: OgreScreenPosition.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/14
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Xml.Serialization;
using Mogre;
using PAGE.Generic.Math;

namespace PAGE.OGRE.Math
{
    /// <summary>
    /// Represents an Mogre screen position in 2D space coordinates.
    /// </summary>
    [XmlRoot(ElementName = "screen-position", IsNullable = false)]
    public class OgreScreenPosition : ScreenPosition
    {
        #region Fields

        protected Vector2 vector2 = Vector2.UNIT_X; 

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the position's x component.
        /// </summary>
        [XmlAttribute("x")]
        public float X
        {
            get { return this.vector2.x; }
            set { this.vector2.x = value; }
        }

        /// <summary>
        /// Gets or sets the position's y component.
        /// </summary>
        [XmlAttribute("y")]
        public float Y
        {
            get { return this.vector2.y; }
            set { this.vector2.y = value; }
        }

        /// <summary>
        /// Gets the Vector2 representing this position.
        /// </summary>
        [XmlIgnore]
        public Vector2 Vector2
        {
            get { return vector2; }
            set { vector2 = value; }
        } 

        #endregion

        #region Constructors

        public OgreScreenPosition(){}

        public OgreScreenPosition(Vector2 position)
        {
            this.vector2 = position;
        }

        public OgreScreenPosition(float x, float y)
        {
            this.vector2 = new Vector2(x, y);
        }

        #endregion

        #region Static Operators

        public static implicit operator OgreScreenPosition(Vector2 vec)
        {
            return new OgreScreenPosition(vec);
        }

        public static implicit operator Vector2(OgreScreenPosition position)
        {
            return position == null ? new Vector2() : position.Vector2;
        }

        #endregion

        #region Public Methods

        public override Distance DistanceTo(ScreenPosition position)
        {
            if (!(position is OgreScreenPosition))
                throw new ArgumentException("position is not an OgreScreenPosition.");

            Vector2 dif = ((OgreScreenPosition) position) - this.vector2;
            return new OgreDistance(dif.Normalise());
        }

        public override ScreenPosition Add(ScreenPosition position)
        {
            if (!(position is OgreScreenPosition))
                throw new ArgumentException("position is not an OgreScreenPosition.");

            return new OgreScreenPosition(this.vector2 + (OgreScreenPosition) position);
        }

        public override ScreenPosition Subtract(ScreenPosition position)
        {
            if (!(position is OgreScreenPosition))
                throw new ArgumentException("position is not an OgreScreenPosition.");

            return new OgreScreenPosition(this.vector2 - (OgreScreenPosition) position);
        }

        #endregion
    }
}
