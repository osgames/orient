/* ****************************************************
 * Name: OgrePosition.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/31
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;
using Mogre;
using PAGE.Generic.Math;

namespace PAGE.OGRE.Math
{
    /// <summary>
    /// Represents a position in 3D space coordinates.
    /// </summary>
    public class OgrePosition : Position
    {
        #region Fields

        protected Vector3 vector3 = Vector3.ZERO; 

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the position's x component.
        /// </summary>
        public float X
        {
            get { return this.vector3.x; }
            set { this.vector3.x = value; }
        }

        /// <summary>
        /// Gets or sets the position's y component.
        /// </summary>
        public float Y
        {
            get { return this.vector3.y; }
            set { this.vector3.y = value; }
        }

        /// <summary>
        /// Gets or sets the position's z component.
        /// </summary>
        public float Z
        {
            get { return this.vector3.z; }
            set { this.vector3.z = value; }
        }

        /// <summary>
        /// Gets the Vector3 representing this position.
        /// </summary>
        [XmlIgnore]
        public Vector3 Vector3
        {
            get { return vector3; }
            set { vector3 = value; }
        } 

        #endregion

        #region Constructors

        public OgrePosition(){}

        public OgrePosition(Vector3 position)
        {
            this.vector3 = position;
        }

        public OgrePosition(float x, float y, float z)
        {
            this.vector3 = new Vector3(x, y, z);
        }

        #endregion

        #region Static Operators

        public static implicit operator OgrePosition(Vector3 vec)
        {
            return new OgrePosition(vec);
        }

        public static implicit operator Vector3(OgrePosition position)
        {
            return position == null ? new Vector3() : position.Vector3;
        }

        #endregion

        #region Public Methods

        public override bool Equals(object obj)
        {
            if (obj is OgrePosition)
            {
                OgrePosition position = (OgrePosition)obj;
                return 
                    this.Vector3.x == position.Vector3.x &&
                    this.Vector3.y == position.Vector3.y &&
                    this.Vector3.z == position.Vector3.z;
            }
            else
            {
                throw new ArgumentException("Object is not an OgrePosition");
            }
        }

        public override Distance DistanceTo(Position position)
        {
            if (!(position is OgrePosition))
                throw new ArgumentException("position is not an OgrePosition.");

            Vector3 dif = ((OgrePosition) position) - this.vector3;
            return new OgreDistance(dif.Normalise());
        }

        public override Direction DirectionTo(Position position)
        {
            if (!(position is OgrePosition))
                throw new ArgumentException("position is not an OgrePosition.");

            Vector3 direction = ((OgrePosition)position).Vector3 - this.Vector3;
            direction.Normalise();
            return new OgreDirection(direction);
        }

        public override Position Add(Position position)
        {
            if (!(position is OgrePosition))
                throw new ArgumentException("position is not an OgrePosition.");

            return new OgrePosition(this.vector3 + (OgrePosition) position);
        }

        public override Position Subtract(Position position)
        {
            if (!(position is OgrePosition))
                throw new ArgumentException("position is not an OgrePosition.");

            return new OgrePosition(this.vector3 - (OgrePosition) position);
        }

        public override Position Invert()
        {
            return new OgrePosition(-this.vector3);
        }

        #endregion

        #region Serialize Methods

        public override void ReadXml(XmlElement element)
        {
            this.X = Convert.ToSingle(element.GetAttribute("x"),
                CultureInfo.InvariantCulture);
            this.Y = Convert.ToSingle(element.GetAttribute("y"),
                CultureInfo.InvariantCulture);
            this.Z = Convert.ToSingle(element.GetAttribute("z"),
                CultureInfo.InvariantCulture);
        }

        public override void WriteXml(XmlElement element)
        {
            element.SetAttribute("x", Convert.ToString(this.X, CultureInfo.InvariantCulture));
            element.SetAttribute("y", Convert.ToString(this.Y, CultureInfo.InvariantCulture));
            element.SetAttribute("z", Convert.ToString(this.Z, CultureInfo.InvariantCulture));
        }

        #endregion
    }
}