/* ****************************************************
 * Name: OgreDirection.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/31
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;
using Mogre;
using PAGE.Generic.Math;
using Angle=PAGE.Generic.Math.Angle;

namespace PAGE.OGRE.Math
{
    /// <summary>
    /// Represents a direction (front vector) in 3D space coordinates.
    /// </summary>
    public class OgreDirection : Direction
    {
        #region Fields

        protected Vector3 vector3 = Vector3.UNIT_Z;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the direction's x component.
        /// </summary>
        public float X
        {
            get { return this.vector3.x; }
            set { this.vector3.x = value; }
        }

        /// <summary>
        /// Gets or sets the direction's y component.
        /// </summary>
        public float Y
        {
            get { return this.vector3.y; }
            set { this.vector3.y = value; }
        }

        /// <summary>
        /// Gets or sets the direction's z component.
        /// </summary>
        public float Z
        {
            get { return this.vector3.z; }
            set { this.vector3.z = value; }
        }

        /// <summary>
        /// Gets the Vector3 representing this direction.
        /// </summary>
        [XmlIgnore]
        public Vector3 Vector3
        {
            get { return vector3; }
            set { vector3 = value; }
        }

        #endregion

        #region Constructors

        public OgreDirection(){}

        public OgreDirection(Vector3 direction)
        {
            this.vector3 = direction;
        }

        public OgreDirection(float x, float y, float z)
        {
            this.vector3 = new Vector3(x, y, z);
        }

        #endregion

        #region Static Operators

        public static implicit operator OgreDirection(Vector3 vec)
        {
            return new OgreDirection(vec);
        }

        public static implicit operator Vector3(OgreDirection direction)
        {
            return direction == null ? new Vector3() : direction.Vector3;
        }

        #endregion

        #region Public Methods

        public override bool Equals(object obj)
        {
            if (obj is OgreDirection)
            {
                OgreDirection direction = (OgreDirection)obj;
                return
                    this.Vector3.x == direction.Vector3.x &&
                    this.Vector3.y == direction.Vector3.y &&
                    this.Vector3.z == direction.Vector3.z;
            }
            else
            {
                throw new ArgumentException("Object is not an OgreDirection");
            }
        }

        public override Direction Invert()
        {
            return new OgreDirection(-this.vector3);
        }

        public override Angle AngleBetween(Direction direction)
        {
            if (!(direction is OgreDirection))
                throw new ArgumentException("direction is not an OgreDirection.");

            return new OgreAngle((float)System.Math.Acos(
                (this.vector3.DotProduct((OgreDirection)direction) /
                (this.vector3.Normalise() * ((OgreDirection)direction).Vector3.Normalise()))));
        }

        #endregion

        #region Serialize Methods

        public override void ReadXml(XmlElement element)
        {
            this.X = Convert.ToSingle(element.GetAttribute("x"),
                CultureInfo.InvariantCulture);
            this.Y = Convert.ToSingle(element.GetAttribute("y"),
                CultureInfo.InvariantCulture);
            this.Z = Convert.ToSingle(element.GetAttribute("z"),
                CultureInfo.InvariantCulture);
        }

        public override void WriteXml(XmlElement element)
        {
            element.SetAttribute("x", Convert.ToString(this.X, CultureInfo.InvariantCulture));
            element.SetAttribute("y", Convert.ToString(this.Y, CultureInfo.InvariantCulture));
            element.SetAttribute("z", Convert.ToString(this.Z, CultureInfo.InvariantCulture));
        }

        #endregion
    }
}
