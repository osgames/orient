/* ****************************************************
 * Name: OgreOrientation.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/31
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;
using Mogre;
using PAGE.Generic.Math;

namespace PAGE.OGRE.Math
{
    /// <summary>
    /// Represents an orientation in 3D space using a quaternion.
    /// </summary>
    [XmlRoot(ElementName = "orientation", IsNullable = false)]
    public class OgreOrientation : Orientation
    {
        #region Fields

        protected Quaternion quaternion = Quaternion.IDENTITY; 

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the orientation's x component.
        /// </summary>
        [XmlAttribute("x")]
        public float X
        {
            get { return this.quaternion.x; }
            set { this.quaternion.x = value; }
        }

        /// <summary>
        /// Gets or sets the orientation's y component.
        /// </summary>
        [XmlAttribute("y")]
        public float Y
        {
            get { return this.quaternion.y; }
            set { this.quaternion.y = value; }
        }

        /// <summary>
        /// Gets or sets the orientation's z component.
        /// </summary>
        [XmlAttribute("z")]
        public float Z
        {
            get { return this.quaternion.z; }
            set { this.quaternion.z = value; }
        }

        /// <summary>
        /// Gets or sets the orientation's z component.
        /// </summary>
        [XmlAttribute("w")]
        public float W
        {
            get { return this.quaternion.w; }
            set { this.quaternion.w = value; }
        }

        /// <summary>
        /// Gets the Quaternion representing this orientation.
        /// </summary>
        [XmlIgnore]
        public Quaternion Quaternion
        {
            get { return quaternion; }
            set { quaternion = value; }
        } 

        #endregion

        #region Constructors

        public OgreOrientation(){}

        public OgreOrientation(Quaternion orientation)
        {
            this.quaternion = orientation;
        }

        public OgreOrientation(float fW, float fX, float fY, float fZ)
        {
            this.quaternion = new Quaternion(fW, fX, fY, fZ);
        }

        #endregion

        #region Static Operators

        public static implicit operator OgreOrientation(Quaternion quat)
        {
            return new OgreOrientation(quat);
        }

        public static implicit operator Quaternion(OgreOrientation orientation)
        {
            return orientation == null ? new Quaternion() : orientation.Quaternion;
        }

        #endregion

        #region Public Methods

        public override bool Equals(object obj)
        {
            if (obj is OgreOrientation)
            {
                OgreOrientation orientation = (OgreOrientation)obj;
                return
                    this.Quaternion.x == orientation.Quaternion.x &&
                    this.Quaternion.y == orientation.Quaternion.y &&
                    this.Quaternion.z == orientation.Quaternion.z &&
                    this.Quaternion.w == orientation.Quaternion.w;
            }
            else
            {
                throw new ArgumentException("Object is not an OgreOrientation");
            }
        }

        public override Direction Orientate(Direction direction)
        {
            if (!(direction is OgreDirection))
                throw new ArgumentException("direction is not an OgreDirection.");

            return new OgreDirection(this.quaternion * (OgreDirection)direction);
        }

        public override Position Orientate(Position position)
        {
            if (!(position is OgrePosition))
                throw new ArgumentException("position is not an OgrePosition.");

            return new OgrePosition(this.quaternion * (OgrePosition)position);
        }

        public override Orientation Add(Orientation orientation)
        {
            if (!(orientation is OgreOrientation))
                throw new ArgumentException("orientation is not an OgreOrientation.");

            return new OgreOrientation((OgreOrientation) orientation*this.quaternion);
        }

        #endregion

        #region Serialize Methods

        public override void ReadXml(XmlElement element)
        {
            this.X = Convert.ToSingle(element.GetAttribute("x"),
                CultureInfo.InvariantCulture);
            this.Y = Convert.ToSingle(element.GetAttribute("y"),
                CultureInfo.InvariantCulture);
            this.Z = Convert.ToSingle(element.GetAttribute("z"),
                CultureInfo.InvariantCulture);
            this.W = Convert.ToSingle(element.GetAttribute("w"),
                CultureInfo.InvariantCulture);
        }

        public override void WriteXml(XmlElement element)
        {
            element.SetAttribute("x", Convert.ToString(this.X, CultureInfo.InvariantCulture));
            element.SetAttribute("y", Convert.ToString(this.Y, CultureInfo.InvariantCulture));
            element.SetAttribute("z", Convert.ToString(this.Z, CultureInfo.InvariantCulture));
            element.SetAttribute("w", Convert.ToString(this.W, CultureInfo.InvariantCulture));
        }

        #endregion
    }
}