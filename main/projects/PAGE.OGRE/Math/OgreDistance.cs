/* ****************************************************
 * Name: OgreDistance.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/02
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Globalization;
using System.Xml;
using PAGE.Generic.Math;

namespace PAGE.OGRE.Math
{
    /// <summary>
    /// Represents a distance in 3D space coordinates.
    /// </summary>
    public class OgreDistance : Distance
    {
        #region Fields

        protected double value = 0;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the double value representing this distance.
        /// </summary>
        public double Value
        {
            get { return value; }
            set { this.value = value > 0 ? value : 0; }
        } 

        #endregion

        #region Constructors

        public OgreDistance() { }

        public OgreDistance(OgrePosition position1, OgrePosition position2)
        {
            this.Value = (OgreDistance)position1.DistanceTo(position2);
        }

        public OgreDistance(double value)
        {
            this.Value = value;
        }

        #endregion

        #region Static Operators

        public static implicit operator OgreDistance(double value)
        {
            return new OgreDistance(value);
        }

        public static implicit operator double(OgreDistance distance)
        {
            return distance == null ? 0 : distance.Value;
        }

        #endregion

        #region Public Methods

        protected override Distance AddDistance(Distance distance)
        {
            if (!(distance is OgreDistance))
                throw new ArgumentException("distance is not an OgreDistance.");

            return new OgreDistance(this.Value + ((OgreDistance) distance).Value);
        }

        protected override Distance SubtractDistance(Distance distance)
        {
            if (!(distance is OgreDistance))
                throw new ArgumentException("distance is not an OgreDistance.");

            return new OgreDistance(this.Value - ((OgreDistance)distance).Value);
        }

        protected override int CompareToDistance(Distance distance)
        {
            if (!(distance is OgreDistance))
                throw new ArgumentException("distance is not an OgreDistance.");

            if (distance.Equals(ZeroValue))
                distance = new OgreDistance(0);

            return this.Value.CompareTo(((OgreDistance)distance).Value);
        }

        public override Position PositionAlongDirection(Direction direction)
        {
            if (!(direction is OgreDirection))
                throw new ArgumentException("direction is not an OgreDirection.");

            return new OgrePosition(
                ((OgreDirection) direction).Vector3 * (float)this.value);
        }

        public override Distance Divide(int numFractions)
        {
            return new OgreDistance(this/numFractions);
        }

        public override Distance Multiply(int numTimes)
        {
            return new OgreDistance(this*numTimes);
        }
        #endregion

        #region Serialize Methods

        public override void ReadXml(XmlElement element)
        {
            this.value = Convert.ToDouble(element.GetAttribute("value"),
                CultureInfo.InvariantCulture);
        }

        public override void WriteXml(XmlElement element)
        {
            element.SetAttribute("value", Convert.ToString(this.value,
                 CultureInfo.InvariantCulture));
        }

        #endregion
    }
}
