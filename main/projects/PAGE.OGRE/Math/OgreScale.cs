/* ****************************************************
 * Name: OgreScale.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/31
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;
using Mogre;
using PAGE.Generic.Math;

namespace PAGE.OGRE.Math
{
    /// <summary>
    /// Represents a scale in 3D space coordinates.
    /// </summary>
    public class OgreScale : Scale
    {
        #region Fields

        protected Vector3 vector3 = Vector3.UNIT_SCALE;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the scale's x component.
        /// </summary>
        public float X
        {
            get { return this.vector3.x; }
            set { this.vector3.x = value; }
        }

        /// <summary>
        /// Gets or sets the scale's y component.
        /// </summary>
        public float Y
        {
            get { return this.vector3.y; }
            set { this.vector3.y = value; }
        }

        /// <summary>
        /// Gets or sets the scale's z component.
        /// </summary>
        public float Z
        {
            get { return this.vector3.z; }
            set { this.vector3.z = value; }
        }

        /// <summary>
        /// Gets the Vector3 representing this scale.
        /// </summary>
        [XmlIgnore]
        public Vector3 Vector3
        {
            get { return vector3; }
            set { vector3 = value; }
        }

        #endregion

        #region Constructors

        public OgreScale(){}

        public OgreScale(Vector3 scale)
        {
            this.vector3 = scale;
        }

        public OgreScale(float x, float y, float z)
        {
            this.vector3 = new Vector3(x, y, z);
        }

        #endregion

        #region Static Operators

        public static implicit operator OgreScale(Vector3 vec)
        {
            return new OgreScale(vec);
        }

        public static implicit operator Vector3(OgreScale scale)
        {
            return scale == null ? new Vector3() : scale.Vector3;
        }

        #endregion

        #region Serialize Methods

        public override void ReadXml(XmlElement element)
        {
            this.X = Convert.ToSingle(element.GetAttribute("x"),
                CultureInfo.InvariantCulture);
            this.Y = Convert.ToSingle(element.GetAttribute("y"),
                CultureInfo.InvariantCulture);
            this.Z = Convert.ToSingle(element.GetAttribute("z"),
                CultureInfo.InvariantCulture);
        }

        public override void WriteXml(XmlElement element)
        {
            element.SetAttribute("x", Convert.ToString(this.X, CultureInfo.InvariantCulture));
            element.SetAttribute("y", Convert.ToString(this.Y, CultureInfo.InvariantCulture));
            element.SetAttribute("z", Convert.ToString(this.Z, CultureInfo.InvariantCulture));
        }

        #endregion

        #region Public Methods

        public override bool Equals(object obj)
        {
            if (obj is OgreScale)
            {
                OgreScale scale = (OgreScale)obj;
                return
                    this.Vector3.x == scale.Vector3.x &&
                    this.Vector3.y == scale.Vector3.y &&
                    this.Vector3.z == scale.Vector3.z;
            }
            else
            {
                throw new ArgumentException("Object is not an OgreScale");
            }
        }

        #endregion
    }
}
