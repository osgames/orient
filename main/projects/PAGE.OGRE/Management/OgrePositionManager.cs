/* ****************************************************
 * Name: OgrePositionManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/09/20 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Management;
using PAGE.OGRE.Domain.Assets;
using PAGE.OGRE.Math;

namespace PAGE.OGRE.Management
{
    /// <summary>
    /// Represents a manager for a specific feature or resource of an asset.
    /// </summary>
    public class OgrePositionManager : PositionManager
    {
        #region Properties

        public new IOgreInteractionAsset Asset
        {
            get { return base.Asset as IOgreInteractionAsset; }
        }

        #endregion

        #region Constructors

        public OgrePositionManager(IInteractionAsset asset)
            : base(asset)
        {
        }

        #endregion

        #region Public Methods

        public override void Update(float millisSinceLastUpdate)
        {
            //no transition to be made
            if (this.totalTimeLeft < 0) return;

            //updates asset position according to time
            this.totalTimeLeft -= millisSinceLastUpdate;
            this.Asset.Node.Position += (OgrePosition)
                this.distancePerMilli.Multiply((int) millisSinceLastUpdate).
                    PositionAlongDirection(this.directionToTarget);
        }

        public override void Stop()
        {
            //no more updates
            this.totalTimeLeft = -1;
        }

        public override void Dispose()
        {
        }

        #endregion
    }
}