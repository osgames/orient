/* ****************************************************
 * Name: OgreTextManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/17 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Management;
using PAGE.OGRE.Domain.Assets;
using PAGE.OGRE.Domain.Cameras;
using PAGE.OGRE.GUI;

namespace PAGE.OGRE.Management
{
    public class OgreTextManager : TextManager
    {
        #region Constructor

        public OgreTextManager(IOgreInteractionAsset asset, OgreCamera camera)
            : base(asset)
        {
            this.mainText = new OgreFollowAssetText(asset, camera);
        }

        #endregion

        #region Public Methods

        public override void Stop()
        {
            base.Stop();
            this.MainText.Visible = false;
        }

        public override void Dispose()
        {
            //do nothing
        }

        #endregion
    }
}