/* ****************************************************
 * Name: OgreVisibilityManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/05/07 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using Mogre;
using PAGE.Generic.Management;
using PAGE.OGRE.Domain.Assets;

namespace PAGE.OGRE.Management
{
    /// <summary>
    /// Represents a manager that enables effects to control a character's
    /// visibility like fade in/out.
    /// </summary>
    public class OgreVisibilityManager : AssetManager
    {
        #region Fields

        protected bool fading = false;
        protected bool fadeIn = false; 
        protected float curAlpha = 0;
        protected float curFadeTime = 0;
        protected float fadingTime = 0;

        #endregion

        #region Properties

        protected new IOgreEntityAsset Asset
        {
            get { return base.Asset as IOgreEntityAsset; }
        }

        #endregion

        #region Constructors

        public OgreVisibilityManager(IOgreEntityAsset asset)
            : base(asset)
        {
            //enables alpha operation
            if (this.Asset.Entity != null)
                this.Asset.Entity.GetSubEntity(0).GetMaterial().
                    GetTechnique(0).GetPass(0).SetSceneBlending(
                    SceneBlendType.SBT_TRANSPARENT_ALPHA);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Fades the visibility of the character, going from invisible to visible state
        /// in the given duration time. In the end the character will be visible.
        /// </summary>
        /// <param name="transitionTime">the time in seconds to perform the transition
        /// between visibility states.</param>
        /// <param name="force">if set to true, other ongoing transitions will be 
        /// canceled. If set to false and a fading operation is on course it does
        /// nothing.</param>
        public virtual void FadeIn(float transitionTime, bool force)
        {
            if (!force && this.fading) return;

            this.Asset.Visible = true;
            this.SetAlpha(this.curAlpha = 0);
            this.curFadeTime = 0;
            this.fadingTime = transitionTime;
            this.fading = true;
            this.fadeIn = true;
        }

        /// <summary>
        /// Fades the visibility of the character, going from visble to invisible state
        /// in the given duration time. In the end the character will be invisible.
        /// </summary>
        /// <param name="transitionTime">the time in seconds to perform the transition
        /// between visibility states.</param>
        /// <param name="force">if set to true, other ongoing transitions will be 
        /// canceled. If set to false and a fading operation is on course it does
        /// nothing.</param>
        public virtual void FadeOut(float transitionTime, bool force)
        {
            if (!force && this.fading) return;

            this.Asset.Visible = true;
            this.SetAlpha(this.curAlpha = 1);
            this.curFadeTime = 0;
            this.fadingTime = transitionTime;
            this.fading = true;
            this.fadeIn = false;
        }

        public override void Update(float millisSinceLastUpdate)
        {
            if(!this.fading) return;

            //updates current alpha value based on elapsed time
            this.curFadeTime += millisSinceLastUpdate/1000f;
            this.curAlpha = this.curFadeTime/this.fadingTime;

            //checks fade time end
            if(this.curFadeTime >= this.fadingTime)
            {
                //sets visibility according to effect (in/out)
                this.curAlpha = 1;
                this.fading = false;
                this.Asset.Visible = this.fadeIn;
            }

            //sets alpha value according to effect (in/out)
            if (!this.fadeIn) this.curAlpha = 1 - this.curAlpha;
            this.SetAlpha(this.curAlpha);
        }

        public void Stop()
        {
            if(this.fading)
            {
                if (this.Asset.Visible) this.SetAlpha(this.curAlpha = 1);
                else this.SetAlpha(this.curAlpha = 0);
            }
        }

        public override void Dispose()
        {
        } 

        #endregion

        #region Protected Methods

        /// <summary>
        /// Changes the alpha value of the character's material.
        /// </summary>
        /// <param name="alphaValue">the alpha value to set the material to. Must be 
        /// between 0 and 1.</param>
        protected void SetAlpha(float alphaValue)
        {
            //changes texture 
            for (uint i = 0; i < this.Asset.Entity.NumSubEntities; i++)
            {
                Pass pass = this.Asset.Entity.GetSubEntity(i).GetMaterial().GetTechnique(0).GetPass(0);
                if (pass.NumTextureUnitStates == 0) return;
                pass.GetTextureUnitState(0).SetAlphaOperation(
                    LayerBlendOperationEx.LBX_MODULATE, LayerBlendSource.LBS_TEXTURE,
                    LayerBlendSource.LBS_MANUAL, 1.0f, alphaValue);
            }
        }

        #endregion
    }
}