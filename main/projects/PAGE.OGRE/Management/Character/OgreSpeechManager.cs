/* ****************************************************
 * Name: OgreSpeechManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/24 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Management.Character;
using PAGE.OGRE.Domain.Assets;

namespace PAGE.OGRE.Management.Character
{
    public class OgreSpeechManager : SpeechManager
    {
        #region Constructors

        public OgreSpeechManager(OgreCharacter character)
            : base(character)
        {
        } 

        #endregion

        #region Public Methods

        public override void StartSpeech(string utterance)
        {
            //does nothing here...
        }

        public override void Dispose()
        {
            //does nothing here...
        }

        #endregion
    }
}