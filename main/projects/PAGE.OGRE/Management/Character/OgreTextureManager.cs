/* ****************************************************
 * Name: OgreTextureManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/12 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using PAGE.Generic.Management.Character;
using PAGE.OGRE.Domain.Assets;

namespace PAGE.OGRE.Management.Character
{
    public class OgreTextureManager : TextureManager
    {
        #region Constructors

        public OgreTextureManager(OgreCharacter character)
            : base(character)
        {
        } 

        #endregion

        #region Properties

        public new OgreCharacter Character
        {
            get { return base.Character as OgreCharacter; }
        }

        #endregion

        #region Public Methods

        public override void Update(float millisSinceLastUpdate)
        {
            //does nothing...
        }

        public override void Stop()
        {
            //does nothing...
        }

        public override void Dispose()
        {
            //does nothing...
        }

        /// <summary>
        /// Changes the given sub-entity texture with the given texture name.
        /// </summary>
        /// <param name="subEntityNum">the number of the sub-entity if the character
        /// to change the texture to.</param>
        /// <param name="textureName">the name of the new texture to put on the given
        /// sub-entity.</param>
        public void ChangeEntityTexture(uint subEntityNum, string textureName)
        {
            try
            {
                this.Character.Entity.GetSubEntity(subEntityNum).GetMaterial().
                    GetTechnique(0).GetPass(0).RemoveAllTextureUnitStates();
                this.Character.Entity.GetSubEntity(subEntityNum).GetMaterial().
                    GetTechnique(0).GetPass(0).CreateTextureUnitState(textureName);
            }
            catch (Exception)
            {
                //TODO catch ogre exception here...
            }
        } 

        #endregion
    }
}