/* ****************************************************
 * Name: OgreWalkingNavigationManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/11 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using Mogre;
using PAGE.Generic.Management.Character;
using PAGE.Generic.Math;
using PAGE.OGRE.Domain.Assets;
using PAGE.OGRE.Math;

namespace PAGE.OGRE.Management.Character
{
    public class OgreWalkingNavigationManager : WalkingNavigationManager
    {
        #region Fields

        // the direction at which the object is moving
        protected OgreDirection walkDirection = Vector3.UNIT_X;

        #endregion

        #region Properties

        public new OgreCharacter Character
        {
            get { return base.Character as OgreCharacter; }
        }

        #endregion

        #region Constructors

        public OgreWalkingNavigationManager(OgreCharacter character)
            : base(character)
        {
        } 

        #endregion

        #region Override Methods

        protected override bool IsInPosition(Position targetPosition)
        {
            OgrePosition position = targetPosition as OgrePosition;
            position.Y = ((OgrePosition)this.Character.AbsolutePosition).Y;
            return base.IsInPosition(position);
        }

        protected override void UpdateCharacterMovement(float millisSinceLastUpdate)
        {
            //check to see if we've arrived at a waypoint
            if ((OgreDistance)this.distance.CompareTo(new OgreDistance(0)) == 0)
            {
                //set the node to the destination we've just reached
                this.Character.Node.Position = (OgrePosition)this.currentDestination;
                this.walkDirection = Vector3.UNIT_X;
            }
            else
            {
                //calculate movement distance
                this.moveDistance = new OgreDistance(
                    millisSinceLastUpdate * 0.001f *
                    (OgreDistance)this.walkStepDist * this.MoveSpeed);

                //moves character a little bit
                this.Character.Node.Translate((OgrePosition)
                    this.moveDistance.PositionAlongDirection(
                    new OgreDirection(this.walkDirection)));

                //subtracts "walked" distance
                this.distance = this.distance.Subtract(this.moveDistance);
            }

        }

        protected override void  UpdateCharacterDirection()
        {
            //character doesn't climb
            ((OgrePosition)this.currentDestination).Y =
                ((OgrePosition)this.Character.AbsolutePosition).Y;

            //update the direction and the distance
            this.walkDirection = (OgreDirection)
                this.Character.AbsolutePosition.DirectionTo(this.currentDestination);
            this.distance = 
                this.Character.AbsolutePosition.DistanceTo(this.currentDestination);

            //update rotation
            Vector3 src = (OgreDirection)this.Character.AbsoluteDirection;
            if ((1.0f + src.DotProduct(this.walkDirection)) < 0.0001f)
            {
                this.Character.Node.Yaw(180.0f);
            }
            else
            {
                Quaternion quat = src.GetRotationTo(this.walkDirection);
                this.Character.Node.Rotate(quat);
            }
        }

        #endregion
    }
}