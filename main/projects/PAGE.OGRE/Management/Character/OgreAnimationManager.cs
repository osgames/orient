/* ****************************************************
 * Name: OgreAnimationManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using Mogre;
using PAGE.Generic.Management.Character;
using PAGE.OGRE.Domain.Assets;

namespace PAGE.OGRE.Management.Character
{
    public class OgreAnimationManager : AnimationManager
    {
        #region Fields

        protected const float ANIMATION_DELTA = 0.001f;

        protected AnimationState animSource, animTarget;
        protected float timeleft, duration, animationSpeed = 1.0f;
        protected BlendingTransition transition = BlendingTransition.BlendThenAnimate;
        protected Random random = new Random(DateTime.Now.Millisecond);
        protected bool loop, isAnimating;

        #endregion

        #region Constructor

        public OgreAnimationManager(OgreCharacter character)
            : base(character)
        {
        }

        #endregion

        #region Properties

        protected new OgreCharacter Character
        {
            get { return base.Character as OgreCharacter; }
        }

        #endregion

        #region Public Methods

        public override void Stop()
        {
            //this.Init();
            this.animationDuration = this.animationTimeLeft = 0;
        }

        public override void BlendAnimation(
            string animationName, float animSpeed, BlendingTransition animTransition, 
            float animDuration, bool randomStartTime, bool animLoop)
        {
            if (this.Character.Entity == null) return;

            //no animation, failure
            if (string.IsNullOrEmpty(animationName) ||
                !this.Character.Entity.AllAnimationStates.HasAnimationState(animationName))
            {
                this.ActionFailure();
                return;
            }

            //control variables
            this.isAnimating = true;
            this.animationSpeed = animSpeed;
            this.loop = animLoop;

            //animation duration
            this.animationDuration = this.animationSpeed*
                                     this.Character.Entity.GetAnimationState(animationName).Length;

            //check random animation start time
            float startTime = 0;
            if (randomStartTime)
                startTime = this.random.Next(0, (int) (this.animationDuration*0.5f));

            //no old animation, no transition
            if (this.animSource == null)
            {
                this.animSource = this.Character.Entity.GetAnimationState(animationName);
                this.animSource.Enabled = true;
                this.animSource.Weight = 1;
                this.animSource.TimePosition = startTime;
                this.animationTimeLeft = this.animationDuration - startTime;
                this.timeleft = 0;
                this.duration = 1;
                this.animTarget = null;
                return;
            }

            //source and target animations are the same
            if (this.animSource.AnimationName == animationName)
            {
                this.animSource.Enabled = true;
                this.animSource.Weight = 1;
                this.timeleft = 0;

                //repositions animation
                if (!this.loop)
                    this.animSource.TimePosition = startTime;
                return;
            }

            this.animationTimeLeft = this.animationDuration - startTime;
            if (animTransition == BlendingTransition.BlendSwitch)
            {
                if (this.animSource != null) this.animSource.Enabled = false;
                this.animSource = this.Character.Entity.GetAnimationState(animationName);
                this.animSource.Enabled = true;
                this.animSource.Weight = 1;
                this.animSource.TimePosition = startTime;
                this.timeleft = 0;
            }
            else
            {
                AnimationState newTarget = this.Character.Entity.GetAnimationState(animationName);

                if (this.timeleft > 0)
                {
                    // oops, weren't finished yet 
                    if (newTarget == this.animTarget)
                    {
                        // nothing to do! (ignoring duration here) 
                    }
                    else if (newTarget == this.animSource)
                    {
                        // going back to the source state, so let's switch 
                        this.animSource = this.animTarget;
                        this.animTarget = newTarget;
                        this.timeleft = this.duration - this.timeleft; // i'm ignoring the new duration here 
                    }
                    else
                    {
                        // ok, newTarget is really new, so either we simply replace the target with this one, or 
                        // we make the target the new source 
                        if (this.timeleft < this.duration*0.5)
                        {
                            // simply replace the target with this one 
                            this.animTarget.Enabled = false;
                            this.animTarget.Weight = 0;
                        }
                        else
                        {
                            // old target becomes new source 
                            this.animSource.Enabled = false;
                            this.animSource.Weight = 0;
                            this.animSource = this.animTarget;
                        }
                        this.animTarget = newTarget;
                        this.animTarget.Enabled = true;
                        this.animTarget.Weight = (float) (1.0 - (this.timeleft/this.duration));
                        this.animTarget.TimePosition = startTime;
                    }
                }
                else
                {
                    // assert( target == 0, "target should be 0 when not blending" ) 
                    // this.mSource.SetEnabled(true); 
                    // this.mSource.SetWeight(1); 
                    this.transition = animTransition;
                    this.timeleft = this.duration = animDuration;
                    this.animTarget = newTarget;
                    this.animTarget.Enabled = true;
                    this.animTarget.Weight = 0;
                    this.animTarget.TimePosition = startTime;
                }
            }
        }

        public override void Update(float millisSinceLastUpdate)
        {
            base.Update(millisSinceLastUpdate);

            //no animation or not animating, return
            if ((this.animSource == null) || !this.isAnimating) return;

            float time = millisSinceLastUpdate*0.001f*this.animationSpeed;

            if (this.timeleft > 0)
            {
                this.timeleft -= time;
                if (this.timeleft <= 0)
                {
                    // finish blending 
                    this.animSource.Enabled = false;
                    this.animSource.Weight = 0;
                    this.animSource = this.animTarget;
                    this.animSource.Enabled = true;
                    this.animSource.Weight = 1;
                    this.animTarget = null;
                }
                else
                {
                    // still blending, advance weights 
                    this.animSource.Weight = (this.timeleft/this.duration);
                    this.animTarget.Weight = (float) (1.0 - (this.timeleft/this.duration));

                    if (this.transition == BlendingTransition.BlendWhileAnimating)
                    {
                        this.animTarget.AddTime(time);
                    }
                }
            }

            //if animation time has passed and we're not in transition, looped
            if ((this.animSource.TimePosition + time) >= this.animSource.Length)
            {
                //loop/end ocurred, sends success at each loop
                if (this.timeleft <= 0)
                {
                    this.ActionSuccess();

                    //if not looping, stop animation
                    if (!this.loop)
                    {
                        this.isAnimating = false;
                        this.animSource.TimePosition = this.animSource.Length - ANIMATION_DELTA;
                        return;
                    }
                }
            }

            //updates animetion state
            this.animationTimeLeft -= time;
            this.animSource.AddTime(time);
        }

        public override float GetAnimationDuration(string animationName)
        {
            return this.animationSpeed*this.Character.Entity.GetAnimationState(animationName).Length;
        }

        public override void Dispose()
        {
            this.animSource = null;
            this.animTarget = null;
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Cleans all animations states for the character.
        /// </summary>
        protected void Init()
        {
            if (this.Character.Entity == null) return;

            AnimationStateSet animSet = this.Character.Entity.AllAnimationStates;
            AnimationStateIterator it = animSet.GetAnimationStateIterator();
            while (it.MoveNext())
            {
                AnimationState anim = it.Current;
                anim.Enabled = false;
                anim.Weight = 0;
                anim.TimePosition = 0;
            }
        }

        #endregion
    }
}