namespace OSMPreviewer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openOSMFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.rotateTrackBar = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.moveTrackBar = new System.Windows.Forms.TrackBar();
            this.screenshotBtn = new System.Windows.Forms.Button();
            this.previewPanel1 = new PAGE.Orient.GUI.Windows.Forms.Controls.PreviewPanel(this.components);
            this.showStatsBtn = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rotateTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moveTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(792, 24);
            this.menuStrip1.TabIndex = 15;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.openToolStripMenuItem.Text = "&Open...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(127, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // openOSMFileDialog
            // 
            this.openOSMFileDialog.DefaultExt = "osm";
            this.openOSMFileDialog.Filter = "OSM files (*.osm)|*.osm";
            this.openOSMFileDialog.RestoreDirectory = true;
            this.openOSMFileDialog.Title = "Open OSM File";
            // 
            // rotateTrackBar
            // 
            this.rotateTrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rotateTrackBar.LargeChange = 10;
            this.rotateTrackBar.Location = new System.Drawing.Point(91, 538);
            this.rotateTrackBar.Maximum = 100;
            this.rotateTrackBar.MaximumSize = new System.Drawing.Size(1000, 30);
            this.rotateTrackBar.Name = "rotateTrackBar";
            this.rotateTrackBar.Size = new System.Drawing.Size(200, 30);
            this.rotateTrackBar.SmallChange = 5;
            this.rotateTrackBar.TabIndex = 17;
            this.rotateTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.rotateTrackBar.Value = 50;
            this.rotateTrackBar.Scroll += new System.EventHandler(this.rotateTrackBar_Scroll);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 542);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Rotate Speed";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(290, 542);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Move Speed";
            // 
            // moveTrackBar
            // 
            this.moveTrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.moveTrackBar.LargeChange = 10;
            this.moveTrackBar.Location = new System.Drawing.Point(364, 538);
            this.moveTrackBar.Maximum = 100;
            this.moveTrackBar.MaximumSize = new System.Drawing.Size(1000, 30);
            this.moveTrackBar.Name = "moveTrackBar";
            this.moveTrackBar.Size = new System.Drawing.Size(200, 30);
            this.moveTrackBar.SmallChange = 5;
            this.moveTrackBar.TabIndex = 19;
            this.moveTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.moveTrackBar.Value = 50;
            this.moveTrackBar.Scroll += new System.EventHandler(this.moveTrackBar_Scroll);
            // 
            // screenshotBtn
            // 
            this.screenshotBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.screenshotBtn.Location = new System.Drawing.Point(695, 542);
            this.screenshotBtn.Name = "screenshotBtn";
            this.screenshotBtn.Size = new System.Drawing.Size(85, 23);
            this.screenshotBtn.TabIndex = 36;
            this.screenshotBtn.Text = "Screenshot";
            this.screenshotBtn.UseVisualStyleBackColor = true;
            this.screenshotBtn.Click += new System.EventHandler(this.screenshotBtn_Click);
            // 
            // previewPanel1
            // 
            this.previewPanel1.AllowNavigation = false;
            this.previewPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.previewPanel1.CaptureKeyboard = true;
            this.previewPanel1.CaptureMouse = true;
            this.previewPanel1.Cursor = System.Windows.Forms.Cursors.Cross;
            this.previewPanel1.Location = new System.Drawing.Point(0, 24);
            this.previewPanel1.MoveSpeed = 50F;
            this.previewPanel1.Name = "previewPanel1";
            this.previewPanel1.RotateSpeed = 50F;
            this.previewPanel1.Size = new System.Drawing.Size(792, 508);
            this.previewPanel1.TabIndex = 16;
            this.previewPanel1.TabStop = true;
            this.previewPanel1.UpdateInterval = ((long)(10));
            // 
            // showStatsBtn
            // 
            this.showStatsBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.showStatsBtn.Location = new System.Drawing.Point(596, 542);
            this.showStatsBtn.Name = "showStatsBtn";
            this.showStatsBtn.Size = new System.Drawing.Size(85, 23);
            this.showStatsBtn.TabIndex = 37;
            this.showStatsBtn.Text = "Show Stats";
            this.showStatsBtn.UseVisualStyleBackColor = true;
            this.showStatsBtn.Click += new System.EventHandler(this.showStatsBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 573);
            this.Controls.Add(this.showStatsBtn);
            this.Controls.Add(this.screenshotBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.moveTrackBar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rotateTrackBar);
            this.Controls.Add(this.previewPanel1);
            this.Controls.Add(this.menuStrip1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "OSM Previewer";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rotateTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moveTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openOSMFileDialog;
        private PAGE.Orient.GUI.Windows.Forms.Controls.PreviewPanel previewPanel1;
        private System.Windows.Forms.TrackBar rotateTrackBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar moveTrackBar;
        private System.Windows.Forms.Button screenshotBtn;
        private System.Windows.Forms.Button showStatsBtn;
    }
}

