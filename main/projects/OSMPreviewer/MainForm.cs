/* ****************************************************
 * Name: MainForm.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/16 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.IO;
using System.Windows.Forms;
using PAGE.OGRE.Math;
using PAGE.Orient;
using PAGE.Orient.Domain.Assets;
using PAGE.Orient.Domain.Cameras;
using PAGE.Orient.GUI.Windows.Forms.Forms;
using PAGE.Orient.Resources;
using PAGE.Orient.Resources.Assets;

namespace OSMPreviewer
{
    public partial class MainForm : Form
    {
        private OrientSet set = null;

        public MainForm()
        {
            InitializeComponent();

            this.FormClosing += Form1_FormClosing;

            OrientGraphicsRealizer.Instance.Setup("./data/resources.xml",this.previewPanel1);
            OrientGraphicsRealizer.Instance.Camera.CameraMode = CameraMode.Static;

            this.previewPanel1.AllowNavigation = true;

            this.rotateTrackBar.KeyDown += rotateTrackBar_KeyDown;
            this.moveTrackBar.KeyDown += moveTrackBar_KeyDown;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            OrientGraphicsRealizer.Instance.Destroy();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox ab = new AboutBox();
            ab.FormClosed += ab_FormClosed;
            ab.Show();
            ab.Refresh();
            this.Enabled = false;
        }

        private void ab_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Enabled = true;
            this.Focus();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fileName;

            if(this.openOSMFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileName = this.openOSMFileDialog.FileName;
            }
            else
            {
                return;
            }

            if (this.set != null)
            {
                OrientGraphicsRealizer.Instance.RemoveSet(this.set);
            }

            OrientResourceSet newSet = new OrientResourceSet("set " + DateTime.Now.Ticks, new OrientResourcesFactory());
            newSet.FileName = Path.GetFileName(fileName);
            newSet.SkyBox = "Examples/CloudyNoonSkyBox";
            OrientGraphicsRealizer.Instance.ResourcesManager.Sets.Add(newSet);

            this.set = (OrientSet)OrientGraphicsRealizer.Instance.LoadSet(newSet.IdToken);
            this.set.Visible = true;

            OrientGraphicsRealizer.Instance.Camera.AbsolutePosition = new OgrePosition(0, 10, 0);
        }

        private void rotateTrackBar_Scroll(object sender, EventArgs e)
        {
            this.previewPanel1.RotateSpeed = this.rotateTrackBar.Value;
        }

        private void moveTrackBar_Scroll(object sender, EventArgs e)
        {
            this.previewPanel1.MoveSpeed = this.moveTrackBar.Value;
        }

        //disable trackbar keyboard events
        private void rotateTrackBar_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        private void moveTrackBar_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        private void screenshotBtn_Click(object sender, EventArgs e)
        {
            int num = 0;
            string fileName = "osmPreviewSS0.png";
            while(File.Exists(fileName))
            {
                fileName = "osmPreviewSS" + ++num + ".png";
            }
            OrientGraphicsRealizer.Instance.SaveScreenshot(640, 480, fileName);
        }

        private void showStatsBtn_Click(object sender, EventArgs e)
        {
            if (this.showStatsBtn.Text == "Show Stats")
            {
                OrientGraphicsRealizer.Instance.GraphicStatsVisible = true;
                this.showStatsBtn.Text = "Hide Stats";
            }
            else
            {
                OrientGraphicsRealizer.Instance.GraphicStatsVisible = false;
                this.showStatsBtn.Text = "Show Stats";
            }
        }
    }
}