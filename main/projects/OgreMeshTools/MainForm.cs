using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace OgreMeshTools
{
    public partial class MainForm : Form
    {
        protected OutputForm outputForm = new OutputForm();

        public MainForm()
        {
            InitializeComponent();
            this.outputForm.Show();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            this.outputForm.Close();
        }

        #region Functions Buttons

        private void convertBtn_Click(object sender, EventArgs e)
        {
            string args = "";
            if (this.lodLevelsNUD.Value != 0)
                args += " -l " + this.lodLevelsNUD.Value;
            if (this.lodDistNUD.Value != 0)
                args += " -d " + this.lodDistNUD.Value;
            if (this.lodPercentNUD.Value != 0)
                args += " -p " + this.lodPercentNUD.Value;
            if (this.notGenEdgeLCB.Checked) args += " -e";
            if (this.notReVertBufCB.Checked) args += " -r";
            if (this.genTangentCB.Checked) args += " -t";
            if (this.notOptRedTracksCB.Checked) args += " -o";
            if (this.quietModeCB.Checked) args += " -q";

            foreach (string fileName in this.openFileDialog1.FileNames)
                SendCommand("OgreXmlConverter.exe", args + " \"" + fileName + "\"");
        }

        private void transformBtn_Click(object sender, EventArgs e)
        {
            string args = "transform";
            if (this.scaleCheckBox.Checked)
                args += " -scale=" +
                    this.sXTxtBox.Value.ToString(CultureInfo.InvariantCulture) + "/" +
                    this.sYTxtBox.Value.ToString(CultureInfo.InvariantCulture) + "/" +
                    this.sZTxtBox.Value.ToString(CultureInfo.InvariantCulture);
            if (this.translateCheckBox.Checked)
                args += " -translate=" +
                    this.tXTxtBox.Value.ToString(CultureInfo.InvariantCulture) + "/" +
                    this.tYTxtBox.Value.ToString(CultureInfo.InvariantCulture) + "/" +
                    this.tZTxtBox.Value.ToString(CultureInfo.InvariantCulture);
            if (this.rotateCheckBox.Checked)
                args += " -rotate=" + this.angleTxtBox.Value.ToString(CultureInfo.InvariantCulture) + "/" +
                    this.rXTxtBox.Value.ToString(CultureInfo.InvariantCulture) + "/" +
                    this.rYTxtBox.Value.ToString(CultureInfo.InvariantCulture) + "/" +
                    this.rZTxtBox.Value.ToString(CultureInfo.InvariantCulture);
            if (this.alignCheckBox.Checked)
            {
                if (this.xAlignRadioBtn.Checked)
                {
                    args += " -xalign=";
                    if (this.leftARadioBtn.Checked) args += "left";
                    else if (this.centerARadioBtn.Checked) args += "center";
                    else args += "right";
                }
                else if (this.yAlignRadioBtn.Checked)
                {
                    args += " -yalign=";
                    if (this.leftARadioBtn.Checked) args += "top";
                    else if (this.centerARadioBtn.Checked) args += "center";
                    else args += "bottom";
                }
                else
                {
                    args += " -zalign=";
                    if (this.leftARadioBtn.Checked) args += "front";
                    else if (this.centerARadioBtn.Checked) args += "center";
                    else args += "back";
                }
            }
            if (this.noNormNormCheckBox.Checked) args += " -no-normalise-normals";
            if (this.noUpdateBBCheckBox.Checked) args += " -no-update-boundingbox";

            foreach (string fileName in this.openFileDialog1.FileNames)
                SendCommand("meshmagick.exe", args + " \"" + fileName + "\"");
        }

        private void renameBtn_Click(object sender, EventArgs e)
        {
            string args = "rename";
            if((this.animBeforeTxtBox.Text != "") && (this.animAfterTxtBox.Text != ""))
                args += " -animation=" + 
                    this.animBeforeTxtBox.Text + "/" + this.animAfterTxtBox.Text;
            if ((this.boneBeforeTxtBox.Text != "") && (this.boneAfterTxtBox.Text != ""))
                args += " -bone=" +
                    this.boneBeforeTxtBox.Text + "/" + this.boneAfterTxtBox.Text;
            if ((this.matBeforeTxtBox.Text != "") && (this.matAfterTxtBox.Text != ""))
                args += " -material=" +
                    this.matBeforeTxtBox.Text + "/" + this.matAfterTxtBox.Text;
            if (this.skelNameTxtBox.Text != "")
                args += " -skeleton=" + this.skelNameTxtBox.Text;

            foreach (string fileName in this.openFileDialog1.FileNames)
                SendCommand("meshmagick.exe", args + " \"" + fileName + "\"");
        }

        private void optimiseBtn_Click(object sender, EventArgs e)
        {
            string args = "optimise \"FILE_NAME\"";
            if (this.vertexTolNUP.Value != 0)
                args += " -tolerance=" + this.vertexTolNUP.Value.ToString(CultureInfo.InvariantCulture);
            if (this.posTolNUP.Value != 0)
                args += " -pos_tolerance=" + this.posTolNUP.Value.ToString(CultureInfo.InvariantCulture);
            if (this.normTolNUP.Value != 0)
                args += " -norm_tolerance=" + this.normTolNUP.Value.ToString(CultureInfo.InvariantCulture);
            if (this.uvTolNUP.Value != 0)
                args += " -uv_tolerance=" + this.uvTolNUP.Value.ToString(CultureInfo.InvariantCulture);
            if (this.keepIdTracksCheckBox.Checked)
                args += " -keep-identity-tracks";

            foreach (string fileName in this.openFileDialog1.FileNames)
                SendCommand("meshmagick.exe", args.Replace("FILE_NAME", fileName));
        }

        private void infoBtn_Click(object sender, EventArgs e)
        {
            foreach(string fileName in this.openFileDialog1.FileNames)
                SendCommand("meshmagick.exe", "info \"" + fileName + "\"");
        }

        private void mergeBtn_Click(object sender, EventArgs e)
        {
            string args = "meshmerge ";
            foreach (string fileName in this.openFileDialog1.FileNames)
                args += "\"" + fileName + "\" ";
            args += "-- " + this.mergedFiletxtBox.Text;
            SendCommand("meshmagick.exe", args);
        }

        #endregion

        #region Protected Methods

        protected void SendCommand(string fileName, string args)
        {
            this.outputForm.OutputText
                += ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\r\n" +
                   "Sending command:\r\n" +
                   fileName + " " + args + "\r\n";

            Process proc = new Process();
            proc.EnableRaisingEvents = false;
            proc.StartInfo.FileName = fileName;
            proc.StartInfo.Arguments = args;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            
            string result = proc.StandardOutput.ReadToEnd();
            this.outputForm.OutputText
                += "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\r\n" +
                   result;
        }

        #endregion

        private void chooseFileBtn_Click(object sender, EventArgs e)
        {
            if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.fileNamesTxtBox.Text = "";
                foreach(string fileName in this.openFileDialog1.FileNames)
                {
                    this.tabControl1.Enabled = true;
                    this.fileNamesTxtBox.Text += Path.GetFileName(fileName) + "; ";
                }
            }
            else
            {
                this.tabControl1.Enabled = false;
            }
        }

        private void xAlignRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            this.leftARadioBtn.Text = "left";
            this.centerARadioBtn.Text = "center";
            this.rightARadioBtn.Text = "right";
        }

        private void yAlignRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            this.leftARadioBtn.Text = "top";
            this.centerARadioBtn.Text = "center";
            this.rightARadioBtn.Text = "bottom";
        }

        private void zAlignRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            this.leftARadioBtn.Text = "front";
            this.centerARadioBtn.Text = "center";
            this.rightARadioBtn.Text = "back";
        }
    }
}