using System.Windows.Forms;

namespace OgreMeshTools
{
    public partial class OutputForm : Form
    {
        public string OutputText
        {
            get { return this.textBox1.Text; }
            set
            {
                this.textBox1.Text = value;
                this.textBox1.Select(this.textBox1.Text.Length - 1, 0);
                this.textBox1.ScrollToCaret();
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            e.Cancel = true;
        }

        public OutputForm()
        {
            InitializeComponent();
        }
    }
}