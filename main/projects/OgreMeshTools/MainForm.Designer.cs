namespace OgreMeshTools
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.mergedFiletxtBox = new System.Windows.Forms.TextBox();
            this.mergeBtn = new System.Windows.Forms.Button();
            this.infoBtn = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.optimiseBtn = new System.Windows.Forms.Button();
            this.keepIdTracksCheckBox = new System.Windows.Forms.CheckBox();
            this.uvTolNUP = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.normTolNUP = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.posTolNUP = new System.Windows.Forms.NumericUpDown();
            this.vertexTolNUP = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.renameBtn = new System.Windows.Forms.Button();
            this.skelNameTxtBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.matAfterTxtBox = new System.Windows.Forms.TextBox();
            this.matBeforeTxtBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.boneAfterTxtBox = new System.Windows.Forms.TextBox();
            this.boneBeforeTxtBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.animAfterTxtBox = new System.Windows.Forms.TextBox();
            this.animBeforeTxtBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.alignCheckBox = new System.Windows.Forms.CheckBox();
            this.zAlignRadioBtn = new System.Windows.Forms.RadioButton();
            this.yAlignRadioBtn = new System.Windows.Forms.RadioButton();
            this.xAlignRadioBtn = new System.Windows.Forms.RadioButton();
            this.rotateCheckBox = new System.Windows.Forms.CheckBox();
            this.translateCheckBox = new System.Windows.Forms.CheckBox();
            this.scaleCheckBox = new System.Windows.Forms.CheckBox();
            this.transformBtn = new System.Windows.Forms.Button();
            this.noUpdateBBCheckBox = new System.Windows.Forms.CheckBox();
            this.noNormNormCheckBox = new System.Windows.Forms.CheckBox();
            this.rightARadioBtn = new System.Windows.Forms.RadioButton();
            this.centerARadioBtn = new System.Windows.Forms.RadioButton();
            this.leftARadioBtn = new System.Windows.Forms.RadioButton();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.angleTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.rZTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.rYTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.rXTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tZTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.tYTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.tXTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.sZTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.sYTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.sXTxtBox = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.convertBtn = new System.Windows.Forms.Button();
            this.quietModeCB = new System.Windows.Forms.CheckBox();
            this.notOptRedTracksCB = new System.Windows.Forms.CheckBox();
            this.genTangentCB = new System.Windows.Forms.CheckBox();
            this.notReVertBufCB = new System.Windows.Forms.CheckBox();
            this.notGenEdgeLCB = new System.Windows.Forms.CheckBox();
            this.lodPercentNUD = new System.Windows.Forms.NumericUpDown();
            this.label28 = new System.Windows.Forms.Label();
            this.lodDistNUD = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.lodLevelsNUD = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.fileNamesTxtBox = new System.Windows.Forms.TextBox();
            this.chooseFileBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uvTolNUP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.normTolNUP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.posTolNUP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vertexTolNUP)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.angleTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rZTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rYTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rXTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tZTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tYTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tXTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sZTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sYTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sXTxtBox)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lodPercentNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lodDistNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lodLevelsNUD)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Enabled = false;
            this.tabControl1.Location = new System.Drawing.Point(12, 87);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(380, 261);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.mergedFiletxtBox);
            this.tabPage4.Controls.Add(this.mergeBtn);
            this.tabPage4.Controls.Add(this.infoBtn);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(372, 235);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Basic";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // mergedFiletxtBox
            // 
            this.mergedFiletxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mergedFiletxtBox.Location = new System.Drawing.Point(92, 45);
            this.mergedFiletxtBox.Name = "mergedFiletxtBox";
            this.mergedFiletxtBox.Size = new System.Drawing.Size(264, 20);
            this.mergedFiletxtBox.TabIndex = 2;
            this.mergedFiletxtBox.Text = "merged.mesh";
            // 
            // mergeBtn
            // 
            this.mergeBtn.Location = new System.Drawing.Point(11, 42);
            this.mergeBtn.Name = "mergeBtn";
            this.mergeBtn.Size = new System.Drawing.Size(75, 23);
            this.mergeBtn.TabIndex = 1;
            this.mergeBtn.Text = "&Merge";
            this.mergeBtn.UseVisualStyleBackColor = true;
            this.mergeBtn.Click += new System.EventHandler(this.mergeBtn_Click);
            // 
            // infoBtn
            // 
            this.infoBtn.Location = new System.Drawing.Point(11, 13);
            this.infoBtn.Name = "infoBtn";
            this.infoBtn.Size = new System.Drawing.Size(75, 23);
            this.infoBtn.TabIndex = 0;
            this.infoBtn.Text = "&Info";
            this.infoBtn.UseVisualStyleBackColor = true;
            this.infoBtn.Click += new System.EventHandler(this.infoBtn_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.optimiseBtn);
            this.tabPage1.Controls.Add(this.keepIdTracksCheckBox);
            this.tabPage1.Controls.Add(this.uvTolNUP);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.normTolNUP);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.posTolNUP);
            this.tabPage1.Controls.Add(this.vertexTolNUP);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(364, 226);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Optimise";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // optimiseBtn
            // 
            this.optimiseBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.optimiseBtn.Location = new System.Drawing.Point(283, 197);
            this.optimiseBtn.Name = "optimiseBtn";
            this.optimiseBtn.Size = new System.Drawing.Size(75, 23);
            this.optimiseBtn.TabIndex = 10;
            this.optimiseBtn.Text = "&Optimise";
            this.optimiseBtn.UseVisualStyleBackColor = true;
            this.optimiseBtn.Click += new System.EventHandler(this.optimiseBtn_Click);
            // 
            // keepIdTracksCheckBox
            // 
            this.keepIdTracksCheckBox.AutoSize = true;
            this.keepIdTracksCheckBox.Location = new System.Drawing.Point(9, 129);
            this.keepIdTracksCheckBox.Name = "keepIdTracksCheckBox";
            this.keepIdTracksCheckBox.Size = new System.Drawing.Size(118, 17);
            this.keepIdTracksCheckBox.TabIndex = 9;
            this.keepIdTracksCheckBox.Text = "keep identity tracks";
            this.keepIdTracksCheckBox.UseVisualStyleBackColor = true;
            // 
            // uvTolNUP
            // 
            this.uvTolNUP.Location = new System.Drawing.Point(106, 94);
            this.uvTolNUP.Name = "uvTolNUP";
            this.uvTolNUP.Size = new System.Drawing.Size(63, 20);
            this.uvTolNUP.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "UV tolerance:";
            // 
            // normTolNUP
            // 
            this.normTolNUP.Location = new System.Drawing.Point(106, 68);
            this.normTolNUP.Name = "normTolNUP";
            this.normTolNUP.Size = new System.Drawing.Size(63, 20);
            this.normTolNUP.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Normals tolerance:";
            // 
            // posTolNUP
            // 
            this.posTolNUP.Location = new System.Drawing.Point(106, 42);
            this.posTolNUP.Name = "posTolNUP";
            this.posTolNUP.Size = new System.Drawing.Size(63, 20);
            this.posTolNUP.TabIndex = 4;
            // 
            // vertexTolNUP
            // 
            this.vertexTolNUP.Location = new System.Drawing.Point(106, 16);
            this.vertexTolNUP.Name = "vertexTolNUP";
            this.vertexTolNUP.Size = new System.Drawing.Size(63, 20);
            this.vertexTolNUP.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Position tolerance:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Vertex tolerance:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.renameBtn);
            this.tabPage2.Controls.Add(this.skelNameTxtBox);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.matAfterTxtBox);
            this.tabPage2.Controls.Add(this.matBeforeTxtBox);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.boneAfterTxtBox);
            this.tabPage2.Controls.Add(this.boneBeforeTxtBox);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.animAfterTxtBox);
            this.tabPage2.Controls.Add(this.animBeforeTxtBox);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(364, 226);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Rename";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // renameBtn
            // 
            this.renameBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.renameBtn.Location = new System.Drawing.Point(283, 197);
            this.renameBtn.Name = "renameBtn";
            this.renameBtn.Size = new System.Drawing.Size(75, 23);
            this.renameBtn.TabIndex = 16;
            this.renameBtn.Text = "&Rename";
            this.renameBtn.UseVisualStyleBackColor = true;
            this.renameBtn.Click += new System.EventHandler(this.renameBtn_Click);
            // 
            // skelNameTxtBox
            // 
            this.skelNameTxtBox.Location = new System.Drawing.Point(68, 110);
            this.skelNameTxtBox.Name = "skelNameTxtBox";
            this.skelNameTxtBox.Size = new System.Drawing.Size(268, 20);
            this.skelNameTxtBox.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 113);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Skeleton:";
            // 
            // matAfterTxtBox
            // 
            this.matAfterTxtBox.Location = new System.Drawing.Point(207, 84);
            this.matAfterTxtBox.Name = "matAfterTxtBox";
            this.matAfterTxtBox.Size = new System.Drawing.Size(129, 20);
            this.matAfterTxtBox.TabIndex = 12;
            // 
            // matBeforeTxtBox
            // 
            this.matBeforeTxtBox.Location = new System.Drawing.Point(68, 84);
            this.matBeforeTxtBox.Name = "matBeforeTxtBox";
            this.matBeforeTxtBox.Size = new System.Drawing.Size(133, 20);
            this.matBeforeTxtBox.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Material:";
            // 
            // boneAfterTxtBox
            // 
            this.boneAfterTxtBox.Location = new System.Drawing.Point(207, 58);
            this.boneAfterTxtBox.Name = "boneAfterTxtBox";
            this.boneAfterTxtBox.Size = new System.Drawing.Size(129, 20);
            this.boneAfterTxtBox.TabIndex = 9;
            // 
            // boneBeforeTxtBox
            // 
            this.boneBeforeTxtBox.Location = new System.Drawing.Point(68, 58);
            this.boneBeforeTxtBox.Name = "boneBeforeTxtBox";
            this.boneBeforeTxtBox.Size = new System.Drawing.Size(133, 20);
            this.boneBeforeTxtBox.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Bone:";
            // 
            // animAfterTxtBox
            // 
            this.animAfterTxtBox.Location = new System.Drawing.Point(207, 32);
            this.animAfterTxtBox.Name = "animAfterTxtBox";
            this.animAfterTxtBox.Size = new System.Drawing.Size(129, 20);
            this.animAfterTxtBox.TabIndex = 6;
            // 
            // animBeforeTxtBox
            // 
            this.animBeforeTxtBox.Location = new System.Drawing.Point(68, 32);
            this.animBeforeTxtBox.Name = "animBeforeTxtBox";
            this.animBeforeTxtBox.Size = new System.Drawing.Size(133, 20);
            this.animBeforeTxtBox.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(204, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "after:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(65, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "before:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Animation:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.alignCheckBox);
            this.tabPage3.Controls.Add(this.zAlignRadioBtn);
            this.tabPage3.Controls.Add(this.yAlignRadioBtn);
            this.tabPage3.Controls.Add(this.xAlignRadioBtn);
            this.tabPage3.Controls.Add(this.rotateCheckBox);
            this.tabPage3.Controls.Add(this.translateCheckBox);
            this.tabPage3.Controls.Add(this.scaleCheckBox);
            this.tabPage3.Controls.Add(this.transformBtn);
            this.tabPage3.Controls.Add(this.noUpdateBBCheckBox);
            this.tabPage3.Controls.Add(this.noNormNormCheckBox);
            this.tabPage3.Controls.Add(this.rightARadioBtn);
            this.tabPage3.Controls.Add(this.centerARadioBtn);
            this.tabPage3.Controls.Add(this.leftARadioBtn);
            this.tabPage3.Controls.Add(this.label25);
            this.tabPage3.Controls.Add(this.label24);
            this.tabPage3.Controls.Add(this.angleTxtBox);
            this.tabPage3.Controls.Add(this.label20);
            this.tabPage3.Controls.Add(this.rZTxtBox);
            this.tabPage3.Controls.Add(this.label21);
            this.tabPage3.Controls.Add(this.rYTxtBox);
            this.tabPage3.Controls.Add(this.label22);
            this.tabPage3.Controls.Add(this.rXTxtBox);
            this.tabPage3.Controls.Add(this.label23);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.tZTxtBox);
            this.tabPage3.Controls.Add(this.label17);
            this.tabPage3.Controls.Add(this.tYTxtBox);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.tXTxtBox);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.sZTxtBox);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.sYTxtBox);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.sXTxtBox);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(364, 226);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Transform";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // alignCheckBox
            // 
            this.alignCheckBox.AutoSize = true;
            this.alignCheckBox.Location = new System.Drawing.Point(343, 124);
            this.alignCheckBox.Name = "alignCheckBox";
            this.alignCheckBox.Size = new System.Drawing.Size(15, 14);
            this.alignCheckBox.TabIndex = 42;
            this.alignCheckBox.UseVisualStyleBackColor = true;
            // 
            // zAlignRadioBtn
            // 
            this.zAlignRadioBtn.AutoSize = true;
            this.zAlignRadioBtn.Location = new System.Drawing.Point(216, 121);
            this.zAlignRadioBtn.Name = "zAlignRadioBtn";
            this.zAlignRadioBtn.Size = new System.Drawing.Size(57, 17);
            this.zAlignRadioBtn.TabIndex = 41;
            this.zAlignRadioBtn.Text = "Z-align";
            this.zAlignRadioBtn.UseVisualStyleBackColor = true;
            this.zAlignRadioBtn.CheckedChanged += new System.EventHandler(this.zAlignRadioBtn_CheckedChanged);
            // 
            // yAlignRadioBtn
            // 
            this.yAlignRadioBtn.AutoSize = true;
            this.yAlignRadioBtn.Location = new System.Drawing.Point(139, 122);
            this.yAlignRadioBtn.Name = "yAlignRadioBtn";
            this.yAlignRadioBtn.Size = new System.Drawing.Size(57, 17);
            this.yAlignRadioBtn.TabIndex = 40;
            this.yAlignRadioBtn.Text = "Y-align";
            this.yAlignRadioBtn.UseVisualStyleBackColor = true;
            this.yAlignRadioBtn.CheckedChanged += new System.EventHandler(this.yAlignRadioBtn_CheckedChanged);
            // 
            // xAlignRadioBtn
            // 
            this.xAlignRadioBtn.AutoSize = true;
            this.xAlignRadioBtn.Checked = true;
            this.xAlignRadioBtn.Location = new System.Drawing.Point(62, 121);
            this.xAlignRadioBtn.Name = "xAlignRadioBtn";
            this.xAlignRadioBtn.Size = new System.Drawing.Size(57, 17);
            this.xAlignRadioBtn.TabIndex = 39;
            this.xAlignRadioBtn.TabStop = true;
            this.xAlignRadioBtn.Text = "X-align";
            this.xAlignRadioBtn.UseVisualStyleBackColor = true;
            this.xAlignRadioBtn.CheckedChanged += new System.EventHandler(this.xAlignRadioBtn_CheckedChanged);
            // 
            // rotateCheckBox
            // 
            this.rotateCheckBox.AutoSize = true;
            this.rotateCheckBox.Location = new System.Drawing.Point(343, 69);
            this.rotateCheckBox.Name = "rotateCheckBox";
            this.rotateCheckBox.Size = new System.Drawing.Size(15, 14);
            this.rotateCheckBox.TabIndex = 38;
            this.rotateCheckBox.UseVisualStyleBackColor = true;
            // 
            // translateCheckBox
            // 
            this.translateCheckBox.AutoSize = true;
            this.translateCheckBox.Location = new System.Drawing.Point(343, 44);
            this.translateCheckBox.Name = "translateCheckBox";
            this.translateCheckBox.Size = new System.Drawing.Size(15, 14);
            this.translateCheckBox.TabIndex = 37;
            this.translateCheckBox.UseVisualStyleBackColor = true;
            // 
            // scaleCheckBox
            // 
            this.scaleCheckBox.AutoSize = true;
            this.scaleCheckBox.Location = new System.Drawing.Point(343, 18);
            this.scaleCheckBox.Name = "scaleCheckBox";
            this.scaleCheckBox.Size = new System.Drawing.Size(15, 14);
            this.scaleCheckBox.TabIndex = 36;
            this.scaleCheckBox.UseVisualStyleBackColor = true;
            // 
            // transformBtn
            // 
            this.transformBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.transformBtn.Location = new System.Drawing.Point(283, 197);
            this.transformBtn.Name = "transformBtn";
            this.transformBtn.Size = new System.Drawing.Size(75, 23);
            this.transformBtn.TabIndex = 35;
            this.transformBtn.Text = "&Transform";
            this.transformBtn.UseVisualStyleBackColor = true;
            this.transformBtn.Click += new System.EventHandler(this.transformBtn_Click);
            // 
            // noUpdateBBCheckBox
            // 
            this.noUpdateBBCheckBox.AutoSize = true;
            this.noUpdateBBCheckBox.Location = new System.Drawing.Point(9, 187);
            this.noUpdateBBCheckBox.Name = "noUpdateBBCheckBox";
            this.noUpdateBBCheckBox.Size = new System.Drawing.Size(140, 17);
            this.noUpdateBBCheckBox.TabIndex = 34;
            this.noUpdateBBCheckBox.Text = "No update boundingbox";
            this.noUpdateBBCheckBox.UseVisualStyleBackColor = true;
            // 
            // noNormNormCheckBox
            // 
            this.noNormNormCheckBox.AutoSize = true;
            this.noNormNormCheckBox.Location = new System.Drawing.Point(9, 164);
            this.noNormNormCheckBox.Name = "noNormNormCheckBox";
            this.noNormNormCheckBox.Size = new System.Drawing.Size(126, 17);
            this.noNormNormCheckBox.TabIndex = 33;
            this.noNormNormCheckBox.Text = "No normalise normals";
            this.noNormNormCheckBox.UseVisualStyleBackColor = true;
            // 
            // rightARadioBtn
            // 
            this.rightARadioBtn.AutoSize = true;
            this.rightARadioBtn.Location = new System.Drawing.Point(216, 143);
            this.rightARadioBtn.Name = "rightARadioBtn";
            this.rightARadioBtn.Size = new System.Drawing.Size(45, 17);
            this.rightARadioBtn.TabIndex = 32;
            this.rightARadioBtn.Text = "right";
            this.rightARadioBtn.UseVisualStyleBackColor = true;
            // 
            // centerARadioBtn
            // 
            this.centerARadioBtn.AutoSize = true;
            this.centerARadioBtn.Location = new System.Drawing.Point(139, 144);
            this.centerARadioBtn.Name = "centerARadioBtn";
            this.centerARadioBtn.Size = new System.Drawing.Size(55, 17);
            this.centerARadioBtn.TabIndex = 31;
            this.centerARadioBtn.Text = "center";
            this.centerARadioBtn.UseVisualStyleBackColor = true;
            // 
            // leftARadioBtn
            // 
            this.leftARadioBtn.AutoSize = true;
            this.leftARadioBtn.Location = new System.Drawing.Point(62, 144);
            this.leftARadioBtn.Name = "leftARadioBtn";
            this.leftARadioBtn.Size = new System.Drawing.Size(39, 17);
            this.leftARadioBtn.TabIndex = 30;
            this.leftARadioBtn.Text = "left";
            this.leftARadioBtn.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 122);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(33, 13);
            this.label25.TabIndex = 29;
            this.label25.Text = "Align:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(59, 96);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(37, 13);
            this.label24.TabIndex = 27;
            this.label24.Text = "Angle:";
            // 
            // angleTxtBox
            // 
            this.angleTxtBox.Location = new System.Drawing.Point(102, 94);
            this.angleTxtBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.angleTxtBox.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.angleTxtBox.Name = "angleTxtBox";
            this.angleTxtBox.Size = new System.Drawing.Size(48, 20);
            this.angleTxtBox.TabIndex = 26;
            this.angleTxtBox.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(213, 70);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(17, 13);
            this.label20.TabIndex = 25;
            this.label20.Text = "Z:";
            // 
            // rZTxtBox
            // 
            this.rZTxtBox.DecimalPlaces = 2;
            this.rZTxtBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.rZTxtBox.Location = new System.Drawing.Point(236, 68);
            this.rZTxtBox.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.rZTxtBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.rZTxtBox.Name = "rZTxtBox";
            this.rZTxtBox.Size = new System.Drawing.Size(48, 20);
            this.rZTxtBox.TabIndex = 24;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(136, 70);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 13);
            this.label21.TabIndex = 23;
            this.label21.Text = "Y:";
            // 
            // rYTxtBox
            // 
            this.rYTxtBox.DecimalPlaces = 2;
            this.rYTxtBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.rYTxtBox.Location = new System.Drawing.Point(159, 68);
            this.rYTxtBox.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.rYTxtBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.rYTxtBox.Name = "rYTxtBox";
            this.rYTxtBox.Size = new System.Drawing.Size(48, 20);
            this.rYTxtBox.TabIndex = 22;
            this.rYTxtBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(59, 70);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(17, 13);
            this.label22.TabIndex = 21;
            this.label22.Text = "X:";
            // 
            // rXTxtBox
            // 
            this.rXTxtBox.DecimalPlaces = 2;
            this.rXTxtBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.rXTxtBox.Location = new System.Drawing.Point(82, 68);
            this.rXTxtBox.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.rXTxtBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.rXTxtBox.Name = "rXTxtBox";
            this.rXTxtBox.Size = new System.Drawing.Size(48, 20);
            this.rXTxtBox.TabIndex = 20;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 70);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 13);
            this.label23.TabIndex = 19;
            this.label23.Text = "Rotate";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(213, 44);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "Z:";
            // 
            // tZTxtBox
            // 
            this.tZTxtBox.DecimalPlaces = 2;
            this.tZTxtBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.tZTxtBox.Location = new System.Drawing.Point(236, 42);
            this.tZTxtBox.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.tZTxtBox.Name = "tZTxtBox";
            this.tZTxtBox.Size = new System.Drawing.Size(48, 20);
            this.tZTxtBox.TabIndex = 17;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(136, 44);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 13);
            this.label17.TabIndex = 16;
            this.label17.Text = "Y:";
            // 
            // tYTxtBox
            // 
            this.tYTxtBox.DecimalPlaces = 2;
            this.tYTxtBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.tYTxtBox.Location = new System.Drawing.Point(159, 42);
            this.tYTxtBox.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.tYTxtBox.Name = "tYTxtBox";
            this.tYTxtBox.Size = new System.Drawing.Size(48, 20);
            this.tYTxtBox.TabIndex = 15;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(59, 44);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "X:";
            // 
            // tXTxtBox
            // 
            this.tXTxtBox.DecimalPlaces = 2;
            this.tXTxtBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.tXTxtBox.Location = new System.Drawing.Point(82, 42);
            this.tXTxtBox.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.tXTxtBox.Name = "tXTxtBox";
            this.tXTxtBox.Size = new System.Drawing.Size(48, 20);
            this.tXTxtBox.TabIndex = 13;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 44);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "Translate";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(213, 18);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "Z:";
            // 
            // sZTxtBox
            // 
            this.sZTxtBox.DecimalPlaces = 2;
            this.sZTxtBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.sZTxtBox.Location = new System.Drawing.Point(236, 16);
            this.sZTxtBox.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.sZTxtBox.Name = "sZTxtBox";
            this.sZTxtBox.Size = new System.Drawing.Size(48, 20);
            this.sZTxtBox.TabIndex = 10;
            this.sZTxtBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(136, 18);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Y:";
            // 
            // sYTxtBox
            // 
            this.sYTxtBox.DecimalPlaces = 2;
            this.sYTxtBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.sYTxtBox.Location = new System.Drawing.Point(159, 16);
            this.sYTxtBox.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.sYTxtBox.Name = "sYTxtBox";
            this.sYTxtBox.Size = new System.Drawing.Size(48, 20);
            this.sYTxtBox.TabIndex = 8;
            this.sYTxtBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(59, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "X:";
            // 
            // sXTxtBox
            // 
            this.sXTxtBox.DecimalPlaces = 2;
            this.sXTxtBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.sXTxtBox.Location = new System.Drawing.Point(82, 16);
            this.sXTxtBox.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.sXTxtBox.Name = "sXTxtBox";
            this.sXTxtBox.Size = new System.Drawing.Size(48, 20);
            this.sXTxtBox.TabIndex = 6;
            this.sXTxtBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Scale";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.convertBtn);
            this.tabPage5.Controls.Add(this.quietModeCB);
            this.tabPage5.Controls.Add(this.notOptRedTracksCB);
            this.tabPage5.Controls.Add(this.genTangentCB);
            this.tabPage5.Controls.Add(this.notReVertBufCB);
            this.tabPage5.Controls.Add(this.notGenEdgeLCB);
            this.tabPage5.Controls.Add(this.lodPercentNUD);
            this.tabPage5.Controls.Add(this.label28);
            this.tabPage5.Controls.Add(this.lodDistNUD);
            this.tabPage5.Controls.Add(this.label27);
            this.tabPage5.Controls.Add(this.lodLevelsNUD);
            this.tabPage5.Controls.Add(this.label26);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(364, 226);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Convert";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // convertBtn
            // 
            this.convertBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.convertBtn.Location = new System.Drawing.Point(283, 197);
            this.convertBtn.Name = "convertBtn";
            this.convertBtn.Size = new System.Drawing.Size(75, 23);
            this.convertBtn.TabIndex = 16;
            this.convertBtn.Text = "&Convert";
            this.convertBtn.UseVisualStyleBackColor = true;
            this.convertBtn.Click += new System.EventHandler(this.convertBtn_Click);
            // 
            // quietModeCB
            // 
            this.quietModeCB.AutoSize = true;
            this.quietModeCB.Location = new System.Drawing.Point(9, 187);
            this.quietModeCB.Name = "quietModeCB";
            this.quietModeCB.Size = new System.Drawing.Size(137, 17);
            this.quietModeCB.TabIndex = 15;
            this.quietModeCB.Text = "Quiet mode, less output";
            this.quietModeCB.UseVisualStyleBackColor = true;
            // 
            // notOptRedTracksCB
            // 
            this.notOptRedTracksCB.AutoSize = true;
            this.notOptRedTracksCB.Location = new System.Drawing.Point(9, 164);
            this.notOptRedTracksCB.Name = "notOptRedTracksCB";
            this.notOptRedTracksCB.Size = new System.Drawing.Size(273, 17);
            this.notOptRedTracksCB.TabIndex = 14;
            this.notOptRedTracksCB.Text = "DON\'T optimise out redundant tracks and keyframes";
            this.notOptRedTracksCB.UseVisualStyleBackColor = true;
            // 
            // genTangentCB
            // 
            this.genTangentCB.AutoSize = true;
            this.genTangentCB.Location = new System.Drawing.Point(9, 141);
            this.genTangentCB.Name = "genTangentCB";
            this.genTangentCB.Size = new System.Drawing.Size(212, 17);
            this.genTangentCB.TabIndex = 13;
            this.genTangentCB.Text = "Generate tangents (for normal mapping)";
            this.genTangentCB.UseVisualStyleBackColor = true;
            // 
            // notReVertBufCB
            // 
            this.notReVertBufCB.AutoSize = true;
            this.notReVertBufCB.Location = new System.Drawing.Point(9, 118);
            this.notReVertBufCB.Name = "notReVertBufCB";
            this.notReVertBufCB.Size = new System.Drawing.Size(326, 17);
            this.notReVertBufCB.TabIndex = 12;
            this.notReVertBufCB.Text = "DON\'T reorganise vertex buffers to OGRE recommended format";
            this.notReVertBufCB.UseVisualStyleBackColor = true;
            // 
            // notGenEdgeLCB
            // 
            this.notGenEdgeLCB.AutoSize = true;
            this.notGenEdgeLCB.Location = new System.Drawing.Point(9, 95);
            this.notGenEdgeLCB.Name = "notGenEdgeLCB";
            this.notGenEdgeLCB.Size = new System.Drawing.Size(250, 17);
            this.notGenEdgeLCB.TabIndex = 11;
            this.notGenEdgeLCB.Text = "DON\'T generate edge lists (for stencil shadows)";
            this.notGenEdgeLCB.UseVisualStyleBackColor = true;
            // 
            // lodPercentNUD
            // 
            this.lodPercentNUD.Location = new System.Drawing.Point(99, 68);
            this.lodPercentNUD.Name = "lodPercentNUD";
            this.lodPercentNUD.Size = new System.Drawing.Size(63, 20);
            this.lodPercentNUD.TabIndex = 10;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 70);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(43, 13);
            this.label28.TabIndex = 9;
            this.label28.Text = "LOD %:";
            // 
            // lodDistNUD
            // 
            this.lodDistNUD.Location = new System.Drawing.Point(99, 42);
            this.lodDistNUD.Name = "lodDistNUD";
            this.lodDistNUD.Size = new System.Drawing.Size(63, 20);
            this.lodDistNUD.TabIndex = 8;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 44);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(75, 13);
            this.label27.TabIndex = 7;
            this.label27.Text = "LOD distance:";
            // 
            // lodLevelsNUD
            // 
            this.lodLevelsNUD.Location = new System.Drawing.Point(99, 16);
            this.lodLevelsNUD.Name = "lodLevelsNUD";
            this.lodLevelsNUD.Size = new System.Drawing.Size(63, 20);
            this.lodLevelsNUD.TabIndex = 6;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 18);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(62, 13);
            this.label26.TabIndex = 5;
            this.label26.Text = "LOD levels:";
            // 
            // fileNamesTxtBox
            // 
            this.fileNamesTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.fileNamesTxtBox.Location = new System.Drawing.Point(12, 41);
            this.fileNamesTxtBox.Name = "fileNamesTxtBox";
            this.fileNamesTxtBox.ReadOnly = true;
            this.fileNamesTxtBox.Size = new System.Drawing.Size(380, 20);
            this.fileNamesTxtBox.TabIndex = 2;
            // 
            // chooseFileBtn
            // 
            this.chooseFileBtn.Location = new System.Drawing.Point(12, 12);
            this.chooseFileBtn.Name = "chooseFileBtn";
            this.chooseFileBtn.Size = new System.Drawing.Size(99, 23);
            this.chooseFileBtn.TabIndex = 3;
            this.chooseFileBtn.Text = "&Choose file(s)...";
            this.chooseFileBtn.UseVisualStyleBackColor = true;
            this.chooseFileBtn.Click += new System.EventHandler(this.chooseFileBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Choose tool option:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "All available formats|*.mesh;*.skeleton;*.xml;*.osm|Mesh files|*.mesh|Skeleton fi" +
                "les|*.skeleton|XML files|*.xml|OSM files|*.osm";
            this.openFileDialog1.Multiselect = true;
            this.openFileDialog1.RestoreDirectory = true;
            this.openFileDialog1.Title = "Choose file(s):";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 350);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chooseFileBtn);
            this.Controls.Add(this.fileNamesTxtBox);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainForm";
            this.Text = "Ogre Mesh Tools";
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uvTolNUP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.normTolNUP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.posTolNUP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vertexTolNUP)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.angleTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rZTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rYTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rXTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tZTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tYTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tXTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sZTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sYTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sXTxtBox)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lodPercentNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lodDistNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lodLevelsNUD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox fileNamesTxtBox;
        private System.Windows.Forms.Button chooseFileBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.CheckBox keepIdTracksCheckBox;
        private System.Windows.Forms.NumericUpDown uvTolNUP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown normTolNUP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown posTolNUP;
        private System.Windows.Forms.NumericUpDown vertexTolNUP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button optimiseBtn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button renameBtn;
        private System.Windows.Forms.TextBox skelNameTxtBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox matAfterTxtBox;
        private System.Windows.Forms.TextBox matBeforeTxtBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox boneAfterTxtBox;
        private System.Windows.Forms.TextBox boneBeforeTxtBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox animAfterTxtBox;
        private System.Windows.Forms.TextBox animBeforeTxtBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown sXTxtBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown sZTxtBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown sYTxtBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown rZTxtBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown rYTxtBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown rXTxtBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown tZTxtBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown tYTxtBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown tXTxtBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown angleTxtBox;
        private System.Windows.Forms.CheckBox noNormNormCheckBox;
        private System.Windows.Forms.RadioButton rightARadioBtn;
        private System.Windows.Forms.RadioButton centerARadioBtn;
        private System.Windows.Forms.RadioButton leftARadioBtn;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button transformBtn;
        private System.Windows.Forms.CheckBox noUpdateBBCheckBox;
        private System.Windows.Forms.Button mergeBtn;
        private System.Windows.Forms.Button infoBtn;
        private System.Windows.Forms.NumericUpDown lodDistNUD;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.NumericUpDown lodLevelsNUD;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox quietModeCB;
        private System.Windows.Forms.CheckBox notOptRedTracksCB;
        private System.Windows.Forms.CheckBox genTangentCB;
        private System.Windows.Forms.CheckBox notReVertBufCB;
        private System.Windows.Forms.CheckBox notGenEdgeLCB;
        private System.Windows.Forms.NumericUpDown lodPercentNUD;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button convertBtn;
        private System.Windows.Forms.TextBox mergedFiletxtBox;
        private System.Windows.Forms.RadioButton xAlignRadioBtn;
        private System.Windows.Forms.CheckBox rotateCheckBox;
        private System.Windows.Forms.CheckBox translateCheckBox;
        private System.Windows.Forms.CheckBox scaleCheckBox;
        private System.Windows.Forms.RadioButton zAlignRadioBtn;
        private System.Windows.Forms.RadioButton yAlignRadioBtn;
        private System.Windows.Forms.CheckBox alignCheckBox;
    }
}

