using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Mogre;
using PAGE.Orient;

namespace SetEditor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            using (MainForm frm = new MainForm())
            {
                frm.Show();
                
                while (frm.Created)
                {
                    try
                    {
                        OrientGraphicsRealizer.Instance.Update();
                        Application.DoEvents();
                    }
                    catch (SEHException)
                    {
                        // Check if it's an Ogre Exception
                        if (OgreException.IsThrown)
                            ShowOgreException();
                        else
                            throw;
                    }
                }
            }
        }

        public static void ShowOgreException()
        {
            if (OgreException.IsThrown)
                MessageBox.Show(
                    OgreException.LastException.FullDescription,
                    "An exception has occured!", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
        }
    }
}