using System.Windows.Forms;
using PreviewPanel=PAGE.Orient.GUI.Windows.Forms.Controls.PreviewPanel;
using SetControl=PAGE.Orient.GUI.Windows.Forms.Controls.SetControl;

namespace SetEditor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cameraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectCamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.editSelectedCamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSelectedCamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteAllCamsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.waypointsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectWaypointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.editSelectedWaypointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSelectedWaypointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteAllWaypointsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemspotsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectItemSpotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.editselectedItemSpotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSelectedItemSpotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteAllItemspotsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createGridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setpointsEditionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.globalPanel = new System.Windows.Forms.Panel();
            this.setControl1 = new PAGE.Orient.GUI.Windows.Forms.Controls.SetControl();
            this.screenshotBtn = new System.Windows.Forms.Button();
            this.backPanel = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxItemSpotName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.itemSpotHeightTxtBox = new System.Windows.Forms.NumericUpDown();
            this.lblItem = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.wpHeightTextBox = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.camHeightTxtBox = new System.Windows.Forms.NumericUpDown();
            this.lblCam = new System.Windows.Forms.Label();
            this.lblWaypoint = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.previewPanel = new PAGE.Orient.GUI.Windows.Forms.Controls.PreviewPanel(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.saveXMLFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openOSMFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.openXMLFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddCamAtCamPos = new System.Windows.Forms.Button();
            this.btnAddWayPointAtCamPos = new System.Windows.Forms.Button();
            this.btnAddItemSpotAtCamPos = new System.Windows.Forms.Button();
            this.editionPanel = new SetEditor.RectanglePanel();
            this.menuStrip1.SuspendLayout();
            this.globalPanel.SuspendLayout();
            this.backPanel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemSpotHeightTxtBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wpHeightTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.camHeightTxtBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.cameraToolStripMenuItem,
            this.waypointsToolStripMenuItem,
            this.itemspotsToolStripMenuItem,
            this.selectedToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(788, 24);
            this.menuStrip1.TabIndex = 34;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.newToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.openToolStripMenuItem.Text = "&Open...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.newToolStripMenuItem.Text = "&New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.saveAsToolStripMenuItem.Text = "Save &As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(160, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // cameraToolStripMenuItem
            // 
            this.cameraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectCamToolStripMenuItem,
            this.toolStripSeparator3,
            this.editSelectedCamToolStripMenuItem,
            this.deleteSelectedCamToolStripMenuItem,
            this.deleteAllCamsToolStripMenuItem});
            this.cameraToolStripMenuItem.Enabled = false;
            this.cameraToolStripMenuItem.Name = "cameraToolStripMenuItem";
            this.cameraToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.cameraToolStripMenuItem.Text = "&Cameras";
            // 
            // selectCamToolStripMenuItem
            // 
            this.selectCamToolStripMenuItem.Name = "selectCamToolStripMenuItem";
            this.selectCamToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.selectCamToolStripMenuItem.Text = "&Select";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(145, 6);
            // 
            // editSelectedCamToolStripMenuItem
            // 
            this.editSelectedCamToolStripMenuItem.Enabled = false;
            this.editSelectedCamToolStripMenuItem.Name = "editSelectedCamToolStripMenuItem";
            this.editSelectedCamToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.editSelectedCamToolStripMenuItem.Text = "&Edit Selected";
            this.editSelectedCamToolStripMenuItem.Click += new System.EventHandler(this.editSelectedCamToolStripMenuItem_Click);
            // 
            // deleteSelectedCamToolStripMenuItem
            // 
            this.deleteSelectedCamToolStripMenuItem.Enabled = false;
            this.deleteSelectedCamToolStripMenuItem.Name = "deleteSelectedCamToolStripMenuItem";
            this.deleteSelectedCamToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.deleteSelectedCamToolStripMenuItem.Text = "&Delete selected";
            this.deleteSelectedCamToolStripMenuItem.Click += new System.EventHandler(this.deleteSelectedCamToolStripMenuItem_Click);
            // 
            // deleteAllCamsToolStripMenuItem
            // 
            this.deleteAllCamsToolStripMenuItem.Name = "deleteAllCamsToolStripMenuItem";
            this.deleteAllCamsToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.deleteAllCamsToolStripMenuItem.Text = "Delete &All";
            this.deleteAllCamsToolStripMenuItem.Click += new System.EventHandler(this.deleteAllCamsToolStripMenuItem_Click);
            // 
            // waypointsToolStripMenuItem
            // 
            this.waypointsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectWaypointToolStripMenuItem,
            this.toolStripSeparator4,
            this.editSelectedWaypointToolStripMenuItem,
            this.deleteSelectedWaypointToolStripMenuItem,
            this.deleteAllWaypointsToolStripMenuItem});
            this.waypointsToolStripMenuItem.Enabled = false;
            this.waypointsToolStripMenuItem.Name = "waypointsToolStripMenuItem";
            this.waypointsToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.waypointsToolStripMenuItem.Text = "&Waypoints";
            // 
            // selectWaypointToolStripMenuItem
            // 
            this.selectWaypointToolStripMenuItem.Name = "selectWaypointToolStripMenuItem";
            this.selectWaypointToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.selectWaypointToolStripMenuItem.Text = "&Select";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(146, 6);
            // 
            // editSelectedWaypointToolStripMenuItem
            // 
            this.editSelectedWaypointToolStripMenuItem.Enabled = false;
            this.editSelectedWaypointToolStripMenuItem.Name = "editSelectedWaypointToolStripMenuItem";
            this.editSelectedWaypointToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.editSelectedWaypointToolStripMenuItem.Text = "&Edit Selected";
            this.editSelectedWaypointToolStripMenuItem.Click += new System.EventHandler(this.editSelectedWaypointToolStripMenuItem_Click);
            // 
            // deleteSelectedWaypointToolStripMenuItem
            // 
            this.deleteSelectedWaypointToolStripMenuItem.Enabled = false;
            this.deleteSelectedWaypointToolStripMenuItem.Name = "deleteSelectedWaypointToolStripMenuItem";
            this.deleteSelectedWaypointToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.deleteSelectedWaypointToolStripMenuItem.Text = "&Delete Selected";
            this.deleteSelectedWaypointToolStripMenuItem.Click += new System.EventHandler(this.deleteSelectedWaypointToolStripMenuItem_Click);
            // 
            // deleteAllWaypointsToolStripMenuItem
            // 
            this.deleteAllWaypointsToolStripMenuItem.Name = "deleteAllWaypointsToolStripMenuItem";
            this.deleteAllWaypointsToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.deleteAllWaypointsToolStripMenuItem.Text = "Delete &All";
            this.deleteAllWaypointsToolStripMenuItem.Click += new System.EventHandler(this.deleteAllWaypointsToolStripMenuItem_Click);
            // 
            // itemspotsToolStripMenuItem
            // 
            this.itemspotsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectItemSpotToolStripMenuItem,
            this.toolStripSeparator5,
            this.editselectedItemSpotToolStripMenuItem,
            this.deleteSelectedItemSpotToolStripMenuItem,
            this.deleteAllItemspotsToolStripMenuItem});
            this.itemspotsToolStripMenuItem.Enabled = false;
            this.itemspotsToolStripMenuItem.Name = "itemspotsToolStripMenuItem";
            this.itemspotsToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.itemspotsToolStripMenuItem.Text = "&Item Spots";
            // 
            // selectItemSpotToolStripMenuItem
            // 
            this.selectItemSpotToolStripMenuItem.Name = "selectItemSpotToolStripMenuItem";
            this.selectItemSpotToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.selectItemSpotToolStripMenuItem.Text = "&Select";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(146, 6);
            // 
            // editselectedItemSpotToolStripMenuItem
            // 
            this.editselectedItemSpotToolStripMenuItem.Enabled = false;
            this.editselectedItemSpotToolStripMenuItem.Name = "editselectedItemSpotToolStripMenuItem";
            this.editselectedItemSpotToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.editselectedItemSpotToolStripMenuItem.Text = "&Edit Selected";
            this.editselectedItemSpotToolStripMenuItem.Click += new System.EventHandler(this.editselectedItemSpotToolStripMenuItem_Click);
            // 
            // deleteSelectedItemSpotToolStripMenuItem
            // 
            this.deleteSelectedItemSpotToolStripMenuItem.Enabled = false;
            this.deleteSelectedItemSpotToolStripMenuItem.Name = "deleteSelectedItemSpotToolStripMenuItem";
            this.deleteSelectedItemSpotToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.deleteSelectedItemSpotToolStripMenuItem.Text = "&Delete Selected";
            this.deleteSelectedItemSpotToolStripMenuItem.Click += new System.EventHandler(this.deleteSelectedItemSpotToolStripMenuItem_Click);
            // 
            // deleteAllItemspotsToolStripMenuItem
            // 
            this.deleteAllItemspotsToolStripMenuItem.Name = "deleteAllItemspotsToolStripMenuItem";
            this.deleteAllItemspotsToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.deleteAllItemspotsToolStripMenuItem.Text = "Delete &All";
            this.deleteAllItemspotsToolStripMenuItem.Click += new System.EventHandler(this.deleteAllItemspotsToolStripMenuItem_Click);
            // 
            // selectedToolStripMenuItem
            // 
            this.selectedToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem,
            this.createGridToolStripMenuItem});
            this.selectedToolStripMenuItem.Enabled = false;
            this.selectedToolStripMenuItem.Name = "selectedToolStripMenuItem";
            this.selectedToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.selectedToolStripMenuItem.Text = "&Selection";
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.deleteToolStripMenuItem.Text = "&Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // createGridToolStripMenuItem
            // 
            this.createGridToolStripMenuItem.Name = "createGridToolStripMenuItem";
            this.createGridToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.createGridToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.createGridToolStripMenuItem.Text = "Create &Grid";
            this.createGridToolStripMenuItem.Click += new System.EventHandler(this.createGridToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setpointsEditionToolStripMenuItem,
            this.toolStripSeparator2,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // setpointsEditionToolStripMenuItem
            // 
            this.setpointsEditionToolStripMenuItem.Name = "setpointsEditionToolStripMenuItem";
            this.setpointsEditionToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.setpointsEditionToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.setpointsEditionToolStripMenuItem.Text = "Set &points edition";
            this.setpointsEditionToolStripMenuItem.Click += new System.EventHandler(this.setpointsEditionToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(173, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // globalPanel
            // 
            this.globalPanel.Controls.Add(this.setControl1);
            this.globalPanel.Controls.Add(this.screenshotBtn);
            this.globalPanel.Controls.Add(this.backPanel);
            this.globalPanel.Controls.Add(this.label6);
            this.globalPanel.Controls.Add(this.label7);
            this.globalPanel.Controls.Add(this.groupBox2);
            this.globalPanel.Controls.Add(this.previewPanel);
            this.globalPanel.Controls.Add(this.label10);
            this.globalPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.globalPanel.Enabled = false;
            this.globalPanel.Location = new System.Drawing.Point(0, 24);
            this.globalPanel.Name = "globalPanel";
            this.globalPanel.Size = new System.Drawing.Size(788, 595);
            this.globalPanel.TabIndex = 35;
            // 
            // setControl1
            // 
            this.setControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.setControl1.Enabled = false;
            this.setControl1.Location = new System.Drawing.Point(446, 304);
            this.setControl1.Name = "setControl1";
            this.setControl1.ResourcesManager = null;
            this.setControl1.Size = new System.Drawing.Size(320, 288);
            this.setControl1.TabIndex = 43;
            this.setControl1.TokenObject = null;
            this.setControl1.TokenObjectList = null;
            // 
            // screenshotBtn
            // 
            this.screenshotBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.screenshotBtn.Location = new System.Drawing.Point(691, 268);
            this.screenshotBtn.Name = "screenshotBtn";
            this.screenshotBtn.Size = new System.Drawing.Size(75, 23);
            this.screenshotBtn.TabIndex = 19;
            this.screenshotBtn.Text = "Screenshot";
            this.screenshotBtn.UseVisualStyleBackColor = true;
            this.screenshotBtn.Click += new System.EventHandler(this.screenshotBtn_Click);
            // 
            // backPanel
            // 
            this.backPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.backPanel.AutoScroll = true;
            this.backPanel.BackColor = System.Drawing.Color.White;
            this.backPanel.Controls.Add(this.editionPanel);
            this.backPanel.Location = new System.Drawing.Point(11, 8);
            this.backPanel.Name = "backPanel";
            this.backPanel.Size = new System.Drawing.Size(423, 459);
            this.backPanel.TabIndex = 40;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.Location = new System.Drawing.Point(8, 470);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(144, 15);
            this.label6.TabIndex = 39;
            this.label6.Text = "Zoom: rotate mouse wheel";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.Location = new System.Drawing.Point(443, 273);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(224, 18);
            this.label7.TabIndex = 35;
            this.label7.Text = "Zoom: + Left Mouse Btn, - Right Mouse Btn";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.btnAddItemSpotAtCamPos);
            this.groupBox2.Controls.Add(this.btnAddWayPointAtCamPos);
            this.groupBox2.Controls.Add(this.btnAddCamAtCamPos);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBoxItemSpotName);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.itemSpotHeightTxtBox);
            this.groupBox2.Controls.Add(this.lblItem);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Controls.Add(this.wpHeightTextBox);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.camHeightTxtBox);
            this.groupBox2.Controls.Add(this.lblCam);
            this.groupBox2.Controls.Add(this.lblWaypoint);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.panel4);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Location = new System.Drawing.Point(11, 488);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(426, 104);
            this.groupBox2.TabIndex = 37;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Set Points";
            // 
            // textBoxItemSpotName
            // 
            this.textBoxItemSpotName.Location = new System.Drawing.Point(280, 79);
            this.textBoxItemSpotName.Name = "textBoxItemSpotName";
            this.textBoxItemSpotName.Size = new System.Drawing.Size(86, 20);
            this.textBoxItemSpotName.TabIndex = 38;
            this.textBoxItemSpotName.Text = "new-item-spot";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(277, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 13);
            this.label8.TabIndex = 37;
            this.label8.Text = "new spot\'s name:";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(83, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 16);
            this.label3.TabIndex = 36;
            this.label3.Text = "item spots (alt+left click)";
            // 
            // itemSpotHeightTxtBox
            // 
            this.itemSpotHeightTxtBox.Location = new System.Drawing.Point(230, 82);
            this.itemSpotHeightTxtBox.Name = "itemSpotHeightTxtBox";
            this.itemSpotHeightTxtBox.Size = new System.Drawing.Size(41, 20);
            this.itemSpotHeightTxtBox.TabIndex = 35;
            // 
            // lblItem
            // 
            this.lblItem.BackColor = System.Drawing.Color.White;
            this.lblItem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblItem.Location = new System.Drawing.Point(37, 83);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(40, 16);
            this.lblItem.TabIndex = 34;
            this.lblItem.Text = "0";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGreen;
            this.panel1.Location = new System.Drawing.Point(15, 84);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(16, 8);
            this.panel1.TabIndex = 22;
            // 
            // wpHeightTextBox
            // 
            this.wpHeightTextBox.Location = new System.Drawing.Point(230, 38);
            this.wpHeightTextBox.Name = "wpHeightTextBox";
            this.wpHeightTextBox.Size = new System.Drawing.Size(41, 20);
            this.wpHeightTextBox.TabIndex = 32;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(160, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "new point height:";
            // 
            // camHeightTxtBox
            // 
            this.camHeightTxtBox.Location = new System.Drawing.Point(230, 60);
            this.camHeightTxtBox.Name = "camHeightTxtBox";
            this.camHeightTxtBox.Size = new System.Drawing.Size(41, 20);
            this.camHeightTxtBox.TabIndex = 15;
            this.camHeightTxtBox.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // lblCam
            // 
            this.lblCam.BackColor = System.Drawing.Color.White;
            this.lblCam.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCam.Location = new System.Drawing.Point(37, 62);
            this.lblCam.Name = "lblCam";
            this.lblCam.Size = new System.Drawing.Size(40, 16);
            this.lblCam.TabIndex = 28;
            this.lblCam.Text = "0";
            // 
            // lblWaypoint
            // 
            this.lblWaypoint.BackColor = System.Drawing.Color.White;
            this.lblWaypoint.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblWaypoint.Location = new System.Drawing.Point(37, 42);
            this.lblWaypoint.Name = "lblWaypoint";
            this.lblWaypoint.Size = new System.Drawing.Size(40, 16);
            this.lblWaypoint.TabIndex = 27;
            this.lblWaypoint.Text = "0";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(83, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(141, 16);
            this.label5.TabIndex = 24;
            this.label5.Text = "camera positions (right click)";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Blue;
            this.panel4.Location = new System.Drawing.Point(15, 62);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(16, 8);
            this.panel4.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(83, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 16);
            this.label4.TabIndex = 22;
            this.label4.Text = "waypoints (left-click)";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Red;
            this.panel3.Location = new System.Drawing.Point(15, 42);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(16, 8);
            this.panel3.TabIndex = 21;
            // 
            // previewPanel
            // 
            this.previewPanel.AllowNavigation = false;
            this.previewPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.previewPanel.CaptureKeyboard = false;
            this.previewPanel.CaptureMouse = true;
            this.previewPanel.Location = new System.Drawing.Point(446, 20);
            this.previewPanel.MoveSpeed = 50F;
            this.previewPanel.Name = "previewPanel";
            this.previewPanel.RotateSpeed = 50F;
            this.previewPanel.Size = new System.Drawing.Size(320, 240);
            this.previewPanel.TabIndex = 34;
            this.previewPanel.UpdateInterval = ((long)(10));
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(440, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 13);
            this.label10.TabIndex = 38;
            this.label10.Text = "Set preview window";
            // 
            // saveXMLFileDialog
            // 
            this.saveXMLFileDialog.DefaultExt = "xml";
            this.saveXMLFileDialog.FileName = "set.xml";
            this.saveXMLFileDialog.Filter = "Xml files (*.xml)|*.xml";
            this.saveXMLFileDialog.Title = "Save Set information";
            // 
            // openOSMFileDialog
            // 
            this.openOSMFileDialog.DefaultExt = "osm";
            this.openOSMFileDialog.Filter = "OSM files (*.osm)|*.osm";
            this.openOSMFileDialog.RestoreDirectory = true;
            this.openOSMFileDialog.Title = "Open OSM File";
            // 
            // openXMLFileDialog
            // 
            this.openXMLFileDialog.DefaultExt = "xml";
            this.openXMLFileDialog.FileName = "set.xml";
            this.openXMLFileDialog.Filter = "Xml files (*.xml)|*.xml";
            this.openXMLFileDialog.RestoreDirectory = true;
            this.openXMLFileDialog.Title = "Open Set File";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(254, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 13);
            this.label2.TabIndex = 39;
            this.label2.Text = "or add at preview cam position:";
            // 
            // btnAddCamAtCamPos
            // 
            this.btnAddCamAtCamPos.Location = new System.Drawing.Point(372, 59);
            this.btnAddCamAtCamPos.Name = "btnAddCamAtCamPos";
            this.btnAddCamAtCamPos.Size = new System.Drawing.Size(36, 19);
            this.btnAddCamAtCamPos.TabIndex = 40;
            this.btnAddCamAtCamPos.Text = "add";
            this.btnAddCamAtCamPos.UseVisualStyleBackColor = true;
            this.btnAddCamAtCamPos.Click += new System.EventHandler(this.btnAddCamAtCamPos_Click);
            // 
            // btnAddWayPointAtCamPos
            // 
            this.btnAddWayPointAtCamPos.Location = new System.Drawing.Point(372, 36);
            this.btnAddWayPointAtCamPos.Name = "btnAddWayPointAtCamPos";
            this.btnAddWayPointAtCamPos.Size = new System.Drawing.Size(36, 21);
            this.btnAddWayPointAtCamPos.TabIndex = 41;
            this.btnAddWayPointAtCamPos.Text = "add";
            this.btnAddWayPointAtCamPos.UseVisualStyleBackColor = true;
            this.btnAddWayPointAtCamPos.Click += new System.EventHandler(this.btnAddWayPointAtCamPos_Click);
            // 
            // btnAddItemSpotAtCamPos
            // 
            this.btnAddItemSpotAtCamPos.Location = new System.Drawing.Point(372, 80);
            this.btnAddItemSpotAtCamPos.Name = "btnAddItemSpotAtCamPos";
            this.btnAddItemSpotAtCamPos.Size = new System.Drawing.Size(36, 19);
            this.btnAddItemSpotAtCamPos.TabIndex = 42;
            this.btnAddItemSpotAtCamPos.Text = "add";
            this.btnAddItemSpotAtCamPos.UseVisualStyleBackColor = true;
            this.btnAddItemSpotAtCamPos.Click += new System.EventHandler(this.btnAddItemSpotAtCamPos_Click);
            // 
            // editionPanel
            // 
            this.editionPanel.Location = new System.Drawing.Point(0, 0);
            this.editionPanel.Name = "editionPanel";
            this.editionPanel.RectangleColor = System.Drawing.Color.Green;
            this.editionPanel.SelectedArea = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.editionPanel.Size = new System.Drawing.Size(272, 322);
            this.editionPanel.TabIndex = 28;
            this.editionPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.editionPanel_Paint);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(788, 619);
            this.Controls.Add(this.globalPanel);
            this.Controls.Add(this.menuStrip1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Set Editor";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.globalPanel.ResumeLayout(false);
            this.globalPanel.PerformLayout();
            this.backPanel.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemSpotHeightTxtBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wpHeightTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.camHeightTxtBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private Panel globalPanel;
        private ToolStripMenuItem openToolStripMenuItem;
        private ToolStripMenuItem saveAsToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem exitToolStripMenuItem;
        private SaveFileDialog saveXMLFileDialog;
        private OpenFileDialog openOSMFileDialog;
        private Label label6;
        private Label label7;
        private GroupBox groupBox2;
        private NumericUpDown camHeightTxtBox;
        private Label lblCam;
        private Label lblWaypoint;
        private Label label5;
        private Panel panel4;
        private Label label4;
        private Panel panel3;
        private PreviewPanel previewPanel;
        private Label label10;
        private ToolStripMenuItem newToolStripMenuItem;
        private OpenFileDialog openXMLFileDialog;
        private ToolStripMenuItem helpToolStripMenuItem;
        private ToolStripMenuItem setpointsEditionToolStripMenuItem;
        private Button screenshotBtn;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem aboutToolStripMenuItem;
        private ToolStripMenuItem selectedToolStripMenuItem;
        private ToolStripMenuItem deleteToolStripMenuItem;
        private ToolStripMenuItem createGridToolStripMenuItem;
        private ToolStripMenuItem cameraToolStripMenuItem;
        private ToolStripMenuItem selectCamToolStripMenuItem;
        private ToolStripMenuItem deleteSelectedCamToolStripMenuItem;
        private SetControl setControl1;
        private ToolStripMenuItem editSelectedCamToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripMenuItem waypointsToolStripMenuItem;
        private ToolStripMenuItem selectWaypointToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripMenuItem editSelectedWaypointToolStripMenuItem;
        private ToolStripMenuItem deleteSelectedWaypointToolStripMenuItem;
        private ToolStripMenuItem deleteAllCamsToolStripMenuItem;
        private ToolStripMenuItem deleteAllWaypointsToolStripMenuItem;
        private RectanglePanel editionPanel;
        private Panel backPanel;
        private ToolStripMenuItem itemspotsToolStripMenuItem;
        private ToolStripMenuItem selectItemSpotToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripMenuItem editselectedItemSpotToolStripMenuItem;
        private ToolStripMenuItem deleteSelectedItemSpotToolStripMenuItem;
        private ToolStripMenuItem deleteAllItemspotsToolStripMenuItem;
        private NumericUpDown wpHeightTextBox;
        private Label label1;
        private Label label3;
        private NumericUpDown itemSpotHeightTxtBox;
        private Label lblItem;
        private Panel panel1;
        private TextBox textBoxItemSpotName;
        private Label label8;
        private Button btnAddItemSpotAtCamPos;
        private Button btnAddWayPointAtCamPos;
        private Button btnAddCamAtCamPos;
        private Label label2;
    }
}

