/* ****************************************************
 * Name: MainForm.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Mogre;
using PAGE.Generic.Resources.Assets;
using PAGE.Generic.Resources.Spots;
using PAGE.OGRE.Domain.Assets;
using PAGE.OGRE.Domain.Spots;
using PAGE.OGRE.Domain.Cameras;
using PAGE.OGRE.Math;
using PAGE.Orient;
using PAGE.Orient.Domain.Assets;
using PAGE.Orient.Resources.Assets;
using PAGE.Orient.GUI.Windows.Forms.Forms;
using PAGE.Orient.GUI.Windows.Forms.Controls;
using PAGE.Util;
using Math=System.Math;
using Rectangle=System.Drawing.Rectangle;

namespace SetEditor
{
    public partial class MainForm : Form
    {
        protected OgreSet set = null;
        protected OrientCharacter character = null;
        protected bool saved = true;
        protected HelpForm hf = null;
        protected ResourceCameraSpot curCamera = null;
        protected ResourceWaypoint curWaypoint = null;
        protected ResourceItemSpot curItemSpot = null;
        protected string oldOSMname = "";
        protected GridSizeForm gsf = null;

        protected TokenObjectList<ResourceSpot> selected = new TokenObjectList<ResourceSpot>();

        public MainForm()
        {
            InitializeComponent();

            OrientGraphicsRealizer.Instance.Setup("./data/resources.xml", this.previewPanel);

            this.previewPanel.AllowNavigation = true;

            this.character = (OrientCharacter) 
                OrientGraphicsRealizer.Instance.LoadCharacter(OrientGraphicsRealizer.Instance.ResourcesManager.Characters[0].IdToken);
            this.character.Animate(null, "stand");
            this.character.Visible = false;
            this.character.HideAllDebuggingSpots();

            #region Events
            this.editionPanel.MouseDown += this.editionPanel_MouseDown;
            this.editionPanel.SelectedAreaChanged += this.editionPanel_SelectedAreaChanged;

            this.MouseWheel += this.MainForm_MouseWheel;

            this.selectCamToolStripMenuItem.DropDownItemClicked += this.selectCamToolStripMenuItem_DropDownItemClicked;
            this.selectWaypointToolStripMenuItem.DropDownItemClicked += this.selectWaypointToolStripMenuItem_DropDownItemClicked;
            this.selectItemSpotToolStripMenuItem.DropDownItemClicked += this.selectItemSpotToolStripMenuItem_DropDownItemClicked;

            this.setControl1.TokenObjectList = OrientGraphicsRealizer.Instance.ResourcesManager.Sets;
            this.setControl1.TokenChanged += this.setControl1_TokenChanged;

            #endregion
        }

        #region Points Panel Handling

        private float minX = float.MaxValue;
        private float minY = float.MaxValue;
        private float width, height;
        private ArrayList recs = new ArrayList();
        private float scaleFactor = 10;
        private int hScroll = 0, vScroll = 0;

        private TokenObjectList<ResourceWaypoint> Waypoints
        {
            get { return this.set.ResourceAsset.ResourceWaypoints; }
        }

        private TokenObjectList<ResourceCameraSpot> CameraSpots
        {
            get { return this.set.ResourceAsset.ResourceCameraSpots; }
        }

        private TokenObjectList<ResourceItemSpot> ItemSpots
        {
            get { return this.set.ResourceAsset.ResourceItemSpots; }
        }

        private void editionPanel_Paint(object sender, PaintEventArgs e)
        {
            //draws set and waypoints
            if (this.recs.Count == 0) return;

            Graphics g = e.Graphics;

            //print rectangles
            foreach (Rectangle rec in recs)
            {
                Point p = new Point((int)(rec.Location.X - this.minX),
                    (int)(rec.Location.Y - this.minY));
                Rectangle newRec =
                    new Rectangle(p, rec.Size);
                g.DrawRectangle(new Pen(Color.Black, 1), newRec);
            }

            float circleWidth = 0.7f/this.scaleFactor;
            int radius = (int)(2.0f / this.scaleFactor);

            //print waypoints 
            foreach (ResourceWaypoint spot in this.Waypoints)
            {
                OgrePosition relativePosition = (OgrePosition) spot.RelativePosition;
                int x = (int)((relativePosition.X / this.scaleFactor) - minX);
                int y = (int)((relativePosition.Z / this.scaleFactor) - minY);
                
                Pen pen = new Pen(Color.Red, circleWidth);

                if(this.selected.Contains(spot)) pen.Color = Color.Yellow;

                g.DrawEllipse(pen, x - radius, y - radius, radius, radius);
            }

            //print item spots 
            foreach (ResourceItemSpot spot in this.ItemSpots)
            {
                OgrePosition relativePosition = (OgrePosition)spot.RelativePosition;
                int x = (int)((relativePosition.X / this.scaleFactor) - minX);
                int y = (int)((relativePosition.Z / this.scaleFactor) - minY);

                Pen pen = new Pen(Color.LightGreen, circleWidth);

                if (this.selected.Contains(spot)) pen.Color = Color.Yellow;

                g.DrawEllipse(pen, x - radius, y - radius, radius, radius);
            }

            //print camera points
            foreach (ResourceCameraSpot spot in this.CameraSpots)
            {
                OgrePosition relativePosition = (OgrePosition)spot.RelativePosition;
                int x = (int)((relativePosition.X / this.scaleFactor) - minX);
                int y = (int)((relativePosition.Z / this.scaleFactor) - minY);

                Pen pen = new Pen(Color.Blue, circleWidth);

                if (this.selected.Contains(spot)) pen.Color = Color.Yellow;

                g.DrawEllipse(pen, x - radius, y - radius, radius, radius);
            }
        }

        private void MainForm_MouseWheel(object sender, MouseEventArgs e)
        {
            this.hScroll = this.editionPanel.HorizontalScroll.Value;
            this.vScroll = this.editionPanel.VerticalScroll.Value;

            this.scaleFactor += e.Delta * 0.0005f;
            if (this.scaleFactor < 0.1f) this.scaleFactor = 0.1f;
            this.CalcRecs();
            this.editionPanel.Invalidate(true);
            this.editionPanel.Refresh();

            this.editionPanel.HorizontalScroll.Value = this.hScroll;
            this.editionPanel.VerticalScroll.Value = this.vScroll;
        } 

        private void editionPanel_MouseDown(object sender, MouseEventArgs e)
        {
            OgrePosition spotVec = new OgrePosition(
                (e.X + this.minX) * this.scaleFactor, 0,
                (e.Y + this.minY) * this.scaleFactor);

            if (e.Button == MouseButtons.Left)
            {
                if (Control.ModifierKeys == Keys.Alt) // alt+left inserts an item spot
                {
                    spotVec.Y = float.Parse(this.itemSpotHeightTxtBox.Text);
                    string spotName = this.textBoxItemSpotName.Text;
                    this.AddItemSpot(spotVec, spotName);
                }
                else
                // normal left click inserts a waypoint
                {
                    spotVec.Y = float.Parse(this.camHeightTxtBox.Text);
                    this.AddWaypoint(spotVec);
                }           
            }
            else if (e.Button == MouseButtons.Right)
            {
                //cam pos
                spotVec.Y = float.Parse(this.camHeightTxtBox.Text);
                this.AddCameraSpot(spotVec);
            }

            this.RefreshAll();
        }

        private void editionPanel_SelectedAreaChanged(object sender, EventArgs e)
        {
            if ((this.editionPanel.SelectedArea.Height > 1) &&
                (this.editionPanel.SelectedArea.Width > 1))
            {
                this.selectedToolStripMenuItem.Enabled = true;
                this.CalcSelected();
                if (this.selected.Count > 0)
                    this.deleteToolStripMenuItem.Enabled = true;
                else
                    this.deleteToolStripMenuItem.Enabled = false;
            }
            else
            {
                this.selectedToolStripMenuItem.Enabled = false;
                this.selected.Clear();
            }
        }

        private void CalcSelected()
        {
            this.selected.Clear();
            TokenObjectList<ResourceSpot> allSpots = this.set.ResourceAsset.AllSpots;

            foreach (ResourceSpot spot in allSpots)
            {
                OgrePosition relativePosition = (OgrePosition)spot.RelativePosition;
                int x = (int)((relativePosition.X/this.scaleFactor) - this.minX);
                int y = (int)((relativePosition.Z/this.scaleFactor) - this.minY);
                if(this.editionPanel.SelectedArea.Contains(x, y))
                {
                    this.selected.Add(spot);
                }
            }
        }
        #endregion

        #region Form Buttons

        private void screenshotBtn_Click(object sender, EventArgs e)
        {
            OrientGraphicsRealizer.Instance.SaveScreenshot("SetEditorScreenshot.png");
        }

        #endregion

        #region Menu Buttons

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.saved)
            {
                DialogResult result = MessageBox.Show(
                    "Set file not saved!\nDo you want to save changes?", "SetEditor",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                else if (result == DialogResult.Yes)
                {
                    this.saveAsToolStripMenuItem_Click(this, EventArgs.Empty);
                }
            }

            if (this.openXMLFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = this.openXMLFileDialog.FileName;
                this.saveXMLFileDialog.FileName = fileName;

                //loads set from xml file
                OrientResourceSet resourceSet = new OrientResourceSet("set0",
                    OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
                resourceSet.LoadFromXml(fileName);
                if (resourceSet.IdToken == null)
                {
                    MessageBox.Show("Invalid set file: " + fileName);
                    return;
                }
                //if exists, removes set from loaded list and adds new one
                OrientGraphicsRealizer.Instance.ResourcesManager.Sets.Remove(resourceSet.IdToken);
                OrientGraphicsRealizer.Instance.ResourcesManager.Sets.Add(resourceSet);
                this.CreateNewSet(resourceSet.IdToken);
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //saves set file
            this.saveXMLFileDialog.FileName = this.set.ResourceAsset.IdToken + ".xml";
            if (this.saveXMLFileDialog.ShowDialog() == DialogResult.OK)
            {
                //calculate navigation paths
                this.set.CreateNavigationPaths();

                //serialize set
                this.set.ResourceAsset.SaveToXml(this.saveXMLFileDialog.FileName);

                this.saved = true;
                this.RefreshAll();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.saved)
            {
                DialogResult result = MessageBox.Show(
                    "Set file not saved!\nDo you want to save changes?", "SetEditor",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                else if (result == DialogResult.Yes)
                {
                    this.saveAsToolStripMenuItem_Click(this, EventArgs.Empty);
                }
            }

            int numSet = 0;
            while(OrientGraphicsRealizer.Instance.ResourcesManager.Sets.Contains("set " + numSet))
            {
                numSet++;
            }
            ResourceSet resourceSet = new ResourceSet("set " + numSet, 
                OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
            OrientGraphicsRealizer.Instance.ResourcesManager.Sets.Add(resourceSet);

            this.CreateNewSet(resourceSet.IdToken);
        }

        private void setpointsEditionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.hf = new HelpForm();
            this.hf.FormClosed += this.hf_FormClosed;
            this.hf.Show();
            this.hf.Refresh();
            this.Enabled = false;
        }

        private void hf_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Enabled = true;
            this.Focus();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox ab = new AboutBox();
            ab.FormClosed += this.ab_FormClosed;
            ab.Show();
            ab.Refresh();
            this.Enabled = false;
        }

        private void ab_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Enabled = true;
            this.Focus();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((this.curCamera != null) && this.selected.Contains(this.curCamera))
            {
                this.curCamera = null;
            }
            if ((this.curWaypoint != null) && this.selected.Contains(this.curWaypoint))
            {
                this.curWaypoint = null;
            }
            if ((this.curItemSpot != null) && this.selected.Contains(this.curItemSpot))
            {
                this.curItemSpot = null;
            }


            foreach (ResourceSpot spot in this.selected.Clone())
            {
                this.Waypoints.Remove(spot.IdToken);
                this.CameraSpots.Remove(spot.IdToken);
                this.ItemSpots.Remove(spot.IdToken);
                this.selected.Remove(spot);
            }
            this.RefreshAll();
        }

        private void createGridToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.gsf = new GridSizeForm();
            this.gsf.FormClosed += this.gsf_FormClosed;
            this.gsf.Show();
            this.gsf.Refresh();
            this.Enabled = false;
        }

        private void gsf_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Enabled = true;
            this.Focus();

            float cellSize = this.gsf.CellSize / this.scaleFactor;
            int numColumns = (int)(this.editionPanel.SelectedArea.Width / cellSize);
            int numRows = (int)(this.editionPanel.SelectedArea.Height / cellSize);

            for (int i = 0; i < numColumns; i++)
            {
                for (int j = 0; j < numRows; j++)
                {
                    OgrePosition spotVec = new OgrePosition(
                        ((cellSize * i) + (cellSize * 0.5f) + this.editionPanel.SelectedArea.Left +
                        this.minX + 5) * this.scaleFactor, 0,
                        ((cellSize * j) + (cellSize * 0.5f) + this.editionPanel.SelectedArea.Top +
                        this.minY + 5) * this.scaleFactor);

                    this.AddWaypoint(spotVec);
                }
            }
            this.RefreshAll();
        }

        private void selectCamToolStripMenuItem_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            foreach (ToolStripMenuItem item in this.selectCamToolStripMenuItem.DropDownItems)
            {
                item.Checked = false;
            }
            ((ToolStripMenuItem)e.ClickedItem).Checked = true;
            OgreCameraSpot spot = (OgreCameraSpot)this.set.CameraSpots[e.ClickedItem.Text][0];

            this.SetCurCamera(spot);
        }

        private void selectWaypointToolStripMenuItem_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            foreach (ToolStripMenuItem item in this.selectWaypointToolStripMenuItem.DropDownItems)
            {
                item.Checked = false;
            }
            ((ToolStripMenuItem)e.ClickedItem).Checked = true;
            OgreWaypoint spot = (OgreWaypoint)this.set.Waypoints[e.ClickedItem.Text][0];
            this.SetCurWaypoint(spot);
        }

        private void selectItemSpotToolStripMenuItem_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            foreach (ToolStripMenuItem item in this.selectItemSpotToolStripMenuItem.DropDownItems)
            {
                item.Checked = false;
            }
            ((ToolStripMenuItem)e.ClickedItem).Checked = true;
            OgreItemSpot spot = (OgreItemSpot)this.set.ItemSpots[e.ClickedItem.Text][0];
            this.SetCurItemSpot(spot);
        }

        private void deleteSelectedCamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.curCamera != null)
            {
                this.CameraSpots.Remove(this.curCamera);
            }
            this.deleteSelectedCamToolStripMenuItem.Enabled = false;
            this.curCamera = null;

            this.RefreshAll();
        }

        private void deleteSelectedWaypointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.curWaypoint != null)
            {
                this.Waypoints.Remove(this.curWaypoint);
            }
            this.deleteSelectedWaypointToolStripMenuItem.Enabled = false;
            this.curWaypoint = null;

            this.RefreshAll();
        }

        private void deleteSelectedItemSpotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.curItemSpot != null)
            {
                this.ItemSpots.Remove(this.curItemSpot);
            }
            this.deleteSelectedItemSpotToolStripMenuItem.Enabled = false;
            this.curItemSpot = null;
            this.RefreshAll();
        }

        private void editSelectedCamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SpotForm esf = new SpotForm(this.curCamera, this.set.ResourceAsset);
            esf.Closable = true;
            esf.WindowState = FormWindowState.Normal;
            esf.FormClosed += this.esf_FormClosed;
            esf.Show();
            esf.Refresh();
            this.Enabled = false;
        }

        private void editSelectedWaypointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SpotForm esf = new SpotForm(this.curWaypoint, this.set.ResourceAsset);
            esf.Closable = true;
            esf.WindowState = FormWindowState.Normal;
            esf.FormClosed += this.esf_FormClosed;
            esf.Show();
            esf.Refresh();
            this.Enabled = false;
        }

        private void editselectedItemSpotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SpotForm esf = new SpotForm(this.curItemSpot, this.set.ResourceAsset);
            esf.Closable = true;
            esf.WindowState = FormWindowState.Normal;
            esf.FormClosed += this.esf_FormClosed;
            esf.Show();
            esf.Refresh();
            this.Enabled = false;
        }

        private void esf_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Enabled = true;
            this.Focus();

            this.RefreshAll();
        }

        private void deleteAllCamsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ResourceCameraSpot spot in this.CameraSpots.Clone())
            {
                this.CameraSpots.Remove(spot);
            }
            this.curCamera = null;

            this.RefreshAll();
        }

        private void deleteAllWaypointsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ResourceWaypoint spot in this.Waypoints.Clone())
            {
                this.Waypoints.Remove(spot);
            }
            this.curWaypoint = null;

            this.RefreshAll();
        }

        private void deleteAllItemspotsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ResourceItemSpot spot in this.ItemSpots.Clone())
            {
                this.ItemSpots.Remove(spot);
            }
            this.curItemSpot = null;

            this.RefreshAll();
        }

        private OgrePosition getRelativePreviewCameraPosition()
        {
            // obtain absolute camera position
            OgrePosition spotVec = (OgrePosition)OrientGraphicsRealizer.Instance.Camera.AbsolutePosition;
            
            //obtain world scale
            OgreScale setScale = (OgreScale)this.set.ResourceAsset.Scale;

            // scale absolute camera position to get relative position in set 
            // note: the minus for x and z has something to do with the camera's node's orientation and the orientation
            // of the set, it is possible that this will not work for sets with directions different from (0,0,1) 
            spotVec.X = -spotVec.X / setScale.X;
            spotVec.Y = spotVec.Y / setScale.Y;
            spotVec.Z = -spotVec.Z / setScale.Z;
            return spotVec;
        }

        private void btnAddWayPointAtCamPos_Click(object sender, EventArgs e)
        {
            this.AddWaypoint(getRelativePreviewCameraPosition());
            this.RefreshAll();
        }

        private void btnAddCamAtCamPos_Click(object sender, EventArgs e)
        {
            AddCameraSpot(getRelativePreviewCameraPosition());
            this.RefreshAll();           
        }

        private void btnAddItemSpotAtCamPos_Click(object sender, EventArgs e)
        {
            this.AddItemSpot(getRelativePreviewCameraPosition(), textBoxItemSpotName.Text);
            this.RefreshAll();
        }

        #endregion

        #region Private Methods

        private void SetCurCamera(OgreCameraSpot camSpot)
        {
            if (this.set == null) return;

            OrientGraphicsRealizer.Instance.Camera.CurrentSpot = camSpot;
            OrientGraphicsRealizer.Instance.Camera.LookAt(this.character.AbsolutePosition);

            if (this.curCamera != null) this.selected.Remove(this.curCamera);
            this.curCamera = camSpot.ResourceSpot;
            if (!this.selected.Contains(this.curCamera)) this.selected.Add(this.curCamera);

            this.deleteSelectedCamToolStripMenuItem.Enabled = true;
            this.editSelectedCamToolStripMenuItem.Enabled = true;
            this.editionPanel.Invalidate();
        }

        private void SetCurWaypoint(OgreWaypoint waypoint)
        {
            this.character.Visible = true;
            this.character.Place(null, this.set, waypoint.ResourceSpot.IdToken, false);

            OrientGraphicsRealizer.Instance.Camera.LookAt(this.character.AbsolutePosition);

            if (this.curWaypoint != null) this.selected.Remove(this.curWaypoint);
            this.curWaypoint = waypoint.ResourceSpot;
            if (!this.selected.Contains(this.curWaypoint)) this.selected.Add(this.curWaypoint);

            this.deleteSelectedWaypointToolStripMenuItem.Enabled = true;
            this.editSelectedWaypointToolStripMenuItem.Enabled = true;
            this.editionPanel.Invalidate();
        }

        private void SetCurItemSpot(OgreItemSpot itemSpot)
        {
            OrientGraphicsRealizer.Instance.Camera.LookAt(itemSpot.AbsolutePosition);

            if (this.curItemSpot != null) this.selected.Remove(this.curItemSpot);
            this.curItemSpot = itemSpot.ResourceSpot;
            if (!this.selected.Contains(this.curItemSpot)) this.selected.Add(this.curItemSpot);

            this.deleteSelectedItemSpotToolStripMenuItem.Enabled = true;
            this.editselectedItemSpotToolStripMenuItem.Enabled = true;
            this.editionPanel.Invalidate();
        }

        private void setControl1_TokenChanged(object sender, EventArgs e)
        {
            if (this.set.ResourceAsset.FileName != this.oldOSMname)
            {
                this.CreateNewSet(this.set.ResourceAsset.IdToken);
            }else
            {
                this.RefreshAll();
            }
        }

        private void RefreshAll()
        {
            this.lblCam.Text = this.CameraSpots.Count.ToString();
            this.lblWaypoint.Text = this.Waypoints.Count.ToString();
            this.lblItem.Text = this.ItemSpots.Count.ToString();

            if (this.set == null) return;

            this.set.Destroy();
            this.set.Reset();
            this.set.Visible = true;
            this.set.ShowAllDebuggingSpots();

            OrientGraphicsRealizer.Instance.Camera.Target = this.set;

            if (this.set.CameraSpots.Count > 0)
            {
                this.cameraToolStripMenuItem.Enabled = true;

                ArrayList spotIDs = new ArrayList();
                foreach (OgreCameraSpot spot in this.set.CameraSpots)
                {
                    spotIDs.Add(spot.ResourceSpot.IdToken);
                }
                spotIDs.Sort();
                this.selectCamToolStripMenuItem.DropDownItems.Clear();
                foreach (string spotID in spotIDs)
                {
                    this.selectCamToolStripMenuItem.DropDownItems.Add(spotID);
                }
            }
            else
            {
                this.cameraToolStripMenuItem.Enabled = false;
            }

            if (this.set.Waypoints.Count > 0)
            {
                this.waypointsToolStripMenuItem.Enabled = true;

                ArrayList spotIDs = new ArrayList();
                foreach (OgreWaypoint spot in this.set.Waypoints)
                {
                    spotIDs.Add(spot.ResourceSpot.IdToken);
                }
                spotIDs.Sort();
                this.selectWaypointToolStripMenuItem.DropDownItems.Clear();
                foreach (string spotID in spotIDs)
                {
                    this.selectWaypointToolStripMenuItem.DropDownItems.Add(spotID);
                }
            }
            else
            {
                this.waypointsToolStripMenuItem.Enabled = false;
            }

            if (this.set.ItemSpots.Count > 0)
            {
                this.itemspotsToolStripMenuItem.Enabled = true;

                ArrayList spotIDs = new ArrayList(); 
                foreach (OgreItemSpot spot in this.set.ItemSpots)
                {
                    spotIDs.Add(spot.ResourceSpot.IdToken);
                }
                spotIDs.Sort();
                this.selectItemSpotToolStripMenuItem.DropDownItems.Clear();
                foreach (string spotID in spotIDs)
                {
                    this.selectItemSpotToolStripMenuItem.DropDownItems.Add(spotID);
                }
            }
            else
            {
                this.itemspotsToolStripMenuItem.Enabled = false;
            }

            if(this.curCamera != null)
            {
                OgreCameraSpot spot = (OgreCameraSpot)this.set.CameraSpots[this.curCamera.IdToken][0];
                OrientGraphicsRealizer.Instance.Camera.CurrentSpot = spot;
            }

            if(this.curWaypoint != null)
            {
                OrientGraphicsRealizer.Instance.Camera.Target = this.character;
                this.character.Visible = true;
                this.character.Place(null, this.set, this.curWaypoint.IdToken, false);
            }
            else
            {
                this.character.Visible = false;
            }

            this.editionPanel.Invalidate();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (!this.saved)
            {
                DialogResult result = MessageBox.Show(
                    "Set file not saved!\nDo you want to save changes?", "SetEditor",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (result == DialogResult.Cancel)
                {
                    e.Cancel = true;
                    return;
                }
                else if (result == DialogResult.Yes)
                {
                    this.saveAsToolStripMenuItem_Click(this, EventArgs.Empty);
                }
            }

            OrientGraphicsRealizer.Instance.Destroy();

            base.OnClosing(e);
        }

        private void CalcScaleFactor()
        {
            minX = float.MaxValue;
            minY = float.MaxValue;

            float maxX = float.MinValue;
            float maxY = float.MinValue;

            //calculate scale factor
            foreach (Entity en in this.set.OSMLoader.Entities.Values)
            {
                AxisAlignedBox bb = en.GetWorldBoundingBox();

                if (!this.AcceptEntity(bb))
                {
                    continue;
                }

                float px = bb.Minimum.x;
                if (px < minX) minX = px;
                float py = bb.Minimum.z;
                if (py < minY) minY = py;
                px = bb.Maximum.x;
                if (px > maxX) maxX = px;
                py = bb.Maximum.z;
                if (py > maxY) maxY = py;
            }
            this.width = maxX - minX;
            this.height = maxY - minY;

            if (width > height)
            {
                this.scaleFactor = width / (this.editionPanel.Width - 10.0f);
            }
            else
            {
                this.scaleFactor = height / (this.editionPanel.Height - 10.0f);
            }

            if (this.scaleFactor < 0.1f) this.scaleFactor = 0.1f;
        }

        private void CalcRecs()
        {
            if (this.set == null) return;

            this.recs.Clear();

            minX = float.MaxValue;
            minY = float.MaxValue;

            float maxX = float.MinValue;
            float maxY = float.MinValue;

            foreach (Entity en in this.set.OSMLoader.Entities.Values)
            {
                AxisAlignedBox bb = en.GetWorldBoundingBox();

                if (!this.AcceptEntity(bb))
                {
                    continue;
                }

                float px, py, bbMaximumX, bbMinimumX, bbMaximumZ, bbMinimumZ;

                if (bb.Maximum.x > bb.Minimum.x)
                {
                    bbMaximumX = bb.Maximum.x;
                    bbMinimumX = bb.Minimum.x;
                }
                else
                {
                    bbMaximumX = bb.Minimum.x;
                    bbMinimumX = bb.Maximum.x;
                }
                if (bb.Maximum.z > bb.Minimum.z)
                {
                    bbMaximumZ = bb.Maximum.z;
                    bbMinimumZ = bb.Minimum.z;
                }
                else
                {
                    bbMaximumZ = bb.Minimum.z;
                    bbMinimumZ = bb.Maximum.z;
                }

                px = bbMaximumX / this.scaleFactor;
                if (px > maxX) maxX = px;
                py = bbMaximumZ / this.scaleFactor;
                if (py > maxY) maxY = py;

                px = bbMinimumX / this.scaleFactor;
                if (px < minX) minX = px;
                py = bbMinimumZ / this.scaleFactor;
                if (py < minY) minY = py;

                Point ulc = new Point((int)px, (int)py);
                int recWidth = (int)(
                    (bbMaximumX / this.scaleFactor) -
                    (bbMinimumX / this.scaleFactor));
                int recHeight = (int)(
                    (bbMaximumZ / this.scaleFactor) -
                    (bbMinimumZ / this.scaleFactor));

                recs.Add(new Rectangle(
                    ulc, new Size(recWidth, recHeight)));
            }

            this.width = Math.Abs(maxX - minX);
            this.height = Math.Abs(maxY - minY);

            this.minX -= 5;
            this.minY -= 5;

            this.editionPanel.Height = (int)this.height + 10;
            this.editionPanel.Width = (int)this.width + 10;

            this.editionPanel.Invalidate();
        }

        private bool AcceptEntity(AxisAlignedBox bb)
        {
            Plane p1 = new Plane(Vector3.UNIT_Y, Vector3.ZERO);
            return bb.Intersects(p1) && (bb.Maximum.y <= 300);
        }

        private void AddWaypoint(OgrePosition wpPosition)
        {
            //new waypoint
            int count = 0;
            while (this.Waypoints.Contains("waypoint" + count))
            {
                count++;
            }
            ResourceWaypoint spot = new ResourceWaypoint("waypoint" + count,
                OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
            spot.RelativePosition = wpPosition;
            spot.Tags.Add("waypoint" + count);
            spot.Tags.Add("waypoint");

            this.Waypoints.Add(spot);
        }

        private void AddCameraSpot(OgrePosition camPosition)
        {
            //new camera point
            int count = 0;
            while (this.CameraSpots.Contains("camera" + count))
            {
                count++;
            }
            ResourceCameraSpot spot = new ResourceCameraSpot("camera" + count, 
                OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
            spot.RelativePosition = camPosition;
            spot.Tags.Add("camera" + count);
            spot.Tags.Add("camera");

            this.CameraSpots.Add(spot);
        }

        private void AddItemSpot(OgrePosition spotPosition, string spotName)
        {
            ResourceItemSpot spot = null;
            
            //new item spot
            if (this.ItemSpots.Contains(spotName)) // there already is a spot with this name so add a number at the end
            {
                int count = 0;
                while (this.ItemSpots.Contains(spotName + count))
                {
                    count++;
                }
                spot = new ResourceItemSpot(spotName + count,
                    OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
            }
            else // no spot with the name existing yet, name can be used
            {
                spot = new ResourceItemSpot(spotName,
                    OrientGraphicsRealizer.Instance.ResourcesManager.ResourcesFactory);
            }
            spot.RelativePosition = spotPosition;
            this.ItemSpots.Add(spot);
        }

        private void CreateNewSet(string idToken)
        {
            if (this.set != null) OrientGraphicsRealizer.Instance.RemoveSet(this.set);

            this.set = (OgreSet)OrientGraphicsRealizer.Instance.LoadSet(idToken);
            this.set.Visible = true;
            this.oldOSMname = this.set.ResourceAsset.FileName;

            this.setControl1.TokenObject = this.set.ResourceAsset;        
            this.curWaypoint = null;
            this.curCamera = null;
            this.curItemSpot = null;

            OrientGraphicsRealizer.Instance.Update();
            this.RefreshAll();
            this.CalcScaleFactor();
            this.CalcRecs();

            if(this.set.CameraSpots.Count > 0)
                this.SetCurCamera((OgreCameraSpot)this.set.CameraSpots[0]);
            if(this.set.Waypoints.Count > 0)
                this.SetCurWaypoint((OgreWaypoint)this.set.Waypoints[0]);
            if (this.set.ItemSpots.Count > 0)
                this.SetCurItemSpot((OgreItemSpot)this.set.ItemSpots[0]);

            this.saved = false;
            this.globalPanel.Enabled = true;
        }

        #endregion


    }
}