/* ****************************************************
 * Name: PathsForm.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/08/19 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Windows.Forms;

namespace SetEditor
{
    public partial class PathsForm : Form
    {
        public PathsForm(string text)
        {
            InitializeComponent();

            this.textBox1.Text = text.Replace("\n", "\r\n");
        }
    }
}