/* ****************************************************
 * Name: RectanglePanel.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading;
using System.Drawing;

namespace SetEditor
{
	/// <summary>
	/// RectanglePicBox: draws a rectangle over the image
	/// </summary>
	public class RectanglePanel : Panel
	{
		#region Fields
		protected Rectangle selectedArea = 
			new Rectangle(new Point(0, 0), new Size(0, 0));

        private int x1, y1;     //auxiliary

        protected Color rectangleColor = Color.Black;
		#endregion 

		#region Properties
		[Browsable(false)]
		public Rectangle SelectedArea
		{
			get{
				Monitor.Enter(this);
				Rectangle r = this.selectedArea;
				Monitor.Exit(this);
				return r;
			}
			set{
				Monitor.Enter(this);
				this.selectedArea = value;
				Monitor.Exit(this);
			}
		}

        [Browsable(true)]
        public Color RectangleColor
        {
            get
            {
                Monitor.Enter(this);
                Color c = this.rectangleColor;
                Monitor.Exit(this);
                return c;
            }
            set
            {
                Monitor.Enter(this);
                this.rectangleColor = value;
                Monitor.Exit(this);
            }
        }
		#endregion

		#region Public Events
		
		public event EventHandler SelectedAreaChanged;

		#endregion 

		#region Override Methods
        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
		{
			base.OnPaint (e);
			Graphics g = e.Graphics;

			Monitor.Enter(this);
			g.DrawRectangle(new Pen(this.rectangleColor, 3), this.selectedArea);
			Monitor.Exit(this);
		}

		protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
		{
			base.OnMouseDown (e);

            if (e.Button != MouseButtons.Middle) return;

			Monitor.Enter(this);
			this.selectedArea.Location = new Point(e.X, e.Y);
			this.selectedArea.Size = new Size(0, 0);
			this.x1 = e.X;
			this.y1 = e.Y;

			if(this.SelectedAreaChanged != null)
			{
				this.SelectedAreaChanged(this, EventArgs.Empty);
			}

			this.Invalidate();
			Monitor.Exit(this);
		}

        protected override void OnMouseMove(System.Windows.Forms.MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (e.Button != MouseButtons.Middle) return;

            Monitor.Enter(this);
            int startX = System.Math.Max(System.Math.Min(x1, e.X), 0);
            int startY = System.Math.Max(System.Math.Min(y1, e.Y), 0);

            int width = System.Math.Min(
                System.Math.Abs(System.Math.Max(e.X, 0) - x1),
                this.Width - startX);

            int height = System.Math.Min(
                System.Math.Abs(System.Math.Max(e.Y, 0) - y1),
                this.Height - startY);

            this.selectedArea.Size = new Size(width, height);
            this.selectedArea.Location = new Point(startX, startY);

            if (this.SelectedAreaChanged != null)
            {
                this.SelectedAreaChanged(this, EventArgs.Empty);
            }

            this.Invalidate();
            Monitor.Exit(this);
        }

		protected override void OnMouseEnter(EventArgs e)
		{
			base.OnMouseEnter (e);

			this.Cursor = Cursors.Cross;
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			base.OnMouseLeave (e);

			this.Cursor = Cursors.Default;
		}

		#endregion 
	}
}
