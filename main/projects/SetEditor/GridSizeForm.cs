/* ****************************************************
 * Name: GridSizeForm.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.ComponentModel;
using System.Windows.Forms;

namespace SetEditor
{
    public partial class GridSizeForm : Form
    {
        [Browsable(true)]
        public float CellSize
        {
            get { return (float)this.numericUpDown1.Value; }
            set { numericUpDown1.Value = (decimal)value; }
        }

        public GridSizeForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}