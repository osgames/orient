/* ****************************************************
 * Name: GetUp.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/09/17
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using PAGE.Generic.Domain.Spots;

namespace ION.Realizer.Orient.Actions.Body
{
    public class GetUp : Animate
    {
        protected override void OnStart(Context context)
        {
            if (!this.Body.IsSeated)
            {
                this.End(); //not sitted, fail
                return;
            }

            base.OnStart(context); //animates body

            //animates get up movement
            GenericWaypoint rightPoint =
                this.Body.SeatedOnItem.GraphicalAsset.Waypoints[SitDown.SIT_RIGHT_TAG][0];
            float transitionTime = this.Body.GraphicalAsset.CurrentAnimationLenght;
            if (rightPoint != null)
            {
                this.Body.GraphicalAsset.ChangePosition(
                    rightPoint.AbsolutePosition, transitionTime);
            }
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);

            if(!this.Body.IsSeated) return;

            //character gets up, becomes unsitted and is placed in left side spot
            this.Body.GraphicalAsset.Place(
                this, this.Body.SeatedOnItem.GraphicalAsset, SitDown.SIT_RIGHT_TAG);
            this.Body.SeatedOnItem.ChosenForSitting = false;
            this.Body.SeatedOnItem = null;

            //sets body property
            if (this.Body.HasProperty(Entities.Body.PROPERTY_IS_SEATED_STR))
                this.Body.GetProperty<Property<string>>(
                    Entities.Body.PROPERTY_IS_SEATED_STR).Value = Constants.FATIMA_FALSE_VALUE;
        }
    }
}