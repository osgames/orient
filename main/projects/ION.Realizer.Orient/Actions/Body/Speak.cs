/* ****************************************************
 * Name: Speak.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections.Generic;
using ION.Core;
using PAGE.Orient.Domain.Assets;

namespace ION.Realizer.Orient.Actions.Body
{
    public class Speak : Animate
    {
        #region Constants

        public const string PARAM_UTTERANCE_STR = "utterance";
        public const string PARAM_TARGET_STR = "target";
        public const string TARGET_USER_STR = "user";

        public const float SUBTITLES_SHOW_SECONDS = 1;

        #endregion

        #region Fields

        protected Queue<string> Utterances = new Queue<string>();
        protected bool checkNewUtterance;
        protected float subtitlesSecondsLeft;
        protected DateTime timerStart;

        #endregion

        #region ION Methods

        protected override void OnStart(Context context)
        {
            //checks arguments
            string paragraph = context.Get<string>(PARAM_UTTERANCE_STR);
            string targetID = context.Contains(PARAM_TARGET_STR) ?
                context.Get<string>(PARAM_TARGET_STR) : null;
            /*
            if ((targetID != null) && (targetID.ToLower() == TARGET_USER_STR))
            {
                this.Body.GraphicalAsset.SaySpeech(this, paragraph);
                return;
            }
             */ 

            //gets utterances
            this.Utterances = new Queue<string>(this.ParagraphSplitter(paragraph));
            if (this.Utterances.Count > 0)
            {
                //makes the character say the first utterance
                this.checkNewUtterance = true;

                //do not receive End() from animation
                context[PARAM_SEND_END_STR] = false;
                base.OnStart(context);
                return;
            }

            this.End();
        }

        protected override void OnFail(Context context)
        {
            //hides subtitles
            this.Body.GraphicalAsset.HideSubtitles();
            base.OnFail(context);
        }

        protected override void OnStep(Context context)
        {
            //calculate time left
            if (this.subtitlesSecondsLeft > 0)
            {
                DateTime now = DateTime.Now;
                this.subtitlesSecondsLeft -= (float) now.Subtract(this.timerStart).TotalSeconds;
                if (this.subtitlesSecondsLeft == 0) this.subtitlesSecondsLeft = -1;
                this.timerStart = now;
            }

            //if speech ended, check for new utterance
            if (this.subtitlesSecondsLeft < 0) 
            {
                this.subtitlesSecondsLeft = 0;
                this.checkNewUtterance = true;
            }

            //if we don't need to check for new utterance return
            if (!this.checkNewUtterance) return;
            this.checkNewUtterance = false;

            // says next utterance if there is more
            if (this.Utterances.Count > 0)
            {
                //say next utterance
                string dequeuedUtterance = this.Utterances.Dequeue();
                this.Body.GraphicalAsset.SaySpeech(this, dequeuedUtterance);
                this.Body.GraphicalAsset.ShowSubtitles(dequeuedUtterance);
            }
            else
            {
                //no more utterances, stops talking and hides the subtitles
                this.Body.GraphicalAsset.HideSubtitles();
                this.End();
            }
        }

        #endregion

        #region ActionCallbackMethods Methods

        public override void SendSuccess()
        {
            this.SetSubtitleTimer();
        }

        public override void SendFailure()
        {
            this.SetSubtitleTimer();
        }

        #endregion

        #region Protected Methods

        protected void SetSubtitleTimer()
        {
            //if audio time has passed, put natural face...
            this.Body.GraphicalAsset.StopTalking();
            this.Body.GraphicalAsset.ChangeFace(OrientCharacter.NATURAL_EXPRESSION, 1);

            //...and sets timer for subtitles
            this.subtitlesSecondsLeft = SUBTITLES_SHOW_SECONDS;
            this.timerStart = DateTime.Now;
        }

        protected LinkedList<string> ParagraphSplitter(string paragraph)
        {
            string sentence;
            LinkedList<string> sentences = new LinkedList<string>();
            int pos = 0;
            int lastPos = 0;
            while (pos < paragraph.Length)
            {
                switch (paragraph[pos])
                {
                    case '!':
                    case '?':
                        sentence = new string(paragraph.ToCharArray(lastPos, pos - lastPos + 1));
                        sentences.AddLast(sentence);
                        pos++;
                        lastPos = pos + 1; // skip space after
                        break;

                    case '.':
                        if ((pos + 1 < paragraph.Length) && (paragraph[pos + 1] == '.'))
                        {
                            pos++;
                        }
                        else
                        {
                            sentence = new string(paragraph.ToCharArray(lastPos, pos - lastPos + 1));
                            sentences.AddLast(sentence);
                            pos++;
                            lastPos = pos + 1; // skip space after
                        }
                        break;

                    default:
                        pos++;
                        break;
                }
            }
            if (lastPos < pos)
            {
                sentence = new string(paragraph.ToCharArray(lastPos, pos - lastPos));
                sentences.AddLast(sentence);
            }

            return sentences;
        }

        #endregion
    }
}