/* ****************************************************
 * Name: Gesture.cs
 * Code: Michael Kriegel - michael@macs.hw.ac.uk
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/09/05
 * 
 * Heriot-Watt University
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using ION.Core;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    internal class Gesture : AttendTo
    {
        #region Constants

        public const string ARG_GESTURE_ACTION_ID_STR = "gesture-action-id";

        #endregion

        #region Fields

        protected string gestureActionID;

        #endregion

        #region ION Methods

        protected override void OnCreate(Arguments arguments)
        {
            //checks arguments
            this.gestureActionID = arguments.ContainsKey(ARG_GESTURE_ACTION_ID_STR) ? 
                arguments.Get<string>(ARG_GESTURE_ACTION_ID_STR) : null;
            base.OnCreate(arguments);
        }

        protected override void OnStart(Context context)
        {
            //1 - character attends to target, both turn to each other,
            // character stands at end
            base.OnStart(context);

            //2 - character performs the greeting gesture while target waits 
            // for it to end
            if(this.gestureActionID != null)
                this.AddNext(this.Character.GetAction(this.gestureActionID), null);
        } 

        #endregion
    }
}
