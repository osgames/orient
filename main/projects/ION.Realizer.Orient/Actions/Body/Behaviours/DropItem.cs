/* ****************************************************
 * Name: DropItem.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/08/29
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Realizer.Orient.Entities;
using PAGE.OGRE.Math;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    internal class DropItem : Stand
    {
        public const string PARAM_DROP_TARGET_ID_STR = "target";
        public const string PARAM_DROP_SPOT_TAG_STR = "spot";
        public const string PARAM_ITEM_ID_STR = "item";

        protected const string DROP_SPOT_TAG = "drop-spot";

        protected override void OnStart(Context context)
        {
            //checks item id
            string itemID = context.Contains(PARAM_ITEM_ID_STR) ? 
                context.Get<string>(PARAM_ITEM_ID_STR) : null;
            if ((itemID == null) || !Universe.Instance.HasEntity<Entities.Item>(itemID))
            {
                this.Fail(); //no item, fail
                return;
            }

            //checks if item was picked
            Entities.Item item = Universe.Instance.GetEntity<Entities.Item>(itemID);
            if (!this.Character.PickedItems.Contains(item))
            {
                this.Fail(); //nothing to do
                return;
            }

            //sets parameters for drop action
            string actionName = Entities.Body.ACTION_DROP_FLOOR_STR;
            Context dropContext = new Context();
            dropContext[Drop.PARAM_ITEM_ID_STR] = itemID;

            //checks target drop site
            string dropTargetID = context.Contains(PARAM_DROP_TARGET_ID_STR) ? 
                context.Get<string>(PARAM_DROP_TARGET_ID_STR) : null;
            if ((dropTargetID != null) && Universe.Instance.HasEntity<Asset>(dropTargetID))
            {
                //1 - character walks to the target drop site
                Context walkContext = new Context();
                walkContext[Move.PARAM_TO_TARGET_STR] = dropTargetID;
                walkContext[Move.PARAM_SPOT_TAG_STR] = DROP_SPOT_TAG;
                this.AddNext(this.Character.GetAction(Entities.Body.ACTION_WALK_TO_STR), walkContext);

                //checks drop location height
                Asset dropTarget = Universe.Instance.GetEntity<Asset>(dropTargetID);
                if ((dropTarget is Character) ||
                    ((dropTarget is Entities.Item) &&
                    (((OgreDistance)((Entities.Item)dropTarget).GraphicalAsset.Height) >=
                    PickItem.FLOOR_HEIGHT_THRESHOLD)))
                {
                    actionName = Entities.Body.ACTION_DROP_TABLE_STR;
                }

                //2 - character drops item 
                this.AddNext(this.Character.GetAction(actionName), dropContext);

                // character turns to drop target
                Context turnContext = new Context();
                turnContext[Turn.PARAM_TO_TARGET_STR] = dropTargetID;
                this.AddSimultaneous(
                    this.Character.GetAction(Entities.Body.ACTION_TURN_TO_STR), turnContext);

                // item is placed in correct spot
                Context placeContext = new Context();
                string spotTag = context.Contains(PARAM_DROP_SPOT_TAG_STR) ? 
                    context.Get<string>(PARAM_DROP_SPOT_TAG_STR) : null;
                if (spotTag != null)
                    placeContext[Item.Place.PARAM_SPOT_TAG_STR] = spotTag;
                placeContext[Item.Place.PARAM_TARGET_ASSET_ID_STR] = dropTargetID;
                placeContext[Item.Place.PARAM_FADE_STR] = Item.Place.DO_NOT_FADE_STR;
                this.AddSimultaneous(item.GetAction(Entities.Item.ACTION_PLACE_STR), placeContext);
            }
            else
            {
                //1 - character drops item on the floor
                this.AddNext(this.Character.GetAction(actionName), dropContext);
                item.GraphicalAsset.AbsolutePosition =
                    this.Character.GraphicalAsset.AbsolutePosition;
            }

            //3 - character stands at end
            base.OnStart(context);
        }
    }
}
