/* ****************************************************
 * Name: PickItem.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/08/29
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using PAGE.OGRE.Math;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    internal class PickItem : Stand
    {
        public const string PARAM_ITEM_ID_STR = "target";
        internal const float FLOOR_HEIGHT_THRESHOLD = 4;
        internal const float TREE_HEIGHT_THRESHOLD = 24;

        protected const string PICK_SPOT_TAG = "pick-spot";

        protected override void OnStart(Context context)
        {
            string itemID = context.Contains(PARAM_ITEM_ID_STR) ? 
                context.Get<string>(PARAM_ITEM_ID_STR) : null;
            if ((itemID == null) || !Universe.Instance.HasEntity<Entities.Item>(itemID))
            {
                this.Fail(); //no target, fail
                return;
            }

            //check picked item
            Entities.Item item = (Entities.Item) Universe.Instance.GetEntity(itemID);
            if (item.Picked)
            {
                if (this.Character.PickedItems.Contains(item))
                    //if character already has the item, end
                    this.End();
                else
                    //if item was picked by someone else
                    this.Fail();
                return;
            }

            //somebody else already wants to pick this
            if (item.ChosenForPicking)
            {
                this.Fail(); //fail
            }
            else
            {
                // put a claim on picking that item
                item.ChosenForPicking = true;
            }

            //gets item height and determines correct pick action
            float itemHeight = ((OgrePosition) item.GraphicalAsset.AbsolutePosition).Y;
            string actionName = Entities.Body.ACTION_PICK_FLOOR_STR;
            if (itemHeight >= FLOOR_HEIGHT_THRESHOLD)
                actionName = Entities.Body.ACTION_PICK_TABLE_STR;
            if (itemHeight >= TREE_HEIGHT_THRESHOLD)
                actionName = Entities.Body.ACTION_PICK_FROM_TREE_STR;

            //1 - character walks to the target item
            Context walkContext = new Context();
            walkContext[Move.PARAM_SPOT_TAG_STR] = PICK_SPOT_TAG;
            walkContext[Move.PARAM_TO_TARGET_STR] = itemID;
            this.AddNext(this.Character.GetAction(Entities.Body.ACTION_WALK_TO_STR), walkContext);

            //2 - character turns to item
            Context turnContext = new Context();
            turnContext[Turn.PARAM_TO_TARGET_STR] = itemID;
            this.AddNext(this.Character.GetAction(Entities.Body.ACTION_TURN_TO_STR), turnContext);

            // and picks it
            Context pickContext = new Context();
            pickContext[Pick.PARAM_ITEM_ID_STR] = itemID;
            this.AddSimultaneous(this.Character.GetAction(actionName), pickContext);

            //3 - character stands at end
            base.OnStart(context);
        }
    }
}
