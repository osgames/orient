/* ****************************************************
 * Name: Toast.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/09/25
 * 
 * INESC-ID Gaips
 * 
 *****************************************************/

using ION.Core;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    internal class Toast : Gesture
    {
        protected override void OnStart(Context context)
        {
            this.walkTo = false;

            // 1 - get up from chair if seated
            if (this.Character.IsSeated)
                this.AddNext(this.Character.GetAction(
                    Entities.Body.ACTION_GET_UP_RIGHT_STR), null);

            // 2 - perform normal greeting
            base.OnStart(context);
        }
    }
}