/* ****************************************************
 * Name: Behaviour.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Collections.Generic;
using ION.Core;
using ION.Core.Events;
using ION.Realizer.Orient.Entities;
using FAtiMA.Actions;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    public abstract class Behaviour : Action,
                                      IEventCallback<ActionEnded>, IEventCallback<ActionFailed>
    {
        #region ContextualizedAction Struct

        protected struct ContextualizedAction
        {
            public Action action;
            public Context context;

            public ContextualizedAction(Action action, Context context)
            {
                this.action = action;
                this.context = context;
            }
        }

        #endregion

        #region Constants

        public const string ARG_SIMULTANEOUS_STR = "simultaneous";

        #endregion

        #region Fields

        protected LinkedList<LinkedList<ContextualizedAction>> actions =
            new LinkedList<LinkedList<ContextualizedAction>>();

        protected LinkedList<ContextualizedAction> atEnd = new LinkedList<ContextualizedAction>();
        protected LinkedList<EventListener> listeners = new LinkedList<EventListener>();

        //used to prevent simultaneous behaviors of the same name (name - Behavior running)
        private static readonly Dictionary<string, Behaviour> InProgress
            = new Dictionary<string, Behaviour>();

        protected bool allowSimultaneousBehavior;

        #endregion

        #region Properties

        protected Character Character
        {
            get { return this.Parent as Character; }
        }

        protected virtual bool Running
        {
            get
            {
                lock (InProgress)
                {
                    if (InProgress.ContainsKey(this.Name))
                        return InProgress[this.Name] == this;
                    return false;
                }
            }
            set
            {
                lock (InProgress)
                {
                    if (value) InProgress[this.Name] = this;
                    else if (this.Running) InProgress.Remove(this.Name);
                }
            }
        }

        protected virtual bool IsSameBehaviourRunning
        {
            get
            {
                lock (InProgress)
                {
                    return InProgress.ContainsKey(this.Name);
                }
            }
        }

        #endregion

        #region Actions Control Methods

        protected void AddNext(Action action, Context context)
        {
            LinkedList<ContextualizedAction> list = new LinkedList<ContextualizedAction>();
            list.AddLast(new ContextualizedAction(action, context));
            this.actions.AddLast(list);
        }

        protected void AddSimultaneous(Action action, Context context)
        {
            if (this.actions.Count == 0)
            {
                this.AddNext(action, context);
            }
            else
            {
                this.actions.Last.Value.AddLast(
                    new ContextualizedAction(action, context));
            }
        }

        protected void AddAtEnd(Action action, Context context)
        {
            this.atEnd.AddLast(new ContextualizedAction(action, context));
        }

        #endregion

        #region ION Methods

        protected override void OnCreate(Arguments arguments)
        {
            Util.Util.LogMessage(this.Character, this, "behaviour CREATED");

            //checks arguments
            this.allowSimultaneousBehavior =
                arguments.ContainsKey(ARG_SIMULTANEOUS_STR) ? 
                arguments.Get<bool>(ARG_SIMULTANEOUS_STR) : false;
        }

        protected override void OnDestroy()
        {
            Util.Util.LogMessage(this.Character, this, "behaviour DESTROYED");
        }

        protected override void OnStart(Context context)
        {
            Util.Util.LogMessage(this.Character, this, "behaviour STARTED");
            if (this.actions.Count > 0)
            {
                // start first batch of actions
                foreach (ContextualizedAction ca in this.actions.First.Value)
                {
                    Action action = ca.action;
                    this.listeners.AddLast(action.CreateEventListener<ActionEnded>(this));
                    this.listeners.AddLast(action.CreateEventListener<ActionFailed>(this));

                    Context actionContext = ca.context ?? context;
                    action.Start(actionContext);
                }
            }
            //updates effects of this behaviour
            //AgentActionEffectUpdater.Instance.UpdateActionEffectsBehaviour(this, context);
        }

        protected override void OnEnd(Context context)
        {
            Util.Util.LogMessage(this.Character, this, "behaviour ENDED");

            //updates effects of this behaviour
            AgentActionEffectUpdater.Instance.UpdateActionEffectsBehaviour(this, context);

            // destroy observers
            foreach (EventListener listener in this.listeners)
            {
                listener.Destroy();
            }
            this.listeners.Clear();

            if (this.actions.Count > 0)
            {
                // ends all the actions still running
                foreach (ContextualizedAction ca in this.actions.First.Value)
                {
                    ca.action.End();
                }

                this.actions.Clear();
            }

            //starts "at end" actions
            if (this.atEnd.Count > 0)
            {
                foreach (ContextualizedAction ca in this.atEnd)
                {
                    Action action = ca.action;
                    Context actionContext = ca.context ?? context;
                    action.Start(actionContext);
                }
                this.atEnd.Clear();
            }
            
            //allow character and others to behave
            this.Running = false;
        }

        protected override void OnFail(Context context)
        {
            Util.Util.LogMessage(this.Character, this, "behaviour FAILED");

            // destroy observers
            foreach (EventListener listener in this.listeners)
            {
                listener.Destroy();
            }
            this.listeners.Clear();

            if (this.actions.Count > 0)
            {
                // fail all the actions still running
                foreach (ContextualizedAction ca in this.actions.First.Value)
                {
                    ca.action.Fail();
                }

                this.actions.Clear();
            }

            foreach (ContextualizedAction ca in this.atEnd)
            {
                Action action = ca.action;
                Context actionContext = ca.context ?? context;
                action.Start(actionContext);
            }

            //allow character and others to behave
            this.Running = false;
        }

        protected override void OnStep(Context context)
        {
        }

        #endregion

        #region Actions Event Handlers

        public virtual void Invoke(ActionEnded evt)
        {
            if (this.actions.Count > 0)
            {
                // remove the action which just ended
                foreach (ContextualizedAction ca in 
                    new LinkedList<ContextualizedAction>(this.actions.First.Value))
                {
                    if (!ca.action.Equals(evt.Action)) continue;

                    this.actions.First.Value.Remove(ca);
                    break;
                }

                if (this.actions.First.Value.Count == 0)
                {
                    // destroy observers
                    foreach (EventListener listener in this.listeners)
                    {
                        listener.Destroy();
                    }
                    this.listeners.Clear();

                    // remove first batch of actions 
                    this.actions.RemoveFirst();

                    if (this.actions.Count > 0)
                    {
                        // start next batch of actions
                        foreach (ContextualizedAction ca in this.actions.First.Value)
                        {
                            Action action = ca.action;
                            Context actionContext = ca.context ?? evt.Context;

                            this.listeners.AddLast(action.CreateEventListener<ActionEnded>(this));
                            this.listeners.AddLast(action.CreateEventListener<ActionFailed>(this));
                            action.Start(actionContext);
                        }
                    }
                    else
                    {
                        // end sequence of actions
                        this.End();
                    }
                }
            }
        }

        public virtual void Invoke(ActionFailed evt)
        {
            this.Fail();
            //this.End();
        }

        #endregion

        #region Protected Methods

        protected virtual void Clean()
        {
            // destroy observers and clears actions
            foreach (EventListener listener in this.listeners)
                listener.Destroy();
            this.listeners.Clear();
            this.actions.Clear();
        }

        #endregion
    }
}