/* ****************************************************
 * Name: WalkToPlace.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/05/21
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    internal class WalkTo : Behaviour
    {
        public const string PARAM_TARGET_SPOT_TAG_STR = "target";

        protected override void OnStart(Context context)
        {
            //checks arguments
            string placeTarget = context.Get<string>(PARAM_TARGET_SPOT_TAG_STR);
            if (placeTarget == null)
            {
                this.Fail(); //no target, fail
                return;
            }

            //1 - walks to given place
            Context walkContext = new Context();
            walkContext[Move.PARAM_SPOT_TAG_STR] = placeTarget;
            walkContext[Move.PARAM_TO_TARGET_STR] = Constants.SET_ENTITY_NAME;
            this.AddNext(this.Character.GetAction(Entities.Body.ACTION_WALK_TO_STR), walkContext);

            //2 - stands at end
            this.AddAtEnd(this.Character.GetAction(Entities.Body.ACTION_STAND_STR), null);

            base.OnStart(context);
        }
    }
}
