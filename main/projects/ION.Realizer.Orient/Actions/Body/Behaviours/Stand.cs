/* ****************************************************
 * Name: Stand.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/10/23
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    public class Stand : Behaviour
    {
        protected override void OnStart(Context context)
        {
            //1 - character just stands at end
            this.AddAtEnd(this.Character.GetAction(Entities.Body.ACTION_STAND_STR), null);

            base.OnStart(context);
        }
    }
}
