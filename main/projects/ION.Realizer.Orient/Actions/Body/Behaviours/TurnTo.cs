/* ****************************************************
 * Name: TurnTo.cs
 * Code: Michael Kriegel - michael@macs.hw.ac.uk
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/09/05
 * 
 * Heriot-Watt University
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using ION.Core;
using ION.Realizer.Orient.Entities;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    public class TurnTo : Stand
    {
        #region Constants

        public const string PARAM_TARGET_STR = "target";

        #endregion

        #region ION Methods

        protected override void OnStart(Context context)
        {
            // checks target
            if (!context.Contains(PARAM_TARGET_STR))
            {
                //no target, fail
                this.Fail();
                return;
            }
            string target = context.Get<string>(PARAM_TARGET_STR);

            //1 - target turns to character only if it is a character entity...
            Context turnContext = new Context();
            if (Universe.Instance.GetEntity(target) is Character)
            {
                turnContext[Turn.PARAM_TO_TARGET_STR] = this.Character.Name;
                this.AddNext(Universe.Instance.GetEntity(target)
                    .GetAction(Entities.Body.ACTION_TURN_TO_STR), turnContext);
            }

            // ... and character turns to target
            turnContext = new Context();
            turnContext[Turn.PARAM_TO_TARGET_STR] = target;
            this.AddNext(this.Character.GetAction(Entities.Body.ACTION_TURN_TO_STR), turnContext);

            //2 - character stands at end
            base.OnStart(context);
        }

        #endregion
    }
}
