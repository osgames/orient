/* ****************************************************
 * Name: DoActionStand.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/10/23
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    internal class SingleActionBehaviour : Stand
    {
        #region Constants

        public const string ARG_ACTION_ID_STR = "action-id";

        #endregion

        #region Fields

        protected string actionID;

        #endregion

        #region ION Methods

        protected override void OnCreate(Arguments arguments)
        {
            base.OnCreate(arguments);

            //checks arguments
            this.actionID = arguments.ContainsKey(ARG_ACTION_ID_STR) ?
                arguments.Get<string>(ARG_ACTION_ID_STR) : null;
        }

        protected override void OnStart(Context context)
        {
            //1 - character just performs the given action
            if (this.actionID != null)
                this.AddNext(this.Character.GetAction(this.actionID), null);

            //2 - and then stands at end
            base.OnStart(context);
        }

        #endregion
    }
}
