using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using ION.Realizer.Orient;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    class PressButton : Gesture
    {
        protected override void OnStart(Context context)
        {
            // modify context (target should be machine but mind will have set the target to be a button)
            if (Universe.Instance.HasEntity<Entities.Item>("machine"))
            {   // machine present, set it as target
                context[PARAM_TARGET_STR] = "machine";
                base.OnStart(context);        
            } 
            else // machine not present, fail this behaviour
            {
                this.Fail();
            }
        }    
    
    
    }
}
