using ION.Core;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    internal class PickFromTree : Behaviour
    {
        protected override void OnStart(Context context)
        {            
            string itemID = context.Get<string>("parameters");
            if ((itemID == null) || !Universe.Instance.HasEntity<Entities.Item>(itemID))
            {
                this.Fail(); //no target, fail
                return;
            }

            //check picked item
            Entities.Item item = (Entities.Item) Universe.Instance.GetEntity(itemID);
            if (item.Picked)
            {
                if (this.Character.PickedItems.Contains(item))
                    //if character already has the item, end
                    this.End();
                else
                    //if item was picked by someone else
                    this.Fail();
                return;
            }

            //somebody else already wants to pick this
            if (item.ChosenForPicking)
            {
                this.Fail(); //fail
            }
            else
            {
                // put a claim on picking that item
                item.ChosenForPicking = true;
            }

            //1 - character walks to the target item
            Context walkContext = new Context();
            walkContext[Move.PARAM_SPOT_TAG_STR] = "pick-spot";
            walkContext[Move.PARAM_TO_TARGET_STR] = itemID;
            //walkContext["item"] = itemID;
            this.AddNext(this.Character.GetAction(Entities.Body.ACTION_WALK_TO_STR), walkContext);

            //2 - character picks item
            Context pickContext = new Context();
            pickContext[Pick.PARAM_ITEM_ID_STR] = itemID;
            this.AddNext(this.Character.GetAction(Entities.Body.ACTION_PICK_FROM_TREE_STR), pickContext);
            
            Context turnContext = new Context();
            turnContext[Turn.PARAM_TO_TARGET_STR] = itemID;
            this.AddSimultaneous(this.Character.GetAction(Entities.Body.ACTION_TURN_TO_STR), turnContext);

            //3 - character stands
            this.AddAtEnd(this.Character.GetAction(Entities.Body.ACTION_STAND_STR), null);

            base.OnStart(context);
        }
    }
}