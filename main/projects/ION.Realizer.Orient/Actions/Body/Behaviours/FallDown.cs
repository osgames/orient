using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    class FallDown : Behaviour
    {
        protected override void OnStart(Context context)
        {
            //1 - character falls
            this.AddNext(this.Character.GetAction(Entities.Body.ACTION_FALL_STR), null);

            //2 - character just lies at end
            this.AddAtEnd(this.Character.GetAction(Entities.Body.ACTION_LIE_STR), null);

            base.OnStart(context);
        }

        protected override void OnEnd(Context context)
        {
            // set character's dying property to true
            if (this.Character.HasProperty<Property<string>>("dying"))
                this.Character.GetProperty<Property<string>>("dying").Value = "True";

            base.OnEnd(context);
        }

    }
}
