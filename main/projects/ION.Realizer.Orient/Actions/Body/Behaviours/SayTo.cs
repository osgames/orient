/* ****************************************************
 * Name: SayTo.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    public class SayTo : AttendTo
    {
        #region Constants

        public const string PARAM_UTTERANCE_STR = "utterance"; 

        #endregion

        #region ION Methods

        protected override void OnStart(Context context)
        {
            //checks character already speaking
            if (!this.allowSimultaneousBehavior && this.IsSameBehaviourRunning)
            {
                this.Fail();
                return;
            }

            //sets this behavior running
            this.Running = true;

            //checks arguments
            string target = context.Get<string>(PARAM_TARGET_STR);
            string utterance = context.Get<string>(PARAM_UTTERANCE_STR);
            if ((target == null) || (utterance == null))
            {
                this.Fail(); //no target or utterance, fail
                return;
            }

            //1 - character attends to target, both turn to each other
            // and wait for the behaviour to end, character stands at end
            base.OnStart(context);

            //2 - character speaks
            Context speakContext = new Context();
            speakContext[Speak.PARAM_TARGET_STR] = target;
            speakContext[Speak.PARAM_UTTERANCE_STR] = utterance;
            this.AddNext(this.Character.GetAction(Entities.Body.ACTION_SPEAK_STR), speakContext);
        }

        #endregion
    }
}
