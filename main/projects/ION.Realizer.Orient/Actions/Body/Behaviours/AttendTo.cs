/* ****************************************************
 * Name: AttendTo.cs
 * Code: Michael Kriegel - michael@macs.hw.ac.uk
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/09/05
 * 
 * Heriot-Watt University
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using ION.Core;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    public class AttendTo : TurnTo
    {

        #region Constants

        public const string ARG_WALK_TO_STR = "walk-to";
        public const string ARG_WALK_TO_SPOT_TAG = "walk-to-spot";
        public const string ARG_WALK_TO_MIN_DISTANCE = "walk-to-min-dist";

        #endregion

        #region Fields

        protected bool walkTo;
        protected string walkToSpotTag;
        protected int walkToMinDist;

        #endregion

        #region ION Methods

        protected override void OnCreate(Arguments arguments)
        {
            //checks arguments
            this.walkTo = arguments.ContainsKey(ARG_WALK_TO_STR) ?
                arguments.Get<bool>(ARG_WALK_TO_STR) : true;
            this.walkToSpotTag = arguments.ContainsKey(ARG_WALK_TO_SPOT_TAG) ?
                arguments.Get<string>(ARG_WALK_TO_SPOT_TAG) : Entities.Body.SPOT_TAG_TALK;
            this.walkToMinDist = arguments.ContainsKey(ARG_WALK_TO_MIN_DISTANCE) ?
                arguments.Get<int>(ARG_WALK_TO_MIN_DISTANCE) : 60;          
            base.OnCreate(arguments);
        }
        
        protected override void OnStart(Context context)
        {
            // checks target
            if (!context.Contains(PARAM_TARGET_STR))
            {
                //no target, fail
                this.Fail();
                return;
            }
            string target = context.Get<string>(PARAM_TARGET_STR);

            //1 - if this is a walk-to behaviour, walk to target's spot first
            if (this.walkTo)
            {
                // find out if character should really be walking to the target
                bool reallyWalkTo = false;

                // always walk to the user (if this is a walk-to behaviour)
                if (target.Equals(Entities.UserEntity.ENTITY_NAME)) reallyWalkTo = true;

                // if target is an item also walk-to
                else if (Universe.Instance.HasEntity<Entities.Item>(target)) reallyWalkTo = true;

                // if target is character, check distance to target, if smaller than min distance don't move
                else if (Universe.Instance.HasEntity<Entities.Character>(target))
                {
                    // get target character object
                    Entities.Character targetChar = Universe.Instance.GetEntity<Entities.Character>(target);

                    // determine distance
                    double distanceToTarget = (double)(PAGE.OGRE.Math.OgreDistance)this.Character.GraphicalAsset.AbsolutePosition.DistanceTo(targetChar.GraphicalAsset.AbsolutePosition);

                    // only really walk if the target is further away then min distance
                    if (distanceToTarget > walkToMinDist) reallyWalkTo = true;
                }

                if (reallyWalkTo)
                {
                    Context walkContext = new Context();
                    walkContext[Move.PARAM_TO_TARGET_STR] = target;
                    walkContext[Move.PARAM_SPOT_TAG_STR] = this.walkToSpotTag;
                    this.AddNext(this.Character.GetAction(Entities.Body.ACTION_WALK_TO_STR), walkContext);
                }
            }

            //2 - both characters turn to each other, character stands at end
            base.OnStart(context);
        }

        #endregion
    }
}
