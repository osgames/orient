/* ****************************************************
 * Name: SitOnChair.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/09/16
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Collections.Generic;
using ION.Core;
using ION.Core.Events;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    internal class SitOnChair : Stand
    {
        #region Constants

        public const string PARAM_CHAIR_TAG_STR = "target"; 

        #endregion

        #region Fields

        protected Context startContext;
        protected Entities.Item sittableChosenItem;
        protected List<Entities.Item> testedItems = new List<Entities.Item>(); 

        #endregion

        #region ION Methods

        protected override void OnStart(Context context)
        {
            this.startContext = context;

            //checks parameters
            string chairTag = context.Contains(PARAM_CHAIR_TAG_STR) ?
                context.Get<string>(PARAM_CHAIR_TAG_STR) : null;
            if (string.IsNullOrEmpty(chairTag))
            {
                this.Fail(); //no type, fail
                return;
            }

            //character already seated on a chair type like the one requested
            if(this.Character.IsSeated && 
               this.Character.SeatedOnItem.GraphicalAsset.ResourceAsset.Tags.Contains(chairTag))
            {
                this.End(); //already seated, success
                return;
            }

            //no sittable items found
            if (!this.ChooseSittableItem(chairTag))
            {
                this.Fail();
                return;
            }

            //1 - character walks to the sittable item left spot
            Context moveContext = new Context();
            moveContext[Move.PARAM_SPOT_TAG_STR] = SitDown.SIT_SPOT_TAG;
            moveContext[Move.PARAM_TO_TARGET_STR] = sittableChosenItem.Name;
            this.AddNext(this.Character.GetAction(Entities.Body.ACTION_WALK_TO_STR), moveContext);

            //2 - character sits on chair
            this.AddNext(this.Character.GetAction(Entities.Body.ACTION_SIT_DOWN_STR), null);

            //3 - character is placed in item's sit spot ...
            Context placeContext = new Context();
            placeContext[Place.PARAM_TARGET_ASSET_ID_STR] = this.sittableChosenItem.Name;
            placeContext[Place.PARAM_SPOT_TAG_STR] = SitDown.SEATED_TAG;
            placeContext[Place.PARAM_FOCUS_STR] = Place.DO_NOT_FOCUS_STR;
            placeContext[Place.PARAM_FADE_STR] = Place.DO_NOT_FADE_STR;
            this.AddNext(this.Character.GetAction(Entities.Body.ACTION_PLACE_STR), placeContext);

            // ... and stands
            this.AddSimultaneous(this.Character.GetAction(Entities.Body.ACTION_STAND_STR), null);

            //4 - character stands at end
            base.OnStart(context);
        }

        protected override void OnFail(Context context)
        {
            //sittable item was not needed
            if (this.sittableChosenItem != null)
                this.sittableChosenItem.ChosenForSitting = false;

            //did not end ok, clears tested items list 
            this.testedItems.Clear();

            base.OnFail(context);
        }

        protected override void OnEnd(Context context)
        {
            //ended ok, clears tested items list 
            this.testedItems.Clear();

            base.OnEnd(context);
        }

        #endregion

        #region Move Action Failed Handling

        public override void Invoke(ActionFailed evt)
        {
            //if character coul not reach chair, try another one
            if (evt.Action is Move)
            {
                //cleans behavior and starts again
                this.Clean();
                this.OnStart(this.startContext);
                return;
            }
            base.Invoke(evt);
        } 

        #endregion

        #region Protected Methods

        protected bool ChooseSittableItem(string chairTag)
        {
            //previous chosen item is "cleared"
            if(this.sittableChosenItem != null)
                this.sittableChosenItem.ChosenForSitting = false;

            //checks all available sittable items
            this.sittableChosenItem = null;
            foreach (Entities.Item item in this.Character.Set.Items)
            {
                if (item.GraphicalAsset.ResourceAsset.Tags.Contains(chairTag) &&
                    !this.testedItems.Contains(item) &&
                    !item.ChosenForSitting)
                {
                    this.sittableChosenItem = item;
                    break;
                }
            }

            //adds to tested items
            if (this.sittableChosenItem != null)
            {
                this.testedItems.Add(this.sittableChosenItem);
                this.sittableChosenItem.ChosenForSitting = true;
            }

            return this.sittableChosenItem != null;
        } 

        #endregion
    }
}