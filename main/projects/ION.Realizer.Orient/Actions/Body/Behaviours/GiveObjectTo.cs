/* ****************************************************
 * Name: GiveObject.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/05/21
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Realizer.Orient.Entities;

namespace ION.Realizer.Orient.Actions.Body.Behaviours
{
    public class GiveObjectTo : AttendTo
    {
        #region ION Methods

        protected override void OnCreate(Arguments arguments)
        {
            base.OnCreate(arguments);

            //override attend to parameters: character has to walk to target to give the object
            this.walkTo = true;
            this.walkToSpotTag = Entities.Body.SPOT_TAG_GIVE_OBJECT;
            this.walkToMinDist = 0;

        }

        protected override void OnStart(Context context)
        {
            //checks target
            string targetID = context.Contains(PARAM_TARGET_STR) ? 
                context.Get<string>(PARAM_TARGET_STR) : null;
            if ((targetID == null) || !Universe.Instance.HasEntity<Character>(targetID))
            {
                this.Fail(); //no target, fail
                return;
            }
            Character target = (Character) Universe.Instance.GetEntity(targetID);

            //1 - character attends to target, both turn to each other
            // and wait for the behaviour to end, character stands at end
            base.OnStart(context);

            //2 - character gives object...
            this.AddNext(this.Character.GetAction(Entities.Body.ACTION_GIVE_OBJ_FRONT_STR), null);

            // ...and target receives at the same time
            this.AddSimultaneous(target.GetAction(Entities.Body.ACTION_RECEIVE_OBJ_FRONT_STR), null);

            //3 - target character also stands at end
            this.AddAtEnd(target.GetAction(Entities.Body.ACTION_STAND_STR), null);
        }

        #endregion
    }
}
