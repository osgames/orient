/* ****************************************************
 * Name: Pick.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/08/29
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;

namespace ION.Realizer.Orient.Actions.Body
{
    public class Pick : Animate
    {
        public const string PARAM_ITEM_ID_STR = "item";
        public const string SYNC_POINT_PICKED_STR = "picked";

        protected Entities.Item currentItem;

        protected override void OnStart(Context context)
        {
            this.currentItem = null;

            // verifies picked item
            string itemID = context.Contains(PARAM_ITEM_ID_STR) ? 
                context.Get<string>(PARAM_ITEM_ID_STR) : null;
            if ((itemID != null) && Universe.Instance.HasEntity<Entities.Item>(itemID))
            {
                this.currentItem = Universe.Instance.GetEntity<Entities.Item>(itemID);
                if (this.currentItem.Picked)
                {
                    //item already picked
                    this.currentItem = null;
                    this.Fail();
                    return;
                }

                //stores item in picked items list
                this.Body.PickedItems.Add(this.currentItem);
                this.currentItem.Picked = true;

                base.OnStart(context);
            }
            else
            {
                this.Fail(); //no item, fail
            }
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);

            //unplaces item
            if (this.currentItem != null)
                this.currentItem.GraphicalAsset.Unplace();
        }

        protected override void OnSyncPoint(string syncPoint)
        {
            base.OnSyncPoint(syncPoint);

            //item is set invisible if picked trigger was raised
            if ((syncPoint == SYNC_POINT_PICKED_STR) && (this.currentItem != null))
                this.currentItem.GraphicalAsset.FadeOut(Constants.FADE_TIME);
        }
    }
}