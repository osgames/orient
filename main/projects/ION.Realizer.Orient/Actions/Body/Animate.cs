/* ****************************************************
 * Name: Animate.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Realizer.Orient.Actions.Body;

namespace ION.Realizer.Orient.Actions.Body
{
    public class Animate : BodyAction
    {
        #region Constants

        public const string ARG_ACTION_ID_STR = "action-id";
        public const string ARG_SEATED_ACTION_ID_STR = "seated-action-id";
        public const string ARG_ANIMATE_FOREVER_STR = "forever";
        public const string ARG_START_RANDOM_STR = "random-start";

        public const string PARAM_SEND_END_STR = "send-end"; 

        #endregion

        #region Fields

        protected string actionID, sittedActionID;
        protected bool animateForever, randomTime;

        #endregion

        #region ION Methods

        protected override void OnCreate(Arguments arguments)
        {
            base.OnCreate(arguments);

            //checks arguments
            this.actionID = arguments.Get<string>(ARG_ACTION_ID_STR);

            this.sittedActionID = arguments.ContainsKey(ARG_SEATED_ACTION_ID_STR) ?
                arguments.Get<string>(ARG_SEATED_ACTION_ID_STR) :
                this.actionID;

            this.animateForever = arguments.ContainsKey(ARG_ANIMATE_FOREVER_STR) ?
                arguments.Get<bool>(ARG_ANIMATE_FOREVER_STR) : false;

            this.randomTime = arguments.ContainsKey(ARG_START_RANDOM_STR) ?
                arguments.Get<bool>(ARG_START_RANDOM_STR) : false;

            this.actionCallback = new BodyActionCallback(this, !animateForever);
        }

        protected override void OnStart(Context context)
        {
            base.OnStart(context);

            this.actionCallback.Reset();

            //verifies sitted state and chooses correct action to perform
            string animActionID =
                    this.Body.IsSeated ? this.sittedActionID : this.actionID;

            //animates body
            if (this.animateForever)
            {
                //does not treat loops, action is finished, start from random time
                this.Body.GraphicalAsset.Animate(null, animActionID, this.randomTime, this.animateForever);

                //checks send End()
                bool sendEnd = context.Contains(PARAM_SEND_END_STR) ?
                    context.Get<bool>(PARAM_SEND_END_STR) : true;
                if (sendEnd) this.End();
            }
            else
            {
                //handles action finish, no loop, start from beggining
                this.Body.GraphicalAsset.Animate(this, animActionID, this.randomTime, this.animateForever);
            }
        } 

        #endregion

        #region ActionCallback Methods

        public override void SendSuccess()
        {
            base.SendSuccess();
            this.Body.GraphicalAsset.StopAnimating();
        }

        #endregion
    }
}