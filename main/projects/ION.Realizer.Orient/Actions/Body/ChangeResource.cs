/* ****************************************************
 * Name: ChangeBodyResource.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using PAGE.Orient.Domain.Assets;

namespace ION.Realizer.Orient.Actions.Body
{
    public class ChangeResource : BodyAction
    {
        public const string ARG_RESOURCE_STR = "resource";
        public const string PARAM_EXPRESSION_STR = "expression";
        public const string PARAM_CLOTHES_STR = "clothes";
        public const string RESOURCE_FACE_STR = "face";
        public const string RESOURCE_BODY_STR = "body";

        private string resource;

        protected override void OnCreate(Arguments arguments)
        {
            base.OnCreate(arguments);

            this.resource = arguments[ARG_RESOURCE_STR] as string;
        }

        protected override void OnStart(Context context)
        {
            base.OnStart(context);

            switch (this.resource)
            {
                case RESOURCE_FACE_STR:
                    //makes a fade-out from previous expressions and fade-in to new expression
                    string expression = context.Get<string>(PARAM_EXPRESSION_STR);
                    this.Body.GraphicalAsset.ChangeFace(OrientCharacter.NATURAL_EXPRESSION, 1);
                    this.Body.GraphicalAsset.ChangeFace(expression, 1);
                    break;

                case RESOURCE_BODY_STR:
                    string clothes = context.Get<string>(PARAM_CLOTHES_STR);
                    this.Body.GraphicalAsset.ChangeTexture(clothes);
                    break;
            }
            this.End();
        }
    }
}