/* ****************************************************
 * Name: Unplace.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Core.Extensions;

namespace ION.Realizer.Orient.Actions.Body
{
    public class Unplace : BodyAction
    {
        protected override void OnStart(Context context)
        {
            base.OnStart(context);

            //fades out character and unplaces from set
            this.Body.GraphicalAsset.FadeOut(Constants.FADE_TIME);
            this.Body.GraphicalAsset.Unplace();
            this.Body.GraphicalAsset.Set = null;
            this.Body.GraphicalAsset.StopMoving();
            this.Body.GraphicalAsset.StopAnimating();
            this.Body.GraphicalAsset.StopTalking();
            this.Body.GraphicalAsset.HideName();

            // reset some body properties
            this.Body.IsSeated = false;
            this.Body.SeatedOnItem = null;
            this.Body.PickedItems.Clear();


            // placement ended
            this.End();
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);

            this.Body.LeftPlace(); //left place
        }
    }
}