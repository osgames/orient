/* ****************************************************
 * Name: SitDown.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/09/17
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using PAGE.Generic.Domain.Spots;

namespace ION.Realizer.Orient.Actions.Body
{
    public class SitDown : Animate
    {
        public const string ARG_ACTION_SIT_LEFT_STR = "action-sit-left";
        public const string ARG_ACTION_SIT_RIGHT_STR = "action-sit-right";
        public const string ARG_ACTION_SIT_FRONT_STR = "action-sit-front";

        public const string PARAM_TARGET_SIT_ITEM_STR = "target";

        public const string SEATED_TAG = "sit-chair";
        public const string SIT_LEFT_TAG = "left";
        public const string SIT_RIGHT_TAG = "right";
        public const string SIT_FRONT_TAG = "front";
        public const string SIT_SPOT_TAG = "sit";

        protected string sitLeftActionID, sitRightActionID, sitFrontActionID;

        protected override void OnCreate(Arguments arguments)
        {
            base.OnCreate(arguments);

            //checks sit animations
            this.sitFrontActionID = this.actionID;
            this.sitLeftActionID = arguments.ContainsKey(ARG_ACTION_SIT_LEFT_STR) ?
                arguments.Get<string>(ARG_ACTION_SIT_LEFT_STR) :
                this.sitFrontActionID;

            this.sitRightActionID = arguments.ContainsKey(ARG_ACTION_SIT_RIGHT_STR) ?
                arguments.Get<string>(ARG_ACTION_SIT_RIGHT_STR) :
                this.sitFrontActionID;
        }

        protected override void OnStart(Context context)
        {
            // verifies item
            string itemID = context.Contains(PARAM_TARGET_SIT_ITEM_STR) ? 
                context.Get<string>(PARAM_TARGET_SIT_ITEM_STR) : null;
            if ((itemID != null) && Universe.Instance.HasEntity<Entities.Item>(itemID))
            {
                this.Body.SeatedOnItem = Universe.Instance.GetEntity<Entities.Item>(itemID);
                this.Body.SeatedOnItem.ChosenForSitting = true;

                //checks animation according to current place
                if (this.Body.GraphicalAsset.CurrentSpot.ResourceSpot.Tags.Contains(SIT_LEFT_TAG))
                    this.actionID = this.sitRightActionID;
                else if (this.Body.GraphicalAsset.CurrentSpot.ResourceSpot.Tags.Contains(SIT_FRONT_TAG))
                    this.actionID = this.sitFrontActionID;
                else if (this.Body.GraphicalAsset.CurrentSpot.ResourceSpot.Tags.Contains(SIT_RIGHT_TAG))
                    this.actionID = this.sitLeftActionID;

                //animates body
                base.OnStart(context); 

                //animates sit movement
                GenericWaypoint sitPoint =
                    this.Body.SeatedOnItem.GraphicalAsset.Waypoints[SEATED_TAG][0];
                float transitionTime = this.Body.GraphicalAsset.CurrentAnimationLenght;
                if (sitPoint != null)
                {
                    this.Body.GraphicalAsset.ChangePosition(
                        sitPoint.AbsolutePosition, transitionTime);
                }
            }
            else
            {
                this.Fail(); //no item, fail
            }
        }

        protected override void OnEnd(Context context)
        {
            //sets body property
            if (this.Body.HasProperty(Entities.Body.PROPERTY_IS_SEATED_STR))
                this.Body.GetProperty<Property<string>>(
                    Entities.Body.PROPERTY_IS_SEATED_STR).Value = Constants.FATIMA_TRUE_VALUE;

            base.OnEnd(context);
        }
    }
}