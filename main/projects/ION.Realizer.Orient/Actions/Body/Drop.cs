/* ****************************************************
 * Name: Drop.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/08/29
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;

namespace ION.Realizer.Orient.Actions.Body
{
    public class Drop : Animate
    {
        public const string PARAM_ITEM_ID_STR = "item";
        public const string SYNC_POINT_DROPPED_STR = "dropped";

        protected Entities.Item currentItem;

        protected override void OnStart(Context context)
        {
            this.currentItem = null;

            // verifies picked item
            string itemID = context.Contains(PARAM_ITEM_ID_STR) ? 
                context.Get<string>(PARAM_ITEM_ID_STR) : null;
            if ((itemID != null) && Universe.Instance.HasEntity<Entities.Item>(itemID))
            {
                this.currentItem = Universe.Instance.GetEntity<Entities.Item>(itemID);
                if (!this.Body.PickedItems.Contains(this.currentItem))
                {
                    //item doesn't belong to this character
                    this.currentItem = null;
                    this.Fail();
                    return;
                }

                //removes item from picked items list
                this.Body.PickedItems.Remove(this.currentItem);
                this.currentItem.Picked = false;
                this.currentItem.ChosenForPicking = false;

                base.OnStart(context);
            }
            else
            {
                this.Fail(); //no item, fail
            }
        }

        protected override void OnSyncPoint(string syncPoint)
        {
            base.OnSyncPoint(syncPoint);

            if ((syncPoint == SYNC_POINT_DROPPED_STR) && (this.currentItem != null))
            {
                //item is set visible
                this.currentItem.GraphicalAsset.FadeIn(Constants.FADE_TIME);
            }
        }
    }
}