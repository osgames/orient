/* ****************************************************
 * Name: Place.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Realizer.Orient.Actions.Camera;
using ION.Realizer.Orient.Entities;
using PAGE.Generic.Domain.Assets;
using PAGE.Orient;

namespace ION.Realizer.Orient.Actions.Body
{
    public class Place : BodyAction
    {
        public const string PARAM_TARGET_ASSET_ID_STR = "set";
        public const string PARAM_SPOT_TAG_STR = "spot";
        public const string PARAM_FADE_STR = "fade";
        public const string PARAM_FOCUS_STR = "focus";

        public const string DO_NOT_FADE_STR = "false";
        public const string DO_NOT_FOCUS_STR = "false";
        public const string NO_PLACE_STR = "none";

        protected override void OnStart(Context context)
        {
            base.OnStart(context);

            //checks for asset where to place the character
            if (!context.Contains(PARAM_TARGET_ASSET_ID_STR) ||
                !Universe.Instance.HasEntity(context.Get<string>(PARAM_TARGET_ASSET_ID_STR)))
            {
                // no asset, cannot place
                this.Fail();
                return;
            }

            Asset asset = Universe.Instance.GetEntity<Asset>(context.Get<string>(PARAM_TARGET_ASSET_ID_STR));
            GenericAsset graphicalAsset = asset.GraphicalAsset;

            this.Body.GraphicalAsset.Reset();

            //associates character with set
            if (asset is Set)
            {
                this.Body.GraphicalAsset.Set = graphicalAsset as GenericSet;
                this.Body.Set = asset as Set;
            }
            else if (asset is Entities.Item)
            {
                this.Body.GraphicalAsset.Set = ((Entities.Item)asset).Set.GraphicalAsset;
                this.Body.Set = ((Entities.Item)asset).Set;
            }


            //places character
            string spotTag = context.Contains(PARAM_SPOT_TAG_STR) ? 
                context.Get<string>(PARAM_SPOT_TAG_STR) : null;
            if (spotTag != null)
            {
                this.Body.CurrentPlaceID = spotTag;
                this.Body.GraphicalAsset.PlaceRandomly(null, graphicalAsset, spotTag, true);
            }
            else
            {
                this.Body.GraphicalAsset.PlaceRandomly(null, graphicalAsset, true);
            }

            //character left place
            this.Body.LeftPlace();

            //fades in character
            if (!context.Contains(PARAM_FADE_STR) ||
                (context.Get<string>(PARAM_FADE_STR) != DO_NOT_FADE_STR))
                this.Body.GraphicalAsset.FadeIn(Constants.FADE_TIME);
            else
                this.Body.GraphicalAsset.Visible = true;


            //show name if showing names is switched on
            if (OrientGraphicsRealizer.Instance.CharacterNamesShown)
                this.Body.GraphicalAsset.ShowName(this.Body.Name);
            else
                this.Body.GraphicalAsset.HideName();

            // for testing
            // this.Body.GraphicalAsset.ShowAllDebuggingSpots();


            //focus camera on character body
            if (!context.Contains(PARAM_FOCUS_STR) ||
                (context.Get<string>(PARAM_FOCUS_STR) != DO_NOT_FOCUS_STR))
            {
                Entities.Camera camera;
                if (Universe.Instance.TryGetEntity(Constants.ORIENT_CAMERA_NAME, out camera))
                {
                    Context camContext = new Context();
                    camContext[FocusCharacter.PARAM_CHARACTER_ID_STR] = this.Body.Name;
                    camContext[FocusCharacter.PARAM_TARGET_STR] = FocusCharacter.TARGET_BODY;
                    camera.GetAction(Entities.Camera.ACTION_FOCUS_CHARACTER_STR).Start(camContext);
                }
            }

            // placement ended
            this.End();
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);

            this.Body.ArrivedPlace(); //arrived place
        }
    }
}