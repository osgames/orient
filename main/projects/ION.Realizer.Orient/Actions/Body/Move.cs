/* ****************************************************
 * Name: Move.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Core.Events;
using ION.Realizer.Orient.Entities;
using PAGE.Generic.Domain.Assets;

namespace ION.Realizer.Orient.Actions.Body
{
    public class Move : MultipleAttemptAction,
        IEventCallback<ActionEnded>, IEventCallback<ActionFailed>
    {
        #region Constants

        protected const string GET_UP_ACTION = Entities.Body.ACTION_GET_UP_RIGHT_STR;

        //arguments and parameters
        public const string ARG_ACTION_ID_STR = "action-id";
        public const string PARAM_TO_TARGET_STR = "target";
        public const string PARAM_SPOT_TAG_STR = "spot";
        public const string TARGET_USER_STR = "user";

        #endregion

        #region Fields

        protected string actionID;
        protected bool waitingForGetUp;

        #endregion

        #region ION Methods

        protected override void OnCreate(Arguments arguments)
        {
            base.OnCreate(arguments);
            this.actionID = arguments.Get<string>(ARG_ACTION_ID_STR);

            //registers listeners for get-up action
            this.Body.GetAction(GET_UP_ACTION).CreateEventListener<ActionEnded>(this);
            this.Body.GetAction(GET_UP_ACTION).CreateEventListener<ActionFailed>(this);
        }

        protected override void OnStart(Context context)
        {
            base.OnStart(context);

            //if character is seated, first get up...
            if (this.Body.IsSeated)
            {
                this.waitingForGetUp = true;
                this.Body.GetAction(GET_UP_ACTION).Start();
                return;
            }

            //...then move
            this.Body.LeftPlace(); //left place

            // checks target
            GenericAsset target = this.Body.GraphicalAsset.Set;
            string targetID = context.Contains(PARAM_TO_TARGET_STR) ?
                context.Get<string>(PARAM_TO_TARGET_STR) : null;
            if (targetID != null)
            {
                if (targetID.Equals(TARGET_USER_STR)) //user is target
                {
                    UserEntity userEntity = Universe.Instance.GetEntity<UserEntity>(targetID);
                    target = userEntity.UserDummy;
                }
                else if (targetID.Equals(this.Body.Name)) // self is target
                {
                    this.End();
                }
                else
                {   //someone/something else is target
                    target = ((Asset)Universe.Instance.GetEntity(targetID)).GraphicalAsset;
                }
            }

            if (!(targetID.Equals(this.Body.Name)))
            {
                // checks spot
                string spotTag = context.Contains(PARAM_SPOT_TAG_STR) ?
                    context.Get<string>(PARAM_SPOT_TAG_STR) : null;
                if (spotTag != null)
                {
                    //moves to tagged target spot
                    this.Body.CurrentPlaceID = spotTag;
                    this.Body.GraphicalAsset.Move(this, this.actionID, target, spotTag);
                }
                else
                {
                    //moves to closest target spot
                    this.Body.GraphicalAsset.Move(this, this.actionID, target);
                }
            }
        }

        protected override void OnEnd(Context context)
        {
            this.Body.ArrivedPlace(); //arrived place
            base.OnEnd(context);
        }

        #endregion

        #region GetUp Handling

        public void Invoke(ActionEnded evt)
        {
            this.RestartMove();
        }

        public void Invoke(ActionFailed evt)
        {
            this.RestartMove();
        }

        private void RestartMove()
        {
            //character is up, start moving
            if (this.IsInvalid || !this.waitingForGetUp) return;
            this.OnStart(this.startContext);
            this.waitingForGetUp = false;
        }

        #endregion
    }
}