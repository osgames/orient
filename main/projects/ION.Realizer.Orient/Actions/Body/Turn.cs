/* ****************************************************
 * Name: Turn.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Realizer.Orient.Entities;

namespace ION.Realizer.Orient.Actions.Body
{
    public class Turn : BodyAction
    {
        #region Constants

        public const string PARAM_TO_TARGET_STR = "target";

        #endregion

        #region ION Methods

        protected override void OnStart(Context context)
        {
            base.OnStart(context);

            //checks target
            if (!context.Contains(PARAM_TO_TARGET_STR))
            {
                //no target, fail
                this.Fail();
                return;
            }

            //gets target
            string target = context.Get<string>(PARAM_TO_TARGET_STR);
            Entity targetEntity = Universe.Instance.HasEntity(target) ?
                Universe.Instance.GetEntity(target) : null;

            // checks if target is the user
            if ((target == UserEntity.ENTITY_NAME) && (targetEntity is UserEntity))
            {
                //turns to user position
                UserEntity user = targetEntity as UserEntity;
                if (user.UserDummy != null)
                    this.Body.GraphicalAsset.TurnTo(user.UserDummy);
            }
            else if (targetEntity is Asset)
            {
                //else turns to target character
                this.Body.GraphicalAsset.TurnTo(
                    ((Asset) targetEntity).GraphicalAsset);
            }
            else
            {
                //no possible target, fail
                this.Fail();
                return;
            }

            this.End(); //turn ok
        }


        #endregion
    }
}