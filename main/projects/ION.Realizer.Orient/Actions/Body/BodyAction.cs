/* ****************************************************
 * Name: BodyAction.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using ION.Core;
using ION.Realizer.Orient.Entities;
using ION.Realizer.Orient.Events;
using PAGE;
using PAGE.Generic;
using PAGE.Generic.Resources.Actions;

namespace ION.Realizer.Orient.Actions.Body
{
    public abstract class BodyAction : Action, IActionCallback
    {
        #region BodyActionCallback Class

        protected class BodyActionCallback : ActionCallback
        {
            protected BodyAction action;

            public BodyActionCallback(BodyAction action, bool ignoreMultipleResponses)
                : base(ignoreMultipleResponses)
            {
                this.action = action;
            }

            public BodyActionCallback(BodyAction action)
                : this(action, false)
            {
            }

            protected override void Succeeded()
            {
                Util.Util.LogMessage(this.action.Body, this.action, "PAGE sent SUCCEEDED");
                this.action.End();
            }

            protected override void Failed()
            {
                Util.Util.LogMessage(this.action.Body, this.action, "PAGE sent FAILED");
                this.action.Fail();
            }

            protected override void Triggered(ActionTrigger trigger)
            {
                Util.Util.LogMessage(this.action.Body, this.action, "PAGE sent TRIGGER: " + trigger.IdToken);
                this.action.OnSyncPoint(trigger.IdToken);
            }
        }

        #endregion

        #region Fields

        protected BodyActionCallback actionCallback;
        protected static int nameCounter = 1;

        #endregion

        #region Properties

        public Character Body
        {
            get { return (Character) this.Parent; }
        }

        public static string RandomName
        {
            get { return nameCounter++.ToString(); }
        }

        #endregion

        #region Constructors

        public BodyAction()
        {
            this.actionCallback = new BodyActionCallback(this);
        }

        #endregion

        #region ION Methods

        protected override void OnCreate(Arguments arguments)
        {
            Console.WriteLine(Universe.Instance.Tick + ": CREATED " + Body.Name + "." + this.Name);
            Util.Util.LogMessage(this.Body, this, "action CREATED");
        }

        protected override void OnDestroy()
        {
            Console.WriteLine(Universe.Instance.Tick + ": DESTROYED " + Body.Name + "." + this.Name);
            Util.Util.LogMessage(this.Body, this, "action DESTROYED");
        }

        protected override void OnStart(Context context)
        {
            Console.WriteLine(Universe.Instance.Tick + ": STARTED " + Body.Name + "." + this.Name);
            Util.Util.LogMessage(this.Body, this, "action STARTED");
        }

        protected override void OnEnd(Context context)
        {
            Console.WriteLine(Universe.Instance.Tick + ": ENDED " + Body.Name + "." + this.Name);
            Util.Util.LogMessage(this.Body, this, "action ENDED");
        }

        protected override void OnFail(Context context)
        {
            Console.WriteLine(Universe.Instance.Tick + ": FAILED " + Body.Name + "." + this.Name);
            Util.Util.LogMessage(this.Body, this, "action FAILED");
        }

        protected override void OnStep(Context context)
        {
        }

        #endregion

        #region Protected Methods

        // BODY ACTION
        protected virtual void OnSyncPoint(string syncPoint)
        {
            Console.WriteLine(Universe.Instance.Tick + ": SYNCPOINT REACHED " + Body.Name + "." + this.Name + " " +
                              syncPoint);
            this.RaiseEvent(new SyncPointReached(this, syncPoint));
        }

        #endregion

        #region ActionCallback Methods

        // CALLBACK
        public virtual void SendSuccess()
        {
            this.actionCallback.SendSuccess();
        }

        public virtual void SendFailure()
        {
            this.actionCallback.SendFailure();
        }

        public virtual void SendTrigger(ActionTrigger trigger)
        {
            this.actionCallback.SendTrigger(trigger);
        }

        #endregion
    }
}