/* ****************************************************
 * Name: MultipleAttemptAction.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/10/27
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using ION.Core;

namespace ION.Realizer.Orient.Actions.Body
{
    public class MultipleAttemptAction : BodyAction
    {
        #region Constants

        protected const int MAX_ATTEMPTS = 7;
        protected const int MAX_ATTEMPT_MILLIS = 4000;
        protected const int MIN_ATTEMPT_MILLIS = 1500;

        #endregion

        #region Fields

        protected Context startContext;
        protected int attemptNumber;
        protected Random rand;
        protected bool waitingForNextAttempt;
        protected long millisToNextAttempt;
        protected DateTime lastStepTime;

        #endregion

        #region ION Methods

        protected override void OnCreate(Arguments arguments)
        {
            base.OnCreate(arguments);
            this.rand = new Random((int) DateTime.Now.Ticks);
        }

        protected override void OnStart(Context context)
        {
            base.OnStart(context);

            if (!this.waitingForNextAttempt)
            {
                //stores context and resets attempts
                this.startContext = context;
                this.attemptNumber = 0;
            }
            this.waitingForNextAttempt = false;
        }

        protected override void OnStep(Context context)
        {
            base.OnStep(context);

            //not waiting for attempt, return
            if (!this.waitingForNextAttempt) return;

            //update time
            DateTime now = DateTime.Now;
            this.millisToNextAttempt -= (long) now.Subtract(this.lastStepTime).TotalMilliseconds;
            this.lastStepTime = now;

            //not yet time for new attempt, return
            if (this.millisToNextAttempt >= 0) return;

            //attempt to act again
            this.OnStart(this.startContext);
        }

        protected override void OnFail(Context context)
        {
            this.Body.GetAction(Entities.Body.ACTION_STAND_STR).Start();
            base.OnFail(context);
        }

        #endregion

        #region ActionCallback Methods

        public override void SendFailure()
        {
            this.Body.GetAction(Entities.Body.ACTION_STAND_STR).Start();

            //checks action attempts
            if (this.attemptNumber >= MAX_ATTEMPTS)
            {
                //no more attempts
                this.waitingForNextAttempt = false;
                base.SendFailure(); //action really fails
                return;
            }

            //define time for next action attempt
            this.attemptNumber++;
            this.millisToNextAttempt = this.rand.Next(MIN_ATTEMPT_MILLIS, MAX_ATTEMPT_MILLIS);
            this.waitingForNextAttempt = true;
            this.lastStepTime = DateTime.Now;
        }

        #endregion
    }
}