/* ****************************************************
 * Name: CameraAction.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/05/20
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using System.Collections.Generic;
using PAGE.Orient;
using ION.Realizer.Orient.Entities;
using ION.Realizer.Orient.Actions.User;
using PAGE.Orient.Domain.Cameras;
using PAGE.OGRE.Math;
using PAGE.Generic.Math;
using PAGE.Generic.Domain.Spots;
using Mogre;
using User.Interface;
using UserControllerClass = ION.Realizer.Orient.Entities.UserController;

namespace ION.Realizer.Orient.Actions.Camera
{
    public abstract class FirstPersonCameraAction : CameraAction
    {

        protected System.Collections.Generic.List<GenericWaypoint> exitWaypoints;
        static private bool beenToGarden = false;
        
        protected void updateUserEntity(GenericWaypoint userWayPoint)
        {
            //check if user entity exists
            if (Universe.Instance.HasEntity<UserEntity>(UserEntity.ENTITY_NAME))
            {
                //get user entity
                UserEntity user = Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME);
                
                //check for user actions caused by navigation
                if (user.Waypoint != null)
                {
                    //check if user approaches the tree (new wayPoint has tag nearTree)
                    if (!user.Waypoint.ResourceSpot.Tags.Contains("neartree") 
                        && userWayPoint.ResourceSpot.Tags.Contains("neartree")
                        && Universe.Instance.HasEntity<ION.Realizer.Orient.Entities.Item>("tree"))                        
                    {
                        Dictionary<string, object> arguments = new Dictionary<string, object>();
                        arguments.Add("target", "tree"); //action parameters
                        user.GetAction<UserStoryActionApproachTree>(UserStoryActionApproachTree.ACTION_NAME).Start(new Arguments(arguments));                   
                    }

                    //check if user approaches the machine (new wayPoint has tag nearMachine)
                    if (!user.Waypoint.ResourceSpot.Tags.Contains("nearmachine") 
                        && userWayPoint.ResourceSpot.Tags.Contains("nearmachine")
                        && Universe.Instance.HasEntity<ION.Realizer.Orient.Entities.Item>("machine"))
                    {
                        Dictionary<string, object> arguments = new Dictionary<string, object>();
                        arguments.Add("target", "machine"); //action parameters
                        user.GetAction<UserStoryActionApproachMachine>(UserStoryActionApproachMachine.ACTION_NAME).Start(new Arguments(arguments));
                    }

                    //check if user has stepped on a tiny tree
                    if (!user.Waypoint.ResourceSpot.Tags.Contains("has-tree") && userWayPoint.ResourceSpot.Tags.Contains("has-tree"))
                    {
                        Dictionary<string, object> arguments = new Dictionary<string, object>();
                        user.GetAction<UserStoryActionStepOnTree>(UserStoryActionStepOnTree.ACTION_NAME).Start(new Arguments(arguments));
                    }

                    //check if user is in the garden
                    if (!beenToGarden && !user.Waypoint.ResourceSpot.Tags.Contains("garden-spot") && userWayPoint.ResourceSpot.Tags.Contains("garden-spot"))
                    {
                        beenToGarden = true;
                        Dictionary<string, object> arguments = new Dictionary<string, object>();
                        arguments.Add("target", "garden"); //action parameters
                        user.GetAction<UserStoryActionFollowTo>(UserStoryActionFollowTo.ACTION_NAME).Start(new Arguments(arguments));
                    }
                
                }
                
                //set waypoint for user entity
                user.Waypoint = userWayPoint;
                //move the UserDummy
                user.UserDummy.Place(null, OrientGraphicsRealizer.Instance.Camera.currentSet, userWayPoint.ResourceSpot.IdToken, false);
            }
        }

        protected void updateUserEntity(PAGE.Generic.Math.Direction newDirection)
        {
            //check if user entity exists
            if (Universe.Instance.HasEntity<UserEntity>(UserEntity.ENTITY_NAME))
            {
                //get user entity
                UserEntity user = Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME);

                //set user dummy direction
                user.UserDummy.AbsoluteDirection = newDirection;
            }
        }

        // sets the user entity's direction to the direction of the camera
        protected void updateUserEntityAngle()
        {
            OrientCamera cam = OrientGraphicsRealizer.Instance.Camera;
            updateUserEntity(cam.AbsoluteDirection);
        }

        /// <summary>
        /// Places the first person camera in the scene (on a waypoint)
        /// </summary>
        public void placeFirstPersonCam()
        {
            
            OrientCamera cam = OrientGraphicsRealizer.Instance.Camera;
            // reset at the start of scenario so that the user can follow the character again when being asked to
            beenToGarden = false;
            // this is the first placement of the first person camera, so no next destination yet
            cam.nextDestination = null;
            // also no reorientation yet
            cam.newFacingAngle = 0.0f;


            // find a waypoint for the user to start
            cam.currentWayPoint = null;

            // check if the user has a previous location
            if (!UserEntity.PreviousLocation.Equals(""))
            { // previous location exists, place the user on a spot tagged exit:previouslocation
                foreach (GenericWaypoint wp in cam.currentSet.Waypoints)
                {
                    bool found = false;
                    foreach (string tag in wp.ResourceSpot.Tags)
                    {
                        if (tag.StartsWith("exit"))
                        {
                            if (tag.EndsWith(UserEntity.PreviousLocation))
                            {
                                cam.currentWayPoint = wp;
                                found = true;
                                break;
                            }
                        }
                    }
                    if (found) break;
                }
            }
            
            //no previous location or entry spot from previous location was not found
            if (cam.currentWayPoint == null) 
            { //place the user on a spot tagged "user-spot"
                foreach (GenericWaypoint wp in cam.currentSet.Waypoints)
                {
                   if (wp.ResourceSpot.Tags.Contains("user-spot"))
                   {
                       cam.currentWayPoint = wp;
                       break;
                   }
                }
            }

            // if no starting point has been found up to here use a random waypoint
            if (cam.currentWayPoint == null) cam.currentWayPoint = cam.currentSet.Waypoints[0];
            
            // update previous location variable for next location change (set it to this scene)
            // current location is stored in the property location of the object episode, so check if this exists and set it only then
            if (Universe.Instance.HasEntity<ModellessItem>("episode"))
            {
                ModellessItem episode = Universe.Instance.GetEntity<ModellessItem>("episode");
                if (episode.HasProperty<Property<string>>("location"))
                    UserEntity.PreviousLocation = episode.GetProperty<Property<string>>("location").Value;   
            }

            OgrePosition newCameraPos = cam.addFirstPersonHeightOffset(cam.currentWayPoint.AbsolutePosition);

            cam.Unplace();
            cam.AbsolutePosition = newCameraPos;
            Direction cameraDir = new OgreDirection(1.0f, 0.0f, 0.0f);
            cam.AbsoluteDirection = cameraDir;
            
            //for debugging purposes show waypoints
            //cam.currentSet.ShowAllDebuggingSpots();
            
            prepareSceneExits();            
        }

        protected void prepareSceneExits()
        {
            //check if there are scene exits from last episode to make invisible         
            if (exitWaypoints!=null) // 
                for (int i = 0; i < exitWaypoints.Count; i++) // number of waypoints tells us how many particle systems there are to make invisible
                {
                    Mogre.SceneManager sm = PAGE.Orient.OrientGraphicsRealizer.Instance.OgreApplication.SceneManager;
                    string psNodeName = "particleExitNode" + i;
                    Mogre.SceneNode pExitNode = (Mogre.SceneNode) sm.RootSceneNode.GetChild(psNodeName);
                    if (pExitNode!=null) pExitNode.SetVisible(false);
                }

            exitWaypoints = new List<GenericWaypoint>();
            foreach (GenericWaypoint wp in OrientGraphicsRealizer.Instance.Camera.currentSet.Waypoints)
            {
                foreach (string tag in wp.ResourceSpot.Tags)
                {
                    // search for right tag format exit:destinationName:episodeToLoad
                    if ((tag.StartsWith("exit")) && (tag.Split(':').Length == 2))
                    {
                        exitWaypoints.Add(wp);
                    }
                }
            }

            for (int i = 0; i < exitWaypoints.Count; i++)
            {   
                // place something on the exits
                Mogre.SceneManager sm = PAGE.Orient.OrientGraphicsRealizer.Instance.OgreApplication.SceneManager;
                string psName = "particleExit" + i;
                string psNodeName = "particleExitNode" + i;
                Mogre.SceneNode pExitNode;
                if (sm.HasParticleSystem(psName))
                {
                    // particle system already exists from last episode, just get scene node and make it visible
                    pExitNode = (Mogre.SceneNode) sm.RootSceneNode.GetChild(psNodeName);
                    pExitNode.SetVisible(true);
                }
                else
                {
                    // particle system doesnt exist yet, create it together with node
                    Mogre.ParticleSystem ps = sm.CreateParticleSystem(psName, "eCircus/SceneExit");
                    pExitNode = sm.RootSceneNode.CreateChildSceneNode(psNodeName);
                    pExitNode.AttachObject(ps);
                }
                OgrePosition wpPos = (OgrePosition) exitWaypoints[i].AbsolutePosition;
                pExitNode.SetPosition(wpPos.X, 12, wpPos.Z);
            }
        
        }

        private string destinationToDEWithPronoun(string destinationName)
        {
            if (destinationName.Equals("village", System.StringComparison.CurrentCultureIgnoreCase))
                return "zum Dorf";
            if (destinationName.Equals("garden", System.StringComparison.CurrentCultureIgnoreCase))
                return "zum Garten";
            if (destinationName.Equals("beach", System.StringComparison.CurrentCultureIgnoreCase))
                return "zum Strand";
            if (destinationName.Equals("glade", System.StringComparison.CurrentCultureIgnoreCase))
                return "zur Lichtung";
            return "";
        }

        // this function checks if the spot where the user just stands adds additional options to the user interface 
        protected void checkSpotContext()
        {
                   
            //check if user entity exists
            if (Universe.Instance.HasEntity<UserEntity>(UserEntity.ENTITY_NAME))
            {
            
                //get user entity & controller
                UserEntity user = Universe.Instance.GetEntity<UserEntity>(UserEntity.ENTITY_NAME);
                UserControllerClass controller = Universe.Instance.GetEntity<UserControllerClass>(UserControllerClass.ENTITY_NAME);

                bool exitFound = false;
                bool buttonSpotFound = false;

                // evaluate tags of current way point 
                foreach (string tag in user.Waypoint.ResourceSpot.Tags)
                {
                    // search for right tag format exit:destinationName:episodeToLoad -> this marks a scene exit
                    if ((tag.StartsWith("exit")) && (tag.Split(':').Length == 2))
                    {
                        exitFound = true;
                        string destinationName = tag.Split(':')[1];

                        // set leave scene context in user controller
                        controller.leaveSceneDestinationContext = destinationName;

                        // display leave scene information in user interface
                        UserInterface.Instance.displayContextInfo("press Start on the LINPad to travel to the " + destinationName + " from here",
                                                                  "Benutzt den LINPad Startknopf um "+  destinationToDEWithPronoun(destinationName) +" zu reisen");
                    }

                    // check if this spot is a spot where the machine buttons can be pressed
                    if (tag.Equals("button-spot"))
                    {
                        buttonSpotFound = true;

                        // set button context to true
                        controller.buttonContext = true;
                        
                        // display button pressing information in user interface (if button 1 has not already been pressed)
                        if (!controller.button1AlreadyPressed)
                            UserInterface.Instance.displayContextInfo("use button 1 or 2 on the WiiMote to operate the machine",
                                                                      "Bedient die Maschine mit den WiiMote Kn�pfen 1 und 2");
                                                                                       
                    }
                }

                // if no exit was found reset leave scene context 
                if (!exitFound)
                    controller.leaveSceneDestinationContext = "";
                
                // if no button spot was found reset button spot context 
                if (!buttonSpotFound)
                    controller.buttonContext = false;
                
                // if this spot has no special context at all, reset the context info in the user interface
                if (!exitFound && !buttonSpotFound) 
                    UserInterface.Instance.displayContextInfo("","");                
            }
        }
    }
}