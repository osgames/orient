/* ****************************************************
 * Name: PlaceOnSet.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/05/20
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Realizer.Orient.Entities;
using PAGE.Orient;
using PAGE.Orient.Domain.Assets;

namespace ION.Realizer.Orient.Actions.Camera
{
    public class PlaceOnSet : CameraAction
    {
        public const string PARAM_TARGET_STR = "target";
        public const string PARAM_SET_ID_STR = "set";

        protected override void OnStart(Context context)
        {
            if (context.Contains(PARAM_TARGET_STR))
            {
                Asset asset;
                if (Universe.Instance.TryGetEntity(context.Get<string>(PARAM_TARGET_STR), out asset))
                {
                    OrientGraphicsRealizer.Instance.Camera.Target = asset.GraphicalAsset;
                }
            }

            if (context.Contains(PARAM_SET_ID_STR) && 
                Universe.Instance.HasEntity<Set>(context.Get<string>(PARAM_SET_ID_STR)))
            {
                OrientSet set = Universe.Instance.GetEntity<Set>(context.Get<string>(PARAM_SET_ID_STR)).GraphicalAsset;
                OrientGraphicsRealizer.Instance.Camera.PlaceOnSetCamera(set);

                // placement ended
                this.End();
            }
            else
            {
                // no set, cannot place
                this.Fail();
            }
        }
    }
}