/* ****************************************************
 * Name: FocusCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/05/20
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Realizer.Orient.Entities;
using PAGE.Orient;
using PAGE.Orient.Domain.Assets;

namespace ION.Realizer.Orient.Actions.Camera
{
    public class FocusCharacter : FirstPersonCameraAction
    {
        public const string PARAM_CHARACTER_ID_STR = "character";
        public const string PARAM_TARGET_STR = "target";
        public const string TARGET_BODY = "body";
        public const string TARGET_FACE = "face";

        protected override void OnStart(Context context)
        {
            if (context.Contains(PARAM_CHARACTER_ID_STR) &&
                Universe.Instance.HasEntity<Character>(context.Get<string>(PARAM_CHARACTER_ID_STR)))
            {
                OrientCharacter character =
                    Universe.Instance.GetEntity<Character>(context.Get<string>(PARAM_CHARACTER_ID_STR)).GraphicalAsset;

                if (context.Contains(PARAM_TARGET_STR))
                {
                    if (context.Get<string>(PARAM_TARGET_STR) == TARGET_BODY)
                    {
                        OrientGraphicsRealizer.Instance.Camera.FocusCharacterBody(character);
                        return;
                    }
                    if (context.Get<string>(PARAM_TARGET_STR) == TARGET_FACE)
                    {
                        OrientGraphicsRealizer.Instance.Camera.FocusCharacterFace(character);
                        return;
                    }
                }
            }

            // no character or target, cannot place
            this.Fail();
        }

        protected override void OnStep(Context context)
        {
            // wait one ION step, so that camera could have updated itself by now
            updateUserEntityAngle();
            this.End(); // placement ended
        }


    }
}