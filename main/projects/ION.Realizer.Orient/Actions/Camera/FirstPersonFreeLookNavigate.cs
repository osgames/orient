/* ****************************************************
 * Name: FirstPersonFreeLookNavigate.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/05/20
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using PAGE.Orient;
using PAGE.Orient.Domain.Cameras;
using User.Interface;

namespace ION.Realizer.Orient.Actions.Camera
{
    public class FirstPersonFreeLookNavigate : FirstPersonCameraAction
    {
        protected override void OnStart(Context context)
        {
            OrientGraphicsRealizer.Instance.Camera.CameraMode = CameraMode.FirstPersonFreeLook;
            placeFirstPersonCam();
            updateUserEntity(OrientGraphicsRealizer.Instance.Camera.currentWayPoint);
            updateUserEntityAngle();
            bool keyboardCapture = true;
            bool mouseCapture = true;

            if (context.Contains("keyboard") &&
                (context.Get<string>("keyboard") == "false"))
                keyboardCapture = false;

            if (context.Contains("mouse") &&
                (context.Get<string>("mouse") == "false"))
                mouseCapture = false;

            OrientGraphicsRealizer.Instance.Camera.CaptureKeyboard = keyboardCapture;
            OrientGraphicsRealizer.Instance.Camera.CaptureMouse = mouseCapture;

            //follwing user entering a new scene, change the display system state on the 2D overlay
            UserInterface.Instance.updateSystemButtontoIdle();

            this.End();
        }

    }
}