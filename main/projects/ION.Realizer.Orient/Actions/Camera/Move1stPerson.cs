using System.Collections.Generic;
using System.Text;
using PAGE.Orient;
using ION.Core;
using ION.Realizer.Orient.Entities;
using ION.Realizer.Orient.Actions.UserController;
using PAGE.Orient.Domain.Cameras;
using PAGE.Generic.Domain.Spots;
using PAGE.OGRE.Math;
using Mogre;

namespace ION.Realizer.Orient.Actions.Camera
{
    class Move1stPerson : FirstPersonCameraAction
    {
        private class MoveToInformation
        {
            public GenericWaypoint WayPoint; // the waypoint that could be moved to
            public float Angle; //the angle that waypoint is off the current viewing direction
            public float Score; //the score that waypoint scored
            public OgreDirection Direction; //the direction from current position to the waypoint
        
            public MoveToInformation()
            {
                this.WayPoint = null;
                this.Angle = 0f;
                this.Score = 0f;
                this.Direction = null;
            }
        
        }
        
        
        protected override void OnStart(Context context)
        {
            if (context.Contains("direction"))
            {
                if ((OrientGraphicsRealizer.Instance.Camera.CameraMode == CameraMode.FirstPersonFreeLook) ||
                    (OrientGraphicsRealizer.Instance.Camera.CameraMode == CameraMode.FirstPersonNoFreeLook))
                {
                    moveFirstPersonCam((string)context["direction"]);
                }
            }
            this.End();
        }

        /// <summary>
        /// searches for a waypoint in the given direction and sets the first person camera to move to
        /// the closest match as a destination if any is found
        /// </summary>
        /// <param name="direction">the direction into which to move. identified by the constants in User.UserNavigationAction</param>
        /// <remarks>sets the orient camera's nextDestination variable to the new target waypoint. the camera is actually moved
        /// there in the respective FirstPerson algorithm. Also sets the newFacingAngle variable </remarks>
        public void moveFirstPersonCam(string direction)
        {     
            // get pointer to orient camera
            OrientCamera cam = OrientGraphicsRealizer.Instance.Camera;

            // move camera only if it is not already moving/animated
            if ((cam.nextDestination == null) && (cam.newFacingAngle == 0f))
            {
                OgreDirection viewingDirection = (OgreDirection)cam.AbsoluteDirection;

                if (direction.Equals(UserNavigationAction.DIR_F)) //forward
                {
                    OgreDirection desiredMovingDirection = viewingDirection;
                    MoveToInformation moveToInfo = findBestWayPoint(desiredMovingDirection);

                    if (moveToInfo.WayPoint!=null) // if a wp was found, move there (by setting the next destination
                    {
                        cam.nextDestination = moveToInfo.WayPoint; // set the destination
                        cam.newFacingAngle = moveToInfo.Angle; //set the angle that the camera needs to turn before facing the wp
                        updateUserEntity(cam.nextDestination); //update position of user entity
                        updateUserEntity(moveToInfo.Direction);
                        //check if new waypoint has special context for the user
                        checkSpotContext();
                    }
                }
                else if (direction.Equals(UserNavigationAction.DIR_B))
                {

                        OgreDirection desiredMovingDirection = cam.rotateVectorXZPlaneClockWise(viewingDirection, Math.PI);
                        MoveToInformation moveToInfo = findBestWayPoint(desiredMovingDirection);
                        if (moveToInfo.WayPoint != null) // if a wp was found, move there (by setting the next destination
                        {
                            cam.nextDestination = moveToInfo.WayPoint; // set the destination
                            cam.newFacingAngle = moveToInfo.Angle; //set the angle that the camera needs to turn before facing the wp
                            updateUserEntity(cam.nextDestination); //update position of user entity
                            updateUserEntity(cam.rotateVectorXZPlaneClockWise(moveToInfo.Direction,Math.PI));
                            //check if new waypoint has special context for the user
                            checkSpotContext();
                        }                        
                }
                else // movement command was not forward, in this case just turn camera into respective direction
                {
                    if (direction.Equals(UserNavigationAction.DIR_FR)) cam.newFacingAngle = Math.PI / 4.0f;
                    //if (direction.Equals(UserNavigationAction.DIR_R)) cam.newFacingAngle = Math.PI / 2.0f;
                    //if (direction.Equals(UserNavigationAction.DIR_BR)) cam.newFacingAngle = 3f * Math.PI / 4.0f;
                    //if (direction.Equals(UserNavigationAction.DIR_B)) cam.newFacingAngle = Math.PI;
                    //if (direction.Equals(UserNavigationAction.DIR_BL)) cam.newFacingAngle = -3 * Math.PI / 4.0f;
                    //if (direction.Equals(UserNavigationAction.DIR_L)) cam.newFacingAngle = -Math.PI / 2.0f;
                    if (direction.Equals(UserNavigationAction.DIR_FL)) cam.newFacingAngle = -Math.PI / 4.0f;

                    if (cam.newFacingAngle != 0f) // if there is a rotation update the orientation of the user dummy
                    {
                        updateUserEntity(cam.rotateVectorXZPlaneClockWise(viewingDirection, cam.newFacingAngle));
                    }
                }
            }
        }

        private MoveToInformation findBestWayPoint(OgreDirection desiredMovingDirection)
        {
            OrientCamera cam = OrientGraphicsRealizer.Instance.Camera;
            //score every waypoint with following algorithm: if waypoint direction is more than 25 degree off intended moving direction, discard this point,
            //if a point is not discarded it scores negative if its further away and the more the angle is off
            MoveToInformation bestWpInfo = new MoveToInformation();

            for (int i = 0; i < cam.currentSet.Waypoints.Count; i++)
            {
                GenericWaypoint wp = cam.currentSet.Waypoints[i];
                if ((wp != cam.currentWayPoint) && (wp.IsEmpty) && !(wp.ResourceSpot.Tags.Contains("non-user-spot")))
                {
                    OgreDirection vecToWp = (OgreDirection)cam.currentWayPoint.AbsolutePosition.DirectionTo(wp.AbsolutePosition);
                    float angle = (OgreAngle)vecToWp.AngleBetween(desiredMovingDirection);
                    if (angle <= 0.436) //if angle is between -25 and 25 degrees this is a possible target destination
                    {
                        // calculate the destinations score (angle*30) + distance
                        float dist = (float)(OgreDistance)cam.currentWayPoint.AbsolutePosition.DistanceTo(wp.AbsolutePosition);
                        float score = angle * 30.0f + dist;

                        // if there was no waypoint at all yet or current score is better than best score, remember score and waypoint
                        if ((score < bestWpInfo.Score) || (bestWpInfo.WayPoint == null))
                        {
                            bestWpInfo.Score = score;
                            bestWpInfo.WayPoint = wp;
                            bestWpInfo.Direction = vecToWp;

                            // calculate orientation of angle
                            Vector3 vecY = new Vector3(0, 1, 0);
                            if (vecY.DotProduct(vecToWp.Vector3.CrossProduct(desiredMovingDirection.Vector3)) < 0)
                                angle *= -1;
                            bestWpInfo.Angle = angle;

                        }
                    }
                }
            }
            return bestWpInfo;
        }
            
    
    }
}
