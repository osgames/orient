using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using PAGE.Orient;
using PAGE.Orient.Domain.Cameras;
using PAGE.OGRE.Math;

namespace ION.Realizer.Orient.Actions.Camera
{
    class Shake : FirstPersonCameraAction
    {
        private const int shakingDuration = 9500;
        private const int endShakeDuration = 3000;
        private const double ampFactorNormalShake = 0.7;
        private const double ampFactorEndShake = 2.5;
        private DateTime startTime;
        private OrientCamera cam;
        private Random r = new Random();

        protected override void OnStart(Context context)
        {
            cam = OrientGraphicsRealizer.Instance.Camera;
            startTime = DateTime.Now;
            //start shaking
            performShake(ampFactorNormalShake);
        }

        protected override void OnStep(Context context)
        {
            TimeSpan timePassed = DateTime.Now.TimeOfDay - startTime.TimeOfDay;
            if (timePassed.TotalMilliseconds > shakingDuration) // shaking is over
                this.End();
            else if (timePassed.TotalMilliseconds > (shakingDuration - endShakeDuration)) // end phase (more intense shaking)
                performShake(ampFactorEndShake);
            else // normal shaking phase (before end)
                performShake(ampFactorNormalShake);
        }

        //move the camera by a random vector (max 1,1,1) multiplied by the ampfactor
        private void performShake(double ampFactor) 
        { 
            double angle = (r.NextDouble()-0.5) * ampFactor;
            cam.Yaw(angle);
            angle = (r.NextDouble() - 0.5) * ampFactor;
            cam.Pitch(angle);
        }

        protected override void OnEnd(Context context)
        {
            // reset camera y direction
            OgreDirection camDir = (OgreDirection) cam.AbsoluteDirection;
            camDir.Y = 0;
            camDir = cam.rotateVectorXZPlaneClockWise(camDir, -(float) Math.PI/2.0f);
            cam.AbsoluteDirection = camDir;
            updateUserEntityAngle();
        }


    }
}
