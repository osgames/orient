/* ****************************************************
 * Name: FocusAsset.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/05/20
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Realizer.Orient.Entities;
using PAGE.Orient;
using PAGE.Orient.Domain.Cameras;

namespace ION.Realizer.Orient.Actions.Camera
{
    public class FocusAsset : FirstPersonCameraAction
    {
        protected override void OnStart(Context context)
        {
            if (context.Contains("target"))
            {
                Asset asset;
                if (Universe.Instance.TryGetEntity(context.Get<string>("target"), out asset))
                {
                    OrientGraphicsRealizer.Instance.Camera.FocusAsset(asset.GraphicalAsset);
                }
            }
            else
            {
                // no target, cannot place
                this.Fail();
            }
        }

        protected override void OnStep(Context context)
        {
            // wait one ION step, so that camera could have updated itself by now
            updateUserEntityAngle();
            this.End(); // placement ended
        }
    }
}