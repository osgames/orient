/* ****************************************************
 * Name: FreeNavigate.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/05/20
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using PAGE.Orient;
using PAGE.Orient.Domain.Cameras;

namespace ION.Realizer.Orient.Actions.Camera
{
    public class FreeNavigate : CameraAction
    {
        protected override void OnStart(Context context)
        {
            OrientGraphicsRealizer.Instance.Camera.CameraMode = CameraMode.Free;
            bool keyboardCapture = true;
            bool mouseCapture = true;

            if (context.Contains("keyboard") &&
                (context.Get<string>("keyboard") == "false"))
                keyboardCapture = false;

            if (context.Contains("mouse") &&
                (context.Get<string>("mouse") == "false"))
                mouseCapture = false;

            OrientGraphicsRealizer.Instance.Camera.CaptureKeyboard = keyboardCapture;
            OrientGraphicsRealizer.Instance.Camera.CaptureMouse = mouseCapture;

            this.End();
        }
    }
}