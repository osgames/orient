/* ****************************************************
 * Name: CameraAction.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/05/20
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using PAGE.Orient;
using ION.Realizer.Orient.Entities;
using PAGE.Orient.Domain.Cameras;
using PAGE.OGRE.Math;
using Mogre;

namespace ION.Realizer.Orient.Actions.Camera
{
    public abstract class CameraAction : Action
    {
        public Entities.Camera Camera
        {
            get { return (Entities.Camera)this.Parent; }
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }

        protected override void OnStart(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnStep(Context context)
        {
        }
        
   
    }
}