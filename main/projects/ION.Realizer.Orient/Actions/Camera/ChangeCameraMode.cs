/* ****************************************************
 * Name: ChangeCameraMode.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/05/20
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using PAGE.Orient;
using PAGE.Orient.Domain.Cameras;

namespace ION.Realizer.Orient.Actions.Camera
{
    public class ChangeCameraMode : CameraAction
    {
        protected override void OnStart(Context context)
        {
            if (context.Contains("mode"))
            {
                string mode = context.Get<string>("mode");
                switch (mode)
                {
                    case "static":
                        OrientGraphicsRealizer.Instance.Camera.CameraMode = CameraMode.Static;
                        break;
                    case "dynamic-targeted":
                        OrientGraphicsRealizer.Instance.Camera.CameraMode = CameraMode.DynamicTargeted;
                        break;
                    case "static-targeted":
                        OrientGraphicsRealizer.Instance.Camera.CameraMode = CameraMode.StaticTargeted;
                        break;
                    case "free":
                        OrientGraphicsRealizer.Instance.Camera.CameraMode = CameraMode.Free;
                        break;
                    default:
                        this.Fail();
                        return;
                }
                this.End();
            }
            else
            {
                this.Fail();
            }
        }
    }
}