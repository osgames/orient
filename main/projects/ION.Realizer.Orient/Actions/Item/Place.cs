/* ****************************************************
 * Name: Place.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/08/28
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Realizer.Orient.Entities;
using PAGE.Generic.Domain.Assets;

namespace ION.Realizer.Orient.Actions.Item
{
    public class Place : ItemAction
    {
        public const string PARAM_TARGET_ASSET_ID_STR = "target";
        public const string PARAM_SPOT_TAG_STR = "spot";
        public const string PARAM_FADE_STR = "fade";
        public const string DO_NOT_FADE_STR = "false";

        protected override void OnStart(Context context)
        {
            base.OnStart(context);

            //checks for target asset where to place the item
            string assetID = context.Contains(PARAM_TARGET_ASSET_ID_STR) ?
                context.Get<string>(PARAM_TARGET_ASSET_ID_STR) : null;
            if ((assetID != null) && Universe.Instance.HasEntity<Asset>(assetID))
            {
                Asset asset = Universe.Instance.GetEntity<Asset>(assetID);
                GenericAsset graphicalAsset = asset.GraphicalAsset;

                //associates item with set
                if (asset is Set)
                    this.Item.Set = asset as Set;
                else if (asset is Entities.Item)
                    this.Item.Set = ((Entities.Item) asset).Set;

                //places item in spot
                string spotTag = context.Contains(PARAM_SPOT_TAG_STR) ?
                    context.Get<string>(PARAM_SPOT_TAG_STR) : null;
                if (spotTag != null)
                {
                    this.Item.GraphicalAsset.PlaceRandomly(null, graphicalAsset, spotTag, true);
                }
                else
                {
                    this.Item.GraphicalAsset.PlaceRandomly(null, graphicalAsset, true);
                }

                //fades in item
                if (!context.Contains(PARAM_FADE_STR) ||
                    (context.Get<string>(PARAM_FADE_STR) != DO_NOT_FADE_STR))
                    this.Item.GraphicalAsset.FadeIn(Constants.FADE_TIME);


                // for testing
                // this.Item.GraphicalAsset.ShowAllDebuggingSpots();

                // placement ended
                this.End();
            }
            else
            {
                // no asset, cannot place
                this.Fail();
            }
        }
    }
}