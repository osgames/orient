/* ****************************************************
 * Name: Unplace.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/08/28
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Realizer.Orient.Util;

namespace ION.Realizer.Orient.Actions.Item
{
    public class Unplace : ItemAction
    {
        public const string PARAM_FADE_STR = "fade";
        public const string DO_NOT_FADE_STR = "false";

        protected override void OnStart(Context context)
        {
            base.OnStart(context);

            //fades out item and unplaces from current place
            if (!context.Contains(PARAM_FADE_STR) || 
                (context.Get<string>(PARAM_FADE_STR) != DO_NOT_FADE_STR))
                this.Item.GraphicalAsset.FadeOut(Constants.FADE_TIME);
            
            this.Item.GraphicalAsset.Unplace();

            // unplacement ended
            this.End();
        }
    }
}