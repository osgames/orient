/* ****************************************************
 * Name: ItemAction.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/08/28
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;

namespace ION.Realizer.Orient.Actions.Item
{
    public class ItemAction : Action
    {
        public Entities.Item Item
        {
            get { return (Entities.Item)this.Parent; }
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }

        protected override void OnStart(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnStep(Context context)
        {
        }
    }
}