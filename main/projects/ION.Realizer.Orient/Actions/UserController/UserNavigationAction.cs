using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;

namespace ION.Realizer.Orient.Actions.UserController
{
    public class UserNavigationAction : Action
    {

        #region Constants
        public const string ACTION_NAME = "UserNavigationAction";
        public const string DIRECTION = "Direction";
        //Direction constants: F - forwards, B - backwards, L - left, R - right
        public const string DIR_F = "F";
        public const string DIR_FR = "FR";
        public const string DIR_R = "R";
        public const string DIR_BR = "BR";
        public const string DIR_B = "B";
        public const string DIR_BL = "BL";
        public const string DIR_L = "L";
        public const string DIR_FL = "FL";
        public const string START_BTN = "START";
        public const string SELECT_BTN = "SELECT";


        #endregion

        protected override void OnStart(Context context)
        {
            if (!((ION.Realizer.Orient.Entities.UserController)this.Parent).trainingMode)
            {   // normal mode
                // check here only for start button (camera controller takes care of direction buttons)
                if (context[DIRECTION].Equals(START_BTN))
                {
                    //start button can be used to start a leave scene action
                    //check here to see if leave scene context is set
                    string destination = ((ION.Realizer.Orient.Entities.UserController)this.Parent).leaveSceneDestinationContext;
                    if (!destination.Equals(""))
                    {
                        Action leaveSceneAction = Universe.Instance.GetEntity("user").GetAction("leavescene");
                        Dictionary<string, object> arguments = new Dictionary<string, object>();
                        arguments.Add("target", destination); //action parameters
                        if (!leaveSceneAction.IsRunning) leaveSceneAction.Start(new Arguments(arguments));
                        UserInterface.Instance.displayMessage("You leave the scene","Ihr verlasst die Szene", 3000d);
                        ((ION.Realizer.Orient.Entities.UserController)this.Parent).clearContext();
                        ((ION.Realizer.Orient.Entities.UserController)this.Parent).leaveSceneDestinationContext = "";
                        UserInterface.Instance.displayContextInfo("","");
                    }
                }
            }
            else
            {   // training mode 
                if (context[DIRECTION].Equals(START_BTN))
                    UserInterface.Instance.displayMessage("LINPad start","LINPad start",1000f);
                if (context[DIRECTION].Equals(DIR_F))
                    UserInterface.Instance.displayMessage("LINPad go forward","LINPad vorw�rts", 1000f);
                if (context[DIRECTION].Equals(DIR_FR))
                    UserInterface.Instance.displayMessage("LINPad turn right","LINPad rechts", 1000f);
                if (context[DIRECTION].Equals(DIR_FL))
                    UserInterface.Instance.displayMessage("LINPad turn left","LINPad links", 1000f);
                if (context[DIRECTION].Equals(DIR_B))
                    UserInterface.Instance.displayMessage("LINPad step back","LINPad zur�ck", 1000f);            
            }
        }

        protected override void OnStep(Context context)
        {
            this.End();
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }


    }
}
