using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;

namespace ION.Realizer.Orient.Actions.UserController
{
    public class UserOptionAction: Action
    {
        #region Constants
        public const string ACTION_NAME = "UserOptionAction";
        public const string CHOICE = "Choice";
        #endregion

        protected override void OnStart(Context context)
        {
        }

        protected override void OnStep(Context context)
        {
            this.End();
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
