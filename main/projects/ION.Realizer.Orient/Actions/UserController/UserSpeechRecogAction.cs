using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;
using ION.Realizer.Orient.Entities;

namespace ION.Realizer.Orient.Actions.UserController
{
    public class UserSpeechRecogAction : Action
    {
        #region Constants
        public const string ACTION_NAME = "UserSpeechRecogAction";
        public const string CHAR_NAME_ARRAY = "CharacterNameArray";
        public const string CHAR_NAME = "CharacterName";
        #endregion

        private ION.Realizer.Orient.Entities.UserController userController;

        private class CharacterSelectorFilter : IFilter<Entity>
        {

            #region IFilter<Entity> Members

            public bool Blocks(Entity obj)
            {
                // block every entity that is not a character
                if (!(obj is Character)) return true;
                // block invisible characters
                if (!(obj as Character).GraphicalAsset.Visible) return true;
                // filter out already selected character
                if (UserInterface.Instance.getCharacterContext().Equals(obj.Name)) return true;
                return false;
            }

            #endregion
        }


        protected override void OnStart(Context context)
        {
            if (!userController.trainingMode)
            {   // normal operation mode
                string [] charNameArray = (string [])context[CHAR_NAME_ARRAY];
                
                // find the first entry in the character array that is a) a character that is present in the scene
                // and b) a character who is not already selected

                bool found = false;
                uint counter = 0;
                string charName = "";

                while (!found && charNameArray.Length > counter)
                {
                    if (Universe.Instance.HasEntity<Character>(charNameArray[counter]))                    
                        if (Universe.Instance.GetEntity<Character>(charNameArray[counter]).GraphicalAsset.Visible)
                            if (!UserInterface.Instance.getCharacterContext().Equals(charNameArray[counter]))
                            {
                                charName = charNameArray[counter];
                                found = true;
                                break;
                            }                        

                    counter++;
                }

                // if found is false at this point we have to select a character randomly

                if (!found)
                {
                    // returns a list of all characters currently in the scene that are not selected
                    IFilterable<Entity> characters = Universe.Instance.Entities.FilteredBy(new CharacterSelectorFilter());
                    
                    // there is more than one character left to select
                    if (characters.Count > 0)
                    {
                        IEnumerator<Entity> enumerator = characters.GetEnumerator(); 
                        Random ran = new Random();
                        int ranNumber = ran.Next(characters.Count);
                        while (ranNumber >= 0 )
                        {
                            enumerator.MoveNext();
                            ranNumber--;
                        }
                        charName = enumerator.Current.Name;
                        found = true;
                    }
                }


                // if still nothing was found, there is no character (or only one here, ignore the request)
                if (found)
                {
                    userController.charNameContext = charName;
                    context[CHAR_NAME] = charName;
                    //follwing the new value of the inputContext variable, change the display system state on the 2D overlay
                    UserInterface.Instance.updateSystemButtontoIdle();
                    UserInterface.Instance.updateCharacterContext(charName);
                }   else
                {
                    this.Fail();
                }
            }
            else
            { 
                //training mode
                string charName = ((string [])context[CHAR_NAME_ARRAY])[0];
                userController.charNameContext = charName;
                //follwing the new value of the inputContext variable, change the display system state on the 2D overlay
                UserInterface.Instance.updateSystemButtontoIdle();
                UserInterface.Instance.updateCharacterContext(charName);
            }
        }

        protected override void OnStep(Context context)
        {
            this.End();
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
            userController = (ION.Realizer.Orient.Entities.UserController)this.Parent;
        }

        protected override void OnDestroy()
        {
        }
    }
}
