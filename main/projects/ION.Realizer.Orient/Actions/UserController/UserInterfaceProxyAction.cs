//This Action monitors a server socket which accepts user interface devices input
//when a Wii gesture, DanceMat or other input is detected, a corresponding action is created

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using ION.Core.Extensions;
using System.Xml;


namespace ION.Realizer.Orient.Actions.UserController
{
    public class UserInterfaceProxyAction : Action
    {
        private ION.Realizer.Orient.Entities.UserController proxy;
        public const string ACTION_NAME = "UserInterfaceProxyAction";


        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnStart(Context context)
        {
            this.proxy = (ION.Realizer.Orient.Entities.UserController)this.Parent;            
        }

        protected override void OnStep(Context context)
        {
            //this.proxy.ConnectToSlave();
            //Console.WriteLine("OnStep Connect");
            
            if (this.proxy.master.isReadyToRead)
            {
               //read from Gesture Socket Server
                String inputXML = this.proxy.master.input;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(inputXML.Substring(0,inputXML.Length-5));
               
                //check which device was used
                XmlElement root = doc.DocumentElement;
                XmlNodeList elemList = root.GetElementsByTagName("device");
                IEnumerator ienum = elemList.GetEnumerator();
                string device = "";
                while (ienum.MoveNext())
                {
                    XmlNode value = (XmlNode)ienum.Current;
                    device = value.InnerText;
                }

                if (device.Equals("Wii")) processWiiGesture(root);
                else
                if (device.Equals("DancePad")) processDanceMat(root);
                else
                if (device.Equals("MobilePhone")) processMobilePhone(root);           
                
                this.proxy.master.isReadyToRead=false;
            }

        }


        protected override void OnEnd(Context context)
        {
            //throw new Exception("The method or operation is not implemented.");
        }

        protected override void OnFail(Context context)
        {
            throw new Exception("It was not possible to load the User Interface Proxy Action");

        }

        protected override void OnDestroy()
        {
        }

        private void processDanceMat(XmlElement root)
        {
            XmlNodeList elemList = root.GetElementsByTagName("value");
            IEnumerator ienum = elemList.GetEnumerator();
            if (ienum.MoveNext())
            {
                XmlNode value = (XmlNode)ienum.Current;
                string val = value.InnerText;

                // map value from server onto correct direction string
                string direction = "";
                if (val.Equals("Up")) direction = UserNavigationAction.DIR_F;
                else
                    if (val.Equals("Down")) direction = UserNavigationAction.DIR_B;
                    else
                        if (val.Equals("Left")) direction = UserNavigationAction.DIR_L;
                        else
                            if (val.Equals("Right")) direction = UserNavigationAction.DIR_R;
                            else
                                if (val.Equals("RightUp") || val.Equals("Circle")) direction = UserNavigationAction.DIR_FR;
                                else
                                    if (val.Equals("RightDown") || val.Equals("Square")) direction = UserNavigationAction.DIR_BR;
                                    else
                                        if (val.Equals("LeftDown") || val.Equals("Triangle")) direction = UserNavigationAction.DIR_BL;
                                        else
                                            if (val.Equals("LeftUp") || val.Equals("Cross")) direction = UserNavigationAction.DIR_FL;
                                            else
                                                if (val.Equals("Start")) direction = UserNavigationAction.START_BTN;
                                                else
                                                    if (val.Equals("Select")) direction = UserNavigationAction.SELECT_BTN;




                if (!direction.Equals(""))
                {
                    //Start the Action for this dance mat movement
                    UserNavigationAction userNavigationAction = proxy.GetAction<UserNavigationAction>(UserNavigationAction.ACTION_NAME);
                    if (!userNavigationAction.IsRunning)
                    {
                        Dictionary<string, object> arguments = new Dictionary<string, object>();
                        arguments.Add(UserNavigationAction.DIRECTION, direction);
                        userNavigationAction.Start(new Arguments(arguments));
                    }
                }
            
            }
        }

        private void processWiiGesture(XmlElement root)
        {
            /*XmlNodeList elemList = root.GetElementsByTagName("value");
            IEnumerator ienum = elemList.GetEnumerator();
            double mostProbable = 0;
            String mostProbableGesture = null;
            while (ienum.MoveNext())
            {
                XmlNode value = (XmlNode)ienum.Current;
                //ApplicationLogger.Instance().WriteLine("value from UserGesture socket: " + value.InnerText);

                XmlAttributeCollection attributes = value.Attributes;

                XmlAttribute probabilityAttr = attributes["probability"];
                // changed by Meiyii and Michael 
                if (probabilityAttr != null)
                {
                    String probString = probabilityAttr.InnerXml;
                    double probability = System.Convert.ToDouble(probString);
                    //ApplicationLogger.Instance().WriteLine("probability: " + probString);

                    if (probability > mostProbable)
                    {
                        mostProbable = probability;
                        mostProbableGesture = value.InnerText;
                    }
                }
                else
                {
                    mostProbableGesture = value.InnerText;
                }
            }*/

            XmlNodeList typeList = root.GetElementsByTagName("type");
            IEnumerator ienum = typeList.GetEnumerator();
            string typeval = "";

            if (ienum.MoveNext()) 
            {
                XmlNode type = (XmlNode)ienum.Current;
                typeval = type.InnerText;
            }

            XmlNodeList elemList = root.GetElementsByTagName("value");
            ienum = elemList.GetEnumerator();

            if(typeval.Equals("choice")) //A, B, -, Home, +, 1, 2, Up, Down, Left, Right
            {
                string choice = "";
                if (ienum.MoveNext())
                {
                    XmlNode value = (XmlNode)ienum.Current;
                    choice = value.InnerText;
                }

                if (!choice.Equals(""))
                {
                    //Start the Action for this gesture
                    UserWiiButtonAction userWiiButtonAction = proxy.GetAction<UserWiiButtonAction>(UserWiiButtonAction.ACTION_NAME);
                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                    arguments.Add(UserWiiButtonAction.BUTTON_NAME, choice); //name of button
                    userWiiButtonAction.Start(new Arguments(arguments));
                }

            }
            else if (typeval.Equals("gesture")) 
            {
                string gesture = "";
                if (ienum.MoveNext())
                {
                    XmlNode value = (XmlNode)ienum.Current;
                    gesture = value.InnerText;
                }

                if (!gesture.Equals(""))
                {
                    //Start the Action for this gesture
                    UserGestureAction userGestureAction = proxy.GetAction<UserGestureAction>(UserGestureAction.ACTION_NAME);
                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                    arguments.Add(UserGestureAction.GESTURE_NAME, gesture);
                    userGestureAction.Start(new Arguments(arguments)); 
                }
            }            
        }

        /* Processing the input from the mobile phone which can be keyboard entry, 
         * choice selection, item selection using RFID or speech (magic words of character names)
         * by Meiyii 09/09/08
         */
        private void processMobilePhone(XmlElement root)
        {
            XmlNodeList typeList = root.GetElementsByTagName("type");
            IEnumerator ienum = typeList.GetEnumerator();
            string typeval = "";

            if (ienum.MoveNext())
            {
                XmlNode type = (XmlNode)ienum.Current;
                typeval = type.InnerText;
            }

            XmlNodeList elemList = root.GetElementsByTagName("value");
            ienum = elemList.GetEnumerator();

            if (typeval == "RFID")
            {
                string item = "";
                if (ienum.MoveNext())
                {
                    XmlNode value = (XmlNode)ienum.Current;
                    item = value.InnerText;
                }

                if (!item.Equals(""))
                {
                    UserSelectItemAction userSelectItemAction = proxy.GetAction<UserSelectItemAction>(UserSelectItemAction.ACTION_NAME);
                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                    arguments.Add(UserSelectItemAction.ITEM_NAME, item);
                    userSelectItemAction.Start(new Arguments(arguments));
                }
            }
            else if (typeval == "choice") //start, back
            {
                string choice = "";
                if (ienum.MoveNext())
                {
                    XmlNode value = (XmlNode)ienum.Current;
                    choice = value.InnerText;
                }

                if (!choice.Equals(""))
                {
                    UserMobilePhoneButtonAction userMobilePhoneButtonAction = proxy.GetAction<UserMobilePhoneButtonAction>(UserMobilePhoneButtonAction.ACTION_NAME);
                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                    arguments.Add(UserMobilePhoneButtonAction.BUTTON_NAME, choice);
                    userMobilePhoneButtonAction.Start(new Arguments(arguments));
                }
            }
            else if (typeval == "Audio")
            {
                string [] character = null;
                if (ienum.MoveNext())
                {
                    XmlNode value = (XmlNode)ienum.Current;
                    string val = value.InnerText;
                    
                    character = val.Split(' ');
                }

                if (!character[0].Equals(""))
                {
                    UserSpeechRecogAction userSpeechRecogAction = proxy.GetAction<UserSpeechRecogAction>(UserSpeechRecogAction.ACTION_NAME);
                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                    arguments.Add(UserSpeechRecogAction.CHAR_NAME_ARRAY, character);
                    arguments.Add(UserSpeechRecogAction.CHAR_NAME, character[0]);
                    userSpeechRecogAction.Start(new Arguments(arguments));
                }
            }
        }

    }
}
