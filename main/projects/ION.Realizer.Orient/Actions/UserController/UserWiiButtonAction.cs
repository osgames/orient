using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using ION.Core;
using User.Interface;
using ION.Realizer.Orient.Entities;
using PAGE.OGRE.Math;
using PAGE.Orient;
using ION.Realizer.Orient.Actions.User;

namespace ION.Realizer.Orient.Actions.UserController
{
    public class UserWiiButtonAction : Action
    {
        #region Constants
        public const string ACTION_NAME = "UserWiiButtonAction";
        public const string BUTTON_NAME = "Button";
        public const string BUTTON_A = "A";
        public const string BUTTON_B = "B";
        public const string BUTTON_MINUS = "Minus";
        public const string BUTTON_HOME = "Home";
        public const string BUTTON_PLUS = "Plus";
        public const string BUTTON_1 = "One";
        public const string BUTTON_2 = "Two";
        public const string BUTTON_UP = "Up";
        public const string BUTTON_DOWN = "Down";
        public const string BUTTON_LEFT = "Left";
        public const string BUTTON_RIGHT = "Right";
        #endregion

        private ION.Realizer.Orient.Entities.UserController userController;

        protected override void OnStart(Context context)
        {         
            string buttonName = (string) context[BUTTON_NAME];

            if (!userController.trainingMode)
            {
                if (buttonName.Equals(BUTTON_A)) // start gesture on button A
                {
                    if (Universe.Instance.HasEntity<UserEntity>(UserEntity.ENTITY_NAME))
                    {
                        // button A was pressed, an action should be started
                        if (userController.gestureContext.Equals(UserGestureAction.GESTURE_GREETBACK)) startGreet();
                        else if (userController.gestureContext.Equals(UserGestureAction.GESTURE_EATDRINK)) startEatDrink();
                        else if (userController.gestureContext.Equals(UserGestureAction.GESTURE_USERPICK)) startUserPick();
                        else if (userController.gestureContext.Equals(UserGestureAction.GESTURE_ACCEPT)) startAccept();
                        else if (userController.gestureContext.Equals(UserGestureAction.GESTURE_REJECT)) startReject();
                        else if (userController.gestureContext.Equals(UserGestureAction.GESTURE_ASK)) startAsk();
                        else if (userController.gestureContext.Equals(UserGestureAction.GESTURE_APOLOGISE)) startApologise();
                        else if (userController.gestureContext.Equals(UserGestureAction.GESTURE_GIVE)) startGive();
                        else if (userController.gestureContext.Equals("heal")) startHeal();
                        //follwing the new value of the inputContext variable, change the display system state on the 2D overlay
                        UserInterface.Instance.updateSystemButtontoIdle();
                    }
                    else UserInterface.Instance.displayMessage("You cannot perform any action now",
                                                               "Ihr k�nnt gerade keine Aktion ausf�hren", 3000f);
                }
                else if (buttonName.Equals(BUTTON_B))
                {
                    //follwing the new value of the inputContext variable, change the display system state on the 2D overlay
                    UserInterface.Instance.updateSystemButtontoBusy();
                }
                else if (buttonName.Equals(BUTTON_1) || buttonName.Equals(BUTTON_2))
                {
                    if (userController.buttonContext) // user is in spot where the machine buttons can be pressed
                        startButtonPress(buttonName);
                }
                else if (buttonName.Equals(BUTTON_PLUS))
                {
                    // show hide character names
                    OrientGraphicsRealizer.Instance.ToggleCharacterNames();
                }
            }
            else
            { 
                //training mode
                if (!buttonName.Equals(BUTTON_B))
                    UserInterface.Instance.displayMessage("WiiMote Button","WiiMote Knopf", 1000f);
            }
        }

        //start a greet action
        private void startGreet()
        {
            string target = userController.charNameContext;
            // check if characterContext is not empty and character exists in scene
            if (target.Equals(""))
            {
                UserInterface.Instance.displayMessage("Please select a character to greet first",
                                                      "Bitte w�hlt eine Person zum Gr�ssen aus",3000d);  
            }
            else if (Universe.Instance.HasEntity(target))
            {
                //later here check for distance
                Action greetAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionGreet.ACTION_NAME);
                Dictionary<string, object> arguments = new Dictionary<string, object>();
                arguments.Add("target", target); //action parameters
                if (!greetAction.IsRunning) greetAction.Start(new Arguments(arguments));
                userController.clearContext();            
            } 
            else UserInterface.Instance.displayMessage(target + " is not available",
                                                       target + " ist nicht verf�gbar", 3000d);          
        }

        //start an eat or drink action
        private void startEatDrink()
        {
            string target = userController.itemContext;
            UserEntity user = Universe.Instance.GetEntity<UserEntity>("user");
           
            // if the user is holding nothing
            if (target.Equals(""))
            {
                UserInterface.Instance.displayMessage("Please select an item to consume",
                                                      "Bitte w�hlt ein Objekt zum Konsumieren aus", 3000d);
            }
            else 
            {
                if (user.inventoryItemExists(target))
                {
                    // get item name
                    string itemName = user.getItemName(target);
                    if (user.getItemEatable(itemName))
                    {
                        Action eatAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionEat.ACTION_NAME);
                        Dictionary<string, object> arguments = new Dictionary<string, object>();
                        arguments.Add("target", itemName); //action parameters
                        if (!eatAction.IsRunning) eatAction.Start(new Arguments(arguments));
                        UserInterface.Instance.displayMessage("You eat " + articelize(target),
                                                              "Ihr esst " + Translations.Instance.translateItem_deWithIndefArticle(target, 4), 3000f);
                        userController.clearContext();
                    }
                    else if (user.getItemDrinkable(itemName))
                    {
                        Action drinkAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionDrink.ACTION_NAME);
                        Dictionary<string, object> arguments = new Dictionary<string, object>();
                        arguments.Add("target", itemName); //action parameters
                        if (!drinkAction.IsRunning) drinkAction.Start(new Arguments(arguments));
                        UserInterface.Instance.displayMessage("You drink " + articelize(target),
                                                              "Ihr trinkt " + Translations.Instance.translateItem_deWithIndefArticle(target, 4), 3000f);
                        userController.clearContext();
                    }
                    else UserInterface.Instance.displayMessage("You can't consume " + articelize(target),
                                                          "Ihr k�nnt " + Translations.Instance.translateItem_deWithDefArticle(target, 4) + " nicht konsumieren", 3000d);
                } 
                else UserInterface.Instance.displayMessage("You don't have " + articelize(target),
                                                      "Ihr habt " + Translations.Instance.translateItem_deWithNegArticle(target, 4), 3000d);

            }
        }

        //start a user pick action
        private void startUserPick()
        {                        
            string target = userController.itemContext;
            UserEntity user = Universe.Instance.GetEntity<UserEntity>("user");
            // check if characterContext is not empty and character exists in scene
            if (target.Equals(""))
            {
                UserInterface.Instance.displayMessage("Please select an item first",
                                                      "Bitte w�hlt zuerst ein Objekt aus", 3000d);
            }
            else 
            {
                Entity item = getPickItemEntity(target);

                // check if item exists
                if (item == null)
                {
                    UserInterface.Instance.displayMessage("There is no " + target + " here",
                                                          "Hier gibt es " + Translations.Instance.translateItem_deWithNegArticle(target, 4), 3000d);
                }
                else
                {
                    if (user.inventoryItemExists(target)) // user already has an item of this type
                    {
                        UserInterface.Instance.displayMessage("You already have " + articelize(target),
                                                              "Ihr habt bereits " + Translations.Instance.translateItem_deWithIndefArticle(target, 4), 3000d);
                    }
                    else // user does not have an item of this type yet
                    {
                        Entities.Item itemEntity = (Entities.Item)item;
                        if (itemEntity.ChosenForPicking)
                        {
                            UserInterface.Instance.displayMessage("Choose another " + target + ". A Spryte is about to pick this one up.",
                                                                  Translations.Instance.translateItem_deWithDefArticle(target, 1) + " ist bereits f�r ein Spryte reserviert", 3000d);
                        }
                        else
                        {
                            if (target.Equals("soil"))
                                UserInterface.Instance.displayMessage("You are not allowed to pick up the soil",
                                                                      "Ihr d�rft die Erde nicht nehmen", 3000f);
                            else if (target.Equals("greenDrink"))
                                UserInterface.Instance.displayMessage("You are not allowed to pick up the green drink",
                                                                      "Ihr d�rft das Getr�nk nicht nehmen",3000f);
                            else
                            {
                                if (item.HasProperty<Property<string>>("onGround") && (item.GetProperty<Property<string>>("onGround").Value.Equals("True")))
                                {
                                    Action userPickFromGroundAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionUserPickFromGround.ACTION_NAME);
                                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                                    arguments.Add("target", item.Name); //action parameters
                                    if (!userPickFromGroundAction.IsRunning) userPickFromGroundAction.Start(new Arguments(arguments));
                                    UserInterface.Instance.displayMessage("You pick " + articelize(target) + " from the ground",
                                                                          "Ihr nehmt " + Translations.Instance.translateItem_deWithIndefArticle(target, 4) + " vom Boden", 3000f);
                                    userController.clearContext();
                                }
                                else if (item.HasProperty<Property<string>>("onTree") && (item.GetProperty<Property<string>>("onTree").Value.Equals("True")))
                                {
                                    Action userPickFromTreeAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionUserPickFromTree.ACTION_NAME);
                                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                                    arguments.Add("target", item.Name); //action parameters
                                    if (!userPickFromTreeAction.IsRunning) userPickFromTreeAction.Start(new Arguments(arguments));
                                    UserInterface.Instance.displayMessage("You pick " + articelize(target) + " from the tree",
                                                                          "Ihr nehmt " + Translations.Instance.translateItem_deWithIndefArticle(target, 4) + " vom Baum", 3000f);
                                    userController.clearContext();
                                }
                            }
                        }
                    }
                }
            }
        }

        //start a user accept action
        private void startAccept()
        {
            string target = userController.charNameContext;
            string offer = userController.itemContext;
            // check if characterContext is not empty and character exists in scene
            if (target.Equals(""))
            {
                UserInterface.Instance.displayMessage("Please select a character first",
                                                      "Bitte w�hlt eine Person aus",3000d);
            }
            else if (Universe.Instance.HasEntity(target))
            {
                if (offer.Equals(""))
                {
                    UserInterface.Instance.displayMessage("Select an offer to accept",
                                                          "W�hlt etwas zum Zustimmen aus",3000d);
                }
                else 
                {
                    Entity item = null;
                    if (Universe.Instance.HasEntity(offer))
                    {
                        item = Universe.Instance.GetEntity(offer);
                    }
                    else
                    {
                        item = getAcceptItemEntity(target, offer);
                    }

                    if (item == null)
                    {
                        UserInterface.Instance.displayMessage("There is no such offer",
                                                              Translations.Instance.translateItem_deWithDefArticle(target, 3) + " kann nicht zugestimmt werden", 3000d);
                    }
                    else
                    {
                        if (item.HasProperty<Property<string>>("isActivity") && (item.GetProperty<Property<string>>("isActivity").Value.Equals("True")))
                        {
                            Action acceptInvitationAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionAcceptInvitation.ACTION_NAME);
                            Dictionary<string, object> arguments = new Dictionary<string, object>();
                            arguments.Add("target", target); //action parameters
                            arguments.Add("parameters", item.Name);
                            if (!acceptInvitationAction.IsRunning) acceptInvitationAction.Start(new Arguments(arguments));
                            userController.clearContext();
                        }
                        else if (item.HasProperty<Property<string>>("eatable") && (item.GetProperty<Property<string>>("eatable").Value.Equals("True")))
                        {
                            Action acceptFoodAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionAcceptFood.ACTION_NAME);
                            Dictionary<string, object> arguments = new Dictionary<string, object>();
                            arguments.Add("target", target); //action parameters
                            arguments.Add("parameters", item.Name);
                            if (!acceptFoodAction.IsRunning) acceptFoodAction.Start(new Arguments(arguments));
                            userController.clearContext();
                        }
                        else if (item.HasProperty<Property<string>>("recycledDrink") && (item.GetProperty<Property<string>>("recycledDrink").Value.Equals("True")))
                        {
                            Action acceptDrinkAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionAcceptDrink.ACTION_NAME);
                            Dictionary<string, object> arguments = new Dictionary<string, object>();
                            arguments.Add("target", target); //action parameters
                            arguments.Add("parameters", item.Name);
                            if (!acceptDrinkAction.IsRunning) acceptDrinkAction.Start(new Arguments(arguments));
                            userController.clearContext();
                        }
                        else if (item.HasProperty<Property<string>>("recycledSoil") && (item.GetProperty<Property<string>>("recycledSoil").Value.Equals("True")))
                        {
                            Action acceptSoilAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionAcceptSoil.ACTION_NAME);
                            Dictionary<string, object> arguments = new Dictionary<string, object>();
                            arguments.Add("target", target); //action parameters
                            arguments.Add("parameters", item.Name);
                            if (!acceptSoilAction.IsRunning) acceptSoilAction.Start(new Arguments(arguments));
                            userController.clearContext();
                        }
                    }
                }              
            }
            else UserInterface.Instance.displayMessage(target + " is not available",
                                                  Translations.Instance.translateItem_deWithDefArticle(target, 1) + " ist nicht verf�gbar", 3000d);

        }

        //start a user accept action
        private void startReject()
        {
            string target = userController.charNameContext;
            string offer = userController.itemContext;
            // check if characterContext is not empty and character exists in scene
            if (target.Equals(""))
            {
                UserInterface.Instance.displayMessage("Please select a character first",
                                                      "Bitte w�hlt eine Person aus",3000d);
            }
            else if (Universe.Instance.HasEntity(target))
            {
                if (offer.Equals(""))
                {
                    UserInterface.Instance.displayMessage("Please select an offer to reject",
                                                          "W�hlt etwas zum Ablehnen aus",3000d);
                }
                else 
                {
                    Entity item = null;
                    if (Universe.Instance.HasEntity(offer))
                    {
                        item = Universe.Instance.GetEntity(offer);
                    }
                    else
                    {
                        item = getAcceptItemEntity(target,offer);
                    }

                    if (item == null)
                    {
                        UserInterface.Instance.displayMessage("Offer is not valid",
                                                              Translations.Instance.translateItem_deWithDefArticle(target, 1) + " kann nicht abgelehnt werden", 3000d);
                    }
                    else
                    {
                        if (item.HasProperty<Property<string>>("isActivity") && (item.GetProperty<Property<string>>("isActivity").Value.Equals("True")))
                        {
                            Action rejectInvitationAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionRejectInvitation.ACTION_NAME);
                            Dictionary<string, object> arguments = new Dictionary<string, object>();
                            arguments.Add("target", target); //action parameters
                            arguments.Add("parameters", item.Name); //action parameters
                            if (!rejectInvitationAction.IsRunning) rejectInvitationAction.Start(new Arguments(arguments));
                            userController.clearContext();
                        }
                        else if ((item.HasProperty<Property<string>>("eatable") && (item.GetProperty<Property<string>>("eatable").Value.Equals("True"))) ||
                            (item.HasProperty<Property<string>>("recycledDrink") && (item.GetProperty<Property<string>>("recycledDrink").Value.Equals("True"))) ||
                            (item.HasProperty<Property<string>>("recycledSoil") && (item.GetProperty<Property<string>>("recycledSoil").Value.Equals("True"))))
                        {
                            Action rejectOfferAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionRejectOffer.ACTION_NAME);
                            Dictionary<string, object> arguments = new Dictionary<string, object>();
                            arguments.Add("target", target); //action parameters
                            arguments.Add("parameters", item.Name); //action parameters
                            if (!rejectOfferAction.IsRunning) rejectOfferAction.Start(new Arguments(arguments));
                            userController.clearContext();
                        }
                    }       
                }
            }
            else UserInterface.Instance.displayMessage(target + " is not available",
                                                  Translations.Instance.translateItem_deWithDefArticle(target, 1) + " ist nicht verf�gbar", 3000d);
        }

        //start an ask action
        private void startAsk()
        {
            string target = userController.charNameContext;
            string topic = userController.itemContext;

            Entity episode = Universe.Instance.GetEntity("episode");
            string episodeName = episode.GetProperty<Property<string>>("name").Value;
            
            // check if characterContext is not empty and character exists in scene
            if (target.Equals(""))
            {
                UserInterface.Instance.displayMessage("Please select a character first",
                                                      "Bitte w�hlt eine Person aus",3000d);
            }
            else if (Universe.Instance.HasEntity(target))
            {
                if (topic.Equals(""))
                {
                    UserInterface.Instance.displayMessage("Please select a topic to ask about",
                                                          "Bitte w�hlt ein Thema zum Fragen aus", 3000d);
                }
                else if (topic.Equals("gardening") && episodeName.Equals("TheGardening"))
                {                   
                    //later here check for distance
                    Action askGardeningQuestionAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionAskGardeningQuestion.ACTION_NAME);
                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                    arguments.Add("target", target); //action parameters
                    arguments.Add("parameters", "gardening");
                    if (!askGardeningQuestionAction.IsRunning) askGardeningQuestionAction.Start(new Arguments(arguments));
                    userController.clearContext();                    
                }
                else if (topic.Equals("recycling") && episodeName.Equals("TheRecycling"))
                {
                    //later here check for distance
                    Action askRecyclingQuestionAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionAskRecyclingQuestion.ACTION_NAME);
                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                    arguments.Add("target", target); //action parameters
                    arguments.Add("parameters", "recycling");
                    if (!askRecyclingQuestionAction.IsRunning) askRecyclingQuestionAction.Start(new Arguments(arguments));
                    userController.clearContext();
                }
                else 
                {
                    //later here check for distance
                    Action askQuestionAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionAskQuestion.ACTION_NAME);
                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                    arguments.Add("target", target); //action parameters
                    if (!askQuestionAction.IsRunning)
                    {
                        askQuestionAction.Start(new Arguments(arguments));
                        UserInterface.Instance.displayMessage("You ask " + target + " about " + topic,
                                      "Ihr fragt " + target + " �ber " + Translations.Instance.translateItem_de(topic), 3000f);
                    }
                    userController.clearContext();
                }
            }
            else UserInterface.Instance.displayMessage(target + " is not available",
                                                       target + " ist nicht verf�gbar", 3000d);
        }

        //start an apologise action
        private void startApologise()
        {
            string target = userController.charNameContext;
        
            if (target.Equals(""))
            {
                UserInterface.Instance.displayMessage("Please select a character first",
                                                      "Bitte w�hlt eine Person aus",3000d);
            }
            else if (Universe.Instance.HasEntity(target))
            {
                Action apologiseAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionApologise.ACTION_NAME);
                Dictionary<string, object> arguments = new Dictionary<string, object>();
                arguments.Add("target", target); //action parameters
                if (!apologiseAction.IsRunning) apologiseAction.Start(new Arguments(arguments));
                userController.clearContext();
            }
            else UserInterface.Instance.displayMessage(target + " is not available",
                                                       target + " ist nicht verf�gbar", 3000d);
        }

        //start a user hand over soil action
        private void startGive()
        {
            string target = userController.charNameContext;
            string giveItem = userController.itemContext;
            UserEntity user = Universe.Instance.GetEntity<UserEntity>("user");

            // check if characterContext is not empty and character exists in scene
            if (target.Equals(""))
            {
                UserInterface.Instance.displayMessage("Please select a character first",
                                                       "Bitte w�hlt eine Person aus",3000d);
            }
            else if (Universe.Instance.HasEntity<Character>(target))
            {
                // if the user does not have the item
                if (!user.inventoryItemExists(giveItem))
                {
                    UserInterface.Instance.displayMessage("You have no " + giveItem + " to give",
                                                          "Ihr habt " + Translations.Instance.translateItem_deWithNegArticle(giveItem, 4), 3000d);
                }
                else
                {
                   Character targetChar = Universe.Instance.GetEntity<Character>(target);

                    string itemName = user.getItemName(giveItem);
                    if (user.getItemRecycledSoil(itemName))            
                    {
                        if (targetChar.HasProperty<Property<string>>("isGardener") &&
                        targetChar.GetProperty<Property<string>>("isGardener").Value.Equals("true", StringComparison.CurrentCultureIgnoreCase))
                        {
                            Action handOverSoilAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionHandOverSoil.ACTION_NAME);
                            Dictionary<string, object> arguments = new Dictionary<string, object>();
                            arguments.Add("target", target); //action parameters
                            arguments.Add("parameters", giveItem); //action parameters
                            if (!handOverSoilAction.IsRunning) handOverSoilAction.Start(new Arguments(arguments));
                            userController.clearContext();

                        }
                        else
                        {
                            Action doNotHandOverSoilAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionDoNotHandOverSoil.ACTION_NAME);
                            Dictionary<string, object> arguments = new Dictionary<string, object>();
                            arguments.Add("target", target); //action parameters
                            if (!doNotHandOverSoilAction.IsRunning) doNotHandOverSoilAction.Start(new Arguments(arguments));                            userController.clearContext();

                        }
                    }
                    else
                    {
                        UserInterface.Instance.displayMessage(target + "doesn't want your " + giveItem,
                            target + " will " + Translations.Instance.translateItem_deWithDefArticle(giveItem,4) + " nicht", 3000d);
                    }
                }
            }
            else UserInterface.Instance.displayMessage(target + " is not available",
                                                       target + " ist nicht verf�gbar", 3000d);
        }

        private void startHeal()
        {
            string target = userController.charNameContext;
            UserEntity user = Universe.Instance.GetEntity<UserEntity>("user");

            // check if characterContext is not empty and character exists in scene
            if (target.Equals(""))
            {
                UserInterface.Instance.displayMessage("Please select a character to heal first",
                                                      "Bitte w�hlt eine Person zum Heilen aus",3000d);
            }
            else if (Universe.Instance.HasEntity<Character>(target))
            {

                Character targetChar = Universe.Instance.GetEntity<Character>(target);
                if (targetChar.HasProperty<Property<string>>("dying") &&
                    targetChar.GetProperty<Property<string>>("dying").Value.Equals("True"))
                { 
                    Action healAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionHealDyingSpryte.ACTION_NAME);
                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                    arguments.Add(UserStoryAction.TARGET_NAME, target); //action parameters
                    if (!healAction.IsRunning) healAction.Start(new Arguments(arguments));
                    userController.clearContext();               
                }
                else
                {
                    UserInterface.Instance.displayMessage(target + " does not need to be healed",
                                                          target + " muss nicht geheilt werden", 3000d);

                }
            }
            else UserInterface.Instance.displayMessage(target + " is not available",
                                                       target + " ist nicht verf�gbar", 3000d);
        }

        // start a button pressing action
        private void startButtonPress(string buttonName)
        {
            // check if button objects exist
            if (!Universe.Instance.HasEntity<ModellessItem>("button1")) return;
            if (!Universe.Instance.HasEntity<ModellessItem>("button2")) return;
            if (!Universe.Instance.HasEntity<ModellessItem>("body")) return;
            if (!Universe.Instance.HasEntity<Character>("Cerkith")) return;

            if (buttonName.Equals(BUTTON_1)) // pressing button 1
            {
                if (userController.button1AlreadyPressed)
                    UserInterface.Instance.displayMessage("You have already pressed this button",
                                                          "Ihr habt diesen Knopf schon gedr�ckt", 3000d);
                else
                {
                    Action produceLiquidAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionProduceLiquid.ACTION_NAME);
                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                    arguments.Add(UserStoryActionProduceLiquid.TARGET_NAME, "Cerkith"); //action target
                    arguments.Add(UserStoryActionProduceLiquid.PARAMETERS_NAME, "body button1"); //action parameters                    
                    if (!produceLiquidAction.IsRunning) produceLiquidAction.Start(new Arguments(arguments));
                    userController.button1AlreadyPressed = true;
                    userController.clearContext();
                }
            }
            else // pressing button 2
            {
                if (userController.button2AlreadyPressed)
                    UserInterface.Instance.displayMessage("You have already pressed this button",
                                                          "Ihr habt diesen Knopf schon gedr�ckt", 3000d);
                else if (userController.button1AlreadyPressed)
                    UserInterface.Instance.displayMessage("Wait for a Spryte to press this button",
                                                          "Lasst ein Spryte diesen Knopf dr�cken", 3000d);
                else
                {
                    Action pressWrongButtonAction = Universe.Instance.GetEntity("user").GetAction(UserStoryActionPressWrongButton.ACTION_NAME);
                    Dictionary<string, object> arguments = new Dictionary<string, object>();
                    arguments.Add(UserStoryActionPressWrongButton.TARGET_NAME, "button2"); //action target
                    if (!pressWrongButtonAction.IsRunning) pressWrongButtonAction.Start(new Arguments(arguments));
                    userController.button2AlreadyPressed = true;
                    userController.clearContext();                
                }

            }

        }

        private List<Entity> getItemEntities(string itemTypeName)
        { 
            List<Entity> list = new List<Entity>();
            foreach(Entity item in Universe.Instance.Entities)
            {
                if (item.HasProperty<Property<string>>("is") && (item.GetProperty<Property<string>>("is").Value.Equals(itemTypeName)))
                {
                    list.Add(item);
                }
            }
            return list;
        }

        private float getDistanceToUser(Entity entity)
        {
            ION.Realizer.Orient.Entities.UserEntity userEntity = Universe.Instance.GetEntity<UserEntity>("user");

            float distance = float.MaxValue;
            OgrePosition compareToPoint = null;

            if (entity is ION.Realizer.Orient.Entities.Item)
            {
                ION.Realizer.Orient.Entities.Item item = (ION.Realizer.Orient.Entities.Item)entity;
                compareToPoint = (OgrePosition)item.GraphicalAsset.AbsolutePosition;
            }
            else if (entity is ION.Realizer.Orient.Entities.Character)
            {
                ION.Realizer.Orient.Entities.Character character = (ION.Realizer.Orient.Entities.Character)entity;
                compareToPoint = (OgrePosition)character.GraphicalAsset.AbsolutePosition;
            }

            if (compareToPoint != null)
            {
                compareToPoint.Y = (userEntity.AbsolutePosition as OgrePosition).Y; // ignore height differences
                distance = (float)(OgreDistance)compareToPoint.DistanceTo(userEntity.AbsolutePosition);
            }

            return distance;
        }

        private Entity getPickItemEntity(string name)
        {
            List<Entity> itemList = getItemEntities(name);
            Entity minDisItem = null;
            float minDis = float.MaxValue;
            float threshold = 30f;

            foreach (Entity item in itemList)
            {
                float itemDis = getDistanceToUser(item);
                if(itemDis < threshold && itemDis < minDis)
                {
                    minDis = itemDis;
                    minDisItem = item;
                }
            }
            return minDisItem;
        }

        private Entity getAcceptItemEntity(string target, string name)
        {
            Entity targetEntity = Universe.Instance.GetEntity(target);
            if (targetEntity.HasProperty<Property<string>>("holds"))
            {
                string value = targetEntity.GetProperty<Property<string>>("holds").Value;
                List<Entity> itemList = getItemEntities(name);
                foreach (Entity item in itemList)
                {
                    if (item.Name.Equals(value))
                    {
                        return item;
                    }
                }
            }
            return null;
        }

        // returns the noun with an indefinite article (a or an) in front
        // function determines wheter to use a or an
        private string articelize(string noun)
        {
            if (noun.StartsWith("a") || noun.StartsWith("e") || noun.StartsWith("o") || noun.StartsWith("u") || noun.StartsWith("i"))
                return "an " + noun;
            else
                return "a " + noun;
        }

        protected override void OnStep(Context context)
        {
            this.End();
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
            userController = (ION.Realizer.Orient.Entities.UserController)this.Parent;
        }

        protected override void OnDestroy()
        {
        }

    }
}
