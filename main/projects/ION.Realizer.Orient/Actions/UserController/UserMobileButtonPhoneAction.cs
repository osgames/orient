/* ****************************************************
 * Name: UserMobilePhoneButtonAction.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/30
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using ION.Core;
using User.Interface;
using ION.Realizer.Orient.Entities;
using PAGE.OGRE.Math;

namespace ION.Realizer.Orient.Actions.UserController
{
    public class UserMobilePhoneButtonAction : Action
    {
        #region Constants
        public const string ACTION_NAME = "UserMobilePhoneButtonAction";
        public const string BUTTON_NAME = "Button";
        public const string BUTTON_START = "Start";
        public const string BUTTON_BACK = "Back";
        #endregion

        private ION.Realizer.Orient.Entities.UserController userController;

        protected override void OnStart(Context context)
        {
            string buttonName = (string)context[BUTTON_NAME];
            if (buttonName.Equals(BUTTON_START)) // start the recording
            {
                // button start was pressed, switch to input mode
                //follwing the new value of the inputContext variable, change the display system state on the 2D overlay
                UserInterface.Instance.updateSystemButtontoBusy();

            }
        }

        protected override void OnStep(Context context)
        {
            this.End();
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
            userController = (ION.Realizer.Orient.Entities.UserController)this.Parent;
        }

        protected override void OnDestroy()
        {
        }

    }
}