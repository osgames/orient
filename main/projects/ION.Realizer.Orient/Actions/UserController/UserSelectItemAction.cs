using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;

namespace ION.Realizer.Orient.Actions.UserController
{
    public class UserSelectItemAction: Action
    {
        #region Constants
        public const string ACTION_NAME = "UserItemSelectionAction";
        public const string ITEM_NAME = "ItemName";
        #endregion

        protected override void OnStart(Context context)
        {
            string itemName = (string)context[ITEM_NAME];
            ION.Realizer.Orient.Entities.UserController userController = this.Parent as ION.Realizer.Orient.Entities.UserController;
            if (itemName.Equals("heal")) // heal is a special case as it is treated like a gesture
            {
                userController.gestureContext = itemName;
                //follwing the new value of the inputContext variable, change the display system state on the 2D overlay
                UserInterface.Instance.updateSystemButtontoIdle();
                UserInterface.Instance.updateGestureContext(itemName, Translations.Instance.translateGesture_de(itemName));                  
            }
            else
            {
                userController.itemContext = itemName;
                //following the new value of the inputContext variable, change the display system state on the 2D overlay
                UserInterface.Instance.updateSystemButtontoIdle();
                UserInterface.Instance.updateItemContext(itemName, Translations.Instance.translateItem_de(itemName));
            }
        }

        protected override void OnStep(Context context)
        {
            this.End();
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
