using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;

namespace ION.Realizer.Orient.Actions.UserController
{
    public class UserGestureAction : Action
    {
        #region Constants
        public const string ACTION_NAME = "UserGestureAction";
        public const string GESTURE_NAME = "GestureName";
        public const string GESTURE_GREETBACK = "GreetBack";
        public const string GESTURE_EATDRINK = "EatDrink";
        public const string GESTURE_USERPICK = "UserPick";
        public const string GESTURE_ACCEPT = "Accept";
        public const string GESTURE_REJECT = "Reject";
        public const string GESTURE_ASK = "Ask";
        public const string GESTURE_APOLOGISE = "Apologise";
        public const string GESTURE_GIVE = "Give";
        
        #endregion

        protected override void OnStart(Context context)
        {
            string gestureName = (string) context[GESTURE_NAME];
            ION.Realizer.Orient.Entities.UserController userController = this.Parent as ION.Realizer.Orient.Entities.UserController;
            userController.gestureContext = gestureName;
            UserInterface.Instance.updateSystemButtontoIdle();
            if (gestureName.Equals(GESTURE_GREETBACK)) UserInterface.Instance.updateGestureContext("greet", Translations.Instance.translateGesture_de(gestureName));
            else if (gestureName.Equals(GESTURE_EATDRINK)) UserInterface.Instance.updateGestureContext("consume", Translations.Instance.translateGesture_de(gestureName));
            else if (gestureName.Equals(GESTURE_USERPICK)) UserInterface.Instance.updateGestureContext("pick", Translations.Instance.translateGesture_de(gestureName));
            else UserInterface.Instance.updateGestureContext(gestureName, Translations.Instance.translateGesture_de(gestureName));                   
        }

        protected override void OnStep(Context context)
        {
            this.End();
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    
    }
}
