/* ****************************************************
 * Name: NarratorSay.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.IO;
using ION.Core;
using ION.Realizer.Orient.Util;
using PAGE.Generic.GUI;
using PAGE.OGRE.GUI;
using PAGE.OGRE.Math;
using PAGE.Orient;
using WMPLib;

namespace ION.Realizer.Orient.Actions.Narrator
{
    internal class Say : Action
    {
        private const ushort MINIMAL_DURATION = 2000;
        private const ushort MILLISECONDS_PER_WORD = 400;

        private OgreStaticDisplayText text;
        private WindowsMediaPlayer audioPlayer;
        private PerformanceTimer timer;
        private long duration;
        private bool timerON;

        protected override void OnCreate(Arguments arguments)
        {
            this.text = new OgreStaticDisplayText("");
            OrientGraphicsRealizer.Instance.GUIManager.AddText(this.text);
            this.text.Position = new OgreScreenPosition(10, 10);
            this.text.Visible = false;

            this.audioPlayer = new WindowsMediaPlayer();
            this.audioPlayer.PlayStateChange += AudioPlayer_ReachedEnd;

            this.timer = new PerformanceTimer();
            this.timerON = false;
        }

        protected override void OnDestroy()
        {
            OrientGraphicsRealizer.Instance.GUIManager.RemoveText(this.text);
        }

        protected override void OnStart(Context context)
        {
            if (context.Contains("utterance"))
            {
                // text
                this.text.Text = context.Get<string>("utterance");
                this.text.Visible = true;
                this.text.Alignment = TextAlignment.Left;

                if (context.Contains("file") && File.Exists(context.Get<string>("file")))
                {
                    // audio
                    this.audioPlayer.URL = context.Get<string>("file");
                }
                else
                {
                    // duration
                    if (context.Contains("duration"))
                    {
                        this.duration = context.Get<long>("duration");
                    }
                    else
                    {
                        string[] words = this.text.Text.Split(' ');
                        this.duration = MINIMAL_DURATION + words.Length*MILLISECONDS_PER_WORD;
                    }

                    // reset timer
                    this.timer.Reset();
                    this.timerON = true;
                }
            }
            else
            {
                this.End();
            }
        }

        protected override void OnEnd(Context context)
        {
            this.text.Visible = false;
            this.timerON = false;
        }

        protected override void OnFail(Context context)
        {
            // TODO: never fails; "unreachable code" exception?
        }

        protected override void OnStep(Context context)
        {
            if (timerON)
            {
                if (this.timer.Time > this.duration)
                {
                    this.End();
                }
            }
        }

        protected void AudioPlayer_ReachedEnd(int NewState)
        {
            if (NewState == (int) WMPPlayState.wmppsStopped)
            {
                this.End();
            }
        }
    }
}