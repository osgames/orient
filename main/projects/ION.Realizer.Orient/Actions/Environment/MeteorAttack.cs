using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using PAGE.Util;
using ION.Realizer.Orient.Entities;
using User.Interface;
using ION.Core.Extensions;
using FAtiMA.Actions;

namespace ION.Realizer.Orient.Actions.Environment
{
    class MeteorAttack : Action
    {

        #region Constants

        public const string ARG_PARAMETERS_STR = "parameters";

        #endregion        
        
        #region Fields

        private WMPLib.WindowsMediaPlayer mediaPlayer;
        private DateTime startTime;
        private string target;
        private Character targetChar;
        private bool startedFalling;

        #endregion

        protected override void OnCreate(Arguments arguments)
        {
            mediaPlayer = new WMPLib.WindowsMediaPlayer();
            mediaPlayer.PlayStateChange += mediaPlayer_PlayStateChange;
        }

		protected override void OnStart(Context context)
		{
			// check if a target is provided
			if (context.Contains(ARG_PARAMETERS_STR))
			{
				target = (string) context[ARG_PARAMETERS_STR];

				if (Universe.Instance.HasEntity<Character>(target))
				{
				    //opens and plays meteor sound file
				    string soundFileName = PathUtil.ReturnFullPath("data\\audio-data\\effects\\meteor.mp3");
				    if (File.Exists(soundFileName))
				    {
					    mediaPlayer.URL = soundFileName;
					    mediaPlayer.controls.play();
				    }

                    //make the camera shake
                    Entities.Camera camera;
                    if (Universe.Instance.TryGetEntity(Constants.ORIENT_CAMERA_NAME, out camera))
                    {
                        camera.GetAction(Entities.Camera.ACTION_SHAKE).Start();
                    }

                    UserInterface.Instance.animateMeteor();

                    // aquire target character
                    targetChar = Universe.Instance.GetEntity<Character>(target);
                    
                    // put target character to sleep
                    // targetChar.GetAction<PutToSleepAction>(PutToSleepAction.ACTION_NAME).Start();

				} else this.Fail(); // target does not exist
			}
			else this.Fail(); // no target: fail 
            startTime = DateTime.Now;
            startedFalling = false;
		}

        protected override void OnStep(Context context)
        {
            TimeSpan elapsed = DateTime.Now.TimeOfDay - startTime.TimeOfDay;
            if (elapsed.TotalMilliseconds > 9000 && !startedFalling)
            {
                // start the falling down behaviour
                RemoteAction ra = new RemoteAction();
                ra.Subject = target;
                ra.ActionType = Character.BEHAVIOUR_FALL_DOWN_STR;
                Dictionary<string, object> arguments = new Dictionary<string, object>();
                arguments.Add("REMOTE_ACTION_PARAMETERS", ra);
                targetChar.GetAction<Actions.Body.Behaviours.FallDown>(Character.BEHAVIOUR_FALL_DOWN_STR).Start(new Arguments(arguments));
                startedFalling = true;
            }
        }

        protected override void OnEnd(Context context)
        {

        }

        protected override void OnFail(Context context)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected void mediaPlayer_PlayStateChange(int NewState)
        {
            //sound file has finished playing
            if (NewState == (int)WMPLib.WMPPlayState.wmppsStopped)
                this.End();
        }
    
    }
}
