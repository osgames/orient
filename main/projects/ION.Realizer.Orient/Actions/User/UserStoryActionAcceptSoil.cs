/* ****************************************************
 * Name: UserStoryActionAcceptSoil.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/22
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;
using ION.Realizer.Orient.Entities;

namespace ION.Realizer.Orient.Actions.User
{
    class UserStoryActionAcceptSoil : UserStoryAction
    {
        #region Constants
        public const string ACTION_NAME = "AcceptSoil";
        #endregion

        protected override void OnStart(ION.Core.Context context)
        {
            string target = (string)context[UserStoryAction.TARGET_NAME];
            string item = (string)context[UserStoryAction.PARAMETERS_NAME];
            UserInterface.Instance.displayMessage("You accept " + item + " from " + target,
                                                  "Ihr akzeptiert ",3000f);

            UserEntity userEntity = this.Parent as UserEntity;
            userEntity.AddItem(item);
            UserInterface.Instance.updateInventoryList(userEntity.InventoryList);
            this.End();
        }

        protected override void OnStep(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}