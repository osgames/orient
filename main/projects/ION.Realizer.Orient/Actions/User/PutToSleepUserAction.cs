using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using ION.Core.Extensions;
using ION.Realizer.Orient.Entities;

namespace ION.Realizer.Orient.Actions.User
{
    public class PutToSleepUserAction : PutToSleepAction
    {
        protected override void OnStart(Context context)
        {
            (this.Parent as UserEntity).IsActive = false;
        }


        protected override void OnStep(Context context)
        {
            this.End();
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
