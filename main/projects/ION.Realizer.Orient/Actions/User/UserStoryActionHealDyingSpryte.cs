/* ****************************************************
 * Name: UserStoryActionHealDyingSpryte.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/19
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;
using ION.Realizer.Orient.Entities;
using FAtiMA.Actions;

namespace ION.Realizer.Orient.Actions.User
{
    class UserStoryActionHealDyingSpryte : UserStoryAction
    {
        #region Constants
        public const string ACTION_NAME = "HealDyingSpryte";
        #endregion

        protected override void OnStart(ION.Core.Context context)
        {
            string target = (string)context[UserStoryAction.TARGET_NAME];
            UserInterface.Instance.displayMessage("You heal " + target,
                                                  "Ihr heilt " + target, 3000f);

            //get target character
            Character targetChar = Universe.Instance.GetEntity<Character>(target);

            //set targets dying value to false
            targetChar.GetProperty<Property<string>>("dying").Value = "False";
            
            //start the getting up behaviour
            // start the falling down behaviour
            RemoteAction ra = new RemoteAction();
            ra.Subject = target;
            ra.ActionType = Character.BEHAVIOUR_GETUP_FALLEN_STR;
            Dictionary<string, object> arguments = new Dictionary<string, object>();
            arguments.Add("REMOTE_ACTION_PARAMETERS", ra);
            targetChar.GetAction<Actions.Body.Behaviours.GetUpFallen>(Character.BEHAVIOUR_GETUP_FALLEN_STR).Start(new Arguments(arguments));
           

            this.End();
        }

        protected override void OnStep(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}