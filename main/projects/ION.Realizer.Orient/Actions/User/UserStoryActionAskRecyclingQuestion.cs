/* ****************************************************
 * Name: UserStoryActionAskRecyclingQuestion.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/23
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;

namespace ION.Realizer.Orient.Actions.User
{
    class UserStoryActionAskRecyclingQuestion : UserStoryAction
    {
        #region Constants
        public const string ACTION_NAME = "AskRecyclingQuestion";
        #endregion

        protected override void OnStart(ION.Core.Context context)
        {
            string target = (string)context[UserStoryAction.TARGET_NAME];
            string topic = (string)context[UserStoryAction.PARAMETERS_NAME];
            UserInterface.Instance.displayMessage("You ask " + target + " " + topic + " question",
                                                  "Ihr fragt " + target + " " + " nach dem Recycling", 3000f);
            this.End();
        }

        protected override void OnStep(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}