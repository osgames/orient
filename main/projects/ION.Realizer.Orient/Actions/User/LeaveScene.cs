using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;

namespace ION.Realizer.Orient.Actions.User
{
    public class LeaveScene : Action
    {
        #region Constants
        public const string ACTION_NAME = "leavescene";
        #endregion
        
        
        protected override void OnStart(Context context)
        {
            //follwing user leaving a scene, change the display system state on the 2D overlay
            UserInterface.Instance.updateSystemButtontoBusy();
            this.End();
        }


        protected override void OnStep(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
