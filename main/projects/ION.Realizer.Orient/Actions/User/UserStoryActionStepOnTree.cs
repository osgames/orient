/* ****************************************************
 * Name: UserStoryActionStepOnTree.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/23
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;

namespace ION.Realizer.Orient.Actions.User
{
    class UserStoryActionStepOnTree : UserStoryAction
    {
        #region Constants
        public const string ACTION_NAME = "StepOnTree";
        #endregion

        protected override void OnStart(ION.Core.Context context)
        {         
            UserInterface.Instance.displayMessage("You step on a tiny tree",
                                                  "Ihr tretet auf einen kleinen Baum", 3000f);
            this.End();
        }

        protected override void OnStep(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}