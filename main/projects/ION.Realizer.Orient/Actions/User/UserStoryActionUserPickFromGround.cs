/* ****************************************************
 * Name: UserStoryActionPickFromGround.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/19
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;
using ION.Realizer.Orient.Entities;

namespace ION.Realizer.Orient.Actions.User
{
    class UserStoryActionUserPickFromGround : UserStoryAction
    {
        #region Constants
        public const string ACTION_NAME = "UserPickFromGround";
        #endregion

        protected override void OnStart(ION.Core.Context context)
        {
            string target = (string)context[UserStoryAction.TARGET_NAME];

            // remove the item from the world
            ION.Realizer.Orient.Entities.Item item = Universe.Instance.GetEntity<ION.Realizer.Orient.Entities.Item>(target);
            if (item != null)
            {
                // remove graphical asset
                item.Picked = true;
                item.GraphicalAsset.Unplace();
            }
            
            // add the item to the user inventory
            UserEntity userEntity = this.Parent as UserEntity;
            userEntity.AddItem(target);
            UserInterface.Instance.updateInventoryList(userEntity.InventoryList);
            this.End();
        }

        protected override void OnStep(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}