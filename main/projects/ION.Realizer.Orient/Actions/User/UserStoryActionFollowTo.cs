/* ****************************************************
 * Name: UserStoryActionFollow.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/22
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;

namespace ION.Realizer.Orient.Actions.User
{
    class UserStoryActionFollowTo : UserStoryAction
    {
        #region Constants
        public const string ACTION_NAME = "FollowTo";
        #endregion

        protected override void OnStart(ION.Core.Context context)
        {
            string place = (string)context[UserStoryAction.TARGET_NAME];
            //string place = (string)context[UserStoryAction.PARAMETERS_NAME];
            UserInterface.Instance.displayMessage("You are in the " + place,
                                                  "Ihr seid jetzt im Garten", 3000f);
            this.End();
        }

        protected override void OnStep(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}