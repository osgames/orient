using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;

namespace ION.Realizer.Orient.Actions.User
{
    public class UserStoryActionGreet : UserStoryAction
    {

        #region Constants
        public const string ACTION_NAME = "GreetBack";
        #endregion        

        protected override void OnStart(Context context)
        {
            string target = (string) context[UserStoryAction.TARGET_NAME];
            UserInterface.Instance.displayMessage("You greet " + target, "Ihr gr�sst " + target, 3000f);
            this.End();
        }

        protected override void OnStep(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }    
    }
}
