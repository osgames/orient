/* ****************************************************
 * Name: UserStoryActionDoNotHandOverSoil.cs
 * Code: Michael Kriegel - michael@macs.hw.ac.uk
 * Date: 2009/01/23
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;
using ION.Realizer.Orient.Entities;

namespace ION.Realizer.Orient.Actions.User
{
    class UserStoryActionDoNotHandOverSoil : UserStoryAction
    {
        #region Constants
        public const string ACTION_NAME = "DoNotHandOverSoil";
        #endregion

        protected override void OnStart(ION.Core.Context context)
        {
            string target = (string)context[UserStoryAction.TARGET_NAME];
            UserInterface.Instance.displayMessage("You try to give the soil to " + target,
                                                  "Ihr versucht " + target + " die Erde zu geben", 3000f);

            this.End();
        }

        protected override void OnStep(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}