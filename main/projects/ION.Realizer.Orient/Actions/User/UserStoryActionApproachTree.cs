/* ****************************************************
 * Name: UserStoryActionApproachTree.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/19
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;

namespace ION.Realizer.Orient.Actions.User
{
    class UserStoryActionApproachTree : UserStoryAction
    {
        #region Constants
        public const string ACTION_NAME = "ApproachTree";
        #endregion

        protected override void OnStart(ION.Core.Context context)
        {
            UserInterface.Instance.displayMessage("You approach the tree","Ihr n�hert euch dem Baum", 3000f);
            this.End();
        }

        protected override void OnStep(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}