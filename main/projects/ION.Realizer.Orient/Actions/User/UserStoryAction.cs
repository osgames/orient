using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using FAtiMA.Actions;

namespace ION.Realizer.Orient.Actions.User
{
    // base class for all user actions that should be communicated to other agents
    public abstract class UserStoryAction : Action
    {
        #region Constants
        public const string TARGET_NAME = "target";
        public const string PARAMETERS_NAME = "parameters";
        #endregion

        protected override void OnEnd(Context context)
        {
            // update action effects
            AgentActionEffectUpdater.Instance.UpdateActionEffectsUserStoryAction(this, context);
        }


    }
}
