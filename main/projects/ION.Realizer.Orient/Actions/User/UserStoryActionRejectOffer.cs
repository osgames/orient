/* ****************************************************
 * Name: UserStoryActionRejectOffer.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/23
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;

namespace ION.Realizer.Orient.Actions.User
{
    class UserStoryActionRejectOffer : UserStoryAction
    {
        #region Constants
        public const string ACTION_NAME = "RejectOffer";
        #endregion

        protected override void OnStart(ION.Core.Context context)
        {
            string target = (string)context[UserStoryAction.TARGET_NAME];
            string item = (string)context[UserStoryAction.PARAMETERS_NAME];
            UserInterface.Instance.displayMessage("You reject " + item + " from " + target,
                                                  "Ihr lehnt " + Translations.Instance.translateItem_deWithDefArticle(item, 4) + " von " + target + " ab", 3000f);
            this.End();
        }

        protected override void OnStep(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}