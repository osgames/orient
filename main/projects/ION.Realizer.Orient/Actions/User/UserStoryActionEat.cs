/* ****************************************************
 * Name: UserStoryActionEat.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/19
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;
using ION.Realizer.Orient.Entities;

namespace ION.Realizer.Orient.Actions.User
{
    class UserStoryActionEat : UserStoryAction
    {
        #region Constants
        public const string ACTION_NAME = "Eat";
        #endregion 

        protected override void OnStart(ION.Core.Context context)
        {
            string target = (string)context[UserStoryAction.TARGET_NAME];

            UserEntity userEntity = this.Parent as UserEntity;
            userEntity.RemoveItem(target);
            UserInterface.Instance.updateInventoryList(userEntity.InventoryList);
            this.End();
        }

        protected override void OnEnd(ION.Core.Context context)
        {
            base.OnEnd(context);       
        }

        protected override void OnFail(ION.Core.Context context)
        {            
        }

        protected override void OnStep(ION.Core.Context context)
        {            
        }

        protected override void OnCreate(ION.Core.Arguments arguments)
        {            
        }

        protected override void OnDestroy()
        {           
        }
    }
}
