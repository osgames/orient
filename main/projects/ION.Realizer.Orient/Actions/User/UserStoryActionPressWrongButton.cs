/* ****************************************************
 * Name: UserStoryActionPressWrongButton.cs
 * Code: Meiyii Lim - myl@macs.hw.ac.uk
 * Date: 2008/09/23
 * 
 * Heriot-Watt University
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using User.Interface;

namespace ION.Realizer.Orient.Actions.User
{
    class UserStoryActionPressWrongButton : UserStoryAction
    {
        #region Constants
        public const string ACTION_NAME = "PressWrongButton";
        #endregion

        protected override void OnStart(ION.Core.Context context)
        {
            string target = (string)context[UserStoryAction.TARGET_NAME];
            UserInterface.Instance.displayMessage("You press " + target,"Ihr dr�ckt auf den roten Knopf", 3000f);
            this.End();
        }

        protected override void OnStep(Context context)
        {
        }

        protected override void OnEnd(Context context)
        {
            base.OnEnd(context);
        }

        protected override void OnFail(Context context)
        {
        }

        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}