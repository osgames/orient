/* ****************************************************
 * Name: PerformanceTimer.cs
 * Code: Daniel Strigl, Code Project
 *       Marco Vala - marco.vala@gaips.inesc-id.pt
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Threading;

namespace ION.Realizer.Orient.Util
{
    internal class PerformanceTimer
    {
        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceCounter(out long lpPerformanceCount);

        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceFrequency(out long lpFrequency);

        private long frequency;
        private long resetTime;

        public PerformanceTimer()
        {
            if (QueryPerformanceFrequency(out this.frequency) == false)
            {
                Console.WriteLine("AQUI");
                // high-performance counter not supported
                throw new Win32Exception();
            }
            this.resetTime = 0;
        }

        // start the timer
        public void Reset()
        {
            // allow other waiting threads to execute
            Thread.Sleep(0);

            QueryPerformanceCounter(out this.resetTime);
        }

        // returns the total time (in milliseconds)
        public long Time
        {
            get 
            {
                long time;
                QueryPerformanceCounter(out time);
                return (long)((double)(time-this.resetTime)*1000.0f/(double)this.frequency); 
            }
        }
    }
}