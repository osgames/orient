/* ****************************************************
 * Name: Util.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/10/23
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Realizer.Orient.Entities;
using Mogre;

namespace ION.Realizer.Orient.Util
{
    public class Util
    {
        public static void LogMessage(string message)
        {
            LogManager.Singleton.GetLog(Constants.ION_LOG_FILE).LogMessage(message);
        }

        public static void LogMessage(Asset asset, Action action, string message)
        {
            LogMessage(asset.Name + ",\t" + asset.GraphicalAsset.ResourceAsset.IdToken +
                ":\t" + action.Name + "->\t" + message);
        }
    }
}