/* ****************************************************
 * Name: Camera.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Realizer.Orient.Actions.Camera;

namespace ION.Realizer.Orient.Entities
{
    public sealed class Camera : Entity
    {
        public const string ACTION_PLACE_ON_SET_STR = "place-on-set";
        public const string ACTION_FREE_NAVIGATE_STR = "free-navigate";
        public const string ACTION_FP_NO_FREE_LOOK_STR = "firstPersonNoFreeLook-navigate";
        public const string ACTION_FP_FREE_LOOK_STR = "firstPersonFreeLook-navigate";
        public const string ACTION_FOCUS_CHARACTER_STR = "focus-character";
        public const string ACTION_FOCUS_ASSET_STR = "focus-asset";
        public const string ACTION_MOVE_1ST_PERSON_STR = "move-1stperson";
        public const string ACTION_SHAKE = "shake";

        protected override void OnCreate(Arguments arguments)
        {
            this.CreateAction<PlaceOnSet>(ACTION_PLACE_ON_SET_STR);
            this.CreateAction<FreeNavigate>(ACTION_FREE_NAVIGATE_STR);
            this.CreateAction<FirstPersonNoFreeLookNavigate>(ACTION_FP_NO_FREE_LOOK_STR);
            this.CreateAction<FirstPersonFreeLookNavigate>(ACTION_FP_FREE_LOOK_STR);
            this.CreateAction<FocusCharacter>(ACTION_FOCUS_CHARACTER_STR);
            this.CreateAction<FocusAsset>(ACTION_FOCUS_ASSET_STR);
            this.CreateAction<Move1stPerson>(ACTION_MOVE_1ST_PERSON_STR);
            this.CreateAction<Shake>(ACTION_SHAKE);
        }

        protected override void OnDestroy()
        {
        }
    }
}
