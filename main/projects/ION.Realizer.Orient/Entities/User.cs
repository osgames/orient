using System;
using System.Collections.Generic;
using System.Text;
using ION.Realizer.Orient.Actions.User;
using PAGE.Orient.Domain.Assets;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Math;
using PAGE.Orient;
using ION.Core;


namespace ION.Realizer.Orient.Entities
{
   public class UserEntity : Entity
   {
       #region Constants
       public const string ENTITY_NAME = "user";
       public const string DISPLAY_NAME_PROPERTY_NAME = "displayName";
       public const string IS_PERSON_PROPERTY_NAME = "isPerson";
       public const string HAS_SOIL_PROPERTY_NAME = "hasSoil";
       #endregion

       #region private variables
           private bool isActive;
           private Position absolutePosition;
           private GenericWaypoint waypoint;
           private OrientCharacter userDummy;
           // a list storing the name of inventory items
           private static List<string> inventoryList = new List<string>();
           // a list storing the type of inventory items (directly corresponding to inventoryList)
           private static List<InventoryItemData> inventoryDataList = new List<InventoryItemData>();
           // a string storing where the user was before
           private static string previousLocation = ""; 
        #endregion

       #region Properties
           public bool IsActive
           {
               get { return isActive; }
               set { isActive = value; }
           }

           public static string PreviousLocation
           {
                get { return previousLocation; }
                set { previousLocation = value; }
           }

           public Position AbsolutePosition
           {
               get { return absolutePosition; }
           }

           public GenericWaypoint Waypoint
           {
               get { return waypoint; }
               set
               {
                   waypoint = value;
                   absolutePosition = value.AbsolutePosition;
               }
           }

           public OrientCharacter UserDummy
           {
               get { return userDummy; }
           }

           public List<String> InventoryList
           {
               get
               {
                   return inventoryList;
               }
           }

           #endregion

       #region Inventory

       private class InventoryItemData
       {
           public string type;
           public bool eatable;
           public bool drinkable;
           public bool recycledSoil;

           public InventoryItemData(string type_in)
           {
               type = type_in;
               eatable = false;
               drinkable = false;
               recycledSoil = false;
           }

       }


           public void AddItem(String item)
           {
               // get the entity for this item
               if (Universe.Instance.HasEntity<Item>(item))
               {
                   Item entity = Universe.Instance.GetEntity<Item>(item);
                   if (entity.HasProperty<Property<string>>("is"))
                   {
                       string itemType = entity.GetProperty<Property<string>>("is").Value;
                       InventoryItemData itemData = new InventoryItemData(itemType);
                       if (entity.HasProperty<Property<string>>("eatable"))
                           if (entity.GetProperty<Property<string>>("eatable").Value.ToLower().Equals("true"))
                               itemData.eatable = true;

                       if (entity.HasProperty<Property<string>>("drinkable"))
                           if (entity.GetProperty<Property<string>>("drinkable").Value.ToLower().Equals("true"))
                               itemData.drinkable = true;

                       if (entity.HasProperty<Property<string>>("recycledSoil"))
                           if (entity.GetProperty<Property<string>>("recycledSoil").Value.ToLower().Equals("true"))
                               itemData.recycledSoil = true;

                       // add item to inventory list and associated data to inventory data list
                       inventoryDataList.Add(itemData);
                       inventoryList.Add(item);
                   }
               }
           }

           public void RemoveItem(String item)
           {
               int index = inventoryList.IndexOf(item);
               inventoryList.RemoveAt(index);
               inventoryDataList.RemoveAt(index);
           }

           public bool getItemEatable(String item)
           {
               int index = inventoryList.IndexOf(item);
               return inventoryDataList[index].eatable;
           }

           public bool getItemDrinkable(String item)
           {
               int index = inventoryList.IndexOf(item);
               return inventoryDataList[index].drinkable;           
           }

           public bool getItemRecycledSoil(String item)
           {
               int index = inventoryList.IndexOf(item);
               return inventoryDataList[index].recycledSoil;           
           }

           public string getItemType(String item)
           {
               int index = inventoryList.IndexOf(item);
               return inventoryDataList[index].type;           
           }

           public string getItemName(String type)
           {
               foreach (InventoryItemData data in inventoryDataList)
               { 
                    if (data.type.Equals(type))
                    {
                        int index = inventoryDataList.IndexOf(data);
                        return InventoryList[index];            
                    }
               }
               return "";
           }


           public bool inventoryItemExists(string itemTypeName)
           {
               foreach (InventoryItemData data in inventoryDataList)
               { 
                    if (data.type.Equals(itemTypeName))
                        return true;
               }
               return false;
           }

           #endregion

           public UserEntity()
            {
                isActive = true;
                userDummy = (OrientCharacter)
                    OrientGraphicsRealizer.Instance.LoadCharacter("userdummy");
                userDummy.Animate(null, "stand");
                userDummy.Visible = false;
                //userDummy.ShowAllDebuggingSpots();
                // for testing on whether the character ask for soil
                /*inventoryList.Clear();
                inventoryDataList.Clear();
                inventoryList.Add("soil");
                InventoryItemData iSoil = new InventoryItemData("soil");
                iSoil.recycledSoil = true;
                inventoryDataList.Add(iSoil);
                User.Interface.UserInterface.Instance.updateInventoryList(inventoryList);*/
            }

            protected override void OnCreate(Arguments arguments)
            {
                //user properties (for now), are created by the SF (StartState.cs), there could be a more elegant solution later

                //create user actions (for now hard-coded)

                //put to sleep and awake actions
                this.CreateAction<AwakeUserAction>(AwakeUserAction.ACTION_NAME);
                this.CreateAction<PutToSleepUserAction>(PutToSleepUserAction.ACTION_NAME);

                //leaving the scene
                this.CreateAction<LeaveScene>(LeaveScene.ACTION_NAME);

                //story actions
                this.CreateAction<UserStoryActionGreet>(UserStoryActionGreet.ACTION_NAME);
                this.CreateAction<UserStoryActionAcceptInvitation>(UserStoryActionAcceptInvitation.ACTION_NAME);
                this.CreateAction<UserStoryActionRejectInvitation>(UserStoryActionRejectInvitation.ACTION_NAME);
                this.CreateAction<UserStoryActionApologise>(UserStoryActionApologise.ACTION_NAME);
                this.CreateAction<UserStoryActionRejectOffer>(UserStoryActionRejectOffer.ACTION_NAME);
                this.CreateAction<UserStoryActionAskQuestion>(UserStoryActionAskQuestion.ACTION_NAME);

                // meal scenario
                this.CreateAction<UserStoryActionEat>(UserStoryActionEat.ACTION_NAME);
                this.CreateAction<UserStoryActionDrink>(UserStoryActionDrink.ACTION_NAME);
                this.CreateAction<UserStoryActionUserPickFromGround>(UserStoryActionUserPickFromGround.ACTION_NAME);
                this.CreateAction<UserStoryActionUserPickFromTree>(UserStoryActionUserPickFromTree.ACTION_NAME);
                this.CreateAction<UserStoryActionApproachTree>(UserStoryActionApproachTree.ACTION_NAME);
                this.CreateAction<UserStoryActionHealDyingSpryte>(UserStoryActionHealDyingSpryte.ACTION_NAME);
                this.CreateAction<UserStoryActionAcceptFood>(UserStoryActionAcceptFood.ACTION_NAME);
                this.CreateAction<UserStoryActionGiveAttention>(UserStoryActionGiveAttention.ACTION_NAME);
                
                // recycling scenario
                this.CreateAction<UserStoryActionApproachMachine>(UserStoryActionApproachMachine.ACTION_NAME);
                this.CreateAction<UserStoryActionAcceptDrink>(UserStoryActionAcceptDrink.ACTION_NAME);
                this.CreateAction<UserStoryActionAcceptSoil>(UserStoryActionAcceptSoil.ACTION_NAME);
                this.CreateAction<UserStoryActionAskRecyclingQuestion>(UserStoryActionAskRecyclingQuestion.ACTION_NAME);
                this.CreateAction<UserStoryActionPressWrongButton>(UserStoryActionPressWrongButton.ACTION_NAME);
                this.CreateAction<UserStoryActionProduceLiquid>(UserStoryActionProduceLiquid.ACTION_NAME);
               
                
                // gardening scenario
                this.CreateAction<UserStoryActionAskGardeningQuestion>(UserStoryActionAskGardeningQuestion.ACTION_NAME);
                this.CreateAction<UserStoryActionFollowTo>(UserStoryActionFollowTo.ACTION_NAME);
                this.CreateAction<UserStoryActionHandOverSoil>(UserStoryActionHandOverSoil.ACTION_NAME);
                this.CreateAction<UserStoryActionDoNotHandOverSoil>(UserStoryActionDoNotHandOverSoil.ACTION_NAME);
                this.CreateAction<UserStoryActionPerformDance>(UserStoryActionPerformDance.ACTION_NAME);
                this.CreateAction<UserStoryActionStepOnTree>(UserStoryActionStepOnTree.ACTION_NAME);
            }
        }
    
    
}
