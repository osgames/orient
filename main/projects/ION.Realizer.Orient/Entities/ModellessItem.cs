/* ****************************************************
 * Name: Narrator.cs
 * Code: Rui Figueiredo - rui.figueiredo@gaips.inesc-id.pt
 * Date: 2008/09/08
 * 
 * Inesc-ID GAIPS
 * 
 * Represents an item without model (Repository for properties)
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace ION.Realizer.Orient.Entities
{
    public class ModellessItem : ION.Core.Entity
    {
    }
}
