/* ****************************************************
 * Name: Character.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Collections.Generic;
using ION.Core;
using ION.Realizer.Orient.Actions.Body.Behaviours;

namespace ION.Realizer.Orient.Entities
{
    public class Character : Body
    {
        #region Constants

        public const string PROPERTY_MOVE_TYPE_STR = "move-type";
        public const string PROPERTY_EXPRESSION_STR = "expression";
        public const string PROPERTY_EMOTION_STR = "emotion";
        public const string PROPERTY_BODY_POSITION_STR = "position";

        public const string BEHAVIOUR_SIT_ON_CHAIR_STR = "sit-on-chair";
        public const string BEHAVIOUR_GET_UP_FROM_CHAIR_STR = "get-up-from-chair";
        public const string BEHAVIOUR_PICK_ITEM_STR = "pick-item";
        public const string BEHAVIOUR_DROP_ITEM_STR = "drop-item";
        public const string BEHAVIOUR_SAY_STR = "say";
        public const string BEHAVIOUR_SAY_TO_STR = "say-to";
        public const string BEHAVIOUR_GIVE_OBJECT_STR = "give-object";
        public const string BEHAVIOUR_GIVE_MONEY_STR = "give-money";
        public const string BEHAVIOUR_GIVE_GIFT_STR = "give-gift";
        public const string BEHAVIOUR_WALK_AWAY_STR = "walk-away";
        public const string BEHAVIOUR_WALK_TO_PLACE_STR = "walk-to-place";
        public const string BEHAVIOUR_THUMB_UP_STR = "thumb-up";
        public const string BEHAVIOUR_WAVE_HAND_STR = "wave-hand";
        public const string BEHAVIOUR_BOW_STR = "bow";
        public const string BEHAVIOUR_SPIT_STR = "spit";
        public const string BEHAVIOUR_TOAST_STR = "toast";

        public const string BEHAVIOUR_GREET_STR = "GreetGesture";
        public const string BEHAVIOUR_EAT_STR = "Eat";
        public const string BEHAVIOUR_EATING_STR = "eating";
        public const string BEHAVIOUR_PICK_FROM_GROUND_STR = "PickFromGround";
        public const string BEHAVIOUR_PICK_FROM_TREE_STR = "PickFromTree";
        public const string BEHAVIOUR_PICK_FROM_TABLE_STR = "PickFromTable";
        public const string BEHAVIOUR_PICK_FROM_MACHINE_STR = "PickFromMachine";
        public const string BEHAVIOUR_REST_ON_STR = "RestOn";
        public const string BEHAVIOUR_SURPRISE_STR = "Surprise";
        public const string BEHAVIOUR_ATTEND_TO_STR = "AttendTo";
        public const string BEHAVIOUR_GO_TO_STR = "GoTo";
        public const string BEHAVIOUR_ANGRY_STR = "AngryGesture";
        public const string BEHAVIOUR_FORGIVE_STR = "Forgive";
        public const string BEHAVIOUR_FORGIVE_ON_HEALING_STR = "ForgiveOnHealing";
        public const string BEHAVIOUR_OFFER_STR = "OfferGesture";
        public const string BEHAVIOUR_APOLOGISE_STR = "Apologise";
        public const string BEHAVIOUR_POINTS_AT_STR = "PointsAt";
        public const string BEHAVIOUR_PRESS_BUTTON_STR = "PressButton";
        public const string BEHAVIOUR_FALL_DOWN_STR = "FallDown";
        public const string BEHAVIOUR_GETUP_FALLEN_STR = "GetUpFallen";


        #endregion

        #region Protected Methods

        protected override void CreateProperties()
        {
            base.CreateProperties();

            // create properties
            this.CreateProperty<Property<string>>(PROPERTY_MOVE_TYPE_STR).Value = "walk-to";
            this.CreateProperty<Property<string>>(PROPERTY_EXPRESSION_STR).Value = "natural";
            this.CreateProperty<Property<string>>(PROPERTY_EMOTION_STR).Value = "neutral";     // body status
            this.CreateProperty<Property<string>>(PROPERTY_BODY_POSITION_STR).Value = "standing";   // body position
        }

        protected override void CreateActions()
        {
            base.CreateActions();

            Dictionary<string, object> args = new Dictionary<string, object>();

            // create behaviours
            this.CreateAction<SitOnChair>(BEHAVIOUR_SIT_ON_CHAIR_STR);
          
            this.CreateAction<PickItem>(BEHAVIOUR_PICK_ITEM_STR);
            this.CreateAction<DropItem>(BEHAVIOUR_DROP_ITEM_STR);

            args[Behaviour.ARG_SIMULTANEOUS_STR] = false;
            this.CreateAction<SayTo>(BEHAVIOUR_SAY_TO_STR, new Arguments(args));
            args.Clear();

            this.CreateAction<WalkTo>(BEHAVIOUR_WALK_TO_PLACE_STR);
            this.CreateAction<WalkTo>(BEHAVIOUR_GO_TO_STR);

            this.CreateAction<GiveObjectTo>(BEHAVIOUR_GIVE_OBJECT_STR);
            this.CreateAction<GiveObjectTo>(BEHAVIOUR_GIVE_MONEY_STR);
            this.CreateAction<GiveObjectTo>(BEHAVIOUR_GIVE_GIFT_STR);

            this.CreateAction<PickItem>(BEHAVIOUR_PICK_FROM_GROUND_STR);
            this.CreateAction<PickItem>(BEHAVIOUR_PICK_FROM_TABLE_STR);
            this.CreateAction<PickItem>(BEHAVIOUR_PICK_FROM_MACHINE_STR);
            this.CreateAction<PickFromTree>(BEHAVIOUR_PICK_FROM_TREE_STR);

            //do action and then stand behaviours
            args[SingleActionBehaviour.ARG_ACTION_ID_STR] = ACTION_REST_STR;
            this.CreateAction<SingleActionBehaviour>(BEHAVIOUR_REST_ON_STR, new Arguments(args));
            args.Clear();

            args[SingleActionBehaviour.ARG_ACTION_ID_STR] = ACTION_CLAP_ABOVE_HEAD_STR;
            this.CreateAction<SingleActionBehaviour>(BEHAVIOUR_SURPRISE_STR, new Arguments(args));
            args.Clear();

            args[SingleActionBehaviour.ARG_ACTION_ID_STR] = ACTION_EAT_STR;
            this.CreateAction<SingleActionBehaviour>(BEHAVIOUR_EAT_STR, new Arguments(args));
            args.Clear();

            args[SingleActionBehaviour.ARG_ACTION_ID_STR] = ACTION_EAT_STR;
            this.CreateAction<SingleActionBehaviour>(BEHAVIOUR_EATING_STR, new Arguments(args));
            args.Clear();

            args[SingleActionBehaviour.ARG_ACTION_ID_STR] = ACTION_GET_UP_RIGHT_STR;
            this.CreateAction<SingleActionBehaviour>(BEHAVIOUR_GET_UP_FROM_CHAIR_STR, new Arguments(args));
            args.Clear();

            args[SingleActionBehaviour.ARG_ACTION_ID_STR] = ACTION_SPEAK_STR;
            this.CreateAction<SingleActionBehaviour>(BEHAVIOUR_SAY_STR, new Arguments(args));
            args.Clear();

            args[SingleActionBehaviour.ARG_ACTION_ID_STR] = ACTION_HANDSTRIKE_STR;
            this.CreateAction<SingleActionBehaviour>(BEHAVIOUR_ANGRY_STR, new Arguments(args));
            args.Clear();

            // fall down and get up from floor
            this.CreateAction<FallDown>(BEHAVIOUR_FALL_DOWN_STR);

            args[SingleActionBehaviour.ARG_ACTION_ID_STR] = ACTION_GETUP_FALLEN_STR;
            this.CreateAction<GetUpFallen>(BEHAVIOUR_GETUP_FALLEN_STR, new Arguments(args));
            args.Clear();

            this.CreateAction<AttendTo>(BEHAVIOUR_ATTEND_TO_STR);

            args[AttendTo.ARG_WALK_TO_SPOT_TAG] = "button-spot";
            args[Gesture.ARG_GESTURE_ACTION_ID_STR] = ACTION_PRESS_BUTTON_STR;
            this.CreateAction<PressButton>(BEHAVIOUR_PRESS_BUTTON_STR, new Arguments(args));
            args.Clear();

            this.CreateGestureActions();
        }

        protected void CreateGestureActions()
        {
            Dictionary<string, object> args = new Dictionary<string, object>();

            // create greet behaviors
            args[AttendTo.ARG_WALK_TO_STR] = true;
            args[Gesture.ARG_GESTURE_ACTION_ID_STR] = ACTION_DIAGONAL_HAND_RISE_STR;
            this.CreateAction<Gesture>(BEHAVIOUR_GREET_STR, new Arguments(args));

            args[AttendTo.ARG_WALK_TO_STR] = false;
            args[Gesture.ARG_GESTURE_ACTION_ID_STR] = ACTION_THUMB_UP_STR;
            this.CreateAction<Gesture>(BEHAVIOUR_THUMB_UP_STR, new Arguments(args));

            args[Gesture.ARG_GESTURE_ACTION_ID_STR] = ACTION_WAVE_HAND_STR;
            this.CreateAction<Gesture>(BEHAVIOUR_WAVE_HAND_STR, new Arguments(args));

            args[Gesture.ARG_GESTURE_ACTION_ID_STR] = ACTION_BOW_STR;
            this.CreateAction<Gesture>(BEHAVIOUR_BOW_STR, new Arguments(args));

            args[Gesture.ARG_GESTURE_ACTION_ID_STR] = ACTION_SPIT_STR;
            this.CreateAction<Gesture>(BEHAVIOUR_SPIT_STR, new Arguments(args));

            args[Gesture.ARG_GESTURE_ACTION_ID_STR] = ACTION_ELBOWSTRIKE_STR;
            this.CreateAction<Gesture>(BEHAVIOUR_FORGIVE_STR, new Arguments(args));
            this.CreateAction<Gesture>(BEHAVIOUR_FORGIVE_ON_HEALING_STR, new Arguments(args));

            args[Gesture.ARG_GESTURE_ACTION_ID_STR] = ACTION_HANDRISE_STR;
            this.CreateAction<Gesture>(BEHAVIOUR_OFFER_STR, new Arguments(args));

            args[Gesture.ARG_GESTURE_ACTION_ID_STR] = ACTION_NIGHTFEVER_STR;
            this.CreateAction<Gesture>(BEHAVIOUR_APOLOGISE_STR, new Arguments(args)); 

            args[Gesture.ARG_GESTURE_ACTION_ID_STR] = ACTION_WAVE_HAND_STR;
            this.CreateAction<Toast>(BEHAVIOUR_TOAST_STR, new Arguments(args));

            args[AttendTo.ARG_WALK_TO_SPOT_TAG] = "point-spot";
            args[Gesture.ARG_GESTURE_ACTION_ID_STR] = ACTION_POINT_AT_STR;
            this.CreateAction<Gesture>(BEHAVIOUR_POINTS_AT_STR, new Arguments(args));

        } 

        #endregion
    }
}