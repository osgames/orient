/* ****************************************************
 * Name: Item.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/27
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using ION.Core;
using ION.Realizer.Orient.Actions.Item;
using PAGE.OGRE.Domain.Assets;
using PAGE.Orient;

namespace ION.Realizer.Orient.Entities
{
    public class Item : Asset
    {
        #region Constants

        public const string ACTION_PLACE_STR = "place-item";
        public const string ACTION_UNPLACE_STR = "unplace-item";
        public const string ARG_TYPE_STR = "type";

        #endregion

        #region Fields

        protected bool chosenForSitting;
        protected bool chosenForPicking = false;
        protected bool picked = false;
        protected Set set; 

        #endregion

        #region Properties

        public new OgreItem GraphicalAsset
        {
            get { return this.asset as OgreItem; }
        }

        protected internal Set Set
        {
            get { return set; }
            set
            {
                set = value;
                //adds this item to set's item list
                if (!set.Items.Contains(this)) set.Items.Add(this);
            }
        }

        protected internal bool Picked
        {
            get { return this.picked; }
            set { this.picked = value; }
        }

        protected internal bool ChosenForPicking
        {
            get { return this.chosenForPicking; }
            set { this.chosenForPicking = value; }
        }

        protected internal bool ChosenForSitting
        {
            get { return this.chosenForSitting; }
            set { this.chosenForSitting = value; }
        } 

        #endregion

        #region ION Methods

        protected override void OnCreate(Arguments arguments)
        {
            Console.WriteLine(Universe.Instance.Tick + ": CREATED " + this.Name + " [ION.Realizer.Orient.Item]");

            // load item
            this.asset = OrientGraphicsRealizer.Instance.LoadItem(arguments.Get<string>(ARG_TYPE_STR));
            //this.GraphicalAsset.ShowAllDebuggingSpots();
            //this.GraphicalAsset.Node.ShowBoundingBox = true;

            // placement
            this.CreateAction<Place>(ACTION_PLACE_STR);
            this.CreateAction<Unplace>(ACTION_UNPLACE_STR);
        }

        protected override void OnDestroy()
        {
            Console.WriteLine(Universe.Instance.Tick + ": DESTROYED " + this.Name + " [ION.Realizer.Orient.Item]");

            // unload item
            OrientGraphicsRealizer.Instance.RemoveItem(this.GraphicalAsset);
        } 

        #endregion
    }
}
