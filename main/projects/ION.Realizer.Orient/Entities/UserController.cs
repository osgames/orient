using System;
using System.Collections.Generic;
using System.Text;
using ION.Core;
using ION.Core.Extensions;
using AMS.Profile;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using ION.Realizer.Orient.Actions.UserController;
using User.Interface;

namespace ION.Realizer.Orient.Entities
{
    public class UserController : Entity
    {
        #region Constants
        public const string ENTITY_NAME = "UserController";
        #endregion

        public ServerSocket master;
        public string gestureContext = "";
        public string charNameContext = "";
        public string itemContext = "";
        public string leaveSceneDestinationContext = "";
        public bool buttonContext = false;
        public bool button1AlreadyPressed = false;
        public bool button2AlreadyPressed = false;
        public bool trainingMode = true;

        public void EndTrainingMode()
        {
            trainingMode = false;
            clearContext();

        }

        protected override void OnCreate(Arguments arguments)
        {
            this.CreateAction<UserGestureAction>(UserGestureAction.ACTION_NAME);
            this.CreateAction<UserNavigationAction>(UserNavigationAction.ACTION_NAME);
            this.CreateAction<UserKeyboardAction>(UserKeyboardAction.ACTION_NAME);
            this.CreateAction<UserOptionAction>(UserOptionAction.ACTION_NAME);
            this.CreateAction<UserSelectItemAction>(UserSelectItemAction.ACTION_NAME);
            this.CreateAction<UserMobilePhoneButtonAction>(UserMobilePhoneButtonAction.ACTION_NAME);
            this.CreateAction<UserSpeechRecogAction>(UserSpeechRecogAction.ACTION_NAME);
            this.CreateAction<UserWiiButtonAction>(UserWiiButtonAction.ACTION_NAME);

            // create the server socket

            int port = 45700; // default port
            //Read port from Orient's profile
            //AMS.Profile.dll -> http://www.codeproject.com/csharp/ReadWriteXmlIni.asp            
            Xml profile = new Xml("OrientProfile.xml");
            try
            {
                using (profile.Buffer())
                {
                    port = int.Parse((string)profile.GetValue("Application", "InteractionServerPort"));
                }
            }
            catch (Exception ex)
            {
                ApplicationLogger.Instance().WriteLine("Error reading orient profile (UserController): " + ex.Message);
            }


            this.master = new ServerSocket(port);
            this.master.startListening();
            //create the proxy action to monitor the Server Socket
            UserInterfaceProxyAction proxyAction = this.CreateAction<UserInterfaceProxyAction>(UserInterfaceProxyAction.ACTION_NAME);
            proxyAction.Start();
        
        }

        public void clearContext()
        {
            gestureContext = "";
            charNameContext = "";
            itemContext = "";
            //follwing the new value of the inputContext variable, change the display system state on the 2D overlay
            UserInterface.Instance.updateSystemButtontoIdle();
            User.Interface.UserInterface.Instance.clearContext();
        }

    }

    // State object for reading client data asynchronously
    public class StateObject
    {
        // Client  socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 1024;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Received data string.
        public StringBuilder sb = new StringBuilder();
    }
    public class ServerSocket
    {
        int port;
        Socket listener;
        public Boolean isReadyToRead;
        public String input;
        public StateObject state;


        // Thread signal.
        public ManualResetEvent allDone = new ManualResetEvent(false);
        public ServerSocket(int p)
        {
            this.port = p;
            this.isReadyToRead = false;
            this.input = null;
            this.state = null;
        }

        public void startListening()
        {
            Thread thread = new Thread(new ThreadStart(this.threadTask));
            thread.IsBackground = true;
            thread.Start();
        }



        public void threadTask()
        {
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];

            // Establish local endpoint...
            IPAddress ipAddress = IPAddress.Any;
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, this.port);
            // Create a TCP/IP socket...
            this.listener = new Socket(AddressFamily.Unspecified, SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.
            try
            {
                this.listener.Bind(localEndPoint);
                this.listener.Listen(100);

                while (true)
                {
                    // Set the event to nonsignaled state.
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.
                    // Console.WriteLine("Waiting for a connection...");
                    listener.BeginAccept(
                        new AsyncCallback(AcceptCallback),
                        listener);

                    // Wait until a connection is made before continuing.
                    allDone.WaitOne();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            //Console.WriteLine("\nPress ENTER to continue...");
            // Console.Read();

        }

        public void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.
            allDone.Set();

            // Get the socket that handles the client request.
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            // Create the state object.
            this.state = new StateObject();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(this.ReadCallback), state);
        }

        public void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;

            // Retrieve the state object and the handler socket
            // from the asynchronous state object.
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;

            // Read data from the client socket. 
            int bytesRead = handler.EndReceive(ar);

            if (bytesRead > 0)
            {
                // There  might be more data, so store the data received so far.
                state.sb.Append(Encoding.ASCII.GetString(
                    state.buffer, 0, bytesRead));

                // Check for end-of-file tag. If it is not there, read 
                // more data.
                content = state.sb.ToString();
                if (content.IndexOf("<EOF>") > -1)
                {
                    // All the data has been read from the 
                    // client. Display it on the console.
                    //Console.WriteLine("Read {0} bytes from socket. \n Data : {1}",
                    //   content.Length, content);
                    // Echo the data back to the client.
                    this.input = content;
                    this.isReadyToRead = true;
                    //Send(handler, content);
                }
                else
                {
                    // Not all data received. Get more.
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
                }
            }
        }

        public void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
                //Console.WriteLine("Sent {0} bytes to client.", bytesSent);
                this.input = null;
                this.isReadyToRead = false;
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

    }

}
