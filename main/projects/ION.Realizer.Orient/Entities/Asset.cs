/* ****************************************************
 * Name: Asset.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/27
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using PAGE.Generic.Domain.Assets;

namespace ION.Realizer.Orient.Entities
{
    public class Asset : Entity
    {
        protected GenericAsset asset;

        internal GenericAsset GraphicalAsset
        {
            get { return this.asset; }
        }
    }
}