/* ****************************************************
 * Name: Set.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections.Generic;
using ION.Core;
using ION.Realizer.Orient.Actions.Camera;
using ION.Realizer.Orient.Actions.Environment;
using ION.Realizer.Orient.Entities;
using PAGE.Orient;
using PAGE.Orient.Domain.Assets;

namespace ION.Realizer.Orient.Entities
{
    public class Set : Asset
    {
        public const string ARG_SET_TYPE_STR = "type";

        protected List<Item> items = new List<Item>();

        public new OrientSet GraphicalAsset
        {
            get { return this.asset as OrientSet; }
        }

        protected internal List<Item> Items
        {
            get { return items; }
        }

        protected override void OnCreate(Arguments arguments)
        {
            Console.WriteLine(Universe.Instance.Tick + ": CREATED " + this.Name + " [ION.Realizer.Orient.Set]");

            // load set
            this.asset = OrientGraphicsRealizer.Instance.LoadSet(
                arguments.Get<string>(ARG_SET_TYPE_STR));
            this.asset.Visible = true;
            //this.GraphicalAsset.ShowAllDebuggingSpots();

            //place camera on set
            Camera camera;
            if (Universe.Instance.TryGetEntity(Constants.ORIENT_CAMERA_NAME, out camera))
            {
                Context camContext = new Context();
                camContext[PlaceOnSet.PARAM_SET_ID_STR] = this.Name;
                camera.GetAction(Camera.ACTION_PLACE_ON_SET_STR).Start(camContext);
            }

            //create environment actions
            CreateAction<MeteorAttack>("MeteorAttack");
        }

        protected override void OnDestroy()
        {
            Console.WriteLine(Universe.Instance.Tick + ": DESTROYED " + this.Name + " [ION.Realizer.Orient.Set]");

            // unload set
            OrientGraphicsRealizer.Instance.RemoveSet(this.GraphicalAsset);
        }
    }
}
