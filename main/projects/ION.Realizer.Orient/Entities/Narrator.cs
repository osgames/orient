/* ****************************************************
 * Name: Narrator.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;
using ION.Realizer.Orient.Actions.Narrator;

namespace ION.Realizer.Orient.Entities
{
    public sealed class Narrator : Entity
    {
        protected override void OnCreate(Arguments arguments)
        {
            this.CreateAction<Say>("say");
        }

        protected override void OnDestroy()
        {
        }
    }
}
