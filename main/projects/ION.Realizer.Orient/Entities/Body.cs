/* ****************************************************
 * Name: Body.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 *       Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections.Generic;
using ION.Core;
using ION.Realizer.Orient.Actions.Body;
using PAGE.Orient;
using PAGE.Orient.Domain.Assets;

namespace ION.Realizer.Orient.Entities
{
    public class Body : Asset
    {
        #region Constants

        //spots
        public const string SPOT_TAG_TALK = "talk";
        public const string SPOT_TAG_GIVE_OBJECT = "give-object";

        //properties
        public const string PROPERTY_PLACE_STR = "place";
        public const string PROPERTY_IS_SEATED_STR = "isSeated";

        //actions
        public const string ACTION_PLACE_STR = "enter-stage";
        public const string ACTION_UNPLACE_STR = "leave-stage";
        public const string ACTION_WALK_TO_STR = "walk-to-action";
        public const string ACTION_TURN_TO_STR = "turn-to-action";
        public const string ACTION_SPEAK_STR = "speak-action";
        public const string ACTION_STAND_STR = "stand-action";
        public const string ACTION_TALK_STR = "talk-action";
        public const string ACTION_THUMB_UP_STR = "thumb-up-action";
        public const string ACTION_WAVE_HAND_STR = "wave-hand-action";
        public const string ACTION_EAT_STR = "eat-action";
        public const string ACTION_BOW_STR = "bow-action";
        public const string ACTION_DIAGONAL_HAND_RISE_STR = "diagonal-hand-rise-action";
        public const string ACTION_GIVE_OBJ_FRONT_STR = "give-object-front-action";
        public const string ACTION_RECEIVE_OBJ_FRONT_STR = "receive-object-front-action";
        public const string ACTION_SPIT_STR = "spit-action";
        public const string ACTION_PICK_FLOOR_STR = "pick-floor-action";
        public const string ACTION_PICK_TABLE_STR = "pick-table-action";
        public const string ACTION_DROP_FLOOR_STR = "drop-floor-action";
        public const string ACTION_DROP_TABLE_STR = "drop-table-action";
        public const string ACTION_SIT_DOWN_STR = "sit-down-action";
        public const string ACTION_GET_UP_STR = "get-up-action";
        public const string ACTION_GET_UP_RIGHT_STR = "get-up-right-action";
        public const string ACTION_CHANGE_CLOTHES_STR = "change-clothes";
        public const string ACTION_CHANGE_FACE_STR = "change-face";
        public const string ACTION_HANDSTRIKE_STR = "handstrike-action";
        public const string ACTION_PICK_FROM_TREE_STR = "pick-from-tree-action";
        public const string ACTION_HANDRISE_STR = "handrise-action";
        public const string ACTION_ELBOWSTRIKE_STR = "elbowstrike-action";
        public const string ACTION_FACEWAVE_STR = "facewave-action";
        public const string ACTION_NIGHTFEVER_STR = "nightfever-action";
        public const string ACTION_REST_STR = "rest-action";
        public const string ACTION_CLAP_ABOVE_HEAD_STR = "clap-above-head-action";
        public const string ACTION_POINT_AT_STR = "point-action";
        public const string ACTION_PRESS_BUTTON_STR = "press-button-action";
        public const string ACTION_FALL_STR = "fall-action";
        public const string ACTION_LIE_STR = "lie-action";
        public const string ACTION_GETUP_FALLEN_STR = "getup-fallen-action";


        public const string ARG_TYPE_STR = "type";

        #endregion

        #region Fields

        protected Set set;

        protected List<Item> pickedItems = new List<Item>();

        protected Item seatedOnItem;
        protected bool isSeated;

        protected string currentPlaceID;
        protected Property<string> currentPlace;

        #endregion

        #region Properties

        protected internal bool IsSeated
        {
            get { return seatedOnItem != null; }
            set { isSeated = value; }
        }

        protected internal Item SeatedOnItem
        {
            get { return seatedOnItem; }
            set { seatedOnItem = value; }
        }

        protected internal Set Set
        {
            get { return set; }
            set { set = value; }
        }

        protected internal string CurrentPlaceID
        {
            get { return currentPlaceID; }
            set { currentPlaceID = value; }
        }

        public new OrientCharacter GraphicalAsset
        {
            get { return this.asset as OrientCharacter; }
        }

        public List<Item> PickedItems
        {
            get { return pickedItems; }
        }

        protected internal Property<string> CurrentPlace
        {
            get { return currentPlace; }
        }

        #endregion

        #region ION Methods

        protected override void OnCreate(Arguments arguments)
        {
            Console.WriteLine(Universe.Instance.Tick + ": CREATED " + this.Name + " [ION.Realizer.Orient.Body]");

            // load body
            this.asset = OrientGraphicsRealizer.Instance.LoadCharacter(
                arguments.Get<string>(ARG_TYPE_STR));

            //this.GraphicalAsset.ShowAllDebuggingSpots();
            //this.GraphicalAsset.Node.ShowBoundingBox = true;
            
            this.CreateActions();
            this.CreateProperties();

            this.LeftPlace();   //starts with no position
        }

        protected override void OnDestroy()
        {
            Console.WriteLine(Universe.Instance.Tick + ": DESTROYED " + this.Name + " [ION.Realizer.Orient.Body]");

            // unload body
            OrientGraphicsRealizer.Instance.RemoveCharacter(this.GraphicalAsset);

            this.PickedItems.Clear();
        } 

        #endregion

        #region Initialization Methods

        protected virtual void CreateProperties()
        {
            //create propterties
            this.currentPlace = this.CreateProperty<Property<string>>(PROPERTY_PLACE_STR);
        }

        protected virtual void CreateActions()
        {
            this.CreateResourceActions();
            this.CreateAnimateActions();

            Dictionary<string, object> args = new Dictionary<string, object>();

            // placement
            this.CreateAction<Place>(ACTION_PLACE_STR);
            this.CreateAction<Unplace>(ACTION_UNPLACE_STR);

            // move actions
            args[Move.ARG_ACTION_ID_STR] = "walk";
            this.CreateAction<Move>(ACTION_WALK_TO_STR, new Arguments(args));
            args.Clear();

            // turn
            this.CreateAction<Turn>(ACTION_TURN_TO_STR);
        }

        protected virtual void CreateAnimateActions()
        {
            Dictionary<string, object> args = new Dictionary<string, object>();

            // animate forever actions
            args[Animate.ARG_START_RANDOM_STR] = true;
            args[Animate.ARG_ANIMATE_FOREVER_STR] = true;
            args[Animate.ARG_ACTION_ID_STR] = "idle";
            args[Animate.ARG_SEATED_ACTION_ID_STR] = "idle-sitting";
            this.CreateAction<Animate>(ACTION_STAND_STR, new Arguments(args)).Start();

            args[Animate.ARG_ACTION_ID_STR] = "talking";
            args[Animate.ARG_SEATED_ACTION_ID_STR] = "talking-sitting";
            this.CreateAction<Animate>(ACTION_TALK_STR, new Arguments(args));

            // speak
            this.CreateAction<Speak>(ACTION_SPEAK_STR, new Arguments(args));

            //lie on floor
            args[Animate.ARG_ACTION_ID_STR] = "onback-hurt";
            args[Animate.ARG_SEATED_ACTION_ID_STR] = "onback-hurt";
            this.CreateAction<Animate>(ACTION_LIE_STR, new Arguments(args));           
            args.Clear();

            // simple animation actions
            args[Animate.ARG_ANIMATE_FOREVER_STR] = false;
            args[Animate.ARG_ACTION_ID_STR] = "idle";
            this.CreateAction<Animate>(ACTION_REST_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "thumb-up";
            this.CreateAction<Animate>(ACTION_THUMB_UP_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "wave-hand";
            this.CreateAction<Animate>(ACTION_WAVE_HAND_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_START_RANDOM_STR] = true;
            args[Animate.ARG_ACTION_ID_STR] = "eat";
            args[Animate.ARG_SEATED_ACTION_ID_STR] = "eat-sitting";
            this.CreateAction<Animate>(ACTION_EAT_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "bow";
            this.CreateAction<Animate>(ACTION_BOW_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "diagonal-hand-rise";
            this.CreateAction<Animate>(ACTION_DIAGONAL_HAND_RISE_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "handstrike";
            this.CreateAction<Animate>(ACTION_HANDSTRIKE_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "handrise";
            this.CreateAction<Animate>(ACTION_HANDRISE_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "elbowstrike";
            this.CreateAction<Animate>(ACTION_ELBOWSTRIKE_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "facewave";
            this.CreateAction<Animate>(ACTION_FACEWAVE_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "nightfever";
            this.CreateAction<Animate>(ACTION_NIGHTFEVER_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "clap-above-head";
            this.CreateAction<Animate>(ACTION_CLAP_ABOVE_HEAD_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "give-object-front";
            this.CreateAction<Animate>(ACTION_GIVE_OBJ_FRONT_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "receive-object-front";
            this.CreateAction<Animate>(ACTION_RECEIVE_OBJ_FRONT_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "spit-standing";
            args[Animate.ARG_SEATED_ACTION_ID_STR] = "spit-sitting";
            this.CreateAction<Animate>(ACTION_SPIT_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "point";
            this.CreateAction<Animate>(ACTION_POINT_AT_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "press-button";
            this.CreateAction<Animate>(ACTION_PRESS_BUTTON_STR, new Arguments(args));
            args.Clear();

            //pick actions
            args[Animate.ARG_ACTION_ID_STR] = "pick-floor";
            args[Animate.ARG_SEATED_ACTION_ID_STR] = "pick-floor-sitting";
            this.CreateAction<Pick>(ACTION_PICK_FLOOR_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "pick-table";
            args[Animate.ARG_SEATED_ACTION_ID_STR] = "pick-table-sitting";
            this.CreateAction<Pick>(ACTION_PICK_TABLE_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "pick-tree";
            this.CreateAction<Pick>(ACTION_PICK_FROM_TREE_STR, new Arguments(args));
            args.Clear();

            //drop actions
            args[Animate.ARG_ACTION_ID_STR] = "drop-floor";
            args[Animate.ARG_SEATED_ACTION_ID_STR] = "drop-floor-sitting";
            this.CreateAction<Drop>(ACTION_DROP_FLOOR_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "drop-table";
            args[Animate.ARG_SEATED_ACTION_ID_STR] = "drop-table-sitting";
            this.CreateAction<Drop>(ACTION_DROP_TABLE_STR, new Arguments(args));
            args.Clear();

            //sit down action
            args[Animate.ARG_ACTION_ID_STR] = "sit-down";
            args[SitDown.ARG_ACTION_SIT_FRONT_STR] = "sit-down";
            args[SitDown.ARG_ACTION_SIT_LEFT_STR] = "sit-down-left";
            args[SitDown.ARG_ACTION_SIT_RIGHT_STR] = "sit-down-left";
            this.CreateAction<SitDown>(ACTION_SIT_DOWN_STR, new Arguments(args));
            args.Clear();

            //get up actions
            args[Animate.ARG_ACTION_ID_STR] = "get-up";
            this.CreateAction<GetUp>(ACTION_GET_UP_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "get-up-right";
            this.CreateAction<GetUp>(ACTION_GET_UP_RIGHT_STR, new Arguments(args));
            args.Clear();

            // falling and getting up from floor
            args[Animate.ARG_ACTION_ID_STR] = "hitonhead-collapse";
            this.CreateAction<Animate>(ACTION_FALL_STR, new Arguments(args));
            args.Clear();

            args[Animate.ARG_ACTION_ID_STR] = "hitonhead-getup";
            this.CreateAction<Animate>(ACTION_GETUP_FALLEN_STR, new Arguments(args));
            args.Clear();

        }

        protected virtual void CreateResourceActions()
        {
            Dictionary<string, object> args = new Dictionary<string, object>();

            // change face and clothes
            args[ChangeResource.ARG_RESOURCE_STR] = ChangeResource.RESOURCE_FACE_STR;
            Action changeFace = this.CreateAction<ChangeResource>(
                ACTION_CHANGE_FACE_STR, new Arguments(args));
            args[Animate.ARG_SEATED_ACTION_ID_STR] = ChangeResource.RESOURCE_BODY_STR;
            this.CreateAction<ChangeResource>(ACTION_CHANGE_CLOTHES_STR, new Arguments(args));
            args.Clear();

            //initial body parameters
            Context context = new Context();
            context[ChangeResource.PARAM_EXPRESSION_STR] = "natural";
            changeFace.Start(context);
        }

        #endregion

        #region Protected Methods

        protected internal void LeftPlace()
        {
            this.CurrentPlace.Value = Place.NO_PLACE_STR;
        }

        protected internal void ArrivedPlace()
        {
            this.CurrentPlace.Value = this.currentPlaceID;
        } 

        #endregion
    }
}
