/* ****************************************************
 * Name: FreeNavigationModeChanged.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/28
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;

namespace ION.Realizer.Orient.Events
{
    public class FreeNavigationModeChanged : Event
    {
        public bool keyboardCapture = true;
        public bool mouseCapture = true;

        public FreeNavigationModeChanged(bool keyboardCapture, bool mouseCapture)
        {
            this.keyboardCapture = keyboardCapture;
            this.mouseCapture = mouseCapture;
        }
    }
}