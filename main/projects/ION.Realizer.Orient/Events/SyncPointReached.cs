/* ****************************************************
 * Name: SyncPointReached.cs
 * Code: Marco Vala - marco.vala@gaips.inesc-id.pt
 * Date: 2007/01/01
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using ION.Core;

namespace ION.Realizer.Orient.Events
{
    public sealed class SyncPointReached : Event
    {
        private Action action;
        private string syncPoint;

        internal SyncPointReached(Action action, string syncPoint)
        {
            this.action = action;
            this.syncPoint = syncPoint;
        }

        public Action Action
        {
            get { return this.action; }
        }

        public string SyncPoint
        {
            get { return this.syncPoint; }
        }
    }
}