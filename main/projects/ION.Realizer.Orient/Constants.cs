/* ****************************************************
 * Name: Constants.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/08/29
 * 
 * Inesc-ID GAIPS
 * 
 *****************************************************/
namespace ION.Realizer.Orient
{
    /// <summary>
    /// Contains the definition of all constants used in Orient's ION layer.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Character and item fade time when (dis)appearing.
        /// </summary>
        public const float FADE_TIME = 1;

        /// <summary>
        /// Name of current ION set entity.
        /// </summary>
        public const string SET_ENTITY_NAME = "SceneryEntity";

        /// <summary>
        /// Name of the orient ION camera entity.
        /// </summary>
        public const string ORIENT_CAMERA_NAME = "orient-camera";

        public const string FATIMA_TRUE_VALUE = "True";
        public const string FATIMA_FALSE_VALUE = "False";

        /// <summary>
        /// Name of ION log file.
        /// </summary>
        public const string ION_LOG_FILE = "ION.log";
    }
}
