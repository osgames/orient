using PAGE.Generic;
using PAGE.Generic.Resources.Actions;
using PAGE.Orient.Domain.Assets;

namespace DemoFormApp
{
    class TalkActionCallbackClass : ActionCallback
    {
        protected OrientCharacter character = null;
        protected string utterance;
        protected bool gibberish = false;

        public TalkActionCallbackClass(OrientCharacter character, string utterance, bool gibberish)
        {
            this.character = character;
            this.utterance = utterance;
            this.gibberish = gibberish;
        }

        protected override void Succeeded()
        {
            this.character.Animate(null, "talking", true);
            this.character.ShowSubtitles(this.utterance);
            this.character.SaySpeech(
                new StandActionCallbackClass(this.character), this.utterance, this.gibberish);
        }

        protected override void Failed()
        {
            this.Succeeded();
        }

        protected override void Triggered(ActionTrigger trigger)
        {
        }
    }
}
