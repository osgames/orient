/* ****************************************************
 * Name: Program.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Windows.Forms;
using ION.Core;
using PAGE.Orient;

namespace DemoFormApp
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            using (MainForm frm = new MainForm())
            {
                DateTime timeBeforeION = DateTime.Now;
                DateTime timeBeforeOGRE = DateTime.Now;

                frm.Show();

                Universe.Instance.Update();
                Universe.Instance.Update();
                Universe.Instance.Update();

                frm.Init();

                while (frm.Created)
                {
                    if ((DateTime.Now - timeBeforeOGRE).Milliseconds > 15)
                    {
                        timeBeforeOGRE = DateTime.Now;
                        OrientGraphicsRealizer.Instance.Update();
                    }

                    if ((DateTime.Now - timeBeforeION).Milliseconds > 60)
                    {
                        timeBeforeION = DateTime.Now;
                        Universe.Instance.Update();
                    }

                    Application.DoEvents();
                }
            }
        }
    }
}