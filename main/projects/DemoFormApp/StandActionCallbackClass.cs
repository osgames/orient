using System;
using System.Windows.Forms;
using PAGE.Generic;
using PAGE.Generic.Resources.Actions;
using PAGE.OGRE.Domain.Assets;
using PAGE.Orient;

namespace DemoFormApp
{
    public class StandActionCallbackClass : ActionCallback
    {
        protected OgreCharacter character = null;
        protected TextBox textbox = null;

        public TextBox TextBox
        {
            get { return textbox; }
            set { textbox = value; }
        }

        public StandActionCallbackClass(OgreCharacter character)
        {
            this.character = character;
        }

        protected override void Succeeded()
        {
            this.character.Animate(null, "idle-standing");
            //TODO facial// this.character.ChangeFace("natural");
            this.character.HideSubtitles();
        }

        protected override void Failed()
        {
            OrientGraphicsRealizer.Instance.SaveScreenshot("screenshot.png");
        }

        protected override void Triggered(ActionTrigger trigger)
        {
            Console.WriteLine("trigger");
            if (this.textbox != null)
                this.textbox.Text = trigger.IdToken + ": " + trigger.Time + "secs";
        }
    }
}
