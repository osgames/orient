using System;
using System.Windows.Forms;
using PAGE.Generic;
using PAGE.Generic.Resources.Actions;
using PAGE.OGRE.Domain.Assets;
using PAGE.Orient;

namespace DemoFormApp
{
    class PushActionCallbackClass : ActionCallback
    {
        protected OgreCharacter character1 = null;
        protected OgreCharacter character2 = null;
        protected TextBox textbox = null;
        protected int successes = 0;

        public TextBox TextBox
        {
            get { return textbox; }
            set { textbox = value; }
        }

        public PushActionCallbackClass(OgreCharacter character1, OgreCharacter character2)
        {
            this.character1 = character1;
            this.character2 = character2;
            this.ignoreMultipleResponses = false;
        }

        protected override void Succeeded()
        {
            if (successes == 0)
            {
                this.successes++;
                this.character1.Animate(this, "give-object-front");
                //TODO facial// this.character1.ChangeFace("natural");
            }
            else if (successes == 1)
            {
                this.successes++;
                this.character1.Animate(this, "idle-standing");
                OrientGraphicsRealizer.Instance.RemoveCharacter(this.character2);
            }
            else
            {
                this.character1.Animate(null, "idle-standing");
            }
        }

        protected override void Failed()
        {
            OrientGraphicsRealizer.Instance.SaveScreenshot("screenshot.png");
            this.character1.Animate(null, "idle-standing");
            //TODO facial// this.character1.ChangeFace("natural");
        }

        protected override void Triggered(ActionTrigger trigger)
        {
            Console.WriteLine("trigger");
            if (this.textbox != null)
                this.textbox.Text = trigger.IdToken + ": " + trigger.Time + "secs";
        }
    }
}
