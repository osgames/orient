/* ****************************************************
 * Name: Form1.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ION.Core;
using ION.Realizer.Orient;
using ION.Realizer.Orient.Actions.Body.Behaviours;
using ION.Realizer.Orient.Entities;
using Mogre;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.GUI;
using PAGE.Generic.Resources.Spots;
using PAGE.OGRE;
using PAGE.OGRE.GUI;
using PAGE.OGRE.Math;
using PAGE.Orient;
using PAGE.Orient.Domain.Assets;
using PAGE.Orient.Domain.Cameras;
using PAGE.Orient.GUI.Windows.Forms.Util;
using WMPLib;
using Camera=ION.Realizer.Orient.Entities.Camera;
using Form=System.Windows.Forms.Form;

namespace DemoFormApp
{
    public partial class MainForm : Form
    {
        private bool bodyCam;
        private Camera camera;
        private Item table, cheese, chair, throne, plate, pizza, plate2, meat;
        private Character character, character2;
        private Set set1, set2, currentSet;
        private Context context = new Context();
        private int curWaypointIndex;
        private OgreStaticDisplayText ogreTextBox;
        private Random random = new Random((int) DateTime.Now.Ticks);
        private int screenshotCount;
        private bool showStats = true;

        public MainForm()
        {
            InitializeComponent();
            this.FormClosing += Form1_FormClosing;

            OrientGraphicsRealizer.Instance.Setup("./data/resources.xml", this.panel1);

            LogManager.Singleton.CreateLog(Constants.ION_LOG_FILE, false, true, false);

            Dictionary<string, Object> args = new Dictionary<string, object>();
            args["type"] = "chartestset";
            this.set1 = Universe.Instance.CreateEntity<Set>("chartestset", new Arguments(args));
            this.currentSet = this.set1;

            args["type"] = "netball_court";
            this.set2 = Universe.Instance.CreateEntity<Set>("netball_court", new Arguments(args));

            args["type"] = "helper";
            this.character = Universe.Instance.CreateEntity<Character>("helper", new Arguments(args));
            args["type"] = "helper";
            this.character2 = Universe.Instance.CreateEntity<Character>("helper-red", new Arguments(args));

            args["type"] = "table";
            this.table = Universe.Instance.CreateEntity<Item>("table", new Arguments(args));
            args["type"] = "cheese";
            this.cheese = Universe.Instance.CreateEntity<Item>("cheese", new Arguments(args));
            args["type"] = "chair";
            this.chair = Universe.Instance.CreateEntity<Item>("chair", new Arguments(args));
            args["type"] = "throne";
            this.throne = Universe.Instance.CreateEntity<Item>("throne", new Arguments(args));
            args["type"] = "plate";
            this.plate = Universe.Instance.CreateEntity<Item>("plate", new Arguments(args));
            args["type"] = "food-pizza";
            this.pizza = Universe.Instance.CreateEntity<Item>("food-pizza", new Arguments(args));
            args["type"] = "plate";
            this.plate2 = Universe.Instance.CreateEntity<Item>("plate2", new Arguments(args));
            args["type"] = "food-meat-potatoes";
            this.meat = Universe.Instance.CreateEntity<Item>("meat-potatoes", new Arguments(args));

            this.camera = Universe.Instance.CreateEntity<Camera>("orient-camera");

            this.ogreTextBox = new OgreStaticDisplayText(this.textBox.Text);
            OrientGraphicsRealizer.Instance.GUIManager.AddText(this.ogreTextBox);
            this.ogreTextBox.Position = new OgreScreenPosition(10, 10);
            this.ogreTextBox.Alignment = TextAlignment.Left;
            this.ogreTextBox.CharHeight = 30;
            this.colorDialog1.Color =
                GUIUtil.ConvertToColor((OgreColor) this.ogreTextBox.Color);

            this.panel1.AllowNavigation = true;
        }

        private bool init = true;

        public void Init()
        {
            if (!init) return;
            init = false;

            this.set1.GraphicalAsset.ShowAllDebuggingSpots();
            this.set2.GraphicalAsset.ShowAllDebuggingSpots();
            this.set2.GraphicalAsset.Visible = false;

            this.context["set"] = this.set1.Name;
            this.context["spot"] = this.set1.GraphicalAsset.ResourceAsset.ResourceWaypoints[1].IdToken;
            this.character.GetAction("enter-stage").Start(this.context);
            this.character.GraphicalAsset.ChangeFace("happy", 0.6f);

            this.context["spot"] = this.set1.GraphicalAsset.ResourceAsset.ResourceWaypoints[2].IdToken;
            this.character2.GetAction("enter-stage").Start(this.context);
            this.character2.GraphicalAsset.ChangeFace("sad", 1);
            this.character2.GraphicalAsset.ChangeTexture("helper_red.jpg");

            this.context["target"] = this.set1.Name;
            this.context["spot"] = "table-spot";
            this.table.GetAction("place-item").Start(this.context);
            this.table.GraphicalAsset.ShowAllDebuggingSpots();

            this.context["target"] = this.set1.Name;
            this.context["spot"] = "drop-spot";
            this.cheese.GetAction("place-item").Start(this.context);
            this.cheese.GraphicalAsset.ShowAllDebuggingSpots();

            this.context["target"] = this.table.Name;
            this.context["spot"] = "chair2";
            this.chair.GetAction("place-item").Start(this.context);
            this.chair.GraphicalAsset.ShowAllDebuggingSpots();

            this.context["target"] = this.table.Name;
            this.context["spot"] = "throne";
            this.throne.GetAction("place-item").Start(this.context);
            this.throne.GraphicalAsset.ShowAllDebuggingSpots();

            this.context["target"] = this.table.Name;
            this.context["spot"] = "plate1";
            this.plate.GetAction("place-item").Start(this.context);

            this.context["target"] = this.plate.Name;
            this.context["spot"] = "food";
            this.pizza.GetAction("place-item").Start(this.context);

            this.context["target"] = this.table.Name;
            this.context["spot"] = "plate2";
            this.plate2.GetAction("place-item").Start(this.context);

            this.context["target"] = this.plate2.Name;
            this.context["spot"] = "food";
            this.meat.GetAction("place-item").Start(this.context);

            OrientGraphicsRealizer.Instance.Camera.CameraMode = CameraMode.Free;
            this.camera.GetAction("place-on-set").Start(this.context);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            LogManager.Singleton.DestroyLog(Constants.ION_LOG_FILE);
            OrientGraphicsRealizer.Instance.Destroy();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.character == null) return;
            if (this.character.GraphicalAsset.Visible) this.character.GraphicalAsset.FadeOut(1);
            else this.character.GraphicalAsset.FadeIn(1);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (this.character != null)
            {
                this.character.GetAction("stand-action").Start();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (this.currentSet == null) return;
            this.currentSet.GraphicalAsset.Visible = !this.currentSet.GraphicalAsset.Visible;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (this.character != null)
            {
                StandActionCallbackClass callback = new StandActionCallbackClass(this.character.GraphicalAsset);
                callback.TextBox = this.textBox3;
                this.character.GraphicalAsset.MoveAway(callback, "walk", this.currentSet.GraphicalAsset);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Dictionary<string, Object> args = new Dictionary<string, object>();
            args["type"] = "helper-blue";
            this.character2 = Universe.Instance.CreateEntity<Character>("helper-blue", new Arguments(args));

            this.context["set"] = this.set1.Name;
            this.context.Keys.Remove("spot");
            this.character.GetAction("enter-stage").Start(this.context);

            int index = this.random.Next(0, this.currentSet.GraphicalAsset.Waypoints.Count - 1);
            new RandomRunCallbackClass(this.currentSet.GraphicalAsset, this.character2.GraphicalAsset).SendSuccess();
            /*this.character2.Place(null, this.currentSet, "waypoint0", false);
            this.character2.Move(new StandActionCallbackClass(this.character2), 
                "run-fearful", this.currentSet, this.currentSet.Waypoints[index].ResourceSpot.IdToken);
             */

            PushActionCallbackClass callback = new PushActionCallbackClass(
                this.character.GraphicalAsset, this.character2.GraphicalAsset);
            callback.TextBox = this.textBox3;
            this.character.GraphicalAsset.Move(callback, "walk", this.character2.GraphicalAsset, "give-object");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (this.character == null) return;
            this.context[SayTo.PARAM_UTTERANCE_STR] = this.textBox4.Text;
            this.context[TurnTo.PARAM_TARGET_STR] = this.character2.Name;
            this.character.GetAction(Character.BEHAVIOUR_SAY_TO_STR).Start(this.context);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.init) return;
            if (this.character == null) return;

            this.listBox1.Items.Clear();
            this.textBox2.Text = "";

            OgrePosition position = (OgrePosition) this.character.GraphicalAsset.AbsolutePosition;
            this.textBox1.Text =
                "x = " + (int) position.X + ", y = " + (int) position.Y + ", z = " + (int) position.Z;

            GenericSpot curSpot = (GenericSpot) this.character.GraphicalAsset.CurrentSpot;
            if (curSpot != null)
            {
                foreach (string tag in curSpot.ResourceSpot.Tags)
                {
                    this.listBox1.Items.Add(tag);
                }
                this.textBox2.Text = curSpot.ResourceSpot.IdToken;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.character != null)
            {
                this.character.GraphicalAsset.StopMoving();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OrientGraphicsRealizer.Instance.SaveScreenshot(
                "screenshot" + this.screenshotCount++ + ".png");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            OrientCharacter c = (OrientCharacter) OrientGraphicsRealizer.Instance.LoadCharacter("helper");
            c.Visible = true;
            c.Place(null, this.character.GraphicalAsset, "carry", true);
        }

        private void hideTxtBtn_Click(object sender, EventArgs e)
        {
            this.ogreTextBox.Text = this.textBox.Text;
            this.ogreTextBox.Visible = !this.ogreTextBox.Visible;
        }

        private void changeColorBtn_Click(object sender, EventArgs e)
        {
            if (this.colorDialog1.ShowDialog() == DialogResult.OK)
            {
                this.ogreTextBox.Color = GUIUtil.ConvertToOgreColor(this.colorDialog1.Color);
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (this.character2 != null)
            {
                this.context["target"] = this.character2.Name;
                this.character.GetAction("turn-to-action").Start(this.context);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.currentSet.GraphicalAsset.Reset();

            if (this.currentSet == this.set1) this.currentSet = this.set2;
            else this.currentSet = this.set1;

            this.currentSet.GraphicalAsset.Visible = true;

            this.context["set"] = this.currentSet.Name;
            this.camera.GetAction("place-on-set").Start(this.context);

            this.character.GetAction("leave-stage").Start();

            //this.character.GraphicalAsset.Reset();
            int index = this.random.Next(0, this.currentSet.GraphicalAsset.Waypoints.Count - 1);
            this.context["spot"] = this.currentSet.GraphicalAsset.Waypoints[index].ResourceSpot.IdToken;
            this.character.GetAction("enter-stage").Start(this.context);

            if ((this.character2 != null) && !this.character2.GraphicalAsset.Destroyed)
            {
                Universe.Instance.DestroyEntity(this.character2.Name);
                this.character2 = null;
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            this.bodyCam = !this.bodyCam;
            this.context["character"] = this.character.Name;
            if (this.bodyCam)
            {
                OrientGraphicsRealizer.Instance.Camera.PlaceOnCharacterBodyCamera(this.character.GraphicalAsset);
                this.context["target"] = "body";
                this.camera.GetAction("focus-character").Start(this.context);
            }
            else
            {
                OrientGraphicsRealizer.Instance.Camera.PlaceOnCharacterFaceCamera(this.character.GraphicalAsset);
                this.context["target"] = "face";
                this.camera.GetAction("focus-character").Start(this.context);
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            this.curWaypointIndex++;
            if (this.curWaypointIndex == this.currentSet.GraphicalAsset.ResourceAsset.ResourceWaypoints.Count)
                this.curWaypointIndex = 0;

            //GenericWaypoint wp = this.currentSet.GraphicalAsset.Waypoints[this.curWaypointIndex];
            //OrientGraphicsRealizer.Instance.Camera.AbsolutePosition = wp.AbsolutePosition.Add(new OgrePosition(0, 3, 0));

            this.context["character"] = this.character.Name;
            this.context["target"] = "face";
            this.camera.GetAction("focus-character").Start(this.context);

            ResourceWaypoint curSpot =
                this.currentSet.GraphicalAsset.ResourceAsset.ResourceWaypoints[this.curWaypointIndex];
            this.context["set"] = this.set1.Name;
            this.context["spot"] = curSpot.IdToken;
            this.character.GetAction("enter-stage").Start(this.context);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            OrientGraphicsRealizer.Instance.Fullscreen = !OrientGraphicsRealizer.Instance.Fullscreen;
            WindowsMediaPlayer player = new WindowsMediaPlayer();
            player.URL =
                "C:\\Users\\Pedro\\Desktop\\Work\\Code\\PAGE\\orient\\rundir\\data\\audio-data\\backgrounds\\MOUNTAIN_BIRDS_DP_CROW.mp3";
        }

        private void button16_Click(object sender, EventArgs e)
        {
            OrientGraphicsRealizer.Instance.GraphicStatsVisible = showStats = !showStats;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            this.context["target"] = this.cheese.Name;
            this.character.GetAction("pick-item").Start(this.context);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            this.context["item"] = this.cheese.Name;
            this.context["target"] = this.table.Name;
            this.context["spot"] = "top-corner1";
            this.character.GetAction("drop-item").Start(this.context);
        }

        private void button19_Click(object sender, EventArgs e)
        {
            this.context["target"] = "arm-chair";
            this.character.GetAction("sit-on-chair").Start(this.context);
        }

        private void button20_Click(object sender, EventArgs e)
        {
            this.character.GetAction(Character.BEHAVIOUR_EAT_STR).Start(this.context);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            this.character.GetAction(Character.BEHAVIOUR_GET_UP_FROM_CHAIR_STR).Start(this.context);
        }

        private void button22_Click(object sender, EventArgs e)
        {
            this.context["target"] = this.character2.Name;
            this.character.GetAction(Character.BEHAVIOUR_TOAST_STR).Start(this.context);
        }

        private void button23_Click(object sender, EventArgs e)
        {
            Context context = new Context();
            context[TurnTo.PARAM_TARGET_STR] = this.character2.Name;
            this.character.GetAction(Character.BEHAVIOUR_GIVE_OBJECT_STR).Start(context);
        }

        private void button24_Click(object sender, EventArgs e)
        {
            if (this.character2 == null) return;
            this.context[SayTo.PARAM_UTTERANCE_STR] = this.textBox4.Text;
            this.context[TurnTo.PARAM_TARGET_STR] = this.character.Name;
            this.character2.GetAction(Character.BEHAVIOUR_SAY_TO_STR).Start(this.context);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if(this.character != null) this.character.GraphicalAsset.Unplace();
        }
    }
}