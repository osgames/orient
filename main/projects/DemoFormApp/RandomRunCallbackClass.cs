using System;
using PAGE.Generic;
using PAGE.Generic.Resources.Actions;
using PAGE.OGRE.Domain.Assets;

namespace DemoFormApp
{
    class RandomRunCallbackClass : ActionCallback
    {
        protected OgreSet set = null;
        protected OgreCharacter character = null;
        protected Random random = new Random();
        protected int prevIndex = -1;
        protected int successCount = 0;

        public RandomRunCallbackClass(OgreSet set, OgreCharacter character)
        {
            this.set = set;
            this.character = character;
            this.ignoreMultipleResponses = false;
        }

        protected override void Succeeded()
        {
            if (this.successCount == 2)
            {
                this.character.Animate(null, "idle-standing");
                return;
            }

            int index = this.random.Next(0, this.set.Waypoints.Count);
            if ((index == this.prevIndex) && (this.set.Waypoints.Count > 1))
            {
                this.Succeeded();
                return;
            }
            this.character.Move(this, "walk", this.set,
                this.set.Waypoints[index].ResourceSpot.IdToken);
            this.prevIndex = index;
            successCount++;
        }

        protected override void Failed()
        {
            this.Succeeded();
        }

        protected override void Triggered(ActionTrigger trigger)
        {
            //nothing
        }
    }
}
