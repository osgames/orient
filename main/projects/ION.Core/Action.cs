//
//	ION Framework - Core Classes
//	Copyright (C) 2007  GAIPS / INESC-ID Lisboa
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//
//	Author:  Marco Vala <marco.vala@inesc-id.pt>
//	Contributions:  Rui Prada, Carlos Martinho, Guilherme Raimundo, Jo�o Dias, Rui Figueiredo
//
//	Revision History:
//
//
using System;
using System.Collections.Generic;

namespace ION.Core
{
    public abstract class Action : Element
    {
        private Context context;

        protected Action()
        {
            this.context = null;
        }


        #region Properties

        private ElementList<Property> propertyList = new ElementList<Property>();
        private ElementNamespace<Property> propertyNamespace = new ElementNamespace<Property>();

        internal override ElementList<Property> PropertyList
        {
            // defines the list of the properties
            get { return this.propertyList; }
        }

        internal override ElementNamespace<Property> PropertyNamespace
        {
            // defines the namespace of the properties
            get { return this.propertyNamespace; }
        }

        public TProperty CreateProperty<TProperty>()
            where TProperty : Property, new()
        {
            return this.CreateProperty<TProperty>(null, null);
        }

        public TProperty CreateProperty<TProperty>(string name)
            where TProperty : Property, new()
        {
            return this.CreateProperty<TProperty>(name, null);
        }

        public TProperty CreateProperty<TProperty>(Arguments arguments)
            where TProperty : Property, new()
        {
            return this.CreateProperty<TProperty>(null, arguments);
        }

        public TProperty CreateProperty<TProperty>(string name, Arguments arguments)
            where TProperty : Property, new()
        {
            TProperty property = new TProperty();
            AddRequest(new Requests.CreateProperty(property, this, name, new Arguments(arguments)));
            return property;
        }

        public void DestroyProperty(string name)
        {
            Property property = this.propertyNamespace.Get(name);
            AddRequest(new Requests.DestroyElement(property));
        }

        public bool HasProperty(string name)
        {
            return this.propertyNamespace.Has(name);
        }

        public bool HasProperty<TProperty>(string name)
            where TProperty : Property
        {
            return this.propertyNamespace.Has<TProperty>(name);
        }

        public Property GetProperty(string name)
        {
            return this.propertyNamespace.Get(name);
        }

        public TProperty GetProperty<TProperty>(string name)
            where TProperty : Property
        {
            return this.propertyNamespace.Get<TProperty>(name);
        }

        public bool TryGetProperty(string name, out Property property)
        {
            return this.propertyNamespace.TryGet(name, out property);
        }

        public bool TryGetProperty<TProperty>(string name, out TProperty property)
            where TProperty : Property
        {
            return this.propertyNamespace.TryGet<TProperty>(name, out property);
        }

        public IFilterable<Property> Properties
        {
            get { return this.propertyList; }
        }

        #endregion


        #region Create

        internal override void ExecuteCreate(Element parent, string name, Arguments arguments)
        {
            base.ExecuteCreate(parent, name, arguments);

            // add parent link
            parent.ActionList.Add(this);
            parent.ActionNamespace.Add(name, this);

            // raise event: ActionCreated<>
            Type eventType = typeof(Events.ActionCreated<>).MakeGenericType(this.GetType());
            Type[] eventParameters = { this.GetType(), typeof(Element), typeof(string), typeof(Arguments) };
            object[] eventArguments = { this, parent, name, arguments };
            this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));

            // execute pending requests
            this.ExecuteRequests();
        }

        #endregion

        #region Destroy

        internal override void ExecuteDestroy()
        {
            if (this.Parent != null)
            {
                // raise event: ActionDestroyed<>
                Type eventType = typeof(Events.ActionDestroyed<>).MakeGenericType(this.GetType());
                Type[] eventParameters = { this.GetType(), typeof(Element), typeof(string) };
                object[] eventArguments = { this, this.Parent, this.Name };
                this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));

                // end action if it is running
                if (this.IsRunning)
                {
                    this.ExecuteEnd();
                }

                // destroy properties
                LinkedList<Property> properties = new LinkedList<Property>(this.PropertyList);
                foreach (Property property in properties)
                {
                    property.ExecuteDestroy();
                }

                // remove parent link
                this.Parent.ActionList.Remove(this);
                this.Parent.ActionNamespace.Remove(this.Name);

                base.ExecuteDestroy();
            }
        }

        #endregion

        #region Suspend

        internal override void ExecuteSuspend()
        {
            if (!this.IsSuspended)
            {
                // raise event: ActionSuspended<>
                Type eventType = typeof(Events.ActionSuspended<>).MakeGenericType(this.GetType());
                Type[] eventParameters = { this.GetType() };
                object[] eventArguments = { this };
                this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));

                // suspend properties
                foreach (Property property in this.PropertyList)
                {
                    property.ExecuteSuspend();
                }

                base.ExecuteSuspend();
            }
        }

        #endregion

        #region Resume

        internal override void ExecuteResume()
        {
            if (this.IsSuspended)
            {
                // raise event: ActionResumed<>
                Type eventType = typeof(Events.ActionResumed<>).MakeGenericType(this.GetType());
                Type[] eventParameters = { this.GetType() };
                object[] eventArguments = { this };
                this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));

                // resume properties
                foreach (Property property in this.PropertyList)
                {
                    property.ExecuteResume();
                }

                base.ExecuteResume();
            }
        }

        #endregion


        #region Start

        public void Start()
        {
            this.Start(new Context());
        }

        public void Start(Arguments arguments)
        {
            this.AddRequest(new Requests.StartAction(this, new Arguments(arguments)));
        }

        public void Start(Context context)
        {
            this.AddRequest(new Requests.StartAction(this, new Context(context)));
        }

        internal void ExecuteStart(Context context)
        {
            if (this.IsRunning)
            {
                //this.ExecuteEnd();
                // return? (end = last runs; return = first runs)
                return;
            }

            Universe.Instance.ActionsRunning.Add(this);
            this.context = context;
            this.OnStart(context);

            // raise ActionStarted<> event
            Type eventType = typeof(Events.ActionStarted<>).MakeGenericType(this.GetType());
            Type[] eventParameters = { this.GetType(), typeof(Context) };
            object[] eventArguments = { this, context };
            this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));
        }

        protected abstract void OnStart(Context context);

        #endregion

        #region End

        public void End()
        {
            this.AddRequest(new Requests.EndAction(this));
        }

        internal void ExecuteEnd()
        {
            // request ignored: action is idle
            if (this.IsIdle)
            {
                return;
            }

            // raise ActionEnded<> event
            Type eventType = typeof(Events.ActionEnded<>).MakeGenericType(this.GetType());
            Type[] eventParameters = { this.GetType(), typeof(Context) };
            object[] eventArguments = { this, this.context };
            this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));

            this.OnEnd(this.context);
            this.context = null;
            Universe.Instance.ActionsRunning.Remove(this);
        }

        protected abstract void OnEnd(Context context);

        #endregion

        #region Fail

        public void Fail()
        {
            this.AddRequest(new Requests.FailAction(this));
        }

        internal void ExecuteFail()
        {
            // request ignored: action is idle
            if (this.IsIdle)
            {
                return;
            }

            // raise ActionFailed<> event
            Type eventType = typeof(Events.ActionFailed<>).MakeGenericType(this.GetType());
            Type[] eventParameters = { this.GetType(), typeof(Context) };
            object[] eventArguments = { this, this.context };
            this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));

            this.OnFail(this.context);
            this.context = null;
            Universe.Instance.ActionsRunning.Remove(this);
        }

        protected abstract void OnFail(Context context);

        #endregion

        #region Step

        internal void Step()
        {
            this.OnStep(this.context);
        }

        protected abstract void OnStep(Context context);

        #endregion


        public bool IsIdle
        {
            get { return !Universe.Instance.ActionsRunning.Has(this); }
        }

        public bool IsRunning
        {
            get { return Universe.Instance.ActionsRunning.Has(this); }
        }
    }
}
