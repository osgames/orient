//
//	ION Framework - Core Classes
//	Copyright (C) 2007  GAIPS / INESC-ID Lisboa
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//
//	Author:  Marco Vala <marco.vala@inesc-id.pt>
//	Contributions:  Rui Prada, Carlos Martinho, Guilherme Raimundo, Jo�o Dias, Rui Figueiredo
//
//	Revision History:
//
//
using System;
using System.Collections.Generic;

namespace ION.Core
{
    public class Arguments : IEnumerable<KeyValuePair<string, object>>
    {
        private Dictionary<string, object> arguments;

        public Arguments(IEnumerable<KeyValuePair<string, object>> arguments)
        {
            this.arguments = new Dictionary<string, object>();
            if (arguments != null)
            {
                foreach (KeyValuePair<string, object> pair in arguments)
                {
                    this.arguments.Add(pair.Key, pair.Value);
                }
            }
        }

        public Arguments(Arguments arguments)
        {
            if (arguments == null)
            {
                this.arguments = new Dictionary<string, object>();
            }
            else
            {
                this.arguments = new Dictionary<string, object>(arguments.arguments);
            }
        }

        public int Count
        {
            get
            {
                return this.arguments.Count;
            }
        }

        public object this[string key]
        {
            get
            {
                return this.arguments[key];
            }
            set
            {
                this.arguments[key] = value;
            }
        }

        public T Get<T>(string name)
        {
            return (T)this.arguments[name];
        }

        public bool TryGetValue(string key, out object value)
        {
            return this.arguments.TryGetValue(key, out value);
        }

        public Dictionary<string, object>.KeyCollection Keys
        {
            get
            {
                return this.arguments.Keys;
            }
        }

        public Dictionary<string, object>.ValueCollection Values
        {
            get
            {
                return this.arguments.Values;
            }
        }

        public bool ContainsKey(string key)
        {
            return this.arguments.ContainsKey(key);
        }

        public bool ContainsValue(object value)
        {
            return this.arguments.ContainsValue(value);
        }

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return this.arguments.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.arguments.GetEnumerator();
        }
    }
}
