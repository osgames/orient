//
//	ION Framework - Core Classes
//	Copyright (C) 2007  GAIPS / INESC-ID Lisboa
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//
//	Author:  Marco Vala <marco.vala@inesc-id.pt>
//	Contributions:  Rui Prada, Carlos Martinho, Guilherme Raimundo, Jo�o Dias, Rui Figueiredo
//
//	Revision History:
//
//
using System;
using System.Collections.Generic;

namespace ION.Core
{
    internal class EventRouter
    {
        private Dictionary<Type, LinkedList<EventListener>> audience;

        public EventRouter()
        {
            this.audience = null;
        }

        public void AddEventListener<TEvent>(EventListener<TEvent> listener) where TEvent : Event
        {
            if (this.audience == null)
            {
                this.audience = new Dictionary<Type, LinkedList<EventListener>>();
            }

            LinkedList<EventListener> listeners;
            if (!this.audience.TryGetValue(typeof(TEvent), out listeners))
            {
                listeners = new LinkedList<EventListener>();
                this.audience.Add(typeof(TEvent), listeners);
            }

            listeners.AddLast(listener);
        }

        public void RemoveEventListener<TEvent>(EventListener<TEvent> listener) where TEvent : Event
        {
            if (this.audience != null)
            {
                LinkedList<EventListener> listeners;
                if (this.audience.TryGetValue(typeof(TEvent), out listeners))
                {
                    listeners.Remove(listener);
                }
            }
        }

        public void DistributeEvent(Event e)
        {
            if (this.audience != null)
            {
                Type type = e.GetType();
                LinkedList<EventListener> listeners;
                while (type != null)
                {
                    if (this.audience.TryGetValue(type, out listeners))
                    {
                        foreach (EventListener listener in listeners)
                        {
                            listener.Notify(e);
                        }
                    }
                    type = type.BaseType;
                }
            }
        }
    }
}
