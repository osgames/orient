//
//	ION Framework - Core Classes
//	Copyright (C) 2007  GAIPS / INESC-ID Lisboa
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//
//	Author:  Marco Vala <marco.vala@inesc-id.pt>
//	Contributions:  Rui Prada, Carlos Martinho, Guilherme Raimundo, Jo�o Dias, Rui Figueiredo
//
//	Revision History:
//
//
using System;
using System.Collections.Generic;

namespace ION.Core
{
    public abstract class Element
    {
        public enum Status { ACTIVE, SUSPENDED, INVALID }

        private ulong uid;
        private Status status;
        private Element parent;
        private string name;
        private RequestList requests;

        protected Element()
        {
            this.uid = Universe.Instance.UIDSeed++;
            this.status = Status.ACTIVE;
            this.parent = null;
            this.name = null;
            this.requests = new RequestList();
        }

        protected Element(ulong uid)
        {
            this.uid = uid;
            this.status = Status.ACTIVE;
            this.parent = null;
            this.name = null;
            this.requests = new RequestList();
        }

        public ulong UID
        {
            get { return this.uid; }
        }

        public Element Parent
        {
            get { return this.parent; }
        }

        public string Name
        {
            get { return this.name; }
        }

        public bool IsSuspended
        {
            get { return this.status == Status.SUSPENDED; }
        }

        public bool IsInvalid
        {
            get { return this.status == Status.INVALID; }
        }



        #region Create

        internal virtual void ExecuteCreate(Element parent, string name, Arguments arguments)
        {
            this.parent = parent;
            this.name = name;
            this.OnCreate(arguments);
            Universe.Instance.AddElement(this);
        }

        protected abstract void OnCreate(Arguments arguments);

        #endregion

        #region Destroy

        public void Destroy()
        {
            this.AddRequest(new Requests.DestroyElement(this));
        }

        internal virtual void ExecuteDestroy()
        {
            this.OnDestroy();
            this.status = Status.INVALID;
            this.parent = null;
            //this.name = null;
            Universe.Instance.RemoveElement(this);
        }

        protected abstract void OnDestroy();

        #endregion

        #region Suspend

        public void Suspend()
        {
            this.AddRequest(new Requests.SuspendElement(this));
        }

        internal virtual void ExecuteSuspend()
        {
            this.status = Status.SUSPENDED;
            this.OnSuspend();
        }

        protected internal virtual void OnSuspend() 
        {
        }

        #endregion

        #region Resume

        public void Resume()
        {
            this.AddRequest(new Requests.ResumeElement(this));
        }

        internal virtual void ExecuteResume()
        {
            this.status = Status.ACTIVE;
            this.OnResume();
        }

        protected internal virtual void OnResume()
        {
        }

        #endregion



        // REQUESTS
        internal void AddRequest(Request request)
        {
            if (this.IsInvalid)
            {
                Console.WriteLine("denied: element is invalid");
                return;
            }

            if (this.IsSuspended && !(request is Requests.ResumeElement))
            {
                Console.WriteLine("denied: element is suspended");
                return;
            }

            if (this.parent == null)
            {
                this.requests.Add(request);
            }
            else
            {
                Universe.Instance.AddRequest(request);
            }
        }

        internal void ExecuteRequests()
        {
            this.requests.Execute();
        }



        // EVENTS
        public EventListener CreateEventListener<TEvent>(IEventCallback<TEvent> callback) where TEvent : Event
        {
            return this.CreateEventListener<TEvent>(callback, null);
        }

        public EventListener CreateEventListener<TEvent>(IEventCallback<TEvent> callback, IFilter<TEvent> filter) where TEvent : Event
        {
            EventListener<TEvent> listener = new EventListener<TEvent>(this, filter, callback);
            this.AddRequest(new Requests.CreateEventListener(listener));
            return listener;
        }

        public void DestroyEventListener(EventListener listener)
        {
            listener.Destroy();
        }

        internal virtual EventRouter EventRouter
        {
            get { return this.parent.EventRouter; }
        }

        public virtual void RaiseEvent(Event evt)
        {
            evt.Source = this;
            evt.Trail.Add(this);
            if (this.parent != null)
            {
                this.parent.RaiseEvent(evt);
            }
        }


        // PARENT
        internal virtual ElementList<Entity> EntityList
        {
            get { return null; }
        }

        internal virtual ElementNamespace<Entity> EntityNamespace
        {
            get { return null; }
        }

        internal virtual ElementList<Action> ActionList
        {
            get { return null; }
        }

        internal virtual ElementNamespace<Action> ActionNamespace
        {
            get { return null; }
        }

        internal virtual ElementList<Property> PropertyList
        {
            get { return null; }
        }

        internal virtual ElementNamespace<Property> PropertyNamespace
        {
            get { return null; }
        }
    }
}
