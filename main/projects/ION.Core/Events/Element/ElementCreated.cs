//
//	ION Framework - Core Classes
//	Copyright (C) 2007  GAIPS / INESC-ID Lisboa
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//
//	Author:  Marco Vala <marco.vala@inesc-id.pt>
//	Contributions:  Rui Prada, Carlos Martinho, Guilherme Raimundo, Jo�o Dias, Rui Figueiredo
//
//	Revision History:
//
//
using System;
using System.Collections.Generic;

namespace ION.Core.Events
{
    public abstract class ElementCreated : Event
    {
        private Element element;
        private Element parent;
        private string name;
        private Arguments arguments;

        protected ElementCreated(Element element, Element parent, string name, Arguments arguments)
        {
            this.element = element;
            this.parent = parent;
            this.name = name;
            this.arguments = arguments;
        }

        public Element Element
        {
            get { return this.element; }
        }

        public Element Parent
        {
            get { return this.parent; }
        }

        public string Name
        {
            get { return this.name; }
        }

        public Arguments Arguments
        {
            get { return this.arguments; }
        }
    }
}
