//
//	ION Framework - Core Classes
//	Copyright (C) 2007  GAIPS / INESC-ID Lisboa
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//
//	Author:  Marco Vala <marco.vala@inesc-id.pt>
//	Contributions:  Rui Prada, Carlos Martinho, Guilherme Raimundo, Jo�o Dias, Rui Figueiredo
//
//	Revision History:
//
//
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ION.Core
{
    public sealed class Universe : Element
    {
        // UIDS
        internal ulong UIDSeed = 1000;

        // NAMES
        internal Regex ValidName = new Regex("[a-zA-Z_]\\w*");



        // SINGLETON
        private static Universe instance = null;

        public static Universe Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Universe();
                    instance.AddElement(instance);
                }
                return instance;
            }
        }



        private Universe()
            : base(0)
        {
        }




        // TICK
        private ulong tick = 0;

        public ulong Tick
        {
            get { return this.tick; }
            internal set { this.tick = value; }
        }

        // ACTIONS
        internal ElementList<Action> ActionsRunning = new ElementList<Action>();

        // EVENT LISTENERS
        internal List<EventListener> ActiveListeners = new List<EventListener>();

        public void Update()
        {
            this.ExecuteRequests();

            foreach (Action action in this.ActionsRunning)
            {
                action.Step();
            }

            int count = this.ActiveListeners.Count;
            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    this.ActiveListeners[i].Invoke();
                }
                this.ActiveListeners.RemoveRange(0, count);
            }

            this.Tick++;
        }



        #region Elements UID

        private ElementList<Element> elementsList = new ElementList<Element>();

        internal void AddElement(Element element)
        {
            this.elementsList.Add(element);
        }

        internal void RemoveElement(Element element)
        {
            this.elementsList.Remove(element);
        }

        public bool Exists<TElement>(ulong uid) where TElement : Element
        {
            Element element;
            return (this.elementsList.TryGet(uid, out element) && element is TElement);
        }

        public TElement Get<TElement>(ulong uid) where TElement : Element
        {
            return elementsList.Get<TElement>(uid);
        }

        public bool TryGet<TElement>(ulong uid, out TElement element) where TElement : Element
        {
            Element baseElement;
            if (!this.elementsList.TryGet(uid, out baseElement) || !(baseElement is TElement))
            {
                element = null;
                return false;
            }
            element = baseElement as TElement;
            return true;
        }

        public IFilterable<Element> Elements
        {
            get
            {
                return this.elementsList;
            }
        }

        #endregion

        #region Entities

        private ElementList<Entity> entityList = new ElementList<Entity>();
        private ElementNamespace<Entity> entityNamespace = new ElementNamespace<Entity>();

        internal override ElementList<Entity> EntityList
        {
            // defines the list of the entities
            get { return this.entityList; }
        }

        internal override ElementNamespace<Entity> EntityNamespace
        {
            // defines the namespace of the entities
            get { return this.entityNamespace; }
        }

        public Entity CreateEntity()
        {
            return this.CreateEntity<Entity>(null, null);
        }

        public Entity CreateEntity(string name)
        {
            return this.CreateEntity<Entity>(name, null);
        }

        public Entity CreateEntity(Arguments arguments)
        {
            return this.CreateEntity<Entity>(null, arguments);
        }

        public Entity CreateEntity(string name, Arguments arguments)
        {
            return this.CreateEntity<Entity>(name, arguments);
        }

        public TEntity CreateEntity<TEntity>()
            where TEntity : Entity, new()
        {
            return this.CreateEntity<TEntity>(null, null);
        }

        public TEntity CreateEntity<TEntity>(string name)
            where TEntity : Entity, new()
        {
            return this.CreateEntity<TEntity>(name, null);
        }

        public TEntity CreateEntity<TEntity>(Arguments arguments)
            where TEntity : Entity, new()
        {
            return this.CreateEntity<TEntity>(null, arguments);
        }

        public TEntity CreateEntity<TEntity>(string name, Arguments arguments)
            where TEntity : Entity, new()
        {
            TEntity entity = new TEntity();
            AddRequest(new Requests.CreateEntity(entity, this, name, new Arguments(arguments)));
            return entity;
        }

        public void DestroyEntity(string name)
        {
            Entity entity = this.entityNamespace.Get(name);
            AddRequest(new Requests.DestroyElement(entity));
        }

        public bool HasEntity(string name)
        {
            return this.entityNamespace.Has(name);
        }

        public bool HasEntity<TEntity>(string name)
            where TEntity : Entity
        {
            return this.entityNamespace.Has<TEntity>(name);
        }

        public Entity GetEntity(string name)
        {
            return this.entityNamespace.Get(name);
        }

        public TEntity GetEntity<TEntity>(string name)
            where TEntity : Entity
        {
            return this.entityNamespace.Get<TEntity>(name);
        }

        public bool TryGetEntity(string name, out Entity entity)
        {
            return this.entityNamespace.TryGet(name, out entity);
        }

        public bool TryGetEntity<TEntity>(string name, out TEntity entity)
            where TEntity : Entity
        {
            return this.entityNamespace.TryGet<TEntity>(name, out entity);
        }

        public IFilterable<Entity> Entities
        {
            get { return this.entityList; }
        }

        #endregion

        #region Events

        private EventRouter eventRouter = new EventRouter();

        internal override EventRouter EventRouter
        {
            get { return this.eventRouter; }
        }

        public override void RaiseEvent(Event evt)
        {
            base.RaiseEvent(evt);
            this.EventRouter.DistributeEvent(evt);
        }

        #endregion



        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
