//
//	ION Framework - Core Classes
//	Copyright (C) 2007  GAIPS / INESC-ID Lisboa
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//
//	Author:  Marco Vala <marco.vala@inesc-id.pt>
//	Contributions:  Rui Prada, Carlos Martinho, Guilherme Raimundo, Jo�o Dias, Rui Figueiredo
//
//	Revision History:
//
//
using System;
using System.Collections.Generic;

namespace ION.Core
{
    internal class RequestList
    {
        private List<Request> requests;
        private List<Request> destroyRequests;

        public RequestList()
        {
            this.requests = null;
            this.destroyRequests = null;
        }

        // O(1)
        public void Add(Request request)
        {
            if (request is Requests.DestroyElement)
            {
                // lazy creation
                if (this.destroyRequests == null)
                {
                    this.destroyRequests = new List<Request>();
                }
                this.destroyRequests.Add(request);
            }
            else
            {
                // lazy creation
                if (this.requests == null)
                {
                    this.requests = new List<Request>();
                }
                this.requests.Add(request);
            }
        }

        public void Execute()
        {
            if (this.requests != null)
            {
                int count = this.requests.Count;
                for (int i=0; i < count; i++)
                {
                    this.requests[i].Execute();
                }
                this.requests.RemoveRange(0, count);
            }
            if (this.destroyRequests != null)
            {
                int count = this.destroyRequests.Count;
                for (int i = 0; i < count; i++)
                {
                    this.destroyRequests[i].Execute();
                }
                this.destroyRequests.RemoveRange(0, count);
            }
        }
    }
}
