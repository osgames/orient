//
//	ION Framework - Core Classes
//	Copyright (C) 2007  GAIPS / INESC-ID Lisboa
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//
//	Author:  Marco Vala <marco.vala@inesc-id.pt>
//	Contributions:  Rui Prada, Carlos Martinho, Guilherme Raimundo, Jo�o Dias, Rui Figueiredo
//
//	Revision History:
//
//
using System;
using System.Collections.Generic;

namespace ION.Core
{
    public abstract class Property : Element
    {
        private bool isChanging;

        protected Property()
        {
            this.isChanging = false;
        }


        #region Create

        internal override void ExecuteCreate(Element parent, string name, Arguments arguments)
        {
            base.ExecuteCreate(parent, name, arguments);

            // add parent link
            parent.PropertyList.Add(this);
            parent.PropertyNamespace.Add(name, this);

            // raise event: PropertyCreated<>
            Type eventType = typeof(Events.PropertyCreated<>).MakeGenericType(this.GetType());
            Type[] eventParameters = { this.GetType(), typeof(Element), typeof(string), typeof(Arguments) };
            object[] eventArguments = { this, parent, name, arguments };
            this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));

            // execute pending requests
            this.ExecuteRequests();
        }

        #endregion

        #region Destroy

        internal override void ExecuteDestroy()
        {
            if (this.Parent != null)
            {
                // raise event: PropertyDestroyed<>
                Type eventType = typeof(Events.PropertyDestroyed<>).MakeGenericType(this.GetType());
                Type[] eventParameters = { this.GetType(), typeof(Element), typeof(string) };
                object[] eventArguments = { this, this.Parent, this.Name };
                this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));

                // remove parent link
                this.Parent.PropertyList.Remove(this);
                this.Parent.PropertyNamespace.Remove(this.Name);

                base.ExecuteDestroy();
            }
        }

        #endregion

        #region Suspend

        internal override void ExecuteSuspend()
        {
            if (!this.IsSuspended)
            {
                // raise event: PropertySuspended<>
                Type eventType = typeof(Events.PropertySuspended<>).MakeGenericType(this.GetType());
                Type[] eventParameters = { this.GetType() };
                object[] eventArguments = { this };
                this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));
         
                base.ExecuteSuspend();
            }
        }

        #endregion

        #region Resume

        internal override void ExecuteResume()
        {
            if (this.IsSuspended)
            {
                // raise event: PropertyResumed<>
                Type eventType = typeof(Events.PropertyResumed<>).MakeGenericType(this.GetType());
                Type[] eventParameters = { this.GetType() };
                object[] eventArguments = { this };
                this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));

                base.ExecuteResume();
            }
        }

        #endregion


        #region Change

        protected void Change()
        {
            if (!this.isChanging)
            {
                this.AddRequest(new Requests.ChangeProperty(this));
                this.isChanging = true;
            }
        }

        internal void ExecuteChange()
        {
            this.OnChange();
            this.isChanging = false;

            // raise event: PropertyChanged<>
            Type eventType = typeof(Events.PropertyChanged<>).MakeGenericType(this.GetType());
            Type[] eventParameters = { this.GetType() };
            object[] eventArguments = { this };
            this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));
        }

        protected internal abstract void OnChange();

        #endregion
    }
}
