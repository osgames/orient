//
//	ION Framework - Core Classes
//	Copyright (C) 2007  GAIPS / INESC-ID Lisboa
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//
//	Author:  Marco Vala <marco.vala@inesc-id.pt>
//	Contributions:  Rui Prada, Carlos Martinho, Guilherme Raimundo, Jo�o Dias, Rui Figueiredo
//
//	Revision History:
//
//
using System;
using System.Collections.Generic;

namespace ION.Core
{
    internal class EventListener<TEvent> : EventListener
        where TEvent : Event
    {
        private Element element;
        private IFilter<TEvent> filter;
        private IEventCallback<TEvent> callback;
        private LinkedList<TEvent> eventsCollected;

        public EventListener(Element element, IFilter<TEvent> filter, IEventCallback<TEvent> callback)
        {
            this.element = element;
            this.filter = filter;
            this.callback = callback;
            this.eventsCollected = new LinkedList<TEvent>();
        }

        internal override void Notify(Event evt)
        {
            if (evt.Trail.Has(this.element))
            {
                if (this.filter == null || !this.filter.Blocks((TEvent)evt))
                {
                    if (this.eventsCollected.Count == 0)
                    {
                        Universe.Instance.ActiveListeners.Add(this);
                    }
                    this.eventsCollected.AddLast(evt as TEvent);
                }
            }
        }

        internal override void Invoke()
        {
            foreach (TEvent evt in this.eventsCollected)
            {
                this.callback.Invoke(evt);
            }
            this.eventsCollected.Clear();
        }

        internal override void ExecuteCreate()
        {
            this.element.EventRouter.AddEventListener<TEvent>(this);
        }

        public override void Destroy()
        {
            this.element.AddRequest(new Requests.DestroyEventListener(this));
        }

        internal override void ExecuteDestroy()
        {
            this.element.EventRouter.RemoveEventListener<TEvent>(this);
        }
    }
}
