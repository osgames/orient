//
//	ION Framework - Core Classes
//	Copyright (C) 2007  GAIPS / INESC-ID Lisboa
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//
//	Author:  Marco Vala <marco.vala@inesc-id.pt>
//	Contributions:  Rui Prada, Carlos Martinho, Guilherme Raimundo, Jo�o Dias, Rui Figueiredo
//
//	Revision History:
//
//
using System;
using System.Collections.Generic;

namespace ION.Core
{
    public class Entity : Element
    {
        public Entity()
        {
        }


        #region Actions

        private ElementList<Action> actionList = new ElementList<Action>();
        private ElementNamespace<Action> actionNamespace = new ElementNamespace<Action>();

        internal override ElementList<Action> ActionList
        {
            // defines the list of the actions
            get { return this.actionList; }
        }

        internal override ElementNamespace<Action> ActionNamespace
        {
            // defines the namespace of the actions
            get { return this.actionNamespace; }
        }

        public TAction CreateAction<TAction>()
            where TAction : Action, new()
        {
            return this.CreateAction<TAction>(null, null);
        }

        public TAction CreateAction<TAction>(string name)
            where TAction : Action, new()
        {
            return this.CreateAction<TAction>(name, null);
        }

        public TAction CreateAction<TAction>(Arguments arguments)
            where TAction : Action, new()
        {
            return this.CreateAction<TAction>(null, arguments);
        }

        public TAction CreateAction<TAction>(string name, Arguments arguments)
            where TAction : Action, new()
        {
            TAction action = new TAction();
            AddRequest(new Requests.CreateAction(action, this, name, new Arguments(arguments)));
            return action;
        }

        public void DestroyAction(string name)
        {
            Action action = this.actionNamespace.Get(name);
            AddRequest(new Requests.DestroyElement(action));
        }

        public bool HasAction(string name)
        {
            return this.actionNamespace.Has(name);
        }

        public bool HasAction<TAction>(string name)
            where TAction : Action
        {
            return this.actionNamespace.Has<TAction>(name);
        }

        public Action GetAction(string name)
        {
            return this.actionNamespace.Get(name);
        }

        public TAction GetAction<TAction>(string name)
            where TAction : Action
        {
            return this.actionNamespace.Get<TAction>(name);
        }

        public bool TryGetAction(string name, out Action action)
        {
            return this.actionNamespace.TryGet(name, out action);
        }

        public bool TryGetAction<TAction>(string name, out TAction action)
            where TAction : Action
        {
            return this.actionNamespace.TryGet<TAction>(name, out action);
        }

        public IFilterable<Action> Actions
        {
            get { return this.actionList; }
        }

        #endregion

        #region Properties

        private ElementList<Property> propertyList = new ElementList<Property>();
        private ElementNamespace<Property> propertyNamespace = new ElementNamespace<Property>();

        internal override ElementList<Property> PropertyList
        {
            // defines the list of the properties
            get { return this.propertyList; }
        }

        internal override ElementNamespace<Property> PropertyNamespace
        {
            // defines the namespace of the properties
            get { return this.propertyNamespace; }
        }

        public TProperty CreateProperty<TProperty>()
            where TProperty : Property, new()
        {
            return this.CreateProperty<TProperty>(null, null);
        }

        public TProperty CreateProperty<TProperty>(string name)
            where TProperty : Property, new()
        {
            return this.CreateProperty<TProperty>(name, null);
        }

        public TProperty CreateProperty<TProperty>(Arguments arguments)
            where TProperty : Property, new()
        {
            return this.CreateProperty<TProperty>(null, arguments);
        }

        public TProperty CreateProperty<TProperty>(string name, Arguments arguments)
            where TProperty : Property, new()
        {
            TProperty property = new TProperty();
            AddRequest(new Requests.CreateProperty(property, this, name, new Arguments(arguments)));
            return property;
        }

        public void DestroyProperty(string name)
        {
            Property property = this.propertyNamespace.Get(name);
            AddRequest(new Requests.DestroyElement(property));
        }

        public bool HasProperty(string name)
        {
            return this.propertyNamespace.Has(name);
        }

        public bool HasProperty<TProperty>(string name)
            where TProperty : Property
        {
            return this.propertyNamespace.Has<TProperty>(name);
        }

        public Property GetProperty(string name)
        {
            return this.propertyNamespace.Get(name);
        }

        public TProperty GetProperty<TProperty>(string name)
            where TProperty : Property
        {
            return this.propertyNamespace.Get<TProperty>(name);
        }

        public bool TryGetProperty(string name, out Property property)
        {
            return this.propertyNamespace.TryGet(name, out property);
        }

        public bool TryGetProperty<TProperty>(string name, out TProperty property)
            where TProperty : Property
        {
            return this.propertyNamespace.TryGet<TProperty>(name, out property);
        }

        public IFilterable<Property> Properties
        {
            get { return this.propertyList; }
        }

        #endregion


        #region Create

        internal override void ExecuteCreate(Element parent, string name, Arguments arguments)
        {
            base.ExecuteCreate(parent, name, arguments);

            // add parent link
            parent.EntityList.Add(this);
            parent.EntityNamespace.Add(name, this);

            // raise event: EntityCreated<>
            Type eventType = typeof(Events.EntityCreated<>).MakeGenericType(this.GetType());
            Type[] eventParameters = { this.GetType(), typeof(Element), typeof(string), typeof(Arguments) };
            object[] eventArguments = { this, parent, name, arguments };
            this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));

            // execute pending requests
            this.ExecuteRequests();
        }

        #endregion

        #region Destroy

        internal override void ExecuteDestroy()
        {
            if (this.Parent != null)
            {
                // raise event: EntityDestroyed<>
                Type eventType = typeof(Events.EntityDestroyed<>).MakeGenericType(this.GetType());
                Type[] eventParameters = { this.GetType(), typeof(Element), typeof(string) };
                object[] eventArguments = { this, this.Parent, this.Name };
                this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));

                // remove parent link
                this.Parent.EntityList.Remove(this);
                this.Parent.EntityNamespace.Remove(this.Name);

                base.ExecuteDestroy();

                // destroy actions
                LinkedList<Action> actions = new LinkedList<Action>(this.ActionList);
                foreach (Action action in actions)
                {
                    action.ExecuteDestroy();
                }

                // destroy properties
                LinkedList<Property> properties = new LinkedList<Property>(this.PropertyList);
                foreach (Property property in properties)
                {
                    property.ExecuteDestroy();
                }
            }
        }

        #endregion

        #region Suspend

        internal override void ExecuteSuspend()
        {
            if (!this.IsSuspended)
            {
                // raise event: EntitySuspended<>
                Type eventType = typeof(Events.EntitySuspended<>).MakeGenericType(this.GetType());
                Type[] eventParameters = { this.GetType() };
                object[] eventArguments = { this };
                this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));

                // suspend actions
                foreach (Action action in this.ActionList)
                {
                    action.ExecuteSuspend();
                }

                // suspend properties
                foreach (Property property in this.PropertyList)
                {
                    property.ExecuteSuspend();
                }

                base.ExecuteSuspend();
            }
        }

        #endregion

        #region Resume

        internal override void ExecuteResume()
        {
            if (this.IsSuspended)
            {
                // raise event: EntityResumed<>
                Type eventType = typeof(Events.EntityResumed<>).MakeGenericType(this.GetType());
                Type[] eventParameters = { this.GetType() };
                object[] eventArguments = { this };
                this.RaiseEvent(Event.Generate(eventType, eventParameters, eventArguments));

                // resume actions
                foreach (Action action in this.ActionList)
                {
                    action.ExecuteResume();
                }

                // resume properties
                foreach (Property property in this.PropertyList)
                {
                    property.ExecuteResume();
                }

                base.ExecuteResume();
            }
        }

        #endregion


        protected override void OnCreate(Arguments arguments)
        {
        }

        protected override void OnDestroy()
        {
        }
    }
}
