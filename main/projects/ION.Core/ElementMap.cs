//
//	ION Framework - Core Classes
//	Copyright (C) 2007  GAIPS / INESC-ID Lisboa
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//
//	Author:  Marco Vala <marco.vala@inesc-id.pt>
//	Contributions:  Rui Prada, Carlos Martinho, Guilherme Raimundo, Jo�o Dias, Rui Figueiredo
//
//	Revision History:
//
//
using System;
using System.Collections.Generic;

namespace ION.Core
{
    internal class ElementMap<TKey, TElement> : IFilterable<TElement>
        where TElement : Element
    {
        private Dictionary<TKey, TElement> items;

        public ElementMap()
        {
            this.items = null;
        }

        // O(1)
        public void Add(TKey key, TElement element)
        {
            if (key != null && element != null)
            {
                // add the element to the map
                if (this.items == null)
                {
                    this.items = new Dictionary<TKey, TElement>();
                }
                this.items.Add(key, element);
            }
        }

        // O(1)
        public void Remove(TKey key)
        {
            if (key != null && this.items != null)
            {
                // remove the element from the items collection
                this.items.Remove(key);
                if (this.items.Count == 0)
                {
                    this.items = null;
                }
            }
        }

        // O(1)
        public bool Has(TKey key)
        {
            return (this.items != null && this.items.ContainsKey(key));
        }

        // O(1)
        public bool Has<TTElement>(TKey key) where TTElement : TElement
        {
            TElement element;
            return (this.items != null && this.items.TryGetValue(key, out element) && element is TTElement);
        }

        // O(1)
        // throws KeyNotFound
        public TElement Get(TKey key)
        {
            TElement element;
            if (this.items == null || !this.items.TryGetValue(key, out element))
            {
                throw new Exception("KeyNotFound: " + key);
            }
            return element;
        }

        // O(1)
        // throws KeyNotFound
        // throws WrongType
        //  OU
        // throws ElementNotFound
        public TTElement Get<TTElement>(TKey key) where TTElement : TElement
        {
            TElement element = this.Get(key);
            if (!(element is TTElement))
            {
                throw new Exception("WrongType: " + key);
            }
            return (element as TTElement);
        }

        // O(1)
        public bool TryGet(TKey key, out TElement element)
        {
            if (this.items != null)
            {
                return this.items.TryGetValue(key, out element);
            }
            element = null;
            return false;
        }

        // O(1)
        public bool TryGet<TTElement>(TKey key, out TTElement element) where TTElement : TElement
        {
            TElement baseElement;
            if (this.TryGet(key, out baseElement) && baseElement is TTElement)
            {
                element = baseElement as TTElement;
                return true;
            }
            element = null;
            return false;
        }

        #region IFilterable<TElement> Members

        // O(1)
        public int Count
        {
            get
            {
                if (this.items != null)
                {
                    return this.items.Count;
                }
                return 0;
            }
        }

        // O(n)
        public IFilterable<TElement> FilteredBy(IFilter<TElement> filter)
        {
            ElementList<TElement> filteredItems = new ElementList<TElement>();
            if (this.items != null)
            {
                foreach (TElement item in this.items.Values)
                {
                    if (!filter.Blocks(item))
                    {
                        filteredItems.Add(item);
                    }
                }
            }
            return filteredItems;
        }

        // O(1)
        public IEnumerator<TElement> GetEnumerator()
        {
            if (this.items != null)
            {
                return this.items.Values.GetEnumerator();
            }
            return (new LinkedList<TElement>()).GetEnumerator();
        }

        // O(1)
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            if (this.items != null)
            {
                return this.items.Values.GetEnumerator();
            }
            return (new LinkedList<TElement>()).GetEnumerator();
        }

        #endregion
    }
}
