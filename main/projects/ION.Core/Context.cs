//
//	ION Framework - Core Classes
//	Copyright (C) 2007  GAIPS / INESC-ID Lisboa
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//
//	Author:  Marco Vala <marco.vala@inesc-id.pt>
//	Contributions:  Rui Prada, Carlos Martinho, Guilherme Raimundo, Jo�o Dias, Rui Figueiredo
//
//	Revision History:
//
//
using System;
using System.Collections.Generic;

namespace ION.Core
{
    public class Context
    {
        private Dictionary<string, object> values = new Dictionary<string, object>();
        private Arguments arguments;

        public Context()
        {
            
        }

        internal Context(Context context)
        {
            if (context != null)
            {
                this.values = new Dictionary<string, object>(context.values);
            }
        }

        internal Context(Arguments arguments)
        {
            if (arguments != null)
            {
                foreach (KeyValuePair<string, object> pair in arguments)
                {
                    this.values.Add(pair.Key, pair.Value);
                }
            }
        }


        public Arguments Arguments
        {
            get { return this.arguments; }
            internal set { this.arguments = value; }
        }

        public object this[string key]
        {
            get
            {
                return this.values[key];
            }
            set
            {
                this.values[key] = value;
            }
        }

        public T Get<T>(string name)
        {
            return (T)this.values[name];
        }

        public bool Contains(string name)
        {
            return this.values.ContainsKey(name);
        }

        public List<string> Keys
        {
            get { return new List<string>(this.values.Keys); }
        }

        public List<object> Values
        {
            get { return new List<object>(this.values.Values); }
        }
    }
}
