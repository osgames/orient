//
//	ION Framework - Core Classes
//	Copyright (C) 2007  GAIPS / INESC-ID Lisboa
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Lesser General Public
//	License as published by the Free Software Foundation; either
//	version 2.1 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Lesser General Public License for more details.
//
//	You should have received a copy of the GNU Lesser General Public
//	License along with this library; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
//
//	Author:  Marco Vala <marco.vala@inesc-id.pt>
//	Contributions:  Rui Prada, Carlos Martinho, Guilherme Raimundo, Jo�o Dias, Rui Figueiredo
//
//	Revision History:
//
//
using System;
using System.Collections.Generic;

namespace ION.Core
{
    internal class ElementList<TElement> : IFilterable<TElement>
        where TElement : Element
    {
        private Dictionary<ulong, TElement> items;

        public ElementList()
        {
            this.items = null;
        }

        // O(1)
        public void Add(TElement element)
        {
            // add the element to the items collection
            if (this.items == null)
            {
                this.items = new Dictionary<ulong, TElement>();
            }
            this.items.Add(element.UID, element);
        }

        // O(1)
        public void Remove(TElement element)
        {
            if (this.items != null)
            {
                // remove the element from the items collection
                this.items.Remove(element.UID);
                if (this.items.Count == 0)
                {
                    this.items = null;
                }
            }
        }

        // O(1)
        public bool Has(TElement element)
        {
            return (this.items != null && this.items.ContainsKey(element.UID));
        }


        // O(1)
        // throws ElementNotFound
        public TElement Get(ulong uid)
        {
            TElement element;
            if (this.items == null || !this.items.TryGetValue(uid, out element))
            {
                throw new Exception("ElementNotFound");
            }
            return element;
        }

        // O(1)
        // throws ElementNotFound
        public bool TryGet(ulong uid, out TElement element)
        {
            if (this.items == null)
            {
                element = null;
                return false;
            }
            return this.items.TryGetValue(uid, out element);
        }

        // O(1) 
        // throws ElementNotFound 
        public TSubElement Get<TSubElement>(ulong uid)
            where TSubElement : TElement
        {
            TElement element;
            if (this.items == null || !items.TryGetValue(uid, out element) || !(element is TSubElement))
            {
                throw new Exception("ElementNotFound");
            }
            return (TSubElement)element;
        }



        #region IFilterable<TElement> Members

        // O(1)
        public int Count
        {
            get 
            {
                if (this.items != null)
                {
                    return this.items.Count;
                }
                return 0;
            }
        }

        // O(n)
        public IFilterable<TElement> FilteredBy(IFilter<TElement> filter)
        {
            ElementList<TElement> filteredItems = new ElementList<TElement>();
            if (this.items != null)
            {
                foreach (TElement item in this.items.Values)
                {
                    if (!filter.Blocks(item))
                    {
                        filteredItems.Add(item);
                    }
                }
            }
            return filteredItems;
        }

        // O(1)
        public IEnumerator<TElement> GetEnumerator()
        {
            if (this.items != null)
            {
                return this.items.Values.GetEnumerator();
            }
            return (new LinkedList<TElement>()).GetEnumerator();
        }

        // O(1)
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            if (this.items != null)
            {
                return this.items.Values.GetEnumerator();
            }
            return (new LinkedList<TElement>()).GetEnumerator();
        }

        #endregion
    }
}
