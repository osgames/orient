/* ****************************************************
 * Name: IDestroyable.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/21 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
namespace PAGE
{
    /// <summary>
    /// Represents an object that can be destroyed and reseted.
    /// </summary>
    public interface IDestroyable  : IResetable
    {
        /// <summary>
        /// Gets a value indicating if the objects has been destroyed.
        /// </summary>
        bool Destroyed { get;}

        /// <summary>
        /// Destroys the object.
        /// </summary>
        void Destroy();

        /// <summary>
        /// Checks if the element has been destroyed (by a call to Destroy), in which 
        /// case an exception is thrown.
        /// </summary>
        /// <exception cref="ObjectDestroyedException">if the element has been 
        /// destroyed.</exception>
        void VerifyDestruction();
    }
}
