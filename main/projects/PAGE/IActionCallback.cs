/* ****************************************************
 * Name: IActionCallback.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Resources.Actions;

namespace PAGE
{
    public interface IActionCallback
    {
        /// <summary>
        /// Called when the action somehow succeeded.
        /// </summary>
        void SendSuccess();

        /// <summary>
        /// Called when the action somehow failed.
        /// </summary>
        void SendFailure();

        /// <summary>
        /// Called when an action event was triggered during the action execution.
        /// </summary>
        void SendTrigger(ActionTrigger trigger);
    }
}