/* ****************************************************
 * Name: Form.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/01/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

namespace PAGE.Generic.GUI
{
    /// <summary>
    /// Respresents a gui form containing 2D widgets for
    /// user manipulation and interaction.
    /// </summary>
    public abstract class Form : IDestroyable
    {
        #region Fields

        protected Destroyable destroyable = new Destroyable();
        protected string name = "";         //form name

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the form visibility property.
        /// </summary>
        public abstract bool Visible { get; set;}

        /// <summary>
        /// Activates this form, making it respond to user input.
        /// </summary>
        public abstract void Activate();

        /// <summary>
        /// Gets the form's name.
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        public bool Destroyed
        {
            get { return this.destroyable.Destroyed; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a form with the given name.
        /// </summary>
        /// <param name="name">The form's name.</param>
        public Form(string name)
        {
            this.name = name;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Destroys the form by setting Destroyed property to true.
        /// Further use of the form can originate ObjectDestroyedException.
        /// </summary>
        public virtual void Destroy()
        {
            this.destroyable.Destroy();
        }

        public void VerifyDestruction()
        {
            this.destroyable.VerifyDestruction();
        }

        public void Reset()
        {
            this.destroyable.Reset();
        }
        #endregion
    }
}