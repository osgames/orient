/* ****************************************************
 * Name: IFollowAssetText.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/09 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Domain.Assets;
using PAGE.Generic.GUI;
using PAGE.Generic.Math;

namespace PAGE.Generic.GUI
{
    /// <summary>
    /// Represents a text box that "follows" an asset whenever it moves, and
    /// that is placed in a certain position relative to the asset.
    /// </summary>
    public interface IFollowAssetText : IStaticDisplayText, IUpdatable
    {
        /// <summary>
        /// Gets the character associated with the text.
        /// </summary>
        IInteractionAsset Asset { get; }

        /// <summary>
        /// Gets or sets the position of the text relative to the character.
        /// </summary>
        Position RelativeTextPosition { get; set;}
    }
}