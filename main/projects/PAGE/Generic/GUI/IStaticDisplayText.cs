/* ****************************************************
 * Name: IStaticDisplayText.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/09 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Generic;
using PAGE.Generic.GUI;
using PAGE.Generic.Math;

namespace PAGE.Generic.GUI
{
    public enum TextAlignment { Center, Left, Right };

    public interface IStaticDisplayText
    {
        ScreenPosition Position { get; set; }

        int LineLenght { get; set; }

        string Text { get; set; }

        bool Visible { get; set; }

        float CharHeight { get; set; }

        Color Color { get; set; }

        bool Shadow { get; set; }

        TextAlignment Alignment { get; set; }
    }
}
