/* ****************************************************
 * Name: GUIManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/01/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Collections.Generic;

namespace PAGE.Generic.GUI
{
    public abstract class GUIManager : IUpdatable
    {
        #region Fields

        //if the manager should handle the input devices
        protected bool handleInput = true;

        //2D texts
        protected List<IStaticDisplayText> texts = new List<IStaticDisplayText>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the GUI's cursor visibility property.
        /// </summary>
        public abstract bool CursorVisible { get; set;}

        /// <summary>
        /// Gets or sets a value indicating if the GUI manager 
        /// should handle the input devices.
        /// </summary>
        public bool HandleInput
        {
            get { return handleInput; }
            set { this.handleInput = value; }
        } 

        /// <summary>
        /// Sets all the GUI manager's text visibility state.
        /// </summary>
        public bool AllTextVisible
        {
            set { foreach (IStaticDisplayText text in this.texts) text.Visible = value; }
        }

        #endregion

        #region Protected Abstract Methods

        /// <summary>
        /// Handles the GUI's render window resize.
        /// </summary>
        /// <param name="width">The new window width.</param>
        /// <param name="heigth">The new window height.</param>
        protected abstract void WindowResized(uint width, uint heigth);

        #endregion

        #region Public Methods

        /// <summary>
        /// Updates the GUI manager by handling mouse and keyboard
        /// states.
        /// </summary>
        /// <param name="millisSinceLastUpdate">number of milliseconds 
        /// since last update.</param>
        public abstract void Update(float millisSinceLastUpdate);

        /// <summary>
        /// Adds the given text to the GUI manager.
        /// </summary>
        /// <param name="text">the text to add to the manager.</param>
        public void AddText(IStaticDisplayText text)
        {
            if (!this.texts.Contains(text)) this.texts.Add(text);
        }

        /// <summary>
        /// Removes the given text from the GUI manager's text list.
        /// </summary>
        /// <param name="text">the text to remove from the manager.</param>
        public void RemoveText(IStaticDisplayText text)
        {
            this.texts.Remove(text);
        }

        #endregion
    }
}
