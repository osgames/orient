/* ****************************************************
 * Name: GenericAssetList.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/23 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections.Generic;
using PAGE.Generic.Domain.Assets;

namespace PAGE.Generic.Domain.Collections
{
    /// <summary>
    /// Represents a list of IInteractionAsset objects of some type, with hash
    /// functionality (index by tags).
    /// </summary>
    [Serializable]
    public class GenericAssetList<AssetType> : List<AssetType> where 
        AssetType : IInteractionAsset
    {
        #region Fields

        //indexed by tags actions
        private Dictionary<string, List<AssetType>> list = new Dictionary<string, List<AssetType>>(); 

        #endregion

        #region Constructors

        public GenericAssetList()
        {

        }

        public GenericAssetList(IEnumerable<AssetType> assetList)
        {
            this.AddRange(assetList);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a list of all the assets in the list that have the given tag.
        /// </summary>
        /// <param name="assetTag">the assets tag to look up for.</param>
        /// <returns>A list of all the assets in the list that have the given tag,
        /// or an empty list if no asset has the given tag.</returns>
        public List<AssetType> this[string assetTag]
        {
            get
            {
                if (!this.list.ContainsKey(assetTag)) 
                    return new List<AssetType>();

                List<AssetType> assets = this.list[assetTag];
                if (assets == null) 
                    return new List<AssetType>();

                return new List<AssetType>(assets);
            }
        }

        #endregion

        #region Public Methods

        public new void Add(AssetType asset)
        {
            //adds asset to dictionary indexing by each asset tag
            foreach (string tag in asset.ResourceAsset.Tags)
            {
                List<AssetType> assets;
                if (!this.list.ContainsKey(tag))
                {
                    assets = new List<AssetType>();
                    this.list.Add(tag, assets);
                }
                else
                {
                    assets = this.list[tag];
                }
                assets.Add(asset);
            }

            base.Add(asset);
        }

        public new void Remove(AssetType asset)
        {
            //also removes the asset from the dictionary
            if (!this.Contains(asset)) return;
            foreach (List<AssetType> assets in this.list.Values) assets.Remove(asset);

            base.Remove(asset);
        }

        public new void Clear()
        {
            //also clears dictionary
            this.list.Clear();
            base.Clear();
        }

        public GenericAssetList<AssetType> Clone()
        {
            GenericAssetList<AssetType> assetList = new GenericAssetList<AssetType>();
            foreach(AssetType asset in this) assetList.Add(asset);
            return assetList;
        }
		
        #endregion
    }
}