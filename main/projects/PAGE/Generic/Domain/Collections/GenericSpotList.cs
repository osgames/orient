/* ****************************************************
 * Name: GenericSpotList.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/12 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections.Generic;
using PAGE.Generic.Domain.Spots;

namespace PAGE.Generic.Domain.Collections
{
    /// <summary>
    /// Represents a list of GenericSpot objects of some type, with hash
    /// functionality (index by tags).
    /// </summary>
    [Serializable]
    public class GenericSpotList<SpotType> : List<SpotType> where 
        SpotType : IGenericSpot
    {
        #region Fields

        //indexed by tags actions
        private Dictionary<string, List<SpotType>> list = new Dictionary<string, List<SpotType>>(); 

        #endregion

        #region Constructors

        public GenericSpotList()
        {
        }

        public GenericSpotList(IEnumerable<SpotType> spotList)
        {
            this.AddRange(spotList);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a list of all the spots in the list that have the given tag.
        /// </summary>
        /// <param name="spotTag">the spots tag to look up for.</param>
        /// <returns>A list of all the spots in the list that have the given tag,
        /// or an empty list if no spot has the given tag.</returns>
        public List<SpotType> this[string spotTag]
        {
            get
            {
                if (!this.list.ContainsKey(spotTag)) 
                    return new List<SpotType>();

                List<SpotType> spots = this.list[spotTag];
                if (spots == null) 
                    return new List<SpotType>();

                return new List<SpotType>(spots);
            }
        }

        #endregion

        #region Public Methods

        public new void Add(SpotType spot)
        {
            //adds spot to dictionary indexing by each spot tag
            foreach (string tag in spot.ResourceSpot.Tags)
            {
                List<SpotType> spots;
                if (!this.list.ContainsKey(tag))
                {
                    spots = new List<SpotType>();
                    this.list.Add(tag, spots);
                }
                else
                {
                    spots = this.list[tag];
                }
                spots.Add(spot);
            }

            base.Add(spot);
        }

        public new void Remove(SpotType spot)
        {
            //also removes the spot from the dictionary
            if (!this.Contains(spot)) return;
            foreach (List<SpotType> spots in this.list.Values) spots.Remove(spot);

            base.Remove(spot);
        }

        public new void Clear()
        {
            //also clears dictionary
            this.list.Clear();
            base.Clear();
        }

        public GenericSpotList<SpotType> Clone()
        {
            GenericSpotList<SpotType> spotList = new GenericSpotList<SpotType>();
            foreach(SpotType spot in this) spotList.Add(spot);
            return spotList;
        }
		
        #endregion
    }
}