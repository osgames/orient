/* ****************************************************
 * Name: GenericSpot.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/12 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Collections.Generic;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Math;
using PAGE.Generic.Resources.Spots;

namespace PAGE.Generic.Domain.Spots
{
    /// <summary>
    /// A sport represents a point in the world and is associated with a certain 
    /// asset wich contains it. If the spot is "carrier", the elements that are
    /// attached to it change its orientation and position when the spot's properties
    /// change. A spot is said to be empty when there are no elements atteched to it.
    /// </summary>
    public abstract class GenericSpot : IGenericSpot
    {
        #region Fields

        protected ResourceSpot resourceSpot = null;
        protected ContainerElementHelper elementHelper = null;

        #endregion

        #region Properties

        public virtual ResourceSpot ResourceSpot
        {
            get { this.VerifyDestruction(); return this.resourceSpot; }
        }

        public bool Destroyed
        {
            get { return this.elementHelper.Destroyed; }
        }

        public bool Visible
        {
            get { return this.elementHelper.Visible; }
            set { this.elementHelper.Visible = value; }
        }

        public Position AbsolutePosition
        {
            get { return this.elementHelper.AbsolutePosition; }
            set { this.elementHelper.AbsolutePosition = value; }
        }

        public Orientation AbsoluteOrientation
        {
            get { return this.elementHelper.AbsoluteOrientation; }
            set { this.elementHelper.AbsoluteOrientation = value; }
        }

        public Direction AbsoluteDirection
        {
            get { return this.elementHelper.AbsoluteDirection; }
            set { this.elementHelper.AbsoluteDirection = value; }
        }

        public List<IPlaceableElement> ContainedElements
        {
            get { return this.elementHelper.ContainedElements; }
        }

        public IGraphicalElement ParentElement
        {
            get { return this.elementHelper.ParentElement; }
        }

        public bool IsEmpty
        {
            get { return this.elementHelper.IsEmpty; }
        }

        public virtual bool IsCarrier
        {
            get { return this.elementHelper.IsCarrier; }
        }

        /// <summary>
        /// Gets the object containing the help code for this element.
        /// </summary>
        protected ContainerElementHelper ElementHelper
        {
            get { return this.elementHelper; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new GenericSpot based on the information from the given resource,
        /// and that belongs to the given asset.
        /// </summary>
        /// <param name="resource">the spot's resource info.</param>
        /// <param name="parentElement">the element which the spot is associated to.</param>
        public GenericSpot(ResourceSpot resource, IGraphicalElement parentElement)
        {
            this.resourceSpot = resource;
            this.elementHelper = this.CreateElementHelper(parentElement);
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Creates a new helper for this element.
        /// </summary>
        /// <returns>the ContainerElementHelper with the help code.</returns>
        protected abstract ContainerElementHelper 
            CreateElementHelper(IGraphicalElement parentElement);

        #endregion

        #region Public Methods

        public virtual void Destroy()
        {
            this.elementHelper.Destroy();
        }

        public virtual void Reset()
        {
            this.elementHelper.Reset();
        }

        public virtual void VerifyDestruction()
        {
            this.elementHelper.VerifyDestruction();
        }

        public virtual void AddContainedElement(IPlaceableElement element)
        {
           this.elementHelper.AddContainedElement(element);
        }

        public virtual void RemoveContainedElement(IPlaceableElement element)
        {
            this.elementHelper.RemoveContainedElement(element);
        }

        #endregion
    }
}