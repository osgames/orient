/* ****************************************************
 * Name: GenericWaypoint.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Resources.Spots;

namespace PAGE.Generic.Domain.Spots
{
    /// <summary>
    /// A waypoint is a non-carrier spot that indicates a certain destiny point for 
    /// navigation purposes. Is used as a point to place generic characters.
    /// </summary>
    public abstract class GenericWaypoint : GenericSpot
    {
        #region Properties

        /// <summary>
        /// Returns false. Waypoints are just indication spots for navigation purposes.
        /// </summary>
        public override bool IsCarrier
        {
            get { return false; }
        }

        public new ResourceWaypoint ResourceSpot
        {
            get { return this.resourceSpot as ResourceWaypoint; }
        }

        #endregion

        #region Constructors

        public GenericWaypoint(
            ResourceWaypoint resource, IGraphicalElement parentElement)
            : base(resource, parentElement)
        {
            //waypoints don't carry anything
            this.elementHelper.IsCarrier = false;
        }

        #endregion

        #region Public Methods

        public override void AddContainedElement(IPlaceableElement element)
        {
            if (!(element is GenericCharacter))
                throw new ArgumentException("given element is not a character");

            base.AddContainedElement(element);
        }

        public override void RemoveContainedElement(IPlaceableElement element)
        {
            if (!(element is GenericCharacter))
                throw new ArgumentException("given element is not a character");

            base.RemoveContainedElement(element);
        }

        public void AddContainedElement(GenericCharacter character)
        {
            base.AddContainedElement(character);
        }

        public void RemoveContainedElement(GenericCharacter character)
        {
            base.RemoveContainedElement(character);
        }

        #endregion 
    }
}