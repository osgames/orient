/* ****************************************************
 * Name: GenericSpot.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/12 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Resources.Spots;

namespace PAGE.Generic.Domain.Spots
{
    /// <summary>
    /// A sport represents a point in the world and is associated with a certain 
    /// asset wich contains it. If the spot is "carrier", the elements that are
    /// attached to it change its orientation and position when the spot's properties
    /// change. A spot is said to be empty when there are no elements atteched to it.
    /// </summary>
    public interface IGenericSpot : IContainerElement
    {
        /// <summary>
        /// Gets the resource spot info associated with this spot.
        /// </summary>
        ResourceSpot ResourceSpot { get; }
    }
}