/* ****************************************************
 * Name: Util.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Collections.Generic;
using PAGE.Generic.Math;

namespace PAGE.Generic.Domain.Spots
{
    /// <summary>
    /// Utility class that provides some methods from discovering and order spots.
    /// </summary>
    public class Util
    {
        /// <summary>
        /// Order ascendingly by distance, the spots that are around a given position,
        /// and that belong to the given target list.
        /// </summary>
        /// <param name="fromPosition">the position to calculate the spots distances
        /// from.</param>
        /// <param name="targetSpots">the set of target spots to be ordered.</param>
        /// <param name="emptyOnly">if set to true, only empty spots are considered,
        /// otherwise all spots are considered.</param>
        /// <returns>a new List<IGenericSpot> containing the desired spots ordered
        /// ascendingly by distance to the given position (may be empty).</returns>
        public static List<IGenericSpot> OrderClosestSpots(
            Position fromPosition, List<IGenericSpot> targetSpots, bool emptyOnly)
        {
            //if there are no places available, there is no closest spot 
            if ((targetSpots == null) || (targetSpots.Count == 0))
                return null;

            List<IGenericSpot> orderedTargetSpots = new List<IGenericSpot>();
            List<Distance> orderedDistances = new List<Distance>();

            //for all the defined waypoints, order them as they come
            foreach (IGenericSpot s in targetSpots)
            {
                //ignore if spot is not empty and we're looking only for empty ones
                if(emptyOnly && !s.IsEmpty) continue;

                //calculates distance to target
                Position v = s.AbsolutePosition;
                Distance distance = fromPosition.DistanceTo(v);

                //determines index to insert, keeping correct order
                int insertAtIndex = 0;
                for(; insertAtIndex < orderedDistances.Count; insertAtIndex++)
                {
                    Distance testDistance = orderedDistances[insertAtIndex];
                    if (distance.CompareTo(testDistance) <= 0) break;
                }

                //insert at specified index
                orderedDistances.Insert(insertAtIndex, distance);
                orderedTargetSpots.Insert(insertAtIndex, s);
            }

            return orderedTargetSpots;
        }

        /// <summary>
        /// Order ascendingly by distance, the empty spots that are around a given 
        /// position, and that have the given tag associated with it, and belong to 
        /// the given target list.
        /// </summary>
        /// <param name="fromPosition">the position to calculate the spots distances
        /// from.</param>
        /// <param name="targetSpots">the set of target spots to be ordered.</param>
        /// <returns>a new List<IGenericSpot> containing the desired spots ordered
        /// ascendingly by distance to the given position.</returns>
        public static List<IGenericSpot> OrderClosestEmptySpots(
            Position fromPosition, List<IGenericSpot> targetSpots)
        {
            return OrderClosestSpots(fromPosition, targetSpots, true);
        }

        /// <summary>
        /// Order ascendingly by distance, the spots that are around a given position,
        /// and that have the given tag associated with it, and belong to the given 
        /// target list.
        /// </summary>
        /// <param name="fromPosition">the position to calculate the spots distances
        /// from.</param>
        /// <param name="targetSpots">the set of target spots to be ordered.</param>
        /// <returns>a new List<IGenericSpot> containing the desired spots ordered
        /// ascendingly by distance to the given position.</returns>
        public static List<IGenericSpot> OrderClosestSpots(
            Position fromPosition, List<IGenericSpot> targetSpots)
        {
            return OrderClosestSpots(fromPosition, targetSpots, false);
        }

        /// <summary>
        /// Returns the spot that is closest or farthest from the given position,
        /// and that belong to the given target list.
        /// </summary>
        /// <param name="fromPosition">the position to calculate the closest/fartest
        /// spot from.</param>
        /// <param name="targetSpots">the set of spots to look up for the closest/
        /// farthest from the given position.</param>
        /// <param name="emptyOnly">if set to true, only empty spots are considered,
        /// otherwise all spots are considered.</param>
        /// <param name="closest">if set to true, returns the closest spot, else
        /// the farthest from the given position.</param>
        /// <returns>The closest/farthest spot from the given position, or null if
        /// there is no spot that matches all the conditions.</returns>
        public static IGenericSpot FindSpot(
            Position fromPosition, List<IGenericSpot> targetSpots, 
            bool emptyOnly, bool closest)
        {
            //if there are no places available, there is no close spot 
            if ((targetSpots == null) || (targetSpots.Count == 0))
                return null;

            Distance distance = null;
            IGenericSpot spot = null;

            //for all the defined spots, find the closest one
            foreach (IGenericSpot s in targetSpots)
            {
                //ignore if spot is not empty and we're looking only for empty ones
                if (emptyOnly && !s.IsEmpty) continue;

                //sees if distance is smaller
                Position v = s.AbsolutePosition;
                Distance curDistance = fromPosition.DistanceTo(v);

                if ((distance == null) ||       //first distance, no comparing
                    (( closest && (curDistance.CompareTo(distance) < 0)) ||
                    (!closest && (curDistance.CompareTo(distance) > 0))))
                {
                    distance = curDistance;
                    spot = s;
                }
            }

            return spot;
        }

        /// <summary>
        /// Returns the empty spot that is closest to the given position, and that 
        /// belong to the given target list.
        /// </summary>
        /// <param name="fromPosition">the position to calculate the closest empty
        /// spot from.</param>
        /// <param name="targetSpots">the set of spots to look up for the closest and
        /// empty to the given position.</param>
        /// <returns>The closest empty spot to the given position, or null if there 
        /// is no spot that matches all the conditions.</returns>
        public static IGenericSpot FindClosestEmptySpot(
            Position fromPosition, List<IGenericSpot> targetSpots)
        {
            return FindSpot(fromPosition, targetSpots, true, true);
        }

        /// <summary>
        /// Returns the spot that is closest to the given position, and that belongs 
        /// to the given target list.
        /// </summary>
        /// <param name="fromPosition">the position to calculate the closest spot 
        /// from.</param>
        /// <param name="targetSpots">the set of spots to look up for the closest to 
        /// the given position.</param>
        /// <returns>The closest spot to the given position, or null if there is no 
        /// spot that matches all the conditions.</returns>
        public static IGenericSpot FindClosestSpot(
            Position fromPosition, List<IGenericSpot> targetSpots)
        {
            return FindSpot(fromPosition, targetSpots, false, true);
        }


        /// <summary>
        /// Returns the empty spot that is farthest from the given position, and that 
        /// belongs to the given target list.
        /// </summary>
        /// <param name="fromPosition">the position to calculate the farthest empty
        /// spot from.</param>
        /// <param name="targetSpots">the set of spots to look up for the farthest and
        /// empty from the given position.</param>
        /// <returns>The farthest empty spot from the given position, or null if there 
        /// is no spot that matches all the conditions.</returns>
        public static IGenericSpot FindFarthestEmptySpot(
            Position fromPosition, List<IGenericSpot> targetSpots)
        {
            return FindSpot(fromPosition, targetSpots, true, false);
        }

        /// <summary>
        /// Returns the spot that is farthest from the given position, and that belongs 
        /// to the given target list.
        /// </summary>
        /// <param name="fromPosition">the position to calculate the farthest spot 
        /// from.</param>
        /// <param name="targetSpots">the set of spots to look up for the farthest to 
        /// the given position.</param>
        /// <returns>The farthest spot to the given position, or null if there is no 
        /// spot that matches all the conditions.</returns>
        public static IGenericSpot FindFarthestSpot(
            Position fromPosition, List<IGenericSpot> targetSpots)
        {
            return FindSpot(fromPosition, targetSpots, false, false);
        }
    }
}