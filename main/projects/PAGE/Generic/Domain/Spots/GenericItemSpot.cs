/* ****************************************************
 * Name: GenericItemSpot.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/23 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Resources.Spots;

namespace PAGE.Generic.Domain.Spots
{
    /// <summary>
    /// Represents a spot that can carry items in it.
    /// </summary>
    public abstract class GenericItemSpot : GenericSpot
    {
        #region Properties

        public new ResourceItemSpot ResourceSpot
        {
            get { return this.resourceSpot as ResourceItemSpot; }
        }

        #endregion

        #region Constructors

        public GenericItemSpot(
            ResourceItemSpot resource, IGraphicalElement parentElement)
            :base(resource, parentElement)
        {
            this.elementHelper.IsCarrier = resource.IsCarrier;
        }

        #endregion

        #region Public Methods

        public override void AddContainedElement(IPlaceableElement element)
        {
            if (!(element is GenericItem))
                throw new ArgumentException("given element is not an item");

            base.AddContainedElement(element);
        }

        public override void RemoveContainedElement(IPlaceableElement element)
        {
            if (!(element is GenericItem))
                throw new ArgumentException("given element is not an item");

            base.RemoveContainedElement(element);
        }

        public void AddContainedElement(GenericItem item)
        {
            base.AddContainedElement(item);
        }

        public void RemoveContainedElement(GenericItem item)
        {
            base.RemoveContainedElement(item);
        }

        #endregion
    }
}