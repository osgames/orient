/* ****************************************************
 * Name: GenericCameraSpot.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using PAGE.Generic.Domain.Cameras;
using PAGE.Generic.Resources.Spots;

namespace PAGE.Generic.Domain.Spots
{
    /// <summary>
    /// Represents a spot that can carry cameras in it.
    /// </summary>
    public abstract class GenericCameraSpot : GenericSpot
    {
        #region Properties

        /// <summary>
        /// Returns true. Camera spots carry cameras in it.
        /// </summary>
        public override bool IsCarrier
        {
            get { return true; }
        }

        public new ResourceCameraSpot ResourceSpot
        {
            get { return (ResourceCameraSpot)this.resourceSpot; }
        }

        #endregion

        #region Constructors

        public GenericCameraSpot(ResourceCameraSpot resource, IGraphicalElement parentElement)
            :base(resource, parentElement)
        {
            //camera spots carry cameras in it
            this.elementHelper.IsCarrier = true;
        }

        #endregion

        #region Public Methods

        public override void AddContainedElement(IPlaceableElement element)
        {
            if (!(element is GenericCamera))
                throw new ArgumentException("given element is not a Camera");

            base.AddContainedElement(element);
        }

        public override void RemoveContainedElement(IPlaceableElement element)
        {
            if (!(element is GenericCamera))
                throw new ArgumentException("given element is not a Camera");

            base.RemoveContainedElement(element);
        }

        public void AddContainedElement(GenericCamera camera)
        {
            base.AddContainedElement(camera);
        }

        public void RemoveContainedElement(GenericCamera camera)
        {
            base.RemoveContainedElement(camera);
        }

        #endregion
    }
}