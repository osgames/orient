/* ****************************************************
 * Name: IGraphicalElement.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/22 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Math;

namespace PAGE.Generic.Domain
{
    /// <summary>
    /// Represents a graphical element to be represented by the realizer.
    /// </summary>
    public interface IGraphicalElement : IDestroyable
    {
        /// <summary>
        /// Gets or sets a property that tells if the asset is 
        /// currently visible / invisible.
        /// </summary>
        bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the absolute position of the asset.
        /// </summary>
        Position AbsolutePosition { get; set; }

        /// <summary>
        /// Gets or sets the absolute orientation of the asset.
        /// </summary>
        Orientation AbsoluteOrientation { get; set; }

        /// <summary>
        /// Gets or sets the absolute direction of the asset.
        /// </summary>
        Direction AbsoluteDirection { get; set; }
    }
}
