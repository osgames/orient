/* ****************************************************
 * Name: ContainerElementHelper.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/01 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Collections.Generic;
using PAGE.Generic.Domain.Helpers;

namespace PAGE.Generic.Domain.Helpers
{
    /// <summary>
    /// Represents a graphical element that can attach other elements to it. If the 
    /// element is also "carrier", the attached elements will also change their position 
    /// and orientation according to the element's graphical properties.
    /// </summary>
    public abstract class ContainerElementHelper : GraphicalElementHelper, IContainerElement
    {
        #region Fields

        protected IGraphicalElement parentElement = null;
        protected List<IPlaceableElement> containedElements = 
            new List<IPlaceableElement>();

        protected bool isCarrier = true;

        #endregion

        #region Constructors

        public ContainerElementHelper(
            IContainerElement helpedElement, IGraphicalElement parentElement)
            : base(helpedElement)
        {
            this.parentElement = parentElement;
        }

        #endregion

        #region Properties

        protected new IContainerElement HelpedElement
        {
            get { return this.helpedElement as IContainerElement; }
        }

        public List<IPlaceableElement> ContainedElements
        {
            get
            {
                //returns a copy of the list
                this.destroyable.VerifyDestruction();
                return new List<IPlaceableElement>(this.containedElements);
            }
        }

        public IGraphicalElement ParentElement
        {
            get { this.VerifyDestruction(); return parentElement; }
        }

        public bool IsEmpty
        {
            get { this.VerifyDestruction(); return this.containedElements.Count == 0; }
        }

        public virtual bool IsCarrier
        {
            get { return this.isCarrier; }
            set { this.isCarrier = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Resets this container by unplacing all the elements attached.
        /// </summary>
        public override void Reset()
        {
            this.Clear();
        }

        public override void Destroy()
        {
            if (this.destroyable.Destroyed) return;  //ignore destroy call
            this.Clear();                            //detaches all elements
            base.Destroy();
        }

        public virtual void AddContainedElement(IPlaceableElement element)
        {
            this.VerifyDestruction();

            //places the element according to the spot's current position and orientation (is spot does not belong to a character)
            element.AbsolutePosition = this.HelpedElement.AbsolutePosition;

            bool orientate = true;
            if ((this.HelpedElement.ParentElement is Assets.GenericAsset))
            {
                Assets.GenericAsset asset = this.HelpedElement.ParentElement as Assets.GenericAsset;
                if (asset.ResourceAsset is PAGE.Generic.Resources.Assets.ResourceCharacter)
                    orientate = false;            
            }

            if (orientate) element.AbsoluteOrientation = this.HelpedElement.AbsoluteOrientation;

            //if spot is element-carrier, attaches element to spot
            if (this.IsCarrier) this.AttachElement(element);

            if (!this.containedElements.Contains(element))
                this.containedElements.Add(element);
        }

        public virtual void RemoveContainedElement(IPlaceableElement element)
        {
            this.VerifyDestruction();

            //if spot is element-carrier, dettaches element from spot
            if (this.IsCarrier) this.DetachElement(element);

            this.containedElements.Remove(element);
        }

        #endregion

        #region Abstract & Virtual Methods

        /// <summary>
        /// Attaches the given graphical element to the spot. The element's position 
        /// and orientation will change overtime as the spot also changes.
        /// </summary>
        /// <param name="element">the element to attach to the spot.</param>
        protected abstract void AttachElement(IPlaceableElement element);

        /// <summary>
        /// Detaches the given graphical element from the spot. The element's position 
        /// and orientation will no longer change when the spot also changes.
        /// </summary>
        /// <param name="element">the element to detach from the spot.</param>
        protected abstract void DetachElement(IPlaceableElement element);

        #endregion

        #region Private Methods

        /// <summary>
        /// Clears the spot by detaching all associated elements from the spot.
        /// </summary>
        private void Clear()
        {
            this.VerifyDestruction();

            foreach (IPlaceableElement element in new List<IPlaceableElement>(this.containedElements))
            {
                //unplaces all contained assets in this spot
                element.Unplace();
            }
            //removes all contained assets
            this.containedElements.Clear();
        } 

        #endregion
    }
}