/* ****************************************************
 * Name: PlaceableElementHelper.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Collections;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Math;

namespace PAGE.Generic.Domain.Helpers
{
    public abstract class PlaceableElementHelper : GraphicalElementHelper, IPlaceableElement
    {
        #region Fields

        protected IGenericSpot currentSpot;     //assets's current position

        protected bool isPlacing = false;

        #endregion

        #region Properties

        protected new IPlaceableElement HelpedElement
        {
            get{ return this.helpedElement as IPlaceableElement;}
        }

        public IGenericSpot CurrentSpot
        {
            get { return this.currentSpot; }
            set
            {
                this.isPlacing = true;

                if(this.currentSpot != null)
                {
                    this.currentSpot.RemoveContainedElement(this.HelpedElement);
                }
                this.currentSpot = value;
                if (this.currentSpot != null)
                {
                    this.currentSpot.AddContainedElement(this.HelpedElement);
                }

                this.isPlacing = false;
            }
        }

        #endregion

        #region Constructors

        public PlaceableElementHelper(IPlaceableElement helpedElement)
            :base(helpedElement)
        {
        }

        #endregion

        #region Public Methods

        public abstract GenericSpotList<IGenericSpot> 
            GetPlaceableSpots(IInteractionAsset targetSpotContainer);

        public virtual void Place(
            IActionCallback callback, IInteractionAsset targetSpotContainer, string targetID, bool inEmptySpot)
        {
            this.VerifyDestruction();

            //gets all possible spots from container
            List<IGenericSpot> possibleSpots;

            if ((targetID != null) && (targetID != ""))
                possibleSpots = this.GetPlaceableSpots(targetSpotContainer)[targetID];
            else
                possibleSpots = this.GetPlaceableSpots(targetSpotContainer);

            //finds closest spot
            GenericSpot placeSpot = (GenericSpot) Spots.Util.FindClosestSpot(
                this.HelpedElement.AbsolutePosition, possibleSpots);

            //checks if asset is already in final position
            if ((this.currentSpot != null) && (this.currentSpot == placeSpot))
            {
                if (callback != null) callback.SendSuccess();     //calls callback success
                return;
            }

            if (inEmptySpot)
            {
                //gets closest empty spot
                placeSpot = (GenericSpot)Spots.Util.FindClosestEmptySpot(
                    this.HelpedElement.AbsolutePosition, possibleSpots);
            }

            //positions asset
            if (placeSpot != null)
            {
                this.CurrentSpot = placeSpot;                   //saves current position
                if (callback != null) callback.SendSuccess();   //calls callback success
            }
            else
            {
                if (callback != null) callback.SendFailure();   //calls callback failure
            }
        }

        public virtual void Place(
            IActionCallback callback, IInteractionAsset targetSpotContainer, string targetID)
        {
            this.Place(callback, targetSpotContainer, targetID, true);
        }

        public virtual void Place(IActionCallback callback, IInteractionAsset targetSpotContainer, bool inEmptySpot)
        {
            this.Place(callback, targetSpotContainer, "", inEmptySpot);
        }

        public virtual void Place(IActionCallback callback, IInteractionAsset targetSpotContainer)
        {
            this.Place(callback, targetSpotContainer, "", true);
        }

        public virtual void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer, 
            string targetID, bool inEmptySpot)
        {
            this.VerifyDestruction();

            //gets all possible spots from container
            List<IGenericSpot> possibleSpots;

            if ((targetID != null) && (targetID != ""))
                possibleSpots = this.GetPlaceableSpots(targetSpotContainer)[targetID];
            else
                possibleSpots = this.GetPlaceableSpots(targetSpotContainer);

            List<int> nonEmptySpotsIndexes = new List<int>();
            IGenericSpot placeSpot = null;
            Random random = new Random((int)DateTime.Now.Ticks);

            //gets random spot from possible spots
            while (nonEmptySpotsIndexes.Count < possibleSpots.Count)
            {
                int index = random.Next(0, possibleSpots.Count - 1);
                placeSpot = null;   //resets place spot

                //checks if index was already tested
                if (inEmptySpot && nonEmptySpotsIndexes.Contains(index))
                {
                    continue;
                }

                //gets spot in index
                placeSpot = possibleSpots[index];

                if (inEmptySpot){
                    if (!placeSpot.IsEmpty)
                    {
                        nonEmptySpotsIndexes.Add(index);
                        continue;   //spot is not empty, mark and continue
                    }
                    else
                    {
                        break;      //spot is empty, it serves
                    }
                }
                break;              //empty random spot found
            }
            
            //positions asset
            if (placeSpot != null)
            {
                this.CurrentSpot = placeSpot;   //saves current position

                if (callback != null) callback.SendSuccess();   //calls callback success
            }
            else
            {
                if (callback != null) callback.SendFailure();   //calls callback failure
            }
        }

        public virtual void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer, string targetID)
        {
            this.VerifyDestruction();

            this.PlaceRandomly(callback, targetSpotContainer, targetID, true);
        }

        public virtual void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer, bool inEmptySpot)
        {
            this.PlaceRandomly(callback, targetSpotContainer, "", inEmptySpot);
        }

        public virtual void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer)
        {
            this.PlaceRandomly(callback, targetSpotContainer, true);
        }

        public virtual void Unplace()
        {
            this.VerifyDestruction();

            if (!this.isPlacing)
            {
                this.CurrentSpot = null; //unloads from current position
            }
        }

        public virtual void TurnTo(IGraphicalElement toElement)
        {
            this.VerifyDestruction();
            this.HelpedElement.AbsoluteDirection =
                this.HelpedElement.AbsolutePosition.DirectionTo(toElement.AbsolutePosition);
        }

        /// <summary>
        /// Added 16/09/08 by Michael: I needed this function to make the character turn towards the User's
        /// position (since the user does not have an IGraphicalElement)
        /// </summary>
        /// <param name="absoluteTargetPosition"> the position to turn to</param>
        public virtual void TurnTo(Position absoluteTargetPosition)
        {
            this.VerifyDestruction();
            this.HelpedElement.AbsoluteDirection =
                this.HelpedElement.AbsolutePosition.DirectionTo(absoluteTargetPosition);
        }

        #endregion

        #region Override Methods

        public override void Reset()
        {
            base.Reset();

            //takes out asset from current spot
            this.Unplace();
        }

        public override void Destroy()
        {
            //takes out asset from current spot
            this.Unplace();

            base.Destroy();
        }

        #endregion
    }
}