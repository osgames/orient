/* ****************************************************
 * Name: GraphicalElementHelper.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/28 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Math;

namespace PAGE.Generic.Domain.Helpers
{
    /// <summary>
    /// Represents a graphical element to be represented by the realizer.
    /// </summary>
    public abstract class GraphicalElementHelper : IGraphicalElement
    {
        #region Fields

        protected IGraphicalElement helpedElement = null;

        protected bool visible = true;
        protected IDestroyable destroyable = new Destroyable();

        #endregion

        #region Properties

        /// <summary>
        /// Gets the element being helper by this helper.
        /// </summary>
        protected IGraphicalElement HelpedElement
        {
            get{ return this.helpedElement;}
        }

        public abstract Direction AbsoluteDirection { get; set; }

        public abstract Position AbsolutePosition { get; set; }

        public abstract Orientation AbsoluteOrientation { get; set; }

        public virtual bool Visible
        {
            get { return this.visible; }
            set { this.visible = value; }
        }

        public virtual bool Destroyed
        {
            get { return this.destroyable.Destroyed; }
        }

        #endregion

        #region Constructors

        public GraphicalElementHelper(IGraphicalElement helpedElement)
        {
            this.helpedElement = helpedElement;
        }

        #endregion

        #region Virtual Methods

        /// <summary>
        /// Resets the element by setting its visibility to false.
        /// </summary>
        public virtual void Reset()
        {
            //re-generates element
            this.destroyable.Reset();
            this.Visible = false;
        }

        public virtual void Destroy()
        {
            this.destroyable.Destroy();
        }

        public virtual void VerifyDestruction()
        {
            this.destroyable.VerifyDestruction();
        }

        #endregion
    }
}