/* ****************************************************
 * Name: IMovableElement.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Generic.Domain;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Collections;
using PAGE.Generic.Domain.Spots;

namespace PAGE.Generic.Domain
{
    /// <summary>
    /// Represents an element that is movable, i.e., that can be placed in some
    /// spot at a given time.
    /// </summary>
    public interface IPlaceableElement : IGraphicalElement
    {
        /// <summary>
        /// Gets or sets the element's current spot. When overriden by a derived class, 
        /// changes the element position in the world.
        /// </summary>
        IGenericSpot CurrentSpot { get; set; }

        /// <summary>
        /// Gets the list of spots from a target container where this element can be 
        /// placed on.
        /// </summary>
        /// <param name="targetSpotContainer">the target asset containing the spots
        /// to place the element on.</param>
        /// <returns>the list of spots from a target container where this element can
        /// be placed on</returns>
        GenericSpotList<IGenericSpot> GetPlaceableSpots(IInteractionAsset targetSpotContainer);

        /// <summary>
        /// Tries to place the element in one spot that has the given tag associated 
        /// with it, and belonging to the given target. If two or more spots have the 
        /// given tag associated, the closest is used.
        /// </summary>
        /// <param name="callback">the callback to be called in case the action succeeds, 
        /// i.e., the element is placed in a valid target spot, or otherwise fails.</param>
        /// <param name="targetSpotContainer">the asset that contains the desired spot 
        /// to place the element on.</param>
        /// <param name="targetID">the tag of the desired spot to look up for.</param>
        /// <param name="inEmptySpot">if set to true, only empty spots are considered
        /// valid to place the element, otherwise all spots are considered.</param>
        void Place(
            IActionCallback callback, IInteractionAsset targetSpotContainer, string targetID, bool inEmptySpot);

        /// <summary>
        /// Tries to place the element in one empty spot that has the given tag 
        /// associated with it, and belonging to the given target. If two or more 
        /// spots have the given tag associated, the closest is used.
        /// </summary>
        /// <param name="callback">the callback to be called in case the action succeeds, 
        /// i.e., the element is placed in a valid target spot, or otherwise fails.</param>
        /// <param name="targetSpotContainer">the asset that contains the desired spot 
        /// to place the element on.</param>
        /// <param name="targetID">the tag of the desired spot to look up for.</param>
        void Place(
            IActionCallback callback, IInteractionAsset targetSpotContainer, string targetID);

        /// <summary>
        /// Tries to place the element in one spot that belongs to the given target. 
        /// If two or more spots have the given tag associated, the closest is used.
        /// </summary>
        /// <param name="callback">the callback to be called in case the action succeeds, 
        /// i.e., the element is placed in a valid target spot, or otherwise fails.</param>
        /// <param name="targetSpotContainer">the asset that contains the desired spot 
        /// to place the element on.</param>
        /// <param name="inEmptySpot">if set to true, only empty spots are considered
        /// valid to place the element, otherwise all spots are considered.</param>
        void Place(IActionCallback callback, IInteractionAsset targetSpotContainer, bool inEmptySpot);

        /// <summary>
        /// Tries to place the element in one empty spot that belongs to the given target. 
        /// If two or more spots have the given tag associated, the closest is used.
        /// </summary>
        /// <param name="callback">the callback to be called in case the action succeeds, 
        /// i.e., the element is placed in a valid target spot, or otherwise fails.</param>
        /// <param name="targetSpotContainer">the asset that contains the desired spot 
        /// to place the element on.</param>
        void Place(IActionCallback callback, IInteractionAsset targetSpotContainer);

        /// <summary>
        /// Tries to place the element in one spot that has the given tag associated 
        /// with it, and belonging to the given target. If two or more spots have the 
        /// given tag associated, one is randomly choosed from the list.
        /// </summary>
        /// <param name="callback">the callback to be called in case the action succeeds, 
        /// i.e., the element is placed in a valid target spot, or otherwise fails.</param>
        /// <param name="targetSpotContainer">the asset that contains the desired spot 
        /// to place the element on.</param>
        /// <param name="targetID">the tag of the desired spot to look up for.</param>
        /// <param name="inEmptySpot">if set to true, only empty spots are considered
        /// valid to place the element, otherwise all spots are considered.</param>
        void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer,
            string targetID, bool inEmptySpot);

        /// <summary>
        /// Tries to place the element in one empty spot that has the given tag 
        /// associated with it, and belonging to the given target. If two or more 
        /// spots have the given tag associated, one is randomly choosed from the list.
        /// </summary>
        /// <param name="callback">the callback to be called in case the action succeeds, 
        /// i.e., the element is placed in a valid target spot, or otherwise fails.</param>
        /// <param name="targetSpotContainer">the asset that contains the desired spot 
        /// to place the element on.</param>
        /// <param name="targetID">the tag of the desired spot to look up for.</param>
        void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer, string targetID);

        /// <summary>
        /// Tries to place the element in one spot that belongs to the given target. 
        /// If two or more spots have the given tag associated, one is randomly choosed 
        /// from the list.
        /// </summary>
        /// <param name="callback">the callback to be called in case the action succeeds, 
        /// i.e., the element is placed in a valid target spot, or otherwise fails.</param>
        /// <param name="targetSpotContainer">the asset that contains the desired spot 
        /// to place the element on.</param>
        /// <param name="inEmptySpot">if set to true, only empty spots are considered
        /// valid to place the element, otherwise all spots are considered.</param>
        void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer, bool inEmptySpot);


        /// <summary>
        /// Tries to place the element in one empty spot that belongs to the given 
        /// target. If two or more spots have the given tag associated, one is randomly 
        /// choosed from the list.
        /// </summary>
        /// <param name="callback">the callback to be called in case the action succeeds, 
        /// i.e., the element is placed in a valid target spot, or otherwise fails.</param>
        /// <param name="targetSpotContainer">the asset that contains the desired spot 
        /// to place the element on.</param>
        void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer);

        /// <summary>
        /// Unplaces the element from the current spot. If the element had indeed one 
        /// spot associated with it, the spot will no longer hold the element.
        /// </summary>
        void Unplace();

        /// <summary>
        /// Rotates the element to face (front vector) the given element's position.
        /// </summary>
        /// <param name="element">the element to turn to.</param>
        void TurnTo(IGraphicalElement element);
    }
}