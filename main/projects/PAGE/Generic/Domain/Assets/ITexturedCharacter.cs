/* ****************************************************
 * Name: ITexturedCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Management.Character;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// Textured characters can change some textures associated with their graphical
    /// representation. 
    /// </summary>
    public interface ITexturedCharacter : ICharacter
    {
        /// <summary>
        /// Gets the texture manager associated with this character.
        /// </summary>
        TextureManager TextureManager { get;}

        /// <summary>
        /// Changes the current texture of the given body part.
        /// </summary>
        /// <param name="bodyPartName">the name of the body part to change the given
        /// texture to.</param>
        /// <param name="textureName">the name of the texture to set in the given
        /// body part.</param>
        void ChangeTexture(string bodyPartName, string textureName);

        /// <summary>
        /// Changes the current texture of the given body part.
        /// </summary>
        /// <param name="bodyPartIndex">the index of the body part to change the given
        /// texture to.</param>
        /// <param name="textureName">the name of the texture to set in the given
        /// body part.</param>
        void ChangeTexture(int bodyPartIndex, string textureName);
    }
}
