/* ****************************************************
 * Name: IAnimatableCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Management.Character;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// Animatable characters can be animated. If a callback is associated, animations
    /// can fail, succeed when finished or have some triggers that will fire in the 
    /// middle of the animation.
    /// </summary>
    public interface IAnimatableCharacter : ICharacter
    {
        /// <summary>
        /// Gets the animation manager associated with this character.
        /// </summary>
        AnimationManager AnimationManager { get;}

        /// <summary>
        /// Starts animating the character according to the given action.
        /// </summary>
        /// <param name="callback">the callback to receive the animation events from:
        /// Succeeded if the animation finishes, Failed if there is no action defined
        /// with the given name, ActionTrigger when pre-defined animation times are
        /// reached.
        /// </param>
        /// <param name="actionID">the name of the action with the animation info.</param>
        /// <param name="randomStartTime">if set to true, the animation will start
        /// at a random start position.</param>
        /// <param name="loop">if set to true, animation will loop until stop order.</param>
        void Animate(IActionCallback callback, string actionID, bool randomStartTime, bool loop);

        /// <summary>
        /// Starts animating the character according to the given action.
        /// </summary>
        /// <param name="callback">the callback to receive the animation events from:
        /// Succeeded if the animation finishes, Failed if there is no action defined
        /// with the given name, ActionTrigger when pre-defined animation times are
        /// reached.
        /// </param>
        /// <param name="actionID">the name of the action with the animation info.</param>
        /// <param name="randomStartTime">if set to true, the animation will start
        /// at a random start position.</param>
        void Animate(IActionCallback callback, string actionID, bool randomStartTime);

        /// <summary>
        /// Starts animating the character according to the given action. The animation
        /// starts at it's start time.
        /// </summary>
        /// <param name="callback">the callback to receive the animation events from:
        /// Succeeded if the animation finishes, Failed if there is no action defined
        /// with the given name, ActionTrigger when pre-defined animation times are
        /// reached.
        /// </param>
        /// <param name="actionID">the name of the action with the animation info.</param>
        void Animate(IActionCallback callback, string actionID);

        /// <summary>
        /// Stops any animation being performed by the character.
        /// </summary>
        void StopAnimating();

        /// <summary>
        /// Gets the number of seconds of the animation being performed by the character.
        /// </summary>
        float CurrentAnimationLenght { get;}

        /// <summary>
        /// Gets the number of seconds of animation left to be performed by the character.
        /// </summary>
        float CurrentAnimationTimeLeft { get;}
    }
}
