/* ****************************************************
 * Name: GenericAsset.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using PAGE.Generic.Domain.Collections;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Management;
using PAGE.Generic.Math;
using PAGE.Generic.Resources.Assets;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// An asset consists in a visual representation in the Realizer of an entity of 
    /// the system. As such, an asset can be (in)visible, has a certain position and 
    /// orientation in the world, and has a set of camera spots so that cameras can 
    /// follow the assets around, and waypoints indicating special positions around 
    /// them. An asset can also have other sub-assets associated with it.
    /// </summary>
    public abstract class GenericAsset : IInteractionAsset
    {
        #region Fields

        protected List<AssetManager> managers = new List<AssetManager>();
        protected PositionManager positionManager;
        protected TextManager textManager;

        protected GenericSpotList<GenericCameraSpot> cameraSpots = new GenericSpotList<GenericCameraSpot>();
        protected GenericSpotList<GenericWaypoint> waypoints = new GenericSpotList<GenericWaypoint>();
        protected GenericSpotList<GenericItemSpot> itemSpots = new GenericSpotList<GenericItemSpot>();
        protected GenericAssetList<DynamicAsset> containedAssets = new GenericAssetList<DynamicAsset>();

        protected ResourceAsset resourceAsset;
        protected bool spotsCreated;

        protected GraphicalElementHelper elementHelper;

        #endregion

        #region Properties

        public virtual GenericSpotList<GenericSpot> AllSpots
        {
            get
            {
                GenericSpotList<GenericSpot> list = new GenericSpotList<GenericSpot>();
                foreach (GenericCameraSpot spot in this.cameraSpots) list.Add(spot);
                foreach (GenericWaypoint   spot in this.waypoints) list.Add(spot);
                foreach (GenericItemSpot   spot in this.itemSpots) list.Add(spot);
                return list;
            }
        }

        public GenericSpotList<GenericWaypoint> Waypoints
        {
            get
            {
                this.VerifyDestruction();
                return this.waypoints;
            }
        }

        public GenericSpotList<GenericCameraSpot> CameraSpots
        {
            get
            {
                this.VerifyDestruction();
                return this.cameraSpots;
            }
        }

        public GenericSpotList<GenericItemSpot> ItemSpots
        {
            get
            {
                this.VerifyDestruction();
                return this.itemSpots;
            }
        }

        public ResourceAsset ResourceAsset
        {
            get
            {
                this.VerifyDestruction();
                return resourceAsset;
            }
        }

        public bool Destroyed
        {
            get { return this.elementHelper.Destroyed; }
        }

        public virtual bool Visible
        {
            get
            {
                this.VerifyDestruction();
                return this.elementHelper.Visible;
            }
            set
            {
                this.VerifyDestruction();
                this.elementHelper.Visible = value;
            }
        }

        public virtual Position AbsolutePosition
        {
            get
            {
                this.VerifyDestruction();
                return this.elementHelper.AbsolutePosition;
            }
            set
            {
                this.VerifyDestruction();
                this.ClearNonCarrierSpots(); //we're moving, clear spots
                this.elementHelper.AbsolutePosition = value;
            }
        }

        public virtual Orientation AbsoluteOrientation
        {
            get
            {
                this.VerifyDestruction();
                return this.elementHelper.AbsoluteOrientation;
            }
            set
            {
                this.VerifyDestruction();
                this.ClearNonCarrierSpots(); //we're rotating, clear spots
                this.elementHelper.AbsoluteOrientation = value;
            }
        }

        public virtual Direction AbsoluteDirection
        {
            get
            {
                this.VerifyDestruction();
                return this.elementHelper.AbsoluteDirection;
            }
            set
            {
                this.VerifyDestruction();
                this.ClearNonCarrierSpots(); //we're rotating, clear spots
                this.elementHelper.AbsoluteDirection = value;
            }
        }

        /// <summary>
        /// Gets the object containing the help code for this element.
        /// </summary>
        protected GraphicalElementHelper ElementHelper
        {
            get { return this.elementHelper; }
        }

        public PositionManager PositionManager
        {
            get { return positionManager; }
        }

        public TextManager TextManager
        {
            get { return textManager; }
        }

        #endregion

        #region Constructors

        public GenericAsset(ResourceAsset resourceAsset, bool init)
        {
            if (resourceAsset == null)
                throw new ArgumentNullException("resourceAsset", "Resource can't be null");

            this.resourceAsset = resourceAsset;

            if (init) this.Init();
        }

        public GenericAsset(ResourceAsset resourceAsset)
            : this(resourceAsset, true)
        {
        }

        #endregion

        #region Abstract & Virtual Methods

        /// <summary>
        /// Initializatoin code for asset components. 
        /// Creates element helper and asset managers.
        /// </summary>
        protected virtual void Init()
        {
            this.elementHelper = this.CreateElementHelper();
            this.CreateManagers();
        }

        /// <summary>
        /// Creates a new helper for this element.
        /// </summary>
        /// <returns>the GraphicalElementHelper with the help code.</returns>
        protected abstract GraphicalElementHelper CreateElementHelper();

        /// <summary>
        /// Creates all the necessary character managers and adds them to 
        /// the managers list so that they can be updated.
        /// </summary>
        protected virtual void CreateManagers()
        {
            this.positionManager = this.CreatePositionManager();
            if (this.positionManager != null) this.managers.Add(this.positionManager);
        }

        protected abstract PositionManager CreatePositionManager();
        protected abstract TextManager CreateTextManager();

        public virtual void VerifyDestruction()
        {
            this.elementHelper.VerifyDestruction();
        }

        public virtual void ClearNonCarrierSpots()
        {
            foreach (GenericSpot spot in this.AllSpots)
                if (!spot.IsCarrier) spot.Reset();
        }

        /// <summary>
        /// Updates all the asset managers.
        /// </summary>
        /// <param name="millisSinceLastUpdate">the time ocurred since the last 
        /// Update call.</param>
        public virtual void Update(float millisSinceLastUpdate)
        {
            this.VerifyDestruction();

            foreach (AssetManager manager in this.managers)
                if (manager != null) manager.Update(millisSinceLastUpdate);
        }

        /// <summary>
        /// Creates all the spots associated with the asset, according to the 
        /// information on the element's ResourceAsset, and sets its initial parameters.
        /// </summary>
        protected abstract void CreateSpots();

        /// <summary>
        /// Resets the element by setting its visibility and destroyed attributes
        /// to false. If the spots associated with the spot weren't already created, 
        /// creates them, otherwise resets them.
        /// </summary>
        public virtual void Reset()
        {
            this.elementHelper.Reset();

            if (!this.spotsCreated)
            {
                //creates new spots
                this.CreateSpots();
                this.spotsCreated = true;
            }
            else
            {
                //resets existent spots
                foreach (GenericSpot spot in this.AllSpots) spot.Reset();
            }
        }

        /// <summary>
        /// Destroys all spots associated with the asset and sets its destroyed 
        /// attribute to true.
        /// </summary>
        public virtual void Destroy()
        {
            if (this.Destroyed) return;

            //destroys all spots
            foreach (GenericSpot spot in this.AllSpots) spot.Destroy();

            this.cameraSpots.Clear();
            this.waypoints.Clear();
            this.itemSpots.Clear();

            this.spotsCreated = false;

            //disposes all associated managers
            foreach (AssetManager manager in this.managers)
                if (manager != null) manager.Dispose();

            //this.elementHelper.Destroy();
        }

        public virtual void ChangePosition(Position newPosition, float transitionTime)
        {
            if (this.positionManager == null) return;
            this.positionManager.ChangePosition(newPosition, transitionTime);
        }

        #endregion
    }
}