/* ****************************************************
 * Name: ISpeakingCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Management.Character;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// Speaking characters can speak, i.e, reproduce some sounds and animations to 
    /// express some utterance.If a callback is associated, speak actions can fail if 
    /// the character can't produced a sound for the desired utterance, succeed if they 
    /// can do it, or have some triggers that will fire in the middle of the speak 
    /// animations.
    /// </summary>
    public interface ISpeakingCharacter : IAnimatableCharacter
    {
        /// <summary>
        /// Gets the speech manager associated with this character.
        /// </summary>
        SpeechManager SpeechManager { get; }

        /// <summary>
        /// Gets the number of seconds of the speech being performed by the character.
        /// </summary>
        float CurrentSpeechLenght { get; }

        /// <summary>
        /// Gets the number of seconds of speech left to be performed by the character.
        /// </summary>
        float CurrentSpeechTimeLeft { get; }

        /// <summary>
        /// Makes the character say the given utterance. In derived classes, the 
        /// character can make some animations and a sound system can reproduce some
        /// sounds while the character is speaking. 
        /// </summary>
        /// <param name="callback">the callback to receive the speaking events from:
        /// Succeeded when the speak animation finishes, Failed if the character can't
        /// express the given utterance or there is no action defined with the given 
        /// name, ActionTrigger when pre-defined animation times are reached.
        /// </param>
        /// <param name="utterance">the utterance for the character to say.</param>
        void SaySpeech(IActionCallback callback, string utterance);

        /// <summary>
        /// Stops any speech that the character is currently saying.
        /// </summary>
        void StopTalking();

        /// <summary>
        /// Displays the given text on the character's subtitle.
        /// </summary>
        /// <param name="text">the text to display on the subtitle.</param>
        void ShowSubtitles(string text);

        /// <summary>
        /// Hides the current text being displayed on the character's subtitle.
        /// </summary>
        void HideSubtitles();
    }
}