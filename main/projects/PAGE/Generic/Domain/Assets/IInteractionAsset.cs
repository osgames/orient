/* ****************************************************
 * Name: IInteractionAsset.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/12 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Domain.Collections;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Management;
using PAGE.Generic.Math;
using PAGE.Generic.Resources.Assets;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// Interaction assets have a set of camera spots so that cameras can follow 
    /// the assets around, waypoints indicating special navigation positions around them
    /// and item places where items can be placed.
    /// </summary>
    public interface IInteractionAsset : IGraphicalElement, IUpdatable
    {
        /// <summary>
        /// Gets the resource information about the asset.
        /// </summary>
        ResourceAsset ResourceAsset { get; }

        /// <summary>
        /// Gets all the waypoints associated with the asset.
        /// </summary>
        GenericSpotList<GenericWaypoint> Waypoints { get;}

        /// <summary>
        /// Gets all the camera spots associated with the asset.
        /// </summary>
        GenericSpotList<GenericCameraSpot> CameraSpots { get;}

        /// <summary>
        /// Gets all the item spots associated with the asset.
        /// </summary>
        GenericSpotList<GenericItemSpot> ItemSpots { get;}

        /// <summary>
        /// Gets a list with all the spots associated with the asset, which will 
        /// depend on the derived asset class.
        /// </summary>
        GenericSpotList<GenericSpot> AllSpots { get;}

        /// <summary>
        /// Clears all the non-carrier spots associated with the element.
        /// </summary>
        void ClearNonCarrierSpots();

        /// <summary>
        /// Gets the action manager associated with this character.
        /// </summary>
        PositionManager PositionManager { get; }

        /// <summary>
        /// Gets the subtitle manager associated with this character.
        /// </summary>
        TextManager TextManager { get; }

        /// <summary>
        /// Changes the current position of the asset through out the given time.
        /// </summary>
        /// <param name="newPosition">the new position to place the asset.</param>
        /// <param name="transitionTime">the time in seconds to perform the transition
        /// between positions.</param>
        void ChangePosition(Position newPosition, float transitionTime);
    }
}