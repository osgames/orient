/* ****************************************************
 * Name: GenericCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using PAGE.Generic.Management.Character;
using PAGE.Generic.Resources.Assets;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// A character represents an entity of the system. Characters can perform actions 
    /// and interact with world items.
    /// </summary>
    [Serializable]
    public abstract class GenericCharacter : DynamicAsset, ICharacter
    {
        #region Fields

        protected ActionTriggersManager actionTriggersManager;

        #endregion

        #region Properties

        public ActionTriggersManager ActionTriggersManager
        {
            get { return actionTriggersManager; }
        }

        public new ResourceCharacter ResourceAsset
        {
            get { return this.resourceAsset as ResourceCharacter; }
        }

        ResourceAsset IInteractionAsset.ResourceAsset
        {
            get { return this.resourceAsset; }
        }

        #endregion

        #region Constructors

        protected GenericCharacter(
            ResourceCharacter resource, bool init)
            : base(resource, false)
        {
            if (init) this.Init();
        }

        public GenericCharacter(
            ResourceCharacter resource)
            : this(resource, true)
        {
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Creates all the necessary character managers and adds them to 
        /// the managers list so that they can be updated.
        /// </summary>
        protected override void CreateManagers()
        {
            base.CreateManagers();

            this.actionTriggersManager = this.CreateActionTriggersManager();
            if (this.actionTriggersManager != null) this.managers.Add(this.actionTriggersManager);
        }

        protected virtual ActionTriggersManager CreateActionTriggersManager()
        {
            return new ActionTriggersManager(this);
        }

        #endregion
    }
}