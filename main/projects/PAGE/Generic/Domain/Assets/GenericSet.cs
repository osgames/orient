/* ****************************************************
 * Name: GenericSet.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Collections.Generic;
using PAGE.Generic.Domain.Collections;
using PAGE.Generic.Math;
using PAGE.Generic.Navigation;
using PAGE.Generic.Resources.Assets;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// A set represents a scenario where items are placed and characters can omve and 
    /// interact in it. It has a set of waypoints used for character navigation purposes
    /// and a set of camera spot to be used bu camera algorithms to capture the system's
    /// current state.
    /// </summary>
    public abstract class GenericSet : GenericAsset
    {
        #region Fields

        protected GenericAssetList<DynamicAsset> items = new GenericAssetList<DynamicAsset>();

        #endregion

        #region Properties

        public new ResourceSet ResourceAsset
        {
            get { return (ResourceSet) this.resourceAsset; }
        }

        /// <summary>
        /// Gets a list of all the items associated with the set.
        /// </summary>
        public GenericAssetList<DynamicAsset> Items
        {
            get { return items; }
        }

        #endregion

        #region Constructors

        public GenericSet(ResourceSet resource, bool init)
            : base(resource, false)
        {
            if (init) this.Init();
        }

        public GenericSet(ResourceSet resource)
            : this(resource, true)
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Calculates the set's navigation paths and creates the
        /// vertex list to be used by the navigation algorithms.
        /// </summary>
        public virtual void CreateNavigationPaths()
        {
            //calculate navigation paths
            GraphGenerator pathGenerator = new GraphGenerator(this);
            pathGenerator.GeneratePaths(true);
            this.ResourceAsset.VertexList = pathGenerator.Vertexs;
        }

        /// <summary>
        /// Tests if there is an open path between the given points on the scene 
        /// contained in the scene manager, ignoring the given elements as obstacles.
        /// </summary>
        /// <param name="source">the source point</param>
        /// <param name="target">the target point</param>
        /// <param name="ignoreElements">the elements to be ignored in the open path 
        /// calculation process</param>
        /// <param name="allObjectsCount">if all objects in the scene countas obstacles 
        /// or only static set objects count.</param>
        /// <returns>true if there is an open path between the points, false otherwise</returns>
        public abstract bool OpenPath(
            Position source, Position target, List<IGraphicalElement> ignoreElements,
            bool allObjectsCount);


        /// <summary>
        /// Tests if there is an open path between the given points on the scene 
        /// contained in the scene manager.
        /// </summary>
        /// <param name="source">the source point</param>
        /// <param name="target">the target point</param>
        /// <param name="allObjectsCount">if all objects in the scene count as obstacles or 
        /// only static set objects count.</param>
        /// <returns>true if there is an open path between the points,false otherwise
        /// </returns>
        public bool OpenPath(Position source, Position target, bool allObjectsCount)
        {
            return this.OpenPath(
                source, target, new List<IGraphicalElement>(), allObjectsCount);
        }

        #endregion
    }
}