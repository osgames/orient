/* ****************************************************
 * Name: IWalkingCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/04
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Management.Character;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// Walking movable characters can move to some spots by walking or running to them.
    /// </summary>
    public interface IWalkingCharacter : IMovableCharacter
    {
        /// <summary>
        /// Gets the navigation manager for the walking character.
        /// </summary>
        new WalkingNavigationManager NavigationManager { get;}
    }
}
