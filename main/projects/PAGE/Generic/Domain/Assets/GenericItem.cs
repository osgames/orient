/* ****************************************************
 * Name: GenericItem.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Math;
using PAGE.Generic.Resources.Assets;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// An item represents an entity of the system. Items can be placed in spots 
    /// and have other interaction spots that "guide" characters to perform
    /// actions over them.
    /// </summary>
    public abstract class GenericItem : DynamicAsset
    {
        #region Properties

        public new ResourceItem ResourceAsset
        {
            get { return (ResourceItem)this.resourceAsset; }
        }

        /// <summary>
        /// Gets the total height of the character, i.e., the distance between its
        /// surface and top.
        /// </summary>
        public abstract Distance Height { get; }

        #endregion

        #region Constructors

        public GenericItem(ResourceItem resource, bool init)
            : base(resource, false)
        {
            if (init) this.Init();
        }

        public GenericItem(ResourceItem resource)
            : this(resource, true)
        {
        }

        #endregion
    }
}