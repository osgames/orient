/* ****************************************************
 * Name: IMovableCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Management.Character;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// Movable characters can move to some spots. If a callback is associated, 
    /// move actions can fail if the character can't reach its destiny, succeed when 
    /// they reach it, or have some triggers that will fire in the middle of the 
    /// animations.
    /// </summary>
    public interface IMovableCharacter : IAnimatableCharacter
    {
        /// <summary>
        /// Gets or sets the element's current waypoint. When overriden by a derived 
        /// class, changes the element position in the world.
        /// </summary>
        new GenericWaypoint CurrentSpot { get; set; }

        /// <summary>
        /// Gets the navigation manager associated with this character.
        /// </summary>
        NavigationManager NavigationManager { get;}

        /// <summary>
        /// Gets or sets the set that is currently associated with the character and
        /// allows the navigation in it.
        /// </summary>
        GenericSet Set { get; set;}

        /// <summary>
        /// Moves the character to a spot containing the given tag and that belongs to
        /// the given asset. The movement is executed according the given action info.
        /// </summary>
        /// <param name="callback">the callback to receive the action events from:
        /// Succeeded if the character reaches its destiny, Failed if at any 
        /// time the character can't reach the desired destiny, ActionTrigger when 
        /// pre-defined animation times are reached.
        /// </param>
        /// <param name="actionID">the name of the action with the movement and 
        /// animation info.</param>
        /// <param name="targetSpotContainer">the asset that contains the spot to move 
        /// the character to.</param>
        /// <param name="spotTag">the tag of the spot to move to.</param>
        void Move(IActionCallback callback, string actionID,
                  IInteractionAsset targetSpotContainer, string spotTag);

        /// <summary>
        /// Moves the character to a spot that belongs to the given asset, independently
        /// of its tags. The movement is executed according the given action info.
        /// </summary>
        /// <param name="callback">the callback to receive the action events from:
        /// Succeeded if the character reaches its destiny, Failed if at any 
        /// time the character can't reach the desired destiny, ActionTrigger when 
        /// pre-defined animation times are reached.
        /// </param>
        /// <param name="actionID">the name of the action with the movement and 
        /// animation info.</param>
        /// <param name="targetSpotContainer">the asset that contains the spot to move 
        /// the character to.</param>
        void Move(IActionCallback callback, string actionID,
                  IInteractionAsset targetSpotContainer);

        /// <summary>
        /// Moves the character to a spot that belongs to the given asset, independently
        /// of its tags, and which position is the farthest from the given element. 
        /// The movement is executed according the given action info.
        /// </summary>
        /// <param name="callback">the callback to receive the action events from:
        /// Succeeded if the character reaches its destiny, Failed if at any 
        /// time the character can't reach the desired destiny, ActionTrigger when 
        /// pre-defined animation times are reached.
        /// </param>
        /// <param name="actionID">the name of the action with the movement and 
        /// animation info.</param>
        /// <param name="targetSpotContainer">the asset that contains the spot to move 
        /// the character to.</param>
        /// <param name="fromElement">the element to move away from.</param>
        void MoveAway(IActionCallback callback, string actionID,
                      IInteractionAsset targetSpotContainer, IGraphicalElement fromElement);

        /// <summary>
        /// Moves the character to a spot that belongs to the given asset, independently
        /// of its tags, and which position is the farthest from the given element. 
        /// The movement is executed according the given action info.
        /// </summary>
        /// <param name="callback">the callback to receive the action events from:
        /// Succeeded if the character reaches its destiny, Failed if at any 
        /// time the character can't reach the desired destiny, ActionTrigger when 
        /// pre-defined animation times are reached.
        /// </param>
        /// <param name="actionID">the name of the action with the movement and 
        /// animation info.</param>
        /// <param name="targetSpotContainer">the asset that contains the spot to move 
        /// the character to.</param>
        void MoveAway(IActionCallback callback, string actionID,
                      IInteractionAsset targetSpotContainer);

        /// <summary>
        /// Stops the character's current movement.
        /// </summary>
        void StopMoving();
    }
}
