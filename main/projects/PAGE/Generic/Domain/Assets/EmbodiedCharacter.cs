/* ****************************************************
 * Name: EmbodiedCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using PAGE.Generic.Management.Character;
using PAGE.Generic.Math;
using PAGE.Generic.Resources.Assets;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// Embodied characters can perform animations and have a certain set associated.
    /// </summary>
    public abstract class EmbodiedCharacter :
        GenericCharacter, IAnimatableCharacter, ITexturedCharacter
    {
        #region Fields

        //the set to which the character is connected
        protected GenericSet set;

        //managers
        protected AnimationManager animManager;
        protected TextureManager textureManager;

        #endregion

        #region Properties

        public virtual GenericSet Set
        {
            get
            {
                this.VerifyDestruction();
                return this.set;
            }
            set
            {
                this.VerifyDestruction();
                this.set = value;
            }
        }

        public AnimationManager AnimationManager
        {
            get { return animManager; }
        }

        public TextureManager TextureManager
        {
            get { return textureManager; }
        }

        public abstract float CurrentAnimationLenght { get; }

        public abstract float CurrentAnimationTimeLeft { get; }

        /// <summary>
        /// Gets the total height of the character, i.e., the distance between its
        /// surface and top.
        /// </summary>
        public abstract Distance Height { get; }

        #endregion

        #region Constructors

        protected EmbodiedCharacter(
            ResourceCharacter resource, bool init)
            : base(resource, false)
        {
            if (resource == null)
                throw new ArgumentNullException("resource", "resource can't be null");

            if (init) this.Init();
        }

        public EmbodiedCharacter(ResourceCharacter resource)
            : this(resource, true)
        {
        }

        #endregion

        #region Public Methods

        #region Control

        public override void Reset()
        {
            this.set = null;
            base.Reset();
        }

        #endregion

        #region Actions

        public virtual void Animate(IActionCallback callback, string actionID)
        {
            this.Animate(callback, actionID, false, true);
        }

        public virtual void Animate(IActionCallback callback, string actionID, bool randomStartTime)
        {
            this.Animate(callback, actionID, randomStartTime, true);
        }

        public virtual void Animate(IActionCallback callback, string actionID, bool randomStartTime, bool loop)
        {
            this.VerifyDestruction();

            this.AnimationManager.ExecuteAction(callback, actionID, randomStartTime, loop);
            this.ActionTriggersManager.ExecuteAction(callback, actionID);
        }

        public virtual void StopAnimating()
        {
            this.VerifyDestruction();
            if (this.AnimationManager != null) this.AnimationManager.Stop();
        }
        #endregion

        #region Immediate Actions

        public abstract void ChangeTexture(string bodyPartName, string textureName);
        public abstract void ChangeTexture(int bodyPartIndex, string textureName);

        #endregion

        #endregion

        #region Protected Methods

        protected override void CreateManagers()
        {
            base.CreateManagers();

            //creates managers
            this.animManager = this.CreateAnimationManager();
            if (this.animManager != null) this.managers.Add(this.animManager);
            this.textureManager = this.CreateTextureManager();
            if (this.textureManager != null) this.managers.Add(this.textureManager);
        }

        protected abstract AnimationManager CreateAnimationManager();
        protected abstract TextureManager CreateTextureManager();

        #endregion
    }
}