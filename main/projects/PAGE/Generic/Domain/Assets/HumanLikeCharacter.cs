/* ****************************************************
 * Name: HumanLikeCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/30
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Domain.Collections;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Management.Character;
using PAGE.Generic.Resources.Assets;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// Represents a generic human-like character that can move around in a set
    /// with pre-determined waypoints.
    /// </summary>
    public abstract class HumanLikeCharacter :
        EmbodiedCharacter, ISpeakingCharacter, IWalkingCharacter
    {
        #region Fields

        protected WalkingNavigationManager navigationManager;
        protected SpeechManager speechManager;

        #endregion

        #region Properties

        public override IGenericSpot CurrentSpot
        {
            get { return base.CurrentSpot; }
            set
            {
                if (this.positionManager != null) this.positionManager.Stop();
                base.CurrentSpot = value;
            }
        }

        GenericWaypoint IMovableCharacter.CurrentSpot
        {
            get { return this.CurrentSpot as GenericWaypoint; }
            set { this.CurrentSpot = value; }
        }

        public new ResourceHumanLikeCharacter ResourceAsset
        {
            get { return (ResourceHumanLikeCharacter) this.resourceAsset; }
        }

        public abstract float CurrentSpeechLenght { get; }

        public abstract float CurrentSpeechTimeLeft { get; }

        /// <summary>
        /// Gets or sets the set that is currently associated with the character and 
        /// destroys the navigation manager if the set is null.
        /// </summary>
        public override GenericSet Set
        {
            get { return base.Set; }
            set
            {
                base.Set = value;

                this.managers.Remove(this.navigationManager);
                this.navigationManager = null;
            }
        }

        public override bool Visible
        {
            get { return base.Visible; }
            set
            {
                //also hides the subtitles
                base.Visible = value;
                if (!value) this.textManager.Visible = false;
            }
        }

        public SpeechManager SpeechManager
        {
            get { return speechManager; }
        }

        public WalkingNavigationManager NavigationManager
        {
            get { return navigationManager; }
        }

        NavigationManager IMovableCharacter.NavigationManager
        {
            get { return navigationManager; }
        }

        #endregion

        #region Constructors

        protected HumanLikeCharacter(
            ResourceCharacter resource, bool init)
            : base(resource, false)
        {
            if (init) this.Init();
        }

        public HumanLikeCharacter(ResourceCharacter resource)
            : this(resource, true)
        {
        }

        #endregion

        #region Actions

        public override void Animate(IActionCallback callback, string actionID)
        {
            this.StopMoving();
            base.Animate(callback, actionID);
        }

        public override void Animate(IActionCallback callback, string actionID, bool randomStartTime)
        {
            this.StopMoving();
            base.Animate(callback, actionID, randomStartTime);
        }

        public virtual void Move(IActionCallback callback, string actionID,
                                 IInteractionAsset targetSpotContainer)
        {
            this.Move(callback, actionID, targetSpotContainer, "");
        }

        public virtual void Move(IActionCallback callback, string actionID,
                                 IInteractionAsset targetSpotContainer, string spotTag)
        {
            this.VerifyDestruction();

            //no navigation manager, can't continue, failure
            if (this.navigationManager == null)
            {
                if (callback != null) callback.SendFailure();
                return;
            }

            //activates the managers involved in the action
            this.AnimationManager.ExecuteAction(null, actionID);
            this.NavigationManager.ExecuteAction(
                callback, actionID, targetSpotContainer, spotTag);
            this.ActionTriggersManager.ExecuteAction(callback, actionID);
        }

        public virtual void MoveAway(IActionCallback callback, string actionID,
                                     IInteractionAsset targetSpotContainer)
        {
            this.MoveAway(callback, actionID, targetSpotContainer, this);
        }

        public virtual void MoveAway(IActionCallback callback, string actionID,
                                     IInteractionAsset targetSpotContainer, IGraphicalElement fromElement)
        {
            GenericSpotList<IGenericSpot> targetWaypoints =
                new GenericSpotList<IGenericSpot>();

            foreach (GenericWaypoint spot in targetSpotContainer.Waypoints)
                targetWaypoints.Add(spot);

            GenericWaypoint farthestWaypoint = (GenericWaypoint)
                                               Spots.Util.FindFarthestEmptySpot(fromElement.AbsolutePosition,
                                                                                targetWaypoints);

            this.Move(callback, actionID, targetSpotContainer,
                      farthestWaypoint == null ? "" : farthestWaypoint.ResourceSpot.IdToken);
        }

        public virtual void SaySpeech(IActionCallback callback, string utterance)
        {
            this.VerifyDestruction();
            this.SpeechManager.ExecuteAction(callback, "", utterance);
        }

        public virtual void StopMoving()
        {
            this.VerifyDestruction();
            if (this.navigationManager != null) this.navigationManager.Stop();
        }

        public virtual void StopTalking()
        {
            this.VerifyDestruction();
            if (this.SpeechManager != null) this.SpeechManager.Stop();
        }

        #region Immediate Actions

        public virtual void ShowSubtitles(string text)
        {
            this.VerifyDestruction();
            if (this.textManager == null) return;
            this.textManager.MainText.Visible = true;
            this.textManager.MainText.Text = text;
        }

        public virtual void HideSubtitles()
        {
            this.VerifyDestruction();
            if (this.textManager != null) 
                this.textManager.MainText.Visible = false;
        }

        #endregion

        #endregion

        #region Protected Methods

        protected override void CreateManagers()
        {
            base.CreateManagers();

            //creates managers
            this.navigationManager = this.CreateNavigationManager();
            if (this.navigationManager != null) this.managers.Add(this.navigationManager);
            this.textManager = this.CreateTextManager();
            if (this.textManager != null) this.managers.Add(this.textManager);
            this.speechManager = this.CreateSpeechManager();
            if (this.speechManager != null) this.managers.Add(this.speechManager);
        }

        protected abstract WalkingNavigationManager CreateNavigationManager();
        protected abstract SpeechManager CreateSpeechManager();

        #endregion
    }
}