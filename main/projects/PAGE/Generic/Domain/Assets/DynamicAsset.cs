/* ****************************************************
 * Name: DynamicAsset.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/25
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Resources.Assets;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// A dynamic asset is an asset that can change it's position and 
    /// properties overtime.
    /// </summary>
    public abstract class DynamicAsset : PlaceableAsset
    {
        #region Constructors

        public DynamicAsset(ResourceAsset resource, bool init)
            : base(resource, false)
        {
            if (init) this.Init();
        }

        public DynamicAsset(ResourceAsset resource)
            : this(resource, true)
        {
        }

        #endregion
    }
}