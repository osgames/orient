/* ****************************************************
 * Name: IPlaceableAsset.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/08/27 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// Represents an asset that is movable, i.e., that can be placed in some
    /// spot at a given time.
    /// </summary>
    public interface IPlaceableAsset : IPlaceableElement, IInteractionAsset
    {
        
    }
}