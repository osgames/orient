/* ****************************************************
 * Name: PlaceableAsset.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/31
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Domain.Collections;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Math;
using PAGE.Generic.Resources.Assets;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// Represents an asset that can be placed in specific spots.
    /// </summary>
    public abstract class PlaceableAsset : GenericAsset, IPlaceableAsset
    {
        #region Properties

        protected new PlaceableElementHelper ElementHelper
        {
            get { return base.ElementHelper as PlaceableElementHelper; }
        }

        public virtual IGenericSpot CurrentSpot
        {
            get { return this.ElementHelper.CurrentSpot; }
            set { this.ElementHelper.CurrentSpot = value; }
        }

        public override Position AbsolutePosition
        {
            get { return base.AbsolutePosition; }
            set
            {
                this.Unplace();
                base.AbsolutePosition = value;
            }
        }

        #endregion

        #region Contructors

        public PlaceableAsset(ResourceAsset resource, bool init)
            : base(resource, false)
        {
            if(init) this.Init();
        }

        public PlaceableAsset(ResourceAsset resource)
            : this(resource, true)
        {
        }

        #endregion

        #region Public Methods

        public virtual GenericSpotList<IGenericSpot> GetPlaceableSpots(IInteractionAsset targetSpotContainer)
        {
            return this.ElementHelper.GetPlaceableSpots(targetSpotContainer);
        }

        public virtual void Place(
            IActionCallback callback, IInteractionAsset targetSpotContainer, string targetID, bool inEmptySpot)
        {
            this.ElementHelper.Place(callback, targetSpotContainer, targetID, inEmptySpot);
        }

        public virtual void Place(
            IActionCallback callback, IInteractionAsset targetSpotContainer, string targetID)
        {
            this.ElementHelper.Place(callback, targetSpotContainer, targetID, true);
        }

        public virtual void Place(IActionCallback callback, IInteractionAsset targetSpotContainer, bool inEmptySpot)
        {
            this.ElementHelper.Place(callback, targetSpotContainer, "", inEmptySpot);
        }

        public virtual void Place(IActionCallback callback, IInteractionAsset targetSpotContainer)
        {
            this.ElementHelper.Place(callback, targetSpotContainer, "", true);
        }

        public virtual void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer, 
            string targetID, bool inEmptySpot)
        {
            this.ElementHelper.PlaceRandomly(callback, targetSpotContainer, targetID, inEmptySpot);
        }

        public virtual void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer, string targetID)
        {
            this.ElementHelper.PlaceRandomly(callback, targetSpotContainer, targetID, true);
        }

        public virtual void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer, bool inEmptySpot)
        {
            this.ElementHelper.PlaceRandomly(callback, targetSpotContainer, "", inEmptySpot);
        }

        public void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer)
        {
            this.ElementHelper.PlaceRandomly(callback, targetSpotContainer, true);
        }

        public virtual void Unplace()
        {
            this.ElementHelper.Unplace();
        }

        public virtual void TurnTo(IGraphicalElement toElement)
        {
            this.ElementHelper.TurnTo(toElement);
            this.ClearNonCarrierSpots();
        }
       
        /// <summary>
        /// Added 16/09/08 by Michael: I needed this function to make the character turn towards the User's
        /// position (since the user does not have an IGraphicalElement)
        /// </summary>
        /// <param name="toPosition"> the position to turn to</param>
        public virtual void TurnTo(Position toPosition)
        {
            this.ElementHelper.TurnTo(toPosition);
            this.ClearNonCarrierSpots();
        }

        #endregion

        #region Override Methods

        public override void Reset()
        {
            base.Reset();
            this.Unplace();         //takes out asset from current spot
        }

        public override void Destroy()
        {
            this.Unplace();         //takes out asset from current spot
            base.Destroy();
        }

        #endregion
    }
}