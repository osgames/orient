/* ****************************************************
 * Name: ICharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/03 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Management.Character;
using PAGE.Generic.Resources.Assets;

namespace PAGE.Generic.Domain.Assets
{
    /// <summary>
    /// A character represents an entity of the system. Characters can perform actions 
    /// and interact with world items.
    /// </summary>
    public interface ICharacter : IPlaceableAsset
    {
        /// <summary>
        /// Gets the action manager associated with this character.
        /// </summary>
        ActionTriggersManager ActionTriggersManager { get; }

        /// <summary>
        /// Gets the resource information about the character.
        /// </summary>
        new ResourceCharacter ResourceAsset { get;}
    }
}
