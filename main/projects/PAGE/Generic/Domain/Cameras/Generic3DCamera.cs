/* ****************************************************
 * Name: Generic3DCamera.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Generic.Math;

namespace PAGE.Generic.Domain.Cameras
{
    /// <summary>
    /// Represents a camera with fucntions suitable to use in 3D graphical realizers.
    /// </summary>
    public abstract class Generic3DCamera : GenericCamera
    {
        #region Virtual and Abstract Methods

        public abstract void Move(Position pos);
        public abstract void MoveRelative(Position relativePos);
        public abstract void Roll(double angleDeg);
        public abstract void Yaw(double angleDeg);
        public abstract void Pitch(double angleDeg);

        #endregion
    }
}
