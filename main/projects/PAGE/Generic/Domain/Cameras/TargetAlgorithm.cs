/* ****************************************************
 * Name: TargetAlgorithm.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/04/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Generic.Math;

namespace PAGE.Generic.Domain.Cameras
{
    /// <summary>
    /// Represents a camera algorithm that is able to track a certain element,
    /// focusing the element overtime.
    /// </summary>
    public class TargetAlgorithm : CameraAlgorithm
    {
        #region Fields

        //target
        protected IGraphicalElement target = null;
        protected Position targetOffset = null;

        //spots tags parameters
        protected string faceCameraTag = "face-camera";
        protected string bodyCameraTag = "body-camera";

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating the offset position that the algorithm
        /// must add to the target position when focusing it.
        /// </summary>
        public Position TargetOffset
        {
            get { return targetOffset; }
            set { targetOffset = value; }
        }

        /// <summary>
        /// Gets or sets the graphical element that the camera focus overtime.
        /// </summary>
        public IGraphicalElement Target
        {
            get { return this.target; }
            set { this.target = value; }
        }

        #endregion

        #region Constructors

        public TargetAlgorithm(GenericCamera camera, IGraphicalElement targetElement)
            :base(camera)
        {
            this.target = targetElement;
        }

        public TargetAlgorithm(GenericCamera camera)
            : this(camera, null)
        {
        }

        #endregion

        #region Public Methods

        public override void Update(float millisSinceLastUpdate)
        {
            //follows target
            if (this.Target != null)
            {
                if(this.TargetOffset != null) 
                    this.camera.LookAt(this.target.AbsolutePosition.Add(this.TargetOffset));
                else 
                    this.camera.LookAt(this.target.AbsolutePosition);
            }
        }

        #endregion
    }
}