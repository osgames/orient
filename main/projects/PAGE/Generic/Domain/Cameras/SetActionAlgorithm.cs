/* ****************************************************
 * Name: TargetAlgorithm.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Math;

namespace PAGE.Generic.Domain.Cameras
{
    /// <summary>
    /// Represents a camera algorithm that is able to track a certain element,
    /// focusing the element overtime by choosing the best camera spots that are 
    /// present in some set.
    /// </summary>
    public class SetActionAlgorithm : TargetAlgorithm
    {
        #region Fields

        //set with camera positions
        protected GenericSet set = null;

        //the time the camera is in current position
        protected TimeSpan curCameraPosTime = TimeSpan.Zero;

        //the minimum time between camera position changes
        protected float secsBetweenChange = 3.0f;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the minimum time between camera position changes in set.
        /// </summary>
        public float SecsBetweenCameraChange
        {
            get { return this.secsBetweenChange; }
            set { this.secsBetweenChange = value; }
        }

        /// <summary>
        /// Gets or sets the set that contains the camera spots to be used in 
        /// the algorithm.
        /// </summary>
        public GenericSet Set
        {
            get { return this.set; }
            set
            {
                //sets the camera position in the first spot in the set
                this.set = value;
                if ((this.set != null) && (this.set.CameraSpots.Count > 0) && !this.set.Destroyed)
                {
                    this.camera.CurrentSpot = this.set.CameraSpots[0];
                }
                else
                {
                    this.camera.CurrentSpot = null;
                }
            }
        }

        #endregion

        #region Constructors

        public SetActionAlgorithm(GenericCamera camera, IGraphicalElement targetElement)
            : base(camera, targetElement)
        {
        }

        public SetActionAlgorithm(GenericCamera camera)
            : base(camera)
        {
        }

        #endregion

        #region Public Methods

        public override void Update(float millisSinceLastUpdate)
        {
            //chooses best camera target
            if ((this.Target == null) || (this.set == null))
                return;

            //gets total time since last camera position change
            this.curCameraPosTime += TimeSpan.FromMilliseconds(millisSinceLastUpdate);

            //no need to continue, camera change time has not yet been achieved
            if (this.curCameraPosTime.TotalSeconds < this.secsBetweenChange)
            {
                //looks at the target
                base.Update(millisSinceLastUpdate);
                return;
            }

            //the direction of the target object
            Direction targetDirection = this.Target.AbsoluteDirection.Invert();

            //if there are more camera positions and the time interval exceeded
            if (this.set.CameraSpots.Count > 0)
            {
                //tries to change the camera position
                GenericCameraSpot newPosition = null;
                Angle minAngle = null;

                //calculates distances between the camera and the target
                foreach (GenericCameraSpot camSpot in this.set.CameraSpots)
                {
                    Position camPos = camSpot.AbsolutePosition;
                    Direction camVec = camPos.DirectionTo(this.Target.AbsolutePosition);

                    Angle angle = targetDirection.AngleBetween(camVec);

                    if (((minAngle == null) || (angle.CompareTo(minAngle) < 0))
                        && this.set.OpenPath(camSpot.AbsolutePosition, 
                            this.Target.AbsolutePosition, false))
                    {
                        //gets the minimum angle to the target object
                        minAngle = angle;
                        newPosition = camSpot;
                    }
                }

                if ((newPosition != null) && (newPosition != this.camera.CurrentSpot))
                {
                    //changes camera position
                    this.camera.CurrentSpot = newPosition;
                    this.curCameraPosTime = TimeSpan.Zero;
                }
            }

            //looks at the target
            base.Update(millisSinceLastUpdate);
        }

        #endregion
    }
}