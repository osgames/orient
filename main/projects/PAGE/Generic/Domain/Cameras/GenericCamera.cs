/* ****************************************************
 * Name: GenericCamera.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Collections;
using PAGE.Generic.Domain.Helpers;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Math;

namespace PAGE.Generic.Domain.Cameras
{
    /// <summary>
    /// Represents a generic camera that is able to graphicaly represent the state
    /// of the realizer. A camera is a placeable element that can execute a certain
    /// algorithm overtime and that can be placed in defined camera spots.
    /// </summary>
    public abstract class GenericCamera : IPlaceableElement, IUpdatable
    {
        #region Fields

        protected CameraAlgorithm algorithm = null;
        protected PlaceableElementHelper elementHelper = null;

        #endregion

        #region Properties

        public bool Destroyed
        {
            get { return this.elementHelper.Destroyed; }
        }

        public virtual bool Visible
        {
            get { this.VerifyDestruction(); return this.elementHelper.Visible; }
            set { this.VerifyDestruction(); this.elementHelper.Visible = value; }
        }

        public virtual Position AbsolutePosition
        {
            get { this.VerifyDestruction(); return this.elementHelper.AbsolutePosition; }
            set { this.VerifyDestruction(); this.elementHelper.AbsolutePosition = value; }
        }

        public virtual Orientation AbsoluteOrientation
        {
            get { this.VerifyDestruction(); return this.elementHelper.AbsoluteOrientation; }
            set { this.VerifyDestruction(); this.elementHelper.AbsoluteOrientation = value; }
        }

        public virtual Direction AbsoluteDirection
        {
            get { this.VerifyDestruction(); return this.elementHelper.AbsoluteDirection; }
            set { this.VerifyDestruction(); this.elementHelper.AbsoluteDirection = value; }
        }

        /// <summary>
        /// Gets or sets the camera algorithm that manages this camera.
        /// </summary>
        public CameraAlgorithm CameraAlgorithm
        {
            get { return algorithm; }
            set { algorithm = value; }
        }

        /// <summary>
        /// Gets the object containing the help code for this element.
        /// </summary>
        protected PlaceableElementHelper ElementHelper
        {
            get { return this.elementHelper; }
        }

        IGenericSpot IPlaceableElement.CurrentSpot
        {
            get { return this.ElementHelper.CurrentSpot; }
            set { this.ElementHelper.CurrentSpot = value; }
        }

        public GenericCameraSpot CurrentSpot
        {
            get { return ((IPlaceableElement)this).CurrentSpot as GenericCameraSpot; }
            set { ((IPlaceableElement)this).CurrentSpot = value; }
        }

        #endregion

        #region Constructors

        public GenericCamera()
        {
            //default algorithm
            this.algorithm = new CameraAlgorithm(this);
            this.elementHelper = this.CreateElementHelper();
        }

        #endregion

        #region Virtual and Abstract Methods

        /// <summary>
        /// Creates a new helper for this element.
        /// </summary>
        /// <returns>the PlaceableElementHelper with the help code.</returns>
        protected abstract PlaceableElementHelper CreateElementHelper();

        public virtual void VerifyDestruction()
        {
            this.ElementHelper.VerifyDestruction();
        }

        public virtual void Place(
            IActionCallback callback, IInteractionAsset targetSpotContainer, string targetID, bool inEmptySpot)
        {
            this.ElementHelper.Place(callback, targetSpotContainer, targetID, inEmptySpot);
        }

        public virtual void Place(
            IActionCallback callback, IInteractionAsset targetSpotContainer, string targetID)
        {
            this.ElementHelper.Place(callback, targetSpotContainer, targetID, true);
        }

        public virtual void Place(IActionCallback callback, IInteractionAsset targetSpotContainer, bool inEmptySpot)
        {
            this.ElementHelper.Place(callback, targetSpotContainer, "", inEmptySpot);
        }

        public virtual void Place(IActionCallback callback, IInteractionAsset targetSpotContainer)
        {
            this.ElementHelper.Place(callback, targetSpotContainer, "", true);
        }

        public virtual void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer,
            string targetID, bool inEmptySpot)
        {
            this.ElementHelper.PlaceRandomly(callback, targetSpotContainer, targetID, inEmptySpot);
        }

        public virtual void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer, string targetID)
        {
            this.ElementHelper.PlaceRandomly(callback, targetSpotContainer, targetID, true);
        }

        public virtual void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer, bool inEmptySpot)
        {
            this.ElementHelper.PlaceRandomly(callback, targetSpotContainer, "", inEmptySpot);
        }

        public void PlaceRandomly(
            IActionCallback callback, IInteractionAsset targetSpotContainer)
        {
            this.ElementHelper.PlaceRandomly(callback, targetSpotContainer, true);
        }

        public virtual void Unplace()
        {
            this.ElementHelper.Unplace();
        }

        public virtual void TurnTo(IGraphicalElement toElement)
        {
            this.ElementHelper.TurnTo(toElement);
        }

        public virtual GenericSpotList<IGenericSpot> GetPlaceableSpots(IInteractionAsset targetSpotContainer)
        {
            GenericSpotList<IGenericSpot> placeableSpots = new GenericSpotList<IGenericSpot>();
            foreach (GenericCameraSpot spot in targetSpotContainer.CameraSpots)
                placeableSpots.Add(spot);
            return placeableSpots;
        }

        public virtual void Update(float millisSinceLastUpdate)
        {
            this.VerifyDestruction();

            //updates the algorithm
            if(this.algorithm != null) this.algorithm.Update(millisSinceLastUpdate);
        }

        public virtual void Destroy()
        {
            this.ElementHelper.Destroy();
        }

        public virtual void Reset()
        {
            this.ElementHelper.Destroy();
        }

        public abstract void Zoom(float delta);
        public abstract void LookAt(Position at);

        #endregion
    }
}
