/* ****************************************************
 * Name: CameraAlgorithm.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

namespace PAGE.Generic.Domain.Cameras
{
    /// <summary>
    /// Represents an algorithm to manage a certain camera.
    /// </summary>
    public class CameraAlgorithm : IUpdatable
    {
        protected GenericCamera camera = null;

        /// <summary>
        /// Gets the camera that this algorithm manages.
        /// </summary>
        public GenericCamera Camera { get { return this.camera; } }

        public CameraAlgorithm(GenericCamera camera)
        {
            this.camera = camera;
        }

        public virtual void Update(float millisSinceLastUpdate)
        {
            //do nothing, default camera behavior...
        }
    }
}
