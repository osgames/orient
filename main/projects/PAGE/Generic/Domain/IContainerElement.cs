/* ****************************************************
 * Name: IContainerElement.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/01 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Collections.Generic;

namespace PAGE.Generic.Domain
{
    /// <summary>
    /// Represents an element that can attach other elements to it. If the element 
    /// is also "carrier", the attached elements will also change their position 
    /// and orientation according to the element's graphical properties.
    /// </summary>
    public interface IContainerElement : IGraphicalElement
    {
        /// <summary>
        /// Gets a list of the elements that are attached to this element.
        /// </summary>
        /// <remarks>Adding/removing element from this list won't attach/detach
        /// those elements from this element.</remarks>
        List<IPlaceableElement> ContainedElements { get; }

        /// <summary>
        /// Gets the parent element of this element, i.e., the element that contains this 
        /// element.
        /// </summary>
        IGraphicalElement ParentElement { get; }

        /// <summary>
        /// Gets a value indicating wether this element is empty, i.e., if there are no 
        /// assets attached to it.
        /// </summary>
        bool IsEmpty { get; }

        /// <summary>
        /// Gets or a value indicating if some attached assets will "follow" this element
        /// when its moved or rotated.
        /// </summary>
        bool IsCarrier { get; }

        /// <summary>
        /// Adds the given graphical element to this element. The element's position 
        /// and orientation will change overtime as this element also changes.
        /// </summary>
        /// <param name="element">the element to attach to this element.</param>
        void AddContainedElement(IPlaceableElement element);

        /// <summary>
        /// Detaches the given graphical element from this element. The element's position 
        /// and orientation will no longer change when this element also changes.
        /// </summary>
        /// <param name="element">the element to detach from this element.</param>
        void RemoveContainedElement(IPlaceableElement element);
    }
}
