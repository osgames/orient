/* ****************************************************
 * Name: VertexCostList.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/19
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using PAGE.Generic.Math;

namespace PAGE.Generic.Navigation
{
    /// <summary>
    /// Vertex-cost list with hash (vertex-id - cost) functionality.
    /// </summary>
    public class VertexCostList : List<VertexCostPair>
    {
        #region Fields

        protected Dictionary<string, Distance> hash = new Dictionary<string, Distance>();

        #endregion

        #region Properties

        [XmlIgnore]
        public ICollection VertexIDs
        {
            get { return this.hash.Keys; }
        }

        public Distance this[string vertexID]
        {
            get { return this.hash[vertexID]; }
            set
            {
                this.Remove(vertexID);
                this.Add(vertexID, value);
            }
        } 

        #endregion

        #region Public Methods

        public void Add(string vertexID, Distance cost)
        {
            this.Add(new VertexCostPair(vertexID, cost));
        }

        public new void Add(VertexCostPair vertexCost)
        {
            if (this.hash.ContainsKey(vertexCost.VertexID)) return;
            this.hash.Add(vertexCost.VertexID, vertexCost.Cost);
            base.Add(vertexCost);
        }

        public void AddRange(VertexCostList vcList)
        {
            foreach (VertexCostPair vcp in vcList) this.Add(vcp);
        }

        public new void Remove(VertexCostPair vertexCost)
        {
            this.hash.Remove(vertexCost.VertexID);
            base.Remove(vertexCost);
        }

        public void Remove(string vertexID)
        {
            if(!this.hash.ContainsKey(vertexID)) return;

            VertexCostPair vertexCost = new VertexCostPair(vertexID, this.hash[vertexID]);
            this.hash.Remove(vertexID);
            base.Remove(vertexCost);
        }

        public new void Clear()
        {
            this.hash.Clear();
            base.Clear();
        }

        public new bool Contains(VertexCostPair vertexCost)
        {
            return this.hash.ContainsKey(vertexCost.VertexID);
        }

        public bool Contains(string idToken)
        {
            return this.hash.ContainsKey(idToken);
        }

        public VertexCostList Clone()
        {
            VertexCostList vcl = new VertexCostList();
            foreach (VertexCostPair vcp in this)
            {
                vcl.Add(vcp);
            }
            return vcl;
        }
        #endregion
    }
}