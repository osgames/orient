/* ****************************************************
 * Name: Vertex.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/08/17 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.IO;
using System.Xml;
using PAGE.Generic.Math;
using PAGE.Generic.Resources;

namespace PAGE.Generic.Navigation
{
    /// <summary>
    /// Represents a path or edge to a certain vertex, which consists in a 
    /// determined path/edge cost.
    /// </summary>
    public struct VertexCostPair : IXmlSerializable
    {
        public string VertexID;
        public Distance Cost;

        public VertexCostPair(string vertexID, Distance cost)
        {
            this.VertexID = vertexID;
            this.Cost = cost;
        }

        public void ReadXml(XmlElement element)
        {
            this.VertexID = element.GetAttribute("toVertexID");

            XmlElement childElement = (XmlElement)element.SelectSingleNode("cost");
            if (childElement != null) this.Cost.ReadXml(childElement);
        }

        public void WriteXml(XmlElement element)
        {
            element.SetAttribute("toVertexID", this.VertexID);

            XmlElement childElement = element.OwnerDocument.CreateElement("cost");
            this.Cost.WriteXml(childElement);
            element.AppendChild(childElement);
        }
    }

    /// <summary>
    /// Represents a waypoint vertex for navigation purposes. Contains 
    /// information about paths and edges from/to the specified waypoint.
    /// </summary>
    [Serializable]
    public class GenericVertex : TokenObject
    {
        #region Fields

        //vertex edges indexed by target id (targetID - edge cost)
        protected VertexCostList edges = new VertexCostList();

        //vertex paths index by target id (targetID - smallest path cost)
        protected VertexCostList paths = new VertexCostList();

        protected ResourcesFactory resourcesFactory;

        #endregion

        #region Properties

        /// <summary>
        /// Gets a list of vertex edges indexed by target id targetID - edge cost)
        /// </summary>
        public VertexCostList Edges
        {
            get { return edges; }
        }

        /// <summary>
        /// Gets a list of vertex paths index by target id (targetID - smallest path cost)
        /// </summary>
        public VertexCostList Paths
        {
            get { return paths; }
        }

        #endregion

        #region Constructors

        public GenericVertex(string waypointID, ResourcesFactory resourcesFactory)
            : base(waypointID)
        {
            if ((waypointID == null) || (resourcesFactory == null))
            {
                throw new ArgumentException("id and resources factory can't be null");
            }
            this.resourcesFactory = resourcesFactory;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creates an edge to the given vertex, with the given cost.
        /// </summary>
        /// <param name="targetVertexID">the target vertex edge id</param>
        /// <param name="cost">the cost of the new edge</param>
        public void AddEdge(string targetVertexID, Distance cost)
        {
            if (targetVertexID == this.idToken) return;

            if (!this.edges.Contains(targetVertexID))
            {
                this.AddPath(targetVertexID, cost);
                this.edges.Add(targetVertexID, cost);
            }
        }

        /// <summary>
        /// Adds a path to the given vertex, with the given minimal cost.
        /// </summary>
        /// <param name="targetVertexID">the target vertex edge id</param>
        /// <param name="cost">the minimal path cost between this vertex 
        /// and the target.</param>
        public void AddPath(string targetVertexID, Distance cost)
        {
            if ((targetVertexID == this.idToken) ||
                this.edges.Contains(targetVertexID))
            {
                return;
            }

            if (!this.paths.Contains(targetVertexID))
            {
                this.paths.Add(targetVertexID, cost);
            }
            else
            {
                //if new path to target has a smaller cost
                Distance oldCost = this.PathCostTo(targetVertexID);
                if(cost.CompareTo(oldCost) < 0)
                {
                    //replace the previous cost
                    this.paths[targetVertexID] = cost;  
                }
            }
        }

        /// <summary>
        /// Gets the egde cost to the given vertex.
        /// </summary>
        /// <param name="targetVertexID">the id of the target edge vertex</param>
        /// <returns>the cost to the given vertex, or MaxValue if there is no edge
        /// between them.</returns>
        public Distance EdgeCostTo(string targetVertexID)
        {
            if (this.edges.Contains(targetVertexID))
            {
                return this.edges[targetVertexID];
            }
            return Distance.MaxValue;      //no edge to that target
        }

        /// <summary>
        /// Gets the path cost to the given vertex.
        /// </summary>
        /// <param name="targetVertexID">the id of the target path vertex</param>
        /// <returns>the minimal path cost between this vertex and the target,
        /// or MaxValue if there is no path between them.</returns>
        public Distance PathCostTo(string targetVertexID)
        {
            if (this.paths.Contains(targetVertexID))
            {
                return this.paths[targetVertexID];
            }
            return Distance.MaxValue;      //no path to that target
        }

        public override string ToString()
        {
            string ret = "Vertex id: " + this.idToken + "\n";
            ret += "Number of edges: " + this.edges.Count + "\n";
            foreach (string targetVertexID in this.Edges.VertexIDs)
            {
                ret += "Edge cost: " + this.EdgeCostTo(targetVertexID)
                       + " to " + targetVertexID + "\n";
            }
            ret += "Number of paths: " + this.paths.Count + "\n";
            foreach (string targetVertexID in this.Paths.VertexIDs)
            {
                ret += "Path cost: " + this.PathCostTo(targetVertexID)
                       + " to " + targetVertexID + "\n";
            }
            return ret;
        }

        /// <summary>
        /// Writes the structure of the edges and the paths starting from this
        /// vertex, including the cost information.
        /// </summary>
        /// <param name="sw">the stream writer object to the file to 
        /// write the contents.</param>
        public void ToFile(StreamWriter sw)
        {
            sw.WriteLine("Vertex id: " + this.idToken);
            sw.WriteLine();
            sw.WriteLine("Number of edges: " + this.edges.Count);
            foreach (string targetVertexID in this.Edges.VertexIDs)
            {
                sw.WriteLine("Edge cost: " + this.EdgeCostTo(targetVertexID)
                             + " to " + targetVertexID);
            }
            sw.WriteLine();
            sw.WriteLine("Number of paths: " + this.paths.Count);
            foreach (string targetVertexID in this.Paths.VertexIDs)
            {
                sw.WriteLine("Path cost: " + this.PathCostTo(targetVertexID)
                             + " to " + targetVertexID);
            }
        }
        #endregion

        #region Serialization Methods

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            //writes paths
            XmlElement childElement = element.OwnerDocument.CreateElement("paths");
            element.AppendChild(childElement);
            foreach (VertexCostPair path in this.paths)
            {
                XmlElement pathElement = element.OwnerDocument.CreateElement("path");
                path.WriteXml(pathElement);
                childElement.AppendChild(pathElement);
            }

            //writes edges
            childElement = element.OwnerDocument.CreateElement("edges");
            element.AppendChild(childElement);
            foreach (VertexCostPair path in this.edges)
            {
                XmlElement edgeElement = element.OwnerDocument.CreateElement("edge");
                path.WriteXml(edgeElement);
                childElement.AppendChild(edgeElement);
            }
        }

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            //loads paths
            foreach (XmlElement pathElement in element.SelectNodes("paths/path"))
            {
                VertexCostPair path = this.resourcesFactory.CreateNewVertexCostPair();
                path.ReadXml(pathElement);
                this.paths.Add(path);
            }

            //loads edges
            foreach (XmlElement edgeElement in element.SelectNodes("edges/edge"))
            {
                VertexCostPair edge = this.resourcesFactory.CreateNewVertexCostPair();
                edge.ReadXml(edgeElement);
                this.edges.Add(edge);
            }
        }

        #endregion
    }
}
