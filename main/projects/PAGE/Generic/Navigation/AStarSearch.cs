/* ****************************************************
 * Name: AStarSearch.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/08/21 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections.Generic;
using PAGE.Generic.Domain;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Collections;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Math;
using PAGE.Util;

namespace PAGE.Generic.Navigation
{
    /// <summary>
    /// Contains methods to perform an A-Star search over a graph of Vertex objects.
    /// </summary>
    public class AStarSearch
    {
        #region Fields

        //the set on which we are discovering the path
        protected GenericSet set;

        //the possible navigation vertexs
        protected TokenObjectList<GenericVertex> vertexList;

        //the sets waypoints
        protected GenericSpotList<GenericWaypoint> waypoints;

        //open vertexs for A-Star search (vertexID - lastCost)
        protected VertexCostList openVertexsCosts = new VertexCostList();
        protected StringArrayList openVertexs = new StringArrayList();

        //open vertexs parent pointers (vertexID - parentVertexID)
        protected Dictionary<string, string> currentParents = new Dictionary<string, string>();

        //open vertexs parent path costs (vertexID - pathCost)
        protected VertexCostList currentPathCosts = new VertexCostList();

        //closed vertexs for A-Star search (vertexID - lastCost)
        protected VertexCostList closedVertexs = new VertexCostList();

        //black listed vertexs (can't go through them)
        protected StringArrayList blackListedVertexs = new StringArrayList();

        //source and target vertex ids
        protected string sourceID, targetID;

        //current best path and cost during search
        protected StringArrayList currentPath = new StringArrayList();

        //ignore elements during path computation
        protected List<IGraphicalElement> ignoreAssetsList;

        #endregion

        #region Constructor

        public AStarSearch(GenericSet set)
        {
            this.set = set;
            this.vertexList = set.ResourceAsset.VertexList;
            this.waypoints = set.Waypoints;
        } 

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets best current path between a source and a target by doing a A-Star 
        /// search over the set's vertex graph.
        /// </summary>
        /// <param name="sourceVertexID">source path vertex id</param>
        /// <param name="targetVertexID">target path vertex id</param>
        /// <returns>Best available path starting at targetVertexID and ending in 
        /// sourceVertexID (reverse order)</returns>
        /// <param name="ignoreAssets"></param>
        public StringArrayList GetPath(
            string sourceVertexID, string targetVertexID, List<IGraphicalElement> ignoreAssets)
        {
            //checks vertex existence
            if ((this.vertexList.Count == 0) ||
                !this.vertexList.Contains(sourceVertexID) ||
                !this.vertexList.Contains(targetVertexID))
            {
                throw new ArgumentException(
                    "Source or target identifiers are incorrect or vertex list is empty.");
            }

            this.ignoreAssetsList = ignoreAssets;

            //sets source and target info
            this.sourceID = sourceVertexID;
            this.targetID = targetVertexID;

            //inserts start node on open
            this.EnqueueVertex(sourceID, "", Distance.ZeroValue);

            //search for best path
            while (this.openVertexs.Count > 0)
            {
                //gets best estimated vertex
                string curVertexID = this.DequeueVertex();

                //no more vertexs, failure
                if (curVertexID == null) break;

                //adds vertex to current path
                this.currentPath.Add(curVertexID);

                //if current vertex is the target, path found, return it
                if (curVertexID == this.targetID)
                {
                    //collects the pointers of vertexs
                    StringArrayList path = new StringArrayList();
                    while (curVertexID != this.sourceID)
                    {
                        //adds them to path
                        path.Add(curVertexID);
                        curVertexID = this.currentParents[curVertexID];
                    }
                    path.Add(this.sourceID);    //adds first point in path

                    this.ClearAll();
                    return path;                //returns found path
                }

                GenericVertex curVertex = this.vertexList[curVertexID];
                foreach (VertexCostPair costPair in curVertex.Edges)
                {
                    string targetEdgeID = costPair.VertexID;

                    //avoid loops...
                    if (this.currentPath.Contains(targetEdgeID)) continue;

                    Distance pathCost = this.currentPathCosts[curVertexID].Add(
                        curVertex.EdgeCostTo(targetEdgeID));

                    this.EnqueueVertex(targetEdgeID, curVertexID, pathCost);
                }
            }

            this.ClearAll();
            return null;    //no path found
        }

        public StringArrayList GetPath(
            string sourceVertexID, string targetVertexID)
        {
            return this.GetPath(sourceVertexID, targetVertexID,
                new List<IGraphicalElement>());
        } 

        #endregion

        #region Protected Methods

        protected void EnqueueVertex(
            string vertexID, string parentVertexID, Distance pathCost)
        {
            //ignores if vertex is black-listed
            if (this.blackListedVertexs.Contains(vertexID)) return;

            //estimates total vertex cost
            Distance newEstimatedCost = this.HeuristicValue(vertexID).Add(pathCost);

            if (this.closedVertexs.Contains(vertexID))
            {
                //if vertex was visited and its estimation is now better
                Distance oldEstimatedCost = this.closedVertexs[vertexID];
                if (newEstimatedCost.CompareTo(oldEstimatedCost) < 0)
                {
                    //remove it from closed
                    this.closedVertexs.Remove(vertexID);
                    this.currentParents.Remove(vertexID);
                    this.currentPathCosts.Remove(vertexID);
                }
                else return;    //else no point in going in that direction
            }
            else if (this.openVertexs.Contains(vertexID))
            {
                //if vertex is to be visited but its estimation is now better
                Distance oldEstimatedCost = this.openVertexsCosts[vertexID];
                if (newEstimatedCost.CompareTo(oldEstimatedCost) < 0)
                {
                    //remove it from open
                    this.openVertexs.Remove(vertexID);
                    this.openVertexsCosts.Remove(vertexID);
                    this.currentParents.Remove(vertexID);
                    this.currentPathCosts.Remove(vertexID);
                }
                else return;    //else no point in going with the new estimation..
            }

            //vertex is new, inserts new cost in correct order 
            //(openVertexs is ordered ascendingly)
            int insertIdx = 0;
            for (; insertIdx < this.openVertexs.Count; insertIdx++)
            {
                //gets stored estimation
                Distance estimatedCost = this.openVertexsCosts[this.openVertexs[insertIdx]];

                //if the vertex estimation is smaller, put it there
                if (newEstimatedCost.CompareTo(estimatedCost) < 0) break;
            }

            //stores vertex cost and place it on open lists
            this.openVertexs.Insert(insertIdx, vertexID);
            this.openVertexsCosts.Add(vertexID, newEstimatedCost);  //estimation
            this.currentParents.Add(vertexID, parentVertexID);      //parent
            this.currentPathCosts.Add(vertexID, pathCost);          //path cost
        }

        protected string DequeueVertex()
        {
            string newVertexID = null;
            List<string> removeOpenVertexIDs = new List<string>();

            foreach (string vertexID in this.openVertexs)
            {
                //checks if current path is open (no obstacles in between)
                if (currentPath.Count > 0)
                {
                    //gets last path waypoint and new vertex waypoint
                    string lastVertexID = this.currentPath[this.currentPath.Count - 1];
                    GenericWaypoint lastWaypoint = this.waypoints[lastVertexID][0];
                    GenericWaypoint newWaypoint = this.waypoints[vertexID][0];

                    //sees if there is a open path between them
                    if (!this.set.OpenPath(
                             newWaypoint.AbsolutePosition, lastWaypoint.AbsolutePosition, 
                             this.ignoreAssetsList, true))
                    {
                        //if not continue and remove vertex from open
                        removeOpenVertexIDs.Add(vertexID);
                        this.currentParents.Remove(vertexID);
                        this.currentPathCosts.Remove(vertexID);
                        continue;
                    }

                    //re-calculates current path
                    string vertexParentID = this.currentParents[vertexID];
                    int lastPathIndex = this.currentPath.Count - 1;
                    string lastVertexIDInPath = this.currentPath[lastPathIndex];
                    while ((lastVertexIDInPath != vertexParentID) && (lastPathIndex > 0))
                    {
                        //removes all vertexs till last vertex is vertex's parent vertex
                        this.currentPath.RemoveAt(lastPathIndex);
                        lastVertexIDInPath = this.currentPath[--lastPathIndex];
                    }
                }

                //gets first vertex from open list (the smallest estimated cost)
                Distance estimatedCost = this.openVertexsCosts[vertexID];

                //remove it from open
                newVertexID = vertexID;
                removeOpenVertexIDs.Add(vertexID);

                //place it in closed
                this.closedVertexs.Add(vertexID, estimatedCost);

                //new vertex found, stop searching
                break;
            }

            //deletes all "dead-end" vertexs from open
            foreach (string vertexID in removeOpenVertexIDs)
            {
                this.openVertexs.Remove(vertexID);
                this.openVertexsCosts.Remove(vertexID);
            }

            return newVertexID;
        }

        protected Distance HeuristicValue(string vertexID)
        {
            //if vertex was black-listed, null
            if (this.blackListedVertexs.Contains(vertexID)) return Distance.MaxValue;

            //if vertex is source or target, 0 value
            if ((vertexID == this.sourceID) || (vertexID == this.targetID))
                return Distance.ZeroValue;

            //gets waypoint and vertex
            GenericWaypoint waypoint = this.waypoints[vertexID][0];
            GenericVertex vertex = this.vertexList[vertexID];

            //if waypoint is not empty or does not lead to target, max value
            if (!waypoint.IsEmpty || vertex.PathCostTo(this.targetID).Equals(Distance.MaxValue))
            {
                //black-list vertex
                this.blackListedVertexs.Add(vertexID);
                return Distance.MaxValue;
            }

            //else return path distance to target
            return vertex.PathCostTo(targetID);
        }

        protected void ClearAll()
        {
            //clear past search info
            this.openVertexsCosts.Clear();
            this.openVertexs.Clear();
            this.currentPathCosts.Clear();
            this.currentParents.Clear();
            this.closedVertexs.Clear();
            this.blackListedVertexs.Clear();
            this.currentPath.Clear();
        } 

        #endregion
    }
}