/* ****************************************************
 * Name: GraphicsRealizer.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections.Generic;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Cameras;
using PAGE.Generic.GUI;
using PAGE.Generic.Resources;

namespace PAGE.Generic
{
    /// <summary>
    /// A GraphicsRealizer is responsible for the graphical representation of the 
    /// system entities. As such, contains methods for loading/destroying characters, 
    /// sets and items into some graphical engine. 
    /// </summary>
    public abstract class GraphicsRealizer : IDestroyable
    {
        #region Events

        /// <summary>
        /// This event occurs when the realizer is destroyed (by calling Destroy()).
        /// </summary>
        public event EventHandler OnDestroy;

        #endregion

        #region Fields

        protected Destroyable destroyable = new Destroyable();
        protected object locker = new object(); //exclusive code lock

        //managers
        protected ResourcesManager resourcesManager;
        protected GUIManager guiManager;

        //realizer elements
        protected List<GenericCharacter> characters = new List<GenericCharacter>();
        protected List<DynamicAsset> items = new List<DynamicAsset>();
        protected List<GenericSet> sets = new List<GenericSet>();
        protected GenericCamera camera;

        //pause parameters
        protected bool paused;          //if the application is paused
        protected float lastUpdateTime; //last manual update time

        #endregion

        #region Properties 

        /// <summary>
        /// Gets the number of milliseconds occured since the last update call.
        /// </summary>
        protected abstract float LastUpdateTime { get; set; }

        /// <summary>
        /// Gets a list containing the existent (loaded) characters in the realizer.
        /// </summary>
        public List<GenericCharacter> Characters
        {
            get { return new List<GenericCharacter>(characters); }
        }

        /// <summary>
        /// Gets a list containing the existent (loaded) items in the realizer.
        /// </summary>
        public List<DynamicAsset> Items
        {
            get { return new List<DynamicAsset>(items); }
        }

        /// <summary>
        /// Gets a list containing the existent (loaded) sets in the realizer.
        /// </summary>
        public List<GenericSet> Sets
        {
            get { return new List<GenericSet>(sets); }
        }

        /// <summary>
        /// Gets or sets the resources manager associated with the realizer.
        /// </summary>
        public ResourcesManager ResourcesManager
        {
            get { return resourcesManager; }
        }

        /// <summary>
        /// Gets the camera that is displaying the contents of the realizer.
        /// </summary>
        public GenericCamera Camera
        {
            get { return this.camera; }
        }

        /// <summary>
        /// Gets the GUI manager associated with the realizer.
        /// </summary>
        public GUIManager GUIManager
        {
            get { return guiManager; }
        }

        public bool Destroyed
        {
            get { return this.destroyable.Destroyed; }
        }

        #endregion

        #region Abstract & Virtual Methods

        /// <summary>
        /// Setup the realizer according to the given resources configuration file.
        /// </summary>
        /// <param name="resourceFileName">the name of the resources configuration file.
        /// </param>
        public abstract void Setup(string resourceFileName);

        /// <summary>
        /// Creates a new camera to be associated with the realizer.
        /// </summary>
        /// <returns>A new camera to be associated with the realizer.</returns>
        protected abstract GenericCamera CreateCamera();

        /// <summary>
        /// Creates a new resources manager to be associated with the realizer.
        /// </summary>
        /// <param name="resourcesFileName">the name of the file containing the resources
        /// information.</param>
        /// <returns>A new resources manager to be associated with the realizer.</returns>
        protected abstract ResourcesManager CreateResourcesManager(string resourcesFileName);

        /// <summary>
        /// Creates a new GUI manager to be associated with the realizer.
        /// </summary>
        /// <returns>A new GUI manager to be associated with the realizer.</returns>
        protected abstract GUIManager CreateGUIManager();

        #region App Handling

        /// <summary>
        /// Pauses the realizer, updates no longer take effect.
        /// </summary>
        public virtual void Pause()
        {
            lock (this.locker)
            {
                this.paused = !this.paused;
            }
        }

        /// <summary>
        /// Updates the realizer manually, ie., according to the given update time.
        /// </summary>
        /// <param name="updateTime">the number of milliseconds to manually update the
        /// realizer.</param>
        public virtual void ManualUpdate(float updateTime)
        {
            if (!this.paused) return;
            this.lastUpdateTime = updateTime;
            this.Update();
            this.lastUpdateTime = 0;
        }

        /// <summary>
        /// Updates the realizer. The update time is calculated according to the last
        /// Update() call. In general terms, updates all the loaded assets, the camera 
        /// and the GUI manager.
        /// </summary>
        public virtual void Update()
        {
            lock (this.locker)
            {
                if (this.Destroyed) return;

                //updates scene elements
                foreach (GenericCharacter c in this.Characters)
                {
                    if (!c.Destroyed) c.Update(this.LastUpdateTime);
                    else this.RemoveCharacter(c);
                }
                foreach (GenericItem i in this.Items)
                {
                    if (!i.Destroyed) i.Update(this.LastUpdateTime);
                    else this.RemoveItem(i);
                }
                foreach (GenericSet s in this.Sets)
                {
                    if (!s.Destroyed) s.Update(this.LastUpdateTime);
                    else this.RemoveSet(s);
                }

                //updates camera
                if ((this.camera != null) && !this.camera.Destroyed)
                {
                    this.camera.Update(this.LastUpdateTime);
                }

                //updates gui
                if (this.guiManager != null) this.guiManager.Update(this.LastUpdateTime);
            }
        }

        /// <summary>
        /// Destroys the realizer by removing all loaded assets.
        /// </summary>
        public virtual void Destroy()
        {
            if (this.Destroyed) return;

            this.RemoveAll();

            this.destroyable.Destroy();

            if (this.OnDestroy != null) this.OnDestroy(this, EventArgs.Empty);
        }

        public void VerifyDestruction()
        {
            this.destroyable.VerifyDestruction();
        }

        #endregion

        #region Loads and Removes

        /// <summary>
        /// Loads a character into the realizer according the confirguration of the
        /// given character resource.
        /// </summary>
        /// <param name="idResourceToken">the id of the character resource info.</param>
        /// <returns>a new GenericCharacter according to the given resource info.</returns>
        public abstract GenericCharacter LoadCharacter(string idResourceToken);

        /// <summary>
        /// Loads a item into the realizer according the confirguration of the given 
        /// item resource.
        /// </summary>
        /// <param name="idResourceToken">the id of the item resource info.</param>
        /// <returns>a new GenericItem according to the given resource info.</returns>
        public abstract GenericItem LoadItem(string idResourceToken);

        /// <summary>
        /// Loads a set into the realizer according the confirguration of the given set
        /// resource.
        /// </summary>
        /// <param name="idResourceToken">the id of the set resource info.</param>
        /// <returns>a new GenericSet according to the given resource info.</returns>
        public abstract GenericSet LoadSet(string idResourceToken);

        /// <summary>
        /// Removes the given character from the realizer. The character will no longer
        /// be graphically representated by the realizer and will be destroyed.
        /// </summary>
        /// <param name="character">the character to remove from the realizer.</param>
        public abstract void RemoveCharacter(GenericCharacter character);

        /// <summary>
        /// Removes the given item from the realizer. The item will no longer be 
        /// graphically representated by the realizer and will be destroyed.
        /// </summary>
        /// <param name="item">the character to remove from the realizer.</param>
        public abstract void RemoveItem(DynamicAsset item);

        /// <summary>
        /// Removes the given set from the realizer. The set will no longer be 
        /// graphically representated by the realizer and will be destroyed.
        /// </summary>
        /// <param name="set">the set to remove from the realizer.</param>
        public abstract void RemoveSet(GenericSet set);

        /// <summary>
        /// Removes and destroys all characters from the realizer.
        /// </summary>
        public virtual void RemoveAllCharacters()
        {
            lock (this.locker)
            {
                foreach (GenericCharacter c in this.Characters)
                {
                    this.RemoveCharacter(c);
                }
            }
        }

        /// <summary>
        /// Removes and destroys all items from the realizer.
        /// </summary>
        public virtual void RemoveAllItems()
        {
            lock (this.locker)
            {
                foreach (DynamicAsset i in this.Items)
                {
                    this.RemoveItem(i);
                }
            }
        }

        /// <summary>
        /// Removes and destroys all sets from the realizer.
        /// </summary>
        public virtual void RemoveAllSets()
        {
            lock (this.locker)
            {
                foreach (GenericSet s in this.Sets)
                {
                    this.RemoveSet(s);
                }
            }
        }

        /// <summary>
        /// Removes and destroys all assets associated with the realizer.
        /// </summary>
        public virtual void RemoveAll()
        {
            this.RemoveAllCharacters();
            this.RemoveAllItems();
            this.RemoveAllSets();
        }

        #endregion

        #region Resets

        /// <summary>
        /// Resets all characters associated with the realizer.
        /// </summary>
        public virtual void ResetAllCharacters()
        {
            foreach (HumanLikeCharacter c in this.characters)
            {
                c.Reset();
            }
        }

        /// <summary>
        /// Resets all items associated with the realizer.
        /// </summary>
        public virtual void ResetAllItems()
        {
            foreach (DynamicAsset i in this.items)
            {
                i.Reset();
            }
        }

        /// <summary>
        /// Resets all sets associated with the realizer.
        /// </summary>
        public virtual void ResetAllSets()
        {
            foreach (GenericSet s in this.sets)
            {
                s.Reset();
            }
        }

        /// <summary>
        /// Resets all assets associated with the realizer.
        /// </summary>
        public virtual void Reset()
        {
            this.ResetAllCharacters();
            this.ResetAllItems();
            this.ResetAllSets();
            this.destroyable.Reset();
        }

        #endregion

        #endregion
    }
}