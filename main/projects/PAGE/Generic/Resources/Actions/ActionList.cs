/* ****************************************************
 * Name: ActionList.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/20 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using PAGE.Util;

namespace PAGE.Generic.Resources.Actions
{
    /// <summary>
    /// Represents a list of ResourceActions with indexing (by action id) functionality.
    /// </summary>
    [Serializable]
    public class ActionList : TokenObjectList<ResourceAction>, IXmlSerializable
    {
        /// <summary>
        /// Saves (serializes) the resource object to the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name to save the object info into.</param>
        public virtual void SaveToXml(string xmlFileName)
        {
            this.SaveToXml(xmlFileName, "actions");
        }

        /// <summary>
        /// Saves (serializes) the resource object to the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name to save the object info into.</param>
        /// <param name="rootName">the name for the root XML element.</param>
        public void SaveToXml(string xmlFileName, string rootName)
        {
            try
            {
                //creates xml document
                XmlDocument xd = new XmlDocument();

                //root element
                XmlElement rootElement = xd.CreateElement(rootName);
                xd.CreateXmlDeclaration("1.0", Encoding.UTF8.EncodingName, "");
                xd.AppendChild(rootElement);

                //write contents
                this.WriteXml(rootElement);

                //verifies file existence
                if (File.Exists(xmlFileName)) File.Delete(xmlFileName);

                //save the document to file
                StreamWriter writer = new StreamWriter(xmlFileName, false, Encoding.UTF8);
                xd.Save(writer);
                writer.Close();
            }
            catch (XmlException)    //do nothing...
            {
            }
            catch (IOException)    //do nothing...
            {
            }
            catch (InvalidOperationException)    //do nothing...
            {
            }
        }

        /// <summary>
        /// Loads (deserializes) the resource object from the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name containing the object info.</param>
        /// <returns>the ResourceAsset loaded from the XML file.</returns>
        public virtual void LoadFromXml(string xmlFileName)
        {
            this.LoadFromXml(xmlFileName, "actions");
        }

        /// <summary>
        /// Loads (deserializes) the resource object from the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name containing the object info.</param>
        /// <param name="rootName">the name for the root XML element.</param>
        public void LoadFromXml(string xmlFileName, string rootName)
        {
            if (!File.Exists(xmlFileName)) return;

            XmlDocument xd = new XmlDocument();
            try
            {
                xd.Load(xmlFileName);

                //loads root element
                XmlElement rootElement = (XmlElement)xd.SelectSingleNode(rootName);
                if (rootElement == null) return;

                this.ReadXml(rootElement);
            }
            catch (XmlException)      //do nothing...
            {
            }
            catch (XPathException)    //do nothing...
            {
            }
        }

        public virtual void ReadXml(XmlElement element)
        {
            //loads actions
            foreach (XmlElement actionElement in element.SelectNodes("action"))
            {
                //checks action type
                string type = actionElement.GetAttribute("type");
                ResourceAction newAction = this.GetTypeResourceAction(type);
                if (newAction != null)
                {
                    newAction.ReadXml(actionElement);
                    this.Add(newAction);
                }
            }
        }

        public virtual void WriteXml(XmlElement element)
        {
            //write camera spots
            foreach (ResourceAction action in this)
            {
                XmlElement actionElement = element.OwnerDocument.CreateElement("action");
                action.WriteXml(actionElement);
                element.AppendChild(actionElement);
            }
        }

        protected virtual ResourceAction GetTypeResourceAction(string type)
        {
            switch (type)
            {
                case "animation":   return new AnimationAction();
                case "move-to":     return new MoveToAction();
                case "walk-to":     return new WalkToAction();
                case "say":         return new SayAction();
            }
            return null;
        }
    }
}