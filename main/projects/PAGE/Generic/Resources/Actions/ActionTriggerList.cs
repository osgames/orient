/* ****************************************************
 * Name: ActionTriggerList.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/20
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using PAGE.Util;

namespace PAGE.Generic.Resources.Actions
{
    /// <summary>
    /// A list of ActionTrigger objects with hash functionality
    /// (indexed by trigger time or trigger id).
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "triggers", IsNullable = false)]
    public class ActionTriggerList : TokenObjectList<ActionTrigger>
    {
        #region Fields

        //hash indexed by trigger times
        private Dictionary<float, ActionTrigger> timeHash = new Dictionary<float, ActionTrigger>();

        //hash indexed by trigger times
        private Dictionary<string, ActionTrigger> idHash = new Dictionary<string, ActionTrigger>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets the list of trigger times of all the stored 
        /// action triggers.
        /// </summary>
        [XmlIgnore]
        public List<float> Times
        {
            get
            {
                List<float> orderedList = new List<float>(this.timeHash.Keys);
                orderedList.Sort();
                return orderedList;
            }
        }

        /// <summary>
        /// Gets the trigger that occurs at the given time.
        /// </summary>
        /// <param name="time">the time the trigger occurs</param>
        /// <returns>The ActionTrigger that occurs at the given 
        /// time, or null if no event is triggered at that time.</returns>
        public ActionTrigger this[float time]
        {
            get { return this.timeHash[time]; }
        }

        /// <summary>
        /// Gets the trigger that is stored within the list at the given index.
        /// </summary>
        /// <param name="index">the list index where the trigger is stored at</param>
        /// <returns>The ActionTrigger that is stored at the given index.</returns>
        public new ActionTrigger this[int index]
        {
            get
            {
                //overrided on purpose so that the float indexer wasn't called.
                return base[index];
            }
            set { base[index] = value; }
        }

        /// <summary>
        /// Gets the ActionTrigger identified by the given ID.
        /// </summary>
        /// <param name="triggerID">the id token of the desired 
        /// ActionTrigger</param>
        /// <returns>The ActionTrigger identified by the given ID,
        /// or null if no trigger has that ID.</returns>
        public new ActionTrigger this[string triggerID]
        {
            get { return this.idHash[triggerID]; }
        }

        #endregion

        #region Public Methods

        public new void Add(ActionTrigger trigger)
        {
            if (this.Contains(trigger.IdToken)) return;

            //get unique time id
            while (this.timeHash.ContainsKey(trigger.Time))
            {
                trigger.Time = trigger.Time + 0.001f;
            }
            this.timeHash.Add(trigger.Time, trigger);
            this.idHash.Add(trigger.IdToken, trigger);
            base.Add(trigger);
        }

        public new void Remove(ActionTrigger trigger)
        {
            this.timeHash.Remove(trigger.Time);
            this.idHash.Remove(trigger.IdToken);
            base.Remove(trigger);
        }

        public new void RemoveAt(int index)
        {
            ActionTrigger trigger = this[index];
            if (trigger == null) return;
            this.timeHash.Remove(trigger.Time);
            this.idHash.Remove(trigger.IdToken);
            base.RemoveAt(index);
        }

        public new void RemoveRange(int index, int count)
        {
            for (int i = index; i < index + count; i++ )
            {
                ActionTrigger trigger = this[index];
                if (trigger == null) continue;
                this.timeHash.Remove(trigger.Time);
                this.idHash.Remove(trigger.IdToken);
            }
            base.RemoveRange(index, count);
        }

        public new bool Contains(ActionTrigger trigger)
        {
            return
                this.idHash.ContainsKey(trigger.IdToken) &&
                this.timeHash.ContainsKey(trigger.Time);
        }

        public bool Contains(float time)
        {
            return this.timeHash.ContainsKey(time);
        }

        public new void Clear()
        {
            this.timeHash.Clear();
            this.idHash.Clear();
            base.Clear();
        }

        #endregion
    }
}