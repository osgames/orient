/* ****************************************************
 * Name: WalkToAction.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/05 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Xml;
using PAGE.Generic.Math;

namespace PAGE.Generic.Resources.Actions
{
    /// <summary>
    /// Represents an action whose animation is used when a walking character moves 
    /// to some place.
    /// </summary>
    public class WalkToAction : MoveToAction
    {
        #region Fields

        protected Distance walkStepDist = Distance.ZeroValue; 

        #endregion

        #region Properties

        protected override string ActionType
        {
            get { return "walk-to"; }
        }

        /// <summary>
        /// Gets or sets a value indicating how much to move
        /// a character when a cycle of the animation is performed.
        /// </summary>
        public Distance WalkStepDist
        {
            get { return walkStepDist; }
            set { walkStepDist = value; }
        } 

        #endregion

        #region Constructors

        public WalkToAction()
            : this("walk-action0", "anim", Distance.ZeroValue)
        {
        }

        public WalkToAction(string idToken, string animationName, Distance WalkStepDist)
            : base(idToken, animationName)
        {
            this.walkStepDist = WalkStepDist;
        } 

        #endregion

        #region Serialization Methods

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            XmlElement childElement = (XmlElement)element.SelectSingleNode("step-distance");
            if (childElement != null) this.walkStepDist.ReadXml(childElement);
        }

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            XmlElement childElement = element.OwnerDocument.CreateElement("step-distance");
            this.walkStepDist.WriteXml(childElement);
            element.AppendChild(childElement);

        }

        #endregion
    }
}