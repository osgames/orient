/* ****************************************************
 * Name: ActionTrigger.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Globalization;
using System.Xml;

namespace PAGE.Generic.Resources.Actions
{
    /// <summary>
    /// Represents an event that occurs at a certain time during the excution 
    /// of an action.
    /// </summary>
    public class ActionTrigger : TokenObject
    {
        #region Fields

        private float time = 0; 

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the time in seconds when the event is triggered
        /// relative to the start of an action's execution.
        /// </summary>
        public float Time
        {
            get { return time; }
            set { time = value; }
        } 

        #endregion

        #region Constructors

        public ActionTrigger() : this("trigger0")
        {
        }

        public ActionTrigger(string triggerID)
            : base(triggerID)
        {
        }

        public ActionTrigger(string triggerID, float time)
            : base(triggerID)
        {
            this.time = time;
        } 

        #endregion

        #region Serialization Methods

        public override void SaveToXml(string xmlFileName)
        {
            this.SaveToXml(xmlFileName, "trigger");
        }

        public override void LoadFromXml(string xmlFileName)
        {
            this.LoadFromXml(xmlFileName, "trigger");
        }

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            this.time = Convert.ToSingle(element.GetAttribute("time"), CultureInfo.InvariantCulture);
        }

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            element.SetAttribute("time", Convert.ToString(this.time, CultureInfo.InvariantCulture));
        }

        #endregion
    }
}