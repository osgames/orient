/* ****************************************************
 * Name: ResourceAction.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Xml;

namespace PAGE.Generic.Resources.Actions
{
    /// <summary>
    /// Contains the base information about resource actions. Actions can be performed 
    /// by the characters and have events that trigger some body or item behavior.
    /// </summary>
    public abstract class ResourceAction : TokenObject
    {
        #region Fields

        protected ActionTriggerList triggers = new ActionTriggerList(); 

        #endregion

        #region Properties

        /// <summary>
        /// Gets the list of triggers associated with the action.
        /// </summary>
        public ActionTriggerList Triggers
        {
            get { return this.triggers; }
        } 

        protected virtual string ActionType
        {
            get { return "action"; }
        }

        #endregion

        #region Constructors

        public ResourceAction()
            : this("action0")
        {
        }

        public ResourceAction(string idToken)
            : base(idToken)
        {
        }

        #endregion

        #region Serialization Methods

        public override void SaveToXml(string xmlFileName)
        {
            this.SaveToXml(xmlFileName, "action");
        }

        public override void LoadFromXml(string xmlFileName)
        {
            this.LoadFromXml(xmlFileName, "action");
        }

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            //loads trigger
            foreach (XmlElement triggerElement in element.SelectNodes("triggers/trigger"))
            {
                ActionTrigger trigger = new ActionTrigger();
                trigger.ReadXml(triggerElement);
                this.triggers.Add(trigger);
            }
        }

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            //writes action type
            element.SetAttribute("type", this.ActionType);

            //write triggers
            XmlElement childElement = element.OwnerDocument.CreateElement("triggers");
            element.AppendChild(childElement);
            foreach (ActionTrigger trigger in this.triggers)
            {
                XmlElement triggerElement = element.OwnerDocument.CreateElement("trigger");
                trigger.WriteXml(triggerElement);
                childElement.AppendChild(triggerElement);
            }
        }

        #endregion

        #region Public Methods

        public override void Dispose()
        {
            base.Dispose();

            this.triggers.Clear();
        }

        #endregion

    }
}