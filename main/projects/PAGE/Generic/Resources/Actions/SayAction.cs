/* ****************************************************
 * Name: SayAction.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
namespace PAGE.Generic.Resources.Actions
{
    /// <summary>
    /// Represents an action used when a character says something.
    /// </summary>
    public class SayAction : ResourceAction
    {
        protected override string ActionType
        {
            get { return "say"; }
        }

        public SayAction()
            : this("say-action0")
        {
        }

        public SayAction(string idToken)
            : base(idToken)
        {
        }
    }
}