/* ****************************************************
 * Name: MoveToAction.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
namespace PAGE.Generic.Resources.Actions
{
    /// <summary>
    /// Represents an action whose animation is used when a character moves to some place.
    /// </summary>
    public class MoveToAction : AnimationAction
    {
        protected override string ActionType
        {
            get { return "move-to"; }
        }

        #region Constructors

        public MoveToAction()
            : this("move-action0", "anim")
        {
        }

        public MoveToAction(string idToken, string animationName)
            : base(idToken, animationName)
        {
        } 

        #endregion
    }
}