/* ****************************************************
 * Name: AnimateAction.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Globalization;
using System.Xml;

namespace PAGE.Generic.Resources.Actions
{
    /// <summary>
    /// Represents an action with a character animation
    /// associated with it.
    /// </summary>
    public class AnimationAction : ResourceAction
    {
        #region Fields

        //action's corresponding animation name
        protected string animationName;

        //action's animation speed
        protected float animationSpeed = 1.0f;

        //if animation should be blended with previous
        protected bool blend = false;

        #endregion

        #region Properties

        protected override string ActionType
        {
            get { return "animation"; }
        }

        /// <summary>
        /// Gets or sets a value corresponding to the 
        /// action's animation name.
        /// </summary>
        public string AnimationName
        {
            get { return animationName; }
            set { animationName = value; }
        }

        /// <summary>
        /// Gets or sets a value corresponding to the 
        /// action's animation speed.
        /// </summary>
        public float AnimationSpeed
        {
            get { return animationSpeed; }
            set { animationSpeed = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating wether this animation
        /// should blend with the previous one.
        /// </summary>
        public bool Blend
        {
            get { return blend; }
            set { blend = value; }
        }

        #endregion

        #region Constructors

        public AnimationAction()
            : this("animation-action0", "anim")
        {
        }

        public AnimationAction(string idToken, string animationName)
            : base(idToken)
        {
            this.animationName = animationName;
        }

        #endregion

        #region Serialization Methods

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            this.animationName = element.GetAttribute("animation-name");
            this.animationSpeed =
                Convert.ToSingle(element.GetAttribute("animation-speed"), CultureInfo.InvariantCulture);
            this.blend = Convert.ToBoolean(element.GetAttribute("blend"), CultureInfo.InvariantCulture);
        }

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            element.SetAttribute("animation-name", this.animationName);
            element.SetAttribute("animation-speed", 
                Convert.ToString(this.animationSpeed, CultureInfo.InvariantCulture));
            element.SetAttribute("blend", Convert.ToString(this.blend, CultureInfo.InvariantCulture));
        }

        #endregion
    }
}