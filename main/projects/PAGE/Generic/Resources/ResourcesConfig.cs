/* ****************************************************
 * Name: ResourcesConfig.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;

namespace PAGE.Generic.Resources
{
    /// <summary>
    /// Contains information concerning all the realizer resources and files.
    /// </summary>
    public class ResourcesConfig : IXmlSerializable
    {
        #region Fields

        //resource files
        protected string setsPath = "sets";
        protected string charactersPath = "characters";
        protected string itemsPath = "items";

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the path of the resource characters config file.
        /// </summary>
        public string CharactersPath
        {
            get { return charactersPath; }
            set { charactersPath = value; }
        }

        /// <summary>
        /// Gets or sets the path of the resource sets config file.
        /// </summary>
        public string SetsPath
        {
            get { return setsPath; }
            set { setsPath = value; }
        }

        /// <summary>
        /// Gets or sets the path of the resource items config file.
        /// </summary>
        public string ItemsPath
        {
            get { return itemsPath; }
            set { itemsPath = value; }
        }

        #endregion

        #region Serializable Methods

        /// <summary>
        /// Saves (serializes) the resource object to the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name to save the object info into.</param>
        public virtual void SaveToXml(string xmlFileName)
        {
            this.SaveToXml(xmlFileName, "resources-config");
        }

        /// <summary>
        /// Saves (serializes) the resource object to the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name to save the object info into.</param>
        /// <param name="rootName">the name for the root XML element.</param>
        public void SaveToXml(string xmlFileName, string rootName)
        {
            try
            {
                //creates xml document
                XmlDocument xd = new XmlDocument();

                //root element
                XmlElement rootElement = xd.CreateElement(rootName);
                xd.CreateXmlDeclaration("1.0", Encoding.UTF8.EncodingName, "");
                xd.AppendChild(rootElement);

                //write contents
                this.WriteXml(rootElement);

                //verifies file existence
                if (File.Exists(xmlFileName)) File.Delete(xmlFileName);

                //save the document to file
                StreamWriter writer = new StreamWriter(xmlFileName, false, Encoding.UTF8);
                xd.Save(writer);
                writer.Close();
            }
            catch (XmlException) //do nothing...
            {
            }
            catch (IOException) //do nothing...
            {
            }
            catch (InvalidOperationException) //do nothing...
            {
            }
        }

        /// <summary>
        /// Loads (deserializes) the resource object from the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name containing the object info.</param>
        /// <returns>the ResourceAsset loaded from the XML file.</returns>
        public virtual void LoadFromXml(string xmlFileName)
        {
            this.LoadFromXml(xmlFileName, "resources-config");
        }

        /// <summary>
        /// Loads (deserializes) the resource object from the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name containing the object info.</param>
        /// <param name="rootName">the name for the root XML element.</param>
        public void LoadFromXml(string xmlFileName, string rootName)
        {
            if (!File.Exists(xmlFileName)) return;

            XmlDocument xd = new XmlDocument();
            try
            {
                xd.Load(xmlFileName);

                //loads root element
                XmlElement rootElement = (XmlElement) xd.SelectSingleNode(rootName);
                if (rootElement == null) return;

                this.ReadXml(rootElement);
            }
            catch (XmlException) //do nothing...
            {
            }
            catch (XPathException) //do nothing...
            {
            }
        }

        public virtual void ReadXml(XmlElement element)
        {
            XmlElement childElement = (XmlElement) element.SelectSingleNode("SetsPath");
            if (childElement != null) this.setsPath = childElement.InnerXml;

            childElement = (XmlElement) element.SelectSingleNode("CharactersPath");
            if (childElement != null) this.charactersPath = childElement.InnerXml;

            childElement = (XmlElement) element.SelectSingleNode("ItemsPath");
            if (childElement != null) this.itemsPath = childElement.InnerXml;
        }

        public virtual void WriteXml(XmlElement element)
        {
            XmlElement childElement = element.OwnerDocument.CreateElement("SetsPath");
            childElement.InnerXml = this.setsPath;
            element.AppendChild(childElement);

            childElement = element.OwnerDocument.CreateElement("CharactersPath");
            childElement.InnerXml = this.charactersPath;
            element.AppendChild(childElement);

            childElement = element.OwnerDocument.CreateElement("ItemsPath");
            childElement.InnerXml = this.itemsPath;
            element.AppendChild(childElement);
        }

        #endregion
    }
}