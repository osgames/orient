/* ****************************************************
 * Name: ResourceCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Xml;
using PAGE.Generic.Resources.Actions;

namespace PAGE.Generic.Resources.Assets
{
    /// <summary>
    /// Contains the resource information for a character.
    /// </summary>
    public class ResourceCharacter : ResourceAsset
    {
        #region Fields

        protected ActionList actions;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the list of actions available for this character.
        /// </summary>
        public ActionList Actions
        {
            get { return this.actions; }
        }

        #endregion

        #region Constructors

        public ResourceCharacter(string characterID, ResourcesFactory resourcesFactory)
            : base(characterID, resourcesFactory)
        {
        }

        #endregion

        #region Protected Methods

        protected override void InitComponents()
        {
            base.InitComponents();

            //creates a new action list
            this.actions = this.resourcesfactory.CreateNewActionList();
        }

        #endregion

        #region Public Methods

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            //loads actions
            XmlElement actionElement = (XmlElement)element.SelectSingleNode("actions");
            if (actionElement != null) this.Actions.ReadXml(actionElement);
        }

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            //write actions
            XmlElement actionElement = element.OwnerDocument.CreateElement("actions");
            this.Actions.WriteXml(actionElement);
            element.AppendChild(actionElement);
        }

        public override void SaveToXml(string xmlFileName)
        {
            this.SaveToXml(xmlFileName, "character");
        }

        public override void LoadFromXml(string xmlFileName)
        {
            this.LoadFromXml(xmlFileName, "character");
        }

        #endregion
    }
}
