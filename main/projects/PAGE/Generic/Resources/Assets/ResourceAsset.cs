/* ****************************************************
 * Name: ResourceAsset.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Xml;
using PAGE.Generic.Math;
using PAGE.Generic.Resources.Spots;
using PAGE.Util;

namespace PAGE.Generic.Resources.Assets
{
    /// <summary>
    /// Contains the base information about resource assets.
    /// </summary>
    public abstract class ResourceAsset : TokenObject
    {
        #region Fields

        protected string fileName;
        protected StringArrayList tags = new StringArrayList();
        protected ResourcesFactory resourcesfactory;

        //spots
        protected TokenObjectList<ResourceWaypoint> resourceWaypoints =
            new TokenObjectList<ResourceWaypoint>();

        protected TokenObjectList<ResourceCameraSpot> resourceCameraSpots =
            new TokenObjectList<ResourceCameraSpot>();

        protected TokenObjectList<ResourceItemSpot> resourceItemSpots =
            new TokenObjectList<ResourceItemSpot>();

        //graphical elements
        protected Scale scale;
        protected Position translation;
        protected Orientation orientation;
        protected Direction direction;
        protected Color color;

        #endregion

        #region Properties

        public override string IdToken
        {
            get { return base.IdToken; }
            set
            {
                //adds new id to tags, removes old one
                this.Tags.Remove(base.IdToken);
                base.IdToken = value;
                this.Tags.Add(value);
            }
        }

        /// <summary>
        /// Gets or sets the name of the XML resource file.
        /// </summary>
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        /// <summary>
        /// Gets or sets the color associated with the resource.
        /// </summary>
        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        /// <summary>
        /// Gets or sets the resource's initial direction.
        /// </summary>
        public Direction Direction
        {
            get { return direction; }
            set { direction = value; }
        }

        /// <summary>
        /// Gets or sets the resources' initial orientation.
        /// </summary>
        public Orientation Orientation
        {
            get { return orientation; }
            set { orientation = value; }
        }

        /// <summary>
        /// Gets or sets the resources' initial scale factor.
        /// </summary>
        public Scale Scale
        {
            get { return scale; }
            set { scale = value; }
        }

        /// <summary>
        /// Gets or sets the resources' initial translation vector.
        /// </summary>
        public Position Translation
        {
            get { return translation; }
            set { translation = value; }
        }

        /// <summary>
        /// Gets the waypoints associated with this resource.
        /// </summary>
        public TokenObjectList<ResourceWaypoint> ResourceWaypoints
        {
            get { return this.resourceWaypoints; }
        }

        /// <summary>
        /// Gets the camera spots associated with this resource.
        /// </summary>
        public TokenObjectList<ResourceCameraSpot> ResourceCameraSpots
        {
            get { return this.resourceCameraSpots; }
        }

        /// <summary>
        /// Gets the item spots associated with this resource.
        /// </summary>
        public TokenObjectList<ResourceItemSpot> ResourceItemSpots
        {
            get { return this.resourceItemSpots; }
        }

        /// <summary>
        /// Gets the string tags associated with the resource this.
        /// </summary>
        public StringArrayList Tags
        {
            get { return this.tags; }
        }

        /// <summary>
        /// Gets all the spots associated with this resource in a single list.
        /// </summary>
        public TokenObjectList<ResourceSpot> AllSpots
        {
            get
            {
                TokenObjectList<ResourceSpot> list = new TokenObjectList<ResourceSpot>();
                foreach (ResourceCameraSpot spot in this.resourceCameraSpots) list.Add(spot);
                foreach (ResourceWaypoint spot in this.resourceWaypoints) list.Add(spot);
                foreach (ResourceItemSpot spot in this.resourceItemSpots) list.Add(spot);
                return list;
            }
        }

        /// <summary>
        /// Gets the resources factory used by this asset.
        /// </summary>
        public ResourcesFactory ResourcesFactory
        {
            get { return resourcesfactory; }
        }

        #endregion

        #region Constructor

        public ResourceAsset(string idToken, ResourcesFactory resourcesfactory)
            : base(idToken)
        {
            if ((idToken == null) || (resourcesfactory == null))
            {
                throw new ArgumentException("id and resources factory can't be null");
            }

            this.Tags.Add(idToken);
            this.resourcesfactory = resourcesfactory;

            this.InitComponents();
        }

        #endregion

        #region Serialization Methods

        public override void SaveToXml(string xmlFileName)
        {
            this.SaveToXml(xmlFileName, "asset");
        }

        public override void LoadFromXml(string xmlFileName)
        {
            this.LoadFromXml(xmlFileName, "asset");
        }

        public override void ReadXml(XmlElement element)
        {
            if (element == null) throw new ArgumentNullException("element");

            base.ReadXml(element);

            //initilizes components to default value
            this.InitComponents();

            //file name
            this.fileName = element.GetAttribute("filename");

            //translation
            XmlElement childElement = (XmlElement) element.SelectSingleNode("translation");
            if (childElement != null) this.Translation.ReadXml(childElement);

            //scale
            childElement = (XmlElement) element.SelectSingleNode("scale");
            if (childElement != null) this.Scale.ReadXml(childElement);

            //direction
            childElement = (XmlElement) element.SelectSingleNode("direction");
            if (childElement != null) this.Direction.ReadXml(childElement);

            //orientation
            childElement = (XmlElement) element.SelectSingleNode("orientation");
            if (childElement != null) this.Orientation.ReadXml(childElement);

            //color
            childElement = (XmlElement) element.SelectSingleNode("color");
            if (childElement != null) this.Color.ReadXml(childElement);

            //loads assets's camera spots
            foreach (XmlElement spotElement in element.SelectNodes("camera-spots/camera-spot"))
            {
                ResourceCameraSpot spot = this.resourcesfactory.CreateNewCameraSpot("cam-spot0");
                spot.ReadXml(spotElement);
                this.ResourceCameraSpots.Add(spot);
            }

            //loads assets's waypoints
            foreach (XmlElement spotElement in element.SelectNodes("waypoints/waypoint"))
            {
                ResourceWaypoint spot = this.resourcesfactory.CreateNewWaypoint("waypoint0");
                spot.ReadXml(spotElement);
                this.ResourceWaypoints.Add(spot);
            }

            //loads assets's item spots
            foreach (XmlElement spotElement in element.SelectNodes("item-spots/item-spot"))
            {
                ResourceItemSpot spot = this.resourcesfactory.CreateNewItemSpot("item-spot0");
                spot.ReadXml(spotElement);
                this.ResourceItemSpots.Add(spot);
            }

            //loads assets's tags
            foreach (XmlElement tagElement in element.SelectNodes("tags/tag"))
            {
                string tagName = tagElement.InnerXml;
                if (tagName != "") this.Tags.Add(tagName);
            }
        }

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            //file name
            element.SetAttribute("filename", this.fileName);

            //translation
            XmlElement childElement = element.OwnerDocument.CreateElement("translation");
            this.Translation.WriteXml(childElement);
            element.AppendChild(childElement);

            //scale
            childElement = element.OwnerDocument.CreateElement("scale");
            this.Scale.WriteXml(childElement);
            element.AppendChild(childElement);

            //direction
            childElement = element.OwnerDocument.CreateElement("direction");
            this.Direction.WriteXml(childElement);
            element.AppendChild(childElement);

            //orientation
            childElement = element.OwnerDocument.CreateElement("orientation");
            this.Orientation.WriteXml(childElement);
            element.AppendChild(childElement);

            //color
            childElement = element.OwnerDocument.CreateElement("color");
            this.Color.WriteXml(childElement);
            element.AppendChild(childElement);

            //write camera spots
            childElement = element.OwnerDocument.CreateElement("camera-spots");
            element.AppendChild(childElement);
            foreach (ResourceCameraSpot spot in this.ResourceCameraSpots)
            {
                XmlElement spotElement = element.OwnerDocument.CreateElement("camera-spot");
                spot.WriteXml(spotElement);
                childElement.AppendChild(spotElement);
            }

            //write waypoints
            childElement = element.OwnerDocument.CreateElement("waypoints");
            element.AppendChild(childElement);
            foreach (ResourceWaypoint spot in this.ResourceWaypoints)
            {
                XmlElement spotElement = element.OwnerDocument.CreateElement("waypoint");
                spot.WriteXml(spotElement);
                childElement.AppendChild(spotElement);
            }

            //write camera spots
            childElement = element.OwnerDocument.CreateElement("item-spots");
            element.AppendChild(childElement);
            foreach (ResourceItemSpot spot in this.ResourceItemSpots)
            {
                XmlElement spotElement = element.OwnerDocument.CreateElement("item-spot");
                spot.WriteXml(spotElement);
                childElement.AppendChild(spotElement);
            }

            //writes tags
            childElement = element.OwnerDocument.CreateElement("tags");
            element.AppendChild(childElement);
            foreach (string tag in this.Tags)
            {
                XmlElement tagElement = element.OwnerDocument.CreateElement("tag");
                tagElement.InnerXml = tag.ToLower();
                childElement.AppendChild(tagElement);
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Initializes all the resource components to its default values.
        /// </summary>
        protected virtual void InitComponents()
        {
            this.translation = this.resourcesfactory.CreateNewPosition();
            this.scale = this.resourcesfactory.CreateNewScale();
            this.direction = this.resourcesfactory.CreateNewDirection();
            this.orientation = this.resourcesfactory.CreateNewOrientation();
            this.color = this.resourcesfactory.CreateNewColor();
        }

        #endregion

        #region Public Methods

        public override void Dispose()
        {
            base.Dispose();

            //clears lists
            this.resourceCameraSpots.Clear();
            this.resourceWaypoints.Clear();
            this.tags.Clear();
        }

        #endregion
    }
}