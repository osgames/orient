/* ****************************************************
 * Name: ResourceSet.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.Xml;
using PAGE.Generic.Navigation;
using PAGE.Util;

namespace PAGE.Generic.Resources.Assets
{
    /// <summary>
    /// Contains the resource information for a walkable set.
    /// Includes particle system and skybox info.
    /// </summary>
    public class ResourceSet : ResourceAsset
    {
        #region Fields

        protected TokenObjectList<GenericVertex> vertexs = new TokenObjectList<GenericVertex>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the vertex list associated with the resource set.
        /// </summary>
        public TokenObjectList<GenericVertex> VertexList
        {
            get { return this.vertexs; }
            set { this.vertexs = value; }
        }

        #endregion

        #region Constructors

        public ResourceSet(string idToken, ResourcesFactory resourcesFactory)
            : base(idToken, resourcesFactory)
        {
        } 

        #endregion

        #region Serialization Methods

        public override void SaveToXml(string xmlFileName)
        {
            this.SaveToXml(xmlFileName, "set");
        }

        public override void LoadFromXml(string xmlFileName)
        {
            this.LoadFromXml(xmlFileName, "set");
        }

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            //writes vertexs
            XmlElement childElement = element.OwnerDocument.CreateElement("vertexs");
            element.AppendChild(childElement);
            foreach (GenericVertex vertex in this.vertexs)
            {
                XmlElement vertexElement = element.OwnerDocument.CreateElement("vertex");
                vertex.WriteXml(vertexElement);
                childElement.AppendChild(vertexElement);
            }
        }

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            //loads vertexs
            foreach (XmlElement spotElement in element.SelectNodes("vertexs/vertex"))
            {
                GenericVertex vertex = this.ResourcesFactory.CreateNewVertex("vertex0");
                vertex.ReadXml(spotElement);
                this.vertexs.Add(vertex);
            }
        }

        #endregion

        #region Public Methods

        public override void Dispose()
        {
            base.Dispose();

            this.vertexs.Clear();
        }

        #endregion
    }
}