/* ****************************************************
 * Name: ResourceItem.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
namespace PAGE.Generic.Resources.Assets
{
    /// <summary>
    /// Contains the base information for a world item (object).
    /// </summary>
    public class ResourceItem : ResourceAsset
    {
        #region Constructors

        public ResourceItem(string idToken, ResourcesFactory resourcesFactory)
            : base(idToken, resourcesFactory)
        {
        } 

        #endregion

        #region Public Methods

        public override void SaveToXml(string xmlFileName)
        {
            this.SaveToXml(xmlFileName, "item");
        }

        public override void LoadFromXml(string xmlFileName)
        {
            this.LoadFromXml(xmlFileName, "item");
        }

        #endregion
    }
}