/* ****************************************************
 * Name: ResourceHumanLikeCharacter.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/31 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

namespace PAGE.Generic.Resources.Assets
{
    /// <summary>
    /// Contains the resource information for a human-like character.
    /// </summary>
    public class ResourceHumanLikeCharacter : ResourceCharacter
    {
        #region Constructors

        public ResourceHumanLikeCharacter(
            string characterID, ResourcesFactory resourcesFactory)
            : base(characterID, resourcesFactory)
        {
        }

        #endregion
    }
}