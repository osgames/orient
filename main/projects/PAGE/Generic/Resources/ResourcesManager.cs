/* ****************************************************
 * Name: ResourceManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.IO;
using PAGE.Generic.Resources.Assets;
using PAGE.Util;

namespace PAGE.Generic.Resources
{
    /// <summary>
    /// Contains all the resources information to be used by a generic realizer.
    /// </summary>
    public class ResourcesManager : IDisposable
    {
        #region Fields

        protected string configFileName = "file.xml";
        protected ResourcesFactory resourcesFactory;
        protected ResourcesConfig resourcesConfig;

        protected TokenObjectList<ResourceCharacter> characters = new TokenObjectList<ResourceCharacter>();
        protected TokenObjectList<ResourceItem> items = new TokenObjectList<ResourceItem>();
        protected TokenObjectList<ResourceSet> sets = new TokenObjectList<ResourceSet>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the resources configuration file.
        /// </summary>
        public string ConfigFileName
        {
            get { return this.configFileName; }
            set { this.configFileName = value; }
        }

        /// <summary>
        /// Gets the resources configuration object associated with the manager.
        /// </summary>
        public ResourcesConfig ResourcesConfig
        {
            get { return resourcesConfig; }
        }

        /// <summary>
        /// Gets the list of resource items available in the manager.
        /// </summary>
        public TokenObjectList<ResourceItem> Items
        {
            get { return this.items; }
        }

        /// <summary>
        /// Gets the list of resource sets available in the manager.
        /// </summary>
        public TokenObjectList<ResourceSet> Sets
        {
            get { return this.sets; }
        }

        /// <summary>
        /// Gets the list of resource characters available in the manager.
        /// </summary>
        public TokenObjectList<ResourceCharacter> Characters
        {
            get { return this.characters; }
        }

        /// <summary>
        /// Get the resources factory used by this manager.
        /// </summary>
        public ResourcesFactory ResourcesFactory
        {
            get { return resourcesFactory; }
        }

        #endregion

        #region Constructors

        public ResourcesManager(
            string configFileName, ResourcesFactory resourcesFactory)
        {
            if ((configFileName == null) || (resourcesFactory == null))
            {
                throw new ArgumentException("file name and resources factory can't be null");
            }
            this.configFileName = configFileName;
            this.resourcesFactory = resourcesFactory;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Loads all the resources according to the given
        /// configuration file.
        /// </summary>
        public virtual void LoadAll()
        {
            //loads configuration
            this.resourcesConfig = this.resourcesFactory.CreateNewResourcesConfig();
            this.resourcesConfig.LoadFromXml(configFileName);

            //loads elements by correct order
            this.LoadCharacters(this.resourcesConfig.CharactersPath);
            this.LoadItems(this.resourcesConfig.ItemsPath);
            this.LoadSets(this.resourcesConfig.SetsPath);
        }

        /// <summary>
        /// Loads all the resource characters starting from the given path root.
        /// </summary>
        /// <param name="charactersPath">the path to the root folder of the 
        /// resource characters</param>
        public virtual void LoadCharacters(string charactersPath)
        {
            if (!Directory.Exists(charactersPath))
            {
                throw new ApplicationException(
                    "Could not find " + charactersPath + " directory: ");
            }

            //recursion
            foreach (string directory in Directory.GetDirectories(charactersPath))
            {
                this.LoadCharacters(directory);
            }

            //loads characters
            foreach (string fileName in Directory.GetFiles(charactersPath, "*.xml"))
            {
                ResourceCharacter character =
                    this.resourcesFactory.CreateNewCharacter("char0");
                character.LoadFromXml(fileName);

                //checks character existence in internal data base
                if (!this.characters.Contains(character.IdToken))
                {
                    this.characters.Add(character);
                }
            }
        }

        /// <summary>
        /// Loads all the resource items starting from the given path root.
        /// </summary>
        /// <param name="itemsPath">the path to the root folder of the 
        /// resource items</param>
        public virtual void LoadItems(string itemsPath)
        {
            if (!Directory.Exists(itemsPath))
            {
                throw new ApplicationException(
                    "Could not find " + itemsPath + " directory: ");
            }

            //recursion
            foreach (string directory in Directory.GetDirectories(itemsPath))
            {
                this.LoadItems(directory);
            }

            //loads items
            foreach (string fileName in Directory.GetFiles(itemsPath, "*.xml"))
            {
                ResourceItem item =
                    this.resourcesFactory.CreateNewItem("item0");
                item.LoadFromXml(fileName);

                //checks character existence in internal data base
                if (!this.items.Contains(item.IdToken))
                {
                    this.items.Add(item);
                }
            }
        }

        /// <summary>
        /// Loads all the resource sets starting from the given path root.
        /// </summary>
        /// <param name="setsPath">the path to the root folder of the 
        /// resource sets</param>
        public virtual void LoadSets(string setsPath)
        {
            if (!Directory.Exists(setsPath))
            {
                throw new ApplicationException(
                    "Could not find " + setsPath + " directory: ");
            }

            //recursion
            foreach (string directory in Directory.GetDirectories(setsPath))
            {
                this.LoadSets(directory);
            }

            //loads sets
            foreach (string fileName in Directory.GetFiles(setsPath, "*.xml"))
            {
                ResourceSet set =
                    this.resourcesFactory.CreateNewSet("set0");
                set.LoadFromXml(fileName);

                //checks character existence in internal data base
                if (!this.sets.Contains(set.IdToken))
                {
                    this.sets.Add(set);
                }
            }
        }

        /// <summary>
        /// Clears all the stored resources lists.
        /// </summary>
        public virtual void Dispose()
        {
            //clear all dbs
            this.characters.Clear();
            this.sets.Clear();
            this.items.Clear();
        }

        #endregion
    }
}