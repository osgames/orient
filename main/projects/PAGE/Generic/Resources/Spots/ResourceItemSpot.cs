/* ****************************************************
 * Name: ResourceItemSpot.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/23 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Globalization;
using System.Xml;

namespace PAGE.Generic.Resources.Spots
{
    /// <summary>
    /// Represents a resource spot that is used for item placing purposes.
    /// </summary>
    public class ResourceItemSpot : ResourceSpot
    {
        #region Constructors

        protected bool isCarrier = false;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a property that tells if the items contained in the spot
        /// should follow the item's position when it changes.
        /// </summary>
        public bool IsCarrier
        {
            get { return isCarrier; }
            set { isCarrier = value; }
        }

        #endregion

        #region Constructors

        public ResourceItemSpot(string idToken, ResourcesFactory resourcesfactory)
            : base(idToken, resourcesfactory)
        {
        }

        #endregion

        #region Serialization Methods

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            if (element.HasAttribute("carrier"))
                this.isCarrier = Convert.ToBoolean(element.GetAttribute("carrier"),
                    CultureInfo.InvariantCulture);
        }

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            element.SetAttribute("carrier", 
                Convert.ToString(this.isCarrier, CultureInfo.InvariantCulture));
        }

        #endregion
    }
}