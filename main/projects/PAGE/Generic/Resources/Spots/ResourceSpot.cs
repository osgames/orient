/* ****************************************************
 * Name: ResourceSpot.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Xml;
using PAGE.Generic.Math;
using PAGE.Util;

namespace PAGE.Generic.Resources.Spots
{
    /// <summary>
    /// Contains the base information about resource spots. Spots are points in the 
    /// 3D world, which have a certain position and direction. When attached to 
    /// spots, assets can follow the spot position if the spot is a 'carrier'. Spots
    /// have a list of tags that characterize them.
    /// </summary>
    public class ResourceSpot : TokenObject
    {
        #region Fields

        protected StringArrayList tags = new StringArrayList();
        protected ResourcesFactory resourcesfactory = null;
        protected Position relativePosition = null;
        protected Direction relativeDirection = null;

        #endregion

        #region Properties

        public override string IdToken
        {
            get { return base.IdToken; }
            set
            {
                //adds new id to tags, removes old one
                this.Tags.Remove(base.IdToken);
                base.IdToken = value;
                this.Tags.Add(value);
            }
        }

        /// <summary>
        /// Gets the string tags associated with the resource spot.
        /// </summary>
        public StringArrayList Tags
        {
            get { return this.tags; }
        }

        /// <summary>
        /// Gets or sets the spot's position relative to the asset 
        /// which contains it.
        /// </summary>
        public Position RelativePosition
        {
            get { return relativePosition; }
            set { relativePosition = value; }
        }

        /// <summary>
        /// Gets or sets the spot's direction relative to the asset which contains it.
        /// </summary>
        public Direction RelativeDirection
        {
            get { return relativeDirection; }
            set { relativeDirection = value; }
        }

        #endregion

        #region Constructors

        public ResourceSpot(string idToken, ResourcesFactory resourcesfactory)
            : base(idToken)
        {
            if ((idToken == null) || (resourcesfactory == null))
            {
                throw new ArgumentException("id and resources factory can't be null");
            }

            this.tags.Add(idToken);
            this.resourcesfactory = resourcesfactory;
            this.Initialise();
        } 

        #endregion

        #region Serialization Methods

        public override void ReadXml(XmlElement element)
        {
            base.ReadXml(element);

            //translation
            XmlElement childElement = (XmlElement)element.SelectSingleNode("position");
            if (childElement != null) this.RelativePosition.ReadXml(childElement);

            //direction
            childElement = (XmlElement)element.SelectSingleNode("direction");
            if (childElement != null) this.RelativeDirection.ReadXml(childElement);

            //loads assets's tags
            foreach (XmlElement tagElement in element.SelectNodes("tags/tag"))
            {
                string tagName = tagElement.InnerXml;
                if (tagName != "") this.Tags.Add(tagName);
            }
        }

        public override void WriteXml(XmlElement element)
        {
            base.WriteXml(element);

            //translation
            XmlElement childElement = element.OwnerDocument.CreateElement("position");
            this.RelativePosition.WriteXml(childElement);
            element.AppendChild(childElement);

            //direction
            childElement = element.OwnerDocument.CreateElement("direction");
            this.RelativeDirection.WriteXml(childElement);
            element.AppendChild(childElement);

            //writes tags
            childElement = element.OwnerDocument.CreateElement("tags");
            element.AppendChild(childElement);
            foreach (string tag in this.Tags)
            {
                XmlElement tagElement = element.OwnerDocument.CreateElement("tag");
                tagElement.InnerXml = tag.ToLower();
                childElement.AppendChild(tagElement);
            }
        }

        #endregion

        #region Protected Methods

        protected void Initialise()
        {
            this.relativePosition  = this.resourcesfactory.CreateNewPosition();
            this.relativeDirection = this.resourcesfactory.CreateNewDirection();
        }

        #endregion

        #region Public Methods

        public override void Dispose()
        {
            base.Dispose();

            this.tags.Clear();
        }

        #endregion
    }
}