/* ****************************************************
 * Name: ResourceWaypoint.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

namespace PAGE.Generic.Resources.Spots
{
    /// <summary>
    /// Represents a resource spot that is used for scene navigation purposes.
    /// </summary>
    public class ResourceWaypoint : ResourceSpot
    {
        #region Constructors

        public ResourceWaypoint(string idToken, ResourcesFactory resourcesfactory)
            : base(idToken, resourcesfactory)
        {
        } 

        #endregion
    }
}