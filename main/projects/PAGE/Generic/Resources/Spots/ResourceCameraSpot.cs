/* ****************************************************
 * Name: ResourceCameraSpot.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

namespace PAGE.Generic.Resources.Spots
{
    /// <summary>
    /// Represents a resource spot where cameras will be placed on.
    /// </summary>
    public class ResourceCameraSpot : ResourceSpot
    {
        #region Constructors

        public ResourceCameraSpot(string idToken, ResourcesFactory resourcesfactory)
            : base(idToken, resourcesfactory)
        {
        } 

        #endregion
    }
}