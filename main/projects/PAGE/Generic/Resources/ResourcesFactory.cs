/* ****************************************************
 * Name: IResourcesFactory.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/06/23
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Generic.Math;
using PAGE.Generic.Navigation;
using PAGE.Generic.Resources.Actions;
using PAGE.Generic.Resources.Assets;
using PAGE.Generic.Resources.Spots;

namespace PAGE.Generic.Resources
{
    public abstract class ResourcesFactory
    {
        #region Public Methods

        public virtual ResourceCameraSpot CreateNewCameraSpot(string spotID)
        {
            return new ResourceCameraSpot(spotID, this);
        }

        public virtual ResourceWaypoint CreateNewWaypoint(string spotID)
        {
            return new ResourceWaypoint(spotID, this);
        }

        public virtual ResourceItemSpot CreateNewItemSpot(string spotID)
        {
            return new ResourceItemSpot(spotID, this);
        }

        public virtual ResourceCharacter CreateNewCharacter(string characterID)
        {
            return new ResourceHumanLikeCharacter(characterID, this);
        }

        public virtual ResourceItem CreateNewItem(string itemID)
        {
            return new ResourceItem(itemID, this);
        }

        public virtual ResourceSet CreateNewSet(string setID)
        {
            return new ResourceSet(setID, this);
        }

        public virtual GenericVertex CreateNewVertex(string vertexID)
        {
            return new GenericVertex(vertexID, this);
        }

        public virtual ResourcesConfig CreateNewResourcesConfig()
        {
            return new ResourcesConfig();
        }

        public virtual ActionList CreateNewActionList()
        {
            return new ActionList();
        }

        public virtual VertexCostPair CreateNewVertexCostPair()
        {
            return new VertexCostPair();
        }

        public abstract Scale CreateNewScale();
        public abstract Position CreateNewPosition();
        public abstract Direction CreateNewDirection();
        public abstract Orientation CreateNewOrientation();
        public abstract Color CreateNewColor();

        #endregion
    }
}
