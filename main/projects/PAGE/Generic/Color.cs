/* ****************************************************
 * Name: Color.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/30 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Xml;

namespace PAGE.Generic
{
    /// <summary>
    /// Represents a color in some color space.
    /// </summary>
    public abstract class Color : IXmlSerializable
    {
        public abstract void ReadXml(XmlElement element);
        public abstract void WriteXml(XmlElement element);
    }
}