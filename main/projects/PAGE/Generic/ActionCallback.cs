/* ****************************************************
 * Name: ActionCallback.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Generic.Resources.Actions;

namespace PAGE.Generic
{
    /// <summary>
    /// Represents a callback function so that success, failure or trigger events are
    /// received during action executions. If ignoreMultipleResponses is true, the
    /// callback procedures will be called only once (one Success or one Failure response).
    /// </summary>
    public abstract class ActionCallback : IActionCallback, IResetable
    {
        #region Fields

        protected int successCallsNumber;
        protected int failureCallsNumber;
        protected int triggerCallsNumber;
        protected bool ignoreMultipleResponses = true;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the number of total calls (all methods) made to the callback.
        /// </summary>
        public int TotalCallsNumber
        {
            get { return this.successCallsNumber + this.failureCallsNumber + this.triggerCallsNumber; }
        }

        /// <summary>
        /// Gets the number of success calls made to the callback
        /// </summary>
        public int SuccessCallsNumber
        {
            get { return successCallsNumber; }
        }

        /// <summary>
        /// Gets the number of failure calls made to the callback
        /// </summary>
        public int FailureCallsNumber
        {
            get { return failureCallsNumber; }
        }

        /// <summary>
        /// Gets the number of trigger calls made to the callback
        /// </summary>
        public int TriggerCallsNumber
        {
            get { return triggerCallsNumber; }
        }

        #endregion

        #region Constructor

        public ActionCallback() : this(true)
        {
        }

        public ActionCallback(bool ignoreMultipleResponses)
        {
            this.ignoreMultipleResponses = ignoreMultipleResponses;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Called when the action somehow succeeded.
        /// </summary>
        public virtual void SendSuccess()
        {
            if (!this.ignoreMultipleResponses ||
                ((this.successCallsNumber == 0) && (this.failureCallsNumber == 0)))
            {
                //sends success
                this.successCallsNumber++;
                this.Succeeded();
            }
        }

        /// <summary>
        /// Called when the action somehow failed.
        /// </summary>
        public virtual void SendFailure()
        {
            if (!this.ignoreMultipleResponses ||
                ((this.successCallsNumber == 0) && (this.failureCallsNumber == 0)))
            {
                //sends success
                this.failureCallsNumber++;
                this.Failed();
            }
        }

        /// <summary>
        /// Called when an action event was triggered during the action execution.
        /// </summary>
        public virtual void SendTrigger(ActionTrigger trigger)
        {
            //action triggers are always triggered
            this.triggerCallsNumber++;
            this.Triggered(trigger);
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Treats the action success event.
        /// </summary>
        protected abstract void Succeeded();

        /// <summary>
        /// Treats the action failure event.
        /// </summary>
        protected abstract void Failed();

        /// <summary>
        /// Treats the action trigger event.
        /// </summary>
        protected abstract void Triggered(ActionTrigger trigger);

        #endregion

        #region Public Methods

        public virtual void Reset()
        {
            this.successCallsNumber = 0;
            this.failureCallsNumber = 0;
            this.triggerCallsNumber = 0;
        }

        #endregion
    }
}