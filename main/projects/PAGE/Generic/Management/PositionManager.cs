/* ****************************************************
 * Name: PositionManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/09/16 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Math;

namespace PAGE.Generic.Management
{
    /// <summary>
    /// Represents a manager for a specific feature or resource of an asset.
    /// </summary>
    public abstract class PositionManager : AssetManager
    {
        #region Fields

        protected Position targetPosition;
        protected Distance distancePerMilli;
        protected Direction directionToTarget;
        protected float totalTimeLeft = -1;

        #endregion

        #region Constructors

        public PositionManager(IInteractionAsset asset)
            : base(asset)
        {
            this.asset = asset;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Changes the current position of the asset through out the given time.
        /// </summary>
        /// <param name="newPosition">the new position to place the asset.</param>
        /// <param name="transitionTime">the time in seconds to perform the transition
        /// between positions.</param>
        public void ChangePosition(Position newPosition, float transitionTime)
        {
            this.totalTimeLeft = transitionTime*1000;

            this.targetPosition = newPosition;
            this.directionToTarget =
                this.asset.AbsolutePosition.DirectionTo(newPosition);
            this.distancePerMilli =
                this.asset.AbsolutePosition.DistanceTo(newPosition).
                    Divide((int) (transitionTime*1000));
        }

        public override void Dispose()
        {
        }

        /// <summary>
        /// Cancels the current movement.
        /// </summary>
        public abstract void Stop();

        #endregion
    }
}