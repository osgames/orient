/* ****************************************************
 * Name: TextManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/17 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Domain.Assets;
using PAGE.Generic.GUI;

namespace PAGE.Generic.Management
{
    /// <summary>
    /// Text managers are responsible to manage the texts associated with assets.
    /// It includes a follow asset text.
    /// </summary>
    public abstract class TextManager : AssetManager
    {
        #region Fields

        protected IFollowAssetText mainText;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the text associated with the manager.
        /// </summary>
        public IFollowAssetText MainText
        {
            get { return mainText; }
        }

        /// <summary>
        /// Shows or hides the text associated with the manager
        /// </summary>
        public virtual bool Visible
        {
            set { this.mainText.Visible = value; }
        }

        #endregion

        #region Constructor

        public TextManager(IInteractionAsset character)
            : base(character)
        {
        }

        #endregion

        #region Public Methods

        public override void Update(float millisSinceLastUpdate)
        {
            if (this.mainText != null) this.mainText.Update(millisSinceLastUpdate);
        }

        public virtual void Stop()
        {
            this.mainText.Text = "";
        }

        #endregion
    }
}