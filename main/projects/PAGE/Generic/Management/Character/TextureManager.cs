/* ****************************************************
 * Name: TextureManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/09 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using PAGE.Generic.Domain.Assets;

namespace PAGE.Generic.Management.Character
{
    /// <summary>
    /// Texture managers are responsible for managing the visual representation of
    /// characters and provide methods for changing certain textures at any time.
    /// </summary>
    public abstract class TextureManager : CharacterManager
    {
        #region Constructors

        public TextureManager(ITexturedCharacter character)
            : base(character)
        {
        } 

        #endregion
    }
}