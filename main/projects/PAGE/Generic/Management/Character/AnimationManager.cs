/* ****************************************************
 * Name: AnimationManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/02/09 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Resources.Actions;

namespace PAGE.Generic.Management.Character
{
    public enum BlendingTransition
    {
        // stop source and start dest 
        BlendSwitch,

        // cross fade, blend source animation out while blending destination animation in 
        BlendWhileAnimating,

        // blend source to first frame of dest, when done, start dest anim 
        BlendThenAnimate
    }

    /// <summary>
    /// Animation managers are responsible to manage the animations for a certain 
    /// character.
    /// </summary>
    public abstract class AnimationManager : CharacterManager
    {
        #region Fields

        protected bool blendCurrent = true;
        protected float animationDuration, animationTimeLeft;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the duration in seconds of the current animation.
        /// </summary>
        public float AnimationDuration
        {
            get { return animationDuration; }
        }

        /// <summary>
        /// Gets the duration in seconds of the animation time left to play.
        /// </summary>
        public float AnimationTimeLeft
        {
            get { return animationTimeLeft; }
        }

        #endregion

        #region Constructor

        public AnimationManager(IAnimatableCharacter character)
            : base(character)
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Activates the given animation at a given speed by blending it with the 
        /// current one being displayed and having into account a given thansition 
        /// duration.
        /// </summary>
        /// <param name="animationName">the name of the new animation to activate.</param>
        /// <param name="animationSpeed">the speed to display the new animation.</param>
        /// <param name="transition">the type of transition between the animations.</param>
        /// <param name="duration">the duration of the anmation transition.</param>
        public void BlendAnimation(string animationName, float animationSpeed,
                          BlendingTransition transition, float duration)
        {
            this.BlendAnimation(animationName, animationSpeed, transition, duration, false, true);
        }

        /// <summary>
        /// Activates the given animation at a given speed by blending it with the 
        /// current one being displayed and having into account a given thansition 
        /// duration.
        /// </summary>
        /// <param name="animationName">the name of the new animation to activate.</param>
        /// <param name="animSpeed">the speed to display the new animation.</param>
        /// <param name="animTransition">the type of transition between the animations.</param>
        /// <param name="animDuration">the duration of the animation transition.</param>
        /// <param name="randomStartTime">if set to true, the new animation will start 
        /// at a random time position.</param>
        /// <param name="animLoop">if set to true, animation will loop until stop order.</param>
        public abstract void BlendAnimation(string animationName, float animSpeed,
            BlendingTransition animTransition, float animDuration, bool randomStartTime, bool animLoop);

        public override void ExecuteAction(IActionCallback actionCallback, string actionName)
        {
            this.ExecuteAction(actionCallback, actionName, false, true);
        }

        /// <summary>
        /// Loads the action resource with the given action name id. If there is no 
        /// action store with the given name, null will be set in property Action.
        /// </summary>
        /// <param name="actionCallback">the callback to which respond action success,
        /// failure or triggers.</param>
        /// <param name="actionName">the name of the resource action to load.</param>
        /// <param name="randomStartTime">if set to true, the animation to load will 
        /// start at a random time position.</param>
        /// <param name="loop">if set to true, animation will loop until stop order.</param>
        public void ExecuteAction(
            IActionCallback actionCallback, string actionName, bool randomStartTime, bool loop)
        {
            base.ExecuteAction(actionCallback, actionName);

            //if there is no action available, return Failure
            if(this.action == null || !(this.action is AnimationAction))
            {
                this.ActionFailure();
                return;
            }

            AnimationAction animAction = this.action as AnimationAction;

            //initialises animation manager
            this.BlendAnimation(animAction.AnimationName, animAction.AnimationSpeed,
                animAction.Blend && this.blendCurrent ?
                BlendingTransition.BlendThenAnimate :
                BlendingTransition.BlendSwitch,
                animAction.Blend && this.blendCurrent ? 0.2f : 0f, randomStartTime, loop);
        }

        /// <summary>
        /// Gets the duration of the given animation.
        /// </summary>
        /// <param name="animationName">the name of the animation to get the duration.</param>
        /// <returns>The duration of the animation with the given name.</returns>
        public abstract float GetAnimationDuration(string animationName);
        #endregion
    }
}
