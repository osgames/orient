/* ****************************************************
 * Name: ActionTriggersManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/03 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Collections.Generic;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Resources.Actions;

namespace PAGE.Generic.Management.Character
{
    /// <summary>
    /// Manages all the actions executed by the character, being responsible for the
    /// management of the triggering of action events.
    /// </summary>
    public class ActionTriggersManager : CharacterManager
    {
        #region Fields

        //current list of triggers to fire
        protected List<float> triggerTimes = null;

        //current action execution time
        protected TimeSpan curTime = TimeSpan.Zero;

        //is the manager stopped
        protected bool stopped = true;

        #endregion

        #region Constructors

        public ActionTriggersManager(ICharacter character)
            : base(character)
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// If the manager is not stopped, checks the triggers that need to be signaled 
        /// according to the time thas has passed.
        /// </summary>
        /// <param name="millisSinceLastUpdate">the time in milliseconds since the last
        /// update call.</param>
        public override void Update(float millisSinceLastUpdate)
        {
            base.Update(millisSinceLastUpdate);

            if (this.stopped) return;

            this.curTime += TimeSpan.FromMilliseconds(millisSinceLastUpdate);

            //traverses ordered list to find all passed triggers
            foreach (float time in new List<float>(this.triggerTimes))
            {
                if (TimeSpan.FromSeconds(time) > this.curTime)
                {
                    //if time element is in the future, nothing more will be triggered
                    break;
                }

                //triggers new event
                ActionTrigger trigger = this.Action.Triggers[time];
                if (trigger != null)
                {
                    this.ActionTrigger(trigger);
                    this.triggerTimes.Remove(time);     //removes trigger from list
                }
            }

            //no more triggers, stop manager
            if (this.triggerTimes.Count == 0) this.stopped = true;
        }

        public override void ExecuteAction(IActionCallback actionCallback, string actionName)
        {
            base.ExecuteAction(actionCallback, actionName);
            this.LoadTriggers();
        }

        /// <summary>
        /// Stops the manager. This way no more triggers will be fired.
        /// </summary>
        public override void Stop()
        {
            this.stopped = true;
        }

        /// <summary>
        /// Clears the managers' trigger list.
        /// </summary>
        public override void Dispose()
        {
            if(this.triggerTimes != null) this.triggerTimes.Clear();
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Loads the list of triggers to fire during action execution.
        /// </summary>
        protected void LoadTriggers()
        {
            this.curTime = TimeSpan.Zero;
            if (this.Action != null)
            {
                this.triggerTimes = this.Action.Triggers.Times;
                this.stopped = false;
            }
            else
            {
                this.stopped = true;
            }
        }

        /// <summary>
        /// Signals the current hooked-up callback with the given action trigger.
        /// </summary>
        /// <param name="trigger">the action trigger info to signal to the 
        /// callback.</param>
        protected void ActionTrigger(ActionTrigger trigger)
        {
            //if there is a callback pending
            if (this.callback != null)
            {
                //signals that callback with a trigger
                this.callback.SendTrigger(trigger);
            }
        }

        protected override void ActionFailure()
        {
            //this is just a trigger class
        }

        protected override void ActionSuccess()
        {
            //this is just a trigger class
        }

        #endregion
    }
}