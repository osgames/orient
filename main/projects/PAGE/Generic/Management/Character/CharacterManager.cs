/* ****************************************************
 * Name: CharacterManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/09 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Resources.Actions;

namespace PAGE.Generic.Management.Character
{
    /// <summary>
    /// Represents an object to manage a character resources and features.
    /// </summary>
    public abstract class CharacterManager : AssetManager
    {
        #region Fields

        protected const string SUCCESS_TAG = "success";
        protected const string FAILURE_TAG = "failure";

        //current resource action
        protected ResourceAction action;

        //callback associated with this manager
        protected IActionCallback callback;

        //pending callback associated with this manager
        protected IActionCallback pendingCallback;
        protected string resposeType = FAILURE_TAG;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the character associated with this manager.
        /// </summary>
        protected ICharacter Character
        {
            get { return this.asset as ICharacter; }
        }

        /// <summary>
        /// Gets the current action that was loaded with ExecuteAction.
        /// </summary>
        public ResourceAction Action
        {
            get { return action; }
        }

        #endregion

        #region Constructors

        public CharacterManager(ICharacter character)
            : base(character)
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Loads the action resource with the given action name id. If there is no 
        /// action store with the given name, null will be set in property Action.
        /// </summary>
        /// <param name="actionName">the name of the resource action to load.</param>
        /// <param name="actionCallback">the callback to which respond action success,
        /// failure or triggers.</param>
        public virtual void ExecuteAction(IActionCallback actionCallback, string actionName)
        {
            this.action = this.Character.ResourceAsset.Actions[actionName.ToLower()];

            //don't leave un-answered callbacks
            if (this.callback != null) 
                this.ActionFailure();
            this.callback = actionCallback;
        }

        /// <summary>
        /// Stops the manager by cancelling the execution of current managing procedures.
        /// </summary>
        public abstract void Stop();

        public override void Update(float millisSinceLastUpdate)
        {
            //responds to pending callback
            if (this.pendingCallback != null)
            {
                IActionCallback respondToCallback = this.pendingCallback;
                this.pendingCallback = null;

                if (this.resposeType == SUCCESS_TAG)
                    respondToCallback.SendSuccess();
                else
                    respondToCallback.SendFailure();
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Sends a succes response to the current hooked-up callback.
        /// </summary>
        protected virtual void ActionSuccess()
        {
            //if there is a callback pending
            if ((this.callback != null) && (this.pendingCallback == null))
            {
                this.pendingCallback = this.callback;
                this.resposeType = SUCCESS_TAG;
            }
        }

        /// <summary>
        /// Sends a failure response to the current hooked-up callback.
        /// </summary>
        protected virtual void ActionFailure()
        {
            //if there is a callback pending
            if ((this.callback != null) && (this.pendingCallback == null))
            {
                this.pendingCallback = this.callback;
                this.resposeType = FAILURE_TAG;
            }
        }

        #endregion
    }
}