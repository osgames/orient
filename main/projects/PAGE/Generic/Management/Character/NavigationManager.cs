/* ****************************************************
 * Name: NavigationManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/01/11 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Collections.Generic;
using PAGE.Generic.Domain;
using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Domain.Spots;
using PAGE.Generic.Math;
using PAGE.Generic.Navigation;
using PAGE.Generic.Resources.Actions;
using PAGE.Util;

namespace PAGE.Generic.Management.Character
{
    public abstract class NavigationManager : CharacterManager
    {
        #region Fields

        // the moving speed 
        protected float moveSpeed = 1.0f;

        // the distance to move the object 
        protected Distance moveDistance = Distance.ZeroValue;

        //The distance the object has left to travel
        protected Distance distance = Distance.ZeroValue;

        // whether or not the object is moving
        protected bool moving;

        // the destination the character is moving towards
        protected Position currentDestination;

        // the info about the final destination the character wants to move to
        protected List<IGenericSpot> possibleSetWaypoints = new List<IGenericSpot>();
        protected List<IGenericSpot> targetSpots;
        protected IInteractionAsset finalSpotContainer;
        protected string targetTag = "";

        #endregion

        #region Properties

        public new IMovableCharacter Character
        {
            get { return base.Character as IMovableCharacter; }
        }

        /// <summary>
        /// Gets or sets the speed factor of the character movement.
        /// </summary>
        public float MoveSpeed
        {
            get { return moveSpeed; }
            set { moveSpeed = value; }
        }

        #endregion

        #region Constructors

        public NavigationManager(IMovableCharacter character)
            : base(character)
        {
            //creates a list containing all the set waypoints for path search
            if (this.Character.Set != null)
            {
                foreach (GenericWaypoint waypoint in this.Character.Set.Waypoints)
                    this.possibleSetWaypoints.Add(waypoint);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Makes the character move to the closest spot of the given asset.
        /// </summary>
        /// <param name="targetSpotContainer">the asset that contains the spot to 
        /// move to.</param>
        public void GoTo(IInteractionAsset targetSpotContainer)
        {
            //stores target + possible target spots, 
            //takes into account all possible spots
            this.finalSpotContainer = targetSpotContainer;
            this.targetSpots = new List<IGenericSpot>();
            foreach (GenericWaypoint waypoint in targetSpotContainer.Waypoints)
                this.targetSpots.Add(waypoint);
            this.ReCheckPath();
        }

        /// <summary>
        /// Makes the character move to the closest spot of the given asset that has 
        /// the given tag associated with it.
        /// </summary>
        /// <param name="targetSpotContainer">the asset that contains the spot to 
        /// move to.</param>
        /// <param name="spotTag">the tag of the spot to move to.</param>
        public void GoTo(IInteractionAsset targetSpotContainer, string spotTag)
        {
            //stores target + possible target spots, 
            //takes into acount only the tagged spots
            this.finalSpotContainer = targetSpotContainer;
            this.targetSpots = new List<IGenericSpot>();
            foreach (GenericWaypoint waypoint in targetSpotContainer.Waypoints[spotTag])
                this.targetSpots.Add(waypoint);
            this.ReCheckPath();
        }

        /// <summary>
        /// Stops the character's current movement and cancels the destination.
        /// </summary>
        public override void Stop()
        {
            this.currentDestination = this.Character.AbsolutePosition;
            if (this.targetSpots != null) this.targetSpots.Clear();
            else this.targetSpots = null;
            this.targetTag = null;
            this.moving = false;

            //don't leave un-answered callbacks
            if (this.callback != null) base.ActionFailure();
            this.callback = null;
        }

        /// <summary>
        /// Updates the character's movement and checs if the path to target needs to
        /// be re-calculated.
        /// </summary>
        /// <param name="millisSinceLastUpdate">the number of milliseconds that occured 
        /// since the last update call.</param>
        public override void Update(float millisSinceLastUpdate)
        {
            base.Update(millisSinceLastUpdate);

            //checks target destroyed
            if ((this.Character.Set == null) || 
                (this.finalSpotContainer == null) || 
                (this.finalSpotContainer.Destroyed))
            {
                return; //do nothing
            }

            //the character is moving
            if (this.moving)
            {
                //update movement
                this.UpdateCharacterMovement(millisSinceLastUpdate);

                //recheck current path to destination
                this.ReCheckPath();
            }
        }

        public virtual void ExecuteAction(
            IActionCallback actionCallback, string actionName,
            IInteractionAsset targetSpotContainer, string spotTag)
        {
            this.Stop();

            base.ExecuteAction(actionCallback, actionName);

            //if there is no action available, return Failure
            if (this.action == null || !(this.action is MoveToAction))
            {
                this.ActionFailure();
                return;
            }

            if ((spotTag == null) || (spotTag == ""))
                this.GoTo(targetSpotContainer);
            else
                this.GoTo(targetSpotContainer, spotTag);
        }

        /// <summary>
        /// Clears the the current target spots list.
        /// </summary>
        public override void Dispose()
        {
            if (this.targetSpots != null) this.targetSpots.Clear();
        }

        #endregion

        #region Private Methods

        protected virtual bool IsInPosition(Position targetPosition)
        {
            return this.Character.AbsolutePosition.Equals(targetPosition);
        }

        /// <summary>
        /// Checks if there is a new destination spot for the character to move to,
        /// in which case the character is set in motion.
        /// </summary>
        protected virtual void ReCheckPath()
        {
            //checks target destroyed
            if (this.finalSpotContainer.Destroyed)
            {
                this.finalSpotContainer = null;
                if (this.targetSpots != null) this.targetSpots.Clear();
                else this.targetSpots = null;
                return;
            }

            //checks new destination
            if (this.NewDestination())
            {
                //destination changed
                this.moving = true; //we're moving,
                this.Character.Unplace(); //unplace character,
                this.UpdateCharacterDirection(); //update character direction

                //if the character is moving we must clean its waypoint spots
                this.Character.ClearNonCarrierSpots();
            }

            //no change, do nothing
        }

        /// <summary>
        /// Checks if there is a new destination spot for the character to move to.
        /// </summary>
        /// <returns>true if the navigation algorithm found a new waypoint to move to,
        /// false if the current destination is still the best one.</returns>
        protected virtual bool NewDestination()
        {
            //verifies target existence, else Failure
            if ((this.targetSpots == null) ||
                (this.targetSpots.Count == 0))
            {
                this.ActionFailure();
                return false;
            }

            //if we're already in a target position, Success
            if (this.targetSpots.Contains(this.Character.CurrentSpot))
            {
                this.ActionSuccess();
                return false;
            }

            //creates a list to ignore character body during open path calculation
            List<IGraphicalElement> characterList = new List<IGraphicalElement>();
            characterList.Add(this.Character);

            //finds nearest target empty spots relative to character
            Position characterPosition = this.Character.AbsolutePosition;
            List<IGenericSpot> orderedTargetSpots =
                Domain.Spots.Util.OrderClosestEmptySpots(characterPosition, this.targetSpots);

            //for each target empty spot,
            foreach (GenericWaypoint finalTargetSpot in orderedTargetSpots)
            {
                GenericWaypoint targetWaypoint = null;

                //target absolute position
                Position targetPosition = finalTargetSpot.AbsolutePosition;

                //if we're in final position, Success
                if (this.IsInPosition(targetPosition))
                {
                    this.Character.CurrentSpot = finalTargetSpot; //sets current spot
                    this.ActionSuccess();
                    return false;
                }

                //if not, checks open path between character and target
                if (this.Character.Set.OpenPath(
                    characterPosition, targetPosition, characterList, true))
                {
                    //if so, just go to target
                    if (this.currentDestination.Equals(targetPosition))
                    {
                        return false; //not a new destination
                    }
                    this.currentDestination = targetPosition;
                    return true;
                }

                //if not, tries to go to closest waypoint to target
                 if (this.Character.Set == this.finalSpotContainer)
                {
                    //target spot is already a waypoint in set
                    targetWaypoint = finalTargetSpot;
                }
                else
                {
                    //gets the possible target waypoints ordered ascendingly by distance to target
                    List<IGenericSpot> possibleTargetWaypoints =
                        Domain.Spots.Util.OrderClosestEmptySpots(targetPosition, this.possibleSetWaypoints);

                    foreach (GenericWaypoint possibleTargetWaypoint in possibleTargetWaypoints)
                    {
                        //if there is an open path to target waypoint and character is not already there
                        Position waypointPos = possibleTargetWaypoint.AbsolutePosition;
                        if (possibleTargetWaypoint.IsEmpty &&
                            !this.IsInPosition(waypointPos) &&
                            this.Character.Set.OpenPath(waypointPos, targetPosition,
                                                        characterList, true))
                        {
                            //we'll try this waypoint
                            targetWaypoint = possibleTargetWaypoint;
                            break;
                        }
                        //else tries next waypoint
                    }
                }

                //no connection to the target empty spot, try next
                if (targetWaypoint == null) continue;

                Position possibleNextDestination = targetWaypoint.AbsolutePosition;

                //tries to go directly to target waypoint
                if (this.Character.Set.OpenPath(
                    characterPosition, possibleNextDestination, characterList, true))
                {
                    if (this.currentDestination.Equals(possibleNextDestination))
                    {
                        return false; //not a new destination
                    }
                    this.currentDestination = possibleNextDestination; //new destination
                    return true;
                }

                //gets the possible source waypoints ordered ascendingly by distance to character
                List<IGenericSpot> possibleSourceWaypoints =
                    Domain.Spots.Util.OrderClosestEmptySpots(characterPosition, this.possibleSetWaypoints);

                //tries to get a possible source waypoint
                foreach (GenericWaypoint sourceWaypoint in possibleSourceWaypoints)
                {
                    //we've tested and there is no open path to targetWaypoint...
                    if (sourceWaypoint == targetWaypoint) continue;

                    //if there is not an open path to this waypoint 
                    //or waypoint is not empty or character is already there
                    if (!sourceWaypoint.IsEmpty ||
                        characterPosition.Equals(sourceWaypoint.AbsolutePosition) ||
                        !this.Character.Set.OpenPath(
                             characterPosition, sourceWaypoint.AbsolutePosition,
                             characterList, true))
                    {
                        continue; //tries next waypoint
                    }

                    //Makes A-Star search from source to target
                    AStarSearch search = new AStarSearch(this.Character.Set);
                    StringArrayList bestPath = search.GetPath(
                        sourceWaypoint.ResourceSpot.IdToken,
                        targetWaypoint.ResourceSpot.IdToken,
                        characterList);

                    //verifies path existence
                    if (bestPath == null) continue; //tries next waypoint

                    //tries to go directly to closest point to target in path 
                    bool nextDestinationExists = false;
                    foreach (string closestWaypointID in bestPath)
                    {
                        if (closestWaypointID == sourceWaypoint.ResourceSpot.IdToken)
                        {
                            //we've already seen that source is reachable, so go there
                            possibleNextDestination = sourceWaypoint.AbsolutePosition;
                            nextDestinationExists = true;
                            break;
                        }

                        //gets closest waypoint from set
                        GenericWaypoint closestWaypoint =
                            this.Character.Set.Waypoints[closestWaypointID][0];
                        possibleNextDestination = closestWaypoint.AbsolutePosition;

                        //if there is an open path to the closest point in the current best path
                        if (this.Character.Set.OpenPath(
                            characterPosition, possibleNextDestination, characterList, true))
                        {
                            //next destination was found
                            nextDestinationExists = true;
                            break;
                        }
                    }

                    //if there is a new destination
                    if (nextDestinationExists)
                    {
                        if (this.currentDestination.Equals(possibleNextDestination))
                        {
                            return false; //not a new destination
                        }
                        this.currentDestination = possibleNextDestination;
                        return true;
                    }

                    //else keeps trying next source waypoint
                }
            }

            //if all target spots were occupied, Failure
            //if there is no target waypoint, Failure
            //at this point, all source waypoints have been tested
            //so there is no possible path to target, Failure
            this.ActionFailure();
            return false;
        }

        protected override void ActionFailure()
        {
            base.ActionFailure();
            this.Stop();
        }

        protected override void ActionSuccess()
        {
            base.ActionSuccess();
            this.Stop();
        }

        #endregion

        #region Abstract Methods

        /// <summary>
        /// Updates the character movement based in the current movement variables.
        /// </summary>
        /// <param name="millisSinceLastUpdate">the amount of time to update the
        /// movement.</param>
        protected abstract void UpdateCharacterMovement(float millisSinceLastUpdate);

        /// <summary>
        /// Updates the character's direction based on its current target and movement.
        /// </summary>
        protected abstract void UpdateCharacterDirection();

        #endregion
    }
}