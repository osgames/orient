/* ****************************************************
 * Name: SpeechManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/02/09 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Domain.Assets;

namespace PAGE.Generic.Management.Character
{
    /// <summary>
    /// Speech managers are responsible to manage the animations and procedures related 
    /// to speech. Manages the way the speech is reproduced in the characters, the sounds
    /// it makes, etc.
    /// </summary>
    public abstract class SpeechManager : CharacterManager
    {
        #region Fields

        protected bool isTalking;
        protected float speechDuration, speechTimeLeft;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the duration in seconds of the current speech.
        /// </summary>
        public float SpeechDuration
        {
            get { return speechDuration; }
        }

        /// <summary>
        /// Gets the duration in seconds of the speech time left to play.
        /// </summary>
        public float SpeechTimeLeft
        {
            get { return speechTimeLeft; }
        }

        #endregion

        #region Constructors

        public SpeechManager(ISpeakingCharacter character)
            : base(character)
        {
        } 

        #endregion

        #region Public Methods

        public override void ExecuteAction(IActionCallback actionCallback, string actionName)
        {
            this.Stop();
            base.ExecuteAction(actionCallback, actionName);
        }

        public virtual void ExecuteAction(
            IActionCallback actionCallback, string actionName, string utterance)
        {
            this.ExecuteAction(actionCallback, actionName);
            this.StartSpeech(utterance);
        }

        /// <summary>
        /// Activates the action of speak for the character, representing the saying of 
        /// the given utterance in some manner.
        /// </summary>
        /// <param name="utterance">the utterance for the character to speak.</param>
        public abstract void StartSpeech(string utterance);

        public override void Stop()
        {
            this.isTalking = false;

            //don't leave un-answered callbacks
            if (this.callback != null) this.ActionFailure();
            this.callback = null;
        }

        #endregion
    }
}