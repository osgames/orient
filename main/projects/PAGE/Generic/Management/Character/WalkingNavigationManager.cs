/* ****************************************************
 * Name: WalkingNavigationManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/04 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using PAGE.Generic.Domain.Assets;
using PAGE.Generic.Math;
using PAGE.Generic.Resources.Actions;

namespace PAGE.Generic.Management.Character
{
    public abstract class WalkingNavigationManager : NavigationManager
    {
        #region Fields

        // the distance of an object step 
        protected Distance walkStepDist = Distance.ZeroValue;

        #endregion

        #region Properties

        public new IWalkingCharacter Character
        {
            get { return base.Character as IWalkingCharacter; }
        }

        /// <summary>
        /// Gets or sets the distance of one step of movement.
        /// </summary>
        public Distance WalkStepDist
        {
            get { return this.walkStepDist; }
            set { this.walkStepDist = value; }
        }

        #endregion

        #region Constructors

        public WalkingNavigationManager(IWalkingCharacter character)
            : base(character)
        {
        } 

        #endregion

        public override void ExecuteAction(IActionCallback actionCallback, string actionName, IInteractionAsset targetSpotContainer, string spotTag)
        {
            base.ExecuteAction(actionCallback, actionName, targetSpotContainer, spotTag);

            //if there is no walk action available, return Failure
            if (this.action is MoveToAction && !(this.action is WalkToAction))
            {
                this.ActionFailure();
                return;
            }

            WalkToAction walkToAction = this.action as WalkToAction;
            this.WalkStepDist = walkToAction.WalkStepDist;
        }
    }
}