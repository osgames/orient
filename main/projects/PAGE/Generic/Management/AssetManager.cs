/* ****************************************************
 * Name: AssetManager.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/02/09 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using PAGE.Generic.Domain.Assets;

namespace PAGE.Generic.Management
{
    /// <summary>
    /// Represents a manager for a specific feature or resource of an asset.
    /// </summary>
    public abstract class AssetManager : IUpdatable, IDisposable
    {
        #region Fields

        protected IInteractionAsset asset;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the asset that this manager manages.
        /// </summary>
        public IInteractionAsset Asset
        {
            get { return asset; }
        }

        #endregion

        #region Constructors

        public AssetManager(IInteractionAsset asset)
        {
            this.asset = asset;
        }

        #endregion

        #region Abstract Methods

        public abstract void Update(float millisSinceLastUpdate);

        public abstract void Dispose();

        #endregion
    }
}