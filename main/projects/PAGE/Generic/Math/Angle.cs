/* ****************************************************
 * Name: Angle.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Xml;

namespace PAGE.Generic.Math
{
    /// <summary>
    /// Represents an angle in some unit system.
    /// </summary>
    public abstract class Angle : IComparable, IXmlSerializable
    {
        public abstract int CompareTo(object obj);
        public abstract void ReadXml(XmlElement element);
        public abstract void WriteXml(XmlElement element);
    }
}