/* ****************************************************
 * Name: IOrientation.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/28 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Xml;

namespace PAGE.Generic.Math
{
    /// <summary>
    /// Represents an orientation in some space.
    /// </summary>
    public abstract class Orientation : IXmlSerializable
    {
        /// <summary>
        /// Rotates the given direction according to the current orientation.
        /// </summary>
        /// <param name="direction">the direction to rotate.</param>
        /// <returns>The given direction rotated according to the current 
        /// orientation.</returns>
        public abstract Direction Orientate(Direction direction);

        /// <summary>
        /// Rotates the given position according to the current orientation.
        /// </summary>
        /// <param name="position">the position to rotate.</param>
        /// <returns>The given position rotated according to the current 
        /// orientation.</returns>
        public abstract Position Orientate(Position position);

        /// <summary>
        /// Adds the current orientation with the given orientation.
        /// </summary>
        /// <param name="orientation">the orientation to add.</param>
        /// <returns>a new orientation representing the addition of the two 
        /// orientations. </returns>
        public abstract Orientation Add(Orientation orientation);

        public abstract void ReadXml(XmlElement element);
        public abstract void WriteXml(XmlElement element);
    }
}