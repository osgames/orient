/* ****************************************************
 * Name: IDirection.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Xml;

namespace PAGE.Generic.Math
{
    /// <summary>
    /// Represents a direction in some space.
    /// </summary>
    public abstract class Direction : IXmlSerializable
    {
        /// <summary>
        /// Inverts the current direction.
        /// </summary>
        /// <returns>a new direction representing the inverted direction.</returns>
        public abstract Direction Invert();

        /// <summary>
        /// Gets the angle between this direction and the given direction.
        /// </summary>
        /// <param name="direction">the direction to calculate the angle</param>
        /// <returns>The angle between this direction and the given direction.</returns>
        public abstract Angle AngleBetween(Direction direction);

        public abstract void ReadXml(XmlElement element);
        public abstract void WriteXml(XmlElement element);
    }
}