/* ****************************************************
 * Name: IScreenPosition.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/14 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

namespace PAGE.Generic.Math
{
    /// <summary>
    /// Represents a screen position in some space.
    /// </summary>
    public abstract class ScreenPosition
    {
        /// <summary>
        /// Returns the distance between this position and the given position.
        /// </summary>
        /// <param name="position">the position to calculate the distance to.</param>
        /// <returns>The distance to the given position.</returns>
        public abstract Distance DistanceTo(ScreenPosition position);

        /// <summary>
        /// Adds this position to another position.
        /// </summary>
        /// <param name="position">the position to be added.</param>
        /// <returns>a new position representing the adition of the two positions.</returns>
        public abstract ScreenPosition Add(ScreenPosition position);

        /// <summary>
        /// Subtracts this position with another position.
        /// </summary>
        /// <param name="position">the position to be subtracted.</param>
        /// <returns>a new position representing the subtraction of the two positions.</returns>
        public abstract ScreenPosition Subtract(ScreenPosition position);
    }
}
