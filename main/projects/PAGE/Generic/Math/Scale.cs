/* ****************************************************
 * Name: IScale.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/28 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Xml;

namespace PAGE.Generic.Math
{
    /// <summary>
    /// Represents a scale factor in some space.
    /// </summary>
    public abstract class Scale : IXmlSerializable
    {
        public abstract void ReadXml(XmlElement element);
        public abstract void WriteXml(XmlElement element);
    }
}