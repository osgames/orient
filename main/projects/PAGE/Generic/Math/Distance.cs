/* ****************************************************
 * Name: IDistance.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/02 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System;
using System.Xml;

namespace PAGE.Generic.Math
{
    /// <summary>
    /// Represents a distance (non-negative value) in some space.
    /// </summary>
    public class Distance : IComparable, IXmlSerializable
    {
        #region Static Values

        /// <summary>
        /// Represents the maximal distance measure.
        /// </summary>
        public static readonly Distance MaxValue = new Distance();

        /// <summary>
        /// Represents the minimal distance measure (no distance)
        /// </summary>
        public static readonly Distance ZeroValue = new Distance();

        #endregion

        #region Constructor

        protected Distance()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets a distance which is the result of the sum of this distance to 
        /// another distance.
        /// </summary>
        /// <param name="distance">the distance to be added.</param>
        /// <returns>a new distance representing the adition of the two distances.</returns>
        public Distance Add(Distance distance)
        {
            if (this.Equals(MaxValue) || distance.Equals(MaxValue))
            {
                return MaxValue;
            }
            if (this.Equals(ZeroValue))
            {
                return distance;
            }
            if (distance.Equals(ZeroValue))
            {
                return this;
            }

            //returns "normal" addition
            return this.AddDistance(distance);
        }

        /// <summary>
        /// Gets the distance (difference) between this distance and another distance.
        /// </summary>
        /// <param name="distance">the distance to be subtracted.</param>
        /// <returns>a new distance representing the subtraction of the two positions.</returns>
        public Distance Subtract(Distance distance)
        {
            if (distance.Equals(MaxValue))
            {
                return ZeroValue;
            }
            if (this.Equals(MaxValue))
            {
                return MaxValue;
            }
            if (this.Equals(ZeroValue))
            {
                return ZeroValue;
            }
            if (distance.Equals(ZeroValue))
            {
                return this;
            }

            //returns "normal" subtraction
            return this.SubtractDistance(distance);
        }

        public int CompareTo(object obj)
        {
            if (obj is Distance)
            {
                Distance distance = (Distance) obj;
                if (this.Equals(MaxValue))
                {
                    return distance.Equals(MaxValue) ? 0 : int.MaxValue;
                }
                if (distance.Equals(MaxValue))
                {
                    return int.MinValue;
                }
                if (this.Equals(ZeroValue) && distance.Equals(ZeroValue))
                {
                    return 0;
                }
                return this.CompareToDistance(distance);
            }

            throw new ArgumentException("Object is not a Distance");
        }

        /// <summary>
        /// When overriden in a derived class, gets a position corresponding to extend 
        /// this distance along a given direction.
        /// </summary>
        /// <param name="direction">the direction to extend this distance.</param>
        /// <returns>A position corresponding to extend this distance along the given
        /// direction.</returns>
        public virtual Position PositionAlongDirection(Direction direction)
        {
            return null; //to be overriden...
        }

        /// <summary>
        /// When overriden in a derived class, gets a portion of this distance.
        /// </summary>
        /// <param name="numFractions">the number of fractions to fractionate this 
        /// distance</param>
        /// <returns>The distance corresponding to divide this distance in the given
        /// parts.</returns>
        public virtual Distance Divide(int numFractions)
        {
            return null; //to be overriden...
        }

        /// <summary>
        /// When overriden in a derived class, gets this distance multiplied by 
        /// the given number.
        /// </summary>
        /// <param name="numTimes">the number of times to multiply this distance</param>
        /// <returns>This distance multiplied by the given times.</returns>
        public virtual Distance Multiply(int numTimes)
        {
            return null; //to be overriden...
        }

        #endregion

        #region Protected Virtual Methods

        /// <summary>
        /// To be overriden by derived classes. Adds a distance to another, in the 
        /// case when both adding distances are not MaxValue, MinValue or ZeroValue.
        /// </summary>
        /// <param name="distance">the distance to be added.</param>
        /// <returns>a new distance representing the adition of the two distances.</returns>
        protected virtual Distance AddDistance(Distance distance)
        {
            return ZeroValue;
        }

        /// <summary>
        /// To be overriden by derived classes. Subtracts a distance to another, in 
        /// the case when both distances are not MaxValue, MinValue or ZeroValue.
        /// </summary>
        /// <param name="distance">the distance to be subtracted.</param>
        /// <returns>a new distance representing the subtraction of the two positions.</returns>
        protected virtual Distance SubtractDistance(Distance distance)
        {
            return ZeroValue;
        }

        protected virtual int CompareToDistance(Distance distance)
        {
            return 0;
        }

        #endregion

        #region IXmlSerializable Members

        public virtual void ReadXml(XmlElement element)
        {
        }

        public virtual void WriteXml(XmlElement element)
        {
        }

        #endregion
    }
}