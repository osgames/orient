/* ****************************************************
 * Name: IPosition.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/28 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Xml;

namespace PAGE.Generic.Math
{
    /// <summary>
    /// Represents a position in some space.
    /// </summary>
    public abstract class Position : IXmlSerializable
    {
        /// <summary>
        /// Returns the distance between this position and the given position.
        /// </summary>
        /// <param name="position">the position to calculate the distance to.</param>
        /// <returns>The distance to the given position.</returns>
        public abstract Distance DistanceTo(Position position);

        /// <summary>
        /// Gets the direction between this position and the given position.
        /// </summary>
        /// <param name="position">the position to create the direction to.</param>
        public abstract Direction DirectionTo(Position position);

        /// <summary>
        /// Adds this position to another position.
        /// </summary>
        /// <param name="position">the position to be added.</param>
        /// <returns>a new position representing the adition of the two positions.</returns>
        public abstract Position Add(Position position);

        /// <summary>
        /// Subtracts this position with another position.
        /// </summary>
        /// <param name="position">the position to be subtracted.</param>
        /// <returns>a new position representing the subtraction of the two positions.</returns>
        public abstract Position Subtract(Position position);

        /// <summary>
        /// Inverts the current position.
        /// </summary>
        /// <returns>a new position representing the invert position.</returns>
        public abstract Position Invert();

        public abstract void ReadXml(XmlElement element);
        public abstract void WriteXml(XmlElement element);
    }
}