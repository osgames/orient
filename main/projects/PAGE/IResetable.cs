/* ****************************************************
 * Name: IResetable.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/02/21 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
namespace PAGE
{
    /// <summary>
    /// Represents an object that can be reseted to an initial state.
    /// </summary>
    public interface IResetable
    {
        /// <summary>
        /// Resets the object to its logical initial state.
        /// </summary>
        void Reset(); 
    }
}
