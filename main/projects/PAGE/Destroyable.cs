/* ****************************************************
 * Name: Destroyable.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/03/28 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

namespace PAGE
{
    /// <summary>
    /// Respresents an object that can be destroyed.
    /// </summary>
    public class Destroyable : IDestroyable
    {
        #region Fields

        protected bool destroyed = false;

        #endregion

        #region Properties

        public virtual bool Destroyed
        {
            get { return this.destroyed; }
        }

        #endregion

        #region Public Methods

        public virtual void VerifyDestruction()
        {
            if (this.destroyed) throw new ObjectDestroyedException("Graphical element:");
        }

        /// <summary>
        /// Resets the element by setting its destroyed attribute to false.
        /// </summary>
        public virtual void Reset()
        {
            this.destroyed = false;
        }

        /// <summary>
        /// Sets the element destoyed attribute to true.
        /// </summary>
        public virtual void Destroy()
        {
            this.destroyed = true;
        }

        #endregion
    }
}
