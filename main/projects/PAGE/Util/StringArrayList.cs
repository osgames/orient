/* ****************************************************
 * Name: StringArrayList.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections.Generic;

namespace PAGE.Util
{
    /// <summary>
    /// List of strings with hash functionality.
    /// </summary>
    [Serializable]
    public class StringArrayList : List<string>
    {
        #region Fields

        private Dictionary<string, string> hashList = new Dictionary<string, string>();

        #endregion

        #region Public Methods

        public new void Add(string str)
        {
            if ((str == null) || this.hashList.ContainsKey(str)) return;
            this.hashList.Add(str, str);
            base.Add(str);
        }

        public new bool Contains(string str)
        {
            return hashList.ContainsKey(str);
        }

        public new void Insert(int index, string str)
        {
            if (this.Contains(str)) this.Remove(str);
            this.hashList.Add(str, str);
            base.Insert(index, str);
        }

        public void AddRange(StringArrayList list)
        {
            foreach (string s in list) this.Add(s);
        }

        public new void RemoveAt(int index)
        {
            this.hashList.Remove(this[index]);
            base.RemoveAt(index);
        }

        public new void Remove(string str)
        {
            this.hashList.Remove(str);
            base.Remove(str);
        }

        public new void Clear()
        {
            this.hashList.Clear();
            base.Clear();
        }

        public StringArrayList Clone()
        {
            StringArrayList list = new StringArrayList();
            foreach (string str in this) list.Add(str);
            return list;
        }

        #endregion
    }
}
