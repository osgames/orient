/* ****************************************************
 * Name: PathUtil.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System.IO;
using System.Reflection;

namespace PAGE.Util
{
	/// <summary>
	/// Summary description for Path.
	/// </summary>
	public class PathUtil
	{
		/// <summary>
		/// Return the absolute path for the given file name,
		/// according to the application's running directory.
		/// </summary>
		/// <param name="fileName">the file/directory name</param>
		/// <returns>the full path name</returns>
		public static string ReturnFullPath(string fileName)
		{
			string path = Path.GetDirectoryName( 
				Assembly.GetExecutingAssembly().GetName().CodeBase );
			path = path.Replace("file:\\", "");
			path += "\\" + fileName;

			return path;
		}

		/// <summary>
		/// Return the absolute path according to the application's
		/// running directory.
		/// </summary>
		public static string ReturnFullPath()
		{
			string path = Path.GetDirectoryName( 
				Assembly.GetExecutingAssembly().GetName().CodeBase );
			path = path.Replace("file:\\", "");

			return path;
		}

        /// <summary>
        /// Recursively deletes all the files in the given directory.
        /// </summary>
        /// <param name="path">the path to the directory to delete</param>
        public static void DeleteDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                foreach(string directory in Directory.GetDirectories(path))
                {
                    DeleteDirectory(directory);
                }
                foreach (string file in Directory.GetFiles(path))
                {
                    File.Delete(file);
                }
                Directory.Delete(path);
            }
        }

        /// <summary>
        /// Deletes all the files in the given directory.
        /// </summary>
        /// <param name="path">the path to the directory to delete</param>
        public static void ClearDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                foreach (string file in Directory.GetFiles(path))
                {
                    try
                    {
                        File.Delete(file);
                    }catch(IOException)
                    {
                        //Ignore
                    }
                }
            }
        }
	}
}
