/* ****************************************************
 * Name: TokenObjectList.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections.Generic;

namespace PAGE.Util
{
    /// <summary>
    /// Represents a list of TokenObjects or some derived class, with indexing 
    /// (by token id) functionality.
    /// </summary>
    [Serializable]
    public class TokenObjectList<TokenObjectType> 
        : List<TokenObjectType> where TokenObjectType : TokenObject
    {
        #region Fields

        private Dictionary<string, TokenObjectType> hash = 
            new Dictionary<string, TokenObjectType>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets a list of all the token objects's ids stored in the list.
        /// </summary>
        public List<string> IDs
        {
            get { return new List<string>(this.hash.Keys); }
        }

        /// <summary>
        /// Gets the TokenObject associated with the given id.
        /// </summary>
        /// <param name="idToken">the id string that identifies the desired 
        /// TokenObject.</param>
        /// <returns>the TokenObject associated with the given id, or null 
        /// if there is no stored object with that id.</returns>
        public TokenObjectType this[string idToken]
        {
            get
            {
                if (!this.hash.ContainsKey(idToken)) return null;
                return this.hash[idToken];
            }
        }

        /// <summary>
        /// Gets the TokenObject at the given index position on the list.
        /// </summary>
        /// <param name="index">the index of TokeObject to retrieve.</param>
        /// <returns>the TokenObject at the given index, or null if the given index
        /// is invalid.</returns>
        public new TokenObjectType this[int index]
        {
            get
            {
                if(index < this.Count) return base[index];
                return null;
            }
            set { base[index] = value; }
        }

        #endregion

        #region Constructors

        public TokenObjectList()
        {
        }

        /// <summary>
        /// Creates a new TokenObjectList based on a collection of TokenObjectType objects.
        /// </summary>
        /// <param name="collection">the collection to copy the TokenObjectType 
        /// objects from.</param>
        public TokenObjectList(IEnumerable<TokenObjectType> collection)
        {
            this.AddRange(collection);
        }

        #endregion

        #region Public Methods

        public new void Add(TokenObjectType tok)
        {
            //also adds TokenObject to hash list
            if(this.hash.ContainsKey(tok.IdToken)) return;
            this.hash[tok.IdToken] = tok;
            base.Add(tok);
        }

        public new void Remove(TokenObjectType tok)
        {
            //also removes TokenObject from hash list
            this.hash.Remove(tok.IdToken);
            base.Remove(tok);
        }

        /// <summary>
        /// Removes the TokenObject with the given id from the list, if it exists.
        /// </summary>
        /// <param name="idToken">the id of the TokenObject to remove from the list.</param>
        public void Remove(string idToken)
        {
            if (!this.hash.ContainsKey(idToken)) return;
            TokenObjectType tok = this.hash[idToken];
            this.hash.Remove(idToken);
            base.Remove(tok);
        }

        public new void Clear()
        {
            //also clears the hash list
            this.hash.Clear();
            base.Clear();
        }

        public new bool Contains(TokenObjectType tok)
        {
            return this.hash.ContainsKey(tok.IdToken);
        }

        /// <summary>
        /// Checks if a TokenObject with the given id is stored in the list.
        /// </summary>
        /// <param name="idToken">the id of the TokenObject to look up for.</param>
        /// <returns>true if a TokenObject with the given id is stored in the list,
        /// false otherwise.</returns>
        public bool Contains(string idToken)
        {
            return this.hash.ContainsKey(idToken);
        }

        /// <summary>
        /// Creates a new list of TokenObjects that contains the same elements 
        /// as this list.
        /// </summary>
        /// <returns>the cloned TokenObjectList.</returns>
        public TokenObjectList<TokenObjectType> Clone()
        {
            TokenObjectList<TokenObjectType> tol = new TokenObjectList<TokenObjectType>();
            foreach (TokenObjectType tok in this) tol.Add(tok);
            return tol;
        }
        #endregion
    }
}
