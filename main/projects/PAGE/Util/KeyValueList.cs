/* ****************************************************
 * Name: KeyValueList.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2008/04/23
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/

using System.Collections.Generic;
using System.Xml;

namespace PAGE.Util
{
    /// <summary>
    /// Represents a pair of values key - value strings
    /// </summary>
    public struct KeyValuePair : IXmlSerializable
    {
        public string key;
        public string value;

        public KeyValuePair(string key, string value)
        {
            this.key = key;
            this.value = value;
        }

        public void ReadXml(XmlElement element)
        {
            this.key = element.GetAttribute("key");
            this.value = element.GetAttribute("value");
        }

        public void WriteXml(XmlElement element)
        {
            element.SetAttribute("key", this.key);
            element.SetAttribute("value", this.value);
        }
    }

    /// <summary>
    /// Represents a list of key - value pairs, with hash functionality.
    /// </summary>
    public class KeyValueList : List<KeyValuePair>
    {
        #region Fields

        //(key - value) index
        private Dictionary<string , string > hash = new Dictionary<string, string>();

        #endregion

        #region Public Methods

        public string this[string key]
        {
            get { return this.hash[key]; }
            set
            {
                if(this.Contains(key)) this.Remove(key);
                this.Add(key, value);
            }
        }

        public void Add(string key, string value)
        {
            KeyValuePair kvp = new KeyValuePair(key, value);
            this.Add(kvp);
        }

        public new void Add(KeyValuePair keyValuePair)
        {
            if(!this.hash.ContainsKey(keyValuePair.key))
                this.hash.Add(keyValuePair.key, keyValuePair.value);
            base.Add(keyValuePair);
        }

        public bool Contains(string key)
        {
            return this.hash.ContainsKey(key);
        }

        public new void Insert(int index, KeyValuePair keyValuePair)
        {
            if (this.Contains(keyValuePair)) this.Remove(keyValuePair);
            this.hash.Add(keyValuePair.key, keyValuePair.value);
            base.Insert(index, keyValuePair);
        }

        public void AddRange(KeyValueList list)
        {
            foreach (KeyValuePair kvp in list) this.Add(kvp);
        }

        public new void RemoveAt(int index)
        {
            this.hash.Remove(this[index].key);
            base.RemoveAt(index);
        }

        public void Remove(string key)
        {
            if (!this.Contains(key)) return;
            KeyValuePair pair = new KeyValuePair(key, this[key]);
            this.Remove(pair);
        }

        public new void Remove(KeyValuePair keyValuePair)
        {
            this.hash.Remove(keyValuePair.key);
            base.Remove(keyValuePair);
        }

        public new void Clear()
        {
            this.hash.Clear();
            base.Clear();
        }

        public KeyValueList Clone()
        {
            KeyValueList list = new KeyValueList();
            foreach (KeyValuePair kvp in this) list.Add(kvp);
            return list;
        }

        #endregion
    }
}