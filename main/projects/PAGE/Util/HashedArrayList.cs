/* ****************************************************
 * Name: HashedArrayList.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.Collections;

namespace PAGE.Util
{
	/// <summary>
	/// Summary description for HashedArrayList.
	/// </summary>
	[Serializable()]
	public class HashedArrayList : ICollection
	{
		#region Fields
		private Hashtable list = new Hashtable();

		#endregion

		#region Constructors
		public HashedArrayList()
		{

		}
		#endregion

		#region Properties

		public ICollection Values
		{
			get{return this.list.Values;}
		}
		#endregion

		#region Public Methods

		public void Add(object obj)
		{
			this.list.Add(obj, obj);
		}

		public void Remove(object obj)
		{
			this.list.Remove(obj);
		}

		public bool Contains(object obj)
		{
			return this.list.Contains(obj);
		}

		public void Clear()
		{
			this.list.Clear();
		}

		public HashedArrayList Clone()
		{
			HashedArrayList hashlist = new HashedArrayList();
			foreach(object obj in this.list.Values)
			{
				if(!hashlist.Contains(obj))
					hashlist.Add(obj);
			}
			return hashlist;
		}
		
		#endregion

		#region ICollection Members

		public bool IsSynchronized
		{
			get
			{
				return this.list.IsSynchronized;
			}
		}

		public int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		public void CopyTo(Array array, int index)
		{
			this.list.CopyTo(array, index);
		}

		public object SyncRoot
		{
			get
			{
				return this.list.SyncRoot;
			}
		}

		#endregion

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return this.list.Values.GetEnumerator();
		}

		#endregion
	}
}
