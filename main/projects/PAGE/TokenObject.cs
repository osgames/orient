/* ****************************************************
 * Name: TokenObject.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;

namespace PAGE
{
    /// <summary>
    /// Class representing objects with a string identifier.
    /// </summary>
    [Serializable]
    public class TokenObject : IComparable, IXmlSerializable, IDisposable
    {
        #region Fields

        protected string idToken = "";		//the object's identifier idToken
        protected string description = "";

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the object's identifier idToken
        /// </summary>
        public virtual string IdToken
        {
            get { return this.idToken; }
            set { this.idToken = value; }
        }

        /// <summary>
        /// Gets or sets the object's description
        /// </summary>
        public virtual string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new TokenObject named with the given string
        /// </summary>
        /// <param idToken="idToken">the object's identifier idToken</param>
        public TokenObject(string idToken)
        {
            this.idToken = idToken;
        }

        #endregion

        #region Override Methods

        public override string ToString()
        {
            return "idToken - " + this.idToken.ToUpper();
        }

        public override bool Equals(Object o)
        {
            if ((o == null) ||
                (!this.GetType().Equals(o.GetType())))
            {
                return false;
            }

            TokenObject dobj = (TokenObject)o;

            return dobj.IdToken.Equals(this.idToken);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public int CompareTo(object obj)
        {
            try
            {
                return this.idToken.CompareTo(((TokenObject) obj).IdToken);
            }
            catch(InvalidCastException)
            {
                return -1;
            }
        }

        public virtual void Dispose()
        {
        }

        #endregion

        #region Serialization Methods

        /// <summary>
        /// Saves (serializes) the resource object to the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name to save the object info into.</param>
        public virtual void SaveToXml(string xmlFileName)
        {
            this.SaveToXml(xmlFileName, this.GetType().Name);
        }

        /// <summary>
        /// Saves (serializes) the resource object to the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name to save the object info into.</param>
        /// <param name="rootName">the name for the root XML element.</param>
        public void SaveToXml(string xmlFileName, string rootName)
        {
            try
            {
                //creates xml document
                XmlDocument xd = new XmlDocument();

                //root element
                XmlElement rootElement = xd.CreateElement(rootName);
                xd.CreateXmlDeclaration("1.0", Encoding.UTF8.EncodingName, "");
                xd.AppendChild(rootElement);

                //write contents
                this.WriteXml(rootElement);

                //verifies file existence
                if (File.Exists(xmlFileName)) File.Delete(xmlFileName);

                //save the document to file
                StreamWriter writer = new StreamWriter(xmlFileName, false, Encoding.UTF8);
                xd.Save(writer);
                writer.Close();
            }
            catch (XmlException)    //do nothing...
            {
            }
            catch (IOException)    //do nothing...
            {
            }
            catch (InvalidOperationException)    //do nothing...
            {
            }
        }

        /// <summary>
        /// Loads (deserializes) the resource object from the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name containing the object info.</param>
        /// <returns>the ResourceAsset loaded from the XML file.</returns>
        public virtual void LoadFromXml(string xmlFileName)
        {
            this.LoadFromXml(xmlFileName, this.GetType().Name);
        }

        /// <summary>
        /// Loads (deserializes) the resource object from the given XML file.
        /// </summary>
        /// <param name="xmlFileName">the XML file name containing the object info.</param>
        /// <param name="rootName">the name for the root XML element.</param>
        public void LoadFromXml(string xmlFileName, string rootName)
        {
            if (!File.Exists(xmlFileName)) return;

            XmlDocument xd = new XmlDocument();
            try
            {
                xd.Load(xmlFileName);

                //loads root element
                XmlElement rootElement = (XmlElement)xd.SelectSingleNode(rootName);
                if (rootElement == null) return;

                this.ReadXml(rootElement);
            }
            catch (XmlException)      //do nothing...
            {
            }
            catch (XPathException)    //do nothing...
            {
            }
        }

        public virtual void ReadXml(XmlElement element)
        {
            this.IdToken = element.GetAttribute("id");
            this.Description = element.GetAttribute("description");
        }

        public virtual void WriteXml(XmlElement element)
        {
            element.SetAttribute("id", this.IdToken.ToLower());
            element.SetAttribute("description", this.Description);
        }

        #endregion
    }
}
