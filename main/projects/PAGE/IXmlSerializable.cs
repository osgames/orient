using System.Xml;

namespace PAGE
{
    public interface IXmlSerializable
    {
        void ReadXml(XmlElement element);
        void WriteXml(XmlElement element);
    }
}
