/* ****************************************************
 * Name: ObjectDestroyedException.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2007/03/21 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
using System;

namespace PAGE
{
    public class ObjectDestroyedException : ApplicationException
    {
        public ObjectDestroyedException(string message): 
            base("Object has been destroyed: " + message)
        {
            
        }
    }
}
