/* ****************************************************
 * Name: IUpdatable.cs
 * Code: Pedro Sequeira - pedrodbs@gmail.com
 * Date: 2006/11/18 
 *
 * Inesc-ID GAIPS
 * 
 *****************************************************/
namespace PAGE
{
    /// <summary>
    /// Represents objects that are updatable at certain times.
    /// </summary>
    public interface IUpdatable
    {
        /// <summary>
        /// Updates the object state, according to the passed time.
        /// </summary>
        /// <param name="millisSinceLastUpdate">the number of milliseconds
        /// that ocurred since the last update.</param>
        void Update(float millisSinceLastUpdate); 
    }
}
